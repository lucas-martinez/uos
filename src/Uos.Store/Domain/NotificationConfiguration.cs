﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Domain
{
    internal static class NotificationConfiguration
    {
        public static void Notification(this DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<NotificationEntity>()
                .ToTable("notifications")
                .HasKey(e => e.NotificationId)
                .Property<NotificationEntity>(e => e.CreatedOn, (property) =>
                {
                    property.HasColumnName("created_on");
                    property.IsRequired();
                })
                .Property<NotificationEntity>(e => e.NotificationId, (property) =>
                {
                    property.HasColumnName("id");
                    property.IsRequired();
                })
                .Property<NotificationEntity, MessageTypes>(e => e.NotificationLevel, (property) =>
                {
                    property.HasColumnName("level");
                    property.IsRequired();
                })
                .Property<NotificationEntity>(e => e.Link, (property) =>
                {
                    property.HasColumnName("link");
                    property.HasMaxLength(Lengths.Url);
                    property.IsOptional();
                    property.IsUnicode(false);
                })
                .Property<NotificationEntity>(e => e.Message, (property) =>
                {
                    property.HasColumnName("message");
                    property.HasMaxLength(Lengths.MessageText);
                    property.IsOptional();
                    property.IsUnicode(false);
                })
                .Property<NotificationEntity>(e => e.UserEmailAddress, (property) =>
                {
                    property.HasColumnName("user");
                    property.HasMaxLength(Lengths.EmailAddress);
                    property.IsRequired();
                });
        }
    }
}
