﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Domain
{
    internal static class DomainConfiguration
    {
        public static void Domain(this DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CompanyEntity>()
                .ToTable("companies")
                .HasKey(e => e.CompanyCode)
                .Property<CompanyEntity>(e => e.CompanyCode, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_CompanyCode", 1)));
                    property.HasColumnName("code");
                    property.HasMaxLength(Lengths.CompanyCode);
                    property.IsRequired();
                    property.IsUnicode(false);
                })
                .Property<CompanyEntity, int>(e => e.CompanyId, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_CompanyId", 1)));
                    property.HasColumnName("id");
                    property.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
                    property.IsRequired();
                })
                .Property<CompanyEntity>(e => e.CompanyName, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_CompanyName", 2)));
                    property.HasColumnName("name");
                    property.HasMaxLength(Lengths.CompanyName);
                    property.IsRequired();
                    property.IsUnicode(true);
                })
                .Property<CompanyEntity>(e => e.CompanySize, (property) =>
                {
                    property.HasColumnName("size");
                    property.HasMaxLength(Lengths.CompanySize);
                    property.IsFixedLength();
                    property.IsOptional();
                    property.IsUnicode(false);
                })
                .Property<CompanyEntity>(e => e.CompanyType, (property) =>
                {
                    property.HasColumnName("type");
                    property.HasMaxLength(Lengths.CompanyType);
                    property.IsFixedLength();
                    property.IsOptional();
                    property.IsUnicode(false);
                })
                .Property<CompanyEntity>(e => e.Country, (property) =>
                {
                    property.HasColumnName("country");
                    property.HasMaxLength(Lengths.Country);
                    property.IsFixedLength();
                    property.IsOptional();
                    property.IsUnicode(false);
                })
                .Property<CompanyEntity, DateTime>(e => e.CreatedOn, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_CompanyCreated", 4)));
                    property.HasColumnName("created_utc");
                    property.IsRequired();
                })
                .Property<CompanyEntity>(e => e.Currency, (property) =>
                {
                    property.HasColumnName("currency");
                    property.HasMaxLength(Lengths.Currency);
                    property.IsFixedLength();
                    property.IsOptional();
                    property.IsUnicode(false);
                })
                .Property<CompanyEntity>(e => e.Industry, (property) =>
                {
                    property.HasColumnName("industry");
                    property.HasMaxLength(Lengths.Industry);
                    property.IsOptional();
                    property.IsUnicode(false);
                })
                .Property<CompanyEntity>(e => e.MetricSystem, (property) =>
                {
                    property.HasColumnName("metric");
                    property.HasMaxLength(Lengths.MetricSystem);
                    property.IsFixedLength();
                    property.IsOptional();
                    property.IsUnicode(false);
                })
                .Property<CompanyEntity, DateTime>(e => e.RemovedOn, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_CompanyRemoved", 5)));
                    property.HasColumnName("removed_utc");
                    property.IsOptional();
                });
            

            modelBuilder.Entity<CompanyFeatureEntity>()
                .ToTable("features")
                .HasKey(e => new {e.CompanyCode, e.FeatureName })
                .HasRequired(e => e.Company, (navigationProperty) =>
                {
                    navigationProperty.WithMany(e => e.Features).HasForeignKey(e => e.CompanyCode);
                })
                .Property<CompanyFeatureEntity>(e => e.CompanyCode, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_FeatureCompany", 2)));
                    property.HasColumnName("company");
                    property.HasMaxLength(Lengths.CompanyCode);
                    property.IsRequired();
                    property.IsUnicode(false);
                })
                .Property<CompanyFeatureEntity, DateTime>(e => e.CreatedOn, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_FeatureCreated", 4)));
                    property.HasColumnName("created_utc");
                    property.IsRequired();
                })
                .Property<CompanyFeatureEntity>(e => e.FeatureName, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_FeatureName", 3)));
                    property.HasColumnName("name");
                    property.HasMaxLength(Lengths.FeatureName);
                    property.IsRequired();
                    property.IsUnicode(false);
                })
                .Property<CompanyFeatureEntity, DateTime>(e => e.RemovedOn, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_FeatureRemoved", 5)));
                    property.HasColumnName("removed_utc");
                    property.IsOptional();
                })
                .Property<CompanyFeatureEntity, int>(e => e.SubscriptionId, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_FeatureId", 1)));
                    property.HasColumnName("id");
                    property.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
                    property.IsRequired();
                });

            modelBuilder.Entity<CompanyNoticeEntity>()
                .ToTable("company_remarks")
                .HasKey(e => new { e.CompanyCode, e.CreatedOn })
                .HasRequired(e => e.Company, (navigationProperty) =>
                {
                    navigationProperty.WithMany(e => e.Remarks).HasForeignKey(e => e.CompanyCode);
                })
                .Property<CompanyNoticeEntity>(e => e.CompanyCode, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_CompanyNoticeCompany", 2)));
                    property.HasColumnName("company");
                    property.HasMaxLength(Lengths.CompanyCode);
                    property.IsRequired();
                    property.IsUnicode(false);
                })
                .Property<CompanyNoticeEntity, DateTime>(e => e.CreatedOn, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_CompanyNoticeCreated", 3)));
                    property.HasColumnName("created_utc");
                    property.IsRequired();
                })
                .Property<CompanyNoticeEntity>(e => e.Text, (property) =>
                {
                    property.HasColumnName("notice");
                    property.HasMaxLength(Lengths.Remarks);
                    property.IsRequired();
                    property.IsUnicode(true);
                })
                .Property<CompanyNoticeEntity, DateTime>(e => e.RemovedOn, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_CompanyNoticeRemoved", 4)));
                    property.HasColumnName("removed_utc");
                    property.IsOptional();
                })
                .Property<CompanyNoticeEntity, int>(e => e.NoticeId, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_CompanyNoticeId", 1)));
                    property.HasColumnName("id");
                    property.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
                    property.IsRequired();
                });

            modelBuilder.Entity<LocationEntity>()
                .ToTable("locations")
                .HasKey(e => e.LocationId)
                .Property<LocationEntity>(e => e.Address, (property) =>
                {
                    property.HasColumnName("address");
                    property.HasMaxLength(Lengths.StreetAddress);
                    property.IsOptional();
                    property.IsUnicode(true);
                })
                .Property<LocationEntity, decimal>(e => e.Altitude, (property) =>
                {
                    property.HasColumnName("altitude");
                    property.IsOptional();
                })
                .Property<LocationEntity>(e => e.Culture, (property) =>
                {
                    property.HasColumnName("culture");
                    property.HasMaxLength(Lengths.Culture);
                    property.IsOptional();
                    property.IsUnicode(false);
                })
                .Property<LocationEntity>(e => e.DbGeography, (property) =>
                {
                    property.HasColumnName("geography");
                    property.IsOptional();
                })
                .Property<LocationEntity, decimal>(e => e.Latitude, (property) =>
                {
                    property.HasColumnName("latitude");
                    property.IsOptional();
                })
                .Property<LocationEntity>(e => e.Locality, (property) =>
                {
                    property.HasColumnName("locality");
                    property.HasMaxLength(Lengths.Name);
                    property.IsOptional();
                    property.IsUnicode(true);
                })
                .Property<LocationEntity, Guid>(e => e.LocationId, (property) =>
                {
                    property.HasColumnName("id");
                    property.IsRequired();
                })
                .Property<LocationEntity, decimal>(e => e.Longitude, (property) =>
                {
                    property.HasColumnName("longitude");
                    property.IsOptional();
                })
                .Property<LocationEntity>(e => e.PostalCode, (property) =>
                {
                    property.HasColumnName("postal");
                    property.HasMaxLength(Lengths.PostalCode);
                    property.IsOptional();
                    property.IsUnicode(false);
                })
                .Property<LocationEntity>(e => e.Region, (property) =>
                {
                    property.HasColumnName("region");
                    property.HasMaxLength(Lengths.Name);
                    property.IsOptional();
                    property.IsUnicode(true);
                })
                .Property<LocationEntity>(e => e.State, (property) =>
                {
                    property.HasColumnName("state");
                    property.HasMaxLength(Lengths.State);
                    property.IsOptional();
                    property.IsUnicode(false);
                });

            modelBuilder.Entity<StafferEntity>()
                .ToTable("staffers")
                .HasKey(e => new { e.CompanyCode, e.EmailAddress })
                .HasRequired(e => e.Company, (navigationProperty) =>
                {
                    navigationProperty.WithMany(e => e.Staffers).HasForeignKey(e => e.CompanyCode).WillCascadeOnDelete(true);
                })
                .Property<StafferEntity>(e => e.CompanyCode, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_StafferCompany", 1)));
                    property.HasColumnName("company");
                    property.HasMaxLength(Lengths.CompanyCode);
                    property.IsRequired();
                })
                .Property<StafferEntity, DateTime>(e => e.CreatedOn, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_StafferCreated", 4)));
                    property.HasColumnName("created_utc");
                    property.IsRequired();
                })
                .Property<StafferEntity>(e => e.EmailAddress, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_StafferEmail", 2)));
                    property.HasColumnName("email");
                    property.HasMaxLength(Lengths.EmailAddress);
                    property.IsRequired();
                    property.IsUnicode(false);
                })
                .Property<StafferEntity>(e => e.FirstName, (property) =>
                {
                    property.HasColumnName("forename");
                    property.HasMaxLength(Lengths.Name);
                    property.IsOptional();
                    property.IsUnicode(true);
                })
                .Property<StafferEntity>(e => e.LastName, (property) =>
                {
                    property.HasColumnName("surname");
                    property.HasMaxLength(Lengths.Name);
                    property.IsOptional();
                    property.IsUnicode(true);
                })
                .Property<StafferEntity, DateTime>(e => e.RemovedOn, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_StafferRemoved", 5)));
                    property.HasColumnName("removed_utc");
                    property.IsOptional();
                })
                .Property<StafferEntity, int>(e => e.StafferId, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_StafferId", 3) { IsUnique = true }));
                    property.HasColumnName("id");
                    property.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
                    property.IsRequired();
                });

            modelBuilder.Entity<StafferNoticeEntity>()
                .ToTable("staffer_remarks")
                .HasKey(e => new { e.CompanyCode, e.Identity, e.CreatedOn })
                .HasRequired(e => e.Company, (navigationProperty) =>
                {
                    navigationProperty.WithMany().HasForeignKey(e => e.CompanyCode).WillCascadeOnDelete(false);
                })
                .HasRequired(e => e.Staffer, (navigationProperty) =>
                {
                    navigationProperty.WithMany(e => e.Remarks).HasForeignKey(e => new { e.CompanyCode, e.Identity });
                })
                .Property<StafferNoticeEntity>(e => e.CompanyCode, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_StafferNoticeCompany", 2)));
                    property.HasColumnName("company");
                    property.HasMaxLength(Lengths.CompanyCode);
                    property.IsRequired();
                    property.IsUnicode(false);
                })
                .Property<StafferNoticeEntity>(e => e.Identity, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_StafferNoticeIdentity", 2)));
                    property.HasColumnName("identity");
                    property.HasMaxLength(Lengths.CompanyCode);
                    property.IsRequired();
                    property.IsUnicode(false);
                })
                .Property<StafferNoticeEntity, DateTime>(e => e.CreatedOn, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_StafferNoticeCreated", 3)));
                    property.HasColumnName("created_utc");
                    property.IsRequired();
                })
                .Property<StafferNoticeEntity>(e => e.Text, (property) =>
                {
                    property.HasColumnName("notice");
                    property.HasMaxLength(Lengths.Remarks);
                    property.IsRequired();
                    property.IsUnicode(true);
                })
                .Property<StafferNoticeEntity, DateTime>(e => e.RemovedOn, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_StafferNoticeRemoved", 4)));
                    property.HasColumnName("removed_utc");
                    property.IsOptional();
                })
                .Property<StafferNoticeEntity, int>(e => e.NoticeId, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_StafferNoticeId", 1)));
                    property.HasColumnName("id");
                    property.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
                    property.IsRequired();
                });

            modelBuilder.Entity<StafferRoleEntity>()
                .ToTable("staffer_roles")
                .HasKey(e => new { e.CompanyCode, e.EmailAddress, e.RoleName })
                .HasRequired(e => e.Company, (navigationProperty) =>
                {
                    navigationProperty.WithMany(e => e.Roles).HasForeignKey(e => e.CompanyCode).WillCascadeOnDelete(false); ;
                })
                .HasRequired(e => e.Staffer, (navigationProperty) =>
                {
                    navigationProperty.WithMany(e => e.Roles).HasForeignKey(e => new { e.CompanyCode, e.EmailAddress }).WillCascadeOnDelete(true); ;
                })
                .Property<StafferRoleEntity>(e => e.CompanyCode, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_StafferRoleCompany", 1)));
                    property.HasColumnName("company");
                    property.HasMaxLength(Lengths.CompanyCode);
                    property.IsRequired();
                    property.IsUnicode(false);
                })
                .Property<StafferRoleEntity, DateTime>(e => e.CreatedOn, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_StafferRoleCreated", 5)));
                    property.HasColumnName("created_utc");
                    property.IsRequired();
                })
                .Property<StafferRoleEntity>(e => e.EmailAddress, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_StafferRoleEmail", 2)));
                    property.HasColumnName("email");
                    property.HasMaxLength(Lengths.EmailAddress);
                    property.IsRequired();
                    property.IsUnicode(false);
                })
                .Property<StafferRoleEntity, DateTime>(e => e.RemovedOn, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_StafferRoleRemoved", 6)));
                    property.HasColumnName("removed_utc");
                    property.IsOptional();
                })
                .Property<StafferRoleEntity, int>(e => e.RoleId, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_StafferRoleId", 4) { IsUnique = true }));
                    property.HasColumnName("id");
                    property.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
                    property.IsRequired();
                })
                .Property<StafferRoleEntity>(e => e.RoleName, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_StafferRoleName", 3)));
                    property.HasColumnName("name");
                    property.HasMaxLength(Lengths.RoleName);
                    property.IsRequired();
                    property.IsUnicode(false);
                });
        }
    }
}
