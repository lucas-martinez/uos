﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Uos.Domain
{
    public class StafferStore : EntityStore<StafferEntity>, IStafferStore
    {
        public static implicit operator StafferStore(DbContext dbContext)
        {
            return dbContext != null ? new StafferStore(dbContext) : null;
        }

        public StafferStore(DbContext dbContext)
            : base(dbContext)
        {
        }

        public async Task<bool> AnyAsync(string companyCode, string identity, string roleName = null)
        {
            if (companyCode == null) throw new ArgumentNullException("companyCode");

            if (identity == null) throw new ArgumentNullException("identity");
            
            return roleName != null
                ? await Set<StafferRoleEntity>().AnyAsync(e => e.CompanyCode == companyCode && e.EmailAddress == identity && e.RoleName == roleName)
                : await Set<StafferRoleEntity>().AnyAsync(e => e.CompanyCode == companyCode && e.EmailAddress == identity);
        }

        public async Task<StafferEntity> CreateStafferAsync(StafferEntity stafferEntity)
        {
            if (stafferEntity == null) throw new ArgumentNullException("stafferEntity");

            stafferEntity = Set<StafferEntity>().Add(stafferEntity);

            await SaveChangesAsync();

            return stafferEntity;
        }

        public async Task<StafferEntity> DeleteStafferAsync(int stafferId)
        {
            var dbSet = Set<StafferEntity>();

            var stafferEntity = await dbSet.SingleOrDefaultAsync(e => e.StafferId == stafferId);

            if (stafferEntity != null)
            {
                dbSet.Remove(stafferEntity);

                await SaveChangesAsync();
            }

            return stafferEntity;
        }

        public async Task<StafferEntity> DeleteStafferAsync(string companyCode, string identity)
        {
            if (companyCode == null) throw new ArgumentNullException("companyCode");

            if (identity == null) throw new ArgumentNullException("identity");

            var dbSet = Set<StafferEntity>();

            var stafferEntity = await dbSet.FindAsync(companyCode, identity);

            if (stafferEntity != null)
            {
                dbSet.Remove(stafferEntity);

                await SaveChangesAsync();
            }

            return stafferEntity;
        }

        public async Task<StafferRoleEntity> DeleteStafferRoleAsync(int roleId)
        {   
            var dbSet = Set<StafferRoleEntity>();

            var roleEntity = await dbSet.SingleOrDefaultAsync(e => e.RoleId == roleId);

            if (roleEntity != null)
            {
                dbSet.Remove(roleEntity);

                await SaveChangesAsync();
            }
            
            return roleEntity;
        }

        public async Task<StafferRoleEntity> DeleteStafferRoleAsync(string companyCode, string identity, string roleName)
        {
            if (companyCode == null) throw new ArgumentNullException("companyCode");

            if (identity == null) throw new ArgumentNullException("identity");

            var dbSet = Set<StafferRoleEntity>();

            var roleEntity = await dbSet.FindAsync(companyCode, identity, roleName);

            if (roleEntity != null)
            {
                dbSet.Remove(roleEntity);

                await SaveChangesAsync();
            }
            
            return roleEntity;
        }

        public async Task<IDictionary<string, string>> GetCompaniesAsync(string emailAddress)
        {
            if (emailAddress == null) throw new ArgumentNullException("emailAddress");

            return await Set<StafferEntity>().Where(e => e.EmailAddress == emailAddress)
                .Select(e => new { e.CompanyCode, e.Company.CompanyName })
                .ToDictionaryAsync(e => e.CompanyCode, e => e.CompanyName);
        }

        public async Task<IList<string>> GetIdentitiesAsync(string companyCode, string startsWith)
        {
            if (companyCode == null) throw new ArgumentNullException("companyCode");
            
            IQueryable<StafferEntity> source = Set<StafferEntity>().OrderBy(e => e.EmailAddress);

            if (!string.IsNullOrEmpty(companyCode)) source = source.Where(e => e.CompanyCode == companyCode);

            if (!string.IsNullOrEmpty(startsWith)) source = source.Where(e => e.EmailAddress.StartsWith(startsWith));

            return await source.Select(e => e.EmailAddress).Distinct().ToListAsync();
        }

        public Task<StafferEntity> GetStafferAsync(int stafferId)
        {
            return Set<StafferEntity>().SingleOrDefaultAsync(e => e.StafferId == stafferId);
        }

        public Task<StafferEntity> GetStafferAsync(string companyCode, string identity)
        {
            if (companyCode == null) throw new ArgumentNullException("companyCode");

            if (identity == null) throw new ArgumentNullException("identity");
            
            return Set<StafferEntity>().FindAsync(companyCode, identity);
        }

        public Task<StafferRoleEntity> GetStafferRoleAsync(int roleId)
        {
            return Set<StafferRoleEntity>().SingleOrDefaultAsync(e => e.RoleId == roleId);
        }

        public Task<StafferRoleEntity> GetStafferRoleAsync(string companyCode, string identity, string roleName)
        {
            return Set<StafferRoleEntity>().FindAsync(companyCode, identity, roleName);
        }

        public async Task<IList<string>> GetStafferRoleNamesAsync(string companyCode, string identity)
        {
            if (companyCode == null) throw new ArgumentNullException("companyCode");

            if (identity == null) throw new ArgumentNullException("identity");
            
            return await Set<StafferRoleEntity>()
                .Where(e => e.CompanyCode == companyCode && e.EmailAddress == identity)
                .OrderBy(e => e.RoleName)
                .Select(e => e.RoleName)
                .ToListAsync();
        }

        public async Task<IList<string>> GetStafferRoleNamesAsync(string startsWith)
        {
            IQueryable<StafferRoleEntity> source = Set<StafferRoleEntity>().OrderBy(e => e.RoleName);

            if (!string.IsNullOrEmpty(startsWith)) source = source.Where(e => e.RoleName.StartsWith(startsWith));

            return await source.Select(e => e.RoleName).Distinct().ToListAsync();
        }

        public async Task<IDictionary<string, string>> GetStafferRolesAsync(string companyCode)
        {
            return await Set<StafferRoleEntity>()
                .Where(e => e.CompanyCode == companyCode && e.RemovedOn == null)
                .Select(e => new { e.EmailAddress, e.RoleName })
                .GroupBy(e => e.EmailAddress)
                .ToDictionaryAsync(group => group.Key, group => string.Join(" ", group.Select(o => o.RoleName)));
        }

        public async Task<IList<StafferEntity>> GetStaffersAsync(string companyCode, int? start, int? limit, Expression<Func<StafferEntity, string>> orderBy)
        {
            if (companyCode == null) throw new ArgumentNullException("companyCode");

            IQueryable<StafferEntity> source = Set<StafferEntity>().Where(e => e.CompanyCode == companyCode).OrderBy(e => e.EmailAddress);

            if (start.HasValue) source = source.Skip(start.Value);

            if (limit.HasValue) source = source.Take(limit.Value);

            return await source.ToListAsync();
        }

        public async Task SetRolesAsync(StafferEntity staffer, IList<string> roleNames)
        {
            if (staffer == null) throw new ArgumentNullException("staffer");
            
            var dbSet = Set<StafferRoleEntity>();

            var roles = await dbSet.Where(e => e.CompanyCode == staffer.CompanyCode && e.EmailAddress == staffer.EmailAddress).ToListAsync();

            foreach (var role in roles.Where(e => e.RemovedOn == null && !roleNames.Contains(e.RoleName)))
            {
                role.RemovedOn = DateTime.UtcNow;
            }

            foreach (var role in roles.Where(e => e.RemovedOn != null && roleNames.Contains(e.RoleName)))
            {
                role.RemovedOn = null;
            }
            
            dbSet.AddRange(roleNames.Where(name => !roles.Select(e => e.RoleName).Contains(name))
                .Select(name => new StafferRoleEntity
                {
                    CompanyCode = staffer.CompanyCode,
                    EmailAddress = staffer.EmailAddress,
                    RoleName = name
                }));

            await SaveChangesAsync();
        }
    }
}
