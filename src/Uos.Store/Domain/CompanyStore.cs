﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Uos.Domain
{
    public class CompanyStore : EntityStore<CompanyEntity>, ICompanyStore
    {
        public static implicit operator CompanyStore(DbContext dbContext)
        {
            return dbContext != null ? new CompanyStore(dbContext) : null;
        }

        public CompanyStore(DbContext dbContext)
            : base(dbContext)
        {
        }

        public async Task AddCompanyNoticeAsync(string companyCode, string notice)
        {
            var dbSet = Set<CompanyNoticeEntity>();

            var entity = dbSet.Create();

            entity.CompanyCode = companyCode;
            entity.Text = notice;

            dbSet.Add(entity);

            await SaveChangesAsync();
        }

        public async Task<CompanyEntity> CreateCompanyAsync(CompanyEntity companyEntity)
        {
            companyEntity = Set<CompanyEntity>().Add(companyEntity);

            await SaveChangesAsync();
            
            return companyEntity;
        }

        public async Task<CompanyEntity> DeleteCompanyAsync(string companyCode)
        {
            var dbSet = Set<CompanyEntity>();

            var entity = await dbSet.FindAsync(companyCode);

            if (entity != null)
            {
                dbSet.Remove(entity);

                await SaveChangesAsync();
            }

            return entity;
        }

        public async Task<IList<CompanyEntity>> GetCompaniesAsync(int? start, int? limit, Expression<Func<CompanyEntity, string>> orderBy)
        {
            IQueryable<CompanyEntity> source = Set<CompanyEntity>().OrderBy(e => e.CompanyCode);

            if (start.HasValue) source = source.Skip(start.Value);

            if (limit.HasValue) source = source.Take(limit.Value);

            return await source.ToListAsync();
        }

        public async Task<IDictionary<string, string>> GetCompaniesCodeNameAsync(string startsWith)
        {
            IQueryable<CompanyEntity> source = Set<CompanyEntity>();

            if (!string.IsNullOrEmpty(startsWith)) source = source.Where(e => e.CompanyCode.StartsWith(startsWith) || e.CompanyName.StartsWith(startsWith));
            
            var list = await source.Select(e => new { e.CompanyCode, e.CompanyName }).ToListAsync();

            return list.ToDictionary(o => o.CompanyCode, o => o.CompanyName);
        }

        public Task<CompanyEntity> GetCompanyAsync(string companyCode)
        {
            return Set<CompanyEntity>().FindAsync(companyCode);
        }

        public IList<string> GetCompanyFeatures(string companyCode)
        {
            return Set<CompanyFeatureEntity>().Where(e => e.CompanyCode == companyCode).Select(e => e.FeatureName).ToList();
        }

        public async Task<IList<string>> GetCompanyFeaturesAsync(string companyCode)
        {
            return await Set<CompanyFeatureEntity>().Where(e => e.CompanyCode == companyCode && e.RemovedOn == null).Select(e => e.FeatureName).ToListAsync();
        }
        
        public async Task SetFeaturesAsync(CompanyEntity company, IList<string> featureNames)
        {
            var dbSet = Set<CompanyFeatureEntity>();

            var features = await dbSet.Where(e => e.CompanyCode == company.CompanyCode).ToListAsync();
            
            foreach(var feature in features.Where(e => e.RemovedOn == null && !featureNames.Contains(e.FeatureName)))
            {
                feature.RemovedOn = DateTime.UtcNow;
            }

            foreach (var feature in features.Where(e => e.RemovedOn != null && featureNames.Contains(e.FeatureName)))
            {
                feature.RemovedOn = null;
            }

            dbSet.AddRange(featureNames.Where(name => !features.Select(e => e.FeatureName).Contains(name))
                .Select(name => new CompanyFeatureEntity
                {
                    CompanyCode = company.CompanyCode,
                    FeatureName = name
                }));

            await SaveChangesAsync();
        }
    }
}
