﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Domain
{
    internal static class ErrorConfiguration
    {
        public static void Error(this DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ErrorEntity>()
                .ToTable("errors")
                .HasKey(e => e.ErrorId)
                .Property<ErrorEntity>(e => e.CreatedOn, (property) =>
                {
                    property.HasColumnName("created_on");
                    property.IsRequired();
                })
                .Property<ErrorEntity>(e => e.ErrorId, (property) =>
                {
                    property.HasColumnName("id");
                    property.IsRequired();
                })
                .Property<ErrorEntity>(e => e.UserEmailAddress, (property) =>
                {
                    property.HasColumnName("user");
                    property.HasMaxLength(Lengths.EmailAddress);
                    property.IsRequired();
                });

            modelBuilder.Entity<ErrorLocationEntity>()
                .ToTable("error_locations")
                .HasKey(e => new { e.ErrorId, e.LocationIndex})
                .HasRequired(e => e.Error, (navigationProperty) =>
                {
                    navigationProperty.WithMany(e => e.ErrorLocations).HasForeignKey(e => e.ErrorId);
                })
                .Property<ErrorLocationEntity>(e => e.ClassName, (property) =>
                {
                    property.HasColumnName("class");
                    property.HasMaxLength(Lengths.RuntimeMemberName);
                    property.IsRequired();
                })
                .Property<ErrorLocationEntity>(e => e.ErrorId, (property) =>
                {
                    property.HasColumnName("id");
                    property.IsRequired();
                })
                .Property<ErrorLocationEntity>(e => e.LocationIndex, (property) =>
                {
                    property.HasColumnName("index");
                    property.IsRequired();
                })
                .Property<ErrorLocationEntity>(e => e.MethodName, (property) =>
                {
                    property.HasColumnName("method");
                    property.HasMaxLength(Lengths.RuntimeMemberName);
                    property.IsRequired();
                    property.IsUnicode(false);
                });

            modelBuilder.Entity<ErrorMessageEntity>()
                .ToTable("error_messages")
                .HasKey(e => new { e.ErrorId, e.MessageIndex })
                .HasRequired(e => e.Error, (navigationProperty) =>
                {
                    navigationProperty.WithMany(e => e.Messages).HasForeignKey(e => e.ErrorId);
                })
                .Property<ErrorMessageEntity>(e => e.ErrorId, (property) =>
                {
                    property.HasColumnName("id");
                    property.IsRequired();
                })
                .Property<ErrorMessageEntity>(e => e.MessageIndex, (property) =>
                {
                    property.HasColumnName("index");
                    property.IsRequired();
                })
                .Property<ErrorMessageEntity>(e => e.RuntimeType, (property) =>
                {
                    property.HasColumnName("type");
                    property.HasMaxLength(Lengths.RuntimeMemberName);
                    property.IsOptional();
                })
                .Property<ErrorMessageEntity>(e => e.Text, (property) =>
                {
                    property.HasColumnName("text");
                    property.HasMaxLength(Lengths.MessageText);
                    property.IsOptional();
                    property.IsUnicode(false);
                });
        }
    }
}
