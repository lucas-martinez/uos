﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Domain
{
    internal static class UserDefinedConfiguration
    {
        public static void UserDefined(this DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserDefinedPropertyEntity>()
                .ToTable("user_properties")
                .HasKey(e => new { e.CompanyId, e.DocumentType, e.PropertyIndex })
                .Property<UserDefinedPropertyEntity>(e => e.CompanyId, (property) =>
                {
                    property.HasColumnName("company");
                    property.IsRequired();
                })
                .Property<UserDefinedPropertyEntity, DocumentTypes>(e => e.DocumentType, (property) =>
                {
                    property.HasColumnName("document");
                    property.IsRequired();
                })
                .Property<UserDefinedPropertyEntity, bool>(e => e.Lookup, (property) =>
                {
                    property.HasColumnName("lookup");
                    property.IsRequired();
                })
                .Property<UserDefinedPropertyEntity>(e => e.PropertyIndex, (property) =>
                {
                    property.HasColumnName("index");
                    property.IsRequired();
                })
                .Property<UserDefinedPropertyEntity>(e => e.PropertyName, (property) =>
                {
                    property.HasColumnName("name");
                    property.IsRequired();
                })
                .Property<UserDefinedPropertyEntity>(e => e.PropertySyntax, (property) =>
                {
                    property.HasColumnName("syntax");
                    property.IsOptional();
                });

            modelBuilder.Entity<UserDefinedFieldEntity>()
                .ToTable("user_fields")
                .HasKey(e => new { e.CompanyId, e.DocumentType, e.DocumentId, e.PropertyIndex })
                .HasRequired(e => e.Property, (navigationProperty) =>
                {
                    navigationProperty.WithMany().HasForeignKey(e => new { e.CompanyId, e.DocumentType, e.PropertyIndex });
                })
                .Property<UserDefinedFieldEntity>(e => e.CompanyId, (property) =>
                {
                    property.HasColumnName("company");
                    property.IsRequired();
                })
                .Property<UserDefinedFieldEntity>(e => e.DocumentId, (property) =>
                {
                    property.HasColumnName("id");
                    property.IsRequired();
                })
                .Property<UserDefinedFieldEntity, DocumentTypes>(e => e.DocumentType, (property) =>
                {
                    property.HasColumnName("document");
                    property.IsRequired();
                })
                .Property<UserDefinedFieldEntity>(e => e.DocumentValue, (property) =>
                {
                    property.HasColumnName("value");
                    property.HasMaxLength(100);
                    property.IsUnicode(false);
                })
                .Property<UserDefinedFieldEntity>(e => e.PropertyIndex, (property) =>
                {
                    property.HasColumnName("index");
                    property.IsRequired();
                });
        }
    }
}
