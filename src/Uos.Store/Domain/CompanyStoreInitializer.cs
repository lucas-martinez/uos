﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Domain
{
    public class CompanyStoreInitializer
    {
        static readonly Dictionary<string, string> IndustryMapping = new Dictionary<string, string>
        {
            { "Farming/Seeds/Milling", Uos.Domain.Industries.Agriculture },
            { "n/a", null },
            { "Industrial Machinery/Components", Uos.Domain.Industries.Manufacturing },
            { "Real Estate Investment Trusts", Uos.Domain.Industries.RealEstate },
            { "Major Pharmaceuticals", Uos.Domain.Industries.Pharmaceutical },
            { "Oil Refining/Marketing", Uos.Domain.Industries.Utilities },
            { "Hospital/Nursing Management", Uos.Domain.Industries.Health },
            { "Diversified Commercial Services", null },
            { "Radio And Television Broadcasting And Communications Equipment", Uos.Domain.Industries.Media },
            { "Military/Government/Technical", null },
            { "Metal Fabrications", Uos.Domain.Industries.Manufacturing },
            { "Precious Metals", Uos.Domain.Industries.Manufacturing },
            { "Industrial Specialties", Uos.Domain.Industries.Manufacturing },
            { "Real Estate", Uos.Domain.Industries.RealEstate },
            { "Food Distributors", Uos.Domain.Industries.Food },
            { "Power Generation", Uos.Domain.Industries.Utilities },
            { "Packaged Foods", Uos.Domain.Industries.Food },
            { "Medical Specialities", Uos.Domain.Industries.Health },
            { "Biotechnology: Biological Products (No Diagnostic Substances)", Uos.Domain.Industries.Biotechnology },
            { "Biotechnology: Electromedical & Electrotherapeutic Apparatus", Uos.Domain.Industries.Medical },
            { "Mining & Quarrying of Nonmetallic Minerals (No Fuels)", Uos.Domain.Industries.Manufacturing },
            { "Professional Services", Uos.Domain.Industries.Service },
            { "Environmental Services", Uos.Domain.Industries.Service },
            { "Major Banks", Uos.Domain.Industries.Banking },
            { "Oil & Gas Production", Uos.Domain.Industries.Utilities },
            { "Consumer Specialties", Uos.Domain.Industries.Consumer },
            { "Medical/Dental Instruments", Uos.Domain.Industries.Medical },
            { "Beverages (Production/Distribution)", Uos.Domain.Industries.Food },
            { "Package Goods/Cosmetics", Uos.Domain.Industries.Beauty },
            { "Building Products", Uos.Domain.Industries.Construction },
            { "Oil/Gas Transmission", Uos.Domain.Industries.Utilities },
            { "Building Materials", Uos.Domain.Industries.Construction },
            { "Plastic Products", Uos.Domain.Industries.Chemical },
            { "Apparel", Uos.Domain.Industries.Apparel },
            { "Containers/Packaging", Uos.Domain.Industries.Transportation },
            { "EDP Services", Uos.Domain.Industries.Service },
            { "Electric Utilities: Central", Uos.Domain.Industries.Utilities },
            { "Semiconductors", Uos.Domain.Industries.Manufacturing },
            { "Consumer Electronics/Appliances", Uos.Domain.Industries.Consumer },
            { "Oilfield Services/Equipment", Uos.Domain.Industries.Utilities },
            { "Other Consumer Services", Uos.Domain.Industries.Service },
            { "Restaurants", Uos.Domain.Industries.Food },
            { "Major Chemicals", Uos.Domain.Industries.Chemical },
            { "Steel/Iron Ore", Uos.Domain.Industries.Manufacturing },
            { "Computer Software: Prepackaged Software", Uos.Domain.Industries.InformationTechnology },
            { "Telecommunications Equipment", Uos.Domain.Industries.Communications },
            { "Water Supply", Uos.Domain.Industries.Utilities },
            { "Fluid Controls", Uos.Domain.Industries.Utilities },
            { "Advertising", Uos.Domain.Industries.Advertising },
            { "Electrical Products", Uos.Domain.Industries.Manufacturing },
            { "Integrated oil Companies", Uos.Domain.Industries.Utilities },
            { "Electronic Components", Uos.Domain.Industries.Manufacturing },
            { "Investment Bankers/Brokers/Service", Uos.Domain.Industries.Banking },
            { "Publishing", Uos.Domain.Industries.Publishing },
            { "Railroads", Uos.Domain.Industries.Utilities },
            { "Recreational Products/Toys", Uos.Domain.Industries.Manufacturing },
            { "Finance: Consumer Services", Uos.Domain.Industries.FinancialServices },
            { "Multi-Sector Companies", null },
            { "Biotechnology: Commercial Physical & Biological Resarch", Uos.Domain.Industries.Biotechnology },
            { "Biotechnology: In Vitro & In Vivo Diagnostic Substances", Uos.Domain.Industries.Biotechnology },
            { "Services-Misc. Amusement & Recreation", Uos.Domain.Industries.Service },
            { "Building operators", Uos.Domain.Industries.Construction },
            { "Broadcasting", Uos.Domain.Industries.Media },
            { "Paper", Uos.Domain.Industries.News },
            { "Aerospace", Uos.Domain.Industries.Aerospace },
            { "Homebuilding", Uos.Domain.Industries.Construction },
            { "Business Services", Uos.Domain.Industries.Service },
            { "Construction/Ag Equipment/Trucks", Uos.Domain.Industries.Construction },
            { "Specialty Foods", Uos.Domain.Industries.Food },
            { "Clothing/Shoe/Accessory Stores", Uos.Domain.Industries.Consumer },
            { "Auto Parts:O.E.M.", Uos.Domain.Industries.Auto }
        };

        public static void LoadCsv(DbContext dbContext, params string[] fileNames)
        {
            var dbSet = dbContext.Set<CompanyEntity>();

            var models = new List<CompanyEntity>();

            foreach (var name in fileNames)
            {
                using (var reader = File.OpenText(name))
                {
                    var header = reader.ReadLine();

                    var line = header.StartsWith("\"Symbol\"") ? reader.ReadLine() : header;

                    for (; line != null; line = reader.ReadLine())
                    {
                        var cells = line.Split(',');

                        var industry = cells[6].Trim(new[] { '"', ' ' });

                        if (!IndustryMapping.TryGetValue(industry, out industry))
                        {
                            industry = null;
                        }

                        var model = new CompanyEntity
                        {
                            CompanyCode = cells[0].Trim(new[] { '"', ' ' }),
                            CompanyName = cells[1].Trim(new[] { '"', ' ' }),
                            Industry = industry,
                            Url = cells[7].Trim(new[] { '"', ' ' }),
                        };

                        if (model.CompanyCode.Length > Lengths.CompanyCode)
                        {
                            continue;
                        }

                        models.Add(model);
                    }
                }
            }

            var groups = models.GroupBy(e => e.CompanyCode);

            var keys = groups.Select(g => g.Key).ToList();

            var entities = dbSet.Where(e => keys.Contains(e.CompanyCode));

            foreach (var entity in entities)
            {
                var group = groups.Single(g => g.Key == entity.CompanyCode);

                var model = group.First();

                if (model.Industry != entity.Industry)
                {
                    entity.Industry = model.Industry;
                }
            }

            keys = entities.Select(e => e.CompanyCode).ToList();

            foreach (var group in groups)
            {
                if (!keys.Contains(group.Key))
                {
                    dbSet.Add(group.First());
                }
            }
        }
    }
}
