﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Uos.Distribution
{   
    public class CustomerStore : EntityStore<CustomerEntity>, ICustomerStore
    {
        public static implicit operator CustomerStore(DbContext dbContext)
        {
            return dbContext != null ? new CustomerStore(dbContext) : null;
        }

        public CustomerStore(DbContext dbContext)
            : base(dbContext)
        {
        }

        public async Task<CustomerEntity> CreateCustomerAsync(CustomerEntity entity)
        {
            entity = Set<CustomerEntity>().Add(entity);

            await SaveChangesAsync();

            return entity;
        }

        public async Task<CustomerEntity> DeleteCustomerAsync(int id)
        {
            var dbSet = Set<CustomerEntity>();

            var entity = await dbSet.FindAsync(id);

            if (entity != null)
            {
                dbSet.Remove(entity);

                await SaveChangesAsync();
            }

            return entity;
        }

        public async Task<IList<CustomerEntity>> GetCustomersAsync(string companyCode, int? start, int? limit, Expression<Func<CustomerEntity, string>> orderBy)
        {
            IQueryable<CustomerEntity> source = Set<CustomerEntity>();

            source = orderBy != null ? source.OrderBy(orderBy) : source.OrderBy(e => e.CustomerName);

            if (!string.IsNullOrEmpty(companyCode)) source = source.Where(e => e.CompanyCode == companyCode);

            if (start.HasValue) source = source.Skip(start.Value);

            if (limit.HasValue) source = source.Take(limit.Value);

            return await source.ToListAsync();
        }

        public async Task<IDictionary<int, string>> GetCustomersAsync(string companyCode, string startsWith)
        {
            IQueryable<CustomerEntity> source = Set<CustomerEntity>();

            if (!string.IsNullOrEmpty(companyCode)) source = source.Where(e => e.CompanyCode == companyCode);

            if (!string.IsNullOrEmpty(startsWith)) source = source.Where(e => e.CustomerName.StartsWith(startsWith));

            var list = await source.Select(e => new { e.CustomerId, e.CustomerName }).ToListAsync();

            return list.ToDictionary(o => o.CustomerId, o => o.CustomerName);
        }

        public Task<CustomerEntity> GetCustomerAsync(int id)
        {
            return Set<CustomerEntity>().FindAsync(id);
        }
    }
}
