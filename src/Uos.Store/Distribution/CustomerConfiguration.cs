﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Distribution
{
    public static class CustomerConfiguration
    {
        public static void Customer(this DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CustomerEntity>()
                .ToTable("sd.customers")
                .HasKey(e => e.CustomerId)
                .HasRequired(e => e.Company, (navigationProperty) =>
                {
                    navigationProperty.WithMany().HasForeignKey(e => e.CompanyCode);
                })
                .Property<CustomerEntity>(e => e.CompanyCode, (property) =>
                {
                    property.HasColumnName("company");
                    property.HasMaxLength(Domain.Lengths.CompanyCode);
                    property.IsRequired();
                })
                .Property<CustomerEntity, DateTime>(e => e.CreatedOn, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_CustomerCreated", 4)));
                    property.HasColumnName("created_utc");
                    property.IsRequired();
                })
                .Property<CustomerEntity, int>(e => e.CustomerId, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_CustromerId", 1)));
                    property.HasColumnName("id");
                    property.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
                    property.IsRequired();
                })
                .Property<CustomerEntity>(e => e.CustomerName, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_CustromerName", 3)));
                    property.HasColumnName("name");
                    property.HasMaxLength(Lengths.CustomerName);
                    property.IsOptional();
                    property.IsUnicode(false);
                })
                .Property<CustomerEntity>(e => e.CustomerNumber, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_CustromerNumber", 2)));
                    property.HasColumnName("number");
                    property.HasMaxLength(Lengths.CustomerNumber);
                    property.IsOptional();
                    property.IsUnicode(false);
                })
                .Property<CustomerEntity, DateTime>(e => e.RemovedOn, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_CustomerRemoved", 5)));
                    property.HasColumnName("removed_utc");
                    property.IsOptional();
                });
        }
    }
}
