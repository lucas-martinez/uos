﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Transportation
{
    internal static class TransportationConfiguration
    {
        public static void Transportation(this DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FreightEntity>()
                .ToTable("tr.freights")
                .HasKey(e => e.FreightId);
                /*.HasRequired(e => e.Company, (navigationProperty) =>
                {
                    navigationProperty.WithMany().HasForeignKey(e => e.CompanyCode);
                })
                .HasOptional(e => e.Customer, (navigationProperty) =>
                {
                    navigationProperty.WithMany().HasForeignKey(e => e.CustomerId);
                })*/
                /*.Property<LocationEntity>(e => e.CompanyCode, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_LocationCompany", 4)));
                    property.HasColumnName("company");
                    property.HasMaxLength(Domain.Lengths.CompanyCode);
                    property.IsRequired();
                })
                .Property<LocationEntity, DateTime>(e => e.CreatedOn, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_LocationCreated", 6)));
                    property.HasColumnName("created_utc");
                    property.IsRequired();
                })
                .Property<LocationEntity, int>(e => e.CustomerId, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_LocationCustomer", 5)));
                    property.HasColumnName("customer");
                    property.IsOptional();
                })
                .Property<LocationEntity>(e => e.LocationCode, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_LocationCode", 2)));
                    property.HasColumnName("code");
                    property.HasMaxLength(Lengths.LocationCode);
                    property.IsUnicode(false);
                })
                .Property<LocationEntity, int>(e => e.LocationId, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_LocationId", 1) { IsUnique = true }));
                    property.HasColumnName("id");
                    property.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
                    property.IsRequired();
                })
                .Property<LocationEntity>(e => e.LocationName, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_LocationName", 3)));
                    property.HasColumnName("name");
                    property.HasMaxLength(Lengths.LocationName);
                    property.IsUnicode(false);
                })
                .Property<LocationEntity, DateTime>(e => e.RemovedOn, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_LocationRemoved", 7)));
                    property.HasColumnName("removed_utc");
                    property.IsOptional();
                }); */
        }
    }
}
