﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Uos
{
    public static class PropertyConfigurationHelper
    {
        //
        // Summary:
        //     Configures an optional relationship from this entity type. Instances of the entity
        //     type will be able to be saved to the database without this relationship being
        //     specified. The foreign key in the database will be nullable.
        //
        // Parameters:
        //   navigationPropertyExpression:
        //     A lambda expression representing the navigation property for the relationship.
        //     C#: t => t.MyProperty VB.Net: Function(t) t.MyProperty
        //
        // Type parameters:
        //   TTargetEntity:
        //     The type of the entity at the other end of the relationship.
        //
        // Returns:
        //     A configuration object that can be used to further configure the relationship.
        public static EntityTypeConfiguration<TEntityType> HasOptional<TEntityType, TTargetEntity>(this EntityTypeConfiguration<TEntityType> type, Expression<Func<TEntityType, TTargetEntity>> navigationPropertyExpression, Action<OptionalNavigationPropertyConfiguration<TEntityType, TTargetEntity>> map)
            where TEntityType : class
            where TTargetEntity : class
        {
            var navigationProperty = type.HasOptional(navigationPropertyExpression);

            if (map != null) map(navigationProperty);

            return type;
        }

        //
        // Summary:
        //     Configures a required relationship from this entity type. Instances of the entity
        //     type will not be able to be saved to the database unless this relationship is
        //     specified. The foreign key in the database will be non-nullable.
        //
        // Parameters:
        //   navigationPropertyExpression:
        //     A lambda expression representing the navigation property for the relationship.
        //     C#: t => t.MyProperty VB.Net: Function(t) t.MyProperty
        //
        // Type parameters:
        //   TTargetEntity:
        //     The type of the entity at the other end of the relationship.
        //
        // Returns:
        //     A configuration object that can be used to further configure the relationship.
        public static EntityTypeConfiguration<TEntityType> HasRequired<TEntityType, TTargetEntity>(this EntityTypeConfiguration<TEntityType> type, Expression<Func<TEntityType, TTargetEntity>> navigationPropertyExpression, Action<RequiredNavigationPropertyConfiguration<TEntityType, TTargetEntity>> map)
            where TEntityType : class
            where TTargetEntity : class
        {
            var navigationProperty = type.HasRequired(navigationPropertyExpression);

            if (map != null) map(navigationProperty);

            return type;
        }

        //
        // Summary:
        //     Configures a System.struct? property that is defined on this type.
        //
        // Parameters:
        //   propertyExpression:
        //     A lambda expression representing the property to be configured. C#: t => t.MyProperty
        //     VB.Net: Function(t) t.MyProperty
        //
        // Type parameters:
        //   T:
        //     The type of the property being configured.
        //
        // Returns:
        //     A configuration object that can be used to configure the type.
        public static StructuralTypeConfiguration<TStructuralType> Property<TStructuralType, T>(this StructuralTypeConfiguration<TStructuralType> type, Expression<Func<TStructuralType, T?>> propertyExpression, Action<PrimitivePropertyConfiguration> map)
            where T : struct
            where TStructuralType : class
        {
            var property = type.Property(propertyExpression);

            if (map != null) map(property);

            return type;
        }

        //
        // Summary:
        //     Configures a System.struct property that is defined on this type.
        //
        // Parameters:
        //   propertyExpression:
        //     A lambda expression representing the property to be configured. C#: t => t.MyProperty
        //     VB.Net: Function(t) t.MyProperty
        //
        // Type parameters:
        //   T:
        //     The type of the property being configured.
        //
        // Returns:
        //     A configuration object that can be used to configure the type.
        public static StructuralTypeConfiguration<TStructuralType> Property<TStructuralType, T>(this StructuralTypeConfiguration<TStructuralType> type, Expression<Func<TStructuralType, T>> propertyExpression, Action<PrimitivePropertyConfiguration> map)
            where T : struct
            where TStructuralType : class
        {
            var property = type.Property(propertyExpression);

            if (map != null) map(property);

            return type;
        }

        //
        // Summary:
        //     Configures a DbGeography property that is defined on this type.
        //
        // Parameters:
        //   propertyExpression:
        //     A lambda expression representing the property to be configured. C#: t => t.MyProperty
        //     VB.Net: Function(t) t.MyProperty
        //
        // Returns:
        //     A configuration object that can be used to configure the type.
        public static StructuralTypeConfiguration<TStructuralType> Property<TStructuralType>(this StructuralTypeConfiguration<TStructuralType> type, Expression<Func<TStructuralType, DbGeography>> propertyExpression, Action<PrimitivePropertyConfiguration> map)
            where TStructuralType : class
        {
            var property = type.Property(propertyExpression);

            if (map != null) map(property);

            return type;
        }

        //
        // Summary:
        //     Configures a System.string property that is defined on this type.
        //
        // Parameters:
        //   propertyExpression:
        //     A lambda expression representing the property to be configured. C#: t => t.MyProperty
        //     VB.Net: Function(t) t.MyProperty
        //
        // Returns:
        //     A configuration object that can be used to configure the type.
        public static StructuralTypeConfiguration<TStructuralType> Property<TStructuralType>(this StructuralTypeConfiguration<TStructuralType> type, Expression<Func<TStructuralType, string>> propertyExpression, Action<StringPropertyConfiguration> map)
            where TStructuralType : class
        {
            var property = type.Property(propertyExpression);

            if (map != null) map(property);

            return type;
        }

        //
        // Summary:
        //     Configures a System.decimal property that is defined on this type.
        //
        // Parameters:
        //   propertyExpression:
        //     A lambda expression representing the property to be configured. C#: t => t.MyProperty
        //     VB.Net: Function(t) t.MyProperty
        //
        // Returns:
        //     A configuration object that can be used to configure the type.
        public static StructuralTypeConfiguration<TStructuralType> Property<TStructuralType>(this StructuralTypeConfiguration<TStructuralType> type, Expression<Func<TStructuralType, decimal>> propertyExpression, Action<DecimalPropertyConfiguration> map)
            where TStructuralType : class
        {
            var property = type.Property(propertyExpression);

            if (map != null) map(property);

            return type;
        }

        //
        // Summary:
        //     Configures a System.DateTime property that is defined on this type.
        //
        // Parameters:
        //   propertyExpression:
        //     A lambda expression representing the property to be configured. C#: t => t.MyProperty
        //     VB.Net: Function(t) t.MyProperty
        //
        // Returns:
        //     A configuration object that can be used to configure the type.
        public static StructuralTypeConfiguration<TStructuralType> Property<TStructuralType>(this StructuralTypeConfiguration<TStructuralType> type, Expression<Func<TStructuralType, DateTime>> propertyExpression, Action<DateTimePropertyConfiguration> map)
            where TStructuralType : class
        {
            var property = type.Property(propertyExpression);

            if (map != null) map(property);

            return type;
        }

        //
        // Summary:
        //     Configures a System.DateTimeOffset property that is defined on this type.
        //
        // Parameters:
        //   propertyExpression:
        //     A lambda expression representing the property to be configured. C#: t => t.MyProperty
        //     VB.Net: Function(t) t.MyProperty
        //
        // Returns:
        //     A configuration object that can be used to configure the type.
        public static StructuralTypeConfiguration<TStructuralType> Property<TStructuralType>(this StructuralTypeConfiguration<TStructuralType> type, Expression<Func<TStructuralType, DateTimeOffset>> propertyExpression, Action<DateTimePropertyConfiguration> map)
            where TStructuralType : class
        {
            var property = type.Property(propertyExpression);

            if (map != null) map(property);

            return type;
        }

        //
        // Summary:
        //     Configures a System.TimeSpan property that is defined on this type.
        //
        // Parameters:
        //   propertyExpression:
        //     A lambda expression representing the property to be configured. C#: t => t.MyProperty
        //     VB.Net: Function(t) t.MyProperty
        //
        // Returns:
        //     A configuration object that can be used to configure the type.
        public static StructuralTypeConfiguration<TStructuralType> Property<TStructuralType>(this StructuralTypeConfiguration<TStructuralType> type, Expression<Func<TStructuralType, TimeSpan>> propertyExpression, Action<DateTimePropertyConfiguration> map)
            where TStructuralType : class
        {
            var property = type.Property(propertyExpression);

            if (map != null) map(property);

            return type;
        }

        //
        // Summary:
        //     Configures a System.TimeSpan? property that is defined on this type.
        //
        // Parameters:
        //   propertyExpression:
        //     A lambda expression representing the property to be configured. C#: t => t.MyProperty
        //     VB.Net: Function(t) t.MyProperty
        //
        // Returns:
        //     A configuration object that can be used to configure the type.
        public static StructuralTypeConfiguration<TStructuralType> Property<TStructuralType>(this StructuralTypeConfiguration<TStructuralType> type, Expression<Func<TStructuralType, TimeSpan?>> propertyExpression, Action<DateTimePropertyConfiguration> map)
            where TStructuralType : class
        {
            var property = type.Property(propertyExpression);

            if (map != null) map(property);

            return type;
        }

        //
        // Summary:
        //     Configures a System.DateTimeOffset? property that is defined on this type.
        //
        // Parameters:
        //   propertyExpression:
        //     A lambda expression representing the property to be configured. C#: t => t.MyProperty
        //     VB.Net: Function(t) t.MyProperty
        //
        // Returns:
        //     A configuration object that can be used to configure the type.
        public static StructuralTypeConfiguration<TStructuralType> Property<TStructuralType>(this StructuralTypeConfiguration<TStructuralType> type, Expression<Func<TStructuralType, DateTimeOffset?>> propertyExpression, Action<DateTimePropertyConfiguration> map)
            where TStructuralType : class
        {
            var property = type.Property(propertyExpression);

            if (map != null) map(property);

            return type;
        }

        //
        // Summary:
        //     Configures a System.DateTime? property that is defined on this type.
        //
        // Parameters:
        //   propertyExpression:
        //     A lambda expression representing the property to be configured. C#: t => t.MyProperty
        //     VB.Net: Function(t) t.MyProperty
        //
        // Returns:
        //     A configuration object that can be used to configure the type.
        public static StructuralTypeConfiguration<TStructuralType> Property<TStructuralType>(this StructuralTypeConfiguration<TStructuralType> type, Expression<Func<TStructuralType, DateTime?>> propertyExpression, Action<DateTimePropertyConfiguration> map)
            where TStructuralType : class
        {
            var property = type.Property(propertyExpression);

            if (map != null) map(property);

            return type;
        }

        //
        // Summary:
        //     Configures a System.decimal? property that is defined on this type.
        //
        // Parameters:
        //   propertyExpression:
        //     A lambda expression representing the property to be configured. C#: t => t.MyProperty
        //     VB.Net: Function(t) t.MyProperty
        //
        // Returns:
        //     A configuration object that can be used to configure the type.
        public static StructuralTypeConfiguration<TStructuralType> Property<TStructuralType>(this StructuralTypeConfiguration<TStructuralType> type, Expression<Func<TStructuralType, decimal?>> propertyExpression, Action<DecimalPropertyConfiguration> map)
            where TStructuralType : class
        {
            var property = type.Property(propertyExpression);

            return type;
        }

        //
        // Summary:
        //     Configures a System.byte[] property that is defined on this type.
        //
        // Parameters:
        //   propertyExpression:
        //     A lambda expression representing the property to be configured. C#: t => t.MyProperty
        //     VB.Net: Function(t) t.MyProperty
        //
        // Returns:
        //     A configuration object that can be used to configure the type.
        public static StructuralTypeConfiguration<TStructuralType> Property<TStructuralType>(this StructuralTypeConfiguration<TStructuralType> type, Expression<Func<TStructuralType, byte[]>> propertyExpression, Action<BinaryPropertyConfiguration> map)
            where TStructuralType : class
        {
            var property = type.Property(propertyExpression);

            if (map != null) map(property);

            return type;
        }
        
        //
        // Summary:
        //     Configures a DbGeometry property that is defined on this type.
        //
        // Parameters:
        //   propertyExpression:
        //     A lambda expression representing the property to be configured. C#: t => t.MyProperty
        //     VB.Net: Function(t) t.MyProperty
        //
        // Returns:
        //     A configuration object that can be used to configure the type.
        public static StructuralTypeConfiguration<TStructuralType> Property<TStructuralType>(this StructuralTypeConfiguration<TStructuralType> type, Expression<Func<TStructuralType, DbGeometry>> propertyExpression, Action<PrimitivePropertyConfiguration> map)
            where TStructuralType : class
        {
            var property = type.Property(propertyExpression);

            if (map != null) map(property);

            return type;
        }
    }
}
