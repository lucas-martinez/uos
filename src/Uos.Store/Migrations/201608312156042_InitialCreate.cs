namespace Uos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.companies",
                c => new
                    {
                        code = c.String(nullable: false, maxLength: 7, unicode: false),
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false, maxLength: 127),
                        size = c.String(maxLength: 2, fixedLength: true, unicode: false),
                        type = c.String(maxLength: 3, fixedLength: true, unicode: false),
                        country = c.String(maxLength: 3, fixedLength: true, unicode: false),
                        created_utc = c.DateTime(nullable: false),
                        currency = c.String(maxLength: 3, fixedLength: true, unicode: false),
                        industry = c.String(maxLength: 31, unicode: false),
                        metric = c.String(maxLength: 3, fixedLength: true, unicode: false),
                        removed_utc = c.DateTime(),
                        Url = c.String(),
                    })
                .PrimaryKey(t => t.code)
                .Index(t => t.code, name: "IX_CompanyCode")
                .Index(t => t.id, name: "IX_CompanyId")
                .Index(t => t.name, name: "IX_CompanyName")
                .Index(t => t.created_utc, name: "IX_CompanyCreated")
                .Index(t => t.removed_utc, name: "IX_CompanyRemoved");
            
            CreateTable(
                "dbo.features",
                c => new
                    {
                        company = c.String(nullable: false, maxLength: 7, unicode: false),
                        name = c.String(nullable: false, maxLength: 47, unicode: false),
                        created_utc = c.DateTime(nullable: false),
                        removed_utc = c.DateTime(),
                        id = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => new { t.company, t.name })
                .ForeignKey("dbo.companies", t => t.company, cascadeDelete: true)
                .Index(t => t.company, name: "IX_FeatureCompany")
                .Index(t => t.name, name: "IX_FeatureName")
                .Index(t => t.created_utc, name: "IX_FeatureCreated")
                .Index(t => t.removed_utc, name: "IX_FeatureRemoved")
                .Index(t => t.id, name: "IX_FeatureId");
            
            CreateTable(
                "dbo.company_remarks",
                c => new
                    {
                        company = c.String(nullable: false, maxLength: 7, unicode: false),
                        created_utc = c.DateTime(nullable: false),
                        removed_utc = c.DateTime(),
                        id = c.Int(nullable: false, identity: true),
                        notice = c.String(nullable: false, maxLength: 1023),
                    })
                .PrimaryKey(t => new { t.company, t.created_utc })
                .ForeignKey("dbo.companies", t => t.company, cascadeDelete: true)
                .Index(t => t.company, name: "IX_CompanyNoticeCompany")
                .Index(t => t.created_utc, name: "IX_CompanyNoticeCreated")
                .Index(t => t.removed_utc, name: "IX_CompanyNoticeRemoved")
                .Index(t => t.id, name: "IX_CompanyNoticeId");
            
            CreateTable(
                "dbo.staffer_roles",
                c => new
                    {
                        company = c.String(nullable: false, maxLength: 7, unicode: false),
                        email = c.String(nullable: false, maxLength: 63, unicode: false),
                        name = c.String(nullable: false, maxLength: 47, unicode: false),
                        created_utc = c.DateTime(nullable: false),
                        id = c.Int(nullable: false, identity: true),
                        removed_utc = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.company, t.email, t.name })
                .ForeignKey("dbo.companies", t => t.company)
                .ForeignKey("dbo.staffers", t => new { t.company, t.email }, cascadeDelete: true)
                .Index(t => t.company, name: "IX_StafferRoleCompany")
                .Index(t => new { t.company, t.email })
                .Index(t => t.email, name: "IX_StafferRoleEmail")
                .Index(t => t.name, name: "IX_StafferRoleName")
                .Index(t => t.created_utc, name: "IX_StafferRoleCreated")
                .Index(t => t.id, unique: true, name: "IX_StafferRoleId")
                .Index(t => t.removed_utc, name: "IX_StafferRoleRemoved");
            
            CreateTable(
                "dbo.staffers",
                c => new
                    {
                        company = c.String(nullable: false, maxLength: 7, unicode: false),
                        email = c.String(nullable: false, maxLength: 63, unicode: false),
                        created_utc = c.DateTime(nullable: false),
                        forename = c.String(maxLength: 127),
                        surname = c.String(maxLength: 127),
                        LoggedOn = c.DateTime(),
                        removed_utc = c.DateTime(),
                        id = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => new { t.company, t.email })
                .ForeignKey("dbo.companies", t => t.company, cascadeDelete: true)
                .Index(t => t.company, name: "IX_StafferCompany")
                .Index(t => t.email, name: "IX_StafferEmail")
                .Index(t => t.created_utc, name: "IX_StafferCreated")
                .Index(t => t.removed_utc, name: "IX_StafferRemoved")
                .Index(t => t.id, unique: true, name: "IX_StafferId");
            
            CreateTable(
                "dbo.staffer_remarks",
                c => new
                    {
                        company = c.String(nullable: false, maxLength: 7, unicode: false),
                        identity = c.String(nullable: false, maxLength: 63, unicode: false),
                        created_utc = c.DateTime(nullable: false),
                        removed_utc = c.DateTime(),
                        id = c.Int(nullable: false, identity: true),
                        notice = c.String(nullable: false, maxLength: 1023),
                    })
                .PrimaryKey(t => new { t.company, t.identity, t.created_utc })
                .ForeignKey("dbo.companies", t => t.company)
                .ForeignKey("dbo.staffers", t => new { t.company, t.identity }, cascadeDelete: true)
                .Index(t => t.company, name: "IX_StafferNoticeCompany")
                .Index(t => new { t.company, t.identity })
                .Index(t => t.identity, name: "IX_StafferNoticeIdentity")
                .Index(t => t.created_utc, name: "IX_StafferNoticeCreated")
                .Index(t => t.removed_utc, name: "IX_StafferNoticeRemoved")
                .Index(t => t.id, name: "IX_StafferNoticeId");
            
            CreateTable(
                "dbo.locations",
                c => new
                    {
                        id = c.Guid(nullable: false),
                        address = c.String(maxLength: 127),
                        altitude = c.Decimal(precision: 18, scale: 2),
                        culture = c.String(maxLength: 15, unicode: false),
                        Country = c.String(),
                        geography = c.Geography(),
                        latitude = c.Decimal(precision: 18, scale: 2),
                        locality = c.String(maxLength: 127),
                        longitude = c.Decimal(precision: 18, scale: 2),
                        postal = c.String(maxLength: 7, unicode: false),
                        region = c.String(maxLength: 127),
                        state = c.String(maxLength: 2, unicode: false),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.staffer_roles", new[] { "company", "email" }, "dbo.staffers");
            DropForeignKey("dbo.staffer_remarks", new[] { "company", "identity" }, "dbo.staffers");
            DropForeignKey("dbo.staffer_remarks", "company", "dbo.companies");
            DropForeignKey("dbo.staffers", "company", "dbo.companies");
            DropForeignKey("dbo.staffer_roles", "company", "dbo.companies");
            DropForeignKey("dbo.company_remarks", "company", "dbo.companies");
            DropForeignKey("dbo.features", "company", "dbo.companies");
            DropIndex("dbo.staffer_remarks", "IX_StafferNoticeId");
            DropIndex("dbo.staffer_remarks", "IX_StafferNoticeRemoved");
            DropIndex("dbo.staffer_remarks", "IX_StafferNoticeCreated");
            DropIndex("dbo.staffer_remarks", "IX_StafferNoticeIdentity");
            DropIndex("dbo.staffer_remarks", new[] { "company", "identity" });
            DropIndex("dbo.staffer_remarks", "IX_StafferNoticeCompany");
            DropIndex("dbo.staffers", "IX_StafferId");
            DropIndex("dbo.staffers", "IX_StafferRemoved");
            DropIndex("dbo.staffers", "IX_StafferCreated");
            DropIndex("dbo.staffers", "IX_StafferEmail");
            DropIndex("dbo.staffers", "IX_StafferCompany");
            DropIndex("dbo.staffer_roles", "IX_StafferRoleRemoved");
            DropIndex("dbo.staffer_roles", "IX_StafferRoleId");
            DropIndex("dbo.staffer_roles", "IX_StafferRoleCreated");
            DropIndex("dbo.staffer_roles", "IX_StafferRoleName");
            DropIndex("dbo.staffer_roles", "IX_StafferRoleEmail");
            DropIndex("dbo.staffer_roles", new[] { "company", "email" });
            DropIndex("dbo.staffer_roles", "IX_StafferRoleCompany");
            DropIndex("dbo.company_remarks", "IX_CompanyNoticeId");
            DropIndex("dbo.company_remarks", "IX_CompanyNoticeRemoved");
            DropIndex("dbo.company_remarks", "IX_CompanyNoticeCreated");
            DropIndex("dbo.company_remarks", "IX_CompanyNoticeCompany");
            DropIndex("dbo.features", "IX_FeatureId");
            DropIndex("dbo.features", "IX_FeatureRemoved");
            DropIndex("dbo.features", "IX_FeatureCreated");
            DropIndex("dbo.features", "IX_FeatureName");
            DropIndex("dbo.features", "IX_FeatureCompany");
            DropIndex("dbo.companies", "IX_CompanyRemoved");
            DropIndex("dbo.companies", "IX_CompanyCreated");
            DropIndex("dbo.companies", "IX_CompanyName");
            DropIndex("dbo.companies", "IX_CompanyId");
            DropIndex("dbo.companies", "IX_CompanyCode");
            DropTable("dbo.locations");
            DropTable("dbo.staffer_remarks");
            DropTable("dbo.staffers");
            DropTable("dbo.staffer_roles");
            DropTable("dbo.company_remarks");
            DropTable("dbo.features");
            DropTable("dbo.companies");
        }
    }
}
