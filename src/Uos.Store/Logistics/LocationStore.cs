﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Uos.Logistics
{
    public class LocationStore : EntityStore<LocationEntity>, ILocationStore
    {
        public static implicit operator LocationStore(DbContext dbContext)
        {
            return dbContext != null ? new LocationStore(dbContext) : null;
        }

        public LocationStore(DbContext dbContext)
            : base(dbContext)
        {
        }

        public async Task<LocationEntity> CreateLocationAsync(LocationEntity entity)
        {
            entity = Set<LocationEntity>().Add(entity);

            await SaveChangesAsync();

            return entity;
        }

        public async Task<LocationEntity> DeleteLocationAsync(int id)
        {
            var dbSet = Set<LocationEntity>();

            var entity = await dbSet.FindAsync(id);

            if (entity != null)
            {
                dbSet.Remove(entity);

                await SaveChangesAsync();
            }

            return entity;
        }

        public async Task<IList<LocationEntity>> GetLocationsAsync(string companyCode, int? customerId, int? start, int? limit, Expression<Func<LocationEntity, string>> orderBy)
        {
            IQueryable<LocationEntity> source = Set<LocationEntity>().OrderBy(e => e.LocationName);

            if (!string.IsNullOrEmpty(companyCode)) source = source.Where(e => e.CompanyCode == companyCode);

            if (customerId.HasValue) source = source.Where(e => e.CustomerId == customerId.Value);

            if (start.HasValue) source = source.Skip(start.Value);

            if (limit.HasValue) source = source.Take(limit.Value);

            return await source.ToListAsync();
        }

        public Task<LocationEntity> GetLocationAsync(int id)
        {
            return Set<LocationEntity>().FindAsync(id);
        }
    }
}
