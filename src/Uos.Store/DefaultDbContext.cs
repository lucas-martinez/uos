﻿using System.Data.Entity;

namespace Uos
{
    using Distribution;
    using Domain;
    using Inventory;
    using Materials;
    using Personnel;
    using Products;
    using Purchasing;
    using Transportation;

    public partial class DefaultDbContext : DbContext
    {
        public DefaultDbContext()
            : base("DefaultConnection")
        {
        }

        public static DefaultDbContext Create()
        {
            return new DefaultDbContext();
        }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            modelBuilder.Domain();
            modelBuilder.Personnel();
            //modelBuilder.Transportation();
        }
    }
}
