﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Uos
{
    public abstract class EntityStore
    {
        private readonly DbContext _dbContext;

        protected EntityStore(DbContext dbContext)
        {
            _dbContext = dbContext;

            var adapter = (IObjectContextAdapter)dbContext;

            var objectContext = adapter.ObjectContext;

            if (!objectContext.ContextOptions.UseConsistentNullReferenceBehavior)
            {
                objectContext.ContextOptions.UseConsistentNullReferenceBehavior = true;
            }
        }

        protected DbContext DbContext
        {
            get { return _dbContext; }
        }

        public async Task SaveChangesAsync()
        {
            Exception exception = null;

            try
            {
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                exception = ex;

                // TODO: transform complex exceptions into simpler exceptions
            }

            if (exception != null) throw exception;
        }

        public DbSet<T> Set<T>() where T : class, new()
        {
            return _dbContext.Set<T>();
        }
    }

    public abstract class EntityStore<T> : EntityStore, IQueryable<T>
        where T : class, new()
    {
        protected EntityStore(DbContext dbContext)
            : base(dbContext)
        {
        }

        public IQueryable<T> Include<T1>(Expression<Func<T, T1>> path)
        {
            return Set<T>().Include(path);
        }

        public IQueryable<T> Include<T1, T2>(Expression<Func<T, T1>> path1, Expression<Func<T, T2>> path2)
        {
            return Set<T>().Include(path1).Include(path2);
        }

        public IQueryable<T> Include<T1, T2, T3>(Expression<Func<T, T1>> path1, Expression<Func<T, T2>> path2, Expression<Func<T, T3>> path3)
        {
            return Set<T>().Include(path1).Include(path2).Include(path3);
        }

        Type IQueryable.ElementType
        {
            get { return Set<T>().AsQueryable().ElementType; }
        }

        Expression IQueryable.Expression
        {
            get { return Set<T>().AsQueryable().Expression; }
        }

        IQueryProvider IQueryable.Provider
        {
            get { return Set<T>().AsQueryable().Provider; }
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return Set<T>().AsQueryable().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Set<T>().AsQueryable().GetEnumerator();
        }
    }
}
