﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Reflection
{
    using Uos.Implementation;

    public class PropertyPath : IEnumerable<PropertyInfo>, IEnumerable
    {
        private readonly List<PropertyInfo> _components;
        private static readonly PropertyPath _empty = new PropertyPath();

        private PropertyPath()
        {
            _components = new List<PropertyInfo>();
        }

        public PropertyPath(IEnumerable<PropertyInfo> components)
        {
            _components = new List<PropertyInfo>();
            _components.AddRange(components);
        }

        public PropertyPath(PropertyInfo component)
        {
            _components = new List<PropertyInfo>();
            _components.Add(component);
        }

        public bool Equals(PropertyPath other)
        {
            if (object.ReferenceEquals(null, other))
            {
                return false;
            }
            return (object.ReferenceEquals(this, other) || _components.SequenceEqual<PropertyInfo>(other._components, (EqualityComparer<PropertyInfo>)((p1, p2) => p1.IsSameAs(p2))));
        }

        public override bool Equals(object obj)
        {
            if (object.ReferenceEquals(null, obj))
            {
                return false;
            }
            if (object.ReferenceEquals(this, obj))
            {
                return true;
            }
            if (obj.GetType() != typeof(PropertyPath))
            {
                return false;
            }
            return Equals((PropertyPath)obj);
        }

        public override int GetHashCode()
        {
            return _components.Aggregate<PropertyInfo, int>(0, (t, n) => (t ^ ((n.DeclaringType.GetHashCode() * n.Name.GetHashCode()) * 0x18d)));
        }

        public static bool operator ==(PropertyPath left, PropertyPath right)
        {
            return object.Equals(left, right);
        }

        public static bool operator !=(PropertyPath left, PropertyPath right)
        {
            return !object.Equals(left, right);
        }

        IEnumerator<PropertyInfo> IEnumerable<PropertyInfo>.GetEnumerator()
        {
            return _components.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _components.GetEnumerator();
        }

        public override string ToString()
        {
            StringBuilder propertyPathName = new StringBuilder();
            _components.ForEach<PropertyInfo>(delegate(PropertyInfo pi)
            {
                propertyPathName.Append(pi.Name);
                propertyPathName.Append('.');
            });
            return propertyPathName.ToString(0, propertyPathName.Length - 1);
        }

        public int Count
        {
            get { return _components.Count; }
        }

        public static PropertyPath Empty
        {
            get { return _empty; }
        }

        public PropertyInfo this[int index]
        {
            get { return _components[index]; }
        }
    }
}
