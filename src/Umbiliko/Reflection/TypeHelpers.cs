﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Reflection
{
    public static class TypeHelpers
    {
        

        public static System.Reflection.Assembly Assembly(this Type type)
        {
            return type.GetTypeInfo().Assembly;
        }

        public static TypeAttributes Attributes(this Type type)
        {
            return type.GetTypeInfo().Attributes;
        }

        public static Type BaseType(this Type type)
        {
            return type.GetTypeInfo().BaseType;
        }

        public static bool ContainsGenericParameters(this Type type)
        {
            return type.GetTypeInfo().ContainsGenericParameters;
        }

        public static T CreateInstance<T>(this Type type, Func<string, Exception> exceptionFactory = null)
        {
            if (type.GetDeclaredConstructor(new Type[0]) == null)
            {
                throw new InvalidOperationException(string.Format(Strings.CreateInstance_NoParameterlessConstructor, type));
            }
            if (type.IsAbstract())
            {
                throw new InvalidOperationException(string.Format(Strings.CreateInstance_AbstractType, type));
            }
            if (type.IsGenericType())
            {
                throw new InvalidOperationException(string.Format(Strings.CreateInstance_GenericType, type));
            }
            return (T)Activator.CreateInstance(type, true);
        }

        public static T CreateInstance<T>(this Type type, Func<string, string, string> typeMessageFactory, Func<string, Exception> exceptionFactory = null)
        {
            if (!typeof(T).IsAssignableFrom(type))
            {
                new InvalidOperationException(typeMessageFactory(type.ToString(), typeof(T).ToString()));
            }
            return type.CreateInstance<T>(exceptionFactory);
        }

        public static string FullNameWithNesting(this Type type)
        {
            if (!type.IsNested)
            {
                return type.FullName;
            }
            return type.FullName.Replace('+', '.');
        }

        public static PropertyInfo GetAnyProperty(this Type type, string name)
        {
            List<PropertyInfo> source = (from p in type.GetRuntimeProperties()
                                         where p.Name == name
                                         select p).ToList<PropertyInfo>();
            if (source.Count<PropertyInfo>() > 1)
            {
                throw new AmbiguousMatchException();
            }
            return source.SingleOrDefault<PropertyInfo>();
        }

        public static IEnumerable<Type> GetBaseTypes(this Type type)
        {
            type = type.BaseType();
            while (true)
            {
                if (type == null)
                {
                    yield break;
                }
                yield return type;
                type = type.BaseType();
            }
        }

        public static ConstructorInfo GetDeclaredConstructor(this Type type, params Type[] parameterTypes)
        {
            return type.GetDeclaredConstructors().SingleOrDefault<ConstructorInfo>(delegate(ConstructorInfo c)
            {
                if (c.IsStatic)
                {
                    return false;
                }
                return (from p in c.GetParameters() select p.ParameterType).SequenceEqual<Type>(parameterTypes);
            });
        }

        public static ConstructorInfo GetDeclaredConstructor(this Type type, Func<ConstructorInfo, bool> predicate, params Type[][] parameterTypes)
        {
            return (from p in parameterTypes select type.GetDeclaredConstructor(p)).FirstOrDefault<ConstructorInfo>(delegate(ConstructorInfo c)
            {
                if (c != null)
                {
                    return predicate(c);
                }
                return false;
            });
        }

        public static IEnumerable<ConstructorInfo> GetDeclaredConstructors(this Type type)
        {
            return type.GetTypeInfo().DeclaredConstructors;
        }

        public static MethodInfo GetDeclaredMethod(this Type type, string name, params Type[] parameterTypes)
        {
            return type.GetDeclaredMethods(name).SingleOrDefault<MethodInfo>(m => (from p in m.GetParameters() select p.ParameterType).SequenceEqual<Type>(parameterTypes));
        }

        public static IEnumerable<MethodInfo> GetDeclaredMethods(this Type type)
        {
            return type.GetTypeInfo().DeclaredMethods;
        }

        public static IEnumerable<MethodInfo> GetDeclaredMethods(this Type type, string name)
        {
            return type.GetTypeInfo().GetDeclaredMethods(name);
        }

        public static IEnumerable<PropertyInfo> GetDeclaredProperties(this Type type)
        {
            return type.GetTypeInfo().DeclaredProperties;
        }

        public static PropertyInfo GetDeclaredProperty(this Type type, string name)
        {
            return type.GetTypeInfo().GetDeclaredProperty(name);
        }

        public static IEnumerable<Type> GetGenericTypeImplementations(this Type type, Type interfaceOrBaseType)
        {
            Func<Type, bool> predicate = null;
            if (type.IsGenericTypeDefinition())
            {
                return Enumerable.Empty<Type>();
            }
            if (predicate == null)
            {
                predicate = t => t.IsGenericType() && (t.GetGenericTypeDefinition() == interfaceOrBaseType);
            }
            return (interfaceOrBaseType.IsInterface() ? ((IEnumerable<Type>)type.GetInterfaces()) : type.GetBaseTypes()).Union<Type>(new Type[] { type }).Where<Type>(predicate);
        }

        public static IEnumerable<PropertyInfo> GetInstanceProperties(this Type type)
        {
            return (from p in type.GetRuntimeProperties()
                    where !p.IsStatic()
                    select p);
        }

        public static PropertyInfo GetInstanceProperty(this Type type, string name)
        {
            List<PropertyInfo> source = (from p in type.GetRuntimeProperties()
                                         where (p.Name == name) && !p.IsStatic()
                                         select p).ToList<PropertyInfo>();
            if (source.Count<PropertyInfo>() > 1)
            {
                throw new AmbiguousMatchException();
            }
            return source.SingleOrDefault<PropertyInfo>();
        }

        public static IEnumerable<PropertyInfo> GetNonHiddenProperties(this Type type)
        {
            return (from property in type.GetRuntimeProperties()
                    group property by property.Name into propertyGroup
                    select MostDerived(propertyGroup));
        }

        public static IEnumerable<PropertyInfo> GetNonIndexerProperties(this Type type)
        {
            return (from p in type.GetRuntimeProperties()
                    where p.IsPublic() && !p.GetIndexParameters().Any<ParameterInfo>()
                    select p);
        }

        public static MethodInfo GetOnlyDeclaredMethod(this Type type, string name)
        {
            return type.GetDeclaredMethods(name).SingleOrDefault<MethodInfo>();
        }

        public static ConstructorInfo GetPublicConstructor(this Type type, params Type[] parameterTypes)
        {
            ConstructorInfo declaredConstructor = type.GetDeclaredConstructor(parameterTypes);
            if ((declaredConstructor != null) && declaredConstructor.IsPublic)
            {
                return declaredConstructor;
            }
            return null;
        }

        public static MethodInfo GetPublicInstanceMethod(this Type type, string name, params Type[] parameterTypes)
        {
            return type.GetRuntimeMethod(name, m => (m.IsPublic && !m.IsStatic), parameterTypes);
        }

        public static MethodInfo GetRuntimeMethod(this Type type, string name, Func<MethodInfo, bool> predicate, params Type[][] parameterTypes)
        {
            return (from t in parameterTypes select type.GetRuntimeMethod(name, predicate, t)).FirstOrDefault<MethodInfo>(m => (m != null));
        }

        private static MethodInfo GetRuntimeMethod(this Type type, string name, Func<MethodInfo, bool> predicate, Type[] parameterTypes)
        {
            MethodInfo[] methods = type.GetRuntimeMethods().Where<MethodInfo>(delegate(MethodInfo m)
            {
                if (!(name == m.Name) || !predicate(m))
                {
                    return false;
                }
                return (from p in m.GetParameters() select p.ParameterType).SequenceEqual<Type>(parameterTypes);
            }).ToArray<MethodInfo>();
            if (methods.Length == 1)
            {
                return methods[0];
            }
            return methods.SingleOrDefault<MethodInfo>(m => !methods.Any<MethodInfo>(m2 => m2.DeclaringType.IsSubclassOf(m.DeclaringType)));
        }

        public static PropertyInfo GetStaticProperty(this Type type, string name)
        {
            List<PropertyInfo> source = (from p in type.GetRuntimeProperties()
                                         where (p.Name == name) && p.IsStatic()
                                         select p).ToList<PropertyInfo>();
            if (source.Count<PropertyInfo>() > 1)
            {
                throw new AmbiguousMatchException();
            }
            return source.SingleOrDefault<PropertyInfo>();
        }

        public static Type GetTargetType(this Type type)
        {
            Type type2;
            if (!type.IsCollection(out type2))
            {
                type2 = type;
            }
            return type2;
        }

        public static PropertyInfo GetTopProperty(this Type type, string name)
        {
            do
            {
                TypeInfo typeInfo = type.GetTypeInfo();
                PropertyInfo declaredProperty = typeInfo.GetDeclaredProperty(name);
                if ((declaredProperty != null) && !(declaredProperty.GetMethod ?? declaredProperty.SetMethod).IsStatic)
                {
                    return declaredProperty;
                }
                type = typeInfo.BaseType;
            }
            while (type != null);
            return null;
        }

        public static bool IsAbstract(this Type type)
        {
            return type.GetTypeInfo().IsAbstract;
        }

        public static bool IsClass(this Type type)
        {
            return type.GetTypeInfo().IsClass;
        }

        public static bool IsCollection(this Type type)
        {
            return type.IsCollection(out type);
        }

        public static bool IsCollection(this Type type, out Type elementType)
        {
            elementType = type.TryGetElementType(typeof(ICollection<>));
            if ((elementType != null) && !type.IsArray)
            {
                return true;
            }
            elementType = type;
            return false;
        }

        public static bool IsEnum(this Type type)
        {
            return type.GetTypeInfo().IsEnum;
        }

        public static bool IsGenericParameter(this Type type)
        {
            return type.GetTypeInfo().IsGenericParameter;
        }

        public static bool IsGenericType(this Type type)
        {
            return type.GetTypeInfo().IsGenericType;
        }

        public static bool IsGenericTypeDefinition(this Type type)
        {
            return type.GetTypeInfo().IsGenericTypeDefinition;
        }

        public static bool IsInterface(this Type type)
        {
            return type.GetTypeInfo().IsInterface;
        }

        public static bool IsNotPublic(this Type type)
        {
            return !type.IsPublic();
        }

        public static bool IsNullable(this Type type)
        {
            if (type.IsValueType())
            {
                return (Nullable.GetUnderlyingType(type) != null);
            }
            return true;
        }

        public static bool IsPrimitive(this Type type)
        {
            return type.GetTypeInfo().IsPrimitive;
        }

        public static bool IsPublic(this Type type)
        {
            TypeInfo typeInfo = type.GetTypeInfo();
            return (typeInfo.IsPublic || (typeInfo.IsNestedPublic && type.DeclaringType.IsPublic()));
        }

        public static bool IsSealed(this Type type)
        {
            return type.GetTypeInfo().IsSealed;
        }

        public static bool IsSerializable(this Type type)
        {
            return type.GetTypeInfo().IsSerializable;
        }

        public static bool IsSubclassOf(this Type type, Type otherType)
        {
            return type.GetTypeInfo().IsSubclassOf(otherType);
        }
        
        public static bool IsValueType(this Type type)
        {
            return type.GetTypeInfo().IsValueType;
        }

        private static PropertyInfo MostDerived(IEnumerable<PropertyInfo> properties)
        {
            PropertyInfo info = null;
            foreach (PropertyInfo info2 in properties)
            {
                if ((info == null) || ((info.DeclaringType != null) && info.DeclaringType.IsAssignableFrom(info2.DeclaringType)))
                {
                    info = info2;
                }
            }
            return info;
        }

        public static string NestingNamespace(this Type type)
        {
            if (!type.IsNested)
            {
                return type.Namespace;
            }
            string fullName = type.FullName;
            return fullName.Substring(0, (fullName.Length - type.Name.Length) - 1).Replace('+', '.');
        }

        public static bool OverridesEqualsOrGetHashCode(this Type type)
        {
            while (type != typeof(object))
            {
                if (type.GetDeclaredMethods().Any<MethodInfo>(m => (((m.Name == "Equals") || (m.Name == "GetHashCode")) && (m.DeclaringType != typeof(object))) && (m.GetBaseDefinition().DeclaringType == typeof(object))))
                {
                    return true;
                }
                type = type.BaseType();
            }
            return false;
        }

        public static Type TryGetElementType(this Type type, Type interfaceOrBaseType)
        {
            if (type.IsGenericTypeDefinition())
            {
                return null;
            }
            List<Type> list = type.GetGenericTypeImplementations(interfaceOrBaseType).ToList<Type>();
            if (list.Count != 1)
            {
                return null;
            }
            return list[0].GetGenericArguments().FirstOrDefault<Type>();
        }

        public static bool TryUnwrapNullableType(this Type type, out Type underlyingType)
        {
            underlyingType = Nullable.GetUnderlyingType(type) ?? type;
            return (underlyingType != type);
        }
    }
}
