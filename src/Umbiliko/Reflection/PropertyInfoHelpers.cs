﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Uos.Reflection
{
    public static class PropertyInfoHelpers
    {
        public static bool CanWriteExtended(this PropertyInfo propertyInfo)
        {
            if (propertyInfo.CanWrite)
            {
                return true;
            }
            PropertyInfo declaredProperty = GetDeclaredProperty(propertyInfo);
            return ((declaredProperty != null) && declaredProperty.CanWrite);
        }

        private static void CollectProperties(PropertyInfo property, IList<PropertyInfo> collection)
        {
            FindNextProperty(property, collection, true);
            FindNextProperty(property, collection, false);
        }

        public static bool ContainsSame(this IEnumerable<PropertyInfo> enumerable, PropertyInfo propertyInfo)
        {
            return enumerable.Any<PropertyInfo>(item => item.IsSameAs(propertyInfo));
        }

        private static void FindNextProperty(PropertyInfo property, IList<PropertyInfo> collection, bool getter)
        {
            MethodInfo info = getter ? property.Getter() : property.Setter();
            if (info != null)
            {
                Type type = info.DeclaringType.BaseType();
                if ((type != null) && (type != typeof(object)))
                {
                    MethodInfo baseMethod = info.GetBaseDefinition();
                    PropertyInfo item = (from vara in type.GetInstanceProperties().Select(p => new { p, candidateMethod = getter ? p.Getter() : p.Setter() })
                                         where (vara.candidateMethod != null) && (vara.candidateMethod.GetBaseDefinition() == baseMethod)
                                         select vara.p).FirstOrDefault<PropertyInfo>();
                    if (item != null)
                    {
                        collection.Add(item);
                        CollectProperties(item, collection);
                    }
                }
            }
        }

        private static PropertyInfo GetDeclaredProperty(PropertyInfo propertyInfo)
        {
            if (!(propertyInfo.DeclaringType == propertyInfo.ReflectedType))
            {
                return propertyInfo.DeclaringType.GetInstanceProperties().SingleOrDefault<PropertyInfo>(p => (((p.Name == propertyInfo.Name) && !p.GetIndexParameters().Any<ParameterInfo>()) && (p.PropertyType == propertyInfo.PropertyType)));
            }
            return propertyInfo;
        }

        public static IEnumerable<PropertyInfo> GetPropertiesInHierarchy(this PropertyInfo property)
        {
            List<PropertyInfo> collection = new List<PropertyInfo> {
                property
            };
            CollectProperties(property, collection);
            return collection.Distinct<PropertyInfo>();
        }

        public static PropertyInfo GetPropertyInfoForSet(this PropertyInfo propertyInfo)
        {
            if (!propertyInfo.CanWrite)
            {
                PropertyInfo declaredProperty = GetDeclaredProperty(propertyInfo);
                if (declaredProperty != null)
                {
                    return declaredProperty;
                }
                return propertyInfo;
            }
            return propertyInfo;
        }

        public static MethodInfo Getter(this PropertyInfo property)
        {
            return property.GetMethod;
        }

        public static bool IsPublic(this PropertyInfo property)
        {
            MethodInfo ter = property.Getter();
            MethodAttributes attributes = (ter == null) ? MethodAttributes.Private : (ter.Attributes & MethodAttributes.MemberAccessMask);
            MethodInfo info2 = property.Setter();
            MethodAttributes attributes2 = (info2 == null) ? MethodAttributes.Private : (info2.Attributes & MethodAttributes.MemberAccessMask);
            MethodAttributes attributes3 = (attributes > attributes2) ? attributes : attributes2;
            return (attributes3 == MethodAttributes.Public);
        }

        public static bool IsSameAs(this PropertyInfo propertyInfo, PropertyInfo otherPropertyInfo)
        {
            if (propertyInfo != otherPropertyInfo)
            {
                if (!(propertyInfo.Name == otherPropertyInfo.Name))
                {
                    return false;
                }
                if ((!(propertyInfo.DeclaringType == otherPropertyInfo.DeclaringType) && !propertyInfo.DeclaringType.IsSubclassOf(otherPropertyInfo.DeclaringType)) && (!otherPropertyInfo.DeclaringType.IsSubclassOf(propertyInfo.DeclaringType) && !propertyInfo.DeclaringType.GetInterfaces().Contains<Type>(otherPropertyInfo.DeclaringType)))
                {
                    return otherPropertyInfo.DeclaringType.GetInterfaces().Contains<Type>(propertyInfo.DeclaringType);
                }
            }
            return true;
        }

        public static bool IsStatic(this PropertyInfo property)
        {
            return (property.Getter() ?? property.Setter()).IsStatic;
        }

        
        public static MethodInfo Setter(this PropertyInfo property)
        {
            return property.SetMethod;
        }
    }
}
