﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Reflection
{
    public static class ExpressionHelpers
    {
        public static void ForEach<T>(this IEnumerable<T> collection, Action<T> action)
        {
            foreach (var item in collection) action(item);
        }

        public static void ForEach<T>(this IQueryable<T> collection, Action<T> action)
        {
            foreach (var item in collection) action(item);
        }

        public static PropertyPath GetComplexPropertyAccess(this LambdaExpression propertyAccessExpression)
        {
            PropertyPath path = propertyAccessExpression.Parameters.Single<ParameterExpression>().MatchComplexPropertyAccess(propertyAccessExpression.Body);
            if (path == null)
            {
                throw Error.InvalidComplexPropertyExpression(propertyAccessExpression);
            }
            return path;
        }

        public static IEnumerable<PropertyPath> GetComplexPropertyAccessList(this LambdaExpression propertyAccessExpression)
        {
            IEnumerable<PropertyPath> enumerable = propertyAccessExpression.MatchPropertyAccessList((p, e) => e.MatchComplexPropertyAccess(p));
            if (enumerable == null)
            {
                throw Error.InvalidComplexPropertiesExpression(propertyAccessExpression);
            }
            return enumerable;
        }

        public static PropertyPath GetSimplePropertyAccess(this LambdaExpression propertyAccessExpression)
        {
            PropertyPath path = propertyAccessExpression.Parameters.Single<ParameterExpression>().MatchSimplePropertyAccess(propertyAccessExpression.Body);
            if (path == null)
            {
                throw Error.InvalidPropertyExpression(propertyAccessExpression);
            }
            return path;
        }

        public static IEnumerable<PropertyPath> GetSimplePropertyAccessList(this LambdaExpression propertyAccessExpression)
        {
            IEnumerable<PropertyPath> enumerable = propertyAccessExpression.MatchPropertyAccessList((p, e) => e.MatchSimplePropertyAccess(p));
            if (enumerable == null)
            {
                throw Error.InvalidPropertiesExpression(propertyAccessExpression);
            }
            return enumerable;
        }

        private static bool HasDefaultMembersOnly(this NewExpression newExpression, IEnumerable<PropertyPath> propertyPaths)
        {
            return !newExpression.Members.Where<MemberInfo>(((Func<MemberInfo, int, bool>)((t, i) => !string.Equals(t.Name, propertyPaths.ElementAt<PropertyPath>(i).Last<PropertyInfo>().Name, StringComparison.Ordinal)))).Any<MemberInfo>();
        }

        public static bool IsNullConstant(this Expression expression)
        {
            expression = expression.RemoveConvert();
            if (expression.NodeType != ExpressionType.Constant)
            {
                return false;
            }
            return (((ConstantExpression)expression).Value == null);
        }

        public static bool IsStringAddExpression(this Expression expression)
        {
            BinaryExpression expression2 = expression as BinaryExpression;
            if (expression2 == null)
            {
                return false;
            }
            if ((expression2.Method == null) || (expression2.NodeType != ExpressionType.Add))
            {
                return false;
            }
            return ((expression2.Method.DeclaringType == typeof(string)) && string.Equals(expression2.Method.Name, "Concat", StringComparison.Ordinal));
        }

        private static PropertyPath MatchComplexPropertyAccess(this Expression parameterExpression, Expression propertyAccessExpression)
        {
            return parameterExpression.MatchPropertyAccess(propertyAccessExpression);
        }

        private static PropertyPath MatchPropertyAccess(this Expression parameterExpression, Expression propertyAccessExpression)
        {
            MemberExpression expression;
            List<PropertyInfo> components = new List<PropertyInfo>();
            do
            {
                expression = propertyAccessExpression.RemoveConvert() as MemberExpression;
                if (expression == null)
                {
                    return null;
                }
                PropertyInfo member = expression.Member as PropertyInfo;
                if (member == null)
                {
                    return null;
                }
                components.Insert(0, member);
                propertyAccessExpression = expression.Expression;
            }
            while (expression.Expression != parameterExpression);
            return new PropertyPath(components);
        }

        private static IEnumerable<PropertyPath> MatchPropertyAccessList(this LambdaExpression lambdaExpression, Func<Expression, Expression, PropertyPath> propertyMatcher)
        {
            NewExpression newExpression = lambdaExpression.Body.RemoveConvert() as NewExpression;
            if (newExpression != null)
            {
                ParameterExpression parameterExpression = lambdaExpression.Parameters.Single<ParameterExpression>();
                IEnumerable<PropertyPath> source = from a in newExpression.Arguments
                                                   select propertyMatcher(a, parameterExpression) into p
                                                   where p != null
                                                   select p;
                if (source.Count<PropertyPath>() == newExpression.Arguments.Count<Expression>())
                {
                    if (!newExpression.HasDefaultMembersOnly(source))
                    {
                        return null;
                    }
                    return source;
                }
            }
            PropertyPath path = propertyMatcher(lambdaExpression.Body, lambdaExpression.Parameters.Single<ParameterExpression>());
            if (path == null)
            {
                return null;
            }
            return new PropertyPath[] { path };
        }

        private static PropertyPath MatchSimplePropertyAccess(this Expression parameterExpression, Expression propertyAccessExpression)
        {
            PropertyPath path = parameterExpression.MatchPropertyAccess(propertyAccessExpression);
            if ((path != null) && (path.Count == 1))
            {
                return path;
            }
            return null;
        }

        public static Expression RemoveConvert(this Expression expression)
        {
            while ((expression.NodeType == ExpressionType.Convert) || (expression.NodeType == ExpressionType.ConvertChecked))
            {
                expression = ((UnaryExpression)expression).Operand;
            }
            return expression;
        }
    }
}
