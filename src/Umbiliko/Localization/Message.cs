﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Custom.Localization
{
    public abstract class Message : IEnumerable<Text>
    {
        public abstract string this[Cultures culture] { get; }

        protected abstract IEnumerator<Text> GetEnumerator();

        IEnumerator<Text> IEnumerable<Text>.GetEnumerator()
        {
            return GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public override string ToString()
        {
            var culture = Thread.CurrentThread.CurrentCulture.Name.Split();

            return base.ToString();
        }
    }
}
