﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Custom.Localization
{
    public static class CultureHelper
    {
        public static Alphabets Alphabet(this Cultures culture)
        {
            throw new NotImplementedException();
        }

        public static Countries Country(this Cultures culture)
        {
            throw new NotImplementedException();
        }

        public static CultureInfo CultureInfo(this Cultures culture)
        {
            throw new NotImplementedException();
        }

        public static Cultures Encode(this CultureInfo cultureInfo)
        {
            return cultureInfo != null ? Parse(cultureInfo.Name) : Cultures.Invariant;
        }

        public static Languages Language(this Cultures culture)
        {
            throw new NotImplementedException();
        }

        public static Cultures Parse(string cultureName)
        {
            if (string.IsNullOrEmpty(cultureName)) return Cultures.Invariant;

            throw new NotImplementedException();
        }
    }
}
