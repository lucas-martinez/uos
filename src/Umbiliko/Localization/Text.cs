﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Uos.Localization
{
    public class Text
    {
        public static implicit operator Text(string s)
        {
            return new Text(s);
        }

        public static implicit operator string(Text text)
        {
            return text != null ? text.ToString() : null;
        }

        public static implicit operator CultureInfo(Text text)
        {
            return text != null ? new CultureInfo(text.Language) : null;
        }

        private readonly string _language;
        private readonly string _string;

        public Text(string s)
            : this(s, Thread.CurrentThread.CurrentCulture)
        {
        }

        public Text(string s, CultureInfo culture)
        {
            _string = s;
            _language = culture.TwoLetterISOLanguageName;
        }

        public string Language
        {
            get { return _language; }
        }

        public override bool Equals(object obj)
        {
            if (string.IsNullOrEmpty(_string) && obj == null) return true;

            return obj != null && _string.Equals(obj.ToString());
        }

        public override int GetHashCode()
        {
            return string.IsNullOrEmpty(_string) ? 0 : _string.GetHashCode();
        }

        public override string ToString()
        {
            return _string;
        }
    }
}
