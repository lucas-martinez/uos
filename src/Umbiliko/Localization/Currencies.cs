﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Custom.Localization
{
    /// <summary>
    /// http://www.nationsonline.org/oneworld/currencies.htm
    /// </summary>
    public enum Currencies : short
    {
        /// <summary>
        /// Afghani
        /// </summary>
        AFN	= 971,

        /// <summary>
        /// US Dollar (Next day)
        /// </summary>
        USN	 = 997,

        /// <summary>
        /// US Dollar (Same day)
        /// </summary>
        USS = 998
    }
}
