﻿using System;
using System.Reflection;

namespace Uos.Localization
{
    public class LocalizedDisplayNameAttribute : System.ComponentModel.DisplayNameAttribute
    {
        private PropertyInfo _nameProperty;
        private Type _resourceType;

        public LocalizedDisplayNameAttribute(string className, string propertyName)
            : base(className + (propertyName == null ? "_Class" : ("_" + propertyName)))
        {

        }

        public override string DisplayName
        {
            get
            {
                return LanguageService.Instance.Translate(base.DisplayName) ?? "**" + base.DisplayName + "**";
            }
        }
    }
}
