﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Assertion
{
    public sealed class ErrorContext
    {
        private static readonly ExceptionFormatter DefatultExceptionFormatter;
        private static readonly Dictionary<Guid, ExceptionFormatter> ExceptionFormatters;
        private List<ErrorMessage> _messages;
        private readonly IDictionary<string, object> _source;
        private Exception _exception;

        static ErrorContext()
        {
            DefatultExceptionFormatter = new ExceptionFormatter();
            ExceptionFormatters = new Dictionary<Guid, ExceptionFormatter>();
        }

        private ErrorContext(int stackFrame)
        {
            stackFrame++;

            var trace = new StackTrace();

            if (stackFrame > trace.FrameCount) stackFrame = trace.FrameCount - 1;

            StackFrame frame = trace.GetFrame(stackFrame);

            if (frame != null)
            {
                MethodBase method = frame.GetMethod();
                string methodSyntax = method.ToString();
                Type type = method.DeclaringType;
                string typeName = type.FullName;
                string assemblyName = type.Assembly.FullName;

                if (type.IsGenericType)
                {
                    typeName = typeName.Substring(0, typeName.IndexOf('`'));
                    typeName += '<' + String.Join(", ", type.GetGenericArguments().Select(paramType => paramType.Name)) +
                                '>';
                }

                _source = new Dictionary<string, object>
                {
                    {"Method", methodSyntax},
                    {"Type", typeName},
                    {"Assembly", assemblyName}
                };
            }
        }

        public ErrorContext(Exception exception, int stackFrame)
            : this(stackFrame)
        {
            _exception = exception;
            _messages = new List<ErrorMessage>();

            ExceptionFormatter formatter;

            if (!ExceptionFormatters.TryGetValue(exception.GetType().GUID, out formatter)) formatter = DefatultExceptionFormatter;

            _messages = new List<ErrorMessage>(formatter.Format(exception));
        }

        public Exception Exception
        {
            get { return _exception; }
        }

        public ICollection<ErrorMessage> Messages
        {
            get { return _messages; }
        }

        public static ErrorContext Create<TException>(int stackFrame)
            where TException : Exception
        {
            return Create<TException>(stackFrame + 1, string.Empty);
        }

        public static ErrorContext Create<TException>(int stackFrame, string message)
            where TException : Exception
        {
            var exception = (Exception)Activator.CreateInstance(typeof(TException), new object[] { message });

            return new ErrorContext(exception, stackFrame + 1);
        }

        public static ErrorContext Create<TException>(int stackFrame, string format, params object[] args)
            where TException : Exception
        {
            return Create<TException>(stackFrame + 1, String.Format(format, args));
        }

        public ErrorContext Create<TException>(int stackFrame, string paramName, string message)
            where TException : ArgumentException
        {
            var exception = (Exception)Activator.CreateInstance(typeof(TException), new object[] { paramName, message });

            return new ErrorContext(exception, stackFrame + 1);
        }

        public ErrorContext Error<TException>(int stackFrame, string paramName, string format, params object[] args)
            where TException : ArgumentException
        {
            return Create<TException>(stackFrame + 1, paramName, String.Format(format, args));
        }

        /// <summary>
        ///     Make ErrorContext throwable
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static implicit operator Exception(ErrorContext context)
        {
            return context != null ? context._exception : null;
        }

        public static void Register<TFormatter>()
            where TFormatter : ExceptionFormatter, new()
        {
            ExceptionFormatters.Add(typeof(TFormatter).GUID, new TFormatter());
        }

        public static implicit operator string[](ErrorContext context)
        {
            return context != null ? context.Messages.Select(e => (string)e.Text).ToArray() : new string[] { };
        }
    }
}
