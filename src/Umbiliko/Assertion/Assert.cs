﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace Uos.Assertion
{
    public class Assert : IResult
    {
        public static readonly Assert Success = new Assert(null);

        private static readonly ReadOnlyCollection<string> NoError = Array.AsReadOnly(new string[0]);

        public static Assert Empty(string value)
        {
            if (string.IsNullOrEmpty(value)) return Success;

            return new Assert(ErrorContext.Create<InvalidOperationException>(1));
        }

        public static Assert NotEmpty(string value)
        {
            if (!string.IsNullOrEmpty(value)) return Success;

            return new Assert(ErrorContext.Create<InvalidOperationException>(1));
        }

        public static Assert NotEquals<T>(T a, T b, Func<string> message) where T : IEquatable<T>
        {
            if (!object.Equals(a, b)) return Success;

            return new Assert(ErrorContext.Create<InvalidOperationException>(1, message()));
        }

        /*public static Assert NotNull(object value)
        {
            return new Assert(ErrorContext.Create<InvalidOperationException>(1));
        }*/

        public static Assert Null(object value)
        {
            return new Assert(ErrorContext.Create<InvalidOperationException>(1));
        }

        private ErrorContext _context;

        private Assert(ErrorContext context)
        {
            _context = context;

            if (context != null) Trace.Error(context);
        }

        public Assert Always(Action action)
        {
            if (action != null) action();

            return this;
        }

        public Assert Catch<TException>(Action<TException> action)
        {
            return this;
        }

        public void Pardon()
        {
            _context = null;
        }

        public void Throw()
        {
            if (_context != null) throw _context.Exception;
        }

        public void Fail(Action<ErrorContext> action)
        {
            action(_context);
        }

        public void Fail(Action<Exception> action)
        {
            action(_context.Exception);
        }

        /*public Result Result()
        {
            return (_context == null)
                ? Custom.Assurance.Result.Success
                : Custom.Assurance.Result.Fail(_context.Messages);
        }

        public Result<TValue> Result<TValue>()
        {
            return (_context == null)
                ? new Result<TValue> { Succeeded = true }
                : new Result<TValue> { Succeeded = false, Messages = _context.Messages };
        }*/

        IList<string> IResult.Messages
        {
            get { return _context != null ? Array.AsReadOnly(_context.Messages.Select(msg => msg.ToString()).ToArray()) : NoError; }
        }

        bool IResult.Success
        {
            get { return _context == null; }
        }

        public static Assert ArgumentNotNull(object paramValue, string paramName)
        {
            return ArgumentNotNull(paramValue, paramName, Strings.ArgumentNull);
        }

        public static Assert ArgumentNotNull(object paramValue, string paramName, string message)
        {
            if (paramValue != null) return Assert.Success;

            return new Assert(new ErrorContext(new ArgumentNullException(paramName, message), 1));
        }

        public static Assert ArgumentNotNullOrEmpty(string paramValue, string paramName)
        {
            return ArgumentNotNullOrEmpty(paramValue, paramName, Strings.ArgumentNull);
        }

        public static Assert ArgumentNotNullOrEmpty(string paramValue, string paramName, string message)
        {
            if (!string.IsNullOrEmpty(paramValue)) return Assert.Success;

            return new Assert(new ErrorContext(new ArgumentNullException(paramName, message), 1));
        }

        public static Assert ArgumentNullOrWhiteSpace(string paramValue, string paramName)
        {
            return ArgumentNullOrWhiteSpace(paramValue, paramName, Strings.ArgumentNull);
        }

        public static Assert ArgumentNullOrWhiteSpace(string paramValue, string paramName, string message)
        {
            if (!string.IsNullOrWhiteSpace(paramValue)) return Assert.Success;

            return new Assert(new ErrorContext(new ArgumentNullException(paramName, message), 1));
        }

        public static Assert NotNull(object obj)
        {
            return NotNull(obj, Strings.InvalidOperationError);
        }

        public static Assert NotNull<T>(T obj, string paramName)
            where T : class
        {
            return NotNull(obj, Strings.InvalidOperationError);
        }

        public static Assert NotNull(object obj, string message)
        {
            if (obj != null) return Assert.Success;

            return new Assert(new ErrorContext(new InvalidOperationException(message), 1));
        }

        public static Assert NotNull(object obj, string format, params object[] args)
        {
            return NotNull(obj, string.Format(format, args));
        }

        public static Assert False(bool b)
        {
            return False(b, Strings.InvalidOperationError);
        }

        public static Assert False(bool b, string message)
        {
            if (!b) return Assert.Success;

            return new Assert(new ErrorContext(new InvalidOperationException(message), 1));
        }

        public static Assert False(bool b, string format, params object[] args)
        {
            return False(b, string.Format(format, args));
        }

        public static Assert True(bool b)
        {
            return True(b, Strings.InvalidOperationError);
        }

        public static Assert True(bool b, string message)
        {
            if (b) return Assert.Success;

            return new Assert(new ErrorContext(new InvalidOperationException(message), 1));
        }

        public static Assert True(bool b, string format, params object[] args)
        {
            return True(b, string.Format(format, args));
        }

        public static Assert ObjectNotDisposed<T>(T instance, bool disposed)
            where T : IDisposable
        {
            if (!disposed) return Success;

            var objectName = instance.GetType().Name;

            var message = string.Format(Strings.ObjectDisposedError, objectName);

            var exception = new ObjectDisposedException(objectName, message);

            var context = new ErrorContext(exception, 1);

            return new Assert(context);
        }

        public static Assert ObjectNotDisposed<T>(T instance, bool disposed, string message)
            where T : IDisposable
        {
            if (!disposed) return Success;

            var objectName = instance.GetType().Name;

            var exception = new ObjectDisposedException(objectName, message);

            var context = new ErrorContext(exception, 1);

            return new Assert(context);
        }

        public static bool Arguments<TArg0>(params object[] args)
        {
            return args != null && args.Length == 1
                && args[0].GetType().IsAssignableFrom(typeof(TArg0));
        }

        public static bool Arguments<TArg0, TArg1>(params object[] args)
        {
            return args != null && args.Length == 2
                && args[0].GetType().IsAssignableFrom(typeof(TArg0))
                && args[1].GetType().IsAssignableFrom(typeof(TArg1));
        }

        public static bool Arguments<TArg0, TArg1, TArg2>(params object[] args)
        {
            return args != null && args.Length == 3
                && args[0].GetType().IsAssignableFrom(typeof(TArg0))
                && args[1].GetType().IsAssignableFrom(typeof(TArg1))
                && args[2].GetType().IsAssignableFrom(typeof(TArg2));
        }

        public static bool Arguments<TArg0, TArg1, TArg2, TArg3>(params object[] args)
        {
            return args != null && args.Length == 3
                && args[0].GetType().IsAssignableFrom(typeof(TArg0))
                && args[1].GetType().IsAssignableFrom(typeof(TArg1))
                && args[2].GetType().IsAssignableFrom(typeof(TArg2))
                && args[3].GetType().IsAssignableFrom(typeof(TArg3));
        }
    }
}
