﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Assertion
{
    public class ExceptionFormatter
    {
        public virtual IEnumerable<ErrorMessage> Format(Exception exception)
        {
            yield return new ErrorMessage { Text = exception.ToString() };
        }
    }
}
