﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Assertion
{
    using Uos.Localization;

    public class ErrorMessage
    {
        private IDictionary<string, object> _data;

        public IDictionary<string, object> Data
        {
            get { return _data ?? (_data = new Dictionary<string, object>()); }
            set { _data = value; }
        }

        public Text Text { get; set; }

        public override string ToString()
        {
            return string.Format("Message: \"{0}\"", Text);
        }

        public static implicit operator ErrorMessage(string text)
        {
            return new ErrorMessage { Text = text };
        }
    }
}
