﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Assertion
{
    public class Result : IResult
    {
        public static readonly IResult Default = new Result(true);

        public static IResult Fail(ErrorContext context)
        {
            return new Result(false, context.Messages.Select(msg => (string)msg.Text).ToArray());
        }

        public static IResult Fail(params string[] messages)
        {
            return new Result(messages);
        }

        public static IResult Fail(Exception exception)
        {
            return new Result(exception.ToString());
        }

        public static IResult<TResult> Fail<TResult>(params string[] messages)
        {
            return new Result<TResult>(false, messages);
        }

        public static IResult Fail(IList<string> messages)
        {
            return new Result(false, messages);
        }

        public static IResult<TResult> Fail<TResult>(Exception exception)
        {
            return new Result<TResult>(false, exception.ToString());
        }

        public static IResult<TResult> Ok<TResult>(TResult value, params string[] messages)
        {
            return new Result<TResult>(value, messages);
        }

        private readonly bool _success;
        private readonly IList<string> _messages;

        protected Result(bool success, params string[] messages)
        {
            _success = success;
            _messages = messages;
        }

        protected Result(bool success, IList<string> messages)
        {
            _success = success;
            _messages = messages;
        }

        private Result(bool success)
            : this(success, null)
        {
        }

        private Result(IEnumerable<string> messages)
            : this(false, messages.ToArray())
        {
        }

        private Result(params string[] messages)
            : this(false, messages)
        {
        } 

        public IList<string> Messages
        {
            get { return _messages; }
        }

        public bool Success
        {
            get { return _success; }
        }

        public static IResult All(params IResult[] results)
        {
            var messages = new List<string>();
            var success = true;

            foreach (var result in results)
            {
                messages.AddRange(result.Messages);
                success &= result.Success;
            }

            return success && messages.Count.Equals(0) ? Default : new Result(success, messages.ToArray());
        }
    }

    public class Result<TValue> : Result, IResult<TValue>
    {
        private readonly TValue _value;

        public Result(TValue value, params string[] messages)
            : base(true, messages)
        {
            _value = value;
        }

        public Result(TValue value, bool success, params string[] messages)
            : base(success, messages)
        {
            _value = value;
        }

        public Result(TValue value, bool success, IList<string> messages)
            : base(success, messages)
        {
            _value = value;
        }

        public Result(bool success, params string[] messages)
            : base(success, messages)
        {
            _value = default(TValue);
        }

        TValue IResult<TValue>.Value
        {
            get { return _value; }
        }

        public static new IResult<TValue> Fail(params string[] messages)
        {
            return new Result<TValue>(default(TValue), false, messages);
        }

        public static new IResult<TValue> Fail(IList<string> messages)
        {
            return new Result<TValue>(default(TValue), false, messages);
        }
    }
}
