﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Assertion
{
    public class Trace
    {
        private static System.Diagnostics.TraceListener _fileListener;
        private static System.Diagnostics.TraceListener _consoleListener;
        private static System.Diagnostics.TraceListener _logglyListener;

        static Trace()
        {
            Configure(System.Configuration.ConfigurationManager.GetSection("custom/trace") as TraceConfigurationSection ?? new TraceConfigurationSection
            {
            });
        }

        public static void Configure(TraceConfigurationSection configuration)
        {
        }

        public static void Error(Exception exception)
        {
            Error(new ErrorContext(exception, 1));
        }

        public static void Error(ErrorContext context)
        {
            foreach (var msg in context.Messages) Trace.Error(msg.ToString());
        }

        public static void Error(string message)
        {
            System.Diagnostics.Trace.TraceError(message);
            // You must close or flush the trace to empty the output buffer.
            System.Diagnostics.Trace.Flush();
        }

        public static void Error(string format, params object[] args)
        {
            System.Diagnostics.Trace.TraceError(format, args);
            // You must close or flush the trace to empty the output buffer.
            System.Diagnostics.Trace.Flush();
        }

        public static void Error(ErrorMessage message)
        {
            Trace.Error(message.ToString());
        }

        public static void Error(IEnumerable<ErrorMessage> messages)
        {
            foreach (var msg in messages) Trace.Error(msg);
        }

        public static void Error(System.Runtime.ExceptionServices.FirstChanceExceptionEventArgs e)
        {
            Error(e.Exception.ToString());
        }

        public static void Error(UnhandledExceptionEventArgs e)
        {
            Error(e.ExceptionObject.ToString());
        }

        public static void Info(string message)
        {
            System.Diagnostics.Trace.TraceInformation(message);
            // You must close or flush the trace to empty the output buffer.
            System.Diagnostics.Trace.Flush();
        }

        public static void Info(string format, params object[] args)
        {
            System.Diagnostics.Trace.TraceInformation(format, args);
            // You must close or flush the trace to empty the output buffer.
            System.Diagnostics.Trace.Flush();
        }

        public static void SetupOutputToConsole(bool outputToConsole = true)
        {
            if (_consoleListener != null) System.Diagnostics.Trace.Listeners.Remove(_consoleListener);

            _consoleListener = null;

            if (!outputToConsole) return;

            _consoleListener = new System.Diagnostics.ConsoleTraceListener();

            System.Diagnostics.Trace.Listeners.Add(_consoleListener);
        }

        public static void SetupOutputToFile(string fileName)
        {
            if (_fileListener != null) System.Diagnostics.Trace.Listeners.Remove(_fileListener);

            _fileListener = null;

            if (fileName == null) return;

            _fileListener = new System.Diagnostics.TextWriterTraceListener(fileName);

            System.Diagnostics.Trace.Listeners.Add(_fileListener);
        }

        /*public static void SetupOutputToLoggly(bool outputToLoggly = true)
        {
            if (_logglyListener != null) System.Diagnostics.Trace.Listeners.Remove(_logglyListener);

            _logglyListener = null;

            if (!outputToLoggly) return;

            _logglyListener = new Custom.Instrumentation.LogglyTraceListener();

            System.Diagnostics.Trace.Listeners.Add(_logglyListener);
        }*/

        public static void Warning(string message)
        {
            System.Diagnostics.Trace.TraceWarning(message);
            // You must close or flush the trace to empty the output buffer.
            System.Diagnostics.Trace.Flush();
        }

        public static void Warning(string format, params object[] args)
        {
            System.Diagnostics.Trace.TraceWarning(format, args);
            // You must close or flush the trace to empty the output buffer.
            System.Diagnostics.Trace.Flush();
        }
    }
}
