﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Assertion
{
    public interface IResult
    {
        IList<string> Messages { get; }

        bool Success { get; }
    }

    public interface IResult<TResult> : IResult
    {
        TResult Value { get; }
    }
}
