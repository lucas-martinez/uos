﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos
{
    public static class DateTimeExtensions
    {
        public static byte GetWeekOfYear(this DateTime date)
        {
            DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
            var woy = dfi.Calendar.GetWeekOfYear(date, dfi.CalendarWeekRule, dfi.FirstDayOfWeek);
            return (byte)(woy < 0 ? 0 : woy > 255 ? 255 : woy);
        }

        public static DateTimeOffset ToLocalDateTimeOffset(this DateTime value, DateTimeKind kind)
        {
            if (value.Kind != kind) value = DateTime.SpecifyKind(value, kind);

            if (kind == DateTimeKind.Utc) value = value.ToLocalTime();

            return new DateTimeOffset(value);
        }

        public static DateTimeOffset ToUtcDateTimeOffset(this DateTime value, DateTimeKind kind)
        {
            if (value.Kind != kind) value = DateTime.SpecifyKind(value, kind);

            if (kind == DateTimeKind.Local) value = value.ToUniversalTime();

            return new DateTimeOffset(value);
        }
    }
}
