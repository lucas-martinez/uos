﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Data.Schema
{
    public static class EntityTypeExtensions
    {
        public static EntityType<TEntity> Complex<TEntity, TComplexProperty>(this EntityType<TEntity> configuration, Expression<Func<TEntity, TComplexProperty>> property, Action<ComplexType<TComplexProperty>> complexType)
            where TEntity : class, new()
            where TComplexProperty : class, new()
        {
            var complexConfiguration = new ComplexType<TComplexProperty>();

            if (complexType != null)
            {
                complexType(complexConfiguration);
            }

            return configuration;
        }

        public static EntityType<TEntity> Complex<TEntity, TComplexProperty>(this EntityType<TEntity> configuration, Expression<Func<TEntity, ICollection<TComplexProperty>>> property, Action<ComplexType<TComplexProperty>> complexType)
            where TEntity : class, new()
            where TComplexProperty : class, new()
        {
            var complexConfiguration = new ComplexType<TComplexProperty>();

            if (complexType != null)
            {
                complexType(complexConfiguration);
            }

            return configuration;
        }

        public static EntityType<TEntity> CompoundKey<TEntity, TEntityKey>(this EntityType<TEntity> configuration, Expression<Func<TEntity, TEntityKey>> key)
            where TEntity : class, new()
            where TEntityKey : class
        {
            configuration.SetCompoundKey(key);

            return configuration;
        }

        public static EntityType<TEntity> IdentityKey<TEntity>(this EntityType<TEntity> configuration, Expression<Func<TEntity, int>> property, string columnName = null)
            where TEntity : class, new()
        {
            //configuration.AddRequired(property, columnName);

            configuration.SetIdentityKey(property);

            return configuration;
        }

        public static EntityType<TEntity> PrimaryKey<TEntity>(this EntityType<TEntity> configuration, Expression<Func<TEntity, string>> property, string dbName = null, int? maxLength = null, bool unicode = true, bool fixedLength = false)
            where TEntity : class, new()
        {
            //configuration.AddRequired(property, columnName, maxLength, unicode, fixedLength);

            configuration.SetPrimaryKey(property, dbName, maxLength, unicode, fixedLength);

            return configuration;
        }

        public static EntityType<TEntity> PrimaryKey<TEntity, TProperty>(this EntityType<TEntity> configuration, Expression<Func<TEntity, TProperty>> property, string dbName = null)
            where TEntity : class, new()
            where TProperty : struct
        {
            //configuration.AddRequired(property, columnName);

            configuration.SetPrimaryKey(property);

            return configuration;
        }

        public static EntityType<TEntity> Many<TEntity, TItem>(this EntityType<TEntity> configuration, Expression<Func<TEntity, ICollection<TItem>>> property)
            where TEntity : class, new()
            where TItem : class, new()
        {
            configuration.AddMany(property);

            return configuration;
        }

        public static EntityType<TEntity> Optional<TEntity, TTarget, TTargetKey>(this EntityType<TEntity> configuration, Expression<Func<TEntity, TTarget>> property, Expression<Func<TTarget, TTargetKey>> foreighKey)
            where TEntity : class, new()
            where TTarget : class, new()
            where TTargetKey : struct
        {
            configuration.AddOptional(property: property, foreignKey: foreighKey);

            return configuration;
        }

        public static EntityType<TEntity> Required<TEntity, TTarget, TTargetKey>(this EntityType<TEntity> configuration, Expression<Func<TEntity, TTarget>> property, Expression<Func<TTarget, TTargetKey>> foreignKey)
            where TEntity : class, new()
            where TTarget : class, new()
            where TTargetKey : struct
        {
            configuration.AddRequired(property: property, foreignKey: foreignKey);

            return configuration;
        }

        public static EntityType<TEntity> Timestamp<TEntity>(this EntityType<TEntity> configuration, Expression<Func<TEntity, byte[]>> property, string dbName = null)
            where TEntity : class, new()
        {
            configuration.Add(new TimestampProperty<TEntity>(configuration, property, dbName));

            return configuration;
        }

        public static EntityType<TEntity> Timestamp<TEntity>(this EntityType<TEntity> configuration, Expression<Func<TEntity, long>> property, string dbName = null)
            where TEntity : class, new()
        {
            configuration.Add(new TimestampProperty<TEntity>(configuration, property, dbName));

            return configuration;
        }

        public static EntityType<TEntity> Timestamp<TEntity>(this EntityType<TEntity> configuration, Expression<Func<TEntity, ulong>> property, string dbName = null)
            where TEntity : class, new()
        {
            configuration.Add(new TimestampProperty<TEntity>(configuration, property, dbName));

            return configuration;
        }
        
    }
}
