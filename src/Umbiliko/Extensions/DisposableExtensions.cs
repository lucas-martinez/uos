﻿using System;

namespace Uos
{
    public static class DisposableExtensions
    {
        public static void TryDispose(this IDisposable disposable)
        {
            try
            {
                if (disposable != null) disposable.Dispose();
            }
            catch(Exception)
            {
            }
        }
    }
}
