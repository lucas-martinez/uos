﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Xsl;

namespace Uos
{
    using Xml.Linq;

    public static class StringBuilderExtensions
    {
        static XslCompiledTransform _xslExceptionTransform;

        public static XslCompiledTransform XslExceptionTransform
        {
            get
            {
                if (_xslExceptionTransform == null)
                {
                    _xslExceptionTransform = new XslCompiledTransform();

                    var assembly = typeof(ExceptionXElement).Assembly;

                    var info = assembly.GetManifestResourceInfo("Uos.Resources.resources");

                    using (Stream stream = assembly.GetManifestResourceStream(@"Uos.Resources.ExceptionFormatter.xsl"))

                    using (StreamReader reader = new StreamReader(stream))
                    {
                        _xslExceptionTransform.Load(XmlReader.Create(reader));
                    }
                }

                return _xslExceptionTransform;
            }
        }

        public static void WriteExceptionHtml(this StringWriter stringWriter, Exception ex)
        {
            var xmlException = new ExceptionXElement(ex);
            
            var xmlWriter = new XmlTextWriter(stringWriter);

            XslExceptionTransform.Transform(xmlException.CreateReader(), null, xmlWriter);
        }
    }
}
