﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace Uos
{
    public static class ResourceManagerExtensions
    {
        public static IDictionary<object, string> ToDictionary<T>(this ResourceManager strings)
        {
            var result = new Dictionary<object, string>();

            var type = typeof(T);

            if (type.IsValueType)
            {
                var names = Enum.GetNames(type);

                Array.Sort(names, StringComparer.OrdinalIgnoreCase);
                
                foreach (var name in names)
                {
                    result.Add(name, strings.GetString(name));
                }
            }
            else
            {
                var fields = Utilities.ReflectionUtilities.GetConstantFields(typeof(T));

                Func<FieldInfo, string> getDisplayName = (field) =>
                {
                    var attr = field.GetCustomAttribute<DisplayNameAttribute>(false);

                    return attr != null ? attr.DisplayName : field.Name;
                };

                Func<FieldInfo, string> getString = strings != null
                    ? (field) => strings.GetString(field.Name) ?? getDisplayName(field)
                    : getDisplayName;

                foreach (var field in fields)
                {
                    var value = field.GetValue(null);
                    var text = getString(field);
                    result.Add(value, text);
                }
            }

            return result;
        }
    }
}
