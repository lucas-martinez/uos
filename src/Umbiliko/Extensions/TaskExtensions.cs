﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos
{
    public static class TaskExtensions
    {
        public static T Await<T>(this Task<T> task)
        {
            //var awaiter = task.GetAwaiter();

            task.Wait();

            return task.Result;
        }
    }
}
