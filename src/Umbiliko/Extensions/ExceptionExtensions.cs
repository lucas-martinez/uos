﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos
{
    public static class ExceptionExtensions
    {
        public static string InnerMessage(this Exception ex)
        {
            return ex != null ? InnerMessage(ex.InnerException) ?? ex.Message : null;
        }
    }
}
