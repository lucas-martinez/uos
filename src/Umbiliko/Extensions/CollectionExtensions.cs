﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos
{
    public static class CollectionExtensions
    {
        /*public static TCollection Add<TCollection, TItem>(this TCollection collection, int count)
            where TCollection : ICollection<TItem>
            where TItem : new()
        {
            for (var i = 0; i < count; i++)
            {
                collection.Add(new TItem());
            }

            return collection;
        }*/

        public static List<TItem> Add<TItem>(this List<TItem> list, int count)
            where TItem : new()
        {
            for (var i = 0; i < count; i++)
            {
                list.Add(new TItem());
            }

            return list;
        }
    }
}
