<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" encoding="utf-8" indent="no"/>
  <xsl:template match="/">
    <html>
      <body>
        <h1>
          <xsl:value-of select="/Exception/@type"/>
        </h1>
        <h2>
          <xsl:value-of select="/Exception/@message"/>
        </h2>
        <table border="1">
          <caption>Stack Trace</caption>
          <tr bgcolor="#9acd32">
            <th>Class/Type</th>
            <th>Method Name</th>
            <th>File Name</th>
            <th>Line Number</th>
          </tr>
          <xsl:for-each select="//Frame">
            <tr>
              <td>
                <xsl:value-of select="./@type"/>
              </td>
              <td>
                <xsl:value-of select="./@method"/>
              </td>
              <td>
                <xsl:value-of select="./@file"/>
              </td>
              <td>
                <xsl:value-of select="./@line"/>
              </td>
            </tr>
          </xsl:for-each>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>