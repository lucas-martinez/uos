﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Implementation
{
    public sealed class EntityMapper<TObject> : IEnumerable<KeyValuePair<string, EntityMapper<TObject>.Property>>
        where TObject : class
    {
        private readonly Dictionary<string, Property> _properties;

        public EntityMapper(IDictionary<string, Expression<Func<TObject, object>>> mapping)
        {
            _properties = mapping.ToDictionary(pair => pair.Key, pair => Map(pair.Value));
        }

        public Dictionary<string, string> Names { get { return _properties.ToDictionary(pair => pair.Key, pair => pair.Value.Name); } }

        public Dictionary<string, Type> Types { get { return _properties.ToDictionary(pair => pair.Key, pair => pair.Value.PrimitiveType); } }

        public object Get(TObject instance, string propertyName)
        {
            Property property;
            return _properties.TryGetValue(propertyName, out property) ? property.Getter.DynamicInvoke(instance) : null;
        }

        public void Set(TObject instance, string propertyName, object value)
        {
            Property property;
            if (_properties.TryGetValue(propertyName, out property)) property.Setter.DynamicInvoke(instance, value);
        }

        public static PropertyInfo GetPropertyInfo(Expression<Func<TObject, object>> lambdaExpression)
        {
            MemberExpression memberExpression = null;

            if (lambdaExpression.Body.NodeType == ExpressionType.Convert)
            {
                memberExpression = ((UnaryExpression)lambdaExpression.Body).Operand as MemberExpression;
            }
            else if (lambdaExpression.Body.NodeType == ExpressionType.MemberAccess)
            {
                memberExpression = lambdaExpression.Body as MemberExpression;
            }

            if (memberExpression == null) throw new ArgumentException("lambdaExpression");

            return memberExpression.Member as PropertyInfo;
        }

        public static string GetPropertyPath<T, P>(Expression<Func<T, P>> expr)
        {
            var path = new Stack<string>();

            MemberExpression me;
            switch (expr.Body.NodeType)
            {
                case ExpressionType.Convert:
                case ExpressionType.ConvertChecked:
                    var ue = expr.Body as UnaryExpression;
                    me = ((ue != null) ? ue.Operand : null) as MemberExpression;
                    break;
                default:
                    me = expr.Body as MemberExpression;
                    break;
            }

            while (me != null)
            {
                string propertyName = me.Member.Name;

                Type propertyType = me.Type;

                path.Push(propertyName);

                me = me.Expression as MemberExpression;
            }

            return string.Join(".", path);
        }

        private static Property Map(Expression<Func<TObject, object>> lambdaExpression)
        {
            var info = GetPropertyInfo(lambdaExpression);

            var propertyName = GetPropertyPath<TObject, object>(lambdaExpression);

            bool primitive = false;
            bool nullable = false;
            bool collection = false;
            Type primitiveType = null;
            Type type = info.PropertyType;

            if (!type.IsGenericType)
            {
                primitive = true;
                primitiveType = type;

                if (type == typeof(string)) nullable = true;
            }
            else
            {
                var genericDefinition = type.GetGenericTypeDefinition();

                if (genericDefinition == typeof(Nullable<>))
                {
                    nullable = true;
                    primitive = true;
                    primitiveType = type.GetGenericArguments()[0];
                }
                else if (genericDefinition == typeof(IEnumerable<>))
                {
                    collection = true;
                    primitive = true;
                    primitiveType = type.GetGenericArguments()[0];
                }
            }

            return new Property
            {
                Collection = collection,
                Getter = GetAccessor(propertyName),// MakeGetter(info.Name),
                Name = info.Name,
                Nullable = nullable,
                Primitive = primitive,
                PrimitiveType = primitiveType,
                Setter = GetMutator(propertyName),// MakeSetter(info.Name, info.PropertyType),
                Type = type
            };
        }

        public enum PropertyOrFieldAccessType
        {
            Get,
            Set
        }

        public static LambdaExpression GetFieldOrPropertyLambda(PropertyOrFieldAccessType accessType, Type objectType, string fieldOrPropertyExpression)
        {
            ParameterExpression initialObjectParameterExpression = Expression.Parameter(objectType, objectType.Name);


            Expression pathExpression = initialObjectParameterExpression;

            foreach (string property in fieldOrPropertyExpression.Split('.'))
                pathExpression = Expression.PropertyOrField(pathExpression, property);

            LambdaExpression resultExpression;
            switch (accessType)
            {
                case PropertyOrFieldAccessType.Get:

                    resultExpression = Expression.Lambda(
                        getGenericGetFunction(objectType, pathExpression.Type), // This makes it work for valueTypes.
                        pathExpression,
                        initialObjectParameterExpression);
                    break;

                case PropertyOrFieldAccessType.Set:

                    ParameterExpression assignParameterExpression = Expression.Parameter(pathExpression.Type);

                    BinaryExpression assginExpression = Expression.Assign(pathExpression, assignParameterExpression);


                    resultExpression = Expression.Lambda(
                        getGenericSetAction(objectType, assginExpression.Type), // This makes it work for valueTypes.
                        assginExpression,
                        initialObjectParameterExpression,
                        assignParameterExpression);
                    break;

                default: throw new NotImplementedException();
            }

            return resultExpression;
        }

        private static Type getGenericGetFunction(Type param1, Type param2)
        {
            return typeof(Func<,>).MakeGenericType(param1, param2);
        }

        private static Type getGenericSetAction(Type param1, Type param2)
        {
            return typeof(Action<,>).MakeGenericType(param1, param2);
        }

        // returns property getter
        public static Delegate MakeGetter(string propertyName)
        {
            ParameterExpression paramExpression = Expression.Parameter(typeof(TObject), "value");

            Expression propertyGetterExpression = Expression.Property(paramExpression, propertyName);

            return Expression.Lambda(propertyGetterExpression, paramExpression).Compile();
        }

        public static Delegate GetAccessor(string propertyName)
        {
            return GetFieldOrPropertyLambda(PropertyOrFieldAccessType.Get, typeof(TObject), propertyName).Compile();
        }

        public static Delegate GetMutator(string propertyName)
        {
            return GetFieldOrPropertyLambda(PropertyOrFieldAccessType.Set, typeof(TObject), propertyName).Compile();
        }

        // returns property setter:
        public static Delegate MakeSetter(string propertyName, Type propertyType)
        {
            ParameterExpression paramExpression = Expression.Parameter(typeof(TObject));

            ParameterExpression paramExpression2 = Expression.Parameter(propertyType, propertyName);

            MemberExpression propertySetterExpression = Expression.Property(paramExpression, propertyName);

            return Expression.Lambda(Expression.Assign(propertySetterExpression, paramExpression2), paramExpression, paramExpression2).Compile();
        }

        public struct Property
        {
            public Delegate Getter;

            public string Name;

            public Delegate Setter;

            public Type Type;

            public bool Primitive;

            public Type PrimitiveType;

            public bool Nullable;

            public bool Collection;
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _properties.GetEnumerator();
        }

        IEnumerator<KeyValuePair<string, EntityMapper<TObject>.Property>> IEnumerable<KeyValuePair<string, EntityMapper<TObject>.Property>>.GetEnumerator()
        {
            return _properties.GetEnumerator(); ;
        }
    }
}
