﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Implementation
{
    public class EqualityComparer<T> : System.Collections.Generic.EqualityComparer<T>
    {
        private readonly Func<T, T, bool> _comparison;

        public EqualityComparer(Func<T, T, bool> comparison)
        {
            _comparison = comparison;
        }

        public override bool Equals(T x, T y)
        {
            return _comparison(x, y);
        }

        public override int GetHashCode(T obj)
        {
            return object.ReferenceEquals(obj, default(T)) ? 0 : obj.GetHashCode();
        }

        public static implicit operator EqualityComparer<T>(Func<T, T, bool> comparison)
        {
            return comparison != null ? new EqualityComparer<T>(comparison) : null;
        }
    }
}
