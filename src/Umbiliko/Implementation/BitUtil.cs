﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Implementation
{
    public static class BitUtil
    {
        public static int Flags(params bool[] values)
        {
            int result = 0;

            for (var i = 0; i < values.Length; i++)
            {
                if (values[i]) result |= 1 << i;
            }

            return result;
        }
    }
}
