﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos
{
    public class ServicesContainer
    {
        public static ServicesContainer Default { get; } = new ServicesContainer();

        private readonly LightInject.ServiceContainer _serviceContainer;

        private ServicesContainer()
        {
            _serviceContainer = new LightInject.ServiceContainer();
        }

        /// <summary>
        ///     Gets an instance of the given <typeparamref name="TService" /> type.
        /// </summary>
        /// <typeparam name="TService">The type of the requested service.</typeparam>
        /// <returns>The requested service instance.</returns>
        public TService GetInstance<TService>()
        {
            return (TService)_serviceContainer.GetInstance(typeof(TService));
        }

        /// <summary>
        ///     Registers the <typeparamref name="TService" /> with the <typeparamref name="TImplementation" />.
        /// </summary>
        /// <typeparam name="TService">The service type to register.</typeparam>
        /// <typeparam name="TImplementation">The implementing type.</typeparam>
        public void Register<TService, TImplementation>() where TImplementation : TService
        {
            _serviceContainer.Register(typeof(TService), typeof(TImplementation));
        }

        /// <summary>
        ///     Registers the <typeparamref name="TService" /> with the <typeparamref name="TImplementation" />.
        /// </summary>
        /// <typeparam name="TService">The service type to register.</typeparam>
        /// <typeparam name="TImplementation">The implementing type.</typeparam>
        public void Register<TService>(TService instance)
            where TService : class
        {
            _serviceContainer.RegisterInstance(typeof(TService), instance);
        }

        public void Register<TService>(Func<TService> factory)
            where TService : class
        {
            _serviceContainer.Register(serviceFactory => factory != null ? factory() : null);
        }
    }
}
