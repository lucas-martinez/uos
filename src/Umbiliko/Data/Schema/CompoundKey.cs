﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Data.Schema
{
    public class CompoundKey<TEntity, TEntityKey> : EntityKey<TEntity, TEntityKey>
        where TEntity : class, new()
        where TEntityKey: class
    {
        public CompoundKey(Expression<Func<TEntity, TEntityKey>> key)
            : base(key)
        {
        }
    }
}
