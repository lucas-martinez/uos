﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace Uos.Data.Schema
{
    using Reflection;

    public class XmlProperty<TComplex> : Property<TComplex>
        where TComplex : class, new()
    {
        public XmlProperty(ComplexType<TComplex> typeConfiguration, Expression<Func<TComplex, XmlNode>> property, string dbName, bool nullable)
            : base(typeConfiguration, property.GetComplexPropertyAccess(), dbName, DbType.Xml, SqlDbType.Xml, nullable)
        {
        }

        public XmlProperty(ComplexType<TComplex> typeConfiguration, Expression<Func<TComplex, XNode>> property, string dbName, bool nullable)
            : base(typeConfiguration, property.GetComplexPropertyAccess(), dbName, DbType.Xml, SqlDbType.Xml, nullable)
        {
        }
    }
}
