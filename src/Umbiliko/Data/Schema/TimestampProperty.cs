﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Data.Schema
{
    using Uos.Reflection;

    public class TimestampProperty<TComplex> : Property<TComplex>
        where TComplex : class, new()
    {
        public TimestampProperty(ComplexType<TComplex> typeConfiguration, Expression<Func<TComplex, byte[]>> property, string dbName)
            : base(typeConfiguration, property.GetComplexPropertyAccess(), dbName, DbType.Binary, SqlDbType.Timestamp, false)
        {
        }

        public TimestampProperty(ComplexType<TComplex> typeConfiguration, Expression<Func<TComplex, long>> property, string dbName)
            : base(typeConfiguration, property.GetComplexPropertyAccess(), dbName, DbType.Binary, SqlDbType.Timestamp, false)
        {
        }

        public TimestampProperty(ComplexType<TComplex> typeConfiguration, Expression<Func<TComplex, ulong>> property, string dbName)
            : base(typeConfiguration, property.GetComplexPropertyAccess(), dbName, DbType.Binary, SqlDbType.Timestamp, false)
        {
        }
    }
}
