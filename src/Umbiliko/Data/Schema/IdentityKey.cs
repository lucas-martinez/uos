﻿using System;
using System.Linq.Expressions;

namespace Uos.Data.Schema
{
    public class IdentityKey<TEntity, TProperty> : EntityKey<TEntity, TProperty>
        where TEntity : class, new()
        where TProperty : struct
    {
        public IdentityKey(Expression<Func<TEntity, TProperty>> key)
            : base(key)
        {
        }
    }
}
