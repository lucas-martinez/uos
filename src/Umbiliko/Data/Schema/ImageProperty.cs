﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Data.Schema
{
    using Uos.Reflection;

    public class ImageProperty<TComplex> : Property<TComplex>
        where TComplex : class, new()
    {
        public ImageProperty(ComplexType<TComplex> typeConfiguration, Expression<Func<TComplex, Image>> property, string dbName, bool nullable)
            : base(typeConfiguration, property.GetComplexPropertyAccess(), dbName, "image", nullable)
        {
        }
    }
}
