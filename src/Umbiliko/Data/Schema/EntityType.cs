﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Uos.Data.Schema
{
    public class EntityType<TEntity> : ComplexType<TEntity>, IEntityType
        where TEntity : class, new()
    {
        private EntityKey<TEntity> _primaryKey;
        
        private readonly string _entitySetName;
        private readonly string _schema;
        private readonly string _tableName;

        private static string Pluralize(string name)
        {
            return name;
        }
        
        public EntityType()
            : this(Pluralize(typeof(TEntity).Name))
        {
        }

        public EntityType(string entitySetName)
            : this(entitySetName, entitySetName.Flatten('_'), typeof(TEntity).Namespace.Split('.').Last().Flatten('_'))
        {
        }

        public EntityType(string tableName, string schema)
            : this(Pluralize(typeof(TEntity).Name), tableName, schema)
        {
        }

        public EntityType(string entitySetName, string tableName, string schema)
        {
            _entitySetName = entitySetName;
            _schema = schema != null ? schema.ToLowerInvariant() : null;
            _tableName = tableName;
        }

        public string EntitySetName
        {
            get { return _entitySetName; }
        }

        public string SchemaName
        {
            get { return _schema; }
        }

        public string DbName
        {
            get { return _tableName; }
        }

        public NavigationPropertyConfiguration<TEntity, TItem> AddMany<TItem>(Expression<Func<TEntity, ICollection<TItem>>> property)
            where TItem : class, new()
        {
            return new NavigationPropertyConfiguration<TEntity, TItem>(this, property);
        }

        public ReferenceProperty<TEntity, TTarget, TTargetKey> AddOptional<TTarget, TTargetKey>(Expression<Func<TEntity, TTarget>> property, Expression<Func<TTarget, TTargetKey>> foreignKey)
            where TTarget : class, new()
            where TTargetKey : struct
        {
            return new ReferenceProperty<TEntity, TTarget, TTargetKey>(this, referenceProperty: property, keyProperty: foreignKey, nullable: true);
        }

        public ReferenceProperty<TEntity, TTarget, TTargetKey> AddRequired<TTarget, TTargetKey>(Expression<Func<TEntity, TTarget>> property, Expression<Func<TTarget, TTargetKey>> foreignKey)
            where TTarget : class, new()
            where TTargetKey : struct
        {
            return new ReferenceProperty<TEntity, TTarget, TTargetKey>(this, referenceProperty: property, keyProperty: foreignKey, nullable: false);
        }

        public virtual void Apply(DataTable dataTable)
        {

        }

        public override TEntity Create(DataRow dataRow)
        {
            var entity = base.Create(dataRow);

            return entity;
        }

        public EntityType<TEntity> SetCompoundKey<TEntityKey>(Expression<Func<TEntity, TEntityKey>> key)
            where TEntityKey : class
        {
            _primaryKey = new CompoundKey<TEntity, TEntityKey>(key);

            return this;
        }

        public EntityType<TEntity> SetIdentityKey(Expression<Func<TEntity, int>> property, string dbName = null)
        {
            _primaryKey = new IdentityKey<TEntity, int>(property);

            var propertyConfig = new RequiredPropertyConfiguration<TEntity, int>(this, property, dbName);

            return this;
        }

        public EntityType<TEntity> SetPrimaryKey(Expression<Func<TEntity, string>> property, string dbName = null, int? maxLength = null, bool unicode = true, bool fixedLength = false)
        {
            _primaryKey = new EntityKey<TEntity, string>(property);

            var propertyConfig = new StringProperty<TEntity>(this, property, dbName, false, maxLength, unicode, fixedLength);

            return this;
        }

        public EntityType<TEntity> SetPrimaryKey<TProperty>(Expression<Func<TEntity, TProperty>> property, string dbName = null)
            where TProperty : struct
        {
            _primaryKey = new EntityKey<TEntity, TProperty>(property);

            var propertyConfig = new RequiredPropertyConfiguration<TEntity, TProperty>(this, property, dbName);

            return this;
        }

        public override void Update(TEntity target, DataRow source)
        {
            foreach (var property in Properties)
            {
                property.Update(target: target, source: source);
            }
        }

        public override void Update(DataRow target, TEntity source)
        {
            foreach (var property in Properties)
            {
                property.Update(target: target, source: source);
            }
        }

        // PK_ for primary keys
        // UK_ for unique keys
        // IX_ for non clustered non unique indexes
        // UX_ for unique indexes
        // <index or key type>_<table name>_<column 1>_<column 2>_<column n>
        private const string PrimaryKeyType = "PK";
        private const string UniqueKeyType = "UK";
        private const string NonUniqueIndexType = "IX";
        private const string UniqueIndexType = "UX";

        string FormatIndex(string type, params string[] dbNames)
        {
            var name = new List<string> { type };

            if (_schema != null) name.Add(_schema);

            name.Add(DbName);

            name.AddRange(dbNames);

            return string.Join("_", name);
        }

        protected virtual TEntity CloneEntity(TEntity instance)
        {
            throw new NotImplementedException();
        }

        protected virtual IEnumerable<MemberInfo> GetPrimaryKeyMembers()
        {
            throw new NotImplementedException();
        }

        #region IEntityType

        Type IEntityType.ElementType
        {
            get { return typeof(TEntity); }
        }

        Type IEntityType.EntityType
        {
            get { return typeof(TEntity); }
        }

        IEnumerable<MemberInfo> IEntityType.GetPrimaryKeyMembers()
        {
            return GetPrimaryKeyMembers();
        }

        bool IEntityType.IsPrimaryKey(MemberInfo member)
        {
            return GetPrimaryKeyMembers().Any(m => m == member);
        }

        object IEntityType.GetPrimaryKey(object instance)
        {
            throw new NotImplementedException();
        }

        public Expression GetPrimaryKeyQuery(Expression source, Expression[] keys)
        {
            throw new NotImplementedException();
        }

        IEnumerable<EntityInfo> IEntityType.GetDependentEntities(object instance)
        {
            return new EntityInfo[] { };
        }

        IEnumerable<EntityInfo> IEntityType.GetDependingEntities(object instance)
        {
            return new EntityInfo[] { };
        }

        object IEntityType.CloneEntity(object instance)
        {
            throw new NotImplementedException();
        }
        
        bool IEntityType.IsRelationship(MemberInfo member)
        {
            return false;
        }

        IEntityType IEntityType.GetRelatedEntity(MemberInfo member)
        {
            return null;
        }

        bool IEntityType.IsAssociationRelationship(MemberInfo member)
        {
            return false;
        }

        IEnumerable<MemberInfo> IEntityType.GetAssociationKeyMembers(MemberInfo member)
        {
            throw new NotImplementedException();
        }

        IEnumerable<MemberInfo> IEntityType.GetAssociationRelatedKeyMembers(MemberInfo member)
        {
            throw new NotImplementedException();
        }

        bool IEntityType.IsRelationshipSource(MemberInfo member)
        {
            throw new NotImplementedException();
        }

        bool IEntityType.IsRelationshipTarget(MemberInfo member)
        {
            throw new NotImplementedException();
        }

        bool IEntityType.IsNestedEntity(MemberInfo member)
        {
            throw new NotImplementedException();
        }

        string IEntityType.GetAlias()
        {
            return DbName;
        }

        bool IEntityType.IsExtensionTable()
        {
            return false;
        }

        string IEntityType.GetExtensionRelatedAlias()
        {
            return DbName;
        }

        IEnumerable<string> IEntityType.GetExtensionKeyColumnNames()
        {
            return new string[] { };
        }

        IEnumerable<MemberInfo> IEntityType.GetExtensionRelatedMembers()
        {
            return new MemberInfo[] { };
        }

        #endregion IEntityType
    }
}
