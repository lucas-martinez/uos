﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Data.Schema
{
    using Uos.Reflection;

    public class ReferenceProperty<TEntity, TTarget, TTargetKey> : Property<TEntity>
        where TEntity : class, new()
        where TTarget : class, new()
        where TTargetKey : struct
    {
        public ReferenceProperty(EntityType<TEntity> typeConfiguration, Expression<Func<TEntity, TTarget>> referenceProperty, Expression<Func<TTarget, TTargetKey>> keyProperty, bool nullable)
            : base(typeConfiguration, referenceProperty.GetComplexPropertyAccess(), null, null, nullable)
        {
        }

        public override PropertyTypes PropertyFlags
        {
            get { return PropertyTypes.Reference | (Nullable ? PropertyTypes.Optional : PropertyTypes.Required); }
        }
    }
}
