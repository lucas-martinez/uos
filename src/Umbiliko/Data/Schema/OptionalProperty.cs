﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Data.Schema
{
    using Uos.Reflection;

    public class OptionalProperty<TComplex, TProperty> : Property<TComplex>
        where TComplex : class, new()
        where TProperty : struct
    {
        private DbType? _dbType;

        public OptionalProperty(ComplexType<TComplex> typeConfiguration, Expression<Func<TComplex, Nullable<TProperty>>> property, string columnName)
            : base(typeConfiguration, property.GetComplexPropertyAccess(), columnName, true)
        {
            Nullable = true;
        }

        public override PropertyTypes PropertyFlags
        {
            get { return PropertyTypes.Optional; }
        }
    }
}
