﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Data.Schema
{
    using Uos.Reflection;

    public class ComplexProperty<TEntity, TProperty> : Property<TEntity>
        where TEntity : class, new()
        where TProperty : class, new()
    {
        private readonly ComplexType<TProperty> _typeConfiguration;

        public ComplexProperty(EntityType<TEntity> typeConfiguration, Expression<Func<TEntity, TProperty>> property, string dbName = null)
            : base(typeConfiguration, property.GetComplexPropertyAccess(), dbName, null, false)
        {
            _typeConfiguration = new ComplexType<TProperty>();
        }

        public override PropertyTypes PropertyFlags
        {
            get { return PropertyTypes.Complex; }
        }

        public ComplexType<TProperty> TypeConfiguration
        {
            get { return _typeConfiguration; }
        }
    }
}
