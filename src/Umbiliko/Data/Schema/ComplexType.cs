﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Data.Schema
{
    using System.Reflection;

    public class ComplexType<TComplex> : IEnumerable<Property<TComplex>>, IComplexType
        where TComplex : class, new()
    {
        private readonly Dictionary<string, Property<TComplex>> _properties;

        public ComplexType()
        {
            _properties = new Dictionary<string, Property<TComplex>>();
        }

        public ICollection<Property<TComplex>> Properties
        {
            get { return _properties.Values; }
        }

        public virtual TConfiguration Add<TConfiguration>(TConfiguration configuration)
            where TConfiguration : Property<TComplex>
        {
            _properties.Add(configuration.ToString(), configuration);

            return configuration;
        }

        public StringProperty<TComplex> AddOptional(Expression<Func<TComplex, string>> property, string dbName = null, int? maxLength = null, bool unicode = true, bool fixedLength = false)
        {
            return Add(new StringProperty<TComplex>(this, property, dbName, true, maxLength, unicode, fixedLength));
        }

        public OptionalProperty<TComplex, DateTime> AddOptional(Expression<Func<TComplex, Nullable<DateTime>>> property, string dbName = null, int presicion = 0)
        {
            return Add(new OptionalProperty<TComplex, DateTime>(this, property, dbName));
        }

        public OptionalProperty<TComplex, DateTimeOffset> AddOptional(Expression<Func<TComplex, Nullable<DateTimeOffset>>> property, string dbName = null, int presicion = 0)
        {
            return Add(new OptionalProperty<TComplex, DateTimeOffset>(this, property, dbName));
        }

        public OptionalProperty<TComplex, TProperty> AddOptional<TProperty>(Expression<Func<TComplex, Nullable<TProperty>>> property, string dbName = null)
            where TProperty : struct
        {
            return Add(new OptionalProperty<TComplex, TProperty>(this, property, dbName));
        }

        public StringProperty<TComplex> AddRequired(Expression<Func<TComplex, string>> property, string dbName = null, int? maxLength = null, bool unicode = true, bool fixedLength = false)
        {
            return Add(new StringProperty<TComplex>(this, property, dbName, false, maxLength, unicode, fixedLength));
        }

        public Property<TComplex> AddRequired(Expression<Func<TComplex, DateTime>> property, string dbName = null, int presicion = 0)
        {
            return Add(new RequiredPropertyConfiguration<TComplex, DateTime>(this, property, dbName));
        }

        public Property<TComplex> AddRequired(Expression<Func<TComplex, DateTimeOffset>> property, string dbName = null, int presicion = 0)
        {
            return Add(new RequiredPropertyConfiguration<TComplex, DateTimeOffset>(this, property, dbName));
        }

        public Property<TComplex> AddRequired<TProperty>(Expression<Func<TComplex, TProperty>> property, string dbName = null)
            where TProperty : struct
        {
            return Add(new RequiredPropertyConfiguration<TComplex, TProperty>(this, property, dbName));
        }

        protected virtual bool CanBeEvaluatedLocally(Expression expression)
        {
            throw new NotImplementedException();
        }

        public virtual TComplex Create(DataRow dataRow)
        {
            var entity = new TComplex();

            foreach (var property in _properties.Values)
            {
                property.Update(target: entity, source: dataRow);
            }

            return entity;
        }

        protected virtual IEnumerator<Property<TComplex>> GetEnumerator()
        {
            return _properties.Values.GetEnumerator();
        }

        IEnumerator<Property<TComplex>> IEnumerable<Property<TComplex>>.GetEnumerator()
        {
            return GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public virtual void Update(TComplex target, DataRow source)
        {
            foreach (var property in _properties.Values)
            {
                property.Update(target: target, source: source);
            }
        }

        public virtual void Update(DataRow target, TComplex source)
        {
            foreach (var property in _properties.Values)
            {
                property.Update(target: target, source: source);
            }
        }

        public virtual bool IsModified(TComplex instance, TComplex original)
        {
            throw new NotImplementedException();
        }

        #region - IComplexType -

        string IComplexType.GetColumnName(MemberInfo member)
        {
            return Properties
                .Where(p => p.PropertyInfo == member)
                .Select(p => p.DbName)
                .SingleOrDefault();
        }

        IEnumerable<MemberInfo> IComplexType.GetMappedMembers()
        {
            return Properties.Select(p => p.PropertyInfo);
        }

        bool IComplexType.IsSingletonRelationship(MemberInfo member)
        {
            return false;
        }

        bool IComplexType.CanBeEvaluatedLocally(Expression expression)
        {
            return CanBeEvaluatedLocally(expression);
        }

        bool IComplexType.IsModified(object instance, object original)
        {
            return IsModified(instance as TComplex, original as TComplex);
        }

        bool IComplexType.IsMapped(MemberInfo member)
        {
            return Properties.Any(p => p.PropertyInfo == member);
        }

        bool IComplexType.IsColumn(MemberInfo member)
        {
            return true;
        }

        bool IComplexType.IsComputed(MemberInfo member)
        {
            return false;
        }

        bool IComplexType.IsGenerated(MemberInfo member)
        {
            return false;
        }

        bool IComplexType.IsUpdatable(MemberInfo member)
        {
            return true;
        }

        #endregion - IComplexType -
    }
}
