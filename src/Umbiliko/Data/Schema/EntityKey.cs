﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Data.Schema
{
    public abstract class EntityKey<TEntity>
        where TEntity : class, new()
    {
    }

    public class EntityKey<TEntity, TProperty> : EntityKey<TEntity>
        where TEntity : class, new()
    {
        private Expression<Func<TEntity, TProperty>> _key;

        public EntityKey(Expression<Func<TEntity, TProperty>> key)
        {
            _key = key;
        }

        public Expression<Func<TEntity, TProperty>> Key
        {
            get { return _key; }
        }
    }
}
