﻿using System;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Uos.Data.Schema
{
    using Reflection;

    public abstract class Property<TComplex> : IProperty
        where TComplex : class, new()
    {
        private string _dbName;
        private DbType _dbType;
        private string _dbTypeName;
        private Delegate _getter;
        private bool _nullable;
        private string _propertyName;
        private string _propertyString;
        private readonly PropertyPath _propertyPath;
        private readonly Type _propertyType;
        private Delegate _setter;
        private readonly SqlDbType _sqlDbType;
        private readonly ComplexType<TComplex> _typeConfiguration;

        protected Property(ComplexType<TComplex> typeConfiguration, PropertyPath propertyPath, string dbName, bool nullable)
        {
            _dbName = dbName;
            _dbTypeName = null;
            _nullable = nullable;
            _propertyPath = propertyPath;
            _sqlDbType = SqlDbType.Udt;
            _typeConfiguration = typeConfiguration;
        }

        protected Property(ComplexType<TComplex> typeConfiguration, PropertyPath propertyPath, string dbName, string dbTypeName, bool nullable)
        {
            _dbName = dbName;
            _dbType = DbType.Object;
            _dbTypeName = dbTypeName;
            _nullable = nullable;
            _propertyPath = propertyPath;
            _sqlDbType = SqlDbType.Udt;
            _typeConfiguration = typeConfiguration;
        }

        protected Property(ComplexType<TComplex> typeConfiguration, PropertyPath propertyPath, string dbName, DbType dbType, SqlDbType sqlDbType, bool nullable)
        {
            _dbName = dbName;
            _dbType = dbType;
            _dbTypeName = sqlDbType.GetType().ToString();
            _nullable = nullable;
            _propertyPath = propertyPath;
            _typeConfiguration = typeConfiguration;
        }

        protected virtual int DbLength
        {
            get { return -1; }
        }

        public virtual string DbName
        {
            get { return _dbName ?? (_dbName = string.Join("_", _propertyPath.Select(p => p.Name.Flatten('_')))); }
        }

        protected virtual short DbPrecision
        {
            get { return -1; }
        }

        protected virtual short DbScale
        {
            get { return -1; }
        }

        public DbType DbType
        {
            get { return _dbType; }
        }

        public string DbTypeName
        {
            get { return _dbTypeName; }
        }

        protected virtual bool IsVariableLength
        {
            get { return false; }
        }

        public Delegate Getter
        {
            get { return _getter ?? (_getter = BuildPropertyLambda(true).Compile()); }
        }

        public bool Nullable
        {
            get { return _nullable; }
            protected set { _nullable = value; }
        }

        public virtual PropertyTypes PropertyFlags
        {
            get { return _nullable ? PropertyTypes.Optional : PropertyTypes.Required; }
        }

        public PropertyInfo PropertyInfo
        {
            get { return _propertyPath.First(); }
        }

        public string PropertyName
        {
            get { return _propertyName ?? (_propertyName = PropertyInfo.Name); }
        }

        public string GetPropertyString(string separator)
        {
            return string.Join(separator, _propertyPath.Select(p => p.Name));
        }

        public PropertyPath PropertyPath
        {
            get { return _propertyPath; }
        }

        public Type PropertyType
        {
            get { return PropertyInfo.PropertyType; }
        }

        public Delegate Setter
        {
            get { return _getter ?? (_setter = BuildPropertyLambda(false).Compile()); }
        }

        public ComplexType<TComplex> TypeConfiguration
        {
            get { return _typeConfiguration; }
        }

        public virtual void Set(object instance, object value)
        {
            Setter.DynamicInvoke(instance, value);
        }

        public virtual object Get(object instance)
        {
            return Getter.DynamicInvoke(instance);
        }

        public virtual void Update(object target, DataRow source)
        {
            object value = source[DbName];

            if (value == DBNull.Value) value = null;

            Set(target, source[DbName]);
        }

        /// <summary>
        /// Copy value from object to DataRow
        /// </summary>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="source"></param>
        /// <param name="target"></param>
        public virtual void Update(DataRow target, object source)
        {
            var value = Getter.DynamicInvoke(source);

            target[DbName] = value;
        }

        public override string ToString()
        {
            return _propertyString ?? (_propertyString = GetPropertyString("."));
        }

        private LambdaExpression BuildPropertyLambda(bool getter)
        {
            var entityType = typeof(TComplex);

            ParameterExpression initialObjectParameterExpression = Expression.Parameter(entityType, entityType.Name);

            Expression pathExpression = initialObjectParameterExpression;

            foreach (string property in PropertyPath.Select(p => p.Name))
                pathExpression = Expression.PropertyOrField(pathExpression, property);

            LambdaExpression resultExpression;
            if (getter)
            {
                resultExpression = Expression.Lambda(
                    getGenericGetFunction(entityType, pathExpression.Type), // This makes it work for valueTypes.
                    pathExpression,
                    initialObjectParameterExpression);
            }
            else
            {
                ParameterExpression assignParameterExpression = Expression.Parameter(pathExpression.Type);
                BinaryExpression assginExpression = Expression.Assign(pathExpression, assignParameterExpression);

                resultExpression = Expression.Lambda(
                    getGenericSetAction(entityType, assginExpression.Type), // This makes it work for valueTypes.
                    assginExpression,
                    initialObjectParameterExpression,
                    assignParameterExpression);
            }

            return resultExpression;
        }

        private static Type getGenericGetFunction(Type param1, Type param2)
        {
            return typeof(Func<,>).MakeGenericType(param1, param2);
        }

        private static Type getGenericSetAction(Type param1, Type param2)
        {
            return typeof(Action<,>).MakeGenericType(param1, param2);
        }

        #region IProperty

        bool IProperty.IsVariableLength
        {
            get { return IsVariableLength; }
        }

        int IProperty.Length
        {
            get { return DbLength; }
        }

        IEntityType IProperty.EntityType
        {
            get { return _typeConfiguration as IEntityType; }
        }

        bool IProperty.NotNull
        {
            get { return !_nullable; }
        }

        short IProperty.Precision
        {
            get { return DbPrecision; }
        }

        short IProperty.Scale
        {
            get { return DbScale; }
        }

        SqlDbType IProperty.SqlDbType
        {
            get { return _sqlDbType; }
        }

        #endregion
    }
}
