﻿namespace Uos.Data
{
    public struct EntityInfo
    {
        object _instance;
        IEntityType _entityType;

        public EntityInfo(object instance, IEntityType entityType)
        {
            _instance = instance;
            _entityType = entityType;
        }

        public object Instance
        {
            get { return _instance; }
        }

        public IEntityType Mapping
        {
            get { return _entityType; }
        }
    }
}