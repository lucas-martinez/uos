﻿using System;
using System.Data;

namespace Uos.Data
{
    public interface IProperty
    {
        string DbName { get; }

        DbType DbType { get; }

        IEntityType EntityType { get; }

        bool IsVariableLength { get; }

        int Length { get; }

        bool NotNull { get; }

        short Precision { get; }

        Type PropertyType { get; }

        short Scale { get; }

        SqlDbType SqlDbType { get; }
    }
}
