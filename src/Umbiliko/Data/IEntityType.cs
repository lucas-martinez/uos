﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Data
{
    public interface IEntityType : IComplexType
    {
        string DbName { get; }

        Type ElementType { get; }

        Type EntityType { get; }

        /// <summary>
        /// Determines if a property represents or is part of the entities unique identity (often primary key)
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        bool IsPrimaryKey(MemberInfo member);

        IEnumerable<MemberInfo> GetPrimaryKeyMembers();

        object GetPrimaryKey(object instance);

        Expression GetPrimaryKeyQuery(Expression source, Expression[] keys);

        IEnumerable<EntityInfo> GetDependentEntities(object instance);

        IEnumerable<EntityInfo> GetDependingEntities(object instance);

        object CloneEntity(object instance);

        /// <summary>
        /// Determines if a property is mapped as a relationship
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="member"></param>
        /// <returns></returns>
        bool IsRelationship(MemberInfo member);

        /// <summary>
        /// The type of the entity on the other side of the relationship
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        IEntityType GetRelatedEntity(MemberInfo member);

        /// <summary>
        /// Determines if the property is an assocation relationship.
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="member"></param>
        /// <returns></returns>
        bool IsAssociationRelationship(MemberInfo member);

        /// <summary>
        /// Returns the key members on this side of the association
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="member"></param>
        /// <returns></returns>
        IEnumerable<MemberInfo> GetAssociationKeyMembers(MemberInfo member);

        /// <summary>
        /// Returns the key members on the other side (related side) of the association
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="member"></param>
        /// <returns></returns>
        IEnumerable<MemberInfo> GetAssociationRelatedKeyMembers(MemberInfo member);

        bool IsRelationshipSource(MemberInfo member);

        bool IsRelationshipTarget(MemberInfo member);


        bool IsNestedEntity(MemberInfo member);

        string GetAlias();

        bool IsExtensionTable();

        string GetExtensionRelatedAlias();

        IEnumerable<string> GetExtensionKeyColumnNames();

        IEnumerable<MemberInfo> GetExtensionRelatedMembers();
    }
}
