﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Utilities
{
    public static class ReflectionUtilities
    {
        public static List<FieldInfo> GetConstantFields(this Type type)
        {
            FieldInfo[] fieldInfos = type.GetFields(BindingFlags.Public |
                 BindingFlags.Static | BindingFlags.FlattenHierarchy);

            return fieldInfos/*.Where(fi => fi.IsLiteral && !fi.IsInitOnly)*/.ToList();
        }
        
        public static List<object> GetConstantValues(this Type type)
        {
            return GetConstantFields(type).Select(field => field.GetValue(null)).ToList();
        }

        public static MemberInfo GetMember(this Expression expression)
        {
            if (expression == null)
            {
                throw new ArgumentException("The expression cannot be null.");
            }

            if (expression is LambdaExpression)
            {
                var lambdaExpression = (LambdaExpression)expression;
                return lambdaExpression.Body.GetMember();
            }

            if (expression is MemberExpression)
            {
                // Reference type property or field
                var memberExpression = (MemberExpression)expression;
                return memberExpression.Member;
            }

            if (expression is MethodCallExpression)
            {
                // Reference type method
                var methodCallExpression = (MethodCallExpression)expression;
                return methodCallExpression.Method;
            }

            if (expression is UnaryExpression)
            {
                // Property, field of method returning value type
                var unaryExpression = (UnaryExpression)expression;
                return GetMember(unaryExpression);
            }

            throw new ArgumentException("Invalid expression");
        }

        private static MemberInfo GetMember(UnaryExpression unaryExpression)
        {
            if (unaryExpression.Operand is MethodCallExpression)
            {
                var methodExpression = (MethodCallExpression)unaryExpression.Operand;
                return methodExpression.Method;
            }

            return ((MemberExpression)unaryExpression.Operand).Member;
        }
    }
}
