﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Utilities
{
    public static class DateUtilities
    {
        static DateTime? _javaScriptZero;

        public static DateTime JavaScriptZero
        {
            get { return _javaScriptZero ?? (_javaScriptZero = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).Value; }
        }

        public static long ToUnixMoment(this DateTime date)
        {
            return (long)date.Subtract(JavaScriptZero).TotalMilliseconds;
        }

        public static long? ToUnixMoment(this DateTime? date)
        {
            return date.HasValue ? (long)date.Value.Subtract(JavaScriptZero).TotalMilliseconds : (long?)null;
        }
    }
}
