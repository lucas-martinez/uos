﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Umbiliko.Injection
{
    public class DependencyResolver
    {
        private static readonly LightInject.ServiceContainer _serviceContainer = new LightInject.ServiceContainer();

        /// <summary>
        ///     Gets an instance of the given <typeparamref name="TService" /> type.
        /// </summary>
        /// <typeparam name="TService">The type of the requested service.</typeparam>
        /// <returns>The requested service instance.</returns>
        public static TService GetInstance<TService>()
        {
            return (TService)_serviceContainer.GetInstance(typeof(TService));
        }

        /// <summary>
        ///     Registers the <typeparamref name="TService" /> with the <typeparamref name="TImplementation" />.
        /// </summary>
        /// <typeparam name="TService">The service type to register.</typeparam>
        /// <typeparam name="TImplementation">The implementing type.</typeparam>
        public static void Register<TService, TImplementation>() where TImplementation : TService
        {
            _serviceContainer.Register(typeof(TService), typeof(TImplementation));
        }

        //// <summary>
        ///     Registers the <typeparamref name="TService" /> with a single instance.
        /// </summary>
        /// <typeparam name="TService">The service type to register.</typeparam>
        /// <param name="instance">Single instance</param>
        public static void RegisterInstance<TService>(TService instance)
        {
            _serviceContainer.RegisterInstance<TService>(instance);
        }
    }
}
