﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Uos.Xml.Linq
{
    public class StackFrameXElement : XElement
    {
        /// <summary>Create an instance of ExceptionXElement.</summary>
        /// <param name="exception">The Exception to serialize.</param>
        public StackFrameXElement(StackFrame stackFrame)
            : base(new Func<XElement>(() =>
            {
                // Validate arguments
                if (stackFrame == null)
                {
                    throw new ArgumentNullException("stackFrame");
                }
                
                var method = stackFrame.GetMethod();
                var type = method.DeclaringType.FullName;
                
                if (method.ReflectedType != method.DeclaringType)
                {
                    type += " (" + method.ReflectedType.Name + ")";
                }
                
                var root =  new XElement("Frame", new XAttribute("type", type), new XAttribute("method", method));

                var file = stackFrame.GetFileName();

                if (file != null)
                {
                    root.Add(new XAttribute("file", file), new XAttribute("line", stackFrame.GetFileLineNumber()));
                }

                return root;

            })())
        { }
    }
}
