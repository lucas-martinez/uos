﻿using System.Linq;

namespace Uos.Persistence
{
    public interface IUpdatable : IQueryable
    {
    }

    public interface IUpdatable<TEntity> : IUpdatable, IQueryable<TEntity>
    {
    }
}
