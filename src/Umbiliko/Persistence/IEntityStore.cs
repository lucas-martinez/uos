﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Persistence
{
    public interface IEntityStore : IQueryable, IUpdatable
    {
        new IEntityProvider Provider { get; }

        string DbName { get; }
        
        object GetById(object id);
        
        int Insert(object instance);
        
        int Update(object instance);
        
        int Delete(object instance);
        
        int InsertOrUpdate(object instance);
    }

    public interface IEntityStore<T> : IQueryable<T>, IEntityStore, IUpdatable<T>
    {
        new T GetById(object id);

        int Insert(T instance);
        
        int Update(T instance);
        
        int Delete(T instance);
        
        int InsertOrUpdate(T instance);
    }
}
