﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using System.Collections;

namespace Uos.Inquiry
{
	/// <summary>
	/// An IQueryable wrapper that allows us to visit the query's expression tree just before LINQ to SQL gets to it.
	/// This is based on the excellent work of Tomas Petricek: http://tomasp.net/blog/linq-expand.aspx
	/// </summary>
	public class ExpandableQuery<T> : IQueryable<T>, IOrderedQueryable<T>, IOrderedQueryable
	{
        IQueryable<T> _inner;
        ExpandableQueryProvider<T> _provider;
		
		internal protected ExpandableQuery (IQueryable<T> inner)
		{
			_inner = inner;
			_provider = new ExpandableQueryProvider<T> (this);
		}

        Type IQueryable.ElementType
        {
            get { return typeof(T); }
        }

        Expression IQueryable.Expression
        {
            get { return _inner.Expression; }
        }

        internal IQueryable<T> InnerQuery
        {
            // Original query, that we're wrapping
            get { return _inner; }
        }

        IQueryProvider IQueryable.Provider
        {
            get { return _provider; }
        }

        public IEnumerator<T> GetEnumerator ()
        {
            return _inner.GetEnumerator ();
        }

        IEnumerator IEnumerable.GetEnumerator ()
        {
            return _inner.GetEnumerator ();
        }

		public override string ToString ()
        {
            return _inner.ToString ();
        }
	}
}
