﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Personnel
{
    /// <summary>
    /// Human Resources Applications features
    /// </summary>
    public class Features
    {
        /// <summary>
        /// Personnel Administration
        /// </summary>
        public const string Administration = @"PA";

        /// <summary>
        /// Personnel Development
        /// </summary>
        public const string Development = @"PD";
    }
}
