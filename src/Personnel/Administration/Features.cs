﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Personnel.Administration
{
    /// <summary>
    /// Personnel Administration features
    /// </summary>
    public class Features
    {
        /// <summary>
        /// Applicant Management
        /// </summary>
        public const string Applicant = @"PA-APP";

        /// <summary>
        /// Benefits
        /// </summary>
        public const string Benefits = @"PA-BEN";

        /// <summary>
        /// Employee Management
        /// </summary>
        public const string Employee = @"PA-EMP";

        /// <summary>
        /// Incentive Wages
        /// </summary>
        public const string Incentive = @"PA-INV";

        /// <summary>
        /// Payroll
        /// </summary>
        public const string Payroll = @"PA-PAY";

        /// <summary>
        /// Time Management
        /// </summary>
        public const string Time = @"PA-TIM";

        /// <summary>
        /// Travel Expenses
        /// </summary>
        public const string Travel = @"PA-TRV";
    }
}
