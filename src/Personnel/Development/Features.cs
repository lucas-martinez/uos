﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Personnel.Development
{
    /// <summary>
    /// Personnel Development features
    /// </summary>
    public class Features
    {
        /// <summary>
        /// Organizational Management
        /// </summary>
        public const string Organizational = @"PD-OM";

        /// <summary>
        /// Room Reservation Planning
        /// </summary>
        public const string Room = @"PD-RPL";

        /// <summary>
        /// Seminars and Convention Managing
        /// </summary>
        public const string Seminar = @"PD-SCM";

        /// <summary>
        /// Workforce Planning
        /// </summary>
        public const string Workforce = @"PD-WFP";
    }
}
