﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uos.Logistics;

namespace Uos.Inventory
{
    /// <summary>
    /// <seealso cref="http://help.sap.com/saphelp_470/helpdata/en/a5/63386243a211d189410000e829fbbd/frameset.htm"/>
    /// </summary>
    public static class Stock
    {
        /// <summary>
        /// Goods issue
        /// Logistics > Materials Management > Inventory Management > Goods Movement > Goods Issue
        /// A goods issue(GI) is a goods movement with which a material withdrawal or material issue, a material consumption, or a shipment of goods to a customer is posted.A goods issue leads to a reduction in warehouse stock.
        /// </summary>
        /// <returns></returns>
        public static IResult GoodsIssue(IOrderDocument order)
        {
            return Result.NotImplemented;
        }

        /// <summary>
        /// Goods receipt
        /// Logistics > Materials Management > Inventory Management > Goods Movement > Goods Receipt
        /// A goods receipt(GR) is a goods movement with which the receipt of goods from a vendor or from production is posted.A goods receipt leads to an increase in warehouse stock.
        /// Example
        /// MB31 with 101 for Production order
        /// MIGO with movement 101 Purchase order
        /// </summary>
        /// <returns></returns>
        public static IResult GoodsReceive(IOrderDocument order)
        {
            return Result.NotImplemented;
        }

        /// <summary>
        /// Transfer posting
        /// A transfer posting is a general term for stock transfers and changes in stock type or stock category of a material.It is irrelevant whether the posting occurs in conjunction with a physical movement or not.Examples of transfer postings are:
        /// Transfer postings from material to material
        /// Release from quality inspection stock
        /// Transfer of consignment material into company's own stock
        /// MB1B check the movement types allowed you will get an idea about it.
        /// </summary>
        /// <returns></returns>
        public static IResult PostTransfer(IOrderDocument order)
        {
            return Result.NotImplemented;
        }

        /// <summary>
        /// Stock transfer
        /// A stock transfer is the removal of material from one storage location and its transfer to another storage location.Stock transfers can occur either within the same plant or between two plants.
        /// Example
        /// MB1B or MIGO with 311 storage location to storage location transfer.
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public static IResult Transfer(IOrderDocument order)
        {
            return Result.NotImplemented;
        }
    }
}
