﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace Uos.Domain
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class CompanyEntity
    {
        DateTime? _createdOn;
        ICollection<CompanyFeatureEntity> _features;
        ICollection<CompanyNoticeEntity> _remarks;
        ICollection<StafferRoleEntity> _roles;
        ICollection<StafferEntity> _staffers;

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual string CompanyCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual int CompanyId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual string CompanyName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual string CompanySize { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual string CompanyType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual string Country { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual DateTime CreatedOn
        {
            get { return _createdOn ?? (_createdOn = DateTime.UtcNow).Value; }
            set { _createdOn = null; }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual string Currency { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [IgnoreDataMember]
        public virtual ICollection<CompanyFeatureEntity> Features
        {
            get { return _features ?? (_features = new Collection<CompanyFeatureEntity>()); }
            set { _features = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual string Industry { get; set; }

        /// <summary>
        /// Imperial (IMP) or International (INT). It covers both weights and measures.
        /// </summary>
        [DataMember]
        public virtual string MetricSystem { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [IgnoreDataMember]
        public virtual ICollection<CompanyNoticeEntity> Remarks
        {
            get { return _remarks ?? (_remarks = new Collection<CompanyNoticeEntity>()); }
            set { _remarks = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual DateTime? RemovedOn { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [IgnoreDataMember]
        public virtual ICollection<StafferRoleEntity> Roles
        {
            get { return _roles ?? (_roles = new Collection<StafferRoleEntity>()); }
            set { _roles = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [IgnoreDataMember]
        public virtual ICollection<StafferEntity> Staffers
        {
            get { return _staffers ?? (_staffers = new Collection<StafferEntity>()); }
            set { _staffers = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual string Url { get; set; }
    }
}
