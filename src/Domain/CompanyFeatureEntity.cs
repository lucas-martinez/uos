﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Domain
{
    [DataContract]
    public class CompanyFeatureEntity
    {
        DateTime? _createdOn;

        /// <summary>
        /// 
        /// </summary>
        [IgnoreDataMember]
        public virtual CompanyEntity Company { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual string CompanyCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual DateTime CreatedOn
        {
            get { return _createdOn ?? (_createdOn = DateTime.UtcNow).Value; }
            set { _createdOn = null; }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual string FeatureName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual DateTime? RemovedOn { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [IgnoreDataMember]
        public virtual int SubscriptionId { get; set; }
    }
}
