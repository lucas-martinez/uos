﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Domain
{
    public class CompanyLocationEntity : IGeolocation
    {
        Guid? _locationId;
        LocationEntity _location;

        public virtual CompanyEntity Company { get; set; }

        public virtual string CompanyCode { get; set; }
        
        public virtual LocationEntity Location
        {
            get { return _location ?? (_location = new LocationEntity { LocationId = LocationId }); }
            set { _location = value; }
        }

        public virtual Guid LocationId
        {
            get { return _locationId ?? (_locationId = Guid.NewGuid()).Value; }
            set { _locationId = value; }
        }

        Geocode IGeolocation.Geocode
        {
            get
            {
                return new Geocode
                {
                    Altitude = Location.Altitude,
                    Latitude = Location.Latitude,
                    Longitude = Location.Longitude
                };
            }

            set
            {
                Location.Altitude = value.Altitude;
                Location.Latitude = value.Latitude;
                Location.Longitude = value.Longitude;
            }
        }

        PostalAddress IGeolocation.PostalAddress
        {
            get
            {
                return new PostalAddress
                {
                    Address = Location.Address,
                    Country = Location.Country,
                    Locality = Location.Locality,
                    PostalCode = Location.PostalCode,
                    Region = Location.Region
                };
            }

            set
            {
                Location.Address = value.Address;
                Location.Country = value.Country;
                Location.Locality = value.Locality;
                Location.PostalCode = value.PostalCode;
                Location.Region = value.Region;
            }
        }
    }
}
