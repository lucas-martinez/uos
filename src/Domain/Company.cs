﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Uos.Domain
{
    public static class Company
    {
        public static ICompanyStore Store
        {
            get { return DependencyResolver.Default.GetInstance<ICompanyStore>(); }
        }

        public static Task<IResult> AddCompanyNoticeAsync(string companyCode, string notice)
        {
            return Result.Try(async () =>
            {
                var store = Store;

                if (store == null) return Result.Fail(() => Strings.CompanyStoreResolveFailure);

                await store.AddCompanyNoticeAsync(companyCode, notice);

                return Result.Success;
            });
        }

        public static Task<IResult> CreateCompanyAsync(CompanyEntity companyEntity)
        {
            return Result.Try(async () =>
            {
                var store = Store;

                if (store == null) return Result.Fail(() => Strings.CompanyStoreResolveFailure);

                var value = await store.CreateCompanyAsync(companyEntity);
                
                return Result.Ok(value);
            });
        }

        public static Task<IResult> DeleteCompanyAsync(string companyCode)
        {
            return Result.Try(async () =>
            {
                var store = Store;

                if (store == null) return Result.Fail(() => Strings.CompanyStoreResolveFailure);

                var value = await store.GetCompanyAsync(companyCode);

                if (value == null) return Result.Fail(Message.Create(MessageTypes.Error, () => Strings.CompanyEntityNotFound, companyCode));

                return Result.Ok(value);
            });
        }

        public static Task<IResult> GetCompaniesAsync(int? start, int? limit, Expression<Func<CompanyEntity, string>> orderBy)
        {
            return Result.Try(async () =>
            {
                var store = Store;

                if (store == null) return Result.Fail(() => Strings.CompanyStoreResolveFailure);

                var values = await store.GetCompaniesAsync(start, limit, orderBy);

                return Result.Ok(values);
            });
        }

        public static Task<IResult> GetCompaniesCodeNameAsync(string startsWith)
        {
            return Result.Try(async () =>
            {
                var store = Store;

                if (store == null) return Result.Fail(() => Strings.CompanyStoreResolveFailure);

                var values = await store.GetCompaniesCodeNameAsync(startsWith);

                return Result.Ok(values);
            });
        }

        public static Task<IResult> GetCompanyAsync(string companyCode)
        {
            return Result.Try(async () =>
            {
                var store = Store;

                if (store == null) return Result.Fail(() => Strings.CompanyStoreResolveFailure);

                var value = await store.GetCompanyAsync(companyCode);

                if (value == null) return Result.Fail(Message.Create(MessageTypes.Error, () => Strings.CompanyEntityNotFound, companyCode));

                return Result.Ok(value);
            });
        }

        public static Task<IResult> GetCompanyFeaturesAsync(string companyCode)
        {
            return Result.Try(async () =>
            {
                var store = Store;

                if (store == null) return Result.Fail(() => Strings.CompanyStoreResolveFailure);

                var value = await store.GetCompanyFeaturesAsync(companyCode);

                if (value == null) return Result.Fail(Message.Create(MessageTypes.Error, () => Strings.CompanyEntityNotFound, companyCode));

                return Result.Ok(value);
            });
        }

        public static Task<IResult> SetFeaturesAsync(CompanyEntity company, IList<string> featureNames)
        {
            return Result.Try(async () =>
            {
                var store = Store;

                if (store == null) return Result.Fail(() => Strings.CompanyStoreResolveFailure);

                await store.SetFeaturesAsync(company, featureNames);

                return Result.Success;
            });
        }

        public static Task<IResult> UpdateCompanyAsync(string companyCode, Func<CompanyEntity, Task<bool>> patcher)
        {
            return Result.Try(async () =>
            {
                if (patcher == null) throw new ArgumentNullException("patcher");

                var store = Store;

                if (store == null) return Result.Fail(() => Strings.CompanyStoreResolveFailure);

                var value = await store.GetCompanyAsync(companyCode);

                if (value == null) return Result.Fail(Message.Create(MessageTypes.Error, () => Strings.CompanyEntityNotFound, companyCode));

                await patcher(value);

                await store.SaveChangesAsync();

                return Result.Ok(value);
            });
        }
    }
}
