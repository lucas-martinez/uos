﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Domain
{
    /// <summary>
    /// Location Entity
    /// </summary>
    public class LocationEntity : IGeocode, IGeolocation, IPostalAddress
    {
        string _culture;
        Guid? _id;
        DbGeography _dbGeography;

        /// <summary>
        /// Street Address
        /// </summary>
        public virtual string Address { get; set; }

        /// <summary>
        /// The future of geocoding also involves three-dimensional geocoding.
        /// </summary>
        public virtual decimal? Altitude { get; set; }

        /// <summary>
        /// Culture in which this location is described
        /// </summary>
        public virtual string Culture
        {
            get { return _culture ?? System.Globalization.CultureInfo.CurrentUICulture.Name; }
            set { _culture = value; }
        }

        /// <summary>
        /// Country
        /// </summary>
        public virtual string Country { get; set; }

        /// <summary>
        /// Used to calculate distances
        /// </summary>
        public virtual DbGeography DbGeography
        {
            get { return _dbGeography ?? (Latitude.HasValue && Longitude.HasValue ? _dbGeography = DbGeography.FromText(string.Format("POINT({1} {0})", Latitude.Value, Longitude.Value)) : null); }
            set { _dbGeography = value; }
        }

        /// <summary>
        /// The "latitude" (abbreviation: Lat., φ, or phi) of a point on the Earth's surface is the angle between the equatorial plane and the straight line that passes through that point and through (or close to) the center of the Earth.
        /// </summary>
        public virtual decimal? Latitude { get; set; }

        /// <summary>
        /// City or Twon
        /// </summary>
        public virtual string Locality { get; set; }

        /// <summary>
        /// Unique key
        /// </summary>
        public virtual Guid LocationId
        {
            get { return _id ?? (_id = Guid.NewGuid()).Value; }
            set { _id = value; }
        }

        /// <summary>
        /// The "longitude" (abbreviation: Long., λ, or lambda) of a point on the Earth's surface is the angle east or west from a reference meridian to another meridian that passes through that point.
        /// </summary>
        public virtual decimal? Longitude { get; set; }

        /// <summary>
        /// Postal Code
        /// </summary>
        public virtual string PostalCode { get; set; }

        /// <summary>
        /// State or Province (literal)
        /// </summary>
        public virtual string Region { get; set; }

        /// <summary>
        /// State (code)
        /// </summary>
        public virtual string State { get; set; }

        Geocode IGeolocation.Geocode
        {
            get
            {
                return new Geocode
                {
                    Altitude = Altitude,
                    Latitude = Latitude,
                    Longitude = Longitude
                };
            }

            set
            {
                Altitude = value.Altitude;
                Latitude = value.Latitude;
                Longitude = value.Longitude;
            }
        }

        PostalAddress IGeolocation.PostalAddress
        {
            get
            {
                return new PostalAddress
                {
                    Address = Address,
                    Country = Country,
                    Locality = Locality,
                    PostalCode = PostalCode,
                    Region = Region
                };
            }

            set
            {
                Address = value.Address;
                Country = value.Country;
                Locality = value.Locality;
                PostalCode = value.PostalCode;
                Region = value.Region;
            }
        }
    }
}
