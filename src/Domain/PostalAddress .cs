﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Domain
{
    /// <summary>
    /// Postal Address
    /// https://schema.org/PostalAddress
    /// </summary>
    public struct PostalAddress : IPostalAddress
    {
        /// <summary>
        /// Street Address
        /// </summary>
        public string Address { get; set; }
        
        /// <summary>
        /// Country
        /// </summary>
        public string Country { get; set; }
        
        /// <summary>
        /// City or Twon
        /// </summary>
        public string Locality { get; set; }

        /// <summary>
        /// Postal Code
        /// </summary>
        public string PostalCode { get; set; }

        /// <summary>
        /// State or Province
        /// </summary>
        public string Region { get; set; }
    }
}
