﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Domain
{
    public struct Geocode : IGeocode
    {
        /// <summary>
        /// The future of geocoding also involves three-dimensional geocoding.
        /// </summary>
        public decimal? Altitude { get; set; }

        /// <summary>
        /// The "latitude" (abbreviation: Lat., φ, or phi) of a point on the Earth's surface is the angle between the equatorial plane and the straight line that passes through that point and through (or close to) the center of the Earth.
        /// </summary>
        public decimal? Latitude { get; set; }

        /// <summary>
        /// The "longitude" (abbreviation: Long., λ, or lambda) of a point on the Earth's surface is the angle east or west from a reference meridian to another meridian that passes through that point.
        /// </summary>
        public decimal? Longitude { get; set; }
    }
}
