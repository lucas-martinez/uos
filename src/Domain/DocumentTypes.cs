﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Domain
{
    [DataContract]
    public enum DocumentTypes : byte
    {
        [EnumMember(Value = "freight")]
        FreightOrder,

        [EnumMember(Value = "purchase")]
        PurchaseOrder,

        [EnumMember(Value = "Sale")]
        SaleOrder,

        [EnumMember(Value = "transfer")]
        TransferOrder,

        [EnumMember(Value = "transport")]
        TransportOrder,

        [EnumMember(Value = "work-order")]
        WorkOrder
    }
}
