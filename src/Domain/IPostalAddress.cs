﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Domain
{
    /// <summary>
    /// Postal Address
    /// https://schema.org/PostalAddress
    /// </summary>
    public interface IPostalAddress
    {
        /// <summary>
        /// Street Address
        /// </summary>
        string Address { get; set; }
        
        /// <summary>
        /// Country
        /// </summary>
        string Country { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        string Locality { get; set; }

        /// <summary>
        /// Postal Code
        /// </summary>
        string PostalCode { get; set; }

        /// <summary>
        /// State or Province
        /// </summary>
        string Region { get; set; }
    }
}
