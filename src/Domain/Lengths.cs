﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Domain
{
    public static class Lengths
    {
        public const int CompanyCode = 7;

        public const int CompanyName = 0x7F;

        public const int CompanySize = 2;

        public const int CompanyType = 3;

        public const int Country = 3;

        public const int Culture = 0xF;

        public const int CustomerName = 0x7F;

        public const int CustomerNumber = 0xF;

        public const int Currency = 3;

        public const int Description = 0xFF;

        public const int EmailAddress = 0x3F;

        public const int FeatureName = 0x2F;

        public const int FieldValue = 0xFF;

        public const int Industry = 0x1F;

        public const int MessageText = 0xFFF;

        public const int MetricSystem = 3;

        public const int Name = 0x7F;

        public const int PhoneNumber = 0x2F;

        public const int PostalCode = 7;
        
        public const int PropertyName = 0xFF;

        public const int Remarks = 0x3FF;

        public const int RoleName = 0x2F;

        public const int RuntimeMemberName = 0xFF;

        public const int State = 2;

        public const int StreetAddress = 0x7F;

        public const int Url = 0xFF;
    }
}
