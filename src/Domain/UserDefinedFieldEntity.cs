﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Domain
{
    public class UserDefinedFieldEntity
    {
        /// <summary>
        /// 
        /// </summary>
        public virtual int CompanyId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual int DocumentId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual DocumentTypes DocumentType { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public virtual string DocumentValue { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual byte PropertyIndex { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual UserDefinedPropertyEntity Property { get; set; }
    }
}
