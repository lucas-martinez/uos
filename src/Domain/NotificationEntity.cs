﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Domain
{
    public class NotificationEntity
    {
        /// <summary>
        /// 
        /// </summary>
        public virtual int NotificationId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual DateTime CreatedOn { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual DateTime? DismisedOn { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual string Link { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual string Message { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual MessageTypes NotificationLevel { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual string UserEmailAddress { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual DateTime? UserEmailSentOn { get; set; }
    }
}
