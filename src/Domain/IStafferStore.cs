﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Uos.Domain
{
    public interface IStafferStore : IQueryable<StafferEntity>
    {
        Task<bool> AnyAsync(string companyCode, string emailAddress, string roleName = null);

        Task<StafferEntity> CreateStafferAsync(StafferEntity staffer);
        
        Task<StafferEntity> DeleteStafferAsync(int stafferId);
        
        Task<StafferEntity> DeleteStafferAsync(string companyCode, string emailAddress);

        Task<StafferRoleEntity> DeleteStafferRoleAsync(int id);

        Task<StafferRoleEntity> DeleteStafferRoleAsync(string companyCode, string emailAddress, string roleName);

        Task<IDictionary<string, string>> GetCompaniesAsync(string emailAddress);

        Task<IList<string>> GetIdentitiesAsync(string companyCode, string startsWith);

        Task<StafferEntity> GetStafferAsync(int stafferId);
        
        Task<StafferRoleEntity> GetStafferRoleAsync(int roleId);

        Task<StafferRoleEntity> GetStafferRoleAsync(string companyCode, string emailAddress, string roleName);

        Task<IList<string>> GetStafferRoleNamesAsync(string companyCode, string emailAddress);

        Task<IList<string>> GetStafferRoleNamesAsync(string startsWith);

        Task<IDictionary<string, string>> GetStafferRolesAsync(string companyCode);

        Task<StafferEntity> GetStafferAsync(string companyCode, string emailAddress);

        IQueryable<StafferEntity> Include<T>(Expression<Func<StafferEntity, T>> path);

        IQueryable<StafferEntity> Include<T1, T2>(Expression<Func<StafferEntity, T1>> path1, Expression<Func<StafferEntity, T2>> path2);

        Task SetRolesAsync(StafferEntity entity, IList<string> roleNames);

        Task<IList<StafferEntity>> GetStaffersAsync(string companyCode, int? start, int? limit, Expression<Func<StafferEntity, string>> orderBy);

        Task SaveChangesAsync();
    }
}
