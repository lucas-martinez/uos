﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Domain
{
    public class CompanySizes
    {
        /// <summary>
        /// 1-9
        /// </summary>
        public const string Micro = @"XS";

        /// <summary>
        /// 10-49
        /// </summary>
        public const string Small = @"SM";

        /// <summary>
        /// 50-249
        /// </summary>
        public const string Medium = @"MD";

        /// <summary>
        /// 250+
        /// </summary>
        public const string Large = @"LG";
    }
}
