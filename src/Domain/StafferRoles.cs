﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Domain
{
    public class StafferRoles
    {
        #region - Top Management positions -

        /// <summary>
        /// Chief Executive Officer (CEO) or President
        /// </summary>
        public const string President = @"CEO";

        /// <summary>
        /// Chief Operating Officer (COO), Vice President of Operations or General Manager.
        /// </summary>
        public const string OperationsVicePresident = @"COO";

        /// <summary>
        /// Vice President of Marketing or Marketing Manager.
        /// </summary>
        public const string MarketingVicePresident = @"MRK-VP";

        /// <summary>
        /// Chief Financial Officer (CFO) or Controller.
        /// </summary>
        public const string Controller = @"FI";

        /// <summary>
        /// Vice President of Production or Production Manager.
        /// </summary>
        public const string ProductionVicePresident = @"PRD-VP";

        #endregion - Top Management positions -

        #region - Key Personnel positions -

        /// <summary>
        /// Operations manager.
        /// </summary>
        public const string OperationsManager = @"OPS-MAN";

        /// <summary>
        /// Quality control, safety, environmental manager.
        /// </summary>
        public const string EnvironmentalManager = @"ENV-MAN";

        /// <summary>
        /// Accountant, bookkeeper, controller
        /// </summary>
        public const string Accountant = @"FI-ACC";

        /// <summary>
        /// Office manager
        /// </summary>
        public const string OfficeManager = @"OFF-MAN";

        /// <summary>
        /// Receptionist
        /// </summary>
        public const string Receptionist = @"OFF-FRONT";

        /// <summary>
        /// Foreperson, supervisor, lead person
        /// </summary>
        public const string Supervisor = @"OFF-SUPER";

        /// <summary>
        /// Receptionist
        /// </summary>
        public const string MarketingManager = @"MRK-MAN";

        /// <summary>
        /// Purchasing manager
        /// </summary>
        public const string PurchasingManager = @"PUR-MAN";

        /// <summary>
        /// Shipping and receiving person or manager
        /// </summary>
        public const string DeliveryManager = @"SD-SHP";

        /// <summary>
        /// Professional staff
        /// </summary>
        public const string ProfessionalStaff = @"PRO";
        
        #endregion - Key Personnel positions -
    }
}
