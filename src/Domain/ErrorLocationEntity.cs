﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Domain
{
    public class ErrorLocationEntity
    {
        /// <summary>
        /// 
        /// </summary>
        public virtual string ClassName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual ErrorEntity Error { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual int ErrorId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual byte LocationIndex { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual string MethodName { get; set; }
    }
}
