﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Domain
{
    public static class UserDefined
    {
        public static IListResult<UserDefinedFieldEntity> List(string companyCode, DocumentTypes documentType, int documentId)
        {
            return null;
        }

        public static IResult Set(string companyCode, DocumentTypes documentType, List<UserDefinedPropertyEntity> properties)
        {
            return null;
        }

        public static IResult Set(string companyCode, DocumentTypes documentType, byte propertyIndex, string propertyName, string propertySyntax)
        {
            return null;
        }

        public static IResult Set(string companyCode, DocumentTypes documentType, int documentId, byte propertyIndex, string fieldValue)
        {
            return null;
        }

        public static IResult SetField(string companyCode, DocumentTypes documentType, List<string> fieldValues)
        {
            return null;
        }
    }
}
