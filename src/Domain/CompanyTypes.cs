﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Domain
{
    /// <summary>
    /// Company types
    /// </summary>
    public class CompanyTypes
    {
        // Chartered company
        public const string CharteredCompany = @"CC";

        // Cooperative
        public const string Cooperative = @"COO";

        // Corporation
        public const string Corporation = @"COR";
        
        // Goverment entity
        public const string GovermentEntity = @"GOV";

        // Holding company
        public const string HoldingCompany = @"HC";

        // Individual
        public const string Individual = @"IND";

        // Limited liability company
        public const string LimitedLiabilityCompany = @"LLC";

        // Limited partnership
        public const string LimitedPartnership = @"LP";

        // Non Profit Organization
        public const string NonProfitOrganization = "NPO";

        // One man company
        public const string OneManCompany = "OMC";

        // Partnership
        public const string Partnership = "PAR";

        // Private Company
        public const string PrivateCompany = @"PRI";

        // Public Company
        public const string PublicCompany = @"PUB";

        // Sole Propietorship
        public const string SolePropietorship = @"SP";

        // Sole trader
        public const string SoleTrader = @"ST";

        // Statutory company
        public const string StatutoryCompany = @"STA";

        // Subsidiary company
        public const string SubsidiaryCompany = @"SUB";

        // Unlimited partnership
        public const string UnlimitedPartnership = @"UP";
    }
}
