﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Domain
{
    public class UserDefinedPropertyEntity
    {
        /// <summary>
        /// 
        /// </summary>
        public virtual int CompanyId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual DocumentTypes DocumentType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual bool Lookup { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public virtual byte PropertyIndex { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual string PropertyName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual string PropertySyntax { get; set; }
    }
}
