﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Domain
{
    public class Roles
    {
        /// <summary>
        /// Application super user.
        /// </summary>
        public const string Super = @"APP";

        /// <summary>
        /// Application administrator
        /// </summary>
        public const string Administrator = @"APP-ADM";

        /// <summary>
        /// Application debugger
        /// </summary>
        public const string Debuger = @"APP-BUG";

        /// <summary>
        /// Application designer
        /// </summary>
        public const string Designer = @"APP-DES";

        /// <summary>
        /// Application developer
        /// </summary>
        public const string Developer = @"APP-DEV";

        /// <summary>
        /// Application tester
        /// </summary>
        public const string Tester = @"APP-Q&A";

        /// <summary>
        /// Application text translator
        /// </summary>
        public const string Translator = @"APP-TXT";
    }
}
