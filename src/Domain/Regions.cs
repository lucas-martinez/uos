﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Xml.Linq;
using System.IO;

namespace Uos.Domain
{
    public partial class Regions
    {
        public struct Info
        {
            readonly short _phoneCode;
            readonly string _threeLettersCode;
            readonly string _twoLettersCode;

            public Info(string threeLettersCode, string twoLettersCode, short phoneCode)
            {
                _phoneCode = phoneCode;
                _twoLettersCode = twoLettersCode;
                _threeLettersCode = threeLettersCode;
            }

            public short PhoneCode { get { return _phoneCode; } }

            public string ThreeLetterCode { get { return _threeLettersCode; } }

            public string TwoLetterCode { get { return _twoLettersCode; } }
        }
    }
}
