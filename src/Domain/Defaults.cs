﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Domain
{
    public class Defaults
    {
        static List<string> _errors;

        internal static List<string> Errors
        {
            get { return _errors ?? (_errors = new List<string>()); }
        }

        public const string IdentityProvider = @"Uos";
        
        public const string ShortDateTimeFormat = @"yyyy-MM-dd'T'HH:mm:ss.fff";
        
        public static readonly JsonSerializerSettings JsonSerializerSettings = new JsonSerializerSettings
        {
            ContractResolver = ContractResolver.Instance,
            DateFormatString = ShortDateTimeFormat,
            Error = new EventHandler<Newtonsoft.Json.Serialization.ErrorEventArgs>((currentObject, args) =>
            {
                Errors.Add(string.Format("{0} {1}. {2}", args.CurrentObject.GetType(), args.ErrorContext.Path, args.ErrorContext.Error.Message));

                args.ErrorContext.Handled = true;
            }),
            NullValueHandling = NullValueHandling.Ignore
        };
    }

    public class ContractResolver : DefaultContractResolver
    {
        public new static readonly ContractResolver Instance = new ContractResolver();

        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            JsonProperty property = base.CreateProperty(member, memberSerialization);

            if (member.MemberType == MemberTypes.Property && typeof(IEnumerable).IsAssignableFrom(property.PropertyType))
            {
                var shouldDeserialize = property.ShouldDeserialize;

                property.ShouldSerialize = obj =>
                {
                    var value = (member as PropertyInfo).GetValue(obj) as IEnumerable;
                    if (value == null || !value.GetEnumerator().MoveNext()) return false;
                    return shouldDeserialize != null ? shouldDeserialize(obj) : true;
                };
            }

            return property;
        }
    }
}
