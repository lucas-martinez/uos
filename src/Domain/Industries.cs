﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Domain
{
    public class Industries
    {
        public const string Accommodations = @"accommodations";

        public const string Accounting = @"accounting";

        public const string Advertising = @"advertising";

        public const string Aerospace = @"aerospace";

        public const string Agriculture = @"agriculture";

        public const string AirTransportation = @"air-trans";

        public const string Apparel = @"apparel";

        public const string Auto = @"auto";

        public const string Banking = @"banking";

        public const string Beauty = @"beauty";

        public const string Biotechnology = @"biotech";

        public const string Chemical = @"chemical";

        public const string Communications = @"comm";

        public const string Computer = @"computer";

        public const string Construction = @"construction";

        public const string Consulting = @"consulting";

        public const string Consumer = @"consumer";

        public const string Education = @"education";

        public const string Electronics = @"electronics";

        public const string Employment = @"employment";

        public const string Energy = @"energy";

        public const string Entertainment = @"entertaiment";

        public const string Fashion = @"fashion";

        public const string FinancialServices = @"financial";

        public const string Food = @"food";

        public const string Health = @"health";

        public const string Information = @"info";

        public const string InformationTechnology = @"";

        public const string Insurance = @"insurance";

        public const string LegalServices = @"legal-services";

        public const string Manufacturing = @"manufacturing";

        public const string Media = @"media";

        public const string Medical = @"medical";

        public const string Movies = @"movies";

        public const string Music = @"music";

        public const string News = @"news";

        public const string Pharmaceutical = @"pharma";

        public const string PublicAdministration = @"pub-admin";

        public const string PublicRelations = @"pub-rel";

        public const string Publishing = @"publishing";

        public const string RealEstate = @"real-state";

        public const string Retail = @"retail";

        public const string Service = @"service";

        public const string Sports = @"sports";

        public const string Technology = @"tech";

        public const string Telecommunications = @"telecom";

        public const string Tourism = @"tourism";

        public const string Transportation = @"transp";

        public const string Travel = @"travel";

        public const string Utilities = @"utilities";

        public const string VideoGame = @"video-games";

        public const string WebServices = @"web";
    }
}
