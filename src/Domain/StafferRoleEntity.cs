﻿using System;
using System.Runtime.Serialization;
using Uos.Domain;

namespace Uos.Domain
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class StafferRoleEntity
    {
        DateTime? _createdOn;

        /// <summary>
        /// 
        /// </summary>
        [IgnoreDataMember]
        public virtual CompanyEntity Company { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual string CompanyCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual DateTime CreatedOn
        {
            get { return _createdOn ?? (_createdOn = DateTime.UtcNow).Value; }
            set { _createdOn = null; }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual string EmailAddress { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [IgnoreDataMember]
        public virtual int RoleId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual string RoleName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual DateTime? RemovedOn { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [IgnoreDataMember]
        public virtual StafferEntity Staffer { get; set; }
    }
}
