﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Uos.Domain
{
    public interface ICompanyStore : IQueryable<CompanyEntity>
    {
        Task AddCompanyNoticeAsync(string companyCode, string notice);

        Task<CompanyEntity> CreateCompanyAsync(CompanyEntity companyEntity);

        Task<CompanyEntity> DeleteCompanyAsync(string companyCode);

        Task<IList<CompanyEntity>> GetCompaniesAsync(int? start, int? limit, Expression<Func<CompanyEntity, string>> orderBy);

        Task<IDictionary<string, string>> GetCompaniesCodeNameAsync(string startsWith);

        Task<CompanyEntity> GetCompanyAsync(string companyCode);

        IList<string> GetCompanyFeatures(string companyCode);

        Task<IList<string>> GetCompanyFeaturesAsync(string companyCode);
        
        IQueryable<CompanyEntity> Include<T>(Expression<Func<CompanyEntity, T>> path);

        Task SaveChangesAsync();
        
        Task SetFeaturesAsync(CompanyEntity company, IList<string> featureNames);
    }
}
