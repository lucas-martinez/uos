﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Uos.Domain;

namespace Uos.Domain
{
    [DataContract]
    public class StafferEntity
    {
        DateTime? _createdOn;
        ICollection<StafferNoticeEntity> _remarks;
        ICollection<StafferRoleEntity> _roles;

        /// <summary>
        /// 
        /// </summary>
        [IgnoreDataMember]
        public virtual CompanyEntity Company { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual string CompanyCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual DateTime CreatedOn
        {
            get { return _createdOn ?? (_createdOn = DateTime.UtcNow).Value; }
            set { _createdOn = null; }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual string EmailAddress { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual string FirstName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual string LastName { get; set; }

        /// <summary>
        /// UTC date and time of the last login or activity.
        /// </summary>
        public DateTime? LoggedOn { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [IgnoreDataMember]
        public virtual ICollection<StafferNoticeEntity> Remarks
        {
            get { return _remarks ?? (_remarks = new Collection<StafferNoticeEntity>()); }
            set { _remarks = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual DateTime? RemovedOn { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [IgnoreDataMember]
        public virtual ICollection<StafferRoleEntity> Roles
        {
            get { return _roles ?? (_roles = new Collection<StafferRoleEntity>()); }
            set { _roles = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [IgnoreDataMember]
        public virtual int StafferId { get; set; }
    }
}
