﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Domain
{
    public class ErrorEntity
    {
        /// <summary>
        /// 
        /// </summary>
        public virtual DateTime CreatedOn { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual int ErrorId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual List<ErrorLocationEntity> ErrorLocations { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public virtual List<ErrorMessageEntity> Messages { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual string UserEmailAddress { get; set; }
    }
}
