﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Domain
{
    public class ErrorMessageEntity
    {
        /// <summary>
        /// 
        /// </summary>
        public virtual ErrorEntity Error { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual int ErrorId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual byte MessageIndex { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual string RuntimeType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual string Text { get; set; }
    }
}
