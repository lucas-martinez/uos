﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Domain
{
    public class CompanyRoles
    {
        public const string Architect = @"architect";

        public const string Customizer = @"customizer";
        
        public const string Driver = @"driver";

        public const string Editor = @"editor";

        public const string Manager = @"manager";

        public const string Producer = @"producer";

        public const string Scheduler = @"scheduler";

        public const string Translator = @"translator";
    }
}
