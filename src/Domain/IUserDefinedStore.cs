﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Domain
{
    public interface IUserDefinedStore : IDisposable
    {
        IListResult<UserDefinedFieldEntity> List(string companyCode, DocumentTypes documentType, int documentId);

        IResult SetProperties(string companyCode, DocumentTypes documentType, List<KeyValuePair<string, string>> properties);

        IResult SetProperty(string companyCode, DocumentTypes documentType, byte propertyIndex, string propertyName, string propertySyntax);

        IResult SetFields(string companyCode, DocumentTypes documentType, int documentId, byte propertyIndex, string fieldValue);

        IResult SetField(string companyCode, DocumentTypes documentType, List<string> fieldValues);
    }
}
