﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Uos.Domain
{
    public static class Staffer
    {
        public static IStafferStore Store
        {
            get { return DependencyResolver.Default.GetInstance<IStafferStore>(); }
        }

        public static Task<IResult> AnyAsync(string companyCode, string identity, string roleName = null)
        {
            return Result.Try(async () =>
            {
                var store = Store;

                if (store == null) return Result.Fail(() => Strings.RoleStoreResolveFailure);

                var value = await store.AnyAsync(companyCode, identity, roleName);

                return Result.Ok(value);
            });
        }

        public static Task<IResult> CreateStafferAsync(StafferEntity stafferEntity)
        {
            return Result.Try(async () =>
            {
                var store = Store;

                if (store == null) return Result.Fail(() => Strings.StafferStoreResolveFailure);

                stafferEntity = await store.CreateStafferAsync(stafferEntity);

                return Result.Ok(stafferEntity);
            });
        }

        public static Task<IResult> CreateStafferAsync(StafferEntity stafferEntity, params string[] roleNames)
        {
            return Result.Try(async () =>
            {
                var store = Store;

                if (store == null) return Result.Fail(() => Strings.StafferStoreResolveFailure);

                if (roleNames != null)
                {
                    stafferEntity.Roles = roleNames.Select(name => new StafferRoleEntity
                    {
                        EmailAddress = stafferEntity.EmailAddress,
                        RoleName = name
                    }).ToList();
                }

                stafferEntity = await store.CreateStafferAsync(stafferEntity);

                return Result.Ok(stafferEntity);
            });
        }

        public static Task<IResult> CreateStafferAsync(string companyCode, string emailAddress, params string[] roleNames)
        {
            return CreateStafferAsync(new StafferEntity
            {
                CompanyCode = companyCode,
                EmailAddress = emailAddress
            }, roleNames);
        }

        public static Task<IResult> DeleteStafferAsync(int userId)
        {
            return Result.Try(async () =>
            {
                var store = Store;

                if (store == null) return Result.Fail(() => Strings.StafferStoreResolveFailure);

                var stafferEntity = await store.DeleteStafferAsync(userId);

                return Result.Ok(stafferEntity);
            });
        }

        public static Task<IResult> DeleteStafferAsync(string companyCode, string emailAddress)
        {
            return Result.Try(async () =>
            {
                var store = Store;

                if (store == null) return Result.Fail(() => Strings.StafferStoreResolveFailure);

                var stafferEntity = await store.DeleteStafferAsync(companyCode, emailAddress);

                return Result.Ok(stafferEntity);
            });
        }

        public static Task<IResult> DeleteStafferRoleAsync(int roleId)
        {
            return Result.Try(async () =>
            {
                var store = Store;

                if (store == null) return Result.Fail(() => Strings.StafferStoreResolveFailure);

                var roleEntity = await store.DeleteStafferRoleAsync(roleId);

                return Result.Ok(roleEntity);
            });
        }

        public static Task<IResult> DeleteStafferRoleAsync(string companyCode, string emailAddress, string roleName)
        {
            return Result.Try(async () =>
            {
                var store = Store;

                if (store == null) return Result.Fail(() => Strings.StafferStoreResolveFailure);

                var roleEntity = await store.DeleteStafferRoleAsync(companyCode, emailAddress, roleName);

                return Result.Ok(roleEntity);
            });
        }

        public static Task<IResult> GetStafferAsync(int userId)
        {
            return Result.Try(async () =>
            {
                var store = Store;

                if (store == null) return Result.Fail(() => Strings.StafferStoreResolveFailure);

                var stafferEntity = await store.GetStafferAsync(userId);

                return Result.Ok(stafferEntity);
            });
        }

        public static Task<IResult> GetStafferAsync(string companyCode, string emailAddresss)
        {
            return Result.Try(async () =>
            {
                var store = Store;

                if (store == null) return Result.Fail(() => Strings.StafferStoreResolveFailure);

                var stafferEntity = await store.GetStafferAsync(companyCode, emailAddresss);

                return Result.Ok(stafferEntity);
            });
        }

        public static Task<IResult> GetStafferRoleNamesAsync(string startsWith)
        {
            return Result.Try(async () =>
            {
                var store = Store;

                if (store == null) return Result.Fail(() => Strings.RoleStoreResolveFailure);

                var values = await store.GetStafferRoleNamesAsync(startsWith);

                return Result.Ok(values);
            });
        }

        public static Task<IResult> GetStafferRoleNamesAsync(string companyCode, string emailAddress)
        {
            return Result.Try(async () =>
            {
                var store = Store;

                if (store == null) return Result.Fail(() => Strings.RoleStoreResolveFailure);

                var roleNames = await store.GetStafferRoleNamesAsync(companyCode, emailAddress: emailAddress);

                return Result.Ok(roleNames);
            });
        }

        public static Task<IResult> RemoveStafferAsync(int userId)
        {
            return Result.Try(async () =>
            {
                var store = Store;

                if (store == null) return Result.Fail(() => Strings.StafferStoreResolveFailure);

                var stafferEntity = await store.GetStafferAsync(userId);

                stafferEntity.RemovedOn = DateTime.UtcNow;

                await store.SaveChangesAsync();

                return Result.Ok(stafferEntity);
            });
        }

        public static Task<IResult> RemoveStafferAsync(string companyCode, string emailAddress)
        {
            return Result.Try(async () =>
            {
                var store = Store;

                if (store == null) return Result.Fail(() => Strings.StafferStoreResolveFailure);

                var stafferEntity = await store.GetStafferAsync(companyCode, emailAddress);

                stafferEntity.RemovedOn = DateTime.UtcNow;

                await store.SaveChangesAsync();

                return Result.Ok(stafferEntity);
            });
        }

        public static Task<IResult> RemoveStafferRoleAsync(int roleId)
        {
            return Result.Try(async () =>
            {
                var store = Store;

                if (store == null) return Result.Fail(() => Strings.StafferStoreResolveFailure);

                var roleEntity = await store.GetStafferRoleAsync(roleId);

                roleEntity.RemovedOn = DateTime.UtcNow;

                await store.SaveChangesAsync();

                return Result.Ok(roleEntity);
            });
        }

        public static Task<IResult> GetStafferRolesAsync(string companyCode, string emailAddress)
        {
            return Result.Try(async () =>
            {
                var store = Store;

                if (store == null) return Result.Fail(() => Strings.StafferStoreResolveFailure);

                var value = await store.GetStafferRolesAsync(companyCode);

                return Result.Ok<IDictionary<string, string>>(value);
            });
        }

        public static Task<IResult> RemoveStafferRoleAsync(string companyCode, string identity, string roleName)
        {
            return Result.Try(async () =>
            {
                var store = Store;

                if (store == null) return Result.Fail(() => Strings.StafferStoreResolveFailure);

                var roleEntity = await store.GetStafferRoleAsync(companyCode, identity, roleName);

                roleEntity.RemovedOn = DateTime.UtcNow;

                await store.SaveChangesAsync();

                return Result.Ok(roleEntity);
            });
        }
        
        public static Task<IResult> SetRolesAsync(StafferEntity staffer, IList<string> roleNames)
        {
            return Result.Try(async () =>
            {
                var store = Store;

                if (store == null) return Result.Fail(() => Strings.CompanyStoreResolveFailure);

                await store.SetRolesAsync(staffer, roleNames);

                return Result.Success;
            });
        }

        public static Task<IResult> UpdateStafferAsync(string companyCode, string identity, Func<StafferEntity, Task<bool>> patcher)
        {
            return Result.Try(async () =>
            {
                if (patcher == null) throw new ArgumentNullException("patcher");

                var store = Store;

                if (store == null) return Result.Fail(() => Strings.CompanyStoreResolveFailure);

                var value = await store.GetStafferAsync(companyCode, identity);

                if (value == null) return Result.Fail(Message.Create(MessageTypes.Error, () => Strings.CompanyEntityNotFound, companyCode));

                await patcher(value);

                await store.SaveChangesAsync();

                return Result.Ok(value);
            });
        }
    }
}
