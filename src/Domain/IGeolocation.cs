﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Domain
{
    public interface IGeolocation
    {
        Geocode Geocode { get; set; }

        PostalAddress PostalAddress { get; set; }
    }
}
