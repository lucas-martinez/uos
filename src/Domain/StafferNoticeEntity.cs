﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Uos.Domain;

namespace Uos.Domain
{
    [DataContract]
    public class StafferNoticeEntity
    {
        DateTime? _createdOn;

        /// <summary>
        /// 
        /// </summary>
        [IgnoreDataMember]
        public virtual CompanyEntity Company { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual string CompanyCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual DateTime CreatedOn
        {
            get { return _createdOn ?? (_createdOn = DateTime.UtcNow).Value; }
            set { _createdOn = null; }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual string Identity { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual DateTime? RemovedOn { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [IgnoreDataMember]
        public virtual int NoticeId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [IgnoreDataMember]
        public virtual StafferEntity Staffer { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual string Text { get; set; }
    }
}
