﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Transportation
{
    public static class Freight
    {
        public static IResult Create(FreightEntity entity)
        {
            return Result.Ok(new FreightEntity());
        }

        public static IResult Delete(int freightId)
        {
            return Result.Success;
        }

        public static IResult List(int? limit = 0, int? start = 0)
        {
            return Result.Ok(new FreightEntity());
        }

        public static IResult Read(int freightId)
        {
            return Result.Ok(new FreightEntity());
        }

        public static IResult Search(FreightEntity model, int? limit = 0, int? start = 0)
        {
            return Result.Ok(model, "Aww yeah, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content.", "Whenever you need to, be sure to use margin utilities to keep things nice and tidy.");
        }

        public static IResult Update(int freightId, FreightEntity entity)
        {
            return Result.Ok(new FreightEntity());
        }
    }
}
