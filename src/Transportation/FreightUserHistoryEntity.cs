﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Transportation
{
    public class FreightUserHistoryEntity
    {
        /// <summary>
        /// 
        /// </summary>
        public virtual DateTime? EditedOn { get; set; }


        public virtual FreightEntity Freight { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual int FreightId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual DateTime SeenOn { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public virtual string UserEmailAddress { get; set; }
    }
}
