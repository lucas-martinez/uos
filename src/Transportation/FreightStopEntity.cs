﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uos.Logistics;

namespace Uos.Transportation
{
    public class FreightStopEntity
    {
        /// <summary>
        /// 
        /// </summary>
        public virtual int FreightId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual DateTime? StopArrivedOn { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual DateTime? StopExpectedOn { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual decimal? StopDistance { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual byte StopIndex { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual LocationEntity StopLocation { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual int StopLocationId { get; set; }
    }
}
