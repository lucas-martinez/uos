﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Transportation
{
    [DataContract]
    public class CarrierEntity
    {
        DateTime? _createdOn;

        /// <summary>
        /// SCAC
        /// </summary>
        [DataMember]
        public virtual string CarrierCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [IgnoreDataMember]
        public virtual int CarrierId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual string CarrierName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [IgnoreDataMember]
        public virtual DateTime CreatedOn
        {
            get { return _createdOn ?? (_createdOn = DateTime.UtcNow).Value; }
            set { _createdOn = value; }
        }
    }
}
