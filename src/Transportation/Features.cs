﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Transportation
{
    /// <summary>
    /// Transportation Management features
    /// </summary>
    public class Features
    {
        /// <summary>
        /// Order Management
        /// </summary>
        public const string Order = @"TM-OM";

        /// <summary>
        /// Freight Planning & Tendering
        /// </summary>
        public const string Planning = @"TM-FP";

        /// <summary>
        /// Freight Execution & Monitoring
        /// </summary>
        public const string Execution = @"TM-FE";

        /// <summary>
        /// Freight Settlement
        /// </summary>
        public const string Settlement = @"TM-FS";
    }
}
