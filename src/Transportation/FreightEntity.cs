﻿using System;
using System.Runtime.Serialization;
using Uos.Domain;
using Uos.Logistics;

namespace Uos.Transportation
{
    [DataContract]
    public class FreightEntity
    {
        DateTime? _createdOn;

        /// <summary>
        /// 
        /// </summary>
        [IgnoreDataMember]
        public virtual CarrierEntity Carrier { get; set; }

        /// <summary>
        /// SCAC
        /// </summary>
        [DataMember]
        public virtual string CarrierCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual DateTime? DepartureExpectedOn { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual DateTime? DepartureExexutedOn { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual CompanyLocationEntity DepartureLocation { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual int DepartureLocationId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual decimal? DestinationDistance { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual CompanyLocationEntity DestinationLocation { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual int DestinarionLocationId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual DateTime? DestinationArrivedOn { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual DateTime? DestinationExpectedOn { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [IgnoreDataMember]
        public virtual CompanyEntity Company { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual string CompanyCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [IgnoreDataMember]
        public virtual DateTime CreatedOn
        {
            get { return _createdOn ?? (_createdOn = DateTime.UtcNow).Value; }
            set { _createdOn = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [IgnoreDataMember]
        public virtual int FreightId { get; set; }
    }
}
