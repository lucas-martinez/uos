﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;

namespace Uos.Web.Controllers
{
    using Models;
    using Domain;

    [Authorize, RoutePrefix("domain/companies")]
    public class CompaniesApiController : ApiController
    {
        //
        // GET: api/administration/companies
        [HttpGet, Route("")]
        public async Task<IHttpActionResult> GetCompanies(int? start, int? limit, string sortBy)
        {
            Expression<Func<CompanyEntity, string>> orderBy = e => e.CompanyCode;

            switch (sortBy)
            {
                case "name":
                    orderBy = e => e.CompanyName;
                    break;
            }
            
            var result = await Company.GetCompaniesAsync(start, limit, orderBy);

            return Ok(result);
        }

        //
        // GET: api/administration/companies
        [HttpGet, Route("")]
        public async Task<IHttpActionResult> GetCompanies(string startWith)
        {
            var result = await Company.GetCompaniesCodeNameAsync(startWith);

            if (!result.Succeeded) return Ok(result);

            var value = result.OfType<IDictionary<string, string>>().Single();

            result = Result.Ok(value.Select(o => new { CompanyCode = o.Key, CompanyName = o.Value }));

            return Ok(result);
        }

        //
        // GET: api/administration/companies/{code}
        [HttpGet, Route("{code}")]
        public async Task<IHttpActionResult> GetCompany(string code)
        {
            return Ok(await Company.GetCompanyAsync(code));
        }

        //
        // DELETE: api/administration/companies/{code}
        [HttpGet, Route("{code}")]
        public async Task<IHttpActionResult> DeleteCompany(string code)
        {
            return Ok(await Company.DeleteCompanyAsync(code));
        }

        //
        // PATCH|PUT: api/administration/companies/{code}
        [HttpPatch, HttpPut, Route("{code}")]
        public async Task<IHttpActionResult> UpdateCompany(string code, CompanyEntity delta)
        {
            return Ok(await Company.UpdateCompanyAsync(code, (entity) =>
            {
                return Task.FromResult(true);
            }));
        }
    }
}
