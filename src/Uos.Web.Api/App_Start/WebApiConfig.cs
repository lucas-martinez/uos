﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Http.OData.Extensions;
using System.Web.Http.OData.Builder;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;

namespace Uos.Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //http://odata.github.io/WebApi/
            var builder = new ODataConventionModelBuilder();

            //builder.EntitySet<Domain.CompanyEntity>("companies");

            //config.Routes.MapODataServiceRoute("ODataRoute", null, builder.GetEdmModel());

            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
