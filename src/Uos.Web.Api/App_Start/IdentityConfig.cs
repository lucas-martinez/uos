﻿using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Uos.Cognito;
using System.Security.Claims;

namespace Uos.Web
{
    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.

    public static class IdentityConfig
    {
        public static IdentityManager Create(IdentityFactoryOptions<IdentityManager> options, IOwinContext context)
        {
            var manager = new IdentityManager(context);
            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<IdentityEntity>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Configure validation logic for passwords
            /*manager.PasswordValidator = new Microsoft.AspNet.Identity.PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = true,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true,
            };*/
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider = new DataProtectorTokenProvider<IdentityEntity>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }

        // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
        /*public static async Task<ClaimsIdentity> GenerateUserIdentityAsync(this IdentityManager manager, IdentityEntity identity, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(identity, authenticationType);
            
            // Add custom user claims here
            return userIdentity;
        }*/
    }
}
