﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Enterprise
{
    /// <summary>
    /// Enterprise Controlling features
    /// </summary>
    public class Features
    {
        /// <summary>
        /// Executive Information System
        /// </summary>
        public const string Executive = @"EC-EIS";

        /// <summary>
        /// Management Consolidation
        /// </summary>
        public const string Management = @"EC-MC";

        /// <summary>
        /// Profit Center Accounting
        /// </summary>
        public const string Profit = @"EC-PCA";
    }
}
