﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos
{
    [DataContract]
    public class ListResult<TValue> : Result, IListResult<TValue>
    {
        readonly int _count;
        readonly int? _limit;
        readonly int _start;
        readonly IList<TValue> _values;

        public ListResult(IList<TValue> values, int? limit = null, int? start = null, int? count = null)
            : base(true)
        {
            _count = count ?? values.Count;
            _limit = limit;
            _start = start ?? 0;
            _values = values;
        }

        [DataMember(Name = "count")]
        public int Count
        {
            get { return _count; }
        }

        [DataMember(Name = "limit")]
        public int? Limit
        {
            get { return _limit; }
        }

        [DataMember(Name = "start")]
        public int Start
        {
            get { return _start; }
        }

        [DataMember(Name = "values")]
        public IList<TValue> Values
        {
            get { return _values; }
        }

        IEnumerator<TValue> IEnumerable<TValue>.GetEnumerator()
        {
            return _values.GetEnumerator();
        }

        public override IEnumerable<T> OfType<T>()
        {
            return _values.OfType<T>();
        }
    }
}
