﻿using System.Runtime.Serialization;

namespace Uos
{
    [DataContract]
    public enum MessageTypes
    {
        [EnumMember(Value = "error")]
        Error,

        [EnumMember(Value = "info")]
        Info,

        [EnumMember(Value = "success")]
        Success,

        [EnumMember(Value = "Warning")]
        Warning,
    }
}
