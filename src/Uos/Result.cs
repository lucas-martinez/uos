﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos
{
    [DataContract]
    public class Result : IResult
    {
        public static readonly IResult Success = new Result(true, new Message[0]);

        public static readonly IResult Failure = new Result(false, new Message[0]);

        public static readonly IResult NotImplemented = new Result(false, new Message[0]);

        private readonly bool _success;
        private readonly IList<Message> _messages;

        public Result()
            : this(true)
        {
        }

        public Result(params Message[] messages)
            : this(true, messages)
        {
        }

        public Result(IList<Message> messages)
            : this(true, messages)
        {
        }

        [DataMember(Name = "messages")]
        public IList<Message> Messages
        {
            get { return _messages; }
        }

        public Result(bool success, params string[] messages)
            : this(success, messages.Select(text => new Message(success ? MessageTypes.Success : MessageTypes.Error, text)).ToList())
        {
        }

        public Result(bool success, params Message[] messages)
            : this(success, messages.ToList())
        {
        }

        public Result(bool success, IList<Message> messages)
        {
            _success = success;
            _messages = messages;
        }

        public Result(bool success)
            : this(success, new List<Message>())
        {
        }
        

        [DataMember(Name = "success")]
        public bool Succeeded
        {
            get { return _success; }
        }

        public bool Has(Func<string> text)
        {
            return _messages.Any(m => m.Is(text));
        }

        public bool Has(Func<string> text, MessageTypes type)
        {
            return _messages.Any(m => m.Is(text, type));
        }

        public bool Is(Func<string> text)
        {
            return _messages.Count == 1 && _messages[0].Is(text);
        }

        public bool Is(Func<string> text, MessageTypes type)
        {
            return _messages.Count == 1 && _messages[0].Is(text, type);
        }

        public static IResult All(params IResult[] results)
        {
            var messages = new List<Message>();
            var success = true;

            foreach (var result in results)
            {
                messages.AddRange(result.Messages);
                success &= result.Succeeded;
            }

            return success && messages.Count.Equals(0) ? Success : new Result(success, messages.ToArray());
        }
        
        public static IResult Fail(Error error)
        {
            return new Result(false, error.Messages.Select(e => new Message(MessageTypes.Error, e)).ToArray());
        }

        public static IResult Fail(IEnumerable<string> errors)
        {
            return new Result(false, errors.Select(e => new Message(MessageTypes.Error, e)).ToList());
        }

        public static IResult Fail(params string[] messages)
        {
            return new Result(false, messages);
        }

        public static IResult Fail(params Message[] messages)
        {
            return new Result(false, messages);
        }

        public static IResult Fail(Func<string> text)
        {
            return new Result(false, Message.Create(MessageTypes.Error, text));
        }

        public static IResult Fail<TValue>(TValue value, Func<string> text)
        {
            return new Result<TValue>(false, value, Message.Create(MessageTypes.Error, text));
        }

        public static IResult Fail(Exception exception)
        {
            return new Result(Message.Create(exception));
        }

        public static IResult Fail(IList<Message> messages)
        {
            return new Result(false, messages);
        }

        public static IResult<TValue> Fail<TValue>(TValue value, IList<Message> messages)
        {
            return new Result<TValue>(false, value, messages);
        }

        public static IResult Ok(Func<string> text)
        {
            return new Result(true, Message.Create(MessageTypes.Success, text));
        }

        public static IListResult<TItem> Ok<TItem>(params TItem[] values)
        {
            return new ListResult<TItem>(values.ToList());
        }

        public static IListResult<TItem> Ok<TItem>(IList<TItem> values)
        {
            return new ListResult<TItem>(values);
        }
        
        public static IListResult<TItem> Ok<TItem>(IEnumerable<TItem> values)
        {
            return new ListResult<TItem>(values.ToList());
        }

        public static IResult<TResult> Ok<TResult>(TResult value)
        {
            return new Result<TResult>(value);
        }

        public static IResult<TValue> Ok<TValue>(TValue value, IList<Message> messages)
        {
            return new Result<TValue>(false, value, messages);
        }

        public static IResult<TValue> Ok<TValue>(TValue value, params string[] messages)
        {
            return new Result<TValue>(value, messages);
        }
        
        IEnumerator IEnumerable.GetEnumerator()
        {
            yield break;
        }

        public virtual IEnumerable<T> OfType<T>()
        {
            yield break;
        }

        public static IResult Try(Func<IResult> fn)
        {
            IResult result;

            try
            {
                result = fn();
            }
            catch(Exception ex)
            {
                result = Result.Fail(ex);
            }

            return result;
        }

        public static async Task<IResult> Try(Func<Task<IResult>> fn)
        {
            IResult result;

            try
            {
                result = await fn();
            }
            catch (Exception ex)
            {
                result = Result.Fail(ex);
            }

            return result;
        }
    }

    [DataContract]
    public class Result<TValue> : Result, IResult<TValue>
    {
        private readonly TValue _value;

        public Result(TValue value)
            : this(true, value, new List<Message>())
        {
        }

        public Result(TValue value, params string[] messages)
            : this(true, value, messages)
        {   
        }
        
        public Result(TValue value, IList<Message> messages)
            : this(true, value, messages)
        {
        }

        public Result(bool succeeded, TValue value, params string[] messages)
            : this(succeeded, value, messages.Select(text => new Message(succeeded ? MessageTypes.Success : MessageTypes.Error, text)).ToList())
        {
        }

        public Result(bool succeeded, TValue value, IList<Message> messages)
            : base(succeeded, messages)
        {
            _value = value;
        }

        [DataMember(Name = "value")]
        public TValue Value
        {
            get { return _value; }
        }

        IEnumerator<TValue> IEnumerable<TValue>.GetEnumerator()
        {
            yield return _value;

            yield break;
        }

        public override IEnumerable<T> OfType<T>()
        {
            object value = _value;

            if (value != null && value is T)
            {
                yield return (T)value;
            }

            yield break;
        }
    }
}
