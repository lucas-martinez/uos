﻿//using System.Diagnostics.Contracts;
using System.Globalization;
using System.IO;

namespace Uos.Storage
{
    [ContractClass(typeof(ResourceProviderContracts))]
    public interface IResourceProvider
    {
        Stream Get(string path, CultureInfo culture = null);
    }
}