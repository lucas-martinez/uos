﻿using System;
//using System.Diagnostics.Contracts;
using System.Globalization;
using System.IO;

namespace Uos.Storage
{
    [ContractClassFor(typeof (IResourceProvider))]
    internal abstract class ResourceProviderContracts : IResourceProvider
    {
        public Stream Get(string path, CultureInfo culture = null)
        {
            Contract.Requires<ArgumentException>(!string.IsNullOrEmpty(path));

            Contract.Ensures(Contract.Result<Stream>() != null);

            throw new NotImplementedException();
        }
    }
}