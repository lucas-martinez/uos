﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace Uos
{
    public static class Text
    {
        public static string Flatten(this string s, char separator)
        {
            var flat = new char[2 * s.Length];
            var pos = 0;
            var separeUpper = false;
            var separeNumber = false;
            var separeLower = false;

            for (int i = 0, count = s.Length; i < count; i++)
            {
                var c = s[i];

                if (char.IsLower(c))
                {
                    if (separeLower) flat[pos++] = separator;

                    flat[pos++] = c;

                    separeLower = false;
                    separeNumber = true;
                    separeUpper = true;
                }
                else if (char.IsNumber(c))
                {
                    if (separeNumber) flat[pos++] = separator;

                    flat[pos++] = c;

                    separeLower = true;
                    separeNumber = false;
                    separeUpper = true;
                }
                else if (char.IsUpper(c))
                {
                    if (separeUpper) flat[pos++] = separator;

                    flat[pos++] = char.ToLowerInvariant(c);

                    separeLower = false;
                    separeNumber = true;
                    separeUpper = true;
                }
            }

            return new string(flat, 0, pos);
        }

        public static string FormatWith(this string format, params object[] args)
        {
            return string.Format(format, args);
        }

        /// <summary>
        /// Removes duplications
        /// </summary>
        /// <param name="input"></param>
        /// <param name="pattern"></param>
        /// <param name="replacement"></param>
        /// <returns></returns>
        public static string Gsub(this string input, string pattern, string replacement)
        {
            var regex = new System.Text.RegularExpressions.Regex(pattern);
            return regex.Replace(input, replacement);
        }

        public static string Nullable(this string input)
        {
            if (string.IsNullOrEmpty(input)) return null;

            return input;
        }

        public static string NullableFormat(this string input, Func<string, string> formatter)
        {
            if (string.IsNullOrEmpty(input)) return null;

            return formatter(input);
        }

        public static string Plural(this string name)
        {
            if (name.EndsWith("x", StringComparison.InvariantCultureIgnoreCase)
                || name.EndsWith("ch", StringComparison.InvariantCultureIgnoreCase)
                || name.EndsWith("ss", StringComparison.InvariantCultureIgnoreCase))
            {
                return name + "es";
            }
            else if (name.EndsWith("y", StringComparison.InvariantCultureIgnoreCase))
            {
                return name.Substring(0, name.Length - 1) + "ies";
            }
            else if (!name.EndsWith("s"))
            {
                return name + "s";
            }
            return name;
        }

        public static string Singular(this string name)
        {
            if (name.EndsWith("es", StringComparison.InvariantCultureIgnoreCase))
            {
                string rest = name.Substring(0, name.Length - 2);
                if (rest.EndsWith("x", StringComparison.InvariantCultureIgnoreCase)
                    || name.EndsWith("ch", StringComparison.InvariantCultureIgnoreCase)
                    || name.EndsWith("ss", StringComparison.InvariantCultureIgnoreCase))
                {
                    return rest;
                }
            }
            if (name.EndsWith("ies", StringComparison.InvariantCultureIgnoreCase))
            {
                return name.Substring(0, name.Length - 3) + "y";
            }
            else if (name.EndsWith("s", StringComparison.InvariantCultureIgnoreCase)
                && !name.EndsWith("ss", StringComparison.InvariantCultureIgnoreCase))
            {
                return name.Substring(0, name.Length - 1);
            }
            return name;
        }

        public static string SplitWords(this string name)
        {
            StringBuilder sb = null;
            bool lastIsLower = char.IsLower(name[0]);
            for (int i = 0, n = name.Length; i < n; i++)
            {
                bool thisIsLower = char.IsLower(name[i]);
                if (lastIsLower && !thisIsLower)
                {
                    if (sb == null)
                    {
                        sb = new StringBuilder();
                        sb.Append(name, 0, i);
                    }
                    sb.Append(" ");
                }
                if (sb != null)
                {
                    sb.Append(name[i]);
                }
                lastIsLower = thisIsLower;
            }
            if (sb != null)
            {
                return sb.ToString();
            }
            return name;
        }

        public static string TrimStart(this string input, string trim, StringComparison comparisonType = StringComparison.Ordinal)
        {
            if (string.IsNullOrEmpty(input) || string.IsNullOrEmpty(trim)) return input;

            if (!string.Equals(input.Substring(0, trim.Length), trim, comparisonType)) return input;

            return input.Substring(trim.Length);
        }

        public static string TrimEnd(this string input, string trim, StringComparison comparisonType = StringComparison.Ordinal)
        {
            if (string.IsNullOrEmpty(input) || string.IsNullOrEmpty(trim)) return input;

            if (!string.Equals(input.Substring(input.Length - trim.Length), trim, comparisonType)) return input;

            return input.Substring(0, input.Length - trim.Length);
        }
    }
}
