﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos
{
    public class Contract
    {
        public static void Ensures(bool predicate)
        {
            Debug.Assert(predicate);
        }

        public static void Requires(bool predicate)
        {
            if (!predicate)
            {
                Debug.WriteLine("");
                throw new InvalidOperationException();
            }
        }

        public static void Requires<TException>(bool predicate)
            where TException : Exception, new()
        {
            if (!predicate)
            {
                Debug.WriteLine("");
                throw new TException();
            }
        }

        public static void Requires<TException>(bool predicate, string message)
            where TException : Exception, new()
        {
            if (!predicate)
            {
                Debug.WriteLine(message);
                throw new TException();
            }
        }

        public static object Result<T>()
        {
            return 0;
        }
    }
}
