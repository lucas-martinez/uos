﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos
{
    public class UserException : Exception
    {
        readonly Message _message;

        public UserException(string message)
            : base(message)
        {
            _message = new Message(MessageTypes.Error, message);

        }

        public UserException(Message message)
            : base(message)
        {
            _message = message;

        }

        public UserException(Func<string> text)
            : this(Uos.Message.Create(MessageTypes.Error, text))
        {   
        }
        
        public static implicit operator Message(UserException exception)
        {
            return exception != null ? exception._message : null;
        }
    }
}
