﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos
{
    [DataContract]
    public struct Activity
    {
        readonly string _action;
        readonly bool _api;
        readonly string _area;
        readonly string _controller;
        readonly string _feature;
        readonly bool _local;
        readonly string _name;
        readonly string[] _roles;
        readonly Uri _uri;

        public Activity(string name = null, string feature = null, string action = null, string controller = null, string area = null, Uri uri = null, bool local = true, bool api = false, string[] roles = null)
        {
            _action = action;
            _api = api;
            _area = area;
            _controller = controller;
            _feature = feature;
            _local = local;
            _name = name;
            _roles = roles;
            _uri = uri;
        }

        [DataMember(Name = "action")]
        public string Action
        {
            get { return _action; }
        }

        [DataMember(Name = "api")]
        public bool Api
        {
            get { return _api; }
        }

        [DataMember(Name = "area")]
        public string Area
        {
            get { return _area; }
        }

        [DataMember(Name = "controller")]
        public string Controller
        {
            get { return _controller; }
        }

        [DataMember(Name = "feature")]
        public string Feature
        {
            get { return _feature; }
        }

        [DataMember(Name = "local")]
        public bool IsLocalUrl
        {
            get { return _local; }
        }

        [DataMember(Name = "name")]
        public string Name
        {
            get { return _name; }
        }

        [DataMember(Name = "roles")]
        public string[] Roles
        {
            get { return _roles; }
        }

        [DataMember(Name = "url")]
        public string Url
        {
            get { return _uri != null ? (_local ? _uri.PathAndQuery : Convert.ToString(_uri)) : null; }
        }

        [IgnoreDataMember]
        public Uri Uri
        {
            get { return _uri; }
        }
    }
}
