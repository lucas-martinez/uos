﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Uos
{
    public static class Runtime
    {
        public static List<FieldInfo> GetConstantFields(this Type type)
        {
            FieldInfo[] fieldInfos = type.GetFields(BindingFlags.Public |
                 BindingFlags.Static | BindingFlags.FlattenHierarchy);

            return fieldInfos/*.Where(fi => fi.IsLiteral && !fi.IsInitOnly)*/.ToList();
        }

        public static List<object> GetConstantValues(this Type type)
        {
            return GetConstantFields(type).Select(field => field.GetValue(null)).ToList();
        }

        public static MemberInfo GetMember(this Expression expression)
        {
            if (expression == null)
            {
                throw new ArgumentException("The expression cannot be null.");
            }

            if (expression is LambdaExpression)
            {
                var lambdaExpression = (LambdaExpression)expression;
                return lambdaExpression.Body.GetMember();
            }

            if (expression is MemberExpression)
            {
                // Reference type property or field
                var memberExpression = (MemberExpression)expression;
                return memberExpression.Member;
            }

            if (expression is MethodCallExpression)
            {
                // Reference type method
                var methodCallExpression = (MethodCallExpression)expression;
                return methodCallExpression.Method;
            }

            if (expression is UnaryExpression)
            {
                // Property, field of method returning value type
                var unaryExpression = (UnaryExpression)expression;
                return GetMember(unaryExpression);
            }

            throw new ArgumentException("Invalid expression");
        }

        private static MemberInfo GetMember(UnaryExpression unaryExpression)
        {
            if (unaryExpression.Operand is MethodCallExpression)
            {
                var methodExpression = (MethodCallExpression)unaryExpression.Operand;
                return methodExpression.Method;
            }

            return ((MemberExpression)unaryExpression.Operand).Member;
        }

        public static string GetName<T>(this Expression<Func<T, object>> expression)
        {
            return GetName(expression);
        }

        public static string GetName(this Expression expression)
        {
            return GetMember(expression).Name;
        }

        public static object GetValue(this Expression expression)
        {
            return GetMember(expression).GetValue();
        }

        public static object GetValue(this MemberInfo member)
        {
            var property = member as PropertyInfo;

            return property != null ? property.GetValue(null) : null;
        }

        private static void CheckIsEnum<T>(bool withFlags)
            where T : struct
        {
            if (!typeof(T).IsEnum) throw new ArgumentException(string.Format("Type '{0}' is not an enum", typeof(T).FullName));

            if (withFlags && !Attribute.IsDefined(typeof(T), typeof(FlagsAttribute))) throw new ArgumentException(string.Format("Type '{0}' doesn't have the 'Flags' attribute", typeof(T).FullName));
        }

        public static bool IsFlagSet<T>(this T value, T flag) where T : struct
        {
            CheckIsEnum<T>(true);

            long lValue = Convert.ToInt64(value);

            long lFlag = Convert.ToInt64(flag);

            return (lValue & lFlag) != 0;
        }

    }
}
