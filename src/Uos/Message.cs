﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;

namespace Uos
{
    [DataContract]
    public class Message
    {
        public static string AnchorElementTemplate = "<a class=\"aler-link\" href=\"{1}\">{0}</a>";

        readonly string _name;
        readonly string _text;
        readonly MessageTypes _type;

        public Message(string text)
            : this(MessageTypes.Info, text)
        {
        }
        
        public Message(MessageTypes type, string text, string name = null)
        {
            _name = name;
            _text = text;
            _type = type;
        }
        
        [DataMember(Name = "name")]
        public string Name
        {
            get { return _name; }
        }

        [DataMember(Name = "text")]
        public string Text
        {
            get { return _text; }
        }

        [DataMember(Name = "type")]
        public MessageTypes Type { get { return _type; } }


        public bool Is(Func<string> text)
        {
            return text != null && _name == text.Method.Name.TrimStart("get_");
        }

        public bool Is(Func<string> text, MessageTypes type)
        {
            return _type == type && text != null && _name == text.Method.Name.TrimStart("get_");
        }

        public static Message Create(Exception ex)
        {
            return new Message(MessageTypes.Error, ex.Message, name: ex.GetType().Name.TrimEnd("Exception"));
        }

        public static Message Create(MessageTypes type, Func<string> fn)
        {
            var name = fn.Method.Name.TrimStart("get_");
            var text = fn();

            return new Message(type, text: text, name: name);
        }

        public static Message Create(MessageTypes type, Func<string> fn, params object[] values)
        {
            var name = fn.Method.Name.TrimStart("get_");
            var text = fn();
            
            return new Message(type, string.Format(text, values), name);
        }

        public static Message Create(MessageTypes type, Func<string> textGetter, Func<string> ctaGetter, string link)
        {
            var name = textGetter.Method.Name.TrimStart("get_");
            var text = textGetter();
            var cta = string.Format(AnchorElementTemplate, ctaGetter(), link);

            text = text.Contains("{0}") ? string.Format(text, cta) : string.Join(" ", text, cta);

            return new Message(type, text);
        }

        public static Message Create<T>(MessageTypes type, Func<string> textGetter, Func<string> ctaGetter, string link, params object[] values)
        {
            var name = textGetter.Method.Name.TrimStart("get_");
            var text = textGetter();
            var cta = string.Format(AnchorElementTemplate, ctaGetter(), link);
            
            text = string.Format(text, new object[] { cta }.Union(values));

            return new Message(type, text);
        }

        public override bool Equals(object obj)
        {
            var other = obj as Message;

            return other == this;
        }

        public override int GetHashCode()
        {
            if (_name != null) return _name.GetHashCode() ^ _type.GetHashCode();

            if (_text != null) return _text.GetHashCode() ^ _type.GetHashCode();

            return _type.GetHashCode();
        }

        public override string ToString()
        {
            return _text;
        }

        public static implicit operator string(Message message)
        {
            return message != null ? message.ToString() : null;
        }

        public static implicit operator Message(string text)
        {
            return new Message(MessageTypes.Info, text);
        }

        public static implicit operator Message(Exception ex)
        {
            return ex != null ? Message.Create(ex) : null;
        }

        public static implicit operator Message(Func<string> fn)
        {
            return fn != null ? Message.Create(MessageTypes.Info, fn) : null;
        }
        
        public static implicit operator List<Message>(Message model)
        {
            return new List<Message> { model };
        }

        public static bool operator ==(Message a, Message b)
        {
            // If both are null, or both are same instance, return true.
            if (System.Object.ReferenceEquals(a, b))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)a == null) || ((object)b == null))
            {
                return false;
            }

            // Return true if the fields match:
            return a.Text == b.Text && a.Type == b.Type;
        }

        public static bool operator !=(Message a, Message b)
        {
            return !(a == b);
        }
    }
}
