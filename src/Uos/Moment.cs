﻿using System;
using System.Globalization;

namespace Uos
{
    public static class Moment
    {
        static DateTime? _javaScriptZero;

        public static DateTime JavaScriptZero
        {
            get { return _javaScriptZero ?? (_javaScriptZero = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).Value; }
        }

        public static long ToUnixMoment(this DateTime date)
        {
            return (long)date.Subtract(JavaScriptZero).TotalMilliseconds;
        }

        public static long? ToUnixMoment(this DateTime? date)
        {
            return date.HasValue ? (long)date.Value.Subtract(JavaScriptZero).TotalMilliseconds : (long?)null;
        }

        public static byte GetWeekOfYear(this DateTime date)
        {
            DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
            var woy = dfi.Calendar.GetWeekOfYear(date, dfi.CalendarWeekRule, dfi.FirstDayOfWeek);
            return (byte)(woy < 0 ? 0 : woy > 255 ? 255 : woy);
        }

        public static DateTimeOffset ToLocalDateTimeOffset(this DateTime value, DateTimeKind kind)
        {
            if (value.Kind != kind) value = DateTime.SpecifyKind(value, kind);

            if (kind == DateTimeKind.Utc) value = value.ToLocalTime();

            return new DateTimeOffset(value);
        }

        public static DateTimeOffset ToUtcDateTimeOffset(this DateTime value, DateTimeKind kind)
        {
            if (value.Kind != kind) value = DateTime.SpecifyKind(value, kind);

            if (kind == DateTimeKind.Local) value = value.ToUniversalTime();

            return new DateTimeOffset(value);
        }
    }
}
