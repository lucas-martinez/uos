﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Uos
{
    public interface IResult : IEnumerable
    {
        IList<Message> Messages { get; }

        bool Succeeded { get; }

        bool Has(Func<string> text);

        bool Has(Func<string> text, MessageTypes type);

        bool Is(Func<string> text);

        bool Is(Func<string> text, MessageTypes type);

        IEnumerable<T> OfType<T>();
    }

    public interface IResult<TValue> : IResult, IEnumerable<TValue>
    {
        TValue Value { get; }
    }
}
