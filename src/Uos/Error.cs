﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos
{
    [DataContract]
    public class Error
    {
        [DataMember(Name = "messages")]
        public IList<string> Messages { get; set; }
    }
}
