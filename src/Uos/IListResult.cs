﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos
{
    public interface IListResult<TItem> : IResult, IEnumerable<TItem>
    {
        int Count { get; }

        int? Limit { get; }

        int Start { get; }

        IList<TItem> Values { get; }
    }
}
