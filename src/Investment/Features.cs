﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Investment
{
    /// <summary>
    /// Capital Investment Management features
    /// </summary>
    public class Features
    {
        /// <summary>
        /// Capital Investment Management (programs)
        /// </summary>
        public const string Capital = @"IM-";

        /// <summary>
        /// Tangible Fixed Assets (measures)
        /// </summary>
        public const string Fixed = @"IM-FA";
    }
}
