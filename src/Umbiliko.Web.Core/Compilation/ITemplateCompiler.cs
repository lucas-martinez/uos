﻿using System;
//using System.Diagnostics.Contracts;
using System.IO;
using System.Threading.Tasks;
using Uos.Web.Core.Contracts;

namespace Uos.Web.Core.Compilation
{
    using Assertion;

    [ContractClass(typeof(TemplateCompilerContracts))]
    internal interface ITemplateCompiler
    {
        bool EnsureNamespace(string @namespace);

        Type Compile(Stream razorTemplate);

        Type Compile(Stream razorTemplate, Type modelType);

        Task<Type> CompileAsync(Stream razorTemplate);

        Task<Type> CompileAsync(Stream razorTemplate, Type modelType);
    }
}