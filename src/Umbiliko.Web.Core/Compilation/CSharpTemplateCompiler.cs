﻿using System;
using System.IO;
using System.Threading.Tasks;
using RazorEngine;
using RazorEngine.Configuration;
using RazorEngine.Templating;

namespace Uos.Web.Core.Compilation
{
    internal class CSharpTemplateCompiler : BaseTemplateCompiler
    {
        public CSharpTemplateCompiler()
            : base(new TemplateService(new TemplateServiceConfiguration()
            {
                Language = Language.CSharp,
                BaseTemplateType = typeof(Template)
            }))
        {
        }
    }
}