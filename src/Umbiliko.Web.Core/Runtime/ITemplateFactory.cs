﻿using System;
//using System.Diagnostics.Contracts;
using Uos.Web.Core.Contracts;

namespace Uos.Web.Core.Runtime
{
    using Assertion;

    [ContractClass(typeof (TemplateFactoryContracts))]
    public interface ITemplateFactory
    {
        Template Create(Type templateType, TemplateContext templateContext);
    }
}