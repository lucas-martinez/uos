﻿//using System.Diagnostics.Contracts;
using Uos.Web.Core.Contracts;

namespace Uos.Web.Core.Rendering
{
    using Assertion;

    [ContractClass(typeof(RendererContracts<,>))]
    public interface IRenderer<in TTemplate, out TResult>
        where TTemplate : class
        where TResult : class
    {
        TResult Render(TTemplate template, object viewBag);
    }
}