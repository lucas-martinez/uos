﻿using System.IO;
using System.Threading;
using RazorEngine.Templating;

namespace Uos.Web.Core.Rendering
{
    public class StringRenderer : IRenderer<Template, string>
    {
        public string Render(Template template, object viewBag)
        {
            var previousCulture = Thread.CurrentThread.CurrentCulture;
            var previousUICulture = Thread.CurrentThread.CurrentUICulture;
            Thread.CurrentThread.CurrentCulture = template.Culture;
            Thread.CurrentThread.CurrentUICulture = template.Culture;
            using (var writer = new StringWriter())
            {
                template.SetData(null, new ObjectViewBag(viewBag));
                ((ITemplate)template).Run(new ExecuteContext(), writer);
                Thread.CurrentThread.CurrentCulture = previousCulture;
                Thread.CurrentThread.CurrentUICulture = previousUICulture;
                var result = writer.GetStringBuilder().ToString();
                return result;  
            }
        }
    }
}