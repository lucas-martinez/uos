﻿using System;
//using System.Diagnostics.Contracts;
using Uos.Web.Core.Contracts;

namespace Uos.Web.Core.Caching
{
    using Assertion;

    [ContractClass(typeof(TemplateCacheContracts<>))]
    public interface ITemplateCache<T>
    {
        bool ContainsKey(TemplateCacheKey key);

        bool Put(TemplateCacheKey key, T templateInfo, TimeSpan slidingExpiration);

        TemplateCacheItem<T> Get(TemplateCacheKey key);
    }
}