﻿namespace Uos.Web.Core.Caching
{
    public enum CachePolicy
    {
        Instance,
        Shared
    }
}