﻿namespace Uos.Web.Core
{
    public interface IExposingTemplate
    {
        void Run(ITemplateVisitor templateVisitor, object viewBag);
    }
}