﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Accounting
{
    /// <summary>
    /// Financial Accounting features
    /// </summary>
    public class Features
    {
        /// <summary>
        /// Asset Accounting
        /// </summary>
        public const string Asset = @"FI-AA";

        /// <summary>
        /// Consolidation
        /// </summary>
        public const string Consolidation = @"FI-LC";

        /// <summary>
        /// General Ledger Accounting
        /// </summary>
        public const string General = @"FI-GL";

        /// <summary>
        /// Accounts Payable
        /// </summary>
        public const string Payable = @"FI-AP";

        /// <summary>
        /// Accounts Receivable
        /// </summary>
        public const string Receivable = @"FI-AR";

        /// <summary>
        /// Special Purpose Ledger
        /// </summary>
        public const string Special = @"FI-SL";
    }
}
