﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Controlling
{
    /// <summary>
    /// Controlling features
    /// </summary>
    public class Features
    {
        /// <summary>
        /// Activity-Based Costing
        /// </summary>
        public const string Activity = @"CO-ABC";

        /// <summary>
        /// Sales and Profitability Analysis
        /// </summary>
        public const string Analysis = @"CO-PA";

        /// <summary>
        /// Overhead Cost Control
        /// </summary>
        public const string Overhead = @"CO-OM";

        /// <summary>
        /// Product Cost Controlling
        /// </summary>
        public const string Product = @"CO-PC";
    }
}
