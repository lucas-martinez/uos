﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Treasury
{
    /// <summary>
    /// Treasury features
    /// </summary>
    public class Features
    {
        /// <summary>
        /// Cash Management
        /// </summary>
        public const string Cash = @"TR-CM";

        /// <summary>
        /// Founds Management
        /// </summary>
        public const string Founds = @"TR-FM";

        /// <summary>
        /// Treasury Management
        /// </summary>
        public const string Management = @"TR-TM";

        /// <summary>
        /// Market Risk Management
        /// </summary>
        public const string Risk = @"TR-MRM";
    }
}
