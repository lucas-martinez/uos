﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Uos.Logistics
{
    public static class Location
    {
        public static ILocationStore Store
        {
            get { return DependencyResolver.Default.GetInstance<ILocationStore>(); }
        }

        public static Task<IResult> CreateLocationAsync(LocationEntity Location)
        {
            return Result.Try(async () =>
            {
                var store = Store;

                if (store == null) return Result.Fail(() => Strings.LocationStoreResolveFailure);

                var value = await store.CreateLocationAsync(Location);

                return Result.Ok(value);
            });
        }

        public static Task<IResult> DeleteLocationAsync(int id)
        {
            return Result.Try(async () =>
            {
                var store = Store;

                if (store == null) return Result.Fail(() => Strings.LocationStoreResolveFailure);

                var value = await store.GetLocationAsync(id);

                if (value == null) return Result.Fail(Message.Create(MessageTypes.Error, () => Strings.LocationEntityNotFound, id));

                return Result.Ok(value);
            });
        }

        public static Task<IResult> GetLocationsAsync(string companyCode, int? customerId, int? start, int? limit, Expression<Func<LocationEntity, string>> orderBy)
        {
            return Result.Try(async () =>
            {
                var store = Store;

                if (store == null) return Result.Fail(() => Strings.LocationStoreResolveFailure);

                var values = await store.GetLocationsAsync(companyCode, customerId, start, limit, orderBy);

                return Result.Ok(values);
            });
        }

        public static Task<IResult> GetLocationAsync(int id)
        {
            return Result.Try(async () =>
            {
                var store = Store;

                if (store == null) return Result.Fail(() => Strings.LocationStoreResolveFailure);

                var value = await store.GetLocationAsync(id);

                if (value == null) return Result.Fail(Message.Create(MessageTypes.Error, () => Strings.LocationEntityNotFound, id));

                return Result.Ok(value);
            });
        }

        public static Task<IResult> UpdateLocationAsync(int id, Func<LocationEntity, bool> patcher)
        {
            return Result.Try(async () =>
            {
                if (patcher == null) throw new ArgumentNullException("patcher");

                var store = Store;

                if (store == null) return Result.Fail(() => Strings.LocationStoreResolveFailure);

                var value = await store.GetLocationAsync(id);

                if (value == null) return Result.Fail(Message.Create(MessageTypes.Error, () => Strings.LocationEntityNotFound, id));

                patcher(value);

                await store.SaveChangesAsync();

                return Result.Ok(value);
            });
        }
    }
}
