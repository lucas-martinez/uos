﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Uos.Logistics
{
    public interface ILocationStore : IQueryable<LocationEntity>
    {
        Task<LocationEntity> CreateLocationAsync(LocationEntity entity);

        Task<LocationEntity> DeleteLocationAsync(int id);

        Task<LocationEntity> GetLocationAsync(int id);

        Task<IList<LocationEntity>> GetLocationsAsync(string companyCode, int? customerId, int? start, int? limit, Expression<Func<LocationEntity, string>> orderBy);

        Task SaveChangesAsync();
    }
}
