﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Uos.Domain;

namespace Uos.Logistics
{
    public class LocationEntity
    {
        DateTime? _createdOn;

        public virtual CompanyEntity Company { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public virtual string CompanyCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual DateTime CreatedOn
        {
            get { return _createdOn ?? (_createdOn = DateTime.UtcNow).Value; }
            set { _createdOn = null; }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual int? CustomerId { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public virtual string LocationCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual int LocationId { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public virtual string LocationName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual DateTime? RemovedOn { get; set; }
    }
}
