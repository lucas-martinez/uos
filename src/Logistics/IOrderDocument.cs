﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Logistics
{
    public interface IOrderDocument
    {
        string OrderNumber { get; }
    }
}
