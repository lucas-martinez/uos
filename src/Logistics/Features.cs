﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Logistics
{
    /// <summary>
    /// Logistics Applications features
    /// </summary>
    public class Features
    {
        /// <summary>
        /// Generail Logistics
        /// </summary>
        public const string General = @"LO";

        /// <summary>
        /// Enginineering Change Management
        /// </summary>
        public const string Change = @"LO-ECH";

        /// <summary>
        /// Sales and Distribution
        /// </summary>
        public const string Distribution = @"SD";

        /// <summary>
        /// Forecast
        /// </summary>
        public const string Forecast = @"LO-PR";

        /// <summary>
        /// Logistics Information System
        /// </summary>
        public const string Information = @"LO-LIS";

        /// <summary>
        /// Plant Maintenance
        /// </summary>
        public const string Maintenance = @"PM";

        /// <summary>
        /// Materials Management
        /// </summary>
        public const string Materials = @"MM";

        /// <summary>
        /// Logistics Master Data
        /// </summary>
        public const string Master = @"LO-MD";

        /// <summary>
        /// Products Planning
        /// </summary>
        public const string Products = @"PP";

        /// <summary>
        /// Projects System
        /// </summary>
        public const string Projects = @"PS";

        /// <summary>
        /// Quality Management
        /// </summary>
        public const string Quality = @"QM";

        /// <summary>
        /// Environment Health and Safety
        /// </summary>
        public const string Safety = @"LO-EHS";

        /// <summary>
        /// Transportation Management
        /// </summary>
        public const string Transportation = @"TM";

        /// <summary>
        /// Variant Configuration
        /// </summary>
        public const string Variant = @"LO-VC";
    }
}
