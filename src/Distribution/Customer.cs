﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Distribution
{
    /// <summary>
    /// Customer Master Data
    /// The customer master data contains the information about business transaction and how transactions are recorded and executed by the system. 
    /// A Master contains the information about the customers that an organization uses to do business with them.
    /// <seealso cref="http://www.tutorialspoint.com/sap_sd/sap_sd_customer_material_master_data.htm"/>
    /// </summary>
    public static class Customer
    {
        /// <summary>
        /// To change customer centrally.
        /// T-Code: XD02
        /// </summary>
        /// <returns></returns>
        public static IResult ChangeCustomer()
        {
            return Result.NotImplemented;
        }

        /// <summary>
        /// To create customer centrally.
        /// T-Code: XD01
        /// This Includes sales area in the customer master and data is stored in tables KNA1, KNB1 and KNVV.
        /// </summary>
        /// <returns></returns>
        public static IResult CreateCustomer()
        {
            return Result.NotImplemented;
        }

        /// <summary>
        /// To delete customer centrally.
        /// T-Code: XD06
        /// </summary>
        /// <returns></returns>
        public static IResult DeleteCustomer()
        {
            return Result.NotImplemented;
        }

        /// <summary>
        /// To display customer centrally.
        /// T-Code: XD03
        /// </summary>
        /// <returns></returns>
        public static IResult RetriveCustomer()
        {
            return Result.NotImplemented;
        }

        /// <summary>
        /// To change customer sales area.
        /// T-Code: VD02
        /// </summary>
        /// <returns></returns>
        public static IResult ChangeSalesArea()
        {
            return Result.NotImplemented;
        }

        /// <summary>
        /// To create customer sales area.
        /// T-Code: VD01
        /// This includes sales area & data, which will be stored in tables KNA1, KNB1 and KNVV and there is no company code data in this.
        /// </summary>
        /// <returns></returns>
        public static IResult CreateSalesArea()
        {
            return Result.NotImplemented;
        }
        
        /// <summary>
        /// To display customer sales area.
        /// T-Code: VD03
        /// </summary>
        /// <returns></returns>
        public static IResult RetriveSalesArea()
        {
            return Result.NotImplemented;
        }

        //3	
        //FD01,FD02,FD03

        //Used to create/change/display customer company code

        //4	
        //XD04

        //Display change documents

        //5	
        //XD05

        //Display change documentsUsed to block Customer − Global, order, delivery, billing, sales area, etc.
        
        //7	
        //XD07

        //Change Account Group

        //8	
        //VAP1

        //Create Contact Person
        
        /*** Creating a Customer Master Data ***
        * To create a customer master data, you need to use an Account group.
        */
    }
}
