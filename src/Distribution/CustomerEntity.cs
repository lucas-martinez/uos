﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Uos.Domain;

namespace Uos.Distribution
{
    /// <summary>
    /// Customer General Information in Sales and Distribution Master Data.
    /// Table: KNA1	(KUNNR)
    /// </summary>
    [DataContract]
    public class CustomerEntity
    {
        DateTime? _createdOn;

        /// <summary>
        /// 
        /// </summary>
        [IgnoreDataMember]
        public virtual CompanyEntity Company { get; set; }

        /// <summary>
        /// If the customer is a registered company this is that company ID.
        /// </summary>
        [DataMember]
        public virtual int? CompanyId { get; set; }

        /// <summary>
        /// Company Code for the company that owns this customer
        /// </summary>
        [DataMember]
        public virtual string CompanyCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual DateTime CreatedOn
        {
            get { return _createdOn ?? (_createdOn = DateTime.UtcNow).Value; }
            set { _createdOn = null; }
        }

        /// <summary>
        /// 
        /// </summary>
        [IgnoreDataMember]
        public virtual int CustomerId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual string CustomerName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual string CustomerNumber { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual DateTime? RemovedOn { get; set; }
    }
}
