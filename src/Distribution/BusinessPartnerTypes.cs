﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Distribution
{
    public class BusinessPartnerTypes
    {
        /// <summary>
        /// A business partner of type Organization represents a company. 
        /// For each company that is involved in a business process, you need a business partner of type Organization (for example, for the customer and for the supplier).
        /// </summary>
        public const string Organization = @"ORG";

        /// <summary>
        /// A business partner of type Person corresponds to a real person within a company. 
        /// It represents a specific user who works with the SAP SNC Web application. 
        /// A business partner of type Person is connected to the business partner of type Organization that represents the company. 
        /// Each user is assigned to one only business partner of type Organization. 
        /// You link a business partner of type Person to an Internet user and an SAP SNC system user.
        /// </summary>
        public const string Person = @"PER";
    }
}
