﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Uos.Distribution
{
    public interface ICustomerStore : IQueryable<CustomerEntity>
    {
        Task<CustomerEntity> CreateCustomerAsync(CustomerEntity entity);

        Task<CustomerEntity> DeleteCustomerAsync(int id);

        Task<CustomerEntity> GetCustomerAsync(int id);

        Task<IList<CustomerEntity>> GetCustomersAsync(string companyCode, int? start, int? limit, Expression<Func<CustomerEntity, string>> orderBy);

        Task<IDictionary<int, string>> GetCustomersAsync(string companyCode, string startsWith);

        Task SaveChangesAsync();
    }
}
