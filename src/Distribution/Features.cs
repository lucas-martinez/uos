﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Distribution
{
    /// <summary>
    /// Sales and Distribution features
    /// </summary>
    public class Features
    {
        /// <summary>
        /// Billing
        /// </summary>
        public const string Billing = @"SD-BIL";

        /// <summary>
        /// Foreign Treade
        /// </summary>
        public const string Foreign = @"SD-FTT";

        /// <summary>
        /// General Sales Functions
        /// </summary>
        public const string General = @"SD-GF";

        /// <summary>
        /// Sales Information System
        /// </summary>
        public const string Information = @"SD-SIS";

        /// <summary>
        /// Electronic Data Interchange
        /// </summary>
        public const string Interchange = @"SD-EDI";

        /// <summary>
        /// Master Data
        /// </summary>
        public const string Master = @"SD-MD";

        /// <summary>
        /// Sales
        /// </summary>
        public const string Sales = @"SD-SLS";

        /// <summary>
        /// Shipping
        /// </summary>
        public const string Shipping = @"SD-SHP";

        /// <summary>
        /// Sales Support
        /// </summary>
        public const string Support = @"SD-CAS";

        /// <summary>
        /// Transportation
        /// </summary>
        public const string Transportation = @"SD-TR";
    }
}
