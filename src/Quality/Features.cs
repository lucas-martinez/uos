﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Quality
{
    /// <summary>
    /// Quality Management features
    /// </summary>
    public class Features
    {
        /// <summary>
        /// Quality Certificates
        /// </summary>
        public const string Certificates = @"QM-CA";

        /// <summary>
        /// Quality Control
        /// </summary>
        public const string Control = @"QM-C";

        /// <summary>
        /// Inspection Processing
        /// </summary>
        public const string Inspection = @"QM-IM";

        /// <summary>
        /// Quality Notifications
        /// </summary>
        public const string Notifications = @"QM-QN";

        /// <summary>
        /// Planning Tools
        /// </summary>
        public const string Planning = @"QM-PT";
    }
}
