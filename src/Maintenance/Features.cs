﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Maintenance
{
    /// <summary>
    /// Plant Maintenance features
    /// </summary>
    public class Features
    {
        /// <summary>
        /// Equipment and Technical Objects
        /// </summary>
        public const string Equipment = @"PM-EQM";

        /// <summary>
        /// Plant Maintenance Information System
        /// </summary>
        public const string Information = @"PM-IS";

        /// <summary>
        /// Maintenance Order Management
        /// </summary>
        public const string Order = @"PM-WOC";

        /// <summary>
        /// Preventive Maintenance
        /// </summary>
        public const string Preventive = @"PM-PRM";

        /// <summary>
        /// Maintenance Projects
        /// </summary>
        public const string Projects = @"PM-PRO";

        /// <summary>
        /// Service Management
        /// </summary>
        public const string Service = @"PM-SMA";
    }
}
