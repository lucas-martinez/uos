﻿namespace Uos.Web
{
    internal class Conventions
    {
        public const string HtmlSectionName = "Html";

        public const string TextSectionName = "Text";
    }
}
