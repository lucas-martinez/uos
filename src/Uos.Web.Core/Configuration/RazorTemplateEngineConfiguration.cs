﻿using RazorEngine;

namespace Uos.Web.Configuration
{
    using Runtime;

    public class RazorTemplateEngineConfiguration : TemplateEngineConfiguration
    {
        private ITemplateFactory _templateFactory;

        public RazorTemplateEngineConfiguration(string baseDirectory)
            : base(baseDirectory)
        {
            TemplateFactory = new ReflectionTemplateFactory();
            CodeLanguage = Language.CSharp;
        }

        public ITemplateFactory TemplateFactory
        {
            get { return _templateFactory; }
            set { SetRequiredProperty(ref _templateFactory, value, "TemplateFactory"); }
        }

        public Language CodeLanguage { get; set; }
    }
}
