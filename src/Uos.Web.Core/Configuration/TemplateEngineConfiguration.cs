﻿using System;
using Uos.Web.Caching;

namespace Uos.Web.Configuration
{
    using Storage;

    public abstract class TemplateEngineConfiguration
    {
        private IResourceProvider _resourceProvider;

        protected TemplateEngineConfiguration(string baseDirectory)
        {
            ResourceProvider = new FileSystemResourceProvider(baseDirectory);
            CachePolicy = CachePolicy.Shared;
            CacheExpiration = TimeSpan.FromHours(4.0);
        }

        public IResourceProvider ResourceProvider
        {
            get { return _resourceProvider; }
            set { SetRequiredProperty(ref _resourceProvider, value, "ResourceProvider"); }
        }

        public CachePolicy CachePolicy { get; set; }

        public TimeSpan CacheExpiration { get; set; }

        protected static void SetRequiredProperty<T>(ref T property, T value, string propertyName)
            where T : class
        {
            if (value == null)
            {
                throw new ArgumentNullException(propertyName, "Property is required.");
            }
            property = value;
        }
    }
}