﻿namespace Uos.Web.Configuration
{
    public class RazorTemplateEngineBuilder : TemplateEngineBuilder<RazorTemplateEngineConfiguration>
    {
        protected override ITemplateEngine Build(RazorTemplateEngineConfiguration configuration)
        {
            return new RazorTemplateEngine(configuration);
        }

        protected override RazorTemplateEngineConfiguration CreateDefaultConfiguration(string baseDirectory)
        {
            return new RazorTemplateEngineConfiguration(baseDirectory);
        }
    }
}
