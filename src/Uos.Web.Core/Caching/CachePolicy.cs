﻿namespace Uos.Web.Caching
{
    public enum CachePolicy
    {
        Instance,
        Shared
    }
}