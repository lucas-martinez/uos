﻿using System;
//using System.Diagnostics.Contracts;
using Uos.Web.Contracts;

namespace Uos.Web.Caching
{
    [ContractClass(typeof(TemplateCacheContracts<>))]
    public interface ITemplateCache<T>
    {
        bool ContainsKey(TemplateCacheKey key);

        bool Put(TemplateCacheKey key, T templateInfo, TimeSpan slidingExpiration);

        TemplateCacheItem<T> Get(TemplateCacheKey key);
    }
}