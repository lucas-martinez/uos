﻿namespace Uos.Web
{
    public interface IExposingTemplate
    {
        void Run(ITemplateVisitor templateVisitor, object viewBag);
    }
}