﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;

namespace Uos.Web
{
    internal class EmailContext
    {
        public static implicit operator MailMessage(EmailContext context)
        {
            return context != null ? context._message : null;
        }

        readonly MailMessage _message = new MailMessage();

        public EmailContext()
        {
            BodyEncoding = Encoding.UTF8;
            BodyTransferEncoding = TransferEncoding.Base64;
            DeliveryNotificationOptions = DeliveryNotificationOptions.None;
            HeadersEncoding = Encoding.UTF8;
            LinkedResources = new Collection<LinkedResource>();
            Priority = MailPriority.Normal;
            SubjectEncoding = Encoding.UTF8;
        }

        public ICollection<Attachment> Attachments
        {
            get { return _message.Attachments; }
        }

        public MailAddressCollection Bcc
        {
            get { return _message.Bcc; }
        }

        public Encoding BodyEncoding
        {
            get { return _message.BodyEncoding; }
            set { _message.BodyEncoding = value; }
        }

        public TransferEncoding BodyTransferEncoding
        {
            get { return _message.BodyTransferEncoding; }
            set { _message.BodyTransferEncoding = value; }
        }

        public MailAddressCollection CC
        {
            get { return _message.CC; }
        }

        public DeliveryNotificationOptions DeliveryNotificationOptions { get; set; }

        public MailAddress From
        {
            get { return _message.From; }
            set { _message.From = value; }
        }

        public NameValueCollection Headers
        {
            get { return _message.Headers; }
        }

        public Encoding HeadersEncoding
        {
            get { return _message.HeadersEncoding; }
            set { _message.HeadersEncoding = value; }
        }

        public ICollection<LinkedResource> LinkedResources
        {
            get;
            private set;
        }

        public MailPriority Priority
        {
            get { return _message.Priority; }
            set { _message.Priority = value; }
        }

        public MailAddressCollection ReplyToList
        {
            get { return _message.ReplyToList; }
        }

        public MailAddress Sender
        {
            get { return _message.Sender; }
            set { _message.Sender = value; }
        }

        public string Subject
        {
            get { return _message.Subject; }
            set { _message.Subject = value; }
        }

        public Encoding SubjectEncoding
        {
            get { return _message.SubjectEncoding; }
            set { _message.SubjectEncoding = value; }
        }

        public MailAddressCollection To
        {
            get { return _message.To; }
        }
    }
}
