﻿using System.Collections.Generic;
//using System.Diagnostics.Contracts;
using System.Net.Mail;

namespace Uos.Web.Rendering
{
    internal static class RenderingExtensions
    {
        public static MailMessage Render(this EmailTemplate template, object viewBag)
        {
            Contract.Requires(template != null);

            var visitor = new EmailTemplateVisitor();

            var exposingTemplate = (IExposingTemplate)template;

            exposingTemplate.Run(visitor, viewBag);

            MailMessage message = template;

            visitor.CopyTo(message);
            
            if (template.LinkedResources.Count > 0 && message.AlternateViews.Count > 0)
            {
                message.AlternateViews[0].LinkedResources.CopyFrom(template.LinkedResources);
            }
             
            return message;
        }

        internal static void CopyTo<T>(this IEnumerable<T> source, ICollection<T> destination)
        {
            Contract.Requires(source != null);
            Contract.Requires(destination != null);

            foreach (var item in source)
            {
                destination.Add(item);
            }
        }

        internal static void CopyFrom<T>(this ICollection<T> destination, IEnumerable<T> source)
        {
            Contract.Requires(source != null);
            Contract.Requires(destination != null);

            source.CopyTo(destination);
        }
    }
}
