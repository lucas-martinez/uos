﻿//using System.Diagnostics.Contracts;
using Uos.Web.Contracts;

namespace Uos.Web.Rendering
{
    [ContractClass(typeof(RendererContracts<,>))]
    public interface IRenderer<in TTemplate, out TResult>
        where TTemplate : class
        where TResult : class
    {
        TResult Render(TTemplate template, object viewBag);
    }
}