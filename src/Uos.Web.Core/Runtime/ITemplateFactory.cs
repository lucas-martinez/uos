﻿using System;
//using System.Diagnostics.Contracts;
using Uos.Web.Contracts;

namespace Uos.Web.Runtime
{
    [ContractClass(typeof (TemplateFactoryContracts))]
    public interface ITemplateFactory
    {
        Template Create(Type templateType, TemplateContext templateContext);
    }
}