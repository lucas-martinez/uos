﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Uos.Web.Rendering;

namespace Uos.Web
{
    public static class Email
    {
        public static NameValueCollection Settings
        {
            get { return ConfigurationManager.AppSettings; }
        }

        public static SmtpClient Client
        {
            get
            {
                var client = new SmtpClient(Settings["smtp:Host"]);

                int port;
                if (int.TryParse(Settings["smtp:Port"], out port))
                    client.Port = port;

                bool enableSsl;
                if (bool.TryParse(Settings["smtp:EnableSsl"], out enableSsl))
                    client.EnableSsl = enableSsl;

                SmtpDeliveryMethod deliveryMethod;
                if (Enum.TryParse<SmtpDeliveryMethod>(Settings["smtp:DeliveryMethod"], out deliveryMethod))
                    client.DeliveryMethod = deliveryMethod;

                var userName = Settings["smtp:UserName"];
                var password = Settings["smtp:Password"];

                if (userName != null || password != null)
                {
                    client.UseDefaultCredentials = false;
                    client.Credentials = new NetworkCredential(userName, password);
                }

                return client;
            }
        }
        /*
        public static async Task<IResult> SendAsync<T>(string subject, string template, T model, params string[] recipients)
        {
            return await Result.Try(async () =>
            {
                var engine = DependencyResolver.Default.GetInstance<ITemplateEngine>();
                
                var message = await engine.RenderEmailAsync(template, model);

                message.Sender = new MailAddress("admin@umbiliko.com", "Lucas Martinez");
                message.Subject = subject;

                foreach (var recipient in recipients)
                {
                    message.To.Add(recipient);
                }

                Client.Send(message);

                return Result.Success;
            });
        }
        */
        public static async Task<IResult> SendAsync(string subject, string template, object viewBag, params string[] recipients)
        {
            return await Result.Try(async () =>
            {
                var engine = DependencyResolver.Default.GetInstance<ITemplateEngine>();

                IRenderer<EmailTemplate, MailMessage> renderer = new EmailRenderer();
                
                MailMessage message = await engine.RenderAsync(template, renderer, viewBag: viewBag);

                message.Sender = new MailAddress("admin@umbiliko.com", "Lucas Martinez");
                message.Subject = subject;

                foreach (var recipient in recipients)
                {
                    message.To.Add(recipient);
                }

                Client.Send(message);

                return Result.Success;
            });
        }
    }
}
