﻿namespace Uos.Web.Email
{
    internal class Conventions
    {
        public const string HtmlSectionName = "Html";

        public const string TextSectionName = "Text";
    }
}
