﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Cognito
{
    [DataContract]
    public class LoginEntity
    {
        DateTime? _createdOn;

        [DataMember]
        public virtual DateTime CreatedOn
        {
            get { return _createdOn ?? (_createdOn = DateTime.UtcNow).Value; }
            set { _createdOn = null; }
        }

        [IgnoreDataMember]
        public virtual IdentityEntity Identity { get; set; }

        [IgnoreDataMember]
        public virtual int LoginId { get; set; }

        [DataMember]
        public virtual string LoginProvider { get; set; }

        [DataMember]
        public virtual string ProviderKey { get; set; }

        [IgnoreDataMember]
        public virtual DateTime? RemovedOn { get; set; }

        [IgnoreDataMember]
        public virtual Guid UserId { get; set; }
    }
}
