﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Cognito
{
    public class PasswordStrengths
    {
        public const string None = @"N";

        public const string Weak =@"W";

        public const string Strong = @"S";
    }
}
