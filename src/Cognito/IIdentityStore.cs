﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Cognito
{   
    public interface IIdentityStore : IQueryable<IdentityEntity>
    {
        string CompanyCode { get; }

        Task<IdentityEntity> CreateIdentityAsync(IdentityEntity identity);

        Task<RoleEntity> CreateRoleAsync(RoleEntity role);

        Task DeleteIdentityAsync(IdentityEntity entity);

        Task DeleteRoleAsync(RoleEntity entity);

        Task<IdentityEntity> FindAsync(Guid userId);

        Task<IdentityEntity> FindAsync(string identifier);

        Task<RoleEntity> FindRoleAsync(Guid userId, string roleName);

        Task<RoleEntity> FindRoleAsync(string identifier, string roleName);

        Task<IList<IdentityEntity>> GetIdentitiesAsync(string startsWith);

        Task<IList<IdentityEntity>> GetIdentitiesAsync(int? start, int? limit, Expression<Func<IdentityEntity, string>> orderBy);

        Task<IList<RoleEntity>> GetRolesAsync(Guid userId);

        Task<IList<RoleEntity>> GetRolesAsync(string identifier);

        IQueryable<IdentityEntity> Include<T>(Expression<Func<IdentityEntity, T>> path);

        IQueryable<IdentityEntity> Include<T1, T2>(Expression<Func<IdentityEntity, T1>> path1, Expression<Func<IdentityEntity, T2>> path2);

        Task SaveChangesAsync();

        Task SetClaimsAsync(IdentityEntity identity, string claimType, IList<string> claimValues);

        Task SetClaimsAsync(IdentityEntity identity, IDictionary<string, string> claims);

        Task SetRolesAsync(IdentityEntity identity, IList<string> roleNames);
    }
}