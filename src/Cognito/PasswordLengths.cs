﻿namespace Uos.Cognito
{
    public class PasswordLengths
    {
        public const byte Empty = 0;

        public const byte Large = 16;

        public const byte Medium = 8;
    }
}
