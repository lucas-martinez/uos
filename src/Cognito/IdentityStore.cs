﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Threading.Tasks;
using Uos.Domain;

namespace Uos.Cognito
{
    public class IdentityStore : EntityStore<IdentityEntity>, IDisposable,
        IIdentityStore,
        IQueryableUserStore<IdentityEntity>,
        IUserClaimStore<IdentityEntity>,
        IUserEmailStore<IdentityEntity>,
        IUserLockoutStore<IdentityEntity, string>,
        IUserLoginStore<IdentityEntity>,
        IUserPasswordStore<IdentityEntity>,
        IUserPhoneNumberStore<IdentityEntity>,
        IUserRoleStore<IdentityEntity>,
        IUserSecurityStampStore<IdentityEntity>,
        IUserStore<IdentityEntity>,
        IUserTwoFactorStore<IdentityEntity, string>
    {
        readonly DbContext _dbContext;
        readonly IOwinContext _owinContext;
        
        public IdentityStore(IOwinContext owinContext)
            : this(owinContext, owinContext.Get<IdentityDbContext>())
        {
        }

        public IdentityStore(IOwinContext owinContext, DbContext dbContext)
            : base(dbContext)
        {
            _owinContext = owinContext;
            _dbContext = dbContext;
        }

        ~IdentityStore()
        {
            Dispose(false);
        }
        
        protected virtual void Dispose(bool disposing)
        {
        }
        
        #region - IIdentityStore -

        public string CompanyCode
        {
            get
            {
                var user = _owinContext.Authentication.User;

                string value = null;

                if (user != null)
                {
                    value = user.Claims.Actor().Select(c => c.Value).SingleOrDefault();
                }

                return value;
            }
        }

        public async Task<IdentityEntity> CreateIdentityAsync(IdentityEntity identity)
        {
            var entity = Set<IdentityEntity>().Add(identity);

            await SaveChangesAsync();

            return entity;
        }

        public async Task<RoleEntity> CreateRoleAsync(RoleEntity role)
        {
            var entity = Set<RoleEntity>().Add(role);

            await SaveChangesAsync();

            return entity;
        }

        public async Task DeleteIdentityAsync(IdentityEntity entity)
        {
            Set<IdentityEntity>().Remove(entity);

            await SaveChangesAsync();
        }

        public async Task DeleteRoleAsync(RoleEntity entity)
        {
            Set<RoleEntity>().Remove(entity);

            await SaveChangesAsync();
        }

        public virtual Task<IdentityEntity> FindAsync(Guid userId)
        {
            return Set<IdentityEntity>().FindAsync(userId);
        }

        public virtual Task<IdentityEntity> FindAsync(string identifier)
        {
            Guid userId;

            return Guid.TryParse(identifier, out userId) ? FindAsync(userId) : Set<IdentityEntity>().SingleOrDefaultAsync(e => e.EmailAddress == identifier);
        }

        public async Task<RoleEntity> FindRoleAsync(Guid userId, string roleName)
        {
            return await Set<RoleEntity>().SingleOrDefaultAsync(e => e.UserId == userId && e.RoleName == roleName);
        }

        public async Task<RoleEntity> FindRoleAsync(string identifier, string roleName)
        {
            Guid userId;

            return Guid.TryParse(identifier, out userId) ? await FindRoleAsync(userId, roleName) : await Set<RoleEntity>().SingleOrDefaultAsync(e => e.Identity.EmailAddress == identifier && e.RoleName == roleName);
        }

        public async Task<IList<IdentityEntity>> GetIdentitiesAsync(string startsWith)
        {
            if (string.IsNullOrEmpty(startsWith))
            {
                return Array.Empty<IdentityEntity>();
            }

            return await Set<IdentityEntity>().Where(e => e.RemovedOn == null)
                .OrderBy(e => e.EmailAddress)
                .Where(e => e.EmailAddress.StartsWith(startsWith) || e.FullName.StartsWith(startsWith) || e.PhoneNumber.StartsWith(startsWith))
                .ToListAsync();
        }

        public async Task<IList<IdentityEntity>> GetIdentitiesAsync(int? start, int? limit, Expression<Func<IdentityEntity, string>> orderBy)
        {
            IQueryable<IdentityEntity> source = Set<IdentityEntity>().OrderBy(e => e.EmailAddress);

            if (start.HasValue) source = source.Skip(start.Value);

            if (limit.HasValue) source = source.Take(limit.Value);

            return await source.ToListAsync();
        }

        public async Task<IList<RoleEntity>> GetRolesAsync(Guid userId)
        {
            return await Set<RoleEntity>().Where(e => e.UserId == userId).OrderBy(e => e.RoleName).ToListAsync();
        }

        public async Task<IList<RoleEntity>> GetRolesAsync(string identifier)
        {
            Guid userId;

            return Guid.TryParse(identifier, out userId) ? await GetRolesAsync(userId) : await Set<RoleEntity>().Where(e => e.Identity.EmailAddress == identifier).OrderBy(e => e.RoleName).ToListAsync();
        }

        public async Task<IList<string>> GetRoleNamesAsync(Guid userId)
        {
            return await Set<RoleEntity>().Where(e => e.UserId == userId).OrderBy(e => e.RoleName).Select(e => e.RoleName).ToListAsync();
        }

        public async Task<IList<string>> GetRoleNamesAsync(string identifier)
        {
            Guid userId;

            return Guid.TryParse(identifier, out userId) ? await GetRoleNamesAsync(userId) : await Set<RoleEntity>().Where(e => e.Identity.EmailAddress == identifier).OrderBy(e => e.RoleName).Select(e => e.RoleName).ToListAsync();
        }

        public Task<bool> HasRoleAsync(Guid userId, string roleName)
        {
            return Set<RoleEntity>().AnyAsync(e => e.UserId == userId && e.RoleName == roleName);
        }

        public async Task<bool> HasRoleAsync(string identifier, string roleName)
        {
            Guid userId;

            return Guid.TryParse(identifier, out userId) ? await HasRoleAsync(userId, roleName) : await Set<RoleEntity>().AnyAsync(e => e.Identity.EmailAddress == identifier && e.RoleName == roleName);
        }
        
        public async Task SetClaimsAsync(Guid userId, IEnumerable<Claim> claims)
        {
            var dbSet = Set<ClaimEntity>();

            var values = claims.Compact().ToList();

            var types = values.Select(c => c.Type).Distinct();

            var issuers = values.Select(c => c.Issuer).Distinct();

            var cache = dbSet.Where(e => e.UserId == userId && types.Contains(e.ClaimType) && issuers.Contains(e.Issuer)).ToList();

            var issueUtc = DateTime.UtcNow;

            var all = claims.Compact().Select(c => new ClaimEntity
            {
                CreatedOn = issueUtc,
                Issuer = c.OriginalIssuer ?? c.Issuer,
                ClaimType = c.Type,
                ClaimValue = c.Value,
                UserId = userId
            });

            await SaveChangesAsync();
        }

        public async Task SetClaimsAsync(IdentityEntity identity, string claimType, IList<string> claimValues)
        {
            if (identity == null) throw new ArgumentNullException("identity");

            var dbSet = Set<ClaimEntity>();

            if (identity == null)
            {
                throw new UserException(() => Strings.IdentityEntityNotFound);
            }

            var claims = await dbSet.Where(e => e.UserId == identity.UserId && e.ClaimType == claimType).ToListAsync();

            foreach (var claim in claims.Where(e => e.RemovedOn != null && !claimValues.Contains(e.ClaimValue)))
            {
                claim.RemovedOn = DateTime.UtcNow;
            }

            foreach (var claim in claims.Where(e => e.RemovedOn == null && claimValues.Contains(e.ClaimValue)))
            {
                claim.RemovedOn = null;
            }

            dbSet.AddRange(claimValues.Where(value => !claims.Select(e => e.ClaimValue).Contains(value))
                .Select(value => new ClaimEntity
                {
                    ClaimType = claimType,
                    ClaimValue = value,
                    UserId = identity.UserId
                }));

            await SaveChangesAsync();
        }
        public async Task SetClaimsAsync(IdentityEntity identity, IDictionary<string, string> claims)
        {
            if (identity == null) throw new ArgumentNullException("identity");

            var dbSet = Set<ClaimEntity>();

            if (identity == null)
            {
                throw new UserException(() => Strings.IdentityEntityNotFound);
            }
            
            var entities = await dbSet.Where(e => e.UserId == identity.UserId && e.Issuer == Cognito.Defaults.ClaimsIssuer && claims.Keys.Contains(e.ClaimType)).ToListAsync();

            foreach (var claim in claims)
            {
                var entity = entities.SingleOrDefault(e => e.ClaimType == claim.Key);

                if (claim.Value == null && entity != null)
                {
                    entity.RemovedOn = DateTime.UtcNow;
                }
                else if (claim.Value != null && entity == null)
                {
                    dbSet.Add(new ClaimEntity
                    {
                        ClaimType = claim.Key,
                        ClaimValue = claim.Value,
                        Issuer = Cognito.Defaults.ClaimsIssuer,
                        UserId = identity.UserId
                    });
                }
                else if (claim.Value != null && entity != null)
                {
                    entity.ClaimValue = claim.Value;
                    entity.RemovedOn = null;
                }
            }

            await SaveChangesAsync();
        }
        public async Task SetRolesAsync(IdentityEntity identity, IList<string> roleNames)
        {
            if (identity == null) throw new ArgumentNullException("identity");
            
            var dbSet = Set<RoleEntity>();
            
            if (identity == null)
            {
                throw new UserException(() => Strings.IdentityEntityNotFound);
            }

            var roles = await dbSet.Where(e => e.UserId == identity.UserId).ToListAsync();

            foreach (var role in roles.Where(e => e.RemovedOn == null && !roleNames.Contains(e.RoleName)))
            {
                role.RemovedOn = DateTime.UtcNow;
            }

            foreach (var role in roles.Where(e => e.RemovedOn != null && roleNames.Contains(e.RoleName)))
            {
                role.RemovedOn = null;
            }
            
            dbSet.AddRange(roleNames.Where(name => !roles.Select(e => e.RoleName).Contains(name))
                .Select(name => new RoleEntity
                {
                    RoleName = name,
                    UserId = identity.UserId
                }));

            await SaveChangesAsync();
        }

        #endregion - IIdentityStore -

        #region - IDisposable -

        void IDisposable.Dispose()
        {
            Dispose(true);
        }

        #endregion - IDisposable -

        #region - IQueryableUserStore<IdentityEntity, string> -

        IQueryable<IdentityEntity> IQueryableUserStore<IdentityEntity, string>.Users
        {
            get { return Set<IdentityEntity>(); }
        }

        #endregion - IQueryableUserStore<IdentityEntity, string> -

        #region - IUserEmailStore<IdentityEntity, string> -

        Task<IdentityEntity> IUserEmailStore<IdentityEntity, string>.FindByEmailAsync(string email)
        {
            return FindAsync(email);
        }

        Task<string> IUserEmailStore<IdentityEntity, string>.GetEmailAsync(IdentityEntity identity)
        {
            return Task.FromResult(identity.EmailAddress);
        }

        Task<bool> IUserEmailStore<IdentityEntity, string>.GetEmailConfirmedAsync(IdentityEntity identity)
        {
            return Task.FromResult(identity.EmailConfirmed);
        }

        async Task IUserEmailStore<IdentityEntity, string>.SetEmailAsync(IdentityEntity identity, string email)
        {
            identity.EmailAddress = email;

            await SaveChangesAsync();
        }

        async Task IUserEmailStore<IdentityEntity, string>.SetEmailConfirmedAsync(IdentityEntity identity, bool confirmed)
        {
            identity.EmailConfirmed = confirmed;

            await SaveChangesAsync();
        }

        #endregion - IUserEmailStore<IdentityEntity, string> -

        #region - IUserClaimStore<IdentityEntity, string> -

        async Task IUserClaimStore<IdentityEntity, string>.AddClaimAsync(IdentityEntity identity, Claim claim)
        {
            var dbSet = Set<ClaimEntity>();

            var entity = dbSet.Create();

            entity.Issuer = claim.OriginalIssuer ?? claim.Issuer ?? "I";
            entity.ClaimType = claim.Type;
            entity.ClaimValue = claim.Value;
            entity.UserId = entity.UserId;

            dbSet.Add(entity);

            await SaveChangesAsync();
        }

        async Task<IList<Claim>> IUserClaimStore<IdentityEntity, string>.GetClaimsAsync(IdentityEntity identity)
        {
            var claims = await Set<ClaimEntity>()
                .Where(c => c.UserId == identity.UserId)
                .Select(c => new
                {
                    Issuer = c.Issuer,
                    Type = c.ClaimType,
                    Value = c.ClaimValue
                })
                .ToListAsync();

            return claims.Select(c => new Claim(type: c.Type, value: c.Value, valueType: string.Empty, issuer: c.Issuer)).Expand().ToList();
        }

        async Task IUserClaimStore<IdentityEntity, string>.RemoveClaimAsync(IdentityEntity identity, Claim claim)
        {
            var dbSet = Set<ClaimEntity>();

            var entity = await dbSet.SingleOrDefaultAsync(c => c.UserId == identity.UserId && c.ClaimType == claim.Type && c.ClaimValue == claim.Value);

            if (entity != null)
            {
                dbSet.Remove(entity);

                await SaveChangesAsync();
            }
        }

        #endregion - IUserClaimStore<IdentityEntity, string> -

        #region - IUserLockoutStore<IdentityEntity, string> -

        Task<int> IUserLockoutStore<IdentityEntity, string>.GetAccessFailedCountAsync(IdentityEntity identity)
        {
            return Task.FromResult(identity.AccessFailedCount);
        }

        Task<bool> IUserLockoutStore<IdentityEntity, string>.GetLockoutEnabledAsync(IdentityEntity identity)
        {
            return Task.FromResult(identity.LockoutEnabled);
        }

        Task<DateTimeOffset> IUserLockoutStore<IdentityEntity, string>.GetLockoutEndDateAsync(IdentityEntity identity)
        {
            var dateUtc = identity.LockoutEndDateUtc;

            var date = dateUtc.HasValue ? dateUtc.Value.ToLocalDateTimeOffset(DateTimeKind.Local) : DateTimeOffset.MinValue;

            return Task.FromResult(date);
        }

        async Task<int> IUserLockoutStore<IdentityEntity, string>.IncrementAccessFailedCountAsync(IdentityEntity identity)
        {
            identity.AccessFailedCount++;

            await SaveChangesAsync();

            return identity.AccessFailedCount;
        }

        async Task IUserLockoutStore<IdentityEntity, string>.ResetAccessFailedCountAsync(IdentityEntity identity)
        {
            identity.AccessFailedCount = 0;

            identity.LockoutEndDateUtc = null;

            await SaveChangesAsync();
        }

        async Task IUserLockoutStore<IdentityEntity, string>.SetLockoutEnabledAsync(IdentityEntity identity, bool enabled)
        {
            identity.LockoutEnabled = enabled;

            await SaveChangesAsync();
        }

        async Task IUserLockoutStore<IdentityEntity, string>.SetLockoutEndDateAsync(IdentityEntity identity, DateTimeOffset lockoutEnd)
        {
            identity.LockoutEndDateUtc = lockoutEnd.UtcDateTime;

            await SaveChangesAsync();
        }

        #endregion - IUserLockoutStore<IdentityEntity, string> -

        #region - IUserLoginStore<IdentityEntity, string> -

        async Task IUserLoginStore<IdentityEntity, string>.AddLoginAsync(IdentityEntity identity, UserLoginInfo login)
        {
            var dbSet = Set<LoginEntity>();

            var entity = dbSet.Create();

            entity.LoginProvider = login.LoginProvider;
            entity.ProviderKey = login.ProviderKey;
            entity.UserId = identity.UserId;

            dbSet.Add(entity);

            await SaveChangesAsync();
        }

        Task<IdentityEntity> IUserLoginStore<IdentityEntity, string>.FindAsync(UserLoginInfo login)
        {
            var query = (from id in Set<IdentityEntity>()
                         join log in Set<LoginEntity>() on id.UserId equals log.UserId
                         where log.LoginProvider == login.LoginProvider && log.ProviderKey == login.ProviderKey
                         select id);

            return query.FirstOrDefaultAsync();
        }

        async Task<IList<UserLoginInfo>> IUserLoginStore<IdentityEntity, string>.GetLoginsAsync(IdentityEntity identity)
        {
            var logins = await Set<LoginEntity>()
                .Where(l => l.UserId == identity.UserId)
                .Select(l => new { l.LoginProvider, l.ProviderKey })
                .ToListAsync();

            return logins.Select(l => new UserLoginInfo(loginProvider: l.LoginProvider, providerKey: l.ProviderKey)).ToList();
        }

        async Task IUserLoginStore<IdentityEntity, string>.RemoveLoginAsync(IdentityEntity identity, UserLoginInfo login)
        {
            var dbSet = Set<LoginEntity>();

            var entities = await dbSet.Where(e => e.UserId == identity.UserId && e.LoginProvider == login.LoginProvider && e.ProviderKey == login.ProviderKey).ToListAsync();

            foreach (var entity in entities)
            {
                dbSet.Remove(entity);
            }

            await SaveChangesAsync();
        }

        #endregion - IUserLoginStore<IdentityEntity, string> -

        #region - IUserPasswordStore<IdentityEntity, string> -

        Task<string> IUserPasswordStore<IdentityEntity, string>.GetPasswordHashAsync(IdentityEntity identity)
        {
            return Task.FromResult(identity.PasswordHash);
        }

        Task<bool> IUserPasswordStore<IdentityEntity, string>.HasPasswordAsync(IdentityEntity identity)
        {
            return Task.FromResult(!string.IsNullOrEmpty(identity.PasswordHash));
        }

        async Task IUserPasswordStore<IdentityEntity, string>.SetPasswordHashAsync(IdentityEntity identity, string passwordHash)
        {
            identity.PasswordHash = passwordHash;

            await SaveChangesAsync();
        }

        #endregion - IUserPasswordStore<IdentityEntity, string> -

        #region - IUserPhoneNumberStore<IdentityEntity, string> -

        Task<string> IUserPhoneNumberStore<IdentityEntity, string>.GetPhoneNumberAsync(IdentityEntity identity)
        {
            return Task.FromResult(identity.PhoneNumber);
        }

        Task<bool> IUserPhoneNumberStore<IdentityEntity, string>.GetPhoneNumberConfirmedAsync(IdentityEntity identity)
        {
            return Task.FromResult(identity.PhoneConfirmed);
        }

        async Task IUserPhoneNumberStore<IdentityEntity, string>.SetPhoneNumberAsync(IdentityEntity identity, string phoneNumber)
        {
            identity.PhoneNumber = phoneNumber;

            await SaveChangesAsync();
        }

        async Task IUserPhoneNumberStore<IdentityEntity, string>.SetPhoneNumberConfirmedAsync(IdentityEntity identity, bool confirmed)
        {
            identity.PhoneConfirmed = confirmed;

            await SaveChangesAsync();
        }

        #endregion - IUserPhoneNumberStore<IdentityEntity, string> -

        #region - IUserSecurityStampStore<IdentityEntity, string> -

        Task<string> IUserSecurityStampStore<IdentityEntity, string>.GetSecurityStampAsync(IdentityEntity identity)
        {
            return Task.FromResult(identity.SecurityStamp);
        }

        async Task IUserSecurityStampStore<IdentityEntity, string>.SetSecurityStampAsync(IdentityEntity identity, string stamp)
        {
            identity.SecurityStamp = stamp;

            await SaveChangesAsync();
        }

        #endregion - IUserSecurityStampStore<IdentityEntity, string> -

        #region - IUserRoleStore<IdentityEntity, string> -

        async Task IUserRoleStore<IdentityEntity, string>.AddToRoleAsync(IdentityEntity identity, string roleName)
        {
            await CreateRoleAsync(new RoleEntity
            {
                Identity = identity,
                RoleName = roleName,
                UserId = identity.UserId
            });
        }

        async Task<IList<string>> IUserRoleStore<IdentityEntity, string>.GetRolesAsync(IdentityEntity identity)
        {
            var systemRoleNames = (await GetRoleNamesAsync(identity.UserId));

            var companyCode = CompanyCode;

            if (companyCode == null) return systemRoleNames;
            
            var store = Uos.Domain.Staffer.Store;

            if (store == null) return systemRoleNames;

            var roleNames = systemRoleNames.Union(await store.GetStafferRoleNamesAsync(companyCode, emailAddress: identity.EmailAddress)).ToList();

            roleNames.Sort();

            return roleNames;
        }

        async Task<bool> IUserRoleStore<IdentityEntity, string>.IsInRoleAsync(IdentityEntity identity, string roleName)
        {
            if (await HasRoleAsync(identity.UserId, roleName)) return true;

            var companyCode = CompanyCode;

            if (companyCode == null) return false;

            var store = Staffer.Store;

            if (store == null) return false;

            return await store.AnyAsync(companyCode, emailAddress: identity.EmailAddress, roleName: roleName);
        }

        async Task IUserRoleStore<IdentityEntity, string>.RemoveFromRoleAsync(IdentityEntity identity, string roleName)
        {
            var systemRole = await FindRoleAsync(identity.UserId, roleName);

            if (systemRole != null)
            {
                await DeleteRoleAsync(systemRole);
            }
            
            var companyCode = CompanyCode;

            if (companyCode == null) return;

            var store = Staffer.Store;

            if (store == null) return;

            await store.DeleteStafferRoleAsync(companyCode, emailAddress: identity.EmailAddress, roleName: roleName);
        }

        #endregion - IUserRoleStore<IdentityEntity, string> -

        #region - IUserStore<IdentityEntity, string> -

        async Task IUserStore<IdentityEntity, string>.CreateAsync(IdentityEntity identity)
        {
            Set<IdentityEntity>().Add(identity);

            await SaveChangesAsync();
        }

        async Task IUserStore<IdentityEntity, string>.DeleteAsync(IdentityEntity identity)
        {
            Set<IdentityEntity>().Remove(identity);

            await SaveChangesAsync();
        }

        Task<IdentityEntity> IUserStore<IdentityEntity, string>.FindByIdAsync(string userId)
        {
            return FindAsync(userId);
        }

        Task<IdentityEntity> IUserStore<IdentityEntity, string>.FindByNameAsync(string name)
        {
            return FindAsync(name);
        }

        async Task IUserStore<IdentityEntity, string>.UpdateAsync(IdentityEntity identity)
        {
            await SaveChangesAsync();
        }

        #endregion - IUserStore<IdentityEntity, string> -

        #region - IUserTwoFactorStore<IdentityEntity, string> -

        Task<bool> IUserTwoFactorStore<IdentityEntity, string>.GetTwoFactorEnabledAsync(IdentityEntity identity)
        {
            return Task.FromResult(identity.TwoFactorEnabled);
        }

        async Task IUserTwoFactorStore<IdentityEntity, string>.SetTwoFactorEnabledAsync(IdentityEntity identity, bool enabled)
        {
            identity.TwoFactorEnabled = enabled;

            await SaveChangesAsync();
        }

        #endregion - IUserTwoFactorStore<IdentityEntity, string> -
    }
}
