﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Cognito
{
    internal static class InternalClaimTypes
    {
        public const string AccessFailedCount = @"http://schemas.microsoft.com/accesscontrolservice/2016/01/claims/accessfailedcount";

        public const string AccessToken = @"http://schemas.microsoft.com/accesscontrolservice/2016/01/claims/accesstoken";

        public const string CompanyCode = @"http://schemas.microsoft.com/accesscontrolservice/2016/01/claims/companycode";

        public const string CompanyName = @"http://schemas.microsoft.com/accesscontrolservice/2016/01/claims/companyname";

        public const string DisplayName = @"http://schemas.microsoft.com/accesscontrolservice/2016/01/claims/displayname";

        public const string EmailConfirmed = @"http://schemas.microsoft.com/accesscontrolservice/2016/01/claims/emailconfirmed";

        public const string Fingerprint = @"http://schemas.microsoft.com/accesscontrolservice/2016/01/claims/fingerprint";

        public const string IdentityProvider = @"http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider";

        public const string Language = @"http://schemas.microsoft.com/accesscontrolservice/2016/01/claims/language";

        public const string LockoutEnabled = @"http://schemas.microsoft.com/accesscontrolservice/2016/01/claims/lockoutenabled";

        public const string LockoutEndDate = @"http://schemas.microsoft.com/accesscontrolservice/2016/01/claims/lockoutenddate";

        public const string PasswordHash = @"http://schemas.microsoft.com/accesscontrolservice/2016/01/claims/passwordhash";

        public const string PhoneNumber = @"http://schemas.microsoft.com/accesscontrolservice/2016/01/claims/phoneconfirmed";

        public const string PhoneNumberConfirmed = @"http://schemas.microsoft.com/accesscontrolservice/2016/01/claims/phonenumberconfirmed";

        public const string Picture = @"http://schemas.microsoft.com/accesscontrolservice/2016/01/claims/picture";

        public const string Profile = @"http://schemas.microsoft.com/accesscontrolservice/2016/01/claims/profile";

        public const string SecurityStamp = @"http://schemas.microsoft.com/accesscontrolservice/2016/01/claims/securitystamp";

        public const string TwoFactorEnabled = @"http://schemas.microsoft.com/accesscontrolservice/2016/01/claims/twofactorenabled";

        public const string TwoFactorCode = @"http://schemas.microsoft.com/accesscontrolservice/2016/01/claims/twofactorcode";

        public const string TwoFactorConfirmed = @"http://schemas.microsoft.com/accesscontrolservice/2016/01/claims/twofactorconfirmed";

        public const string TwoFactorInstant = @"http://schemas.microsoft.com/accesscontrolservice/2016/01/claims/twofactorinstant";
    }
}
