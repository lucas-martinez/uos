﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace Uos.Cognito
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class IdentityEntity : IUser<string>
    {
        DateTime? _createdOn;
        ICollection<ClaimEntity> _claims;
        ICollection<LoginEntity> _logins;
        ICollection<RoleEntity> _roles;
        Guid? _userId;

        /// <summary>
        /// Used to record failures for the purposes of lockout
        /// </summary>
        [DataMember]
        public virtual int AccessFailedCount { get; set; }

        /// <summary>
        /// Navigation property for user claims
        /// </summary>
        [DataMember]
        public virtual ICollection<ClaimEntity> Claims
        {
            get { return _claims ?? (_claims = new Collection<ClaimEntity>()); }
            set { _claims = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual DateTime CreatedOn
        {
            get { return _createdOn ?? (_createdOn = DateTime.UtcNow).Value; }
            set { _createdOn = null; }
        }

        /// <summary>
        /// Email
        /// </summary>
        [DataMember]
        public virtual string EmailAddress { get; set; }

        /// <summary>
        /// True if the email is confirmed, default is false
        /// </summary>
        [DataMember]
        public virtual bool EmailConfirmed { get; set; }

        /// <summary>
        /// Full name
        /// </summary>
        [DataMember]
        public virtual string FullName { get; set; }
        
        /// <summary>
        /// Is lockout enabled for this user
        /// </summary>
        [DataMember]
        public virtual bool LockoutEnabled { get; set; }

        /// <summary>
        /// DateTime in UTC when lockout ends, any time in the past is considered not locked out.
        /// </summary>
        [DataMember]
        public virtual DateTime? LockoutEndDateUtc { get; set; }

        /// <summary>
        /// Navigation property for user logins
        /// </summary>
        [DataMember]
        public virtual ICollection<LoginEntity> Logins
        {
            get { return _logins ?? (_logins = new Collection<LoginEntity>()); }
            set { _logins = value; }
        }

        /// <summary>
        /// The salted/hashed form of the user password
        /// </summary>
        [DataMember]
        public virtual string PasswordHash { get; set; }

        /// <summary>
        /// The calculated strength of the user password
        /// </summary>
        [DataMember]
        public virtual string PasswordStrength { get; set; }

        /// <summary>
        /// PhoneNumber for the user
        /// </summary>
        [DataMember]
        public virtual string PhoneNumber { get; set; }

        /// <summary>
        /// True if the phone number is confirmed, default is false
        /// </summary>
        [DataMember]
        public virtual bool PhoneConfirmed { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [IgnoreDataMember]
        public virtual DateTime? RemovedOn { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual ICollection<RoleEntity> Roles
        {
            get { return _roles ?? (_roles = new Collection<RoleEntity>()); }
            set { _roles = value; }
        }

        /// <summary>
        /// A random value that should change whenever a users credentials have changed (password changed, login removed)
        /// </summary>
        [IgnoreDataMember]
        public virtual string SecurityStamp { get; set; }

        /// <summary>
        /// Is two factor enabled for the user
        /// </summary>
        [DataMember]
        public virtual bool TwoFactorEnabled { get; set; }

        [IgnoreDataMember]
        public virtual Guid UserId
        {
            get { return _userId ?? (_userId = Guid.NewGuid()).Value; }
            set { _userId = value; }
        }

        string IUser<string>.Id
        {
            get { return UserId.ToString(); }
        }

        string IUser<string>.UserName
        {
            get { return EmailAddress; }
            set { EmailAddress = value; }
        }
    }
}
