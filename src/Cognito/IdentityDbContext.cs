﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Cognito
{
    public class IdentityDbContext : DbContext
    {
        public IdentityDbContext()
            : base("IdentityConnection")
        {
        }

        public static IdentityDbContext Create()
        {
            return new IdentityDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Identity();
        }
    }
}
