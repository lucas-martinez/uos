﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Cognito
{
    [DataContract]
    public class ClaimEntity
    {
        DateTime? _createdOn;

        [IgnoreDataMember]
        public virtual int ClaimId { get; set; }

        [DataMember]
        public virtual string ClaimType { get; set; }

        [DataMember]
        public virtual string ClaimValue { get; set; }

        [DataMember]
        public virtual DateTime CreatedOn
        {
            get { return _createdOn ?? (_createdOn = DateTime.UtcNow).Value; }
            set { _createdOn = null; }
        }

        [IgnoreDataMember]
        public virtual IdentityEntity Identity { get; set; }

        [DataMember]
        public virtual string Issuer { get; set; }
        
        [IgnoreDataMember]
        public virtual DateTime? RemovedOn { get; set; }

        [IgnoreDataMember]
        public virtual Guid UserId { get; set; }
    }
}
