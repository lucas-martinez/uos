﻿using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace Uos.Cognito
{
    // Configure the application sign-in manager which is used in this application.
    public class SignInManager : SignInManager<IdentityEntity, string>
    {
        IOwinContext _context;

        public SignInManager(IdentityManager userManager, IOwinContext context)
            : base(userManager, context.Authentication)
        {
            _context = context;
        }

        public override async Task<ClaimsIdentity> CreateUserIdentityAsync(IdentityEntity entity)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            return await UserManager.CreateIdentityAsync(entity, DefaultAuthenticationTypes.ApplicationCookie);
        }

        public static SignInManager Create(IdentityFactoryOptions<SignInManager> options, IOwinContext context)
        {
            return new SignInManager(context.GetUserManager<IdentityManager>(), context);
        }
    }
}
