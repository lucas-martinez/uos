﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Cognito
{
    [DataContract]
    public class RoleEntity
    {
        DateTime? _createdOn;

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public virtual DateTime CreatedOn
        {
            get { return _createdOn ?? (_createdOn = DateTime.UtcNow).Value; }
            set { _createdOn = null; }
        }

        [IgnoreDataMember]
        public virtual IdentityEntity Identity { get; set; }

        [IgnoreDataMember]
        public virtual DateTime? RemovedOn { get; set; }
        
        [IgnoreDataMember]
        public virtual int RoleId { get; set; }

        [DataMember]
        public virtual string RoleName { get; set; }

        [IgnoreDataMember]
        public virtual Guid UserId { get; set; }
    }
}
