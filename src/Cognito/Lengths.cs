﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Cognito
{
    public class Lengths
    {
        public const int ClaimType = 0xFF;

        public const int ClaimValue = 0xFF;
        
        public const int CountryCode = 3;
        
        public const int CurrencyCode = 3;

        public const int Description = 0xFF;

        public const int EmailAddress = 0x3F;

        public const int FullName = 0x3F;
        
        public const int LoginProvider = 0xFF;

        public const int PasswordHash = 0xFF;

        public const int PasswordStrength = 1;

        public const int PhoneNumber = 0x2F;

        public const int ProviderKey = 0xFF;
        
        public const int RoleName = 0x2F;
        
        public const int SecurityStamp = 0xFF;
    }
}
