﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Uos.Cognito
{
    public static class Identity
    {
        public static IIdentityStore Store
        {
            get { return DependencyResolver.Default.GetInstance<IIdentityStore>(); }
        }

        public static Task<IResult> CreateIdentityAsync(IdentityEntity company)
        {
            return Result.Try(async () =>
            {
                var store = Store;

                if (store == null) return Result.Fail(() => Strings.IdentityStoreResolveFailure);

                var value = await store.CreateIdentityAsync(company);

                return Result.Ok(value);
            });
        }

        public static Task<IResult> DeleteIdentityAsync(string email)
        {
            return Result.Try(async () =>
            {
                var store = Store;

                if (store == null) return Result.Fail(() => Strings.IdentityStoreResolveFailure);

                var value = await store.FindAsync(email);

                if (value == null) return Result.Fail(Message.Create(MessageTypes.Error, () => Strings.IdentityEntityNotFound, email));

                return Result.Ok(value);
            });
        }

        public static Task<IResult> GetIdentities(string startsWith)
        {
            return Result.Try(async () =>
            {
                var store = Store;

                if (store == null) return Result.Fail(() => Strings.IdentityStoreResolveFailure);

                var value = await store.GetIdentitiesAsync(startsWith);

                return Result.Ok(value);
            });
        }

        public static Task<IResult> GetIdentitiesAsync(int? start, int? limit, Expression<Func<IdentityEntity, string>> orderBy)
        {
            return Result.Try(async () =>
            {
                var store = Store;

                if (store == null) return Result.Fail(() => Strings.IdentityStoreResolveFailure);

                var values = await store.GetIdentitiesAsync(start, limit, orderBy);

                return Result.Ok(values);
            });
        }

        public static Task<IResult> GetIdentityAsync(string email)
        {
            return Result.Try(async () =>
            {
                var store = Store;

                if (store == null) return Result.Fail(() => Strings.IdentityStoreResolveFailure);

                var value = await store.FindAsync(email);

                if (value == null) return Result.Fail(Message.Create(MessageTypes.Error, () => Strings.IdentityEntityNotFound, email));

                return Result.Ok(value);
            });
        }

        public static Task<IResult> UpdateIdentityAsync(string emailAddress, Func<IdentityEntity, Task<bool>> patcher)
        {
            return Result.Try(async () =>
            {
                if (patcher == null) throw new ArgumentNullException("patcher");

                var store = Store;

                if (store == null) return Result.Fail(() => Strings.IdentityStoreResolveFailure);

                var value = await store.FindAsync(emailAddress);

                if (value == null) return Result.Fail(Message.Create(MessageTypes.Error, () => Strings.IdentityEntityNotFound, emailAddress));

                await patcher(value);

                await store.SaveChangesAsync();

                return Result.Ok(value);
            });
        }

        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        public static async Task<IResult> AddExternalLoginAsync(this IOwinContext context, string email)
        {
            var loginInfo = await context.Authentication.GetExternalLoginInfoAsync(XsrfKey, email);

            if (loginInfo == null)
            {
                return Result.Fail(() => Strings.ExternalLoginFailed);
            }

            IdentityResult result;

            var man = context.GetUserManager<IdentityManager>();
            {
                result = await man.AddLoginAsync(email, loginInfo.Login);
            }

            return result.Succeeded ? Result.Success : Result.Fail(result.Errors);
        }

        public static async Task<IResult> ChangePasswordAsync(this IOwinContext context, string email, string currentPassword, string newPassword)
        {
            IdentityEntity entity = null;
            IdentityResult result;

            var manager = context.GetUserManager<IdentityManager>();
            {
                result = await manager.ChangePasswordAsync(email, currentPassword, newPassword);

                if (result.Succeeded)
                {
                    entity = await manager.FindByIdAsync(email);
                }
            }

            if (entity != null)
            {
                var signIn = context.Get<SignInManager>();

                await signIn.SignInAsync(entity, isPersistent: false, rememberBrowser: false);
            }

            return result.Succeeded ? Result.Success : Result.Fail(result.Errors);
        }

        public static async Task<IResult> VerifyPhoneNumberAsync(this IOwinContext context, string code)
        {
            return await Result.Try(async () =>
            {
                var identifier = context.Request.User.Identity.GetUserId();

                var entity = await Store.FindAsync(identifier);

                if (entity == null)
                {
                    return Result.Fail(() => Strings.IdentityEntityNotFound);
                }

                var manager = context.GetUserManager<IdentityManager>();

                var result = await manager.ChangePhoneNumberAsync(identifier, entity.PhoneNumber, code);
                
                if (!result.Succeeded)
                {
                    return Result.Fail(() => Strings.VerifyPhoneNumberFailed);
                }

                var signIn = context.Get<SignInManager>();

                await signIn.SignInAsync(entity, isPersistent: false, rememberBrowser: false);
                
                return Result.Ok(() => Strings.VerifyPhoneNumberSucceeded);
            });
        }

        public static async Task<IResult> ConfirmEmailAsync(this IOwinContext context, string email, string code)
        {
            if (string.IsNullOrEmpty(email) && string.IsNullOrEmpty(code))
            {
                return Result.Fail(Message.Create(MessageTypes.Error, () => Strings.EmailArgumentRequired), Message.Create(MessageTypes.Error, () => Strings.CodeArgumentRequired));
            }

            if (string.IsNullOrEmpty(email))
            {
                return Result.Fail(Message.Create(MessageTypes.Error, () => Strings.EmailArgumentRequired));
            }

            if (string.IsNullOrEmpty(code))
            {
                return Result.Fail(Message.Create(MessageTypes.Error, () => Strings.CodeArgumentRequired));
            }

            IdentityResult result;

            var man = context.GetUserManager<IdentityManager>();
            {
                result = await man.ConfirmEmailAsync(email, code);
            }

            return result.Succeeded ? Result.Success : Result.Fail(result.Errors);
        }

        public static async Task<IResult> ExternalSignInAsync(this IOwinContext context)
        {
            var loginInfo = await context.Authentication.GetExternalLoginInfoAsync();

            if (loginInfo == null)
            {
                return Result.Fail(() => Strings.ExternalLoginFailed);
            }

            SignInStatus status;

            var man = context.Get<SignInManager>();
            {
                // Sign in the user with this external login provider if the user already has a login
                status = await man.ExternalSignInAsync(loginInfo, isPersistent: false);
            }

            switch (status)
            {
                case SignInStatus.Success:
                    return Result.Ok(loginInfo);
                case SignInStatus.LockedOut:
                    return Result.Fail(() => Strings.LockedOut);
                case SignInStatus.RequiresVerification:
                    return new Result(false, Message.Create(MessageTypes.Warning, () => Strings.RequiresVerification));
                case SignInStatus.Failure:
                default:
                    return Result.Fail(loginInfo, () => Strings.AccountNotFound);
            }
        }

        public static async Task<IResult> GenerateChangePhoneNumberTokenAsync(this IOwinContext context, string email, string number)
        {
            IResult result = Result.Success;

            var manager = context.GetUserManager<IdentityManager>();
            {
                // Generate the token
                var code = await manager.GenerateChangePhoneNumberTokenAsync(email, number);

                result = Result.Ok(code);
            }

            return result;
        }

        public static async Task<IResult> GenerateEmailConfirmationTokenAsync(this IOwinContext context, string email)
        {
            IResult result = Result.Success;

            var manager = context.GetUserManager<IdentityManager>();
            {
                var identity = await manager.FindByEmailAsync(email);

                if (identity == null)
                {
                    result = Result.Fail(() => "Not found");
                }
                else
                {
                    // Generate the token
                    var token = await manager.GenerateEmailConfirmationTokenAsync(identity.UserId.ToString());

                    result = Result.Ok(token);
                }
            }

            return result;
        }

        public static async Task<IResult> GeneratePasswordResetTokenAsync(this IOwinContext context, string email)
        {
            IResult result = Result.Success;

            var manager = context.GetUserManager<IdentityManager>();
            {
                var identity = await manager.FindByEmailAsync(email);

                if (identity == null)
                {
                    result = Result.Fail(() => "Not found");
                }
                else
                {
                    // Generate the token
                    var token = await manager.GeneratePasswordResetTokenAsync(identity.UserId.ToString());

                    result = Result.Ok(token);
                }
            }

            return result;
        }

        public static async Task<IResult> HasBeenVerified(this IOwinContext context)
        {
            IResult result = Result.Success;

            var man = context.Get<SignInManager>();
            {
                if (!await man.HasBeenVerifiedAsync())
                {
                    result = Result.Fail("");
                }
            }

            return result;
        }

        public static async Task<bool> HasPassword(this IOwinContext context, string email)
        {
            var result = false;

            var man = context.GetUserManager<IdentityManager>();
            {
                var identity = await man.FindByIdAsync(email);

                if (identity != null)
                {
                    result = identity.PasswordHash != null;
                }
            }

            return result;
        }

        public static async Task<IResult> IsEmailConfirmedAsync(this IOwinContext context, string email)
        {
            IResult result;

            var man = context.GetUserManager<IdentityManager>();
            {
                var identity = await man.FindByNameAsync(email);

                result = identity != null
                    ? Result.Ok(await man.IsEmailConfirmedAsync(identity.EmailAddress))
                    : Result.Fail(Message.Create(MessageTypes.Error, () => Strings.InvalidCredentials));
            }

            return result;
        }

        public static async Task<SignInStatus> LoginAsync(this IOwinContext context, string email, string password, bool rememberMe = false, bool shouldLockout = false)
        {
            SignInStatus result;

            var man = context.Get<SignInManager>();
            {
                result = await man.PasswordSignInAsync(email, password, rememberMe, shouldLockout: shouldLockout);
            }

            return result;
        }

        public static IResult Login(this IOwinContext context, string email, string password, string code)
        {
            return Result.Ok(new IdentityEntity());
        }

        public static IResult Logout(this IOwinContext context, params string[] authenticationTypes)
        {
            context.Authentication.SignOut(authenticationTypes);

            return Result.Success;
        }

        public static async Task<IResult> RegisterAsync(this IOwinContext context, string email, string password, Func<string, string, string> urlFormat)
        {
            IdentityResult result;

            var identity = new IdentityEntity { EmailAddress = email };

            var manager = context.GetUserManager<IdentityManager>();

            result = await manager.CreateAsync(identity, password);

            // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
            // Send an email with this link
            string code = await manager.GenerateEmailConfirmationTokenAsync(email);

            var callbackUrl = urlFormat(identity.EmailAddress, code);

            await manager.SendEmailAsync(identity.EmailAddress, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

            if (result.Succeeded)
            {
                var signIn = context.Get<SignInManager>();

                await signIn.SignInAsync(identity, isPersistent: false, rememberBrowser: false);
            }

            return result.Succeeded ? Result.Success : Result.Fail(result.Errors);
        }

        public static async Task<IResult> RegisterExternalLoginAsync(this IOwinContext context, string email, string password = null)
        {
            // Get the information about the user from the external login provider
            var loginInfo = await context.Authentication.GetExternalLoginInfoAsync();

            if (loginInfo == null)
            {
                return Result.Fail(() => Strings.ExternalLoginFailed);
            }

            IdentityResult result;

            var identity = new IdentityEntity { EmailAddress = email };

            var man = context.GetUserManager<IdentityManager>();
            {
                result = password != null
                    ? await man.CreateAsync(identity, password)
                    : await man.CreateAsync(identity);

                if (result.Succeeded)
                {
                    result = await man.AddLoginAsync(identity.EmailAddress, loginInfo.Login);
                }
            }

            if (result.Succeeded)
            {
                var signIn = context.Get<SignInManager>();

                await signIn.SignInAsync(identity, isPersistent: false, rememberBrowser: false);
            }

            return result.Succeeded ? Result.Success : Result.Fail(result.Errors);
        }

        public static void RemoveActor(this ClaimsIdentity identity)
        {
            identity.SetActor(null);
        }

        public static async Task<IResult> RemoveLoginAsync(this IOwinContext context, string email, string loginProvider, string providerKey)
        {
            IdentityEntity entity = null;
            IdentityResult result;

            var manager = context.GetUserManager<IdentityManager>();

            result = await manager.RemoveLoginAsync(email, new UserLoginInfo(loginProvider, providerKey));

            if (result.Succeeded)
            {
                entity = await manager.FindByIdAsync(email);
            }

            if (entity != null)
            {
                var signIn = context.Get<SignInManager>();

                await signIn.SignInAsync(entity, isPersistent: false, rememberBrowser: false);
            }

            return result.Succeeded ? Result.Success : Result.Fail(result.Errors);
        }

        public static async Task<IResult> ResetPasswordAsync(this IOwinContext context, string email, string code, string password)
        {
            IResult result;

            var man = context.GetUserManager<IdentityManager>();
            {
                var identity = await man.FindByNameAsync(email);

                if (identity == null) result = Result.Fail(Message.Create(MessageTypes.Error, () => Strings.InvalidCredentials));
                else
                {
                    var ir = await man.ResetPasswordAsync(identity.EmailAddress, code, password);

                    result = ir.Succeeded ? Result.Success : Result.Fail(ir.Errors);
                }
            }

            return result;
        }

        public static async Task<IResult> SendCode(this IOwinContext context)
        {
            string userId;

            var signIn = context.Get<SignInManager>();

            userId = await signIn.GetVerifiedUserIdAsync();

            if (userId == null)
            {
                return Result.Fail(() => Strings.CodeArgumentRequired);
            }

            IResult result;

            var manager = context.GetUserManager<IdentityManager>();

            result = Result.Ok(await manager.GetValidTwoFactorProvidersAsync(userId));

            return result;
        }

        public static async Task<IResult> SendTwoFactorCodeAsync(this IOwinContext context, string provider)
        {
            IResult result = Result.Success;

            var man = context.Get<SignInManager>();
            {
                if (!await man.SendTwoFactorCodeAsync(provider))
                {
                    result = Result.Fail(() => Strings.SendTwoFactorCodeFailed);
                }
            }

            return result;
        }

        public static void SetActor(this ClaimsIdentity identity, string value)
        {
            var count = identity.Claims.Actor().ToArray().Aggregate(0, (i, c) => { identity.RemoveClaim(c); return i + 1; });

            if (value != null) identity.AddClaim(new Claim(ClaimTypes.Actor, value));
        }

        public static async Task<IResult> SetPasswordAsync(this IOwinContext context, string email, string password)
        {
            IdentityEntity entity = null;
            IdentityResult result;

            var manager = context.GetUserManager<IdentityManager>();

            result = await manager.AddPasswordAsync(email, password);

            if (result.Succeeded)
            {
                entity = await manager.FindByIdAsync(email);
            }

            if (entity != null)
            {
                var signIn = context.Get<SignInManager>();

                await signIn.SignInAsync(entity, isPersistent: false, rememberBrowser: false);
            }

            return result.Succeeded ? Result.Success : Result.Fail(result.Errors);
        }

        public static async Task<IResult> SetPhoneNumberAsync(this IOwinContext context, string number)
        {
            return await Result.Try(async () =>
            {
                IdentityResult result;

                var identifier = context.Request.User.Identity.GetUserId();

                var entity = await Store.FindAsync(identifier);

                if (entity == null)
                {
                    return Result.Fail(() => Strings.IdentityEntityNotFound);
                }

                if (entity.PhoneNumber == number)
                {
                    return Result.Success;
                }

                entity.PhoneNumber = number;

                entity.PhoneConfirmed = false;

                await Store.SaveChangesAsync();

                if (number == null)
                {
                    return Result.Success;
                }
                
                var manager = context.GetUserManager<IdentityManager>();

                var code = await manager.GenerateChangePhoneNumberTokenAsync(Convert.ToString(entity.UserId), number);

                return Result.Ok(code);
            });
        }
        public static Task<IResult> SetClaimsAsync(IdentityEntity identityEntity, IDictionary<string, string> claims)
        {
            return Result.Try(async () =>
            {
                var store = Store;

                if (store == null) return Result.Fail(() => Strings.IdentityStoreResolveFailure);

                await store.SetClaimsAsync(identityEntity, claims);

                return Result.Success;
            });
        }

        public static Task<IResult> SetClaimsAsync(IdentityEntity identityEntity, string claimType, IList<string> claimValues)
        {
            return Result.Try(async () =>
            {
                var store = Store;

                if (store == null) return Result.Fail(() => Strings.IdentityStoreResolveFailure);

                await store.SetClaimsAsync(identityEntity, claimType, claimValues);

                return Result.Success;
            });
        }

        public static Task<IResult> SetRolesAsync(IdentityEntity identityEntity, IList<string> roleNames)
        {
            return Result.Try(async () =>
            {
                var store = Store;

                if (store == null) return Result.Fail(() => Strings.IdentityStoreResolveFailure);

                await store.SetRolesAsync(identityEntity, roleNames);

                return Result.Success;
            });
        }

        public static async Task<IResult> SetTwoFactorEnabledAsync(this IOwinContext context, string email, bool enabled)
        {
            IdentityEntity entity = null;
            IdentityResult result;

            var manager = context.GetUserManager<IdentityManager>();

            result = await manager.SetTwoFactorEnabledAsync(email, true);

            if (result.Succeeded)
            {
                entity = await manager.FindByIdAsync(email);
            }

            if (entity != null)
            {
                var signIn = context.Get<SignInManager>();

                await signIn.SignInAsync(entity, isPersistent: false, rememberBrowser: false);
            }

            return result.Succeeded ? Result.Success : Result.Fail(result.Errors);
        }

        public static async Task<IResult> SignIn(this IOwinContext context, string email, string password, params string[] authenticationTypes)
        {
            var manager = context.GetUserManager<IdentityManager>();

            var identity = await manager.FindAsync(email, password);

            var claimsIdenities = new List<ClaimsIdentity>();

            foreach (var authenticationType in authenticationTypes)
            {
                var claimsIdenity = await manager.CreateIdentityAsync(identity, authenticationType);

                claimsIdenities.Add(claimsIdenity);
            }

            context.Authentication.SignIn(new AuthenticationProperties
            {
                AllowRefresh = true,
                IssuedUtc = DateTimeOffset.Now,
                ExpiresUtc = DateTimeOffset.Now + TimeSpan.FromDays(1)
            }, claimsIdenities.ToArray());

            return Result.Success;
        }

        public static Task<IResult> Switch(this IOwinContext context, string companyCode, params string[] authenticationTypes)
        {
            var principal = context.Authentication.User;

            if (!principal.Identity.IsAuthenticated)
            {
                return Task.FromResult(Result.Failure);
            }

            var identity = principal.Identity as ClaimsIdentity;

            foreach (var claim in identity.Claims.Actor())
            {
                identity.RemoveClaim(claim);
            }

            var data = identity.Claims.UserData().Select(c => Newtonsoft.Json.JsonConvert.DeserializeObject<UserData>(c.Value)).FirstOrDefault();

            context.Authentication.SignOut(authenticationTypes);

            if (companyCode != null && !string.IsNullOrEmpty(companyCode = companyCode.ToUpperInvariant()) && data.Companies.Any(c => c.Code == companyCode))
            {
                identity.AddClaim(new Claim(ClaimTypes.Actor, companyCode));
            }

            SignIn(context, new AuthenticationProperties
            {
                AllowRefresh = true,
                IssuedUtc = DateTimeOffset.Now,
                ExpiresUtc = DateTimeOffset.Now + TimeSpan.FromDays(1)
            }, principal.Identities.ToArray());

            return Task.FromResult(Result.Success);
        }

        //
        // Summary:
        //     Add information to the response environment that will cause the appropriate authentication
        //     middleware to grant a claims-based identity to the recipient of the response.
        //     The exact mechanism of this may vary. Examples include setting a cookie, to adding
        //     a fragment on the redirect url, or producing an OAuth2 access code or token response.
        //
        // Parameters:
        //   identities:
        //     Determines which claims are granted to the signed in user. The ClaimsIdentity.AuthenticationType
        //     property is compared to the middleware's Options.AuthenticationType value to
        //     determine which claims are granted by which middleware. The recommended use is
        //     to have a single ClaimsIdentity which has the AuthenticationType matching a specific
        //     middleware.
        public static void SignIn(this IOwinContext context, params ClaimsIdentity[] identities)
        {
            context.Authentication.SignIn(identities);
        }

        //
        // Summary:
        //     Add information to the response environment that will cause the appropriate authentication
        //     middleware to grant a claims-based identity to the recipient of the response.
        //     The exact mechanism of this may vary. Examples include setting a cookie, to adding
        //     a fragment on the redirect url, or producing an OAuth2 access code or token response.
        //
        // Parameters:
        //   properties:
        //     Contains additional properties the middleware are expected to persist along with
        //     the claims. These values will be returned as the AuthenticateResult.properties
        //     collection when AuthenticateAsync is called on subsequent requests.
        //
        //   identities:
        //     Determines which claims are granted to the signed in user. The ClaimsIdentity.AuthenticationType
        //     property is compared to the middleware's Options.AuthenticationType value to
        //     determine which claims are granted by which middleware. The recommended use is
        //     to have a single ClaimsIdentity which has the AuthenticationType matching a specific
        //     middleware.
        public static void SignIn(this IOwinContext context, AuthenticationProperties properties, params ClaimsIdentity[] identities)
        {
            context.Authentication.SignIn(properties, identities);
        }

        public static void SigOut(this IOwinContext context)
        {
            context.Authentication.SignOut(context.Authentication.User.Identities.Select(identity => identity.AuthenticationType).Distinct().ToArray());
        }

        //
        // Summary:
        //     Add information to the response environment that will cause the appropriate authentication
        //     middleware to revoke any claims identity associated the the caller. The exact
        //     method varies.
        //
        // Parameters:
        //   authenticationTypes:
        //     Identifies which middleware should perform the work to sign out. Multiple authentication
        //     types may be provided to clear out more than one cookie at a time, or to clear
        //     cookies and redirect to an external single-sign out url.
        public static void SignOut(this IOwinContext context, params string[] authenticationTypes)
        {
            context.Authentication.SignOut(authenticationTypes);
        }

        //
        // Summary:
        //     Add information to the response environment that will cause the appropriate authentication
        //     middleware to revoke any claims identity associated the the caller. The exact
        //     method varies.
        //
        // Parameters:
        //   properties:
        //     Additional arbitrary values which may be used by particular authentication types.
        //
        //   authenticationTypes:
        //     Identifies which middleware should perform the work to sign out. Multiple authentication
        //     types may be provided to clear out more than one cookie at a time, or to clear
        //     cookies and redirect to an external single-sign out url.
        public static void SignOut(this IOwinContext context, AuthenticationProperties properties, params string[] authenticationTypes)
        {
            context.Authentication.SignOut(properties, authenticationTypes);
        }

        public static async Task<IResult> TwoFactorSignInAsync(this IOwinContext context, string provider, string code, bool isPersistent, bool rememberBrowser)
        {
            IResult result;

            var signIn = context.Get<SignInManager>();

            var status = await signIn.TwoFactorSignInAsync(provider: provider, code: code, isPersistent: isPersistent, rememberBrowser: rememberBrowser);

            switch (status)
            {
                case SignInStatus.Success:
                    result = Result.Success;
                    break;

                case SignInStatus.LockedOut:
                    result = Result.Fail(() => Strings.LockedOut);
                    break;

                case SignInStatus.Failure:
                default:
                    result = Result.Fail(() => Strings.InvalidCode);
                    break;
            }

            return result;
        }

        public static async Task<IResult> GetIdentityAsync(this IOwinContext context, string email)
        {
            var manager = context.GetUserManager<IdentityManager>();

            var identity = await manager.FindByIdAsync(email);

            return identity != null ? Result.Ok(identity) : Result.Fail(() => Strings.AccountDeleted);
        }

        public static async Task<IResult> GetExternalLoginsAsync(this IOwinContext context, string email)
        {
            LoginOptions value = null;

            var man = context.GetUserManager<IdentityManager>();
            {
                var identity = await man.FindByIdAsync(email);

                if (identity != null)
                {
                    var logins = identity.Logins
                        .Select(e => new ExternalLogin { LoginProvider = e.LoginProvider, ProviderKey = e.ProviderKey })
                        .ToList();

                    var otherLogins = context.Authentication.GetExternalAuthenticationTypes()
                        .Where(auth => logins.All(login => auth.AuthenticationType != login.LoginProvider))
                        .Select(auth => new ExternalLogin { LoginProvider = auth.AuthenticationType, ProviderKey = auth.Caption })
                        .ToList();

                    value = new LoginOptions
                    {
                        HasPassword = identity.PasswordHash != null,
                        Logins = logins,
                        OtherLogins = otherLogins
                    };
                }
            }

            return value != null ? Result.Ok(value) : Result.Fail(() => Strings.AccountDeleted);
        }
    }
}
