﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Cognito
{
    public class Defaults
    {
        public const int AccessFailedLock = 3;

        public static TimeSpan AccessFailedLockoutDuration = TimeSpan.FromMinutes(5);

        public const string ClaimsIssuer = "UOS";
    }
}
