﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Uos.Cognito
{
    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.
    public class IdentityManager : UserManager<IdentityEntity>
    {
        readonly IOwinContext _owin;
        readonly IdentityStore _store;
        PasswordHasher _passwordHasher;
        readonly PasswordValidator _passwordValidator;

        public IdentityManager(IOwinContext context)
            : base(new IdentityStore(context))
        {
            _owin = context;
            _store = Store as IdentityStore;
            _passwordValidator = new PasswordValidator(new PasswordStrength());
        }

        public new PasswordHasher PasswordHasher
        {
            get { return _passwordHasher ?? (_passwordHasher = new PasswordHasher()); }
        }

        public new PasswordValidator PasswordValidator
        {
            get { return _passwordValidator; }
        }

        public override Task<IdentityResult> AddToRoleAsync(string email, string role)
        {
            return base.AddToRoleAsync(email, role);
        }

        public Task<IdentityEntity> FindAsync(string identifier)
        {
            return _store.FindAsync(identifier);
        }

        public override async Task<IdentityEntity> FindAsync(string email, string password)
        {
            //return base.FindAsync(email, password);

            var identity = await Store.FindByNameAsync(email);

            if (identity == null) return null;

            if (identity.LockoutEndDateUtc != null)
            {
                throw new UserException(() => Strings.LockedOut);
            }

            if (!PasswordHasher.VerifyHashedPassword(identity.PasswordHash, password))
            {
                var exception = new UserException(() => Strings.InvalidCredentials);

                identity.AccessFailedCount++;

                if (identity.AccessFailedCount >= Defaults.AccessFailedLock && identity.LockoutEnabled == true)
                {
                    identity.LockoutEndDateUtc = DateTime.UtcNow + Defaults.AccessFailedLockoutDuration;

                    exception = new UserException(() => Strings.LockedOut);
                }

                await _store.SaveChangesAsync();

                throw exception;
            }

            if (identity.AccessFailedCount != 0)
            {
                identity.AccessFailedCount = 0;

                await _store.SaveChangesAsync();
            }

            return identity;
        }

        public override Task<bool> IsInRoleAsync(string email, string role)
        {
            return base.IsInRoleAsync(email, role);
        }

        public override Task<IdentityResult> CreateAsync(IdentityEntity identity)
        {
            return base.CreateAsync(identity);
        }

        public virtual Task<IdentityResult> CreateAsync(string email)
        {
            return base.CreateAsync(new IdentityEntity { EmailAddress = email });
        }

        public async Task<IdentityEntity> CreateAsync(string email, IEnumerable<Claim> claims)
        {
            var identity = await _store.CreateIdentityAsync(new IdentityEntity { EmailAddress = email });

            await _store.SetClaimsAsync(identity.UserId, claims);

            return identity;
        }

        public override async Task<ClaimsIdentity> CreateIdentityAsync(IdentityEntity identity, string authenticationType)
        {
            var user = await base.CreateIdentityAsync(identity, authenticationType);

            // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.

            //user.Claims.Name().ToArray().Aggregate<Claim, int>(0, (i, c) => { user.RemoveClaim(c); return i + 1; });
            if (!user.Claims.Name().Any())
            {
                user.AddClaim(ClaimTypes.Name, identity.EmailAddress);
            }

            var context = new DefaultDbContext();

            var store = new Domain.StafferStore(context);
              

            var companies = await store.GetCompaniesAsync(identity.EmailAddress);

            // Add custom user claims here
            var data = new UserData
            {
                Companies = companies.Select(e => new UserData.Company { Code = e.Key, Name = e.Value }).ToList()
            };

            user.AddClaim(new Claim(ClaimTypes.UserData, Newtonsoft.Json.JsonConvert.SerializeObject(data)));

            return user;
        }

        public override async Task<IList<string>> GetRolesAsync(string userId)
        {
            var result = await Result.Try(async () =>
            {
                var identity = await _store.FindAsync(userId);

                if (identity == null)
                {
                    throw new UserException(() => Strings.IdentityEntityNotFound);
                }

                var roles = await _store.GetRoleNamesAsync(identity.UserId);

                var claims = (_owin.Request.User as ClaimsIdentity).Claims;

                var companyCode = claims.Actor().Select(c => c.Value).FirstOrDefault();

                if (companyCode != null)
                {
                    var r = await Domain.Staffer.Store.GetStafferRoleNamesAsync(companyCode, identity.EmailAddress);

                    roles = roles.Union(r).ToList();
                }

                return Result.Ok(roles);
            });

            return result.Succeeded ? result.OfType<string>().ToList() : (IList<string>)Array.Empty<string>();
        }

        public override Task<IdentityResult> RemoveFromRoleAsync(string email, string role)
        {
            return base.RemoveFromRoleAsync(email, role);
        }

        public static IdentityManager Create(IdentityFactoryOptions<IdentityManager> options, IOwinContext context)
        {
            var manager = new IdentityManager(context);

            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<IdentityEntity>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Configure validation logic for passwords
            /*manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 4,
                RequireNonLetterOrDigit = false,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false,
            };*/

            // Configure user lockout defaults
            manager.UserLockoutEnabledByDefault = true;
            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            manager.MaxFailedAccessAttemptsBeforeLockout = 5;

            // Register two factor authentication providers. This application uses Phone and emails as a step of receiving a code for verifying the user
            // You can write your own provider and plug it in here.
            manager.RegisterTwoFactorProvider("Phone Code", new PhoneNumberTokenProvider<IdentityEntity>
            {
                MessageFormat = "Your security code is {0}"
            });
            manager.RegisterTwoFactorProvider("email Code", new EmailTokenProvider<IdentityEntity>
            {
                Subject = "Security Code",
                BodyFormat = "Your security code is {0}"
            });
            manager.EmailService = new EmailService();
            manager.SmsService = new SmsService();
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider = new DataProtectorTokenProvider<IdentityEntity>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }

        public IResult ValidateUserName(string userName)
        {
            if (string.IsNullOrEmpty(userName)) return Result.Fail("UserNameRequired");

            if (userName.Length < 4) return Result.Fail("UserNameTooShort");

            return Result.Success;
        }

        public bool VerifyHashedPassword(string hashedPassword, string providedPassword)
        {
            return PasswordHasher.VerifyHashedPassword(hashedPassword, providedPassword);
        }
    }
}
