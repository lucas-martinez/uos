﻿using System.Runtime.Serialization;

namespace Uos.Cognito
{
    [DataContract]
    public class ExternalLogin
    {
        [DataMember(Name = "loginProvider")]
        public string LoginProvider { get; set; }

        [DataMember(Name = "providerKey")]
        public string ProviderKey { get; set; }
    }
}
