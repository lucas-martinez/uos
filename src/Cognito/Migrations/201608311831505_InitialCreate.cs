namespace Uos.Cognito.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.identities",
                c => new
                    {
                        email = c.String(nullable: false, maxLength: 63, unicode: false),
                        email_ok = c.Boolean(nullable: false),
                        number = c.String(maxLength: 47, unicode: false),
                        number_ok = c.Boolean(nullable: false),
                        two_factor = c.Boolean(nullable: false),
                        fail_count = c.Int(nullable: false),
                        lockout_on = c.Boolean(nullable: false),
                        lockout_utc = c.DateTime(),
                        hash = c.String(maxLength: 255, unicode: false),
                        stamp = c.String(maxLength: 255, unicode: false),
                        user = c.Guid(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        name = c.String(maxLength: 63),
                        PasswordStrength = c.String(),
                        RemovedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.user)
                .Index(t => t.email, unique: true, name: "IX_IdentityEmail")
                .Index(t => t.number, name: "IX_IdentityPhone")
                .Index(t => t.user, unique: true, name: "IX_IdentityUser")
                .Index(t => t.name, name: "IX_IdentityName");
            
            CreateTable(
                "dbo.claims",
                c => new
                    {
                        user = c.Guid(nullable: false),
                        type = c.String(nullable: false, maxLength: 255, unicode: false),
                        value = c.String(nullable: false, maxLength: 255, unicode: false),
                        issuer = c.String(nullable: false, maxLength: 255, unicode: false),
                        issued = c.DateTime(nullable: false),
                        id = c.Int(nullable: false, identity: true),
                        removed_utc = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.user, t.type, t.value, t.issuer })
                .ForeignKey("dbo.identities", t => t.user, cascadeDelete: true)
                .Index(t => t.user, name: "IX_ClaimUser")
                .Index(t => t.issued, name: "IX_ClaimCreated")
                .Index(t => t.id, unique: true, name: "IX_ClaimId")
                .Index(t => t.removed_utc, name: "IX_ClaimRemoved");
            
            CreateTable(
                "dbo.logins",
                c => new
                    {
                        user = c.Guid(nullable: false),
                        provider = c.String(nullable: false, maxLength: 255, unicode: false),
                        key = c.String(nullable: false, maxLength: 255, unicode: false),
                        id = c.Int(nullable: false, identity: true),
                        created_utc = c.DateTime(nullable: false),
                        removed_utc = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.user, t.provider, t.key })
                .ForeignKey("dbo.identities", t => t.user, cascadeDelete: true)
                .Index(t => t.user, name: "IX_LoginUser")
                .Index(t => t.provider, name: "IX_LoginProvider")
                .Index(t => t.key, name: "IX_LoginKey")
                .Index(t => t.id, unique: true, name: "IX_ClaimId")
                .Index(t => t.created_utc, name: "IX_LoginCreated")
                .Index(t => t.removed_utc, name: "IX_LoginRemoved");
            
            CreateTable(
                "dbo.roles",
                c => new
                    {
                        user = c.Guid(nullable: false),
                        name = c.String(nullable: false, maxLength: 47, unicode: false),
                        created_utc = c.DateTime(nullable: false),
                        removed_utc = c.DateTime(),
                        id = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => new { t.user, t.name })
                .ForeignKey("dbo.identities", t => t.user, cascadeDelete: true)
                .Index(t => t.user, name: "IX_RoleUser")
                .Index(t => t.name, name: "IX_RoleName")
                .Index(t => t.created_utc, name: "IX_RoleCreated")
                .Index(t => t.removed_utc, name: "IX_RoleRemoved")
                .Index(t => t.id, name: "IX_RoleId");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.roles", "user", "dbo.identities");
            DropForeignKey("dbo.logins", "user", "dbo.identities");
            DropForeignKey("dbo.claims", "user", "dbo.identities");
            DropIndex("dbo.roles", "IX_RoleId");
            DropIndex("dbo.roles", "IX_RoleRemoved");
            DropIndex("dbo.roles", "IX_RoleCreated");
            DropIndex("dbo.roles", "IX_RoleName");
            DropIndex("dbo.roles", "IX_RoleUser");
            DropIndex("dbo.logins", "IX_LoginRemoved");
            DropIndex("dbo.logins", "IX_LoginCreated");
            DropIndex("dbo.logins", "IX_ClaimId");
            DropIndex("dbo.logins", "IX_LoginKey");
            DropIndex("dbo.logins", "IX_LoginProvider");
            DropIndex("dbo.logins", "IX_LoginUser");
            DropIndex("dbo.claims", "IX_ClaimRemoved");
            DropIndex("dbo.claims", "IX_ClaimId");
            DropIndex("dbo.claims", "IX_ClaimCreated");
            DropIndex("dbo.claims", "IX_ClaimUser");
            DropIndex("dbo.identities", "IX_IdentityName");
            DropIndex("dbo.identities", "IX_IdentityUser");
            DropIndex("dbo.identities", "IX_IdentityPhone");
            DropIndex("dbo.identities", "IX_IdentityEmail");
            DropTable("dbo.roles");
            DropTable("dbo.logins");
            DropTable("dbo.claims");
            DropTable("dbo.identities");
        }
    }
}
