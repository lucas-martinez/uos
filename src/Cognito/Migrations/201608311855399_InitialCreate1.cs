namespace Uos.Cognito.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate1 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.claims");
            AddPrimaryKey("dbo.claims", "id");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.claims");
            AddPrimaryKey("dbo.claims", new[] { "user", "type", "value", "issuer" });
        }
    }
}
