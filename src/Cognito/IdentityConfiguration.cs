﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Cognito
{
    public static class IdentityConfiguration
    {
        public static void Identity(this DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<IdentityEntity>()
                .ToTable("identities")
                .HasKey(e => e.UserId)
                .Property<IdentityEntity, int>(e => e.AccessFailedCount, (property) =>
                {
                    property.HasColumnName("fail_count").HasColumnOrder(6).IsRequired();
                })
                .Property<IdentityEntity>(e => e.EmailAddress, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_IdentityEmail", 2) { IsUnique = true }));
                    property.HasColumnName("email").HasColumnOrder(1).HasMaxLength(Lengths.EmailAddress).IsRequired().IsUnicode(false);
                })
                .Property<IdentityEntity, bool>(e => e.EmailConfirmed, (property) =>
                {
                    property.HasColumnName("email_ok").HasColumnOrder(2).IsRequired();
                })
                .Property<IdentityEntity>(e => e.FullName, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_IdentityName", 4)));
                    property.HasColumnName("name");
                    property.HasMaxLength(Lengths.FullName);
                    property.IsOptional();
                    property.IsUnicode();
                })
                .Property<IdentityEntity, bool>(e => e.LockoutEnabled, (property) =>
                {
                    property.HasColumnName("lockout_on").HasColumnOrder(7).IsRequired();
                })
                .Property<IdentityEntity>(e => e.LockoutEndDateUtc, (property) =>
                {
                    property.HasColumnName("lockout_utc").HasColumnOrder(8).IsOptional();
                })
                .Property<IdentityEntity>(e => e.PasswordHash, (property) =>
                {
                    property.HasColumnName("hash").HasColumnOrder(9).HasMaxLength(Lengths.PasswordHash).IsOptional().IsUnicode(false);
                })
                .Property<IdentityEntity>(e => e.PhoneNumber, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_IdentityPhone", 3)));
                    property.HasColumnName("number").HasColumnOrder(3).HasMaxLength(Lengths.PhoneNumber).IsOptional().IsUnicode(false);
                })
                .Property<IdentityEntity, bool>(e => e.PhoneConfirmed, (property) =>
                {
                    property.HasColumnName("number_ok").HasColumnOrder(4).IsRequired();
                })
                .Property<IdentityEntity>(e => e.SecurityStamp, (property) =>
                {
                    property.HasColumnName("stamp").HasColumnOrder(10).HasMaxLength(Lengths.SecurityStamp).IsOptional().IsUnicode(false);
                })
                .Property<IdentityEntity, bool>(e => e.TwoFactorEnabled, (property) =>
                {
                    property.HasColumnName("two_factor").HasColumnOrder(5).IsRequired();
                })
                .Property<IdentityEntity, Guid>(e => e.UserId, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_IdentityUser", 1) { IsUnique = true }));
                    property.HasColumnName("user").HasColumnOrder(11).IsRequired();
                    property.IsRequired();
                });

            modelBuilder.Entity<ClaimEntity>()
                .ToTable("claims")
                .HasKey(e => e.ClaimId)
                .HasRequired(e => e.Identity, (property) =>
                {
                    property.WithMany(e => e.Claims).HasForeignKey(e => e.UserId);
                })
                .Property<ClaimEntity, DateTime>(e => e.CreatedOn, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_ClaimCreated", 4)));
                    property.HasColumnName("created_utc");
                    property.IsRequired();
                })
                .Property<ClaimEntity>(e => e.Issuer, (property) =>
                {   
                    property.HasColumnName("issuer").HasColumnOrder(4).HasMaxLength(Lengths.ClaimType).IsRequired().IsUnicode(false);
                })
                .Property<ClaimEntity, DateTime>(e => e.CreatedOn, (property) =>
                {
                    property.HasColumnName("issued").HasColumnOrder(5).IsRequired();
                })
                .Property<ClaimEntity>(e => e.ClaimType, (property) =>
                {
                    property.HasColumnName("type").HasColumnOrder(2).HasMaxLength(Lengths.ClaimType).IsRequired().IsUnicode(false);
                })
                .Property<ClaimEntity>(e => e.ClaimValue, (property) =>
                {
                    property.HasColumnName("value").HasColumnOrder(3).HasMaxLength(Lengths.ClaimValue).IsRequired().IsUnicode(false);
                })
                .Property<ClaimEntity, int>(e => e.ClaimId, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_ClaimId", 2) { IsUnique = true }));
                    property.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
                    property.HasColumnName("id").HasColumnOrder(6).IsRequired();
                })
                .Property<ClaimEntity, DateTime>(e => e.RemovedOn, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_ClaimRemoved", 5)));
                    property.HasColumnName("removed_utc");
                    property.IsOptional();
                })
                .Property<ClaimEntity, Guid>(e => e.UserId, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_ClaimUser", 1)));
                    property.HasColumnName("user").HasColumnOrder(1).IsRequired();
                });

            modelBuilder.Entity<LoginEntity>()
                .ToTable("logins")
                .HasKey(e => new { e.UserId, e.LoginProvider, e.ProviderKey })
                .HasRequired(e => e.Identity, (property) =>
                {
                    property.WithMany(e => e.Logins).HasForeignKey(e => e.UserId);
                })
                .Property<LoginEntity, DateTime>(e => e.CreatedOn, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_LoginCreated", 4)));
                    property.HasColumnName("created_utc");
                    property.IsRequired();
                })
                .Property<LoginEntity>(e => e.LoginProvider, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_LoginProvider", 1)));
                    property.HasColumnName("provider").HasColumnOrder(2).HasMaxLength(Lengths.LoginProvider).IsRequired().IsUnicode(false);
                })
                .Property<LoginEntity>(e => e.ProviderKey, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_LoginKey", 1)));
                    property.HasColumnName("key").HasColumnOrder(3).HasMaxLength(Lengths.ProviderKey).IsRequired().IsUnicode(false);
                })
                .Property<LoginEntity, int>(e => e.LoginId, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_ClaimId", 2) { IsUnique = true }));
                    property.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
                    property.HasColumnName("id").HasColumnOrder(6).IsRequired();
                })
                .Property<LoginEntity, DateTime>(e => e.RemovedOn, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_LoginRemoved", 5)));
                    property.HasColumnName("removed_utc");
                    property.IsOptional();
                })
                .Property<LoginEntity, Guid>(e => e.UserId, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_LoginUser", 1)));
                    property.HasColumnName("user").HasColumnOrder(1).IsRequired();
                });

            modelBuilder.Entity<RoleEntity>()
                .ToTable("roles")
                .HasKey(e => new { e.UserId, e.RoleName })
                .HasRequired(e => e.Identity, (navigationProperty) =>
                {
                    navigationProperty.WithMany(e => e.Roles).HasForeignKey(e => e.UserId);
                })
                .Property<RoleEntity, DateTime>(e => e.CreatedOn, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_RoleCreated", 4)));
                    property.HasColumnName("created_utc");
                    property.IsRequired();
                })
                .Property<RoleEntity, int>(e => e.RoleId, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_RoleId", 4)));
                    property.HasColumnName("id");
                    property.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
                    property.IsRequired();
                })
                .Property<RoleEntity>(e => e.RoleName, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_RoleName", 3)));
                    property.HasColumnName("name");
                    property.HasMaxLength(Lengths.RoleName);
                    property.IsRequired();
                    property.IsUnicode(false);
                })
                .Property<RoleEntity, DateTime>(e => e.RemovedOn, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_RoleRemoved", 5)));
                    property.HasColumnName("removed_utc");
                    property.IsOptional();
                })
                .Property<RoleEntity, Guid>(e => e.UserId, (property) =>
                {
                    property.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_RoleUser", 1)));
                    property.HasColumnName("user");
                    property.IsRequired();
                });
        }
    }
}
