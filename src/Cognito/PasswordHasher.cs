﻿using System;
using System.Security.Cryptography;

namespace Uos.Cognito
{
    /// <summary>
    ///     Make password encryption available outside of Microsoft.AspNet.Identity
    /// </summary>
    public class PasswordHasher : IPasswordHasher
    {
        private const int SaltSize = 16;
        private const int BytesRequired = 32;
        private const int Iterations = 1000; // 1000, afaik, which is the min recommended for Rfc2898DeriveBytes

        public virtual string HashPassword(string password)
        {
            if (password == null) return null;

            var array = new byte[1 + SaltSize + BytesRequired];

            using (var pbkdf2 = new Rfc2898DeriveBytes(password, SaltSize, Iterations))
            {
                byte[] salt = pbkdf2.Salt;
                Buffer.BlockCopy(salt, 0, array, 1, SaltSize);
                byte[] bytes = pbkdf2.GetBytes(BytesRequired);
                Buffer.BlockCopy(bytes, 0, array, SaltSize + 1, BytesRequired);
            }
            return Convert.ToBase64String(array);
        }

        public virtual bool VerifyHashedPassword(string hashedPassword, string providedPassword)
        {
            if (providedPassword == null && hashedPassword == null) return true;

            if (providedPassword == null && hashedPassword != null) return false;

            byte[] bytes, salt = new byte[SaltSize], array = Convert.FromBase64String(hashedPassword);

            Buffer.BlockCopy(array, 1, salt, 0, SaltSize);

            using (var pbkdf2 = new Rfc2898DeriveBytes(providedPassword, salt, Iterations))
            {
                bytes = pbkdf2.GetBytes(BytesRequired);
            }

            bool match = true;

            for (int i = 0; match && i < BytesRequired; i++)
            {
                match = bytes[i] == array[1 + SaltSize + i];
            }

            return match;
        }
    }
}
