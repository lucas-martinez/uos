﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Cognito
{
    [DataContract]
    public class UserData
    {
        IList<Company> _companies;
        IList<string> _features;

        [DataMember]
        public IList<Company> Companies
        {
            get { return _companies ?? (_companies = new List<Company>()); }
            set { _companies = value; }
        }

        [DataMember]
        public IList<string> Features
        {
            get { return _features ?? (_features = new List<string>()); }
            set { _features = value; }
        }

        [DataContract]
        public class Company
        {
            IList<string> _features;

            [DataMember]
            public string Code { get; set; }

            [DataMember]
            public IList<string> Features
            {
                get { return _features ?? (_features = new List<string>()); }
                set { _features = value; }
            }

            [DataMember]
            public string Name { get; set; }
        }
    }
}
