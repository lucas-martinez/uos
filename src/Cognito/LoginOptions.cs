﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Uos.Cognito
{
    [DataContract]
    public class LoginOptions
    {
        [DataMember(Name = "hasPassword")]
        public bool HasPassword { get; set; }

        [DataMember(Name = "logins")]
        public IList<ExternalLogin> Logins { get; set; }

        [DataMember(Name = "otherLogins")]
        public IList<ExternalLogin> OtherLogins { get; set; }

        [IgnoreDataMember]
        public bool ShowRemoveButton
        {
            get { return HasPassword || (Logins != null && Logins.Count > 1); }
        }
    }
}
