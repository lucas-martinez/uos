﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Cognito
{
    public static class IdentityClaims
    {
        public const string Facebook = "Facebook";
        public const string Google = "Google";
        public const string Microsoft = "Microsoft";
        public const string Twitter = "Twitter";
        public const string UosWeb = "Uosweb";

        public const string Address = "address";
        public const string BirthDate = "date-birth";
        public const string Company = "company";
        public const string Country = "country";
        public const string Culture = "culture";
        public const string Email = "email";
        public const string Gender = "gender";
        public const string FirstName = "name-first";
        public const string FullName = "name";
        public const string HomePhone = "phone-home";
        public const string LastName = "name-last";
        public const string Language = "language";
        public const string Locality = "locality";
        public const string MobilePhone = "phone-mobile";
        public const string OtherPhone = "phone-other";
        public const string PostalCode = "postal-code";
        public const string Region = "region";
        public const string Url = "url";

        public static readonly Claim[] Identity = new[]
        {
            new Claim(type: ClaimTypes.Country, value: Country, valueType: ClaimValueTypes.String, issuer: UosWeb),
            new Claim(type: "http://schemas.uosweb.com/accesscontrolservice/2016/01/claims/culture", value: Culture, valueType: ClaimValueTypes.String, issuer: UosWeb),
            new Claim(type: ClaimTypes.GivenName, value: FirstName, valueType: ClaimValueTypes.String, issuer: UosWeb),
            new Claim(type: ClaimTypes.Locality, value: Locality, valueType: ClaimValueTypes.String, issuer: UosWeb),
            new Claim(type: "http://schemas.uosweb.com/accesscontrolservice/2016/01/claims/othercompany", value: Company, valueType: ClaimValueTypes.String, issuer: UosWeb),
            new Claim(type: ClaimTypes.Surname, value: LastName, valueType: ClaimValueTypes.String, issuer: UosWeb),
            new Claim(type: ClaimTypes.StateOrProvince, value: Region, valueType: ClaimValueTypes.String, issuer: UosWeb)
        };
        
        public static readonly Claim[] Persist = new[]
        {
            new Claim(type: ClaimTypes.Country, value: Country, valueType: ClaimValueTypes.String, issuer: UosWeb),
            new Claim(type: ClaimTypes.DateOfBirth, value: BirthDate, valueType: ClaimValueTypes.String, issuer: UosWeb),
            new Claim(type: ClaimTypes.Email, value: Email, valueType: ClaimValueTypes.String, issuer: UosWeb),
            new Claim(type: ClaimTypes.Gender, value: Gender, valueType: ClaimValueTypes.String, issuer: UosWeb),
            new Claim(type: ClaimTypes.GivenName, value: FirstName, valueType: ClaimValueTypes.String, issuer: UosWeb),
            new Claim(type: ClaimTypes.HomePhone, value: HomePhone, valueType: ClaimValueTypes.String, issuer: UosWeb),
            new Claim(type: ClaimTypes.Locality, value: Locality, valueType: ClaimValueTypes.String, issuer: UosWeb),
            new Claim(type: ClaimTypes.MobilePhone, value: MobilePhone, valueType: ClaimValueTypes.String, issuer: UosWeb),
            new Claim(type: ClaimTypes.OtherPhone, value: OtherPhone, valueType: ClaimValueTypes.String, issuer: UosWeb),
            new Claim(type: ClaimTypes.PostalCode, value: PostalCode, valueType: ClaimValueTypes.String, issuer: UosWeb),
            new Claim(type: ClaimTypes.StateOrProvince, value: Region, valueType: ClaimValueTypes.String, issuer: UosWeb),
            new Claim(type: ClaimTypes.StreetAddress, value: Address, valueType: ClaimValueTypes.String, issuer: UosWeb),
            new Claim(type: ClaimTypes.Surname, value: LastName, valueType: ClaimValueTypes.String, issuer: UosWeb),
             new Claim(type: ClaimTypes.Webpage, value: Url, valueType: ClaimValueTypes.String, issuer: UosWeb)
        };

        public static IEnumerable<Claim> Actor(this IEnumerable<Claim> claims)
        {
            return claims.OfType(ClaimTypes.Actor);
        }

        public static IEnumerable<Claim> Compact(this IEnumerable<Claim> claims)
        {
            foreach (var claim in claims)
            {
                var item = Persist.Where(c => c.Type == claim.Type && c.Issuer == claim.Issuer || c.Issuer == claim.OriginalIssuer).Select(c => Type(claim, c.Type)).FirstOrDefault()
                    ?? Persist.Where(c => c.Type == claim.Type).Select(c => Type(claim, c.Type)).FirstOrDefault();

                if (item != null) yield return item;
            }
        }

        public static IEnumerable<Claim> Expand(this IEnumerable<Claim> claims)
        {
            foreach (var claim in claims)
            {
                var item = Persist.Where(c => c.Type == claim.Type && c.Issuer == claim.Issuer || c.Issuer == claim.OriginalIssuer).Select(c => Value(claim, c.Type)).FirstOrDefault()
                    ?? Persist.Where(c => c.Type == claim.Type).Select(c => Type(claim, c.Type)).FirstOrDefault();

                if (item != null) yield return item;
            }
        }

        public static IEnumerable<Claim> Anonymous()
        {
            yield return new Claim(ClaimTypes.Anonymous, "true", "boolean");
        }

        public static IEnumerable<Claim> EmailAddress(string emailAddress)
        {
            return Persist.Where(c => c.Value == Email && c.Issuer == null).Value(emailAddress);
        }

        public static IEnumerable<Claim> EmailAddress(this IEnumerable<Claim> claims)
        {
            return claims.OfType(Email);
        }
        
        public static IEnumerable<Claim> Name(this IEnumerable<Claim> claims)
        {
            return claims.OfType(ClaimTypes.Name);
        }

        public static IEnumerable<Claim> NameIdentifier(this IEnumerable<Claim> claims)
        {
            return claims.OfType(ClaimTypes.NameIdentifier);
        }

        internal static IEnumerable<Claim> OfType(this IEnumerable<Claim> claims, string type)
        {
            var types = Persist.Where(c => c.Value == type).Select(c => c.Type).ToArray();

            return claims.Where(c => c.Type == type || types.Contains(c.Type));
        }

        public static Claim Type(Claim claim, string type)
        {
            return new Claim(type: type, value: claim.Value, valueType: claim.ValueType, issuer: claim.Issuer, originalIssuer: claim.OriginalIssuer, subject: claim.Subject);
        }

        internal static IEnumerable<Claim> Type(this IEnumerable<Claim> claims, string type)
        {
            return claims.Select(c => Type(c, type));
        }

        public static Claim Value(Claim claim, string value)
        {
            return new Claim(type: claim.Type, value: value, valueType: claim.ValueType, issuer: claim.Issuer, originalIssuer: claim.Issuer, subject: claim.Subject);
        }

        public static IEnumerable<Claim> UserData(this IEnumerable<Claim> claims)
        {
            return claims.OfType(ClaimTypes.UserData);
        }

        internal static IEnumerable<Claim> Value(this IEnumerable<Claim> claims, string value)
        {
            return claims.Select(c => Value(c, value));
        }
    }
}
