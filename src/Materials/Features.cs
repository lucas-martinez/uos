﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Materials
{
    /// <summary>
    /// Materials Management features
    /// </summary>
    public class Features
    {
        /// <summary>
        /// Information System
        /// </summary>
        public const string Information = @"MM-IS";

        /// <summary>
        /// Electronic Data Interchange
        /// </summary>
        public const string Interchange = @"MM-EDI";

        /// <summary>
        /// Inventory Management
        /// </summary>
        public const string Inventory = @"MM-IM";

        /// <summary>
        /// Invoice Verification
        /// </summary>
        public const string Invoice = @"MM-IV";

        /// <summary>
        /// Consumption Based Planning
        /// </summary>
        public const string Planning = @"MM-CBP";

        /// <summary>
        /// Purchasing
        /// </summary>
        public const string Purchasing = @"MM-PUR";

        /// <summary>
        /// Warehouse Management
        /// </summary>
        public const string Warehouse = @"MM-WM";
    }
}
