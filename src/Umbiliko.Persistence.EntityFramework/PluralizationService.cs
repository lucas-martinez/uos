﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Pluralization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Persistence.EntityFramework
{
    public class PluralizationService
    {
        private static IPluralizationService _pluralization;

        public static IPluralizationService Pluralization
        {
            get { return _pluralization ?? (_pluralization = System.Data.Entity.DbConfiguration.DependencyResolver.GetService<IPluralizationService>()); }
        }
    }
}
