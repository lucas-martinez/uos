﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Persistence.EntityFramework
{
    using Uos.Persistence;
    using Uos.Persistence.Configuration;
    
    public class EntityFrameworkStore<TEntity> : DbSet<TEntity>, IStore<TEntity>
        where TEntity : class, new()
    {
        private readonly EntityFrameworkContext _context;
        private readonly EntityTypeConfiguration<TEntity> _configuration;

        public EntityFrameworkStore(EntityFrameworkContext context, EntityTypeConfiguration<TEntity> configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        public EntityTypeConfiguration<TEntity> Configuration
        {
            get { return _configuration; }
        }

        void IStore<TEntity>.Create(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public EntityFrameworkContext Context
        {
            get { return _context; }
        }

        int IStore<TEntity>.Count()
        {
            throw new NotImplementedException();
        }

        void IStore<TEntity>.Delete(TEntity entity)
        {
            throw new NotImplementedException();
        }

        IStore<TEntity> IStore<TEntity>.Page(string orderBy, int limit, int offset)
        {
            throw new NotImplementedException();
        }

        IStore<TEntity> IStore<TEntity>.Where(System.Linq.Expressions.Expression<Func<TEntity, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        void IStore<TEntity>.Update(TEntity entity)
        {
            throw new NotImplementedException();
        }

        void IStore.SaveChanges()
        {
            throw new NotImplementedException();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
        }

        void IDisposable.Dispose()
        {
            Dispose(true);
        }

        IEnumerator<TEntity> IEnumerable<TEntity>.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
