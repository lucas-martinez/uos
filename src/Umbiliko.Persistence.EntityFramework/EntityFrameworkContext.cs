﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Persistence.EntityFramework
{   
    public class EntityFrameworkContext : DbContext
    {
        public EntityFrameworkContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
        }

        public EntityFrameworkContext(string nameOrConnectionString, string providerName)
            : base(BuildConnectionString(nameOrConnectionString, providerName))
        {
        }

        public string ConnectionString
        {
            get { return base.Database.Connection.ConnectionString; }
        }

        protected static string BuildConnectionString(string nameOrConnectionString, string providerName)
        {
            var connectionString = nameOrConnectionString;

            // Initialize the connection string builder for the
            // underlying provider.
            var sqlBuilder = new SqlConnectionStringBuilder(connectionString);

            // Build the SqlConnection connection string.
            string providerString = sqlBuilder.ToString();

            // Initialize the EntityConnectionStringBuilder.
            var entityBuilder = new EntityConnectionStringBuilder();

            //Set the provider name.
            entityBuilder.Provider = providerName;

            // Set the provider-specific connection string.
            entityBuilder.ProviderConnectionString = providerString;

            return entityBuilder.ToString();
        }
    }
}
