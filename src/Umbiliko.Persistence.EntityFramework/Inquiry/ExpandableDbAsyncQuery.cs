﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Inquiry
{
    public class ExpandableDbAsyncQuery<T> : ExpandableQuery<T>, IDbAsyncEnumerable<T>
    {
        internal protected ExpandableDbAsyncQuery(IQueryable<T> inner)
            : base(inner)
        {
        }
        
        public IDbAsyncEnumerator<T> GetAsyncEnumerator()
        {
            return new ExpandableDbAsyncEnumerator<T>(this.AsEnumerable().GetEnumerator());
        }

        IDbAsyncEnumerator IDbAsyncEnumerable.GetAsyncEnumerator()
        {
            return GetAsyncEnumerator();
        }
    }
}
