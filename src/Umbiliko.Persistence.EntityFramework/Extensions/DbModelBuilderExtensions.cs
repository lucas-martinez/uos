﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Data
{
    using Schema;
    using System.Data.Entity.ModelConfiguration.Configuration;

    public static class DbModelBuilderExtensions
    {
        public static DbModelBuilder Apply<TEntity>(this DbModelBuilder dbModelBuilder, EntityType<TEntity> entityType)
            where TEntity : class, new()
        {
            var config = dbModelBuilder.Entity<TEntity>();
            
            return dbModelBuilder;
        }

        public static EntityTypeConfiguration<TEntity> HasIdentity<TEntity>(this EntityTypeConfiguration<TEntity> entityType, Expression<Func<TEntity, int>> property, string dbName = null)
            where TEntity : class, new()
        {
            var primitiveProperty = entityType.Property(property);

            primitiveProperty.IsRequired();

            primitiveProperty.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            if (!string.IsNullOrEmpty(dbName)) primitiveProperty.HasColumnName(dbName);

            return entityType;
        }

        public static EntityTypeConfiguration<TEntity> HasIdentity<TEntity>(this EntityTypeConfiguration<TEntity> entityType, Expression<Func<TEntity, long>> property, string dbName = null)
            where TEntity : class, new()
        {
            var primitiveProperty = entityType.Property(property);

            primitiveProperty.IsRequired();

            primitiveProperty.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            if (!string.IsNullOrEmpty(dbName)) primitiveProperty.HasColumnName(dbName);

            return entityType;
        }

        public static EntityTypeConfiguration<TEntity> HasIdentity<TEntity>(this EntityTypeConfiguration<TEntity> entityType, Expression<Func<TEntity, Guid>> property, string dbName = null)
            where TEntity : class, new()
        {
            var primitiveProperty = entityType.Property(property);

            primitiveProperty.IsRequired();

            primitiveProperty.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            if (!string.IsNullOrEmpty(dbName)) primitiveProperty.HasColumnName(dbName);

            return entityType;
        }

        public static EntityTypeConfiguration<TEntity> HasIdentityKey<TEntity>(this EntityTypeConfiguration<TEntity> entityType, Expression<Func<TEntity, int>> property, string dbName = null)
            where TEntity : class, new()
        {
            entityType.HasKey(property);

            entityType.HasIdentity(property);

            var primitiveProperty = entityType.Property(property);

            if (!string.IsNullOrEmpty(dbName)) primitiveProperty.HasColumnName(dbName);

            return entityType;
        }

        public static EntityTypeConfiguration<TEntity> HasIdentityKey<TEntity>(this EntityTypeConfiguration<TEntity> entityType, Expression<Func<TEntity, long>> property, string dbName = null)
            where TEntity : class, new()
        {
            entityType.HasKey(property);

            entityType.HasIdentity(property);

            var primitiveProperty = entityType.Property(property);

            if (!string.IsNullOrEmpty(dbName)) primitiveProperty.HasColumnName(dbName);

            return entityType;
        }

        public static EntityTypeConfiguration<TEntity> HasCompoundKey<TEntity, TEntityKey>(this EntityTypeConfiguration<TEntity> entityType, Expression<Func<TEntity, TEntityKey>> selector)
            where TEntity : class, new()
            where TEntityKey : class
        {
            entityType.HasKey(selector);
            
            return entityType;
        }

        public static EntityTypeConfiguration<TEntity> HasPrimaryKey<TEntity>(this EntityTypeConfiguration<TEntity> entityType, Expression<Func<TEntity, Guid>> property, string dbName = null)
            where TEntity : class, new()
        {
            entityType.HasKey(property);

            var primitiveProperty = entityType.Property(property).IsRequired();

            if (!string.IsNullOrEmpty(dbName)) primitiveProperty.HasColumnName(dbName);

            return entityType;
        }

        public static EntityTypeConfiguration<TEntity> HasPrimaryKey<TEntity>(this EntityTypeConfiguration<TEntity> entityType, Expression<Func<TEntity, string>> property, string dbName = null, int? maxLength = null, bool unicode = true, bool fixedLength = false)
            where TEntity : class, new()
        {
            entityType.HasKey(property);
            
            entityType.HasRequired(property, dbName, maxLength, unicode, fixedLength);

            return entityType;
        }

        public static EntityTypeConfiguration<TEntity> HasRequired<TEntity, TProperty>(this EntityTypeConfiguration<TEntity> entityType, Expression<Func<TEntity, TProperty>> property, string dbName = null)
            where TEntity : class, new()
            where TProperty : struct
        {
            var primitiveProperty = entityType.Property(property).IsRequired();

            if (!string.IsNullOrEmpty(dbName)) primitiveProperty.HasColumnName(dbName);

            return entityType;
        }

        public static StringPropertyConfiguration HasProperty<TEntity>(this EntityTypeConfiguration<TEntity> entityType, Expression<Func<TEntity, string>> property, string dbName = null, int? maxLength = null, bool unicode = true, bool fixedLength = false)
            where TEntity : class, new()
        {
            var stringProperty = entityType.Property(property);

            if (!string.IsNullOrEmpty(dbName)) stringProperty.HasColumnName(dbName);

            if (maxLength.HasValue) stringProperty.HasMaxLength(maxLength.Value);

            stringProperty.IsUnicode(unicode);

            if (fixedLength) stringProperty.IsFixedLength(); else stringProperty.IsVariableLength();

            return stringProperty;
        }

        public static EntityTypeConfiguration<TEntity> HasOptional<TEntity>(this EntityTypeConfiguration<TEntity> entityType, Expression<Func<TEntity, string>> property, string dbName = null, int? maxLength = null, bool unicode = true, bool fixedLength = false)
            where TEntity : class, new()
        {
            entityType.HasProperty(property, dbName, maxLength, unicode, fixedLength).IsOptional();

            return entityType;
        }

        public static BinaryPropertyConfiguration HasProperty<TEntity>(this EntityTypeConfiguration<TEntity> entityType, Expression<Func<TEntity, byte[]>> property, string dbName = null, int? maxLength = null, bool fixedLength = false)
            where TEntity : class, new()
        {
            var binaryProperty = entityType.Property(property);

            if (!string.IsNullOrEmpty(dbName)) binaryProperty.HasColumnName(dbName);

            if (maxLength.HasValue) binaryProperty.HasMaxLength(maxLength.Value);

            if (fixedLength) binaryProperty.IsFixedLength(); else binaryProperty.IsVariableLength();

            return binaryProperty;
        }

        public static EntityTypeConfiguration<TEntity> HasOptional<TEntity>(this EntityTypeConfiguration<TEntity> entityType, Expression<Func<TEntity, byte[]>> property, string dbName = null, int? maxLength = null, bool fixedLength = false)
            where TEntity : class, new()
        {
            entityType.HasProperty(property, dbName, maxLength, fixedLength).IsOptional();
            
            return entityType;
        }

        public static EntityTypeConfiguration<TEntity> HasRequired<TEntity>(this EntityTypeConfiguration<TEntity> entityType, Expression<Func<TEntity, byte[]>> property, string dbName = null, int? maxLength = null, bool fixedLength = false)
            where TEntity : class, new()
        {
            entityType.HasProperty(property, dbName, maxLength, fixedLength).IsRequired();

            return entityType;
        }

        public static EntityTypeConfiguration<TEntity> HasOptional<TEntity, TTarget, TTargetKey>(this EntityTypeConfiguration<TEntity> entityType, Expression<Func<TEntity, TTarget>> property, Expression<Func<TEntity, TTargetKey>> foreignKey, Expression<Func<TTarget, ICollection<TEntity>>> many = null, bool? cascade = null)
            where TEntity : class, new()
            where TTarget : class, new()
        {
            var navigationProperty = entityType.HasOptional(property);

            var dependentPropery = many != null ? navigationProperty.WithMany(many) : navigationProperty.WithMany();

            dependentPropery.HasForeignKey(foreignKey);

            if (cascade.HasValue) dependentPropery.WillCascadeOnDelete(cascade.Value);

            return entityType;
        }

        public static EntityTypeConfiguration<TEntity> HasRequired<TEntity, TTarget, TTargetKey>(this EntityTypeConfiguration<TEntity> entityType, Expression<Func<TEntity, TTarget>> property, Expression<Func<TEntity, TTargetKey>> foreignKey, Expression<Func<TTarget, ICollection<TEntity>>> many = null, bool? cascade = null)
            where TEntity : class, new()
            where TTarget : class, new()
        {
            var navigationProperty = entityType.HasRequired(property);

            var dependentPropery = many != null ? navigationProperty.WithMany(many) : navigationProperty.WithMany();

            dependentPropery.HasForeignKey(foreignKey);

            if (cascade.HasValue) dependentPropery.WillCascadeOnDelete(cascade.Value);

            return entityType;
        }

        public static EntityTypeConfiguration<TEntity> HasPrincipal<TEntity, TTarget, TTargetKey>(this EntityTypeConfiguration<TEntity> entityType, Expression<Func<TEntity, TTarget>> property, Expression<Func<TEntity, TTargetKey>> foreignKey, Expression<Func<TTarget, TEntity>> principal = null, bool? cascade = null)
            where TEntity : class, new()
            where TTarget : class, new()
        {
            var navigationProperty = entityType.HasRequired(property);

            var dependentPropery = principal != null ? navigationProperty.WithOptional(principal) : navigationProperty.WithOptional();

            //dependentPropery.Map(x => x.MapKey().HasForeignKey(foreignKey);

            if (cascade.HasValue) dependentPropery.WillCascadeOnDelete(cascade.Value);

            return entityType;
        }

        public static EntityTypeConfiguration<TEntity> HasOptional<TEntity, TProperty>(this EntityTypeConfiguration<TEntity> entityType, Expression<Func<TEntity, TProperty?>> property, string dbName = null)
            where TEntity : class, new()
            where TProperty : struct
        {
            var primitiveProperty = entityType.Property(property).IsOptional();

            if (!string.IsNullOrEmpty(dbName)) primitiveProperty.HasColumnName(dbName);

            return entityType;
        }

        public static EntityTypeConfiguration<TEntity> HasOptional<TEntity, TTarget>(this EntityTypeConfiguration<TEntity> entityType, Expression<Func<TEntity, TTarget>> property, Expression<Func<TEntity, string>> foreignKey, Expression<Func<TTarget, ICollection<TEntity>>> many = null, bool? cascade = null)
            where TEntity : class, new()
            where TTarget : class, new()
        {
            var navigationProperty = entityType.HasOptional(property);

            var dependentPropery = many != null ? navigationProperty.WithMany(many) : navigationProperty.WithMany();

            dependentPropery.HasForeignKey(foreignKey);

            if (cascade.HasValue) dependentPropery.WillCascadeOnDelete(cascade.Value);

            return entityType;
        }

        public static EntityTypeConfiguration<TEntity> HasOptional<TEntity, TTarget, TTargetKey>(this EntityTypeConfiguration<TEntity> entityType, Expression<Func<TEntity, TTarget>> property, Expression<Func<TEntity, TTargetKey?>> foreignKey, Expression<Func<TTarget, ICollection<TEntity>>> many = null, bool? cascade = null)
            where TEntity : class, new()
            where TTarget : class, new()
            where TTargetKey : struct
        {
            var navigationProperty = entityType.HasOptional(property);

            var dependentPropery = many != null ? navigationProperty.WithMany(many) : navigationProperty.WithMany();

            dependentPropery.HasForeignKey(foreignKey);

            if (cascade.HasValue) dependentPropery.WillCascadeOnDelete(cascade.Value);

            return entityType;
        }

        public static EntityTypeConfiguration<TEntity> HasOptional<TEntity>(this EntityTypeConfiguration<TEntity> entityType, Expression<Func<TEntity, DateTime?>> property, string dbName = null, byte? precision = null)
            where TEntity : class, new()
        {
            var dtProperty = entityType.Property(property).IsOptional();

            if (!string.IsNullOrEmpty(dbName)) dtProperty.HasColumnName(dbName);

            if (precision.HasValue) dtProperty.HasPrecision(precision.Value);

            return entityType;
        }

        public static EntityTypeConfiguration<TEntity> HasRequired<TEntity>(this EntityTypeConfiguration<TEntity> entityType, Expression<Func<TEntity, DateTime>> property, string dbName = null, byte? precision = null)
            where TEntity : class, new()
        {
            var dtProperty = entityType.Property(property).IsRequired();

            if (!string.IsNullOrEmpty(dbName)) dtProperty.HasColumnName(dbName);

            if (precision.HasValue) dtProperty.HasPrecision(precision.Value);

            return entityType;
        }

        public static EntityTypeConfiguration<TEntity> HasRequired<TEntity>(this EntityTypeConfiguration<TEntity> entityType, Expression<Func<TEntity, string>> property, string dbName = null, int? maxLength = null, bool unicode = true, bool fixedLength = false)
            where TEntity : class, new()
        {
            entityType.HasProperty(property, dbName, maxLength, unicode, fixedLength).IsRequired();
            
            return entityType;
        }
    }
}