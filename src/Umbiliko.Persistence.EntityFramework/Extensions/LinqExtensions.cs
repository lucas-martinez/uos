﻿using System.Linq;

namespace Uos
{
    using Inquiry;

    /// <summary>Refer to http://www.albahari.com/nutshell/linqkit.html and
    /// http://tomasp.net/blog/linq-expand.aspx for more information.</summary>
    public static class LinqExtensions
    {
        public static IQueryable<T> AsExpandableAsync<T>(this IQueryable<T> query)
        {
            if (query is ExpandableDbAsyncQuery<T>) return (ExpandableDbAsyncQuery<T>)query;

            if (query is ExpandableQuery<T>)
            {
                var expandable = (ExpandableQuery<T>)query;

                return new ExpandableDbAsyncQuery<T>(expandable.InnerQuery);
            }

            return new ExpandableDbAsyncQuery<T>(query);
        }
    }
}
