﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure.DependencyResolution;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Persistence.EntityFramework
{
    using Uos.Assertion;

    public static class DbDependencyResolverExtensions
    {
        public static T GetService<T>(this IDbDependencyResolver resolver)
        {
            Assert.NotNull<IDbDependencyResolver>(resolver, "resolver");
            return (T)resolver.GetService(typeof(T), null);
        }

        public static T GetService<T>(this IDbDependencyResolver resolver, object key)
        {
            Assert.NotNull<IDbDependencyResolver>(resolver, "resolver");
            return (T)resolver.GetService(typeof(T), key);
        }

        public static object GetService(this IDbDependencyResolver resolver, Type type)
        {
            Assert.NotNull<IDbDependencyResolver>(resolver, "resolver");
            Assert.NotNull<Type>(type, "type");
            return resolver.GetService(type, null);
        }

        internal static IEnumerable<object> GetServiceAsServices(this IDbDependencyResolver resolver, Type type, object key)
        {
            object service = resolver.GetService(type, key);
            if (service != null)
            {
                return new object[] { service };
            }
            return Enumerable.Empty<object>();
        }

        public static IEnumerable<T> GetServices<T>(this IDbDependencyResolver resolver)
        {
            Assert.NotNull<IDbDependencyResolver>(resolver, "resolver");
            return resolver.GetServices(typeof(T), null).OfType<T>();
        }

        public static IEnumerable<T> GetServices<T>(this IDbDependencyResolver resolver, object key)
        {
            Assert.NotNull<IDbDependencyResolver>(resolver, "resolver");
            return resolver.GetServices(typeof(T), key).OfType<T>();
        }

        public static IEnumerable<object> GetServices(this IDbDependencyResolver resolver, Type type)
        {
            Assert.NotNull<IDbDependencyResolver>(resolver, "resolver");
            Assert.NotNull<Type>(type, "type");
            return resolver.GetServices(type, null);
        }
    }
}
