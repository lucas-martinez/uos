﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Compilation;

namespace Uos.Web
{
    using Contracts;

    public static class SiteHelper
    {
        static readonly UmbilikoSettings _settings;
        static string _domainName;
        static string _siteName;

        static SiteHelper()
        {
            _settings = new UmbilikoSettings();
        }

        public static UmbilikoSettings Settings
        {
            get { return _settings; }
        }

        public static string GetDomainName(this HttpContext httpContext)
        {
            return _domainName ?? (_domainName = httpContext.Request.Url.Host);
        }

        public static string GetCurrentSiteName()
        {
            Func<Assembly, string> getAssemblyProduct = (assembly) =>
            {
                var attribute = assembly.GetCustomAttributes(typeof(AssemblyProductAttribute), false).OfType<AssemblyProductAttribute>().FirstOrDefault();

                return attribute != null ? attribute.Product : null;
            };

            return _siteName ?? (_siteName = getAssemblyProduct(BuildManager.GetGlobalAsaxType().BaseType.Assembly));
        }

        public static DomainInfo GetCurrentSite(this HttpContext httpContext)
        {
            var siteName = GetCurrentSiteName();
            var domainName = GetDomainName(httpContext);

            return null;
        }
    }
}
