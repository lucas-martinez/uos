﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Models
{
    public class RobotPostModel
    {
        public string Client { get; set; }

        public string Machine { get; set; }
    }
}
