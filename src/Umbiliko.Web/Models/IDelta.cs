﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Umbiliko.Web.Models
{
    public interface IDelta
    {
        void Clear();

        IEnumerable<string> GetChangedPropertyNames();

        IEnumerable<string> GetUnchangedPropertyNames();

        bool TryGetPropertyType(string name, out Type type);

        bool TryGetPropertyValue(string name, out object value);

        bool TrySetPropertyValue(string name, object value);
    }
}

