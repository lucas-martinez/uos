﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Models
{
    using Binders;

    [DataContract, DataContractBinder]
    public class AccountExitRequest
    {
        public Guid SessionId { get; set; }

        public string IpAddress { get; set; }
    }
}
