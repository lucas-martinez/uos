﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Models
{
    public class RobotRowModel
    {
        public Guid AgentId { get; set; }

        public int ProcessId { get; set; }
        
        public string MachineName { get; set; }

        public int CompletedTasksCount { get; set; }

        public int PendingTasksCount { get; set; }

        public int RunningTasksCount { get; set; }
    }
}
