﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.OData.Properties;

namespace Umbiliko.Web.Models
{

        internal abstract class PropertyAccessor<TEntityType> where TEntityType : class
        {
            protected PropertyAccessor(PropertyInfo property)
            {
                if (property == null)
                {
                    throw Error.ArgumentNull("property");
                }
                this.Property = property;
                if ((this.Property.GetGetMethod() == null) || (!this.Property.PropertyType.IsCollection() && (this.Property.GetSetMethod() == null)))
                {
                    throw Error.Argument("property", SRResources.PropertyMustHavePublicGetterAndSetter, new object[0]);
                }
            }

            public void Copy(TEntityType from, TEntityType to)
            {
                if (from == null)
                {
                    throw Error.ArgumentNull("from");
                }
                if (to == null)
                {
                    throw Error.ArgumentNull("to");
                }
                this.SetValue(to, this.GetValue(from));
            }

            public abstract object GetValue(TEntityType entity);

            public abstract void SetValue(TEntityType entity, object value);

            public PropertyInfo Property { get; private set; }
        }
    }