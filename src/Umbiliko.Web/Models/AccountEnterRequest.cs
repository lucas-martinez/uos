﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Models
{
    using Contracts;
    using Binders;
    
    [DataContract, DataContractBinder]
    public class AccountEnterRequest
    {
        [DataMember(Name = "data")]
        public UserInfo Data { get; set; }

        [DataMember(Name = "name"), Required]
        public string Name { get; set; }

        [DataMember(Name = "password")]
        public string Password { get; set; }
    }
}
