﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Models
{
    public class AreaDeleteModel
    {
        [Required]
        public int Id { get; set; }
    }
}
