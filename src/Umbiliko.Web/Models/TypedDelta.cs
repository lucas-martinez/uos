﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Umbiliko.Web.Models
{
    public abstract class TypedDelta : Delta
    {
        protected TypedDelta()
        {
        }

        public abstract Type EntityType { get; }

        public abstract Type ExpectedClrType { get; }
    }
}
