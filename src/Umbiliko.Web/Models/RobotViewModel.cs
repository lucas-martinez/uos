﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Models
{
    public class RobotViewModel
    {
        public Guid AgentId { get; set; }
        
        public string[] CompletedTasks { get; set; }

        public string MachineName { get; set; }

        public string[] PendingTasks { get; set; }

        public int ProcessId { get; set; }

        public string[] RunningTasks { get; set; }
    }
}
