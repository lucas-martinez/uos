﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.OData.Formatter;
using System.Web.Http.OData.Properties;

namespace Umbiliko.Web.Models
{
    //
    // Summary:
    //     A class the tracks changes (i.e. the delta) for a particular TEntityType.
    //
    // Type parameters:
    //   TEntityType:
    //     TEntityType is the base type of entity this delta tracks changes for.
    //[NonValidatingParameterBinding]
    public class Delta<TEntityType> : TypedDelta, IDelta where TEntityType : class
    {
        private Dictionary<string, PropertyAccessor<TEntityType>> _allProperties;
        private HashSet<string> _changedProperties;
        private TEntityType _entity;
        private Type _entityType;
        private static ConcurrentDictionary<Type, Dictionary<string, PropertyAccessor<TEntityType>>> _propertyCache;
        private HashSet<string> _updatableProperties;

        static Delta()
        {
            Delta<TEntityType>._propertyCache = new ConcurrentDictionary<Type, Dictionary<string, PropertyAccessor<TEntityType>>>();
        }

        public Delta() : this(typeof(TEntityType))
        {
        }

        public Delta(Type entityType) : this(entityType, null)
        {
        }

        public Delta(Type entityType, IEnumerable<string> updatableProperties)
        {
            this.Reset(entityType);
            this.InitializeProperties(updatableProperties);
        }

        public override void Clear()
        {
            this.Reset(this._entityType);
        }

        public void CopyChangedValues(TEntityType original)
        {
            if (original == null)
            {
                throw new ArgumentNullException("original");
            }
            if (!this._entityType.IsAssignableFrom(original.GetType()))
            {
                throw Error.Argument("original", SRResources.DeltaTypeMismatch, new object[] { this._entityType, original.GetType() });
            }
            foreach (PropertyAccessor<TEntityType> accessor in (from s in this.GetChangedPropertyNames() select base._allProperties[s]).ToArray<PropertyAccessor<TEntityType>>())
            {
                accessor.Copy(this._entity, original);
            }
        }

        public void CopyUnchangedValues(TEntityType original)
        {
            if (original == null)
            {
                throw new ArgumentNullException("original");
            }
            if (!this._entityType.IsAssignableFrom(original.GetType()))
            {
                throw new ArgumentException("original", string.Format("Delta Type Mismatch", _entityType, original.GetType()));
            }
            foreach (PropertyAccessor<TEntityType> accessor in from s in this.GetUnchangedPropertyNames() select base._allProperties[s])
            {
                accessor.Copy(this._entity, original);
            }
        }

        public override IEnumerable<string> GetChangedPropertyNames()
        {
            return this._changedProperties;
        }

        public override IEnumerable<string> GetDynamicMemberNames()
        {
            return this._allProperties.Keys;
        }

        public TEntityType GetEntity()
        {
            return this._entity;
        }

        public override IEnumerable<string> GetUnchangedPropertyNames()
        {
            return this._updatableProperties.Except<string>(this.GetChangedPropertyNames());
        }

        private void InitializeProperties(IEnumerable<string> updatableProperties)
        {
            this._allProperties = Delta<TEntityType>._propertyCache.GetOrAdd(this._entityType, backingType => (from p in backingType.GetProperties().Where<PropertyInfo>(delegate (PropertyInfo p)
            {
                if ((p.GetSetMethod() == null) && !p.PropertyType.IsCollection())
                {
                    return false;
                }
                return p.GetGetMethod() != null;
            })
                                                                                                               select new FastPropertyAccessor<TEntityType>(p)).ToDictionary<PropertyAccessor<TEntityType>, string>(p => p.Property.Name));
            if (updatableProperties != null)
            {
                this._updatableProperties = new HashSet<string>(updatableProperties);
                this._updatableProperties.IntersectWith(this._allProperties.Keys);
            }
            else
            {
                this._updatableProperties = new HashSet<string>(this._allProperties.Keys);
            }
        }

        public void Patch(TEntityType original)
        {
            this.CopyChangedValues(original);
        }

        public void Put(TEntityType original)
        {
            this.CopyChangedValues(original);
            this.CopyUnchangedValues(original);
        }

        private void Reset(Type entityType)
        {
            if (entityType == null)
            {
                throw new ArgumentNullException("entityType");
            }
            if (!typeof(TEntityType).IsAssignableFrom(entityType))
            {
                throw new InvalidOperationException(string.Format("Delta Entity Type Not Assignable {0} {1}", entityType.Name, typeof(TEntityType).Name));
            }
            this._entity = Activator.CreateInstance(entityType) as TEntityType;
            this._changedProperties = new HashSet<string>();
            this._entityType = entityType;
        }

        public override bool TryGetPropertyType(string name, out Type type)
        {
            PropertyAccessor<TEntityType> accessor;
            if (name == null)
            {
                throw new ArgumentNullException("name");
            }
            if (this._allProperties.TryGetValue(name, out accessor))
            {
                type = accessor.Property.PropertyType;
                return true;
            }
            type = null;
            return false;
        }

        public override bool TryGetPropertyValue(string name, out object value)
        {
            PropertyAccessor<TEntityType> accessor;
            if (name == null)
            {
                throw new ArgumentNullException("name");
            }
            if (this._allProperties.TryGetValue(name, out accessor))
            {
                value = accessor.GetValue(this._entity);
                return true;
            }
            value = null;
            return false;
        }

        public override bool TrySetPropertyValue(string name, object value)
        {
            if (name == null)
            {
                throw new ArgumentNullException("name");
            }
            if (!this._updatableProperties.Contains(name))
            {
                return false;
            }
            PropertyAccessor<TEntityType> accessor = this._allProperties[name];
            if ((value == null) && !EdmLibHelpers.IsNullable(accessor.Property.PropertyType))
            {
                return false;
            }
            Type propertyType = accessor.Property.PropertyType;
            if (((value != null) && !propertyType.IsCollection()) && !propertyType.IsAssignableFrom(value.GetType()))
            {
                return false;
            }
            accessor.SetValue(this._entity, value);
            this._changedProperties.Add(name);
            return true;
        }

        public override Type EntityType
        {
            get
            {
                return this._entityType;
            }
        }

        public override Type ExpectedClrType
        {
            get
            {
                return typeof(TEntityType);
            }
        }
    }

    //[NonValidatingParameterBinding]
    public abstract class Delta : DynamicObject, IDelta
    {
        protected Delta()
        {
        }

        public abstract void Clear();

        public abstract IEnumerable<string> GetChangedPropertyNames();

        public abstract IEnumerable<string> GetUnchangedPropertyNames();

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            if (binder == null)
            {
                throw new ArgumentNullException("binder");
            }
            return this.TryGetPropertyValue(binder.Name, out result);
        }

        public abstract bool TryGetPropertyType(string name, out Type type);

        public abstract bool TryGetPropertyValue(string name, out object value);

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            if (binder == null)
            {
                throw new ArgumentNullException("binder");
            }
            return this.TrySetPropertyValue(binder.Name, value);
        }

        public abstract bool TrySetPropertyValue(string name, object value);
    }
}

