﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Uos.Web.Binders
{
    public class DataContractBinderAttribute : CustomModelBinderAttribute
    {
        public override IModelBinder GetBinder()
        {
            return new DataConrtactBinder(new[] { new NewtonsoftJsonModelBinder() });
        }
    }
}
