﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Uos.Web.Binders
{
    public class NewtonsoftJsonModelBinder : IFilteredModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (!controllerContext.HttpContext.Request.ContentType.ToLowerInvariant().Contains("json"))
            {
                // not JSON request
                return null;
            }

            var request = controllerContext.HttpContext.Request;
            request.InputStream.Position = 0;
            var incomingData = new StreamReader(request.InputStream).ReadToEnd();

            if (String.IsNullOrEmpty(incomingData))
            {
                // no JSON data
                return null;
            }
            object ret = JsonConvert.DeserializeObject(incomingData, bindingContext.ModelType);
            return ret;
        }

        public bool IsMatch(Type modelType)
        {
            // I then used JSON.net attributes to map to the different object properties (instead of DataContracts) 
            // on the models. The models all inherited from an empty base class JsonModel.
            
            var ret = true; //(typeof(JsonModel).IsAssignableFrom(modelType));
            return ret;
        }
    }
}
