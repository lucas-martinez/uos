﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Uos.Web.Binders
{
    public class DataConrtactBinder : DefaultModelBinder
    {
        private readonly IFilteredModelBinder[] _filteredModelBinders;

        public DataConrtactBinder(IFilteredModelBinder[] filteredModelBinders)
        {
            _filteredModelBinders = filteredModelBinders;
        }

        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            foreach (var filteredModelBinder in _filteredModelBinders)
            {
                if (filteredModelBinder.IsMatch(bindingContext.ModelType))
                {
                    return filteredModelBinder.BindModel(controllerContext, bindingContext);
                }
            }

            return base.BindModel(controllerContext, bindingContext);
        }
    }
}
