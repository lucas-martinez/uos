﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Uos.Web.Http
{
    public class HttpsAttribute : System.Web.Http.Filters.AuthorizationFilterAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (actionContext.Request.RequestUri.Scheme != Uri.UriSchemeHttps)
            {
                HttpResponseMessage response = actionContext.Request.CreateResponse(HttpStatusCode.Forbidden);
                response.Content = new StringContent("<h1>HTTPS Required</h1>", Encoding.UTF8, "text/html");
                response.ReasonPhrase = "HTTPS Required";
                actionContext.Response = response;
            }
            else
            {
                base.OnAuthorization(actionContext);
            }
        }
    }
}