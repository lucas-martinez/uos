﻿using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Uos.Web.Http
{
    using Contracts;

    public abstract class AuthorizationFilterAttribute : FilterAttribute, IAuthorizationFilter
    {
        HttpActionDescriptor _action;
        IAuthenticationContext _authentication;
        HttpActionContext _authorization;
        IHttpController _controller;
        IOwinContext _owinContext;
        private readonly object _typeId = new object();

        protected HttpActionDescriptor Action
        {
            get { return _action; }
        }

        protected IAuthenticationContext Authentication
        {
            get { return _authentication; }
        }

        protected HttpActionContext Authorization
        {
            get { return _authorization; }
        }

        protected IHttpController Controller
        {
            get { return _controller; }
        }

        public override object TypeId
        {
            get { return _typeId; }
        }

        protected abstract bool AuthorizeCore(HttpContextBase httpContext);

        Task<HttpResponseMessage> IAuthorizationFilter.ExecuteAuthorizationFilterAsync(HttpActionContext actionContext, CancellationToken cancellationToken, Func<Task<HttpResponseMessage>> continuation)
        {
            var request = actionContext.Request;

            _action = actionContext.ActionDescriptor;
            _controller = actionContext.ControllerContext.Controller;
            _authentication = ServicesContainer.Default.GetInstance<IAuthenticationContext>();

            /*if (request.RequestUri.Scheme != Uri.UriSchemeHttps)
            {
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.Forbidden);
                response.Content = new StringContent("<h1>HTTPS Required</h1>", Encoding.UTF8, "text/html");
                actionContext.Response = response;

                return new Task<HttpResponseMessage>(delegate () {
                    return response;
                });
            }
            else*/
                return continuation();
        }
    }
}
