﻿using System;
using System.Security.Principal;
using System.Web;

namespace Uos.Web.Http
{
    using Constants;

    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public class FeatureAttribute : AuthorizationFilterAttribute
    {
        string _activity;
        readonly string _feature;

        public FeatureAttribute(string name)
        {
            _feature = name;
        }

        public string Name
        {
            get { return _feature; }
        }

        public string Requires
        {
            get { return _activity; }
            set { _activity = value; }
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return Authentication.Authorize(activity: _activity, feature: _feature) == AuthorizeStatus.Success;
        }
    }
}
