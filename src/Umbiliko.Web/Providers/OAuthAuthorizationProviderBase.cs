﻿using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace Uos.Web.Providers
{
    using Contracts;
    
    public abstract class OAuthAuthorizationProviderBase : IOAuthAuthorizationServerProvider
    {
        private readonly string _publicClientId;

        protected OAuthAuthorizationProviderBase(string publicClientId)
        {
            if (publicClientId == null)
            {
                throw new ArgumentNullException("publicClientId");
            }

            _publicClientId = publicClientId;
        }

        protected abstract UserManager<SessionInfo, string> GetUserManager(IOwinContext owinContext);

        Task IOAuthAuthorizationServerProvider.AuthorizationEndpointResponse(OAuthAuthorizationEndpointResponseContext context)
        {
            throw new NotImplementedException();
        }

        public virtual async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var userManager = GetUserManager(context.OwinContext);

            SessionInfo session = await userManager.FindAsync(context.UserName, context.Password);

            if (session == null)
            {
                context.SetError("invalid_grant", "The user name or password is incorrect.");
                return;
            }

            ClaimsIdentity oauthIdentity = await userManager.CreateIdentityAsync(session, OAuthDefaults.AuthenticationType);

            ClaimsIdentity cookiesIdentity = await userManager.CreateIdentityAsync(session, CookieAuthenticationDefaults.AuthenticationType);

            var properties = new AuthenticationProperties
            {
                AllowRefresh = true,
                //IsPersistent = isPersistent,
                IssuedUtc = session.IssuedUtc,
                //RedirectUri = returnUrl,
                ExpiresUtc = session.ExpiresUtc
            };
            
            AuthenticationTicket ticket = new AuthenticationTicket(oauthIdentity, properties);

            context.Validated(ticket);

            context.Request.Context.Authentication.SignIn(cookiesIdentity);
        }

        public virtual Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        public virtual Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            string clientId;
            string clientSecret;

            if (context.TryGetBasicCredentials(out clientId, out clientSecret) || context.TryGetFormCredentials(out clientId, out clientSecret))
            {
                //UserManager<UserData, string> userManager = context.OwinContext.GetUserManager<UserManager<UserData, string>>();
                //OAuthDbContext dbContext = context.OwinContext.Get<OAuthDbContext>();

                try
                {
                    context.Validated();
                    /*Client client = await dbContext
                        .Clients
                        .FirstOrDefaultAsync(clientEntity => clientEntity.Id == clientId);

                    if (client != null &&
                        userManager.PasswordHasher.VerifyHashedPassword(
                            client.ClientSecretHash, clientSecret) == PasswordVerificationResult.Success)
                    {
                        // Client has been verified.
                        context.OwinContext.Set<Client>("oauth:client", client);
                        context.Validated(clientId);
                    }
                    else
                    {
                        // Client could not be validated.
                        context.SetError("invalid_client", "Client credentials are invalid.");
                        context.Rejected();
                    }*/
                }
                catch
                {
                    // Could not get the client through the IClientManager implementation.
                    context.SetError("server_error");
                    context.Rejected();
                }
            }
            else
            {
                // The client credentials could not be retrieved.
                context.SetError("invalid_client", "Client credentials could not be retrieved through the Authorization header.");

                context.Rejected();
            }

            // Resource owner password credentials does not provide a client ID.
            if (context.ClientId == null)
            {
                context.Validated();
            }

            return Task.FromResult<object>(null);
        }

        public virtual Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        {
            if (context.ClientId == _publicClientId)
            {
                Uri expectedRootUri = new Uri(context.Request.Uri, "/");

                if (expectedRootUri.AbsoluteUri == context.RedirectUri)
                {
                    context.Validated();
                }
            }

            return Task.FromResult<object>(null);
        }

        Task IOAuthAuthorizationServerProvider.AuthorizeEndpoint(OAuthAuthorizeEndpointContext context)
        {
            throw new NotImplementedException();
        }

        Task IOAuthAuthorizationServerProvider.GrantAuthorizationCode(OAuthGrantAuthorizationCodeContext context)
        {
            if (((context.Ticket != null) && (context.Ticket.Identity != null)) && context.Ticket.Identity.IsAuthenticated)
            {
                context.Validated();
            }

            return Task.FromResult<object>(null);
        }

        Task IOAuthAuthorizationServerProvider.GrantClientCredentials(OAuthGrantClientCredentialsContext context)
        {
            return Task.FromResult<object>(null);
        }

        Task IOAuthAuthorizationServerProvider.GrantCustomExtension(OAuthGrantCustomExtensionContext context)
        {
            return Task.FromResult<object>(null);
        }

        Task IOAuthAuthorizationServerProvider.GrantRefreshToken(OAuthGrantRefreshTokenContext context)
        {
            if (((context.Ticket != null) && (context.Ticket.Identity != null)) && context.Ticket.Identity.IsAuthenticated)
            {
                context.Validated();
            }

            return Task.FromResult<object>(null);
        }

        Task IOAuthAuthorizationServerProvider.GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            return GrantResourceOwnerCredentials(context);
        }

        Task IOAuthAuthorizationServerProvider.MatchEndpoint(OAuthMatchEndpointContext context)
        {
            return Task.FromResult<object>(null);
        }

        Task IOAuthAuthorizationServerProvider.TokenEndpoint(OAuthTokenEndpointContext context)
        {
            return TokenEndpoint(context);
        }

        Task IOAuthAuthorizationServerProvider.TokenEndpointResponse(OAuthTokenEndpointResponseContext context)
        {
            throw new NotImplementedException();
        }

        Task IOAuthAuthorizationServerProvider.ValidateAuthorizeRequest(OAuthValidateAuthorizeRequestContext context)
        {
            context.Validated();

            return Task.FromResult<object>(null);
        }

        Task IOAuthAuthorizationServerProvider.ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            return ValidateClientAuthentication(context);
        }

        Task IOAuthAuthorizationServerProvider.ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        {
            return ValidateClientRedirectUri(context);
        }

        Task IOAuthAuthorizationServerProvider.ValidateTokenRequest(OAuthValidateTokenRequestContext context)
        {
            context.Validated();

            return Task.FromResult<object>(null);
        }
    }
}
