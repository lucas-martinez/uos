﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Compilation;
using System.Web.Http;
using System.Web.Http.Description;

namespace Uos.Web.Providers
{
    using Constants;
    using Contracts;

    public class ProductDescriptionProvider
    {
        Assembly _assembly;
        Dictionary<string, FeatureDescription> _features;
        HttpConfiguration _httpConfiguration;
        ProductDescription _product;

        public ProductDescriptionProvider(Assembly productAssembly)
        {
            _assembly = productAssembly;
        }

        protected FeatureDescription this[string name]
        {
            get
            {
                FeatureDescription feature;

                if (!_features.TryGetValue(name, out feature))
                {
                    feature = new FeatureDescription
                    {
                        Name = name
                    };

                    _features.Add(name, feature);
                }

                return feature;
            }
        }

        public Assembly Assembly
        {
            get { return _assembly; }
        }

        public ProductDescription Description
        {
            get { return _product ?? (_product = Describe()); }
        }

        public HttpConfiguration HttpConfiguration
        {
            get { return _httpConfiguration ?? (_httpConfiguration = ServicesContainer.Default.GetInstance<HttpConfiguration>()); }
        }

        public ProductDescription Describe()
        {
            var product = new ProductDescription
            {
                Company = Assembly.GetCustomAttributes<AssemblyCompanyAttribute>().Select(attr => attr.Company).FirstOrDefault(),
                Copyright = Assembly.GetCustomAttributes<AssemblyCopyrightAttribute>().Select(attr => attr.Copyright).FirstOrDefault(),
                Description = Assembly.GetCustomAttributes<AssemblyDescriptionAttribute>().Select(attr => attr.Description).FirstOrDefault(),
                Name = Assembly.GetCustomAttributes<AssemblyProductAttribute>().Select(attr => attr.Product).FirstOrDefault(),
                Title = Assembly.GetCustomAttributes<AssemblyTitleAttribute>().Select(attr => attr.Title).FirstOrDefault(),
                Trademark = Assembly.GetCustomAttributes<AssemblyTrademarkAttribute>().Select(attr => attr.Trademark).FirstOrDefault(),
                Verions = Assembly.GetName().Version.ToString()
            };

            _features = new Dictionary<string, FeatureDescription>();

            AddHttpActivities();

            AddMvcActivities();

            product.Features = _features.Values.OrderBy(feature => feature.Name).ToList();

            _features = null;

            return product;
        }

        protected void AddHttpActivities()
        {
            var documentation = HttpConfiguration.Services.GetDocumentationProvider();

            var apiDescriptions = HttpConfiguration.Services.GetApiExplorer().ApiDescriptions;

            Func<ApiDescription, string> featureSelector = (description) => description.ActionDescriptor.GetCustomAttributes<Http.ActivityAttribute>()
                .Select(attr => attr.Feature)
                .SingleOrDefault() ?? description.ActionDescriptor.ControllerDescriptor.GetCustomAttributes<Http.FeatureAttribute>()
                .Select(attr => attr.Name)
                .SingleOrDefault() ?? string.Empty;

            var apiGroups = apiDescriptions.GroupBy(featureSelector);

            foreach (var group in apiGroups)
            {
                var feature = this[group.Key];

                foreach (var api in group)
                {
                    var activity = api.ActionDescriptor.GetCustomAttributes<Http.ActivityAttribute>().Select(attr => new ActivityDescription
                    {
                        Level = Enum.GetName(typeof(AccessLevels), attr.Level),
                        Name = attr.Name,
                        Roles = attr.Roles
                    }).SingleOrDefault() ?? new ActivityDescription();

                    feature.Activities.Add(activity);

                    activity.Action = api.ActionDescriptor.ActionName;
                    activity.Controller = api.ActionDescriptor.ControllerDescriptor.ControllerName;
                    activity.AcceptMethods = api.HttpMethod.Method;
                    activity.Route = api.RelativePath;

                    var attributes = api.ActionDescriptor.GetCustomAttributes<Attribute>().Select(attr => attr.GetType().Name.TrimEnd("Attribute"))
                        .Union(api.ActionDescriptor.ControllerDescriptor.GetCustomAttributes<Attribute>().Select(attr => attr.GetType().Name.TrimEnd("Attribute")))
                        .OrderBy(name => name)
                        .Distinct()
                        .ToList();

                    activity.Attributes = string.Join(" ", attributes);

                    UpdateRoles(activity, 
                        api.ActionDescriptor.GetCustomAttributes<System.Web.Http.AuthorizeAttribute>().Select(attr => attr.Roles).SingleOrDefault(),
                        api.ActionDescriptor.ControllerDescriptor.GetCustomAttributes<System.Web.Http.AuthorizeAttribute>().Select(attr => attr.Roles).SingleOrDefault());
                }
            }
        }

        protected void AddMvcActivities()
        {
            Func<MethodInfo, bool> actionSelector = (method) => method.IsPublic
                && !method.IsAbstract
                && !method.IsDefined(typeof(NonActionAttribute))
                && !method.IsDefined(typeof(System.Runtime.CompilerServices.CompilerGeneratedAttribute), true)
                && !method.IsGenericMethod
                && !method.IsGenericMethodDefinition
                && !method.Name.StartsWith("get_")
                && !method.Name.StartsWith("set_");

            var mvcMethods = Assembly.GetTypes()
                .Where(type => typeof(System.Web.Mvc.Controller).IsAssignableFrom(type)) //filter controllers
                .SelectMany(type => type.GetMethods(BindingFlags.Instance | BindingFlags.DeclaredOnly | BindingFlags.Public))
                .Where(actionSelector)
                .ToList();

            Func<MethodInfo, string> featureSelector = (method) => method.GetCustomAttributes<Mvc.ActivityAttribute>()
                .Select(attr => attr.Feature)
                .SingleOrDefault() ?? method.DeclaringType.GetCustomAttributes<Mvc.FeatureAttribute>()
                .Select(attr => attr.Name)
                .SingleOrDefault() ?? string.Empty;

            var mvcGroups = mvcMethods.GroupBy(featureSelector);

            foreach (var group in mvcGroups)
            {
                var feature = this[group.Key];

                foreach (var method in group)
                {
                    var activity = method.GetCustomAttributes<Mvc.ActivityAttribute>().Select(attr => new ActivityDescription
                    {
                        Level = Enum.GetName(typeof(AccessLevels), attr.Level),
                        Name = attr.Name,
                        Roles = attr.Roles
                    }).SingleOrDefault() ?? new ActivityDescription();

                    feature.Activities.Add(activity);

                    var route = method.GetCustomAttributes<System.Web.Mvc.RouteAttribute>().Select(attr => attr.Template).SingleOrDefault();
                    var routeArea = method.DeclaringType.GetCustomAttributes<System.Web.Mvc.RouteAreaAttribute>().Select(attr => attr.AreaName).SingleOrDefault();
                    var routePrefix = method.DeclaringType.GetCustomAttributes<System.Web.Mvc.RoutePrefixAttribute>().Select(attr => attr.Prefix).SingleOrDefault();

                    activity.Action = method.Name;
                    activity.Area = routeArea;
                    activity.Controller = method.DeclaringType.Name.TrimEnd("Controller");
                    activity.ResultType = method.ReturnType.Name;

                    var attributes = method.GetCustomAttributes().Select(attr => attr.GetType().Name.TrimEnd("Attribute"))
                        .Union(method.DeclaringType.GetCustomAttributes().Select(attr => attr.GetType().Name.TrimEnd("Attribute")))
                        .OrderBy(name => name)
                        .Distinct()
                        .ToList();

                    activity.Attributes = string.Join(" ", attributes);

                    var methods = method.GetCustomAttributes<System.Web.Mvc.AcceptVerbsAttribute>().SelectMany(attr => attr.Verbs).Union(
                        new[]
                        {
                            method.GetCustomAttributes<System.Web.Mvc.HttpDeleteAttribute>().Select(attr => HttpMethod.Delete.Method).SingleOrDefault(),
                            method.GetCustomAttributes<System.Web.Mvc.HttpGetAttribute>().Select(attr => HttpMethod.Get.Method).SingleOrDefault(),
                            method.GetCustomAttributes<System.Web.Mvc.HttpHeadAttribute>().Select(attr => HttpMethod.Head.Method).SingleOrDefault(),
                            method.GetCustomAttributes<System.Web.Mvc.HttpOptionsAttribute>().Select(attr => HttpMethod.Options.Method).SingleOrDefault(),
                            method.GetCustomAttributes<System.Web.Mvc.HttpPatchAttribute>().Select(attr => @"PATCH").SingleOrDefault(),
                            method.GetCustomAttributes<System.Web.Mvc.HttpPostAttribute>().Select(attr => HttpMethod.Post.Method).SingleOrDefault(),
                            method.GetCustomAttributes<System.Web.Mvc.HttpPutAttribute>().Select(attr => HttpMethod.Put.Method).SingleOrDefault()
                        }.Where(m => m != null)).ToList();

                    activity.AcceptMethods = string.Join(" ", methods);

                    activity.Route = string.Join("/", new string[] { routeArea, routePrefix, route }.Where(item => !string.IsNullOrEmpty(item)));

                    UpdateRoles(activity, 
                        method.GetCustomAttributes<System.Web.Mvc.AuthorizeAttribute>().Select(attr => attr.Roles).SingleOrDefault(),
                        method.DeclaringType.GetCustomAttributes<System.Web.Mvc.AuthorizeAttribute>().Select(attr => attr.Roles).SingleOrDefault());
                }
            }
        }

        readonly static char[] RolesSplitCharacters = new[] { Characters.Space, Characters.Comma, Characters.Bar };

        protected void UpdateRoles(ActivityDescription activity, params string[] requiredRoles)
        {
            var roles = requiredRoles.Where(s => s != null).SelectMany(s => s.Split(RolesSplitCharacters, StringSplitOptions.RemoveEmptyEntries)).Distinct().ToList();

            UpdateRoles(activity, roles);
        }

        protected void UpdateRoles(ActivityDescription activity, List<string> requiredRoles)
        {
            if (activity.Roles != null)
            {
                var roles = activity.Roles.Split(RolesSplitCharacters, StringSplitOptions.RemoveEmptyEntries).Where(role => !requiredRoles.Contains(role)).Distinct().ToList();

                roles.AddRange(requiredRoles.Select(role => role + Characters.Exclamation));

                roles.Sort(StringComparer.OrdinalIgnoreCase);

                activity.Roles = string.Join(" ", roles);
            }
        }
    }
}
