using System;
using System.Reflection;

namespace Uos.Web.Providers
{
    public interface IModelDocumentationProvider
    {
        string GetDocumentation(MemberInfo member);

        string GetDocumentation(Type type);
    }
}