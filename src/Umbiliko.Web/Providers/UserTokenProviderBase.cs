﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Providers
{
    using Contracts;

    public class UserTokenProviderBase : TokenProviderBase, IUserTokenProvider<SessionInfo, string>
    {
        Task<string> IUserTokenProvider<SessionInfo, string>.GenerateAsync(string purpose, UserManager<SessionInfo, string> manager, SessionInfo user)
        {
            throw new NotImplementedException();
        }

        Task<bool> IUserTokenProvider<SessionInfo, string>.IsValidProviderForUserAsync(UserManager<SessionInfo, string> manager, SessionInfo user)
        {
            throw new NotImplementedException();
        }

        Task IUserTokenProvider<SessionInfo, string>.NotifyAsync(string token, UserManager<SessionInfo, string> manager, SessionInfo user)
        {
            throw new NotImplementedException();
        }

        Task<bool> IUserTokenProvider<SessionInfo, string>.ValidateAsync(string purpose, string token, UserManager<SessionInfo, string> manager, SessionInfo user)
        {
            throw new NotImplementedException();
        }
    }
}
