﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataProtection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Web;
using System.Web.Compilation;
using System.Threading.Tasks;

namespace Uos.Web.Providers
{
    using Constants;
    using Contracts;
    using Models;
    using Results;
    using Services;
    
    public abstract class AuthenticationContextBase : IAuthenticationContext
    {
        static string _productName;
        string _bearerToken;
        HttpContext _httpContext;
        IAuthenticationManager _authenticationManager;
        protected IOwinContext _owinContext;
        IDataProtectionProvider _protectionProvider;
        SignInManager<SessionInfo, string> _signInManager;
        UserManager<SessionInfo, string> _userManager;
        readonly DomainStoreBase _domainStore;
        DomainInfo _currentDomain;
        DomainInfo _referredomain;
        protected SessionInfo _currentSession;

        private AuthenticationContextBase(DomainStoreBase domainStore)
        {
            _domainStore = domainStore;
        }

        protected AuthenticationContextBase(DomainStoreBase domainStore, HttpContext httpContext)
            : this(domainStore)
        {
            _httpContext = httpContext;
        }

        protected AuthenticationContextBase(DomainStoreBase domainStore, HttpContextBase httpContext)
            : this(domainStore, httpContext.ApplicationInstance.Context)
        {
        }

        protected AuthenticationContextBase(DomainStoreBase domainStore, IOwinContext owinContext, IDataProtectionProvider protectionProvider)
            : this(domainStore)
        {
            _owinContext = owinContext;
            _protectionProvider = protectionProvider;
        }

        public string BearerToken
        {
            get
            {
                Func<IOwinContext, string> GetToken = (owinContext) =>
                {
                    var oauthTokens = owinContext.Request.Headers
                        .Where(h => string.Equals(h.Key, "Authorization", StringComparison.OrdinalIgnoreCase))
                        .SelectMany(h => h.Value)
                        .Where(v => v.StartsWith("Bearer ", StringComparison.OrdinalIgnoreCase))
                        .Select(v => v.TrimStart("Bearer ").Trim())
                        .ToList();

                    return oauthTokens.FirstOrDefault();
                };

                return _bearerToken ?? (_bearerToken = GetToken(OwinContext));
            }
        }

        public DomainInfo CurrentDomain
        {
            get { return _currentDomain ?? (_currentDomain = DomainStore.FindAsync(Url.Host).Result); }
        }

        public SessionInfo CurrentSession
        {
            get { return _currentSession ?? (_currentSession = Manager.User); }
            set
            {
                _currentSession = value;

                if (_currentSession != null)
                {
                    Manager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                }

                _currentSession = value;

                if (_currentSession != null)
                {
                    Manager.SignIn(new AuthenticationProperties()
                    {
                        AllowRefresh = true,
                        IssuedUtc = _currentSession.IssuedUtc,
                        ExpiresUtc = _currentSession.ExpiresUtc
                    }, _currentSession);
                }
            }
        }

        public DomainStoreBase DomainStore
        {
            get { return _domainStore; }
        }

        public HttpContext HttpContext
        {
            get { return _httpContext ?? (_httpContext = HttpContext.Current); }
        }

        public string IpAddress
        {
            get { return Request.RemoteIpAddress; }
        }

        public IAuthenticationManager Manager
        {
            get { return _authenticationManager ?? (_authenticationManager = OwinContext.Authentication); }
        }

        public abstract IOwinContext OwinContext { get; }
        
        public string ProductName
        {
            get
            {
                Func<Assembly, string> getAssemblyProduct = (assembly) =>
                {
                    var attribute = assembly.GetCustomAttributes(typeof(AssemblyProductAttribute), false).OfType<AssemblyProductAttribute>().FirstOrDefault();

                    return attribute != null ? attribute.Product : null;
                };

                return _productName ?? (_productName = getAssemblyProduct(BuildManager.GetGlobalAsaxType().BaseType.Assembly));
            }
        }

        protected IDataProtectionProvider ProtectionProvider
        {
            get { return _protectionProvider; }
        }

        public DomainInfo ReferredDomain
        {
            get { return _referredomain ?? (_referredomain = DomainStore.FindAsync(UrlReferrer.Host).Result); }
        }

        public IOwinRequest Request
        {
            get { return OwinContext.Request; }
        }

        public abstract ISessionStore Sessions { get; }

        public SignInManager<SessionInfo, string> SignInManager
        {
            get { return _signInManager ?? (_signInManager = new SignInManager<SessionInfo, string>(UserManager, Manager)); }
        }

        public Uri Url
        {
            get { return HttpContext.Request.Url; }
        }

        public Uri UrlReferrer
        {
            get { return HttpContext.Request.UrlReferrer ?? Url; }
        }
        
        public UserManager<SessionInfo, string> UserManager
        {
            get { return _userManager ?? (_userManager = OwinContext.GetUserManager<UserManager<SessionInfo, string>>()); }
        }

        public AuthorizeStatus Authorize(string featureName, string activityName, AccessLevels accessLevel, ICollection<string> roles)
        {
            var domain = CurrentDomain;

            if (domain != null && domain.Environment == Environments.Development)
            {
                return AuthorizeStatus.Success;
            }

            if (domain != null)
            {
                domain.Refresh(featureName, activityName, ref accessLevel, ref roles);
            }

            var token = BearerToken;

            if (activityName == null)
            {
                return AuthorizeStatus.Success;
            }

            if (accessLevel == AccessLevels.Public) return AuthorizeStatus.Success;

            var session = CurrentSession;

            var dataLevel = session.AccessLevel;

            if (!accessLevel.HasFlag(AccessLevels.Internal) && dataLevel < accessLevel) return AuthorizeStatus.Forbidden;

            if (roles.Count > 0)
            {
                roles = roles.Intersect(session.Account.Roles).ToArray();

                if (roles.Count == 0)
                {
                    return AuthorizeStatus.Forbidden;
                }
            }

            var company = session.Account.CompanyCode;
            var segment = session.Account.SegmentCode;

            var consumer = domain.Consumers.FirstOrDefault(e => e.CompanyCode == company && e.SegmentCode == segment);

            if (consumer == null)
            {
                return AuthorizeStatus.Forbidden;
            }

            var subscription = consumer.Subscriptions.SingleOrDefault(e => e.FeatureName == featureName);

            if (subscription == null)
            {
                return AuthorizeStatus.Forbidden;
            }

            if (subscription.ExpiresUtc.HasValue && subscription.ExpiresUtc.Value <= DateTime.UtcNow)
            {
                return AuthorizeStatus.Forbidden;
            }

            if (consumer.RequiredAccessLevel.HasValue && dataLevel < consumer.RequiredAccessLevel.Value)
            {
                return AuthorizeStatus.Forbidden;
            }

            return AuthorizeStatus.Success;
        }

        public virtual UserManager<SessionInfo, string> CreateUserManager()
        {
            var userManager = new UserManager<SessionInfo, string>(new ApplicationUserStore(Sessions));

            // Configure validation logic for usernames
            userManager.UserValidator = new UserValidator<SessionInfo, string>(userManager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Configure validation logic for passwords
            userManager.SetPasswordValidator(new PasswordStrength
            {
                Length = PasswordLengths.Medium,
                Symbol = true,
                Digit = true,
                Lowercase = true,
                Uppercase = true,
            });

            // Configure user lockout defaults
            userManager.UserLockoutEnabledByDefault = true;
            userManager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            userManager.MaxFailedAccessAttemptsBeforeLockout = 5;

            var dataProtectionProvider = ProtectionProvider;

            if (dataProtectionProvider != null)
            {
                userManager.UserTokenProvider = new DataProtectorTokenProvider<SessionInfo, string>(dataProtectionProvider.Create("ASP.NET Identity"));
            }

            userManager.EmailService = new SmtpService();

            userManager.SmsService = new SmsService();

            return userManager;
        }
        
        public async Task<DomainInfo> FindCurrentDomainAsync()
        {
            return _currentDomain ?? (_currentDomain = await DomainStore.FindAsync(Url.Host));
        }

        internal Task<DomainInfo> FindDomainAsync(string domainName)
        {
            return DomainStore.FindAsync(domainName);
        }

        public async Task<DomainInfo> FindReferredDomainAsync()
        {
            return _referredomain ?? (_referredomain = await DomainStore.FindAsync(UrlReferrer.Host));
        }

        public void SignIn(SessionInfo session)
        {
            // todo: somehow use the Persist
            
            if (_currentSession != null)
            {
                Manager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            }

            _currentSession = session;

            if (_currentSession != null)
            {
                Manager.SignIn(new AuthenticationProperties()
                {
                    AllowRefresh = true,
                    IssuedUtc = _currentSession.IssuedUtc,
                    ExpiresUtc = _currentSession.ExpiresUtc
                }, _currentSession);
            }
        }

        public void SignOut()
        {
            Manager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        }

        protected void Dispose(bool disposing)
        {
        }

        void IDisposable.Dispose()
        {
            Dispose(true);
        }
    }
}
