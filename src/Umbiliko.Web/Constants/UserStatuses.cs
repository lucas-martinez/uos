﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Constants
{
    public enum UserStatuses : byte
    {
        Initial = 0,

        Active = 1,

        Email = 2,

        Mobile = 4,
        
        Online = 128
    }
}
