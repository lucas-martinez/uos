﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Constants
{
    public static class Activities
    {
        const string Separator = "-";

        #region - *-accounts -

        public const string CreateAccounts = Actions.Create + Separator + Collections.Accounts;

        public const string DestroyAccounts = Actions.Destroy + Separator + Collections.Accounts;

        public const string DisplayAccounts = Actions.Display + Separator + Collections.Accounts;

        public const string DownloadAccounts = Actions.Download + Separator + Collections.Accounts;

        public const string ReadAccounts = Actions.Read + Separator + Collections.Accounts;

        public const string UpdateAccounts = Actions.Update + Separator + Collections.Accounts;

        public const string UploadAccounts = Actions.Upload + Separator + Collections.Accounts;

        #endregion - *-accounts -

        #region - *-activities -

        public const string CreateActivities = Actions.Create + Separator + Collections.Activities;

        public const string DestroyActivities = Actions.Destroy + Separator + Collections.Activities;

        public const string DisplayActivities = Actions.Display + Separator + Collections.Activities;

        public const string ListActivities = Actions.List + Separator + Collections.Activities;

        public const string ReadActivities = Actions.Read + Separator + Collections.Activities;

        public const string UpdateActivities = Actions.Update + Separator + Collections.Activities;

        #endregion - *-activities -

        #region - *-charges -

        public const string CreateCharges = Actions.Create + Separator + Collections.Charges;

        public const string DestroyCharges = Actions.Destroy + Separator + Collections.Charges;

        public const string DisplayCharges = Actions.Display + Separator + Collections.Charges;

        public const string DownloadCharges = Actions.Download + Separator + Collections.Charges;

        public const string ReadCharges = Actions.Read + Separator + Collections.Charges;

        public const string UpdateCharges = Actions.Update + Separator + Collections.Charges;

        #endregion - *-charges -

        #region - *-companies -

        public const string CreateCompanies = Actions.Create + Separator + Collections.Companies;

        public const string DisplayCompanies = Actions.Display + Separator + Collections.Companies;

        public const string DownloadCompanies = Actions.Download + Separator + Collections.Companies;

        public const string DetailCompany = Actions.Detail + Separator + Collections.Companies;

        public const string DestroyCompanies = Actions.Destroy + Separator + Collections.Companies;

        public const string ListCompanies = Actions.List + Separator + Collections.Companies;

        public const string ReadCompanies = Actions.Read + Separator + Collections.Companies;

        public const string UpdateCompanies = Actions.Update + Separator + Collections.Companies;

        public const string UploadCompanies = Actions.Upload + Separator + Collections.Companies;

        #endregion - *-companies -

        #region - *-consumers -

        public const string CreateConsumers = Actions.Create + Separator + Collections.Consumers;

        public const string DestroyConsumers = Actions.Destroy + Separator + Collections.Consumers;

        public const string DisplayConsumers = Actions.Display + Separator + Collections.Consumers;

        public const string DownloadConsumers = Actions.Download + Separator + Collections.Consumers;

        public const string ReadConsumers = Actions.Read + Separator + Collections.Consumers;

        public const string UpdateConsumers = Actions.Update + Separator + Collections.Consumers;

        public const string UploadConsumers = Actions.Upload + Separator + Collections.Consumers;

        #endregion - *-consumers -

        #region - *-customers -

        public const string CreateCustomers = Actions.Create + Separator + Collections.Customers;

        public const string DestroyCustomers = Actions.Destroy + Separator + Collections.Customers;

        public const string DisplayCustomers = Actions.Display + Separator + Collections.Customers;

        public const string DownloadCustomers = Actions.Download + Separator + Collections.Customers;

        public const string ReadCustomers = Actions.Read + Separator + Collections.Customers;

        public const string UploadCustomers = Actions.Upload + Separator + Collections.Customers;

        #endregion - *-customers -

        #region - *-domains -

        public const string CreateDomains = Actions.Create + Separator + Collections.Domains;

        public const string DestroyDomains = Actions.Destroy + Separator + Collections.Domains;

        public const string DisplayDomains = Actions.Display + Separator + Collections.Domains;

        public const string DownloadDomains = Actions.Download + Separator + Collections.Domains;

        public const string ListDomains = Actions.List + Separator + Collections.Domains;

        public const string ReadDomains = Actions.Read + Separator + Collections.Domains;

        public const string UpdateDomains = Actions.Update + Separator + Collections.Domains;

        public const string UploadDomains = Actions.Upload + Separator + Collections.Domains;

        #endregion - *-domains -

        #region - *-features -

        public const string CreateFeatures = Actions.Create + Separator + Collections.Features;

        public const string DestroyFeatures = Actions.Destroy + Separator + Collections.Features;

        public const string DisplayFeatures = Actions.Display + Separator + Collections.Features;

        public const string ListFeatures = Actions.List + Separator + Collections.Features;

        public const string ReadFeatures = Actions.Read + Separator + Collections.Features;

        public const string UpdateFeatures = Actions.Update + Separator + Collections.Features;

        #endregion - *-features -

        #region - *-instances -

        public const string CreateInstances = Actions.Create + Separator + Collections.Instances;

        public const string DestroyInstances = Actions.Destroy + Separator + Collections.Instances;

        public const string DisplayInstances = Actions.Display + Separator + Collections.Instances;

        public const string ReadInstances = Actions.Read + Separator + Collections.Instances;

        public const string UpdateInstances = Actions.Update + Separator + Collections.Instances;

        #endregion - *-instances -

        #region - *-logins -

        public const string CreateLogins = Actions.Create + Separator + Collections.Logins;

        public const string DestroyLogins = Actions.Destroy + Separator + Collections.Logins;

        public const string DisplayLogins = Actions.Display + Separator + Collections.Logins;

        public const string DownloadLogins = Actions.Download + Separator + Collections.Logins;

        public const string ReadLogins = Actions.Read + Separator + Collections.Logins;

        public const string UpdateLogins = Actions.Update + Separator + Collections.Logins;

        public const string UploadLogins = Actions.Upload + Separator + Collections.Logins;

        #endregion - *-logins -

        #region - *-my-account -

        public const string DestroyMyAccount = Actions.Destroy + Separator + My.Account;

        public const string DisplayMyAccount = Actions.Display + Separator + My.Account;

        public const string DownloadMyAccount = Actions.Download + Separator + My.Account;

        public const string EnterMyAccount = @"enter" + Separator + My.Account;

        public const string ReadMyAccount = Actions.Read + Separator + My.Account;

        public const string UpdateMyAccount = Actions.Update + Separator + My.Account;

        public const string UploadMyAccount = Actions.Upload + Separator + My.Account;
        
        #endregion - *-my-account -

        #region - *-my-company -

        public const string DestroyMyCompany = Actions.Destroy + Separator + My.Company;

        public const string DisplayMyCompany = Actions.Display + Separator + My.Company;

        public const string DownloadMyCompany = Actions.Download + Separator + My.Company;

        public const string ReadMyCompany = Actions.Read + Separator + My.Company;

        public const string UpdateMyCompany = Actions.Update + Separator + My.Company;

        public const string UploadMyCompany = Actions.Upload + Separator + My.Company;

        #endregion - *-my-company -

        #region - *-products -

        public const string CreateProducts = Actions.Create + Separator + Collections.Products;

        public const string DestroyProducts = Actions.Destroy + Separator + Collections.Products;

        public const string DisplayProducts = Actions.Display + Separator + Collections.Products;

        public const string DownloadProducts = Actions.Download + Separator + Collections.Products;

        public const string ListProducts = Actions.List + Separator + Collections.Products;

        public const string ReadProducts = Actions.Read + Separator + Collections.Products;

        public const string UpdateProducts = Actions.Update + Separator + Collections.Products;

        public const string UploadProducts = Actions.Upload + Separator + Collections.Products;

        #endregion - *-products -

        #region - *-redirects -

        public const string CreateRedirects = Actions.Create + Separator + Collections.Redirects;

        public const string DestroyRedirects = Actions.Destroy + Separator + Collections.Redirects;

        public const string DisplayRedirects = Actions.Display + Separator + Collections.Redirects;

        public const string DownloadRedirects = Actions.Download + Separator + Collections.Redirects;

        public const string ReadRedirects = Actions.Read + Separator + Collections.Redirects;

        public const string UpdateRedirects = Actions.Update + Separator + Collections.Redirects;

        public const string UploadRedirects = Actions.Upload + Separator + Collections.Redirects;

        #endregion - *-redirects -

        #region - *-regions -

        public const string CreateRegions = Actions.Create + Separator + Collections.Regions;

        public const string DestroyRegions = Actions.Destroy + Separator + Collections.Regions;

        public const string DisplayRegions = Actions.Display + Separator + Collections.Regions;

        public const string DownloadRegions = Actions.Download + Separator + Collections.Regions;

        public const string ReadRegions = Actions.Read + Separator + Collections.Regions;

        public const string UpdateRegions = Actions.Update + Separator + Collections.Regions;

        public const string UploadRegions = Actions.Upload + Separator + Collections.Regions;

        #endregion - *-regions -


        #region - *-roles -

        public const string CreateRoles = Actions.Create + Separator + Collections.Roles;

        public const string DisplayRoles = Actions.Display + Separator + Collections.Roles;

        public const string DestroyRoles = Actions.Destroy + Separator + Collections.Roles;

        public const string ListRoles = Actions.List + Separator + Collections.Roles;

        public const string ReadRoles = Actions.Read + Separator + Collections.Roles;

        public const string UpdateRoles = Actions.Update + Separator + Collections.Roles;

        #endregion - *-roles -

        #region - *-segments -

        public const string CreateSegments = Actions.Create + Separator + Collections.Segments;

        public const string DisplaySegments = Actions.Display + Separator + Collections.Segments;

        public const string DestroySegments = Actions.Destroy + Separator + Collections.Segments;

        public const string DownloadSegments = Actions.Download + Separator + Collections.Segments;

        public const string ListSegments = Actions.List + Separator + Collections.Segments;

        public const string ReadSegments = Actions.Read + Separator + Collections.Segments;

        public const string UpdateSegments = Actions.Update + Separator + Collections.Segments;

        public const string UploadSegments = Actions.Upload + Separator + Collections.Segments;

        #endregion - *-segments -

        #region - *-sessions -

        public const string CreateSessions = Actions.Create + Separator + Collections.Sessions;

        public const string DestroySessions = Actions.Destroy + Separator + Collections.Sessions;

        public const string DisplaySessions = Actions.Display + Separator + Collections.Sessions;

        public const string ReadSessions = Actions.Read + Separator + Collections.Sessions;

        public const string UpdateSessions = Actions.Update + Separator + Collections.Sessions;

        #endregion - *-sessions -

        #region - *-subscriptions -

        public const string CreateSubscriptions = Actions.Create + Separator + Collections.Subscriptions;

        public const string DestroySubscriptions = Actions.Destroy + Separator + Collections.Subscriptions;

        public const string DisplaySubscriptions = Actions.Display + Separator + Collections.Subscriptions;

        public const string DownloadSubscriptions = Actions.Download + Separator + Collections.Subscriptions;

        public const string ReadSubscriptions = Actions.Read + Separator + Collections.Subscriptions;

        public const string UpdateSubscriptions = Actions.Update + Separator + Collections.Subscriptions;

        public const string UploadSubscriptions = Actions.Upload + Separator + Collections.Subscriptions;

        #endregion - *-subscriptions -

        #region - *-suppliers -

        public const string DestroySupplies = Actions.Destroy + Separator + Collections.Suppliers;

        public const string DisplaySuppliers = Actions.Display + Separator + Collections.Suppliers;

        public const string ReadSuppliers = Actions.Read + Separator + Collections.Suppliers;
        
        #endregion - *-suppliers -

        #region - *-users -

        public const string CreateUsers = Actions.Create + Separator + Collections.Users;

        public const string DisplayUsers = Actions.Display + Separator + Collections.Users;

        public const string DownloadUsers = Actions.Download + Separator + Collections.Users;

        public const string DestroyUsers = Actions.Destroy + Separator + Collections.Users;

        public const string ListUsers = Actions.List + Separator + Collections.Users;

        public const string ReadUsers = Actions.Read + Separator + Collections.Users;

        public const string UpdateUsers = Actions.Update + Separator + Collections.Users;

        public const string UploadUsers = Actions.Upload + Separator + Collections.Users;

        #endregion - *-user -
    }
}
