﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Constants
{
    public enum TokenTypes : byte
    {
        Agent = 0,
        Activate = 1,
        Login = 2,
        PasswordRecover = 3,
    }
}
