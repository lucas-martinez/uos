﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Constants
{
    public class Characters
    {
        public const char At = '@';

        public const char Bar = '|';

        public const char Comma = ',';

        public const char Exclamation = '!';

        public const char Space = ' ';
    }
}
