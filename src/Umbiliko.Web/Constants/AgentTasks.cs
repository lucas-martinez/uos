﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos
{
    [Flags]
    public enum AgentTasks : long
    {
        CompileProject = 1 << 1,

        CompileTheme = 1 << 2,

        DownloadFtp = 1 << 11,

        DownloadGit = 1 << 12,

        MonitorCapacity = 1 << 21,

        MonitorPerformance = 1 << 22,

        PublishSite = 1 << 31,

        ServeSite = 1 << 32
    }
}
