﻿
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
namespace Uos.Web.Constants
{
    public partial class Regions
    {
        /// <summary>
        /// Afghanistan
        /// </summary>
        public static readonly Info AFG = new Info(@"AFG", @"AF", 93);
        
        /// <summary>
        /// Albania
        /// </summary>
        public static readonly Info ALB = new Info(@"ALB", @"AL", 355);
        
        /// <summary>
        /// United Arab Emirates
        /// </summary>
        public static readonly Info ARE = new Info(@"ARE", @"AE", 971);
        
        /// <summary>
        /// Argentina
        /// </summary>
        public static readonly Info ARG = new Info(@"ARG", @"AR", 54);
        
        /// <summary>
        /// Armenia
        /// </summary>
        public static readonly Info ARM = new Info(@"ARM", @"AM", 374);
        
        /// <summary>
        /// Australia
        /// </summary>
        public static readonly Info AUS = new Info(@"AUS", @"AU", 61);
        
        /// <summary>
        /// Austria
        /// </summary>
        public static readonly Info AUT = new Info(@"AUT", @"AT", 43);
        
        /// <summary>
        /// Azerbaijan
        /// </summary>
        public static readonly Info AZE = new Info(@"AZE", @"AZ", 994);
        
        /// <summary>
        /// Belgium
        /// </summary>
        public static readonly Info BEL = new Info(@"BEL", @"BE", 32);
        
        /// <summary>
        /// Bangladesh
        /// </summary>
        public static readonly Info BGD = new Info(@"BGD", @"BD", 880);
        
        /// <summary>
        /// Bulgaria
        /// </summary>
        public static readonly Info BGR = new Info(@"BGR", @"BG", 359);
        
        /// <summary>
        /// Bahrain
        /// </summary>
        public static readonly Info BHR = new Info(@"BHR", @"BH", 973);
        
        /// <summary>
        /// Bosnia and Herzegovina
        /// </summary>
        public static readonly Info BIH = new Info(@"BIH", @"BA", 387);
        
        /// <summary>
        /// Belarus
        /// </summary>
        public static readonly Info BLR = new Info(@"BLR", @"BY", 375);
        
        /// <summary>
        /// Belize
        /// </summary>
        public static readonly Info BLZ = new Info(@"BLZ", @"BZ", 501);
        
        /// <summary>
        /// Bolivia
        /// </summary>
        public static readonly Info BOL = new Info(@"BOL", @"BO", 591);
        
        /// <summary>
        /// Brazil
        /// </summary>
        public static readonly Info BRA = new Info(@"BRA", @"BR", 55);
        
        /// <summary>
        /// Brunei
        /// </summary>
        public static readonly Info BRN = new Info(@"BRN", @"BN", 673);
        
        /// <summary>
        /// Bhutan
        /// </summary>
        public static readonly Info BTN = new Info(@"BTN", @"BT", 975);
        
        /// <summary>
        /// Botswana
        /// </summary>
        public static readonly Info BWA = new Info(@"BWA", @"BW", 267);
        
        /// <summary>
        /// Canada
        /// </summary>
        public static readonly Info CAN = new Info(@"CAN", @"CA", 1);
        
        /// <summary>
        /// Switzerland
        /// </summary>
        public static readonly Info CHE = new Info(@"CHE", @"CH", 41);
        
        /// <summary>
        /// Chile
        /// </summary>
        public static readonly Info CHL = new Info(@"CHL", @"CL", 56);
        
        /// <summary>
        /// China
        /// </summary>
        public static readonly Info CHN = new Info(@"CHN", @"CN", 86);
        
        /// <summary>
        /// Côte d’Ivoire
        /// </summary>
        public static readonly Info CIV = new Info(@"CIV", @"CI", 225);
        
        /// <summary>
        /// Cameroon
        /// </summary>
        public static readonly Info CMR = new Info(@"CMR", @"CM", 237);
        
        /// <summary>
        /// Congo (DRC)
        /// </summary>
        public static readonly Info COD = new Info(@"COD", @"CD", 243);
        
        /// <summary>
        /// Colombia
        /// </summary>
        public static readonly Info COL = new Info(@"COL", @"CO", 57);
        
        /// <summary>
        /// Costa Rica
        /// </summary>
        public static readonly Info CRI = new Info(@"CRI", @"CR", 506);
        
        /// <summary>
        /// Cuba
        /// </summary>
        public static readonly Info CUB = new Info(@"CUB", @"CU", 53);
        
        /// <summary>
        /// Czech Republic
        /// </summary>
        public static readonly Info CZE = new Info(@"CZE", @"CZ", 420);
        
        /// <summary>
        /// Germany
        /// </summary>
        public static readonly Info DEU = new Info(@"DEU", @"DE", 49);
        
        /// <summary>
        /// Djibouti
        /// </summary>
        public static readonly Info DJI = new Info(@"DJI", @"DJ", 253);
        
        /// <summary>
        /// Denmark
        /// </summary>
        public static readonly Info DNK = new Info(@"DNK", @"DK", 45);
        
        /// <summary>
        /// Dominican Republic
        /// </summary>
        public static readonly Info DOM = new Info(@"DOM", @"DO", 1);
        
        /// <summary>
        /// Algeria
        /// </summary>
        public static readonly Info DZA = new Info(@"DZA", @"DZ", 213);
        
        /// <summary>
        /// Ecuador
        /// </summary>
        public static readonly Info ECU = new Info(@"ECU", @"EC", 593);
        
        /// <summary>
        /// Egypt
        /// </summary>
        public static readonly Info EGY = new Info(@"EGY", @"EG", 20);
        
        /// <summary>
        /// Eritrea
        /// </summary>
        public static readonly Info ERI = new Info(@"ERI", @"ER", 291);
        
        /// <summary>
        /// Spain
        /// </summary>
        public static readonly Info ESP = new Info(@"ESP", @"ES", 34);
        
        /// <summary>
        /// Estonia
        /// </summary>
        public static readonly Info EST = new Info(@"EST", @"EE", 372);
        
        /// <summary>
        /// Ethiopia
        /// </summary>
        public static readonly Info ETH = new Info(@"ETH", @"ET", 251);
        
        /// <summary>
        /// Finland
        /// </summary>
        public static readonly Info FIN = new Info(@"FIN", @"FI", 358);
        
        /// <summary>
        /// France
        /// </summary>
        public static readonly Info FRA = new Info(@"FRA", @"FR", 33);
        
        /// <summary>
        /// Faroe Islands
        /// </summary>
        public static readonly Info FRO = new Info(@"FRO", @"FO", 298);
        
        /// <summary>
        /// United Kingdom
        /// </summary>
        public static readonly Info GBR = new Info(@"GBR", @"GB", 44);
        
        /// <summary>
        /// Georgia
        /// </summary>
        public static readonly Info GEO = new Info(@"GEO", @"GE", 995);
        
        /// <summary>
        /// Greece
        /// </summary>
        public static readonly Info GRC = new Info(@"GRC", @"GR", 30);
        
        /// <summary>
        /// Greenland
        /// </summary>
        public static readonly Info GRL = new Info(@"GRL", @"GL", 299);
        
        /// <summary>
        /// Guatemala
        /// </summary>
        public static readonly Info GTM = new Info(@"GTM", @"GT", 502);
        
        /// <summary>
        /// Hong Kong SAR
        /// </summary>
        public static readonly Info HKG = new Info(@"HKG", @"HK", 852);
        
        /// <summary>
        /// Honduras
        /// </summary>
        public static readonly Info HND = new Info(@"HND", @"HN", 504);
        
        /// <summary>
        /// Croatia
        /// </summary>
        public static readonly Info HRV = new Info(@"HRV", @"HR", 385);
        
        /// <summary>
        /// Haiti
        /// </summary>
        public static readonly Info HTI = new Info(@"HTI", @"HT", 509);
        
        /// <summary>
        /// Hungary
        /// </summary>
        public static readonly Info HUN = new Info(@"HUN", @"HU", 36);
        
        /// <summary>
        /// Indonesia
        /// </summary>
        public static readonly Info IDN = new Info(@"IDN", @"ID", 62);
        
        /// <summary>
        /// India
        /// </summary>
        public static readonly Info IND = new Info(@"IND", @"IN", 91);
        
        /// <summary>
        /// Ireland
        /// </summary>
        public static readonly Info IRL = new Info(@"IRL", @"IE", 353);
        
        /// <summary>
        /// Iran
        /// </summary>
        public static readonly Info IRN = new Info(@"IRN", @"IR", 98);
        
        /// <summary>
        /// Iraq
        /// </summary>
        public static readonly Info IRQ = new Info(@"IRQ", @"IQ", 964);
        
        /// <summary>
        /// Iceland
        /// </summary>
        public static readonly Info ISL = new Info(@"ISL", @"IS", 354);
        
        /// <summary>
        /// Israel
        /// </summary>
        public static readonly Info ISR = new Info(@"ISR", @"IL", 972);
        
        /// <summary>
        /// Italy
        /// </summary>
        public static readonly Info ITA = new Info(@"ITA", @"IT", 39);
        
        /// <summary>
        /// Jamaica
        /// </summary>
        public static readonly Info JAM = new Info(@"JAM", @"JM", 1);
        
        /// <summary>
        /// Jordan
        /// </summary>
        public static readonly Info JOR = new Info(@"JOR", @"JO", 962);
        
        /// <summary>
        /// Japan
        /// </summary>
        public static readonly Info JPN = new Info(@"JPN", @"JP", 81);
        
        /// <summary>
        /// Kazakhstan
        /// </summary>
        public static readonly Info KAZ = new Info(@"KAZ", @"KZ", 7);
        
        /// <summary>
        /// Kenya
        /// </summary>
        public static readonly Info KEN = new Info(@"KEN", @"KE", 254);
        
        /// <summary>
        /// Kyrgyzstan
        /// </summary>
        public static readonly Info KGZ = new Info(@"KGZ", @"KG", 996);
        
        /// <summary>
        /// Cambodia
        /// </summary>
        public static readonly Info KHM = new Info(@"KHM", @"KH", 855);
        
        /// <summary>
        /// Korea
        /// </summary>
        public static readonly Info KOR = new Info(@"KOR", @"KR", 82);
        
        /// <summary>
        /// Kuwait
        /// </summary>
        public static readonly Info KWT = new Info(@"KWT", @"KW", 965);
        
        /// <summary>
        /// Laos
        /// </summary>
        public static readonly Info LAO = new Info(@"LAO", @"LA", 856);
        
        /// <summary>
        /// Lebanon
        /// </summary>
        public static readonly Info LBN = new Info(@"LBN", @"LB", 961);
        
        /// <summary>
        /// Libya
        /// </summary>
        public static readonly Info LBY = new Info(@"LBY", @"LY", 218);
        
        /// <summary>
        /// Liechtenstein
        /// </summary>
        public static readonly Info LIE = new Info(@"LIE", @"LI", 423);
        
        /// <summary>
        /// Sri Lanka
        /// </summary>
        public static readonly Info LKA = new Info(@"LKA", @"LK", 94);
        
        /// <summary>
        /// Lithuania
        /// </summary>
        public static readonly Info LTU = new Info(@"LTU", @"LT", 370);
        
        /// <summary>
        /// Luxembourg
        /// </summary>
        public static readonly Info LUX = new Info(@"LUX", @"LU", 352);
        
        /// <summary>
        /// Latvia
        /// </summary>
        public static readonly Info LVA = new Info(@"LVA", @"LV", 371);
        
        /// <summary>
        /// Macao SAR
        /// </summary>
        public static readonly Info MAC = new Info(@"MAC", @"MO", 853);
        
        /// <summary>
        /// Morocco
        /// </summary>
        public static readonly Info MAR = new Info(@"MAR", @"MA", 212);
        
        /// <summary>
        /// Monaco
        /// </summary>
        public static readonly Info MCO = new Info(@"MCO", @"MC", 377);
        
        /// <summary>
        /// Moldova
        /// </summary>
        public static readonly Info MDA = new Info(@"MDA", @"MD", 373);
        
        /// <summary>
        /// Maldives
        /// </summary>
        public static readonly Info MDV = new Info(@"MDV", @"MV", 960);
        
        /// <summary>
        /// Mexico
        /// </summary>
        public static readonly Info MEX = new Info(@"MEX", @"MX", 52);
        
        /// <summary>
        /// Macedonia, FYRO
        /// </summary>
        public static readonly Info MKD = new Info(@"MKD", @"MK", 389);
        
        /// <summary>
        /// Mali
        /// </summary>
        public static readonly Info MLI = new Info(@"MLI", @"ML", 223);
        
        /// <summary>
        /// Malta
        /// </summary>
        public static readonly Info MLT = new Info(@"MLT", @"MT", 356);
        
        /// <summary>
        /// Myanmar
        /// </summary>
        public static readonly Info MMR = new Info(@"MMR", @"MM", 95);
        
        /// <summary>
        /// Montenegro
        /// </summary>
        public static readonly Info MNE = new Info(@"MNE", @"ME", 382);
        
        /// <summary>
        /// Mongolia
        /// </summary>
        public static readonly Info MNG = new Info(@"MNG", @"MN", 976);
        
        /// <summary>
        /// Malaysia
        /// </summary>
        public static readonly Info MYS = new Info(@"MYS", @"MY", 60);
        
        /// <summary>
        /// Nigeria
        /// </summary>
        public static readonly Info NGA = new Info(@"NGA", @"NG", 234);
        
        /// <summary>
        /// Nicaragua
        /// </summary>
        public static readonly Info NIC = new Info(@"NIC", @"NI", 505);
        
        /// <summary>
        /// Netherlands
        /// </summary>
        public static readonly Info NLD = new Info(@"NLD", @"NL", 31);
        
        /// <summary>
        /// Norway
        /// </summary>
        public static readonly Info NOR = new Info(@"NOR", @"NO", 47);
        
        /// <summary>
        /// Nepal
        /// </summary>
        public static readonly Info NPL = new Info(@"NPL", @"NP", 977);
        
        /// <summary>
        /// New Zealand
        /// </summary>
        public static readonly Info NZL = new Info(@"NZL", @"NZ", 64);
        
        /// <summary>
        /// Oman
        /// </summary>
        public static readonly Info OMN = new Info(@"OMN", @"OM", 968);
        
        /// <summary>
        /// Pakistan
        /// </summary>
        public static readonly Info PAK = new Info(@"PAK", @"PK", 92);
        
        /// <summary>
        /// Panama
        /// </summary>
        public static readonly Info PAN = new Info(@"PAN", @"PA", 507);
        
        /// <summary>
        /// Peru
        /// </summary>
        public static readonly Info PER = new Info(@"PER", @"PE", 51);
        
        /// <summary>
        /// Philippines
        /// </summary>
        public static readonly Info PHL = new Info(@"PHL", @"PH", 63);
        
        /// <summary>
        /// Poland
        /// </summary>
        public static readonly Info POL = new Info(@"POL", @"PL", 48);
        
        /// <summary>
        /// Puerto Rico
        /// </summary>
        public static readonly Info PRI = new Info(@"PRI", @"PR", 1);
        
        /// <summary>
        /// Portugal
        /// </summary>
        public static readonly Info PRT = new Info(@"PRT", @"PT", 351);
        
        /// <summary>
        /// Paraguay
        /// </summary>
        public static readonly Info PRY = new Info(@"PRY", @"PY", 595);
        
        /// <summary>
        /// Qatar
        /// </summary>
        public static readonly Info QAT = new Info(@"QAT", @"QA", 974);
        
        /// <summary>
        /// Réunion
        /// </summary>
        public static readonly Info REU = new Info(@"REU", @"RE", 262);
        
        /// <summary>
        /// Romania
        /// </summary>
        public static readonly Info ROU = new Info(@"ROU", @"RO", 40);
        
        /// <summary>
        /// Russia
        /// </summary>
        public static readonly Info RUS = new Info(@"RUS", @"RU", 7);
        
        /// <summary>
        /// Rwanda
        /// </summary>
        public static readonly Info RWA = new Info(@"RWA", @"RW", 250);
        
        /// <summary>
        /// Saudi Arabia
        /// </summary>
        public static readonly Info SAU = new Info(@"SAU", @"SA", 966);
        
        /// <summary>
        /// Senegal
        /// </summary>
        public static readonly Info SEN = new Info(@"SEN", @"SN", 221);
        
        /// <summary>
        /// Singapore
        /// </summary>
        public static readonly Info SGP = new Info(@"SGP", @"SG", 65);
        
        /// <summary>
        /// El Salvador
        /// </summary>
        public static readonly Info SLV = new Info(@"SLV", @"SV", 503);
        
        /// <summary>
        /// Somalia
        /// </summary>
        public static readonly Info SOM = new Info(@"SOM", @"SO", 252);
        
        /// <summary>
        /// Serbia
        /// </summary>
        public static readonly Info SRB = new Info(@"SRB", @"RS", 381);
        
        /// <summary>
        /// Slovakia
        /// </summary>
        public static readonly Info SVK = new Info(@"SVK", @"SK", 421);
        
        /// <summary>
        /// Slovenia
        /// </summary>
        public static readonly Info SVN = new Info(@"SVN", @"SI", 386);
        
        /// <summary>
        /// Sweden
        /// </summary>
        public static readonly Info SWE = new Info(@"SWE", @"SE", 46);
        
        /// <summary>
        /// Syria
        /// </summary>
        public static readonly Info SYR = new Info(@"SYR", @"SY", 963);
        
        /// <summary>
        /// Thailand
        /// </summary>
        public static readonly Info THA = new Info(@"THA", @"TH", 66);
        
        /// <summary>
        /// Tajikistan
        /// </summary>
        public static readonly Info TJK = new Info(@"TJK", @"TJ", 992);
        
        /// <summary>
        /// Turkmenistan
        /// </summary>
        public static readonly Info TKM = new Info(@"TKM", @"TM", 993);
        
        /// <summary>
        /// Trinidad and Tobago
        /// </summary>
        public static readonly Info TTO = new Info(@"TTO", @"TT", 1);
        
        /// <summary>
        /// Tunisia
        /// </summary>
        public static readonly Info TUN = new Info(@"TUN", @"TN", 216);
        
        /// <summary>
        /// Turkey
        /// </summary>
        public static readonly Info TUR = new Info(@"TUR", @"TR", 90);
        
        /// <summary>
        /// Taiwan
        /// </summary>
        public static readonly Info TWN = new Info(@"TWN", @"TW", 886);
        
        /// <summary>
        /// Ukraine
        /// </summary>
        public static readonly Info UKR = new Info(@"UKR", @"UA", 380);
        
        /// <summary>
        /// Uruguay
        /// </summary>
        public static readonly Info URY = new Info(@"URY", @"UY", 598);
        
        /// <summary>
        /// United States
        /// </summary>
        public static readonly Info USA = new Info(@"USA", @"US", 1);
        
        /// <summary>
        /// Uzbekistan
        /// </summary>
        public static readonly Info UZB = new Info(@"UZB", @"UZ", 998);
        
        /// <summary>
        /// Venezuela
        /// </summary>
        public static readonly Info VEN = new Info(@"VEN", @"VE", 58);
        
        /// <summary>
        /// Vietnam
        /// </summary>
        public static readonly Info VNM = new Info(@"VNM", @"VN", 84);
        
        /// <summary>
        /// Yemen
        /// </summary>
        public static readonly Info YEM = new Info(@"YEM", @"YE", 967);
        
        /// <summary>
        /// South Africa
        /// </summary>
        public static readonly Info ZAF = new Info(@"ZAF", @"ZA", 27);
        
        /// <summary>
        /// Zimbabwe
        /// </summary>
        public static readonly Info ZWE = new Info(@"ZWE", @"ZW", 263);
        
    }
}