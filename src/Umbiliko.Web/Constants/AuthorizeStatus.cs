﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Constants
{
    public enum AuthorizeStatus
    {
        /// <summary>
        /// Authorize was successful
        /// </summary>
        Success = 0,

        /// <summary>
        /// User is locked out
        /// </summary>
        LockedOut = 1,

        /// <summary>
        /// Sign in requires addition verification (i.e. two factor)
        /// </summary>
        RequiresVerification = 2,

        /// <summary>
        /// Authorize failed
        /// </summary>
        Failure = 3,

        /// <summary>
        /// Authorize due to access level
        /// </summary>
        Forbidden = 4
    }
}
