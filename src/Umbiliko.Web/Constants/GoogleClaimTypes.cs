﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Constants
{
    public static class GoogleClaimTypes
    {
        public const string CircledByCount = @"http://schemas.google.com/accesscontrolservice/2016/01/claims/circledbycount";
        
        public const string Etag = @"http://schemas.google.com/accesscontrolservice/2016/01/claims/etag";

        public const string Kind = @"http://schemas.google.com/accesscontrolservice/2016/01/claims/kind";

        public const string Id = @"http://schemas.google.com/accesscontrolservice/2016/01/claims/id";

        public const string IsPlusUser = @"http://schemas.google.com/accesscontrolservice/2016/01/claims/isplususer";
        
        public const string ObjectType = @"http://schemas.google.com/accesscontrolservice/2016/01/claims/objecttype";

        public const string Tagline = @"http://schemas.google.com/accesscontrolservice/2016/01/claims/tagline";
    }
}
