﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Constants
{
    public static class Lengths
    {
        public const int AccessLevelLength = 3;
        public const int AccountRolesLength = byte.MaxValue;
        public const int ActitivyAttributesLength = byte.MaxValue;
        public const int ActivityNameLength = 40;
        public const int ActivityRolesLength = byte.MaxValue;
        public const int ActivityResponseTypeLength = 40;
        public const int ActivityTypeLength = 3;
        public const int ActivityVerbsLength = 40;
        public const int ActionNameLength = 40;
        public const int ClaimTypeLength = sbyte.MaxValue;
        public const int ClaimValueLength = sbyte.MaxValue;
        public const int ClaimValueTypeLength = sbyte.MaxValue;
        public const int SegmentCodeLength = 3;
        public const int ClientSecretLength = 128;
        public const int CompanyCodeLength = SegmentCodeLength + 1;
        public const int ControllerNameLength = 40;
        public const int CountryCodeLength = 3;
        public const int CountryNameLength = 20;
        public const int CultureNameLength = 20;
        public const int CurrencyCodeLength = 3;
        public const int CompanyNameLength = 20;
        public const int CompanyTypeLength = 3;
        public const int CustomerSecretLength = byte.MaxValue;
        public const int DataLength = short.MaxValue;
        public const int DateLength = sbyte.MaxValue;
        public const int DescriptionLength = byte.MaxValue;
        public const int DomainNameLength = sbyte.MaxValue;
        public const int EmailAddressLength = 80;
        public const int EnvironmentLength = 3;
        public const int FeatureNameLength = 20;
        public const int GenderLength = 20;
        public const int IpAddressLength = 20;
        public const int FingerprintLength = sbyte.MaxValue;
        public const int LoginTokenLength = byte.MaxValue;
        public const int LoginKeyLength = sbyte.MaxValue;
        public const int LoginLabelLength = sbyte.MaxValue;
        public const int LoginProviderLength = 20;
        public const int MacAddressLength = 60;
        public const int MachineNameLength = 20;
        public const int NameLength = 80;
        public const int PasswordLength = 40;
        public const int PasswordHashLength = sbyte.MaxValue;
        public const int PasswordStrengthLength = 5;
        public const int PhoneNumberLength = 20;
        public const int ProductNameLength = 20;
        public const int RegionNameLength = 20;
        public const int RegionTypeLength = 3;
        public const int StatusCodeLength = 20;
        public const int StreetAddressLength = 20;
        public const int SubscriptionStatusLength = 1;
        public const int SummaryLength = short.MaxValue;
        public const int TextNameLength = 20;
        public const int TextValueLength = short.MaxValue;
        public const int ThemeDataLength = short.MaxValue;
        public const int ThemeNameLength = 20;
        public const int TokenValueLength = byte.MaxValue;
        public const int UserNameLength = 40;
        public const int VersionLength = 20;

        public const int DaysLength = 20;
        public const int HoursLength = 20;
        public const int MillisecondsLength = 20;
        public const int MinutesLength = 20;
        public const int SecondsLength = 20;

        public const int RequestDataLength = short.MaxValue;
        public const int ResultDataLength = short.MaxValue;
    }
}
