﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Constants
{
    public static class Features
    {
        public const string Administration = @"administration";

        public const string Application = @"application";

        public const string Assertion = @"assertion";

        public const string Authentication = @"authentication";

        public const string Authorization = @"authorization";

        public const string Automation = @"automation";

        public const string Configuration = @"configuration";

        public const string Customization = @"customization";

        public const string Formulation = @"formulation";

        public const string Localization = @"localization";

        public const string Management = @"management";

        public const string Production = @"production";

        public const string Search = @"search";
    }
}
