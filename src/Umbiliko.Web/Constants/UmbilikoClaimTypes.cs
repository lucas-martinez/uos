﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Constants
{
    public class UmbilikoClaimTypes
    {
        public const string AccessFailedCount = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/accessfailedcount";

        public const string AccessToken = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/accesstoken";

        public const string AccountInfo = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/accountinfo";

        public const string CompanyCode = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/companycode";

        public const string CompanyName = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/companyname";

        public const string DisplayName = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/displayname";

        public const string DomainName = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/domainname";
        
        public const string EmailConfirmed = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/emailconfirmed";

        public const string Language = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/language";

        public const string LockoutEnabled = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/lockoutenabled";

        public const string LockoutEndDate = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/lockoutenddate";

        public const string LoginId = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/loginid";

        public const string LoginInfo = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/logininfo";

        public const string LoginKey = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/loginkey";

        public const string LoginProvider = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/loginprovider";

        public const string PasswordHash = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/passwordhash";

        public const string PhoneNumber = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/phoneconfirmed";

        public const string PhoneNumberConfirmed = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/phonenumberconfirmed";

        public const string Picture = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/picture";

        public const string Profile = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/profile";

        public const string SecurityStamp = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/securitystamp";

        public const string SegmentCode = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/segmentcode";

        public const string SegmentDescription = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/segmentdescription";

        public const string SessionId = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/sessionid";

        public const string TwoFactorEnabled = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/twofactorenabled";

        public const string TwoFactorCode = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/twofactorcode";

        public const string TwoFactorConfirmed = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/twofactorconfirmed";

        public const string TwoFactorInstant = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/twofactorinstant";
    }
}
