﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Constants
{
    public static class Actions
    {
        public const string Api = @"api";

        public const string Create = @"create";

        public const string Default = @"";

        public const string Display = @"display";

        public const string Download = @"download";

        public const string Destroy = @"destroy";

        public const string Detail = @"detail";

        public const string Enter = @"enter";

        public const string Exit = @"exit";

        public const string List = @"list";

        public const string Migrate = @"migrate";

        public const string Read = @"read";

        public const string Update = @"update";

        public const string Upload = @"upload";
    }
}
