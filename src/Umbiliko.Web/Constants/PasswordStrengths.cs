﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Constants
{
    public class PasswordStrengths
    {
        public const string None = @"N";

        public const string Strong = @"S";

        public const string Weak = @"W";
    }
}
