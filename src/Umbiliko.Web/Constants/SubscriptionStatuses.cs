﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Constants
{
    public class SubscriptionStatuses
    {
        /// <summary>
        /// Subscription is active
        /// </summary>
        public const string Active = @"A";

        /// <summary>
        /// Subscriptions has pending payment
        /// </summary>
        public const string PendingPayment = @"P";

        /// <summary>
        /// Subscription is suspented
        /// </summary>
        public const string Suspended = @"S";
    }
}
