﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Constants
{
    public static class Defaults
    {
        public const int AccessFailedLock = 4;

        public const string AdminName = @"admin";

        public const string InternalCustomerCode = @"UMKO";

        public const string ApplicationCompanyName = @"Uos";

        public const string ApplicationDomainName = @"umbiliko.com";

        public const string LocalHost = @"localhost";

        public const string IdentityProvider = @"Uos";

        public const string GuestName = @"guest";

        public const string ShortDateTimeFormat = @"yyyy-MM-dd'T'HH:mm:ss.fff";

        public const string InernalConsumerCode = @"000";

        public const string ApplicationProductName = @"Uos";

        public const string AdminEmailAddress = @"admin@umbiliko.com";

        public const string AdminMobileNumber = @"+506 83083003";

        public const string AdminPassword = @"SuperUser1";

        public static TimeSpan SessionTimeSpan { get; set; } = TimeSpan.FromDays(1);

        static List<string> _errors;

        internal static List<string> Errors
        {
            get { return _errors ?? (_errors = new List<string>()); }
        }

        public static readonly JsonSerializerSettings JsonSerializerSettings = new JsonSerializerSettings
        {
            ContractResolver = ContractResolver.Instance,
            DateFormatString = ShortDateTimeFormat,
            Error = new EventHandler<Newtonsoft.Json.Serialization.ErrorEventArgs>((currentObject, args) =>
            {
                Errors.Add(string.Format("{0} {1}. {2}", args.CurrentObject.GetType(), args.ErrorContext.Path, args.ErrorContext.Error.Message));

                args.ErrorContext.Handled = true;
            }),
            NullValueHandling = NullValueHandling.Ignore
        };

    }

    public class ContractResolver : DefaultContractResolver
    {
        public new static readonly ContractResolver Instance = new ContractResolver();

        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            JsonProperty property = base.CreateProperty(member, memberSerialization);

            if (member.MemberType == MemberTypes.Property && typeof(IEnumerable).IsAssignableFrom(property.PropertyType))
            {
                var shouldDeserialize = property.ShouldDeserialize;

                property.ShouldSerialize = obj =>
                {
                    var value = (member as PropertyInfo).GetValue(obj) as IEnumerable;
                    if (value == null || !value.GetEnumerator().MoveNext()) return false;
                    return shouldDeserialize != null ? shouldDeserialize(obj) : true;
                };
            }

            return property;
        }
    }
}