﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Constants
{
    public class PasswordLengths
    {
        public const byte Empty = 0;

        public const byte Large = 16;

        public const byte Medium = 8;
    }
}
