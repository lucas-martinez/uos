﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Constants
{
    public class AccessibilityLevels
    {
        /// <summary>
        /// A.K.A Pending. Nor the emails addresss or phone number have been confirmed.
        /// If the user login with an exxternal provider sucha s Google and the provider
        /// claims the user email is verified, the the user is recognized as authenticated.
        /// </summary>
        public const string Alleged = @"PEN";

        /// <summary>
        /// The user is authenticated, eiher using Uos cedentials or an external 
        /// provider;
        /// </summary>
        public const string Authenticated = @"AUT";

        /// <summary>
        /// The user is an authenticated Uos user with a company segment account 
        /// where the company of the segment is the same as the company of the product
        /// and the segment registered as an active consumer of the product.
        /// </summary>
        public const string Internal = @"INT";

        /// <summary>
        /// The user is an authenticated Uos user with a company segment account
        /// where the segment registered as an active consumer of the product.
        /// </summary>
        public const string Private = @"PRI";

        /// <summary>
        /// The user is an authenticated Uos user.
        /// </summary>
        public const string Protected = @"PRO";

        /// <summary>
        /// User may or not be authenticated (with an external provider) but not as an Uos user.
        /// </summary>
        public const string Public = @"PUB";

        /// <summary>
        /// The user is an authenticated Uos user and went thru the two factor authentication.
        /// </summary>
        public const string Secured = @"SEC";

        /// <summary>
        /// The user is autenticated by an external provider. However, it does mean
        /// the user is an authenticated Uos user unless the Protected flag is 
        /// present.
        /// </summary>
        public const string Social = @"SOC";
    }
}
