﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Constants
{
    public class RegionTypes
    {
        public const string City = @"CTY";

        public const string Province = @"PRO";

        public const string State = @"STA";
    }
}
