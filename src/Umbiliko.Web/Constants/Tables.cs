﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Constants
{
    public static class Tables
    {
        public const string Accounts = Schemes.Uos + Collections.Accounts;

        public const string Activities = Schemes.Uos + Collections.Activities;

        public const string Charges = Schemes.Uos + Collections.Charges;

        public const string Claims = Schemes.Uos + Collections.Claims;

        public const string Companies = Schemes.Uos + Collections.Companies;

        public const string Contracts = Schemes.Uos + Collections.Contracts;

        public const string Countries = Schemes.Uos + Collections.Countries;

        public const string Cultures = Schemes.Uos + Collections.Cultures;

        public const string Domains = Schemes.Uos + Collections.Domains;

        public const string Features = Schemes.Uos + Collections.Features;
        
        public const string Indicators = Schemes.Uos + Collections.Indicators;

        public const string Instances = Schemes.Uos + Collections.Instances;

        public const string Logins = Schemes.Uos + Collections.Logins;

        public const string Products = Schemes.Uos + Collections.Products;

        public const string Redirects = Schemes.Uos + Collections.Redirects;

        public const string Regions = Schemes.Uos + Collections.Regions;

        public const string Requests = Schemes.Uos + Collections.Requests;

        public const string Results = Schemes.Uos + Collections.Results;

        public const string Robots = Schemes.Uos + Collections.Robots;

        public const string Roles = Schemes.Uos + Collections.Roles;

        public const string Segments = Schemes.Uos + Collections.Segments;

        public const string Sessions = Schemes.Uos + Collections.Sessions;

        public const string Subscriptions = Schemes.Uos + Collections.Subscriptions;

        public const string Texts = Schemes.Uos + Collections.Texts;

        public const string Themes = Schemes.Uos + Collections.Themes;

        public const string Tokens = Schemes.Uos + Collections.Tokens;

        public const string Users = Schemes.Uos + Collections.Users;
    }
}
