﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Constants
{
    public enum EnterResponses : byte
    {
        Allow = 0,

        Deny = 128,

        DomainUnavailable = Deny | 127,

        NoMatch = Deny | 1,

        IdentityRequired = Deny | 2,

        CodeRequired = Deny | 3,

        PasswordRequired = Deny | 4,

        AnswerReuired = Deny | 5,

        EmailRequired = Deny | 6,

        MovilRequired = Deny | 7,

        ClientRequired = Deny | 8,

        UserRequired = Deny | 9,

        TokenRequired = Deny | 10,

        CompanyRequired = Deny | 11,

        CompanyAndClientRequired = Deny | 12,

        LockedOut = Deny | 20,

        InternalError = Deny | 30
    }
}
