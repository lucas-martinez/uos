﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Constants
{
    public static class Collections
    {
        public const string Accounts = @"accounts";

        public const string Activities = @"activities";

        public const string Robots = @"robots";

        public const string Charges = @"charges";

        public const string Claims = @"claims";

        public const string Companies = @"companies";

        public const string Constants = @"constants";

        public const string Contracts = @"contracts";

        public const string Consumers = @"consumers";

        public const string Countries = @"countries";

        public const string Cultures = @"cultures";

        public const string Currencies = @"currencies";

        public const string Customers = @"customers";

        public const string Domains = @"domains";

        public const string Features = @"features";
        
        public const string Indicators = @"indicators";

        public const string Instances = @"instances";

        public const string Logins = @"logins";

        public const string Products = @"products";

        public const string Redirects = @"redirects";

        public const string Regions = @"regions";

        public const string Requests = @"requests";

        public const string Results = @"results";

        public const string Roles = @"roles";

        public const string Segments = @"segments";

        public const string Sessions = @"sessions";

        public const string Sites = @"sites";

        public const string Subscriptions = @"subscriptions";

        public const string Suppliers = @"suppliers";

        public const string Texts = @"texts";

        public const string Themes = @"themes";

        public const string Tokens = @"tokens";

        public const string Users = @"users";
    }
}