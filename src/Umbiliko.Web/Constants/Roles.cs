﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Constants
{
    public static class Roles
    {
        public const string Administrator = @"administrator";

        public const string Architect = @"architect";

        public const string Customizer = @"customizer";

        public const string Debuger = @"debuger";

        public const string Designer = @"designer";

        public const string Developer = @"developer";

        public const string Editor = @"editor";

        public const string Manager = @"manager";

        public const string Scheduler = @"scheduler";

        public const string Tester = @"tester";

        public const string Translator = @"translator";
    }
}
