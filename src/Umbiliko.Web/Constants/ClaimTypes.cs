﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Umbiliko.Web.Constants
{
    public static class ClaimTypes
    {
        public const string Trademark = @"Umbiliko.Identity.Trademark";

        public const string Client = @"Umbiliko.Identity.Client";

        public const string EmailAddress = global::System.Security.Claims.ClaimTypes.Email;

        public const string Identifier = global::System.Security.Claims.ClaimTypes.NameIdentifier;

        public const string IdentityProvider = @"http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider";

        public const string SecurityStamp = "AspNet.Identity.SecurityStamp";

        public const string UserName = global::System.Security.Claims.ClaimTypes.Name;
        
        /*public static class Email
        {
            public const string Default = ClaimTypes.Email;

            public const string Personal = @"email:personal";

            public const string Work = @"email:work";
        }*/

        public static class Location
        {
            public const string Country = @"location:country";

            public const string PostalCode = "location:postal";
        }

        /*public static class Name
        {
            public const string Default = @"name";

            public const string Family = @"name:family";

            public const string First = @"name:first";

            public const string Last = @"name:last";

            public const string Middle = @"name:middle";
        }*/

        public static class Phone
        {
            public const string Default = @"phone";

            public const string Home = @"phone:home";

            public const string Mobile = @"phone:mobile";

            public const string Work = @"phone:work";
        }

        //
        // Summary:
        //     http://schemas.xmlsoap.org/ws/2009/09/identity/claims/actor.
        public const string Actor = global::System.Security.Claims.ClaimTypes.Actor;
        //
        // Summary:
        //     The URI for a claim that specifies the anonymous user; http://schemas.xmlsoap.org/ws/2005/05/identity/claims/anonymous.
        public const string Anonymous = global::System.Security.Claims.ClaimTypes.Anonymous;
        //
        // Summary:
        //     The URI for a claim that specifies details about whether an identity is authenticated,
        //     http://schemas.xmlsoap.org/ws/2005/05/identity/claims/authenticated.
        public const string Authentication = global::System.Security.Claims.ClaimTypes.Authentication;
        //
        // Summary:
        //     The URI for a claim that specifies the instant at which an entity was authenticated;
        //     http://schemas.microsoft.com/ws/2008/06/identity/claims/authenticationinstant.
        public const string AuthenticationInstant = global::System.Security.Claims.ClaimTypes.AuthenticationInstant;
        //
        // Summary:
        //     The URI for a claim that specifies the method with which an entity was authenticated;
        //     http://schemas.microsoft.com/ws/2008/06/identity/claims/authenticationmethod.
        public const string AuthenticationMethod = global::System.Security.Claims.ClaimTypes.AuthenticationMethod;

        //
        // Summary:
        //     The URI for a claim that specifies an authorization decision on an entity; http://schemas.xmlsoap.org/ws/2005/05/identity/claims/authorizationdecision.
        public const string AuthorizationDecision = global::System.Security.Claims.ClaimTypes.AuthorizationDecision;
        //
        // Summary:
        //     The URI for a claim that specifies the cookie path; http://schemas.microsoft.com/ws/2008/06/identity/claims/cookiepath.
        public const string CookiePath = global::System.Security.Claims.ClaimTypes.CookiePath;
        //
        // Summary:
        //     The URI for a claim that specifies the country/region in which an entity resides,
        //     http://schemas.xmlsoap.org/ws/2005/05/identity/claims/country.
        public const string Country = global::System.Security.Claims.ClaimTypes.Country;
        //
        // Summary:
        //     The URI for a claim that specifies the date of birth of an entity, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/dateofbirth.
        public const string DateOfBirth = global::System.Security.Claims.ClaimTypes.DateOfBirth;
        //
        // Summary:
        //     The URI for a claim that specifies the deny-only primary group SID on an entity;
        //     http://schemas.microsoft.com/ws/2008/06/identity/claims/denyonlyprimarygroupsid.
        //     A deny-only SID denies the specified entity to a securable object.
        public const string DenyOnlyPrimaryGroupSid = global::System.Security.Claims.ClaimTypes.DenyOnlyPrimaryGroupSid;
        //
        // Summary:
        //     The URI for a claim that specifies the deny-only primary SID on an entity; http://schemas.microsoft.com/ws/2008/06/identity/claims/denyonlyprimarysid.
        //     A deny-only SID denies the specified entity to a securable object.
        public const string DenyOnlyPrimarySid = global::System.Security.Claims.ClaimTypes.DenyOnlyPrimarySid;
        //
        // Summary:
        //     The URI for a claim that specifies a deny-only security identifier (SID) for
        //     an entity, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/denyonlysid.
        //     A deny-only SID denies the specified entity to a securable object.
        public const string DenyOnlySid = global::System.Security.Claims.ClaimTypes.DenyOnlySid;
        //
        // Summary:
        //     The URI for a claim that specifies the DNS name associated with the computer
        //     name or with the alternative name of either the subject or issuer of an X.509
        //     certificate, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/dns.
        public const string Dns = global::System.Security.Claims.ClaimTypes.Dns;
        //
        // Summary:
        //     http://schemas.microsoft.com/ws/2008/06/identity/claims/dsa.
        public const string Dsa = "http://schemas.microsoft.com/ws/2008/06/identity/claims/dsa";
        //
        // Summary:
        //     The URI for a claim that specifies the email address of an entity, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/email.
        public const string Email = global::System.Security.Claims.ClaimTypes.Email;
        //
        // Summary:
        //     http://schemas.microsoft.com/ws/2008/06/identity/claims/expiration.
        public const string Expiration = "http://schemas.microsoft.com/ws/2008/06/identity/claims/expiration";
        //
        // Summary:
        //     http://schemas.microsoft.com/ws/2008/06/identity/claims/expired.
        public const string Expired = "http://schemas.microsoft.com/ws/2008/06/identity/claims/expired";
        //
        // Summary:
        //     The URI for a claim that specifies the gender of an entity, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/gender.
        public const string Gender = global::System.Security.Claims.ClaimTypes.Gender;
        //
        // Summary:
        //     The URI for a claim that specifies the given name of an entity, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname.
        public const string GivenName = global::System.Security.Claims.ClaimTypes.GivenName;
        //
        // Summary:
        //     The URI for a claim that specifies the SID for the group of an entity, http://schemas.microsoft.com/ws/2008/06/identity/claims/groupsid.
        public const string GroupSid = global::System.Security.Claims.ClaimTypes.GroupSid;
        //
        // Summary:
        //     The URI for a claim that specifies a hash value, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/hash.
        public const string Hash = global::System.Security.Claims.ClaimTypes.Hash;
        //
        // Summary:
        //     The URI for a claim that specifies the home phone number of an entity, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/homephone.
        public const string HomePhone = global::System.Security.Claims.ClaimTypes.HomePhone;
        //
        // Summary:
        //     http://schemas.microsoft.com/ws/2008/06/identity/claims/ispersistent.
        public const string IsPersistent = global::System.Security.Claims.ClaimTypes.IsPersistent;
        //
        // Summary:
        //     The URI for a claim that specifies the locale in which an entity resides, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/locality.
        public const string Locality = global::System.Security.Claims.ClaimTypes.Locality;
        //
        // Summary:
        //     The URI for a claim that specifies the mobile phone number of an entity, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/mobilephone.
        public const string MobilePhone = global::System.Security.Claims.ClaimTypes.MobilePhone;
        //
        // Summary:
        //     The URI for a claim that specifies the name of an entity, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name.
        public const string Name = global::System.Security.Claims.ClaimTypes.Name;
        //
        // Summary:
        //     The URI for a claim that specifies the name of an entity, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier.
        public const string NameIdentifier = global::System.Security.Claims.ClaimTypes.NameIdentifier;
        //
        // Summary:
        //     The URI for a claim that specifies the alternative phone number of an entity,
        //     http://schemas.xmlsoap.org/ws/2005/05/identity/claims/otherphone.
        public const string OtherPhone = global::System.Security.Claims.ClaimTypes.OtherPhone;
        //
        // Summary:
        //     The URI for a claim that specifies the postal code of an entity, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/postalcode.
        public const string PostalCode = global::System.Security.Claims.ClaimTypes.PostalCode;

        //
        // Summary:
        //     The URI for a claim that specifies the primary group SID of an entity, http://schemas.microsoft.com/ws/2008/06/identity/claims/primarygroupsid.
        public const string PrimaryGroupSid = global::System.Security.Claims.ClaimTypes.PrimaryGroupSid;
        
        //
        // Summary:
        //     The URI for a claim that specifies the primary SID of an entity, http://schemas.microsoft.com/ws/2008/06/identity/claims/primarysid.
        public const string PrimarySid = "http://schemas.microsoft.com/ws/2008/06/identity/claims/primarysid";
        
        //
        // Summary:
        //     The URI for a claim that specifies the role of an entity, http://schemas.microsoft.com/ws/2008/06/identity/claims/role.
        public const string Role = "http://schemas.microsoft.com/ws/2008/06/identity/claims/role";
        
        //
        // Summary:
        //     The URI for a claim that specifies an RSA key, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/rsa.
        public const string Rsa = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/rsa";
        
        //
        // Summary:
        //     The URI for a claim that specifies a serial number, http://schemas.microsoft.com/ws/2008/06/identity/claims/serialnumber.
        public const string SerialNumber = "http://schemas.microsoft.com/ws/2008/06/identity/claims/serialnumber";
        
        //
        // Summary:
        //     The URI for a claim that specifies a security identifier (SID), http://schemas.xmlsoap.org/ws/2005/05/identity/claims/sid.
        public const string Sid = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/sid";
        
        //
        // Summary:
        //     The URI for a claim that specifies a service principal name (SPN) claim, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/spn.
        public const string Spn = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/spn";
        
        //
        // Summary:
        //     The URI for a claim that specifies the state or province in which an entity resides,
        //     http://schemas.xmlsoap.org/ws/2005/05/identity/claims/stateorprovince.
        public const string StateOrProvince = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/stateorprovince";
        
        //
        // Summary:
        //     The URI for a claim that specifies the street address of an entity, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/streetaddress.
        public const string StreetAddress = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/streetaddress";
        
        //
        // Summary:
        //     The URI for a claim that specifies the surname of an entity, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname.
        public const string Surname = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname";
        
        //
        // Summary:
        //     The URI for a claim that identifies the system entity, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/system.
        public const string System = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/system";
        
        //
        // Summary:
        //     The URI for a claim that specifies a thumbprint, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/thumbprint.
        //     A thumbprint is a globally unique SHA-1 hash of an X.509 certificate.
        public const string Thumbprint = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/thumbprint";
        
        //
        // Summary:
        //     The URI for a claim that specifies a user principal name (UPN), http://schemas.xmlsoap.org/ws/2005/05/identity/claims/upn.
        public const string Upn = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/upn";
        
        //
        // Summary:
        //     The URI for a claim that specifies a URI, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/uri.
        public const string Uri = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/uri";
        //
        // Summary:
        //     http://schemas.microsoft.com/ws/2008/06/identity/claims/userdata.
        public const string UserData = "http://schemas.microsoft.com/ws/2008/06/identity/claims/userdata";
        
        //
        // Summary:
        //     http://schemas.microsoft.com/ws/2008/06/identity/claims/version.
        public const string Version = "http://schemas.microsoft.com/ws/2008/06/identity/claims/version";
        //
        // Summary:
        //     The URI for a claim that specifies the webpage of an entity, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/webpage.
        public const string Webpage = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/webpage";
        
        //
        // Summary:
        //     The URI for a distinguished name claim of an X.509 certificate, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/x500distinguishedname.
        //     The X.500 standard defines the methodology for defining distinguished names that
        //     are used by X.509 certificates.
        public const string X500DistinguishedName = global::System.Security.Claims.ClaimTypes.X500DistinguishedName;
    }
}
