﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Compilation;
using System.Web.Http;
using System.Web.Http.Description;

namespace Uos.Web.Controllers
{
    using Constants;
    using Contracts;
    using Providers;

    /// <summary>
    /// Use [ApiExplorerSettings(IgnoreApi = true)] to except from descriptions
    /// </summary>
    [Http.Feature(Features.Configuration), RoutePrefix("api/product")]
    public class ProductApiController : Http.HttpController
    {
        [Http.Activity("inquire-product"), HttpGet, Route("")]
        public IHttpActionResult Inquire()
        {
            var provider = new ProductDescriptionProvider(BuildManager.GetGlobalAsaxType().BaseType.Assembly);
            
            return Ok(provider.Description);
        }
    }
}
