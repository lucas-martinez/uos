﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Services
{
    using Assertion;

    public class SmtpService : IDisposable, IIdentityMessageService, ISmtpService
    {
        private SmtpClient _client;

        public NameValueCollection AppSettings
        {
            get { return ConfigurationManager.AppSettings; }
        }

        public SmtpClient Client
        {
            get
            {
                var client = _client;

                if (client != null) return client;

                client = new SmtpClient(AppSettings["smtp:Host"]);

                int port;
                if (int.TryParse(AppSettings["smtp:Port"], out port))
                    client.Port = port;

                bool enableSsl;
                if (bool.TryParse(AppSettings["smtp:EnableSsl"], out enableSsl))
                    client.EnableSsl = enableSsl;

                SmtpDeliveryMethod deliveryMethod;
                if (Enum.TryParse<SmtpDeliveryMethod>(AppSettings["smtp:DeliveryMethod"], out deliveryMethod))
                    client.DeliveryMethod = deliveryMethod;

                var userName = AppSettings["smtp:UserName"];
                var password = AppSettings["smtp:Password"];

                if (userName != null || password != null)
                {
                    client.UseDefaultCredentials = false;
                    client.Credentials = new NetworkCredential(userName, password);
                }

                _client = client;

                return client;
            }
        }

        public virtual IResult Send(MailMessage message)
        {
            IResult result;

            try
            {
                Client.Send(message);

                result = Result.Default;
            }
            catch (Exception exception)
            {
                var context = new ErrorContext(exception, 0);

                Trace.Error(context);

                result = Result.Fail(context);
            }

            return result;
        }

        public virtual async Task<IResult> SendAsync(MailMessage message)
        {
            IResult result;

            try
            {
                await Client.SendMailAsync(message);

                result = Result.Default;
            }
            catch (Exception exception)
            {
                var context = new ErrorContext(exception, 0);

                Trace.Error(context);

                result = Result.Fail(context);
            }

            return result;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                var client = _client;
                _client = null;

                if (client != null)
                {
                    client.Dispose();
                }
            }
        }

        void IDisposable.Dispose()
        {
            Dispose(true);
        }

        Task IIdentityMessageService.SendAsync(IdentityMessage message)
        {
            var mailMessage = new MailMessage
            {
                Body = message.Body,
                Subject = message.Subject
            };

            mailMessage.To.Add(message.Destination);

            return SendAsync(mailMessage);
        }
    }
}
