﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Uos.Web.Services
{
    using Assertion;
    using Constants;
    using Contracts;
    using Results;

    public class ApplicationUserStore : IDisposable,
        //IQueryableUserStore<UserData, string>,
        IUserClaimStore<SessionInfo, string>,
        IUserEmailStore<SessionInfo, string>,
        IUserLockoutStore<SessionInfo, string>,
        IUserLoginStore<SessionInfo, string>,
        IUserPasswordStore<SessionInfo, string>,
        IUserPhoneNumberStore<SessionInfo, string>,
        IUserRoleStore<SessionInfo, string>,
        IUserSecurityStampStore<SessionInfo, string>,
        IUserStore<SessionInfo, string>,
        IUserTwoFactorStore<SessionInfo, string>
    {
        readonly ISessionStore _sessionStore;
        readonly IUserStore _userStore;

        public ApplicationUserStore(ISessionStore sessionStore)
        {
            _sessionStore = sessionStore;
            _userStore = sessionStore.UserStore;
        }

        ~ApplicationUserStore()
        {
            Dispose(false);
        }

        public ISessionStore SessionStore
        {
            get { return _sessionStore; }
        }

        public IUserStore UserStore
        {
            get { return _userStore; }
        }

        /*public async Task<UserResult> CreateAsync(SessionInfo user, string password)
        {
            var result = Result.All(ValidateUserName(user.Name), ValidateEmailAddress(user.Email), ValidatePassword(password));

            if (!result.Success) return UserResult.Fail(result.Messages);

            user.PasswordHash = PasswordHasher.HashPassword(password);

            return await CreateAsync(user);
        }*/

        protected virtual void Dispose(bool disposing)
        {
        }

        #region - IDisposable -

        void IDisposable.Dispose()
        {
            Dispose(true);
        }

        #endregion - IDisposable -

        #region - IQueryableUserStore<UserData, string> -

        /*IQueryable<UserData> IQueryableUserStore<UserData, string>.Users
        {
            get
            {
                throw new NotImplementedException();
            }
        }*/

        #endregion - IQueryableUserStore<UserData, string> -

        #region - IUserEmailStore<UserData, string> -

        async Task<SessionInfo> IUserEmailStore<SessionInfo, string>.FindByEmailAsync(string email)
        {
            var result = await UserStore.FindUserByEmailAsync(email);

            return result != null && result.Success ? result.Value : null;
        }

        Task<string> IUserEmailStore<SessionInfo, string>.GetEmailAsync(SessionInfo session)
        {
            return Task.FromResult(session.User.Email);
        }

        Task<bool> IUserEmailStore<SessionInfo, string>.GetEmailConfirmedAsync(SessionInfo session)
        {
            return Task.FromResult(session.User.EmailConfirmed ?? false);
        }

        async Task IUserEmailStore<SessionInfo, string>.SetEmailAsync(SessionInfo session, string email)
        {
            var result = await UserStore.UpdateAsync(new UserRequest
            {
                //Id = session.Id,
                Data = new UserInfo
                {
                    Email = email,
                    Name = session.User.Name,
                }
            });

            if (result != null && result.Success)
            {
                result.Value.Update(new ClaimsIdentity()).Update(session);
            }
        }

        async Task IUserEmailStore<SessionInfo, string>.SetEmailConfirmedAsync(SessionInfo session, bool confirmed)
        {
            var result = await UserStore.UpdateAsync(new UserInfo
            {
                Name = session.User.Name,
                //UserId = user.UserId,
                EmailConfirmed = confirmed
            });

            if (result != null && result.Success)
            {
                result.Value.Update(new ClaimsIdentity()).Update(session);
            }
        }

        #endregion - IUserEmailStore<UserData, string> -

        #region - IUserClaimStore<UserData, string> -

        async Task IUserClaimStore<SessionInfo, string>.AddClaimAsync(SessionInfo session, Claim claim)
        {
            var data = new UserInfo
            {
                //UserId = user.UserId,
                Name = session.User.Name,
                Claims = new[]
                {
                    (ClaimInfo)claim
                }
            };

            var result = await UserStore.UpdateAsync(new UserRequest(data, Actions.Create));

            if (result != null && result.Success)
            {
                session.User.AddClaimExact(claim);
            }
        }

        Task<IList<Claim>> IUserClaimStore<SessionInfo, string>.GetClaimsAsync(SessionInfo session)
        {
            List<Claim> claims = session;

            return Task.FromResult((IList<Claim>)claims);
        }

        async Task IUserClaimStore<SessionInfo, string>.RemoveClaimAsync(SessionInfo session, Claim claim)
        {
            var data = new UserInfo
            {
                //UserId = user.UserId,
                Name = session.User.Name,
                Claims = new[]
                {
                    (ClaimInfo)claim
                }
            };

            var result = await UserStore.UpdateAsync(new UserRequest(data, Actions.Destroy));

            if (result.Success)
            {
                session.User.RemoveClaim(claim);
            }
        }

        #endregion - IUserClaimStore<UserData, string> -

        #region - IUserLockoutStore<UserData, string> -

        Task<int> IUserLockoutStore<SessionInfo, string>.GetAccessFailedCountAsync(SessionInfo session)
        {
            return Task.FromResult(session.User.AccessFailedCount ?? 0);
        }

        Task<bool> IUserLockoutStore<SessionInfo, string>.GetLockoutEnabledAsync(SessionInfo session)
        {
            return Task.FromResult(session.User.LockoutEnabled ?? false);
        }

        Task<DateTimeOffset> IUserLockoutStore<SessionInfo, string>.GetLockoutEndDateAsync(SessionInfo session)
        {
            var dateUtc = session.User.LockoutEndDateUtc;

            var date = dateUtc.HasValue ? dateUtc.Value.ToLocalDateTimeOffset(DateTimeKind.Local) : DateTimeOffset.MinValue;

            return Task.FromResult(date);
        }

        async Task<int> IUserLockoutStore<SessionInfo, string>.IncrementAccessFailedCountAsync(SessionInfo session)
        {
            var result = await UserStore.UpdateAsync(new UserInfo
            {
                Name = session.User.Name,
                //UserId = user.UserId,
                AccessFailedCount = session.User.AccessFailedCount++,
            });

            if (result.Success && result.Value != null)
            {
                result.Value.Update(new ClaimsIdentity()).Update(session);
            }

            return session.User.AccessFailedCount ?? 0;
        }

        async Task IUserLockoutStore<SessionInfo, string>.ResetAccessFailedCountAsync(SessionInfo session)
        {
            var result = await UserStore.UpdateAsync(new UserInfo
            {
                Name = session.User.Name,
                //UserId = user.UserId,
                AccessFailedCount = 0,
            });

            if (result.Success && result.Value != null)
            {
                result.Value.Update(new ClaimsIdentity()).Update(session);
            }
        }

        async Task IUserLockoutStore<SessionInfo, string>.SetLockoutEnabledAsync(SessionInfo session, bool enabled)
        {
            var result = await UserStore.UpdateAsync(new UserInfo
            {
                Name = session.User.Name,
                //UserId = user.UserId,
                LockoutEnabled = enabled
            });

            if (result.Success && result.Value != null)
            {
                result.Value.Update(new ClaimsIdentity()).Update(session);
            }
        }

        async Task IUserLockoutStore<SessionInfo, string>.SetLockoutEndDateAsync(SessionInfo session, DateTimeOffset lockoutEnd)
        {
            var result = await UserStore.UpdateAsync(new UserInfo
            {
                Name = session.User.Name,
                //UserId = user.UserId,
                LockoutEndDateUtc = lockoutEnd.UtcDateTime
            });

            if (result != null && result.Success)
            {
                result.Value.Update(new ClaimsIdentity()).Update(session);
            }
        }

        #endregion - IUserLockoutStore<UserData, string> -

        #region - IUserLoginStore<UserData, string> -

        async Task IUserLoginStore<SessionInfo, string>.AddLoginAsync(SessionInfo session, UserLoginInfo login)
        {
            var data = new UserInfo
            {
                //UserId = user.UserId,
                Name = session.User.Name,
                Logins = new[]
                {
                    new LoginInfo
                    {
                        LoginProvider = login.LoginProvider,
                        LoginKey = login.ProviderKey
                    }
                }
            };

            var result = await UserStore.UpdateAsync(new UserRequest(data, Actions.Create));

            if (result != null && result.Success)
            {
                session.User.AddLoginInfo(result.Value.Logins.FirstOrDefault(l => l.LoginProvider == login.LoginProvider && l.LoginKey == login.ProviderKey));
            }
        }

        async Task<SessionInfo> IUserLoginStore<SessionInfo, string>.FindAsync(UserLoginInfo login)
        {
            var result = await UserStore.FindUserByLoginAsync(loginProvider: login.LoginProvider, providerKey: login.ProviderKey);

            return result.Value;
        }

        Task<IList<UserLoginInfo>> IUserLoginStore<SessionInfo, string>.GetLoginsAsync(SessionInfo session)
        {
            IList<UserLoginInfo> logins = session.User.Logins
                .Select(login => new UserLoginInfo(loginProvider: login.LoginProvider, providerKey: login.LoginKey))
                .ToList();

            return Task.FromResult(logins);
        }

        async Task IUserLoginStore<SessionInfo, string>.RemoveLoginAsync(SessionInfo session, UserLoginInfo login)
        {
            var data = new UserInfo
            {
                //UserId = user.UserId,
                Name = session.User.Name,
                Logins = new[]
                {
                    new LoginInfo
                    {
                        LoginProvider = login.LoginProvider,
                        LoginKey = login.ProviderKey
                    }
                }
            };

            var result = await UserStore.UpdateAsync(new UserRequest(data, Actions.Destroy));

            if (result.Success)
            {
                session.User.RemoveLoginInfo(loginProvider: login.LoginProvider, loginKey: login.ProviderKey);
            }
        }

        #endregion - IUserLoginStore<UserData, string> -

        #region - IUserPasswordStore<UserData, string> -

        Task<string> IUserPasswordStore<SessionInfo, string>.GetPasswordHashAsync(SessionInfo session)
        {
            return Task.FromResult(session.User.PasswordHash);
        }

        Task<bool> IUserPasswordStore<SessionInfo, string>.HasPasswordAsync(SessionInfo session)
        {
            return Task.FromResult(!string.IsNullOrEmpty(session.User.PasswordHash));
        }

        async Task IUserPasswordStore<SessionInfo, string>.SetPasswordHashAsync(SessionInfo session, string passwordHash)
        {
            var data = new UserInfo
            {
                Name = session.User.Name,
                PasswordHash = passwordHash,
                //UserId = user.UserId
            };

            var result = await UserStore.UpdateAsync(data);

            if (result != null && result.Success)
            {
                result.Value.Update(new ClaimsIdentity()).Update(session);
            }
        }

        #endregion - IUserPasswordStore<UserData, string> -

        #region - IUserPhoneNumberStore<UserData, string> -

        Task<string> IUserPhoneNumberStore<SessionInfo, string>.GetPhoneNumberAsync(SessionInfo session)
        {
            return Task.FromResult(session.User.PhoneNumber);
        }

        Task<bool> IUserPhoneNumberStore<SessionInfo, string>.GetPhoneNumberConfirmedAsync(SessionInfo session)
        {
            return Task.FromResult(session.User.PhoneNumberConfirmed ?? false);
        }

        async Task IUserPhoneNumberStore<SessionInfo, string>.SetPhoneNumberAsync(SessionInfo session, string phoneNumber)
        {
            var result = await UserStore.UpdateAsync(new UserInfo
            {
                Name = session.User.Name,
                //UserId = user.UserId,
                PhoneNumber = phoneNumber
            });

            if (result != null && result.Success)
            {
                result.Value.Update(new ClaimsIdentity()).Update(session);
            }
        }

        async Task IUserPhoneNumberStore<SessionInfo, string>.SetPhoneNumberConfirmedAsync(SessionInfo session, bool confirmed)
        {
            var result = await UserStore.UpdateAsync(new UserInfo
            {
                Name = session.User.Name,
                //UserId = user.UserId,
                PhoneNumberConfirmed = confirmed
            });

            if (result.Success && result.Value != null)
            {
                result.Value.Update(new ClaimsIdentity()).Update(session);
            }
        }

        #endregion - IUserPhoneNumberStore<UserData, string> -

        #region - IUserSecurityStampStore<UserData, string> -

        Task<string> IUserSecurityStampStore<SessionInfo, string>.GetSecurityStampAsync(SessionInfo session)
        {
            return Task.FromResult(session.User.SecurityStamp);
        }

        async Task IUserSecurityStampStore<SessionInfo, string>.SetSecurityStampAsync(SessionInfo session, string stamp)
        {
            var result = await UserStore.UpdateAsync(new UserInfo
            {
                Name = session.User.Name,
                //UserId = user.UserId,
                SecurityStamp = stamp
            });

            if (result != null && result.Success)
            {
                result.Value.Update(new ClaimsIdentity()).Update(session);
            }
        }

        #endregion - IUserSecurityStampStore<UserData, string> -

        #region - IUserRoleStore<UserData, string> -

        async Task IUserRoleStore<SessionInfo, string>.AddToRoleAsync(SessionInfo session, string roleName)
        {
            var data = new SessionInfo
            {
                Account = new AccountInfo
                {
                    CompanyCode = session.Account.CompanyCode,
                    SegmentCode = session.Account.SegmentCode,
                    Roles = new[]
                    {
                        roleName
                    }
                },
                User = new UserInfo
                {
                    Name = session.User.Name
                }
                /*UserId = user.UserId,
                */
            };

            var result = await UserStore.UpdateAsync(new UserRequest(data.User, Actions.Create));

            if (result != null && result.Success)
            {
                result.Value.Update(new ClaimsIdentity()).Update(session);
            }
        }

        Task<IList<string>> IUserRoleStore<SessionInfo, string>.GetRolesAsync(SessionInfo user)
        {
            return Task.FromResult((IList<string>)new List<string>()/*user.Roles.ToList()*/);
        }

        Task<bool> IUserRoleStore<SessionInfo, string>.IsInRoleAsync(SessionInfo user, string roleName)
        {
            return Task.FromResult(false);// user.Roles.Any(value => value == roleName));
        }

        async Task IUserRoleStore<SessionInfo, string>.RemoveFromRoleAsync(SessionInfo session, string roleName)
        {
            var data = new UserInfo
            {
                Name = session.User.Name,
                //UserId = user.UserId,
                /*Roles = new[]
                {
                    roleName
                }*/
            };

            var result = await UserStore.UpdateAsync(new UserRequest(data, Actions.Destroy));

            if (result != null && result.Success)
            {
                result.Value.Update(new ClaimsIdentity()).Update(session);
            }
        }

        #endregion - IUserRoleStore<UserData, string> -

        #region - IUserStore<UserData, string> -

        async Task IUserStore<SessionInfo, string>.CreateAsync(SessionInfo session)
        {
            SessionInfo data = session;

            var result = await SessionStore.CreateUserAsync(session.User);
        }

        Task IUserStore<SessionInfo, string>.DeleteAsync(SessionInfo session)
        {
            var data = new UserInfo
            {
                Name = session.User.Name,
                //UserId = user.UserId
            };

            return UserStore.DestroyUserAsync(data);
        }

        async Task<SessionInfo> IUserStore<SessionInfo, string>.FindByIdAsync(string userId)
        {
            Guid id;

            if (Guid.TryParse(userId, out id))
            {
                var result = await SessionStore.FindSessionByIdAsync(id);

                return result.Success ? result.Value.User : null;
            }

            return null;
        }

        async Task<SessionInfo> IUserStore<SessionInfo, string>.FindByNameAsync(string name)
        {
            var result = await UserStore.FindUserAsync(name);

            return result.Value;
        }

        async Task IUserStore<SessionInfo, string>.UpdateAsync(SessionInfo session)
        {
            var result = await UserStore.UpdateAsync(session.User);

            if (result != null && result.Success)
            {
                result.Value.Update(new ClaimsIdentity()).Update(session);
            }
        }

        #endregion - IUserStore<UserData, string> -

        #region - IUserTwoFactorStore<UserData, string> -

        Task<bool> IUserTwoFactorStore<SessionInfo, string>.GetTwoFactorEnabledAsync(SessionInfo session)
        {
            return Task.FromResult(session.User.TwoFactorEnabled ?? false);
        }

        async Task IUserTwoFactorStore<SessionInfo, string>.SetTwoFactorEnabledAsync(SessionInfo session, bool enabled)
        {
            var result = await UserStore.UpdateAsync(new UserInfo
            {
                Name = session.User.Name,
                //UserId = user.UserId,
                TwoFactorEnabled = enabled
            });

            if (result != null && result.Success)
            {
                result.Value.Update(new ClaimsIdentity()).Update(session);
            }
        }

        #endregion - IUserTwoFactorStore<UserData, string> -
    }
}
