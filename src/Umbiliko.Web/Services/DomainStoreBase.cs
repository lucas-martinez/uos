﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Services
{
    using Contracts;

    public abstract class DomainStoreBase
    {
        readonly static ConcurrentDictionary<string, DomainInfo> _dictionary;
        readonly static ConcurrentDictionary<string, object> _locks;

        static DomainStoreBase()
        {
            _dictionary = new ConcurrentDictionary<string, DomainInfo>();
            _locks = new ConcurrentDictionary<string, object>();
        }
        
        public async Task<DomainInfo> FindAsync(string domainName)
        {
            DomainInfo data;

            if (domainName == "localhost")
            {
                return DomainInfo.LocalHost();
            }

            if (!_dictionary.TryGetValue(domainName, out data) || data.ExpiresUtc < DateTime.UtcNow)
            {
                data = await QueryAsync(domainName);

                if (data != null)
                {
                    _dictionary.AddOrUpdate(domainName, data, (key, other) => { return data; });
                }
            }

            return data;
        }
        
        protected abstract Task<DomainInfo> QueryAsync(string domainName);
    }
}
