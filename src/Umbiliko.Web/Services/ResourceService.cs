﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Services
{
    public class ResourceService
    {
        public IDictionary<object, string> GetConstantList<T>(ResourceManager resources = null)
        {
            return resources.ToDictionary<T>();
        }

        public NameValueCollection GetCollection<T>(ResourceManager resources = null)
        {
            var collection = new NameValueCollection();

            var fields = Utilities.ReflectionUtilities.GetConstantFields(typeof(T));

            Func<FieldInfo, string> getDisplayName = (field) =>
            {
                var attr = field.GetCustomAttribute<DisplayNameAttribute>(false);

                return attr != null ? attr.DisplayName : field.Name;
            };

            Func<FieldInfo, string> getString = resources != null
                ? (field) => resources.GetString(field.Name) ?? getDisplayName(field)
                : getDisplayName;

            foreach (var field in fields)
            {
                var name = (string)field.GetValue(null);
                var text = getString(field);
                collection.Add(name, text);
            }

            return collection;
        }
    }
}
