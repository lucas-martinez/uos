﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Xsl;

namespace Uos.Web.Services
{
    public class ExceptionService
    {
        public static StringBuilder PrintExceptionToHtml(Exception ex)
        {   
            //initialize a TextWriter, in this case a StringWriter and set it to write to a StringBuilder
            var sb = new StringBuilder();

            var sw = new StringWriter(sb);

            sw.WriteExceptionHtml(ex);

            //outputting to the console the HTML exception (you can send it as the message body of an email)
            //Console.WriteLine(sw);

            return sb;
        }

        public static void SendEmail(Exception ex)
        {
            //var name = ex.GetType().TrimEnd("");
        }
    }
}
