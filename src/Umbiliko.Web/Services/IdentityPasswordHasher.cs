﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace Uos.Web.Services
{
    public class IdentityPasswordHasher : Microsoft.AspNet.Identity.IPasswordHasher
    {
        string Microsoft.AspNet.Identity.IPasswordHasher.HashPassword(string password)
        {
            return new PasswordHasher().HashPassword(password);
        }

        PasswordVerificationResult Microsoft.AspNet.Identity.IPasswordHasher.VerifyHashedPassword(string hashedPassword, string providedPassword)
        {
            return new PasswordHasher().VerifyHashedPassword(hashedPassword, providedPassword) ? PasswordVerificationResult.Success : PasswordVerificationResult.Failed;
        }
    }
}
