﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Services
{
    using Contracts;

    public class FtpArchiveService : IArchivesService
    {
        private static FtpArchiveService _instance;

        public static FtpArchiveService Default
        {
            get { return _instance ?? (_instance = new FtpArchiveService("", 0, "")); }
        }

        private readonly ConcurrentDictionary<string, string> _archives;
        
        public FtpArchiveService(string host, int port, string path)
        {
            _archives = new ConcurrentDictionary<string, string>();
        }

        public virtual Stream Open(string path)
        {
            return null;
        }

        public virtual Task<Stream> OpenAsync(string path)
        {
            return Task.FromResult<Stream>(null);
        }
    }
}
