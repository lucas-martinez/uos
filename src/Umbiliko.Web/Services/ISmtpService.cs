﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Services
{
    using Assertion;

    /// <summary>
    /// SMTP Service
    /// </summary>
    public interface ISmtpService
    {
        /// <summary>
        /// Send mail message
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        IResult Send(MailMessage message);

        /// <summary>
        /// Send mail message asyncronous
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        Task<IResult> SendAsync(MailMessage message);
    }
}
