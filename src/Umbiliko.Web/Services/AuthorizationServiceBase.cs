﻿using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Compilation;
using Uos.Assertion;

namespace Uos.Web.Services
{
    using Contracts;
    using Providers;

    public abstract class AuthorizationServiceBase : IAuthorizationService
    {
        static string _domainName;
        static string _productName;

        IAuthenticationContext _authentication;
        HttpContextBase _httpContext;
        IOwinContext _owinContext;

        protected IAuthenticationContext Authentication
        {
            get { return _authentication ?? (_authentication = OwinContext.Get<IAuthenticationContext>()); }
        }

        protected string DomainName
        {
            get { return _domainName ?? (_domainName = HttpContext.Request.Url.Host); }
        }

        protected HttpContextBase HttpContext
        {
            get { return _httpContext; }
        }

        protected IOwinContext OwinContext
        {
            get { return _owinContext ?? (_owinContext = ServicesContainer.Default.GetInstance<IOwinContext>()); }
        }

        protected static string ProductName
        {
            get
            {
                Func<Assembly, string> getAssemblyProduct = (assembly) =>
                {
                    var attribute = assembly.GetCustomAttributes(typeof(AssemblyProductAttribute), false).OfType<AssemblyProductAttribute>().FirstOrDefault();

                    return attribute != null ? attribute.Product : null;
                };

                return _productName ?? (_productName = getAssemblyProduct(BuildManager.GetGlobalAsaxType().BaseType.Assembly));
            }
        }

        protected abstract DomainInfo Site { get; }

        protected SessionInfo User
        {
            get { return Authentication.CurrentSession; }
        }

        protected virtual bool AuthorizeCore(HttpContextBase httpContext)
        {
            var domainName = _domainName ?? (_domainName = httpContext.Request.Url.Host);

            var productName = ProductName;

            return !(string.IsNullOrEmpty(domainName) || string.IsNullOrEmpty(productName));
        }

        protected virtual IResult Authorize(HttpContextBase httpContext, string activity, string feature)
        {
            var user = User;

            /*var roles = Site.Clients
                .Where(c => c.Client == user.Client)
                .SelectMany(c => c.Groups)
                .SelectMany(e => e.Roles)
                .Where(r => r.Activity == activity && r.Feature == feature);

            if (!roles.Any()) return Result.Fail("not allowed");*/

            return Result.Default;
        }
        
        IResult IAuthorizationService.Authorize(HttpContextBase httpContext, string activity, string feature)
        {
            _httpContext = httpContext;
            return Authorize(httpContext, activity, feature);
        }
    }
}
