﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Uos.Web.Services
{
    using Assertion;

    public interface IAuthorizationService
    {
        IResult Authorize(HttpContextBase httpContext, string activity, string feature);
    }
}
