﻿using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Uos.Web.Services
{
    using Contracts;

    internal class RecaptchaService
    {
        static string _secretKey;
        static string _siteKey;
        static string _siteVerify;

        public static string DefaultMediaType { get; set; } = "application/json";

        public static string SecretKey
        {
            get { return _siteKey ?? ConfigurationManager.AppSettings["recaptcha:SecretKey"]; }
            set { _siteKey = value; }
        }

        public static string SiteKey
        {
            get { return _siteKey ?? ConfigurationManager.AppSettings["recaptcha:SiteKey"]; }
            set { _siteKey = value; }
        }

        public static string SiteVerify
        {
            get { return _siteKey ?? ConfigurationManager.AppSettings["recaptcha:SiteVerify"]; }
            set { _siteKey = value; }
        }

        /// <summary>
        /// Verifying the user's response
        /// </summary>
        /// <param name="response">response(required) The value of 'g-recaptcha-response'.</param>
        /// <param name="remoteIp">remoteip The end user's ip address.</param>
        /// <returns></returns>
        protected virtual async Task<RecaptchaVerifyResponse> VerifyAsync(string response, string remoteIp)
        {
            HttpResponseMessage message;

            using (var request = new HttpClient())
            {
                //request.BaseAddress = SiteVerify;
                request.DefaultRequestHeaders.Accept.Clear();
                request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(DefaultMediaType));
                
                message = await request.PostAsJsonAsync(SiteVerify, new RecaptchaVerifyRequest
                {
                    SecretKey = SecretKey,
                    Response = response,
                    RemoteIp = remoteIp
                });
            }

            return await message.EnsureSuccessStatusCode().Content.ReadAsAsync<RecaptchaVerifyResponse>();
        }
    }
}
