﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Services
{
    public class SmsService : IIdentityMessageService
    {
        async Task IIdentityMessageService.SendAsync(IdentityMessage message)
        {
            var service = ServicesContainer.Default.GetInstance<ISmsService>();

            if (service == null) throw new NotImplementedException();

            await service.SendAsync(message.Body, message.Destination);
        }
    }
}
