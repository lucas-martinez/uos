﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Services
{
    using Assertion;

    public interface ISmsService
    {
        IResult Send(string message);

        IResult Send(string message, string recipient);

        Task<IResult> SendAsync(string message);

        Task<IResult> SendAsync(string message, string recipient);

        IResult Send(string message, string recipient, string origin);

        Task<IResult> SendAsync(string message, string recipient, string origin);
    }
}
