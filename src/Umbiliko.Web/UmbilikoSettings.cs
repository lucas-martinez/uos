﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web
{
    public class UmbilikoSettings
    {
        const string Prefix = "umbiliko:";
        static readonly IDictionary<string, string> Defaults;
        static readonly int _refresh = 1000;

        static UmbilikoSettings()
        {
            Defaults = new Dictionary<string, string>
            {
                { "", "" }
            };
        }

        IDictionary<string, string> _config;
        DateTime _configUtc;
        readonly IDictionary<string, string> _customs;

        public UmbilikoSettings()
        {
            _customs = new Dictionary<string, string>();
        }

        public string this[string name]
        {
            get
            {
                string value;

                if (_customs.TryGetValue(name, out value)) return value;

                if (_config == null || DateTime.UtcNow < (_configUtc + TimeSpan.FromMilliseconds(_refresh)))
                {
                    var settings = ConfigurationManager.AppSettings;

                    _config = settings
                        .AllKeys
                        .Where(k => k.StartsWith(Prefix))
                        .ToDictionary(k => k, k => settings[k].Substring(Prefix.Length));

                    _configUtc = DateTime.UtcNow;
                }

                if (_config.TryGetValue(name, out value)) return value;

                if (Defaults.TryGetValue(name, out value)) return value;

                return null;
            }
            set
            {
                _customs.ContainsKey(name);
            }
        }
    }
}
