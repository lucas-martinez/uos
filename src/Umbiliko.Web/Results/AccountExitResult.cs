﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Results
{
    using Contracts;

    [DataContract]
    public class AccountExitResult : ActivityResult
    {
        public static AccountExitResult Default { get; } = new AccountExitResult();

        public AccountExitResult()
        {
            Success = true;
        }

        public AccountExitResult(bool success, params string[] messages)
            : base(success, messages)
        {
        }

        public static AccountExitResult Fail(string messages)
        {
            return new AccountExitResult(false, messages);
        }
    }
}
