﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Uos.Web.Results
{
    using Assertion;
    using Constants;

    [DataContract]
    public class ActivityResult : ActionResult, IResult
    {
        IList<string> _messages;

        public ActivityResult()
        {
            Success = true;
        }

        protected ActivityResult(bool success, IList<string> messages)
        {
            Messages = messages;
            Success = success;
        }

        public ActivityResult(params string[] messages)
        {
            Success = false;
            Messages = new List<string>(messages);
        }

        [DataMember(Name = "messages", IsRequired = true, Order = 2)]
        public IList<string> Messages
        {
            get { return _messages ?? (_messages = new List<string>()); }
            set { _messages = value; }
        }

        [DataMember(Name = "returnUrl", IsRequired = false, Order = 1)]
        public string ReturnUrl { get; set; }

        [DataMember(Name = "success", IsRequired = true, Order = 0)]
        public bool Success { get; set; }

        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            HttpResponseBase response = context.HttpContext.Response;
            response.ContentType = "application/json";

            var scriptSerializer = JsonSerializer.Create(Defaults.JsonSerializerSettings);

            using (var sw = new StringWriter())
            {
                scriptSerializer.Serialize(sw, this);
                response.Write(sw.ToString());
            }
        }
    }

    [DataContract]
    public class ActivityResult<TValue> : ActivityResult
    {
        public ActivityResult()
            : base()
        {
        }

        protected ActivityResult(bool success, TValue value, IList<string> messages)
            : base(success, messages)
        {
            Value = value;
        }

        protected ActivityResult(bool success, TValue value, params string[] messages)
            : base(success, messages)
        {
            Value = value;
        }

        [DataMember(Name = "data", IsRequired = false, Order = 3)]
        public TValue Value { get; set; }

        public static ActivityResult<TValue> Assert(TValue value)
        {
            return Equals(value, default(TValue)) ? new ActivityResult<TValue>(true, value) : new ActivityResult<TValue>(false, default(TValue), "NotFound");
        }
    }
}
