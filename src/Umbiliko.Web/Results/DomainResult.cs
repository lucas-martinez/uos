﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Results
{
    using Contracts;
    using Results;

    [DataContract]
    public sealed class DomainResult : ActivityResult<DomainInfo>
    {
        public DomainResult()
        {
        }

        private DomainResult(bool success, DomainInfo domain, IList<string> messages)
            : base(success, domain, messages)
        {
        }

        private DomainResult(bool success, DomainInfo domain, params string[] messages)
            : base(success, domain, messages)
        {
        }

        public static DomainResult Assert(DomainInfo domain)
        {
            return domain != null ? new DomainResult(true, domain) : new DomainResult(false, null, "NotFound");
        }

        public static DomainResult Fail(params string[] messages)
        {
            return new DomainResult(false, null, messages);
        }

        public static DomainResult Fail(IEnumerable<string> messages)
        {
            return new DomainResult(false, null, new List<string>(messages));
        }
    }
}
