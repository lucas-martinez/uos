﻿using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Results
{
    using Contracts;
    using Results;

    [DataContract]
    public sealed class UserResult : ActivityResult<UserInfo>
    {
        public UserResult()
        {
        }

        private UserResult(bool success, UserInfo data, IList<string> messages)
            : base(success, data, messages)
        {
        }

        private UserResult(bool success, UserInfo data, params string[] messages)
            : base(success, data, messages)
        {
        }

        public static UserResult Assert(UserInfo user)
        {
            return user != null ? new UserResult(true, user) : new UserResult(false, null, "NotFound");
        }

        public static UserResult Fail(params string[] messages)
        {
            return new UserResult(false, null, messages);
        }

        public static UserResult Fail(IEnumerable<string> messages)
        {
            return new UserResult(false, null, new List<string>(messages));
        }

        public static implicit operator UserResult(UserInfo value)
        {
            return new UserResult(true, value);
        }

        public static implicit operator UserResult(Exception ex)
        {
            return new UserResult(false, null, ex.InnerMessage());
        }
    }
}
