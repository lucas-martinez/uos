﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Umbiliko.Web.Results
{
    using Kendo.Mvc;
    using System;
    using System.Globalization;

    public class AggregateResult
    {
        private object _aggregateValue;
        private readonly AggregateFunction function;
        private int itemCount;

        //
        // Summary:
        //     Initializes a new instance of the Kendo.Mvc.Infrastructure.AggregateResult class.
        //
        // Parameters:
        //   function:
        //     Kendo.Mvc.AggregateFunction that generated the result.
        //
        // Exceptions:
        //   T:System.ArgumentNullException:
        //     function is null.
        public AggregateResult(AggregateFunction function)
                : this(null, function)
        {
        }

        //
        // Summary:
        //     Initializes a new instance of the Kendo.Mvc.Infrastructure.AggregateResult class.
        //
        // Parameters:
        //   value:
        //     The value of the result.
        //
        //   function:
        //     Kendo.Mvc.AggregateFunction that generated the result.
        public AggregateResult(object value, AggregateFunction function)
            : this(value, 0, function)
        {
        }

        //
        // Summary:
        //     Initializes a new instance of the Kendo.Mvc.Infrastructure.AggregateResult class.
        //
        // Parameters:
        //   value:
        //     The value of the result.
        //
        //   count:
        //     The number of arguments used for the calculation of the result.
        //
        //   function:
        //     Function that generated the result.
        //
        // Exceptions:
        //   T:System.ArgumentNullException:
        //     function is null.
        public AggregateResult(object value, int count, AggregateFunction function)
        {
            if (function == null)
            {
                throw new ArgumentNullException("function");
            }
            this._aggregateValue = value;
            this.itemCount = count;
            this.function = function;
        }

        public string Format(string format)
        {
            if (this.Value != null)
            {
                return string.Format(format, this.Value);
            }
            return this.ToString();
        }

        //
        // Summary:
        //     Returns a System.String that represents the current System.Object.
        //
        // Returns:
        //     A System.String that represents the current System.Object.
        public override string ToString()
        {
            if (Value != null)
            {
                return this.Value.ToString();
            }
            return base.ToString();
        }

        public string AggregateMethodName
        {
            get
            {
                return this.function.AggregateMethodName;
            }
        }

        /// <summary>
        /// Gets or sets the text which serves as a caption for the result in a user interface..
        /// </summary>
        public string Caption
        {
            get
            {
                return this.function.Caption;
            }
        }

        /// <summary>
        /// Gets the formatted value of the result.
        /// </summary>
        public object FormattedValue
        {
            get
            {
                if (string.IsNullOrEmpty(this.function.ResultFormatString))
                {
                    return this._aggregateValue;
                }
                return string.Format(CultureInfo.CurrentCulture, this.function.ResultFormatString, new object[] { this._aggregateValue });
            }
        }

        /// <summary>
        /// Gets the name of the function.
        /// </summary>
        public string FunctionName
        {
            get
            {
                return this.function.FunctionName;
            }
        }

        /// <summary>
        /// Gets or sets the number of arguments used for the calulation of the result.
        /// </summary>
        public int ItemCount
        {
            get
            {
                return this.itemCount;
            }
            set
            {
                this.itemCount = value;
            }
        }

        public string Member
        {
            get
            {
                return this.function.SourceField;
            }
        }

        /// <summary>
        /// Gets or sets the value of the result.
        /// </summary>
        public object Value
        {
            get { return _aggregateValue; }
            internal set { _aggregateValue = value; }
        }
    }
}