﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Umbiliko.Web.Results
{
    namespace Kendo.Mvc
    {
        using System;
        using System.Collections.Generic;
        using System.Globalization;
        using System.Linq.Expressions;
        using System.Runtime.CompilerServices;

        public abstract class AggregateFunction : JObject
        {
            private string functionName;

            protected AggregateFunction()
            {
            }

            public abstract Expression CreateAggregateExpression(Expression enumerableExpression, bool liftMemberAccessToNull);
            protected virtual string GenerateFunctionName()
            {
                return string.Format(CultureInfo.InvariantCulture, "{0}_{1}", new object[] { base.GetType().Name, this.GetHashCode() });
            }

            protected override void Serialize(IDictionary<string, object> json)
            {
                json["field"] = this.SourceField;
                json["aggregate"] = this.FunctionName.Split(new char[] { '_' })[0].ToLowerInvariant();
            }

            public abstract string AggregateMethodName { get; }

            public string Caption { get; set; }

            public virtual string FunctionName
            {
                get
                {
                    if (string.IsNullOrEmpty(this.functionName))
                    {
                        this.functionName = this.GenerateFunctionName();
                    }
                    return this.functionName;
                }
                set
                {
                    this.functionName = value;
                }
            }

            public Type MemberType { get; set; }

            public virtual string ResultFormatString { get; set; }

            public virtual string SourceField { get; set; }
        }
    }

}
