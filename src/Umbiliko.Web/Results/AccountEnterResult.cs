﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Results
{
    using Assertion;
    using Contracts;
    using Models;
    using Results;

    [DataContract]
    public class AccountEnterResult : ActivityResult
    {
        [DataMember(Name = "name")]
        public UserInfo Data { get; set; }

        public static AccountEnterResult Fail(AccountEnterRequest request, params string[] messages)
        {
            return new AccountEnterResult
            {
                Messages = messages.ToList(),
                Success = false
            };
        }

        public static AccountEnterResult Fail(AccountEnterRequest request, IEnumerable<string> messages)
        {
            return new AccountEnterResult
            {
                Messages = messages.ToList(),
                Success = false
            };
        }

        public static AccountEnterResult Fail(AccountEnterRequest request, IResult innerResult)
        {
            return new AccountEnterResult
            {
                Messages = innerResult.Messages,
                Success = false
            };
        }

        public static AccountEnterResult Assert(AccountEnterRequest request, UserResult result)
        {
            return new AccountEnterResult
            {
                Data = result.Value,
                Messages = result.Messages,
                ReturnUrl = result.ReturnUrl,
                Success = result.Success
            };
        }
    }
}
