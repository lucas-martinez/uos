﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Results
{
    using Contracts;
    using Binders;

    [DataContract, DataContractBinder]
    public class AccountClaimResult : ActivityResult
    {
    }
}
