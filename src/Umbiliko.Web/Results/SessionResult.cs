﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Uos.Web.Results
{
    using Contracts;

    public sealed class SessionResult : ActivityResult<SessionInfo>
    {
        public SessionResult()
        {
        }

        private SessionResult(bool success, SessionInfo session, IList<string> messages)
            : base(success, session, messages)
        {
        }

        private SessionResult(bool success, SessionInfo session, params string[] messages)
            : base(success, session, messages)
        {
        }

        public static SessionResult Assert(SessionInfo session)
        {
            return session != null ? new SessionResult(true, session) : new SessionResult(false, null, "NotFound");
        }

        public static SessionResult Fail(params string[] messages)
        {
            return new SessionResult(false, null, messages);
        }

        public static SessionResult Fail(SessionInfo value, IList<string> messages)
        {
            return new SessionResult(false, value, messages);
        }

        public static SessionResult Fail(SessionInfo session, params string[] messages)
        {
            return new SessionResult(false, session, messages.ToArray());
        }

        public static SessionResult Fail(IEnumerable<string> messages)
        {
            return new SessionResult(false, null, messages.ToArray());
        }

        public static implicit operator SessionResult(SessionInfo value)
        {
            return new SessionResult(true, value);
        }

        public static implicit operator SessionResult(Exception ex)
        {
            return new SessionResult(false, null, ex.InnerMessage());
        }
    }
}
