﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Contracts
{

    public class ApplicationUser : IUser<string>
    {
        readonly SessionInfo _session;
        readonly UserInfo _user;

        public ApplicationUser()
        {
            _user = new UserInfo();
        }

        public ApplicationUser(SessionInfo session)
        {
            _session = session;
        }

        public AccountInfo Account
        {
            get { return _session != null ? _session.Account : null; }
        }
        
        public UserInfo User
        {
            get { return _user ?? _session.User; }
        }

        string IUser<string>.Id
        {
            get { return _session != null ? Convert.ToString(_session.Id) : null; }
        }

        string IUser<string>.UserName
        {
            get { return User.Name; }
            set { User.Name = value; }
        }
    }
}
