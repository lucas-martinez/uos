﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Contracts
{
    using Constants;

    public class PasswordValidator : IIdentityValidator<string>
    {
        PasswordStrength _requires;

        public PasswordValidator(PasswordStrength required)
        {
            _requires = required;
        }

        public virtual Task<IdentityResult> ValidateAsync(string password)
        {
            if (password == null)
            {
                throw new ArgumentNullException("password");
            }

            var has = PasswordStrength.FromPassword(password);

            var errors = new List<string>();

            if (_requires.Length == 0)
            {
            }
            else if (has.Length == 0)
            {
                if (_requires.Length == 0)
                {
                    errors.Add("Your password can not be empty.");
                }
                else
                {
                    errors.Add(string.Format(CultureInfo.CurrentCulture, "Your password can not be empty and requires a minimum of {0} characters.", _requires.Length));
                }
            }
            if (has.Length < _requires.Length)
            {
                errors.Add(string.Format(CultureInfo.CurrentCulture, "Your password is too short. It must have at least {0} characters."/*Resources.PasswordTooShort*/, _requires.Length));
            }

            if (_requires.Symbol && !has.Symbol)
            {
                errors.Add("Your password requires at least one non letter or digit."/*Resources.PasswordRequireNonLetterOrDigit*/);
            }

            if (_requires.Digit && !has.Digit)
            {
                errors.Add("Your password requires at least one digit."/*Resources.PasswordRequireDigit*/);
            }

            if (_requires.Lowercase && !has.Lowercase)
            {
                errors.Add("Your password requires at least one lowercase letter."/*Resources.PasswordRequireLower*/);
            }

            if (_requires.Uppercase && !has.Uppercase)
            {
                errors.Add("Your password requires at least one uppercase letter."/*Resources.PasswordRequireUpper*/);
            }

            if (errors.Count == 0)
            {
                return Task.FromResult(IdentityResult.Success);
            }

            return Task.FromResult(IdentityResult.Failed(string.Join(" ", errors)));
        }
    }
}
