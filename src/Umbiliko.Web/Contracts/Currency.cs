﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Contracts
{
    public class Currency
    {
        public virtual string Code { get; set; }

        public virtual string CurrencyName { get; set; }

        public virtual string CurrencySymbol { get; set; }
    }
}
