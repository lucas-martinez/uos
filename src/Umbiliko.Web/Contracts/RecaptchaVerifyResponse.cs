﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Contracts
{
    [DataContract]
    internal class RecaptchaVerifyResponse
    {
        [DataMember(Name = "success", IsRequired = true)]
        public bool Success { get; set; }
        
        [DataMember(Name = "error-codes", IsRequired = false)]
        public ICollection<string> ErrorCodes { get; set; }
    }
}
