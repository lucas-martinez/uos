﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Contracts
{
    using Constants;
    
    [DataContract]
    public class SessionInfo : IUser
    {
        UserInfo _user;

        public SessionInfo()
        {
        }

        public SessionInfo(IIdentity identity)
        {
            var claimsIdentity = identity as ClaimsIdentity;

            if (claimsIdentity != null)
            {
                claimsIdentity.Update(this);

                var user = new UserInfo();

                claimsIdentity.Update(user);

                _user = user;

                var account = new AccountInfo();

                claimsIdentity.Update(account);

                if (account.CompanyCode != null) Account = account;
            }
            else
            {
                Debug.Assert(false);
            }
        }

        public SessionInfo(IPrincipal principal)
            : this(identity: principal.Identity)
        {   
        }

        [IgnoreDataMember]
        public AccessLevels AccessLevel
        {
            get
            {
                AccessLevels level;

                if (User != null && User.Name != null)
                {
                    level = User.EmailConfirmed == true ? AccessLevels.Authenticated : AccessLevels.Alleged;

                    var issuers = User.Claims.Where(c => c.Type == ClaimTypes.Name).Select(c => c.Issuer).ToArray();

                    if (User.PasswordHash != null)
                    {
                        level |= AccessLevels.Protected;
                    }

                    if (Account != null && Account.CompanyCode != null && Account.SegmentCode != null)
                    {
                        level |= AccessLevels.Private;

                        if (Account.CompanyCode == Defaults.InternalCustomerCode && Account.SegmentCode == Defaults.InernalConsumerCode)
                        {
                            level |= AccessLevels.Internal;
                        }
                    }

                    if (User.TwoFactorConfirmed == true)
                    {
                        level |= AccessLevels.Secured;
                    }
                }
                else
                {
                    level = AccessLevels.Public;
                }

                foreach (var login in User.Logins)
                {
                    if (login.ExpiresUtc > DateTime.UtcNow)
                    {
                        AccessLevels value;

                        if (Enum.TryParse(login.LoginProvider, out value) && value == (value & AccessLevels.Social))
                        {
                            level |= value;
                        }
                    }
                }

                return level;
            }
        }

        [DataMember(Name = "accessToken")]
        public string AccessToken { get; set; }

        [DataMember(Name = "accessTokenSecret")]
        public string AccessTokenSecret { get; set; }

        [DataMember(Name = "account", Order = 5, IsRequired = false)]
        public AccountInfo Account { get; set; }

        [DataMember(Name = "expiresUtc")]
        public DateTime? ExpiresUtc { get; set; }

        [DataMember(Name = "id", Order = 0, IsRequired = true)]
        public Guid Id { get; set; }

        string IUser<string>.Id
        {
            get { return Convert.ToString(Id); }
        }

        [DataMember(Name = "ipAddress")]
        public string IpAddress { get; set; }

        [DataMember(Name = "issuedUtc")]
        public DateTime? IssuedUtc { get; set; }

        [DataMember(Name = "domainName")]
        public string DomainName { get; set; }

        [DataMember(Name = "fingetprint")]
        public string Fingerprint { get; set; }

        [DataMember(Name = "user", Order = 4, IsRequired = true)]
        public UserInfo User
        {
            get { return _user; }
            set { _user = value; }
        }

        string IUser<string>.UserName
        {
            get { return User.Name; }
            set { User.Name = value; }
        }

        public ClaimsIdentity Update(ClaimsIdentity identity)
        {
            if (Account != null)
            {
                Account.Update(identity);
            }

            if (AccessToken != null) identity.SetClaim(type: UmbilikoClaimTypes.AccessToken, value: AccessToken, issuer: Defaults.IdentityProvider);

            if (DomainName != null) identity.SetClaim(type: UmbilikoClaimTypes.DomainName, value: DomainName, issuer: Defaults.IdentityProvider);

            identity.SetClaim(type: MicrosoftClaimTypes.IdentityProvider, value: Defaults.IdentityProvider, issuer: Defaults.IdentityProvider);

            identity.SetClaim(type: ClaimTypes.NameIdentifier, input: Id, issuer: Defaults.IdentityProvider);

            identity.SetClaim(type: UmbilikoClaimTypes.SessionId, input: Id, issuer: Defaults.IdentityProvider);

            User.Update(identity);

            return identity;
        }

        public static implicit operator SessionInfo(UserInfo user)
        {
            return user == null ? null : new SessionInfo
            {
                User = user
            };
        }

        public static implicit operator ClaimsIdentity(SessionInfo session)
        {
            return session.Update(new ClaimsIdentity(DefaultAuthenticationTypes.ApplicationCookie));
        }

        public static implicit operator SessionInfo(ClaimsIdentity identity)
        {
            return identity.Update(new SessionInfo());
        }

        public static implicit operator SessionInfo(ClaimsPrincipal principal)
        {
            var identity = principal.Identities.FirstOrDefault(id => id.AuthenticationType == DefaultAuthenticationTypes.ApplicationCookie);

            if (identity != null) return identity.Update(new SessionInfo());

            return null;
        }

        public static implicit operator List<Claim>(SessionInfo session)
        {
            var identity = new ClaimsIdentity();

            session.Update(identity);

            return identity.Claims.ToList();
        }
    }
}
