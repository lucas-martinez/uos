﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Contracts
{
    using Constants;
    using System.Security.Claims;

    [DataContract]
    public class AccountInfo
    {
        ICollection<string> _roles;

        [DataMember(Name = "companyCode")]
        public string CompanyCode { get; set; }

        [DataMember(Name = "companyName")]
        public string CompanyName { get; set; }

        [DataMember(Name = "environment")]
        public string Environment { get; set; }

        [DataMember(Name = "expiresUtc")]
        public DateTime? ExpiresUtc { get; set; }


        [DataMember(Name = "roles")]
        public ICollection<string> Roles
        {
            get { return _roles ?? (_roles = new Collection<string>()); }
            set { _roles = value; }
        }

        [DataMember(Name = "segmentCode")]
        public string SegmentCode { get; set; }

        [DataMember(Name = "segmentDescription")]
        public string SegmentDescription { get; set; }

        public ClaimsIdentity Update(ClaimsIdentity identity)
        {
            if (CompanyCode != null) identity.SetClaim(type: UmbilikoClaimTypes.CompanyCode, value: CompanyCode, issuer: Defaults.IdentityProvider);

            if (CompanyName != null) identity.SetClaim(type: UmbilikoClaimTypes.CompanyName, value: CompanyName, issuer: Defaults.IdentityProvider);

            if (Roles != null)
            {
                identity.Clear(type: identity.RoleClaimType, issuer: Defaults.IdentityProvider);

                foreach (var role in Roles)
                {
                    identity.AddRole(role);
                }
            }

            if (SegmentCode != null) identity.SetClaim(type: UmbilikoClaimTypes.SegmentCode, value: SegmentCode, issuer: Defaults.IdentityProvider);

            if (SegmentDescription != null) identity.SetClaim(type: UmbilikoClaimTypes.SegmentDescription, value: SegmentCode, issuer: Defaults.IdentityProvider);
            
            return identity;
        }
    }
}