﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Contracts
{
    using Constants;
    using System.Runtime.Serialization;

    [DataContract]
    public class ConsumerInfo
    {
        [DataMember(Name = "companyCode", Order = 0)]
        public string CompanyCode { get; set; }

        [DataMember(Name = "accessLevel", Order = 2)]
        public AccessLevels? RequiredAccessLevel { get; set; }

        [DataMember(Name = "segmentCode", Order = 1)]
        public string SegmentCode { get; set; }

        [DataMember(Name = "subscriptions", Order = 3)]
        public ICollection<SubscriptionInfo> Subscriptions { get; set; }
    }
}
