﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Contracts
{
    public class CompleteAuthenticationRequest
    {
        public string CompanyCode { get; set; }

        public string PasswordHash { get; set; }

        public string SegmentCode { get; set; }

        public string SmsCode { get; set; }

        public string UserName { get; set; }
    }
}
