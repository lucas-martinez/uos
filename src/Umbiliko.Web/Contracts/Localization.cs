﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Contracts
{
    public class Localization
    {
        public int LocalizationId { get; set; }

        public string CultureName { get; set; }

        public string CountryCode { get; set; }

        public Guid? RegionId { get; set; }
    }
}
