﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Contracts
{
    [DataContract]
    public class ActivityDescription
    {
        /// <summary>
        /// Activity HTTP method
        /// </summary>
        [DataMember(Name = "methods", Order = 2)]
        public string AcceptMethods { get; set; }

        /// <summary>
        /// Activity action
        /// </summary>
        [DataMember(Name = "action", Order = 3)]
        public string Action { get; set; }

        /// <summary>
        /// Controller Area
        /// </summary>
        [DataMember(Name = "area", Order = 5)]
        public string Area { get; set; }

        /// <summary>
        /// Deciration attributes
        /// </summary>
        [DataMember(Name = "attributes", Order = 9)]
        public string Attributes { get; set; }

        /// <summary>
        /// Activity HTTP controller
        /// </summary>
        [DataMember(Name = "controller", Order = 4)]
        public string Controller { get; set; }

        /// <summary>
        /// Activity name
        /// </summary>
        [DataMember(Name = "name", Order = 0)]
        public string Name { get; set; }

        /// <summary>
        /// Result type
        /// </summary>
        [DataMember(Name = "result", Order = 8)]
        public string ResultType { get; set; }

        /// <summary>
        /// Activity pre-required roles
        /// </summary>
        [DataMember(Name = "roles", Order = 6)]
        public string Roles { get; set; }

        /// <summary>
        /// Activity relative path and parameters pattern
        /// </summary>
        [DataMember(Name = "route", Order = 7)]
        public string Route { get; set; }

        /// <summary>
        /// Sample call
        /// </summary>
        [DataMember(Name = "sample", Order = 11)]
        public string Sample { get; set; }

        /// <summary>
        /// Documentation summary
        /// </summary>
        [DataMember(Name = "summary", Order = 10)]
        public string Summary { get; set; }

        /// <summary>
        /// Activity type (Public, Protected or Internal)
        /// </summary>
        [DataMember(Name = "type", Order = 1)]
        public string Level { get; set; }
    }
}
