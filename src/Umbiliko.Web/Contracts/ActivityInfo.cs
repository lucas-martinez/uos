﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Contracts
{
    using Constants;
    
    [DataContract]
    public class ActivityInfo
    {
        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "accessLevel")]
        public AccessLevels AccessLevel { get; set; }

        [DataMember(Name = "roles")]
        public ICollection<string> Roles { get; set; }
    }
}
