﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Contracts
{
    public class Archive
    {
        public string Path { get; set; }

        public string Timestamp { get; set; }
    }
}
