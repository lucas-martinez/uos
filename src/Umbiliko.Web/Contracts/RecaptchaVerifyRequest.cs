﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Contracts
{
    [DataContract]
    internal class RecaptchaVerifyRequest
    {
        [DataMember(Name = "secret", IsRequired = true)]
        public string SecretKey { get; set; }

        [DataMember(Name = "response", IsRequired = true)]
        public string Response { get; set; }

        [DataMember(Name = "remoteip", IsRequired = false)]
        public string RemoteIp { get; set; }
    }
}
