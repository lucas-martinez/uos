﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Umbiliko.Web.Contracts
{
    public enum ClaimCategories : byte
    {
        EmailAddress = 1,

        Name = 2,

        Personal = 4,

        PhoneNumber = 3,

        Role = 4
    }
}
