﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Contracts
{
    using Constants;
    
    [DataContract]
    public class ClaimInfo
    {
        [DataMember(Name = "issuer")]
        public string Issuer { get; set; }

        [DataMember(Name = "provider")]
        public string OriginalIssuer { get; set; }

        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "value")]
        public string Value { get; set; }

        [DataMember(Name = "valueType")]
        public string ValueType { get; set; }
        
        public static implicit operator ClaimInfo(Claim claim)
        {
            return new ClaimInfo
            {
                Issuer = claim.Issuer,
                OriginalIssuer = claim.OriginalIssuer,
                Type = claim.Type,
                Value = claim.Value,
                ValueType = claim.ValueType
            };
        }

        public static implicit operator Claim(ClaimInfo claim)
        {
            return new Claim(claim.Type, claim.Value, claim.ValueType, claim.Issuer, claim.OriginalIssuer);
        }
    }
}