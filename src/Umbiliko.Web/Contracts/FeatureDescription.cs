﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Contracts
{
    [DataContract]
    public class FeatureDescription
    {
        ICollection<ActivityDescription> _activities;

        [DataMember(Name = "activities", Order = 1)]
        public ICollection<ActivityDescription> Activities
        {
            get { return _activities ?? (_activities = new Collection<ActivityDescription>()); }
            set { _activities = value; }
        }

        [DataMember(Name = "name", Order = 0)]
        public string Name { get; set; }
    }
}
