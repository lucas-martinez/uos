﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Contracts
{
    public class Country
    {
        public virtual string CountryCode { get; set; }

        public virtual string CountryName { get; set; }

        public virtual string CountryDivisionType { get; set; }
    }
}
