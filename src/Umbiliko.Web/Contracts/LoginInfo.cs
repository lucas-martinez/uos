﻿using Newtonsoft.Json;
using System;
using System.Runtime.Serialization;

namespace Uos.Web.Contracts
{
    [DataContract]
    public class LoginInfo
    {
        [DataMember(Name = "accessToken"), JsonProperty("accessToken")]
        public string AccessToken { get; set; }

        [DataMember(Name = "expiresUtc"), JsonProperty("expiresUtc")]
        public DateTime? ExpiresUtc { get; set; }

        [DataMember(Name = "label"), JsonProperty("label")]
        public string Label { get; set; }

        [DataMember(Name = "loginConfirmed"), JsonProperty("loginConfirmed")]
        public bool LoginConfirmed { get; set; }

        [DataMember(Name = "loginCount"), JsonProperty("loginCount")]
        public int? LoginCount { get; set; }

        [DataMember(Name = "loginKey"), JsonProperty("loginKey")]
        public string LoginKey { get; set; }

        [DataMember(Name = "loginProvider"), JsonProperty("loginProvider")]
        public string LoginProvider { get; set; }

        [DataMember(Name = "loginUtc"), JsonProperty("loginUtc")]
        public DateTime LoginUtc { get; set; }
    }
}
