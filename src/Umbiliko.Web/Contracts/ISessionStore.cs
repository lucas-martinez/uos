﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Contracts
{
    using Results;

    public interface ISessionStore
    {
        IUserStore UserStore { get; }

        Task<SessionResult> CreateSessionAsync(string userNameOrEmailAddress, string userPassword, string ipAddress, string fingerprint, string domainName);

        Task<SessionResult> CreateSessionAsync(LoginInfo login, string ipAddress, string fingerprint, string domainName);

        Task<SessionResult> CreateSessionAsync(string userToken, string ipAddress, string fingerprint, string domainName);

        Task<SessionResult> CreateSessionAsync(Guid sessionId, string ipAddress, string fingerprint, string domainName, string companyCode, string segmentCode, bool expirePrevious);

        Task<SessionResult> CreateUserAsync(SessionInfo model);

        Task<SessionResult> ExpireSessionAsync(Guid sessionId, string ipAddress, string fingerprint, string domainName);

        Task<SessionResult> ExtendSessionAsync(Guid sessionId, string ipAddress, string fingerprint, string domainName, TimeSpan time);

        Task<SessionResult> FindSessionByIdAsync(Guid sessionId);

        Task<SessionResult> FindSessionByTokenAsync(string accessToken);
    }
}
