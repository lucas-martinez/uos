﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Contracts
{
    [DataContract]
    public class FeatureInfo
    {
        ICollection<ActivityInfo> _activities;

        [DataMember(Name = "activities", Order = 1)]
        public ICollection<ActivityInfo> Activities
        {
            get { return _activities ?? (_activities = new Collection<ActivityInfo>()); }
            set { _activities = value; }
        }

        [DataMember(Name = "name", Order = 0)]
        public string Name { get; set; }
    }
}
