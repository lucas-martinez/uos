﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Contracts
{
    [DataContract]
    public class Account
    {
        ICollection<string> _roles;

        /// <summary>
        /// Groups
        /// </summary>
        [DataMember]
        public ICollection<string> Roles
        {
            get { return _roles ?? (_roles = new Collection<string>()); }
            set { _roles = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public string Email { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public string EmailConfirmed { get; set; }

        /// <summary>
        /// IP address of last login
        /// </summary>
        [DataMember]
        public string LogingAddress { get; set; }

        /// <summary>
        /// Provider name of last login
        /// </summary>
        [DataMember]
        public string LogingProvider { get; set; }

        /// <summary>
        ///  DateTime in UTC of last login
        /// </summary>
        [DataMember]
        public DateTime? LogingUtc { get; set; }

        /// <summary>
        ///  DateTime in UTC of last logout
        /// </summary>
        [DataMember]
        public DateTime? LogoutUtc { get; set; }
        
        /// <summary>
        /// DateTime in UTC when this info expires.
        /// </summary>
        [DataMember]
        public DateTime ValidUtc { get; set; }
    }
}
