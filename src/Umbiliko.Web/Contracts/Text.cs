﻿using System.Collections.Generic;
using System.Globalization;
using System.Threading;

namespace Uos.Web.Contracts
{
    public class Text
    {
        public static string DefaultCulture { get; set; } = "";

        public static implicit operator Text(string s)
        {
            return new Text(s);
        }

        public static implicit operator string (Text text)
        {
            return text != null ? text.ToString() : null;
        }

        string _name;
        Dictionary<string, string> _strings;

        public Text()
        {
        }

        public Text(string name)
            : this()
        {
            _name = name;
        }

        public Text(string name, string value)
            : this(name, value, Thread.CurrentThread.CurrentCulture)
        {
        }

        public Text(string name, string value, CultureInfo culture)
            : this(name)
        {
            _strings = new Dictionary<string, string> { { culture.Name, value } };
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public Dictionary<string, string> Strings
        {
            get { return _strings ?? (_strings = new Dictionary<string, string>(1)); }
            set { _strings = value; }
        }

        public string GetString(CultureInfo culture)
        {
            string result;

            if (_strings.TryGetValue(culture.Name, out result)) return result;

            if (!culture.IsNeutralCulture && _strings.TryGetValue(culture.Parent.Name, out result)) return result;

            if (_strings.TryGetValue(DefaultCulture, out result)) return result;

            return Name;
        }

        public override string ToString()
        {
            return GetString(Thread.CurrentThread.CurrentCulture);
        }
    }
}
