﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Contracts
{
    public class Region
    {
        public virtual Country Country { get; set; }

        public virtual string CountryCode { get; set; }

        public virtual ICollection<Region> RegionChildren { get; set; }

        public virtual string RegionDivisionType { get; set; }

        public virtual Guid RegionId { get; set; } = Guid.NewGuid();

        public virtual string RegionName { get; set; }
        
        public virtual Region RegionParent { get; set; }

        public virtual Guid RegionParentId { get; set; }

        public virtual string RegionType { get; set; }
    }
}
