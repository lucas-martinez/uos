﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Contracts
{
    public class Instance
    {
        ICollection<FeatureInfo> _features;

        [DataMember]
        public ICollection<FeatureInfo> Features
        {
            get { return _features ?? (_features = new Collection<FeatureInfo>()); }
            set { _features = value; }
        }

        public Guid? Identity { get; set; }

        public string MachineName { get; set; }

        public int ProcessId { get; set; }

        public string ProductName { get; set; }

        public string ProductVersion { get; set; }
    }
}
