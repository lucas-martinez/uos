﻿using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.Security.Claims;
using System.Security.Principal;

namespace Uos.Web.Contracts
{
    using Constants;
    
    [DataContract]
    public class UserInfo : IUser<string>
    {
        ICollection<AccountInfo> _accounts;
        ICollection<ClaimInfo> _claims;
        List<string> _errors;
        JsonSerializerSettings _jsonSerializerSettings;
        ICollection<LoginInfo> _logins;

        public UserInfo()
        {
        }

        public UserInfo(IIdentity identity)
        {
            var claimsIdentity = identity as ClaimsIdentity;

            if (claimsIdentity != null)
            {
                claimsIdentity.Update(this);
            }
            else
            {
                Debug.Assert(false);
            }
        }

        public UserInfo(IPrincipal principal)
            : this(identity: principal.Identity)
        {
        }
        
        [DataMember(Name = "accessFailCount")]
        public int? AccessFailedCount { get; set; }
        
        [DataMember(Name = "accounts")]
        public ICollection<AccountInfo> Accounts
        {
            get { return _accounts ?? (_accounts = new Collection<AccountInfo>()); }
            set { _accounts = value; }
        }

        [DataMember(Name = "claims")]
        public ICollection<ClaimInfo> Claims
        {
            get { return _claims ?? (_claims = new Collection<ClaimInfo>()); }
            set { _claims = value; }
        }

        [DataMember(Name = "displayName")]
        public string DisplayName { get; set; }

        [DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "emailConfirmed")]
        public bool? EmailConfirmed { get; set; }

        [DataMember(Name = "environment")]
        public string Environment { get; set; }

        [DataMember(Name = "lockoutEnabled")]
        public bool? LockoutEnabled { get; set; }

        [DataMember(Name = "lockoutEndDate")]
        public DateTime? LockoutEndDateUtc { get; set; }

        [DataMember(Name = "logins")]
        public ICollection<LoginInfo> Logins
        {
            get { return _logins ?? (_logins = new Collection<LoginInfo>()); }
            set { _logins = value; }
        }
        
        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "passwordHash")]
        public string PasswordHash { get; set; }

        [DataMember(Name = "phoneNumber")]
        public string PhoneNumber { get; set; }

        [DataMember(Name = "phoneNumberConfirmed")]
        public bool? PhoneNumberConfirmed { get; set; }

        [DataMember(Name = "picture")]
        public string Picture { get; set; }

        [DataMember(Name = "profile")]
        public string Profile { get; set; }
        
        [DataMember(Name = "screenName")]
        public string ScreenName { get; set; }

        [DataMember(Name = "securityStamp")]
        public string SecurityStamp { get; set; }

        [DataMember(Name = "twoFactorEnabled")]
        public bool? TwoFactorEnabled { get; set; }

        [DataMember(Name = "twoFactorCode")]
        public string TwoFactorCode { get; set; }

        [DataMember(Name = "twoFactorConfirmed")]
        public bool? TwoFactorConfirmed { get; set; }

        [DataMember(Name = "twoFactorInstant")]
        public DateTime? TwoFactorInstant { get; set; }

        string IUser<string>.Id
        {
            get { return Convert.ToString(Name); }
        }

        string IUser<string>.UserName
        {
            get { return Name; }
            set { Name = value; }
        }

        #region - claim based properties -

        [JsonIgnore, IgnoreDataMember]
        public DateTime? DateOfBirth
        {
            get { return GetDateTimeClaim(type: ClaimTypes.DateOfBirth, issuer: Defaults.IdentityProvider) ?? GetDateTimeClaim(type: ClaimTypes.DateOfBirth); }
            internal set { SetClaim(type: ClaimTypes.DateOfBirth, input: value, issuer: Defaults.IdentityProvider); }
        }

        [JsonIgnore, IgnoreDataMember]
        public string Country
        {
            get { return GetClaim(type: ClaimTypes.Country, issuer: Defaults.IdentityProvider) ?? GetClaim(type: ClaimTypes.Country); }
            internal set { SetClaim(type: ClaimTypes.Country, value: value, issuer: Defaults.IdentityProvider); }
        }

        [JsonIgnore, IgnoreDataMember]
        public List<string> Errors
        {
            get { return _errors ?? (_errors = new List<string>()); }
        }

        [JsonIgnore, IgnoreDataMember]
        public string Gender
        {
            get { return GetClaim(type: ClaimTypes.Gender, issuer: Defaults.IdentityProvider) ?? GetClaim(type: ClaimTypes.Gender); }
            internal set { SetClaim(ClaimTypes.Gender, value: value, issuer: Defaults.IdentityProvider); }
        }

        [JsonIgnore, IgnoreDataMember]
        public string GivenName
        {
            get { return GetClaim(type: ClaimTypes.GivenName, issuer: Defaults.IdentityProvider) ?? GetClaim(type: ClaimTypes.GivenName); }
            internal set { SetClaim(ClaimTypes.GivenName, value: value, issuer: Defaults.IdentityProvider); }
        }

        [JsonIgnore, IgnoreDataMember]
        public string Hash
        {
            get { return GetClaim(ClaimTypes.Hash, Defaults.IdentityProvider) ?? GetClaim(ClaimTypes.Hash); }
            internal set { SetClaim(ClaimTypes.Hash, value: value, issuer: Defaults.IdentityProvider); }
        }

        [JsonIgnore, IgnoreDataMember]
        public string HomePhone
        {
            get { return GetClaim(type: ClaimTypes.HomePhone, issuer: Defaults.IdentityProvider) ?? GetClaim(type: ClaimTypes.HomePhone); }
            internal set { SetClaim(ClaimTypes.HomePhone, value: value, issuer: Defaults.IdentityProvider); }
        }

        [JsonIgnore, IgnoreDataMember]
        internal JsonSerializerSettings JsonSerializerSettings
        {
            get
            {
                return _jsonSerializerSettings ?? (_jsonSerializerSettings = new JsonSerializerSettings
                {
                    DateFormatString = Defaults.ShortDateTimeFormat,
                    Error = new EventHandler<Newtonsoft.Json.Serialization.ErrorEventArgs>((currentObject, args) =>
                    {
                        Errors.Add(string.Format("{0} {1}. {2}", args.CurrentObject.GetType(), args.ErrorContext.Path, args.ErrorContext.Error.Message));

                        args.ErrorContext.Handled = true;
                    }),
                    
                });
            }
        }

        [JsonIgnore, IgnoreDataMember]
        public string NameIdentifier
        {
            get { return GetClaim(ClaimTypes.NameIdentifier, Defaults.IdentityProvider) ?? GetClaim(ClaimTypes.NameIdentifier); }
            internal set { SetClaim(ClaimTypes.NameIdentifier, value: value, issuer: Defaults.IdentityProvider); }
        }

        [JsonIgnore, IgnoreDataMember]
        public string MobilePhone
        {
            get { return GetClaim(ClaimTypes.MobilePhone, Defaults.IdentityProvider) ?? GetClaim(ClaimTypes.MobilePhone); }
            internal set { SetClaim(ClaimTypes.MobilePhone, value: value, issuer: Defaults.IdentityProvider); }
        }

        [JsonIgnore, IgnoreDataMember]
        public string OtherPhone
        {
            get { return GetClaim(ClaimTypes.OtherPhone, Defaults.IdentityProvider) ?? GetClaim(ClaimTypes.OtherPhone); }
            internal set { SetClaim(ClaimTypes.OtherPhone, value: value, issuer: Defaults.IdentityProvider); }
        }

        [JsonIgnore, IgnoreDataMember]
        public string PostalCode
        {
            get { return GetClaim(ClaimTypes.PostalCode, Defaults.IdentityProvider) ?? GetClaim(ClaimTypes.PostalCode); }
            internal set { SetClaim(ClaimTypes.PostalCode, value: value, issuer: Defaults.IdentityProvider); }
        }
        
        [JsonIgnore, IgnoreDataMember]
        public string PrimaryGroupSid
        {
            get { return GetClaim(ClaimTypes.PrimaryGroupSid, Defaults.IdentityProvider) ?? GetClaim(ClaimTypes.PrimaryGroupSid); }
            internal set { SetClaim(ClaimTypes.PrimaryGroupSid, value: value, issuer: Defaults.IdentityProvider); }
        }

        [JsonIgnore, IgnoreDataMember]
        public string PrimarySid
        {
            get { return GetClaim(ClaimTypes.PrimarySid, Defaults.IdentityProvider) ?? GetClaim(ClaimTypes.PrimarySid); }
            internal set { SetClaim(ClaimTypes.PrimarySid, value: value, issuer: Defaults.IdentityProvider); }
        }

        [JsonIgnore, IgnoreDataMember]
        public string Sid
        {
            get { return GetClaim(ClaimTypes.Sid, Defaults.IdentityProvider) ?? GetClaim(ClaimTypes.Sid); }
            internal set { SetClaim(ClaimTypes.Sid, value: value, issuer: Defaults.IdentityProvider); }
        }

        [JsonIgnore, IgnoreDataMember]
        public string StateOrProvince
        {
            get { return GetClaim(ClaimTypes.StateOrProvince, Defaults.IdentityProvider) ?? GetClaim(ClaimTypes.StateOrProvince); }
            internal set { SetClaim(ClaimTypes.StateOrProvince, value: value, issuer: Defaults.IdentityProvider); }
        }

        [JsonIgnore, IgnoreDataMember]
        public string StreetAddress
        {
            get { return GetClaim(ClaimTypes.StreetAddress, Defaults.IdentityProvider) ?? GetClaim(ClaimTypes.StreetAddress); }
            internal set { SetClaim(ClaimTypes.StreetAddress, value: value, issuer: Defaults.IdentityProvider); }
        }

        [JsonIgnore, IgnoreDataMember]
        public string Surname
        {
            get { return GetClaim(ClaimTypes.Surname, Defaults.IdentityProvider) ?? GetClaim(ClaimTypes.Surname); }
            internal set { SetClaim(ClaimTypes.Surname, value: value, issuer: Defaults.IdentityProvider); }
        }

        [JsonIgnore, IgnoreDataMember]
        public string Uri
        {
            get { return GetClaim(ClaimTypes.Uri, Defaults.IdentityProvider) ?? GetClaim(ClaimTypes.Uri); }
            internal set { SetClaim(ClaimTypes.Uri, value: value, issuer: Defaults.IdentityProvider); }
        }

        #endregion - claim based properties -

        #region - helper methods -

        public void AddAccountInfo(AccountInfo account)
        {
            foreach (var current in Accounts.Where(o => o.CompanyCode == account.CompanyCode && o.SegmentCode == o.SegmentCode).ToList())
            {
                Accounts.Remove(current);
            }

            Accounts.Add(account);
        }

        public ClaimInfo AddClaim(string type, string value, string valueType = null, string issuer = null)
        {
            if (valueType == null)
            {
                valueType = ClaimValueTypes.String;
            }

            return Claims.Add(type: type, value: value, valueType: valueType, issuer: issuer ?? Defaults.IdentityProvider);
        }

        public ClaimInfo AddClaimExact(Claim claim)
        {
            return AddClaimExact(type: claim.Type, value: claim.Value, valueType: claim.ValueType, issuer: claim.Issuer);
        }

        public ClaimInfo AddClaimExact(string type, string value, string valueType = null, string issuer = null)
        {
            var claim = FindFirst(c => c.Type == type && c.Value == value && c.Issuer == issuer);

            if (claim == null)
            {
                claim = AddClaim(type: type, value: value, valueType: valueType, issuer: issuer);
            }
            else if (claim.ValueType != valueType && valueType != null && valueType != ClaimValueTypes.String)
            {
                RemoveClaim(claim);

                claim = AddClaim(type: type, value: value, valueType: valueType, issuer: issuer);
            }

            return claim;
        }

        public ClaimInfo AddClaimExact(string type, DateTime? input, string issuer = null)
        {
            if (!input.HasValue) return null;

            var value = input.Value.ToString(Defaults.ShortDateTimeFormat);

            var valueType = ClaimValueTypes.DateTime;

            var claim = FindFirst(c => c.Type == type && c.Value == value && c.Issuer == issuer);

            if (claim == null)
            {
                claim = AddClaim(type: type, value: value, valueType: valueType, issuer: issuer);
            }
            else if (claim.ValueType != valueType && valueType != null && valueType != ClaimValueTypes.String)
            {
                RemoveClaim(claim);

                claim = AddClaim(type: type, value: value, valueType: valueType, issuer: issuer);
            }

            return claim;
        }

        public ClaimInfo AddClaimExact(string type, int? input, string issuer = null)
        {
            if (!input.HasValue) return null;

            var value = Convert.ToString(input.Value);

            var valueType = ClaimValueTypes.Integer;

            var claim = FindFirst(c => c.Type == type && c.Value == value && c.Issuer == issuer);

            if (claim == null)
            {
                claim = AddClaim(type: type, value: value, valueType: valueType, issuer: issuer);
            }
            else if (claim.ValueType != valueType && valueType != null && valueType != ClaimValueTypes.String)
            {
                RemoveClaim(claim);

                claim = AddClaim(type: type, value: value, valueType: valueType, issuer: issuer);
            }

            return claim;
        }

        public ClaimInfo AddClaimExact(string type, bool? input, string issuer = null)
        {
            if (!input.HasValue) return null;

            var value = input.Value == true ? bool.TrueString : bool.FalseString;

            var valueType = ClaimValueTypes.Boolean;

            var claim = FindFirst(c => c.Type == type && c.Value == value && c.Issuer == issuer);

            if (claim == null)
            {
                claim = AddClaim(type: type, value: value, valueType: valueType, issuer: issuer);
            }
            else if (claim.ValueType != valueType && valueType != null && valueType != ClaimValueTypes.String)
            {
                RemoveClaim(claim);

                claim = AddClaim(type: type, value: value, valueType: valueType, issuer: issuer);
            }

            return claim;
        }

        public ClaimInfo AddClaimUnique(string type, string value, string valueType = null, string issuer = null)
        {
            var claim = FindFirst(c => c.Type == type && c.Issuer == issuer);

            if (claim == null)
            {
                claim = AddClaim(type: type, value: value, valueType: valueType, issuer: issuer);
            }
            else if (claim.Value != value || (claim.ValueType != valueType && valueType != null && valueType != ClaimValueTypes.String))
            {
                RemoveClaim(claim);

                claim = AddClaim(type: type, value: value, valueType: valueType, issuer: issuer);
            }

            return claim;
        }

        public void AddLoginInfo(LoginInfo login)
        {
            RemoveLoginInfo(login.LoginProvider, login.LoginKey);

            Logins.Add(login);
        }

        public IEnumerable<ClaimInfo> FindAll(Expression<Func<ClaimInfo, bool>> predicate)
        {
            return Claims.AsQueryable().Where(predicate);
        }

        public IEnumerable<ClaimInfo> FindAll(string type, string value = null, string valueType = null, string issuer = null)
        {
            var source = Claims.Where(claim => claim.Type == type);

            if (value != null) source = source.Where(claim => claim.Value == value);

            if (valueType != null) source = source.Where(claim => claim.ValueType == valueType);

            if (issuer != null) source = source.Where(claim => claim.Issuer == issuer);

            return source;
        }

        public ClaimInfo FindFirst(Expression<Func<ClaimInfo, bool>> predicate)
        {
            return FindAll(predicate).FirstOrDefault();
        }

        public ClaimInfo FindFirst(string type, string value = null, string valueType = null, string issuer = null)
        {
            return FindAll(type, value, valueType, issuer).FirstOrDefault();
        }

        public bool? GetBooleanClaim(string type, string issuer = null)
        {
            var value = GetClaim(type: type, issuer: issuer);

            if (value == null) return null;

            return value == bool.TrueString;
        }

        public string GetClaim(string type, string issuer = null)
        {
            return (issuer == null ? FindAll(type) : FindAll(c => c.Type == type && c.Issuer == issuer))
                .Select(c => c.Value)
                .FirstOrDefault();
        }

        public DateTime? GetDateTimeClaim(string type, string issuer = null)
        {
            var value = GetClaim(type: type, issuer: issuer);

            if (value == null) return null;

            DateTime dt;

            if (DateTime.TryParseExact(value, new[] { Defaults.ShortDateTimeFormat }, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal, out dt)) return dt;

            if (DateTime.TryParse(value, out dt)) return dt;

            return null;
        }

        public IEnumerable<string> GetClaims(string type, string issuer = null)
        {
            return (issuer == null ? FindAll(type) : FindAll(c => c.Type == type && c.Issuer == issuer)).Select(c => c.Value);
        }

        public int? GetInteger32Claim(string type, string issuer = null)
        {
            var value = GetClaim(type: type, issuer: issuer);

            if (value == null) return null;

            int result;

            return int.TryParse(value, out result) ? result : (int?)null;
        }

        public IEnumerable<T> GetJsonClaims<T>(string type, string issuer = null)
        {
            return (issuer == null ? FindAll(type) : FindAll(c => c.Type == type && c.Issuer == issuer)).Select(c => JsonConvert.DeserializeObject<T>(c.Value));
        }

        public void RemoveLoginInfo(LoginInfo login)
        {
            RemoveLoginInfo(login.LoginProvider, login.LoginKey);
        }

        public void RemoveLoginInfo(string loginProvider, string loginKey)
        {
            var source = Logins.Where(login => login.LoginKey == loginKey && login.LoginProvider == loginProvider).ToList();

            foreach (var login in source)
            {
                Logins.Remove(login);
            }
        }

        public void RemoveClaim(Claim claim)
        {
            var source = Claims.Where(c => c.Issuer == claim.Issuer && c.Type == claim.Type && c.Value == claim.Value).ToList();

            foreach (var c in source)
            {
                Claims.Remove(c);
            }
        }

        public void RemoveClaim(ClaimInfo claim)
        {
            var source = Claims.Where(c => c.Issuer == claim.Issuer && c.Type == claim.Type && c.Value == claim.Value).ToList();

            foreach (var c in source)
            {
                Claims.Remove(c);
            }
        }

        public void RemoveClaims(string type, string issuer = null)
        {
            var source = (issuer == null ? FindAll(type) : FindAll(c => c.Type == type && c.Issuer == issuer)).ToList();

            foreach (var claim in source)
            {
                RemoveClaim(claim);
            }
        }

        public Claim SetClaim(string type, string value, string valueType = null, string issuer = null)
        {
            RemoveClaims(type, issuer);

            if (value == null) return null;

            return AddClaimUnique(type: type, value: value, valueType: valueType, issuer: issuer);
        }

        public Claim SetClaim(string type, bool? value, string issuer = null)
        {
            RemoveClaims(type, issuer);

            if (!value.HasValue) return null;

            return AddClaimUnique(type: type, value: value.Value ? bool.TrueString : bool.FalseString, valueType: ClaimValueTypes.Boolean, issuer: issuer);
        }

        public Claim SetClaim(string type, DateTime? input, string issuer = null)
        {
            RemoveClaims(type, issuer);

            if (!input.HasValue) return null;

            var value = input.Value.ToString(Defaults.ShortDateTimeFormat);

            return AddClaimUnique(type: type, value: value, valueType: ClaimValueTypes.DateTime, issuer: issuer);
        }

        public Claim SetClaim(string type, int? input, string issuer = null)
        {
            RemoveClaims(type, issuer);

            if (!input.HasValue) return null;

            string value = Convert.ToString(input.Value);

            return AddClaimUnique(type: type, value: value, valueType: ClaimValueTypes.Integer32, issuer: issuer);
        }

        public ClaimsIdentity Update(ClaimsIdentity identity)
        {
            if (Claims != null)
            {
                foreach (var claim in Claims)
                {
                    identity.AddClaimExact(claim);
                }
            }

            if (AccessFailedCount.HasValue) identity.SetClaim(type: UmbilikoClaimTypes.AccessFailedCount, input: AccessFailedCount, issuer: Defaults.IdentityProvider);

            if (Accounts != null)
            {
                identity.Clear(type: UmbilikoClaimTypes.AccountInfo, issuer: Defaults.IdentityProvider);

                foreach (var account in Accounts)
                {
                    identity.AddAccountInfo(account, JsonSerializerSettings);
                }
            }
            
            if (DisplayName != null) identity.SetClaim(type: UmbilikoClaimTypes.DisplayName, value: DisplayName, issuer: Defaults.IdentityProvider);

            if (Email != null) identity.SetClaim(type: ClaimTypes.Email, value: Email, issuer: Defaults.IdentityProvider);

            if (EmailConfirmed != null) identity.SetClaim(type: UmbilikoClaimTypes.EmailConfirmed, input: EmailConfirmed, issuer: Defaults.IdentityProvider);

            if (Defaults.IdentityProvider != null) identity.SetClaim(type: MicrosoftClaimTypes.IdentityProvider, value: Defaults.IdentityProvider, issuer: Defaults.IdentityProvider);

            if (LockoutEnabled.HasValue) identity.SetClaim(type: UmbilikoClaimTypes.LockoutEnabled, input: LockoutEnabled, issuer: Defaults.IdentityProvider);

            if (LockoutEndDateUtc.HasValue) identity.SetClaim(type: UmbilikoClaimTypes.LockoutEndDate, input: LockoutEndDateUtc, issuer: Defaults.IdentityProvider);

            if (Logins != null)
            {
                identity.Clear(type: UmbilikoClaimTypes.LoginInfo, issuer: Defaults.IdentityProvider);

                foreach (var login in Logins)
                {
                    identity.AddLoginInfo(login, JsonSerializerSettings);
                }
            }

            if (Name != null) identity.SetClaim(type: identity.NameClaimType, value: Name, issuer: Defaults.IdentityProvider);

            if (PasswordHash != null) identity.SetClaim(type: UmbilikoClaimTypes.PasswordHash, value: PasswordHash, issuer: Defaults.IdentityProvider);

            if (PhoneNumber != null) identity.SetClaim(type: UmbilikoClaimTypes.PhoneNumber, value: PhoneNumber, issuer: Defaults.IdentityProvider);

            if (PhoneNumberConfirmed.HasValue) identity.SetClaim(type: UmbilikoClaimTypes.PhoneNumberConfirmed, input: PhoneNumberConfirmed.Value, issuer: Defaults.IdentityProvider);

            if (SecurityStamp != null) identity.SetClaim(type: UmbilikoClaimTypes.SecurityStamp, value: SecurityStamp, issuer: Defaults.IdentityProvider);

            
            if (TwoFactorCode != null) identity.SetClaim(type: UmbilikoClaimTypes.TwoFactorCode, value: TwoFactorCode, issuer: Defaults.IdentityProvider);

            if (TwoFactorEnabled.HasValue) identity.SetClaim(type: UmbilikoClaimTypes.TwoFactorEnabled, input: TwoFactorEnabled, issuer: Defaults.IdentityProvider);

            if (TwoFactorConfirmed != null) identity.SetClaim(type: UmbilikoClaimTypes.TwoFactorConfirmed, input: TwoFactorConfirmed, issuer: Defaults.IdentityProvider);

            if (TwoFactorInstant != null) identity.SetClaim(type: UmbilikoClaimTypes.TwoFactorInstant, input: TwoFactorInstant, issuer: Defaults.IdentityProvider);

            return identity;
        }

        #endregion - helper methods -

        public static implicit operator ClaimsIdentity(UserInfo data)
        {
            return data.Update(new ClaimsIdentity(DefaultAuthenticationTypes.ApplicationCookie));
        }

        public static implicit operator UserInfo(ClaimsIdentity identity)
        {
            return identity.Update(new UserInfo());
        }

        public static implicit operator UserInfo(ClaimsPrincipal principal)
        {
            var identity = principal.Identities.FirstOrDefault(id => id.AuthenticationType == DefaultAuthenticationTypes.ApplicationCookie);

            if (identity != null) return identity.Update(new UserInfo());

            return null;
        }

        public static implicit operator List<Claim>(UserInfo data)
        {
            return data.Claims.Select(claim => (Claim)claim).ToList();
        }
    }
}
