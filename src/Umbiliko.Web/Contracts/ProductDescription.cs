﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Contracts
{
    [DataContract]
    public class ProductDescription
    {
        ICollection<FeatureDescription> _features;

        [DataMember(Name = "company", Order = 1)]
        public string Company { get; set; }

        [DataMember(Name = "copyright", Order = 2)]
        public string Copyright { get; set; }

        [DataMember(Name = "description", Order = 4)]
        public string Description { get; set; }

        [DataMember(Name = "features", Order = 7)]
        public ICollection<FeatureDescription> Features
        {
            get { return _features ?? (_features = new Collection<FeatureDescription>()); }
            set { _features = value; }
        }

        [DataMember(Name = "name", Order = 0)]
        public string Name { get; set; }

        [DataMember(Name = "title", Order = 3)]
        public string Title { get; set; }

        [DataMember(Name = "trademark", Order = 5)]
        public string Trademark { get; set; }

        [DataMember(Name = "version", Order = 6)]
        public string Verions { get; set; }
    }
}
