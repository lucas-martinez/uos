﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Umbiliko.Web.Contracts
{
    /// <summary>
    /// Defines constants for the well-known claim types that can be assigned to a subject.
    /// </summary>
    public static class ClaimSystemTypes
    {
        /// <summary>
        /// http://schemas.xmlsoap.org/ws/2009/09/identity/claims/actor.
        /// </summary>
        public const string Actor = global::System.Security.Claims.ClaimTypes.Actor;

        /// <summary>
        /// The URI for a claim that specifies the anonymous user; http://schemas.xmlsoap.org/ws/2005/05/identity/claims/anonymous.
        /// </summary>
        public const string Anonymous = global::System.Security.Claims.ClaimTypes.Anonymous;

        /// <summary>
        /// The URI for a claim that specifies details about whether an identity is authenticated,
        /// http://schemas.xmlsoap.org/ws/2005/05/identity/claims/authenticated.
        /// </summary>
        public const string Authentication = global::System.Security.Claims.ClaimTypes.Authentication;

        /// <summary>
        /// The URI for a claim that specifies the instant at which an entity was authenticated;
        /// http://schemas.microsoft.com/ws/2008/06/identity/claims/authenticationinstant.
        /// </summary>
        public const string AuthenticationInstant = "http://schemas.microsoft.com/ws/2008/06/identity/claims/authenticationinstant";

        /// <summary>
        /// The URI for a claim that specifies the method with which an entity was authenticated;
        /// http://schemas.microsoft.com/ws/2008/06/identity/claims/authenticationmethod.
        /// </summary>
        public const string AuthenticationMethod = global::System.Security.Claims.ClaimTypes.AuthenticationMethod;

        /// <summary>
        /// The URI for a claim that specifies an authorization decision on an entity; http://schemas.xmlsoap.org/ws/2005/05/identity/claims/authorizationdecision.
        /// </summary>
        public const string AuthorizationDecision = global::System.Security.Claims.ClaimTypes.AuthorizationDecision;

        /// <summary>
        /// The URI for a claim that specifies the cookie path; http://schemas.microsoft.com/ws/2008/06/identity/claims/cookiepath.
        /// </summary>
        public const string CookiePath = global::System.Security.Claims.ClaimTypes.CookiePath;

        /// <summary>
        /// The URI for a claim that specifies the country/region in which an entity resides,
        /// http://schemas.xmlsoap.org/ws/2005/05/identity/claims/country.
        /// </summary>
        public const string Country = global::System.Security.Claims.ClaimTypes.Country;

        /// <summary>
        /// The URI for a claim that specifies the date of birth of an entity, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/dateofbirth.
        /// </summary>
        public const string DateOfBirth = global::System.Security.Claims.ClaimTypes.DateOfBirth;

        /// <summary>
        /// The URI for a claim that specifies the deny-only primary group SID on an entity;
        /// http://schemas.microsoft.com/ws/2008/06/identity/claims/denyonlyprimarygroupsid.
        /// A deny-only SID denies the specified entity to a securable object.
        /// </summary>
        public const string DenyOnlyPrimaryGroupSid = global::System.Security.Claims.ClaimTypes.DenyOnlyPrimaryGroupSid;

        /// <summary>
        /// The URI for a claim that specifies the deny-only primary SID on an entity; http://schemas.microsoft.com/ws/2008/06/identity/claims/denyonlyprimarysid.
        /// A deny-only SID denies the specified entity to a securable object.
        /// </summary>
        public const string DenyOnlyPrimarySid = global::System.Security.Claims.ClaimTypes.DenyOnlyPrimarySid;

        /// <summary>
        /// The URI for a claim that specifies a deny-only security identifier (SID) for
        /// an entity, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/denyonlysid.
        /// A deny-only SID denies the specified entity to a securable object.
        /// </summary>
        public const string DenyOnlySid = global::System.Security.Claims.ClaimTypes.DenyOnlySid;

        /// <summary>
        /// http://schemas.microsoft.com/ws/2008/06/identity/claims/denyonlywindowsdevicegroup.
        /// </summary>
        public const string DenyOnlyWindowsDeviceGroup = global::System.Security.Claims.ClaimTypes.DenyOnlyWindowsDeviceGroup;

        /// <summary>
        /// The URI for a claim that specifies the DNS name associated with the computer
        //  name or with the alternative name of either the subject or issuer of an X.509
        //  certificate, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/dns.
        /// </summary>
        public const string Dns = global::System.Security.Claims.ClaimTypes.Dns;

        /// <summary>
        /// http://schemas.microsoft.com/ws/2008/06/identity/claims/dsa.
        /// </summary>
        public const string Dsa = global::System.Security.Claims.ClaimTypes.Dsa;

        /// <summary>
        /// The URI for a claim that specifies the email address of an entity, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/email.
        /// </summary>
        public const string Email = global::System.Security.Claims.ClaimTypes.Email;

        /// <summary>
        /// http://schemas.microsoft.com/ws/2008/06/identity/claims/expiration.
        /// </summary>
        public const string Expiration = global::System.Security.Claims.ClaimTypes.Expiration;

        /// <summary>
        /// http://schemas.microsoft.com/ws/2008/06/identity/claims/expired.
        /// </summary>
        public const string Expired = global::System.Security.Claims.ClaimTypes.Expired;

        /// <summary>
        /// The URI for a claim that specifies the gender of an entity, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/gender.
        /// </summary>
        public const string Gender = global::System.Security.Claims.ClaimTypes.Gender;

        /// <summary>
        /// The URI for a claim that specifies the given name of an entity, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname.
        /// </summary>
        public const string GivenName = global::System.Security.Claims.ClaimTypes.GivenName;

        /// <summary>
        /// The URI for a claim that specifies the SID for the group of an entity, http://schemas.microsoft.com/ws/2008/06/identity/claims/groupsid.
        /// </summary>
        public const string GroupSid = global::System.Security.Claims.ClaimTypes.GroupSid;

        /// <summary>
        /// The URI for a claim that specifies a hash value, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/hash.
        /// </summary>
        public const string Hash = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/hash";

        /// <summary>
        /// The URI for a claim that specifies the home phone number of an entity, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/homephone.
        /// </summary>
        public const string HomePhone = global::System.Security.Claims.ClaimTypes.HomePhone;

        /// <summary>
        /// http://schemas.microsoft.com/ws/2008/06/identity/claims/ispersistent.
        /// </summary>
        public const string IsPersistent = global::System.Security.Claims.ClaimTypes.IsPersistent;

        /// <summary>
        /// The URI for a claim that specifies the locale in which an entity resides, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/locality.
        /// </summary>
        public const string Locality = global::System.Security.Claims.ClaimTypes.Locality;

        /// <summary>
        /// The URI for a claim that specifies the mobile phone number of an entity, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/mobilephone.
        /// </summary>
        public const string MobilePhone = global::System.Security.Claims.ClaimTypes.MobilePhone;

        /// <summary>
        /// The URI for a claim that specifies the name of an entity, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name.
        /// </summary>
        public const string Name = global::System.Security.Claims.ClaimTypes.Name;

        /// <summary>
        /// The URI for a claim that specifies the name of an entity, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier.
        /// </summary>
        public const string NameIdentifier = global::System.Security.Claims.ClaimTypes.NameIdentifier;

        /// <summary>
        /// The URI for a claim that specifies the alternative phone number of an entity, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/otherphone.
        /// </summary>
        public const string OtherPhone = global::System.Security.Claims.ClaimTypes.OtherPhone;

        /// <summary>
        /// The URI for a claim that specifies the postal code of an entity, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/postalcode.
        /// </summary>
        public const string PostalCode = global::System.Security.Claims.ClaimTypes.PostalCode;

        /// <summary>
        /// The URI for a claim that specifies the primary group SID of an entity, http://schemas.microsoft.com/ws/2008/06/identity/claims/primarygroupsid.
        /// </summary>
        public const string PrimaryGroupSid = global::System.Security.Claims.ClaimTypes.PrimaryGroupSid;

        /// <summary>
        /// The URI for a claim that specifies the primary SID of an entity, http://schemas.microsoft.com/ws/2008/06/identity/claims/primarysid.
        /// </summary>
        public const string PrimarySid = global::System.Security.Claims.ClaimTypes.PrimarySid;

        /// <summary>
        /// The URI for a claim that specifies the role of an entity, http://schemas.microsoft.com/ws/2008/06/identity/claims/role.
        /// </summary>
        public const string Role = global::System.Security.Claims.ClaimTypes.Role;

        /// <summary>
        /// The URI for a claim that specifies an RSA key, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/rsa.
        /// </summary>
        public const string Rsa = global::System.Security.Claims.ClaimTypes.Rsa;

        /// <summary>
        /// The URI for a claim that specifies a serial number, http://schemas.microsoft.com/ws/2008/06/identity/claims/serialnumber.
        /// </summary>
        public const string SerialNumber = global::System.Security.Claims.ClaimTypes.SerialNumber;

        /// <summary>
        /// The URI for a claim that specifies a security identifier (SID), http://schemas.xmlsoap.org/ws/2005/05/identity/claims/sid.
        /// </summary>
        public const string Sid = global::System.Security.Claims.ClaimTypes.Sid;

        /// <summary>
        /// The URI for a claim that specifies a service principal name (SPN) claim, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/spn.
        /// </summary>
        public const string Spn = global::System.Security.Claims.ClaimTypes.Spn;

        /// <summary>
        /// The URI for a claim that specifies the state or province in which an entity resides, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/stateorprovince.
        /// </summary>
        public const string StateOrProvince = global::System.Security.Claims.ClaimTypes.StateOrProvince;

        /// <summary>
        /// The URI for a claim that specifies the street address of an entity, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/streetaddress.
        /// </summary>
        public const string StreetAddress = global::System.Security.Claims.ClaimTypes.StreetAddress;

        /// <summary>
        /// The URI for a claim that specifies the surname of an entity, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname.
        /// </summary>
        public const string Surname = global::System.Security.Claims.ClaimTypes.Surname;

        /// <summary>
        /// The URI for a claim that identifies the system entity, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/system.
        /// </summary>
        public const string System = global::System.Security.Claims.ClaimTypes.System;

        /// <summary>
        /// The URI for a claim that specifies a thumbprint, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/thumbprint.
        /// A thumbprint is a globally unique SHA-1 hash of an X.509 certificate.
        /// </summary>
        public const string Thumbprint = global::System.Security.Claims.ClaimTypes.Thumbprint;

        /// <summary>
        /// The URI for a claim that specifies a user principal name (UPN), http://schemas.xmlsoap.org/ws/2005/05/identity/claims/upn.
        /// </summary>
        public const string Upn = global::System.Security.Claims.ClaimTypes.Upn;

        /// <summary>
        /// The URI for a claim that specifies a URI, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/uri.
        /// </summary>
        public const string Uri = global::System.Security.Claims.ClaimTypes.Uri;

        /// <summary>
        /// http://schemas.microsoft.com/ws/2008/06/identity/claims/userdata.
        /// </summary>
        public const string UserData = global::System.Security.Claims.ClaimTypes.UserData;

        /// <summary>
        /// http://schemas.microsoft.com/ws/2008/06/identity/claims/version.
        /// </summary>
        public const string Version = global::System.Security.Claims.ClaimTypes.Version;

        /// <summary>
        /// The URI for a claim that specifies the webpage of an entity, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/webpage.
        /// </summary>
        public const string Webpage = global::System.Security.Claims.ClaimTypes.Webpage;

        /// <summary>
        /// The URI for a claim that specifies the Windows domain account name of an entity,
        /// http://schemas.microsoft.com/ws/2008/06/identity/claims/windowsaccountname.
        /// </summary>
        public const string WindowsAccountName = global::System.Security.Claims.ClaimTypes.WindowsAccountName;

        /// <summary>
        /// http://schemas.microsoft.com/ws/2008/06/identity/claims/windowsdeviceclaim.
        /// </summary>
        public const string WindowsDeviceClaim = global::System.Security.Claims.ClaimTypes.WindowsDeviceClaim;

        /// <summary>
        /// http://schemas.microsoft.com/ws/2008/06/identity/claims/windowsdevicegroup.
        /// </summary>
        public const string WindowsDeviceGroup = global::System.Security.Claims.ClaimTypes.WindowsDeviceGroup;

        /// <summary>
        /// http://schemas.microsoft.com/ws/2008/06/identity/claims/windowsfqbnversion.
        /// </summary>
        public const string WindowsFqbnVersion = global::System.Security.Claims.ClaimTypes.WindowsFqbnVersion;

        /// <summary>
        /// http://schemas.microsoft.com/ws/2008/06/identity/claims/windowssubauthority.
        /// </summary>
        public const string WindowsSubAuthority = global::System.Security.Claims.ClaimTypes.WindowsSubAuthority;

        /// <summary>
        /// http://schemas.microsoft.com/ws/2008/06/identity/claims/windowsuserclaim.
        /// </summary>
        public const string WindowsUserClaim = global::System.Security.Claims.ClaimTypes.WindowsUserClaim;

        /// <summary>
        /// The URI for a distinguished name claim of an X.509 certificate, http://schemas.xmlsoap.org/ws/2005/05/identity/claims/x500distinguishedname.
        //  The X.500 standard defines the methodology for defining distinguished names that
        //  are used by X.509 certificates.
        /// </summary>
        public const string X500DistinguishedName = global::System.Security.Claims.ClaimTypes.X500DistinguishedName;
    }
}
