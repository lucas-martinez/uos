﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Contracts
{
    [DataContract]
    public class SubscriptionInfo
    {
        [DataMember(Name = "featureName")]
        public string FeatureName { get; set; }

        [DataMember(Name = "expiresUtc")]
        public DateTime? ExpiresUtc { get; set; }
    }
}
