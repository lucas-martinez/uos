﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Contracts
{
    using Results;

    public interface IUserStore
    {
        Task<UserResult> CreateUserAsync(UserInfo data);

        Task<UserResult> DestroyUserAsync(UserInfo data);

        Task<UserResult> FindUserAsync(string userNameOrEmailAddress);

        Task<UserResult> FindUserByEmailAsync(string email);

        Task<UserResult> FindUserByLoginAsync(string loginProvider, string providerKey);

        Task<UserResult> UpdateAsync(UserRequest request);
    }
}
