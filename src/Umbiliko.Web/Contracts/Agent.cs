﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Contracts
{
    [DataContract]
    public class Agent
    {
        [DataMember]
        public Guid AgentId { get; set; }

        [DataMember]
        public string Client { get; set; }
    }
}
