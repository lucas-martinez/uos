﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Contracts
{
    [DataContract]
    internal class GoogleUserInfo
    {
        [DataMember(Name = "circledByCount")]
        public int? CircledByCount { get; set; }

        [DataMember(Name = "displayName")]
        public string DisplayName { get; set; }

        [DataMember(Name = "emails")]
        public ICollection<GoogleEmailData> Emails { get; set; }

        [DataMember(Name = "etag")]
        public string Etag { get; set; }

        [DataMember(Name = "gender")]
        public string Gender { get; set; }

        [DataMember(Name = "id")]
        public string Id { get; set; }

        [DataMember(Name = "image")]
        public GoogleImageData Image { get; set; }

        [DataMember(Name = "isPlusUser")]
        public bool? IsPlusUser { get; set; }

        [DataMember(Name = "kind")]
        public string Kind { get; set; }

        [DataMember(Name = "language")]
        public string Language { get; set; }

        [DataMember(Name = "name")]
        public GoogleNameData Name { get; set; }

        [DataMember(Name = "objectType")]
        public string ObjectType { get; set; }

        [DataMember(Name = "tagline")]
        public string Tagline { get; set; }

        [DataMember(Name = "url")]
        public string Url { get; set; }

        [DataMember(Name = "urls")]
        public ICollection<GoogleUrlData> Urls { get; set; }

        [DataMember(Name = "verified")]
        public bool Verified { get; set; }
    }

    internal class GoogleEmailData
    {
        [DataMember(Name = "value")]
        public string Value { get; set; }

        [DataMember(Name = "type")]
        public string Type { get; set; }
    }

    internal class GoogleImageData
    {
        [DataMember(Name = "isDefault")]
        public bool? IsDefault { get; set; }

        [DataMember(Name = "url")]
        public string Url { get; set; }
    }

    internal class GoogleNameData
    {
        [DataMember(Name = "familyName")]
        public string FamilyName { get; set; }

        [DataMember(Name = "givenName")]
        public string GivenName { get; set; }
    }

    internal class GoogleUrlData
    {
        [DataMember(Name = "label")]
        public string Label { get; set; }

        [DataMember(Name = "value")]
        public string Value { get; set; }

        [DataMember(Name = "type")]
        public string Type { get; set; }
    }
}
