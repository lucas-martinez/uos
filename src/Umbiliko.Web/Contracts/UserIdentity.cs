﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Umbiliko.Web.Contracts
{
    using Constants;

    public class UserIdentity : ClaimsIdentity, IUser
    {
        public const string IdentityProviderClaimType = @"http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider";
        public const string AccessFailedCountClaimType = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/accessfailedcount";
        public const string AccessTokenClaimType = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/accesstoken";
        public const string AccountInfoClaimType = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/accountinfo";
        public const string CompanyCodeClaimType = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/companycode";
        public const string EmailClaimType = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/email";
        public const string EmailConfirmedClaimType = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/emailconfirmed";
        public const string LockoutEnabledClaimType = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/lockoutenabled";
        public const string LockoutEndDateClaimType = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/lockoutenddate";
        public const string LoginInfoClaimType = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/logininfo";
        public const string PhoneNumberClaimType = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/phoneconfirmed";
        public const string PhoneNumberConfirmedClaimType = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/phonenumberconfirmed";
        public const string SecurityStampClaimType = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/securitystamp";
        public const string SegmentCodeClaimType = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/segmentcode";
        public const string TwoFactorEnabledClaimType = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/twofactorenabled";
        public const string TwoFactorCodeClaimType = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/twofactorcode";
        public const string TwoFactorConfirmedClaimType = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/twofactorconfirmed";
        public const string TwoFactorInstantClaimType = @"http://schemas.umbiliko.com/accesscontrolservice/2016/01/claims/twofactorinstant";

        UserData _data;

        public UserIdentity()
            : base(DefaultAuthenticationTypes.ApplicationCookie)
        {
            SetClaim(type: IdentityProviderClaimType, value: Defaults.ClaimIdentityProvider, issuer: Defaults.ClaimIssuer);
        }

        public UserIdentity(string authenticationType)
            : base(authenticationType)
        {
        }

        public UserIdentity(string name, string authenticationType)
            : base(authenticationType)
        {
            SetClaim(type: NameClaimType, value: name, issuer: Defaults.ClaimIssuer);
            SetClaim(type: IdentityProviderClaimType, value: Defaults.ClaimIdentityProvider, issuer: Defaults.ClaimIssuer);

            Debug.Assert(AuthenticationType == authenticationType);
            Debug.Assert(Name == name);
        }

        public UserIdentity(IPrincipal user)
            : base(user.Identity)
        {
            Debug.Assert(user.Identity.Name == Name);
            Debug.Assert(user.Identity.AuthenticationType == AuthenticationType);
            Debug.Assert(user.Identity.IsAuthenticated == IsAuthenticated);
        }

        public UserIdentity(IIdentity identity)
            : base(identity)
        {
            Debug.Assert(identity.Name == Name);
            Debug.Assert(identity.AuthenticationType == AuthenticationType);
            Debug.Assert(identity.IsAuthenticated == IsAuthenticated);
        }

        public UserIdentity(IEnumerable<Claim> claims)
            : base(claims)
        {
            Debug.Assert(Claims.Count() == claims.Count());
        }

        public UserIdentity(ClaimsPrincipal principal)
            : base(principal.Identity, principal.Claims)
        {
            Debug.Assert(principal.Identity.Name == Name);
            Debug.Assert(principal.Identity.AuthenticationType == AuthenticationType);
            Debug.Assert(principal.Identity.IsAuthenticated == IsAuthenticated);
            Debug.Assert(Claims.Count() == principal.Claims.Count());
        }

        public UserIdentity(ClaimsIdentity identity)
            : base(identity, identity.Claims)
        {
            Debug.Assert(identity.Name == Name);
            Debug.Assert(identity.AuthenticationType == AuthenticationType);
            Debug.Assert(identity.IsAuthenticated == IsAuthenticated);
            Debug.Assert(Claims.Count() == identity.Claims.Count());
        }

        public UserIdentity(ClaimsIdentity identity, string authenticationType)
            : base(identity.Claims, authenticationType)
        {
            Debug.Assert(identity.Name == Name);
            Debug.Assert(identity.AuthenticationType == AuthenticationType);
            Debug.Assert(identity.IsAuthenticated == IsAuthenticated);
            Debug.Assert(Claims.Count() == identity.Claims.Count());
        }

        public UserIdentity(IIdentity identity, IEnumerable<Claim> claims)
            : base(identity, claims)
        {
            Debug.Assert(identity.Name == Name);
            Debug.Assert(identity.AuthenticationType == AuthenticationType);
            Debug.Assert(identity.IsAuthenticated == IsAuthenticated);
            Debug.Assert(Claims.Count() == claims.Count());
        }

        public int? AccessFailedCount
        {
            get { return GetInteger32Claim(type: AccessFailedCountClaimType, issuer: Defaults.ClaimIssuer); }
            internal set { SetClaim(type: AccessFailedCountClaimType, input: value, issuer: Defaults.ClaimIssuer); }
        }

        public string AccessToken
        {
            get { return GetClaim(type: AccessTokenClaimType, issuer: Defaults.ClaimIssuer) ?? GetClaim(type: ClaimTypes.AuthenticationMethod); }
            internal set { SetClaim(type: AccessTokenClaimType, value: value, issuer: Defaults.ClaimIssuer); }
        }

        public IEnumerable<AccountInfo> Accounts
        {
            get { return GetJsonClaims<AccountInfo>(type: AccountInfoClaimType, issuer: Defaults.ClaimIssuer); }
            set
            {
                Clear(type: AccountInfoClaimType, issuer: Defaults.ClaimIssuer);
                if (value != null)
                {
                    foreach (var account in value)
                    {
                        AddAccountInfo(account);
                    }
                }
                //if (value != null) value.ForEach(AddAccountInfo);
            }
        }

        public bool Anonymous
        {
            get { return GetBooleanClaim(type: ClaimTypes.Anonymous, issuer: Defaults.ClaimIssuer) ?? GetBooleanClaim(type: ClaimTypes.Anonymous) ?? false; }
            internal set { SetClaim(type: ClaimTypes.Anonymous, value: value, issuer: Defaults.ClaimIssuer); }
        }

        public string Authentication
        {
            get { return GetClaim(type: ClaimTypes.AuthenticationMethod, issuer: Defaults.ClaimIssuer) ?? GetClaim(type: ClaimTypes.AuthenticationMethod); }
            internal set { SetClaim(type: ClaimTypes.AuthenticationMethod, value: value, issuer: Defaults.ClaimIssuer); }
        }

        public DateTime? AuthenticationInstant
        {
            get { return GetDateTimeClaim(type: ClaimTypes.AuthenticationInstant, issuer: Defaults.ClaimIssuer) ?? GetDateTimeClaim(type: ClaimTypes.AuthenticationInstant); }
            internal set { SetClaim(type: ClaimTypes.AuthenticationInstant, input: value, issuer: Defaults.ClaimIssuer); }
        }

        public string AuthenticationMethod
        {
            get { return GetClaim(type: ClaimTypes.AuthenticationMethod, issuer: Defaults.ClaimIssuer) ?? GetClaim(type: ClaimTypes.AuthenticationMethod); }
            internal set { SetClaim(type: ClaimTypes.AuthenticationMethod, value: value, issuer: Defaults.ClaimIssuer); }
        }

        public string AuthorizationDecision
        {
            get { return GetClaim(type: ClaimTypes.AuthorizationDecision, issuer: Defaults.ClaimIssuer) ?? GetClaim(type: ClaimTypes.AuthorizationDecision); }
            internal set { SetClaim(type: ClaimTypes.AuthorizationDecision, value: value, issuer: Defaults.ClaimIssuer); }
        }

        public string CompanyCode
        {
            get { return GetClaim(type: CompanyCodeClaimType, issuer: Defaults.ClaimIssuer); }
            internal set { SetClaim(type: CompanyCodeClaimType, value: value, issuer: Defaults.ClaimIssuer); }
        }

        public DateTime? DateOfBirth
        {
            get { return GetDateTimeClaim(type: ClaimTypes.DateOfBirth, issuer: Defaults.ClaimIssuer) ?? GetDateTimeClaim(type: ClaimTypes.DateOfBirth); }
            internal set { SetClaim(type: ClaimTypes.DateOfBirth, input: value, issuer: Defaults.ClaimIssuer); }
        }

        public string Country
        {
            get { return GetClaim(type: ClaimTypes.Country, issuer: Defaults.ClaimIssuer) ?? GetClaim(type: ClaimTypes.Country); }
            internal set { SetClaim(type: ClaimTypes.Country, value: value, issuer: Defaults.ClaimIssuer); }
        }

        public string Email
        {
            get { return GetClaim(type: EmailClaimType, issuer: Defaults.ClaimIssuer); }
            internal set { SetClaim(type: EmailClaimType, value: value, valueType: ClaimValueTypes.Email, issuer: Defaults.ClaimIssuer); }
        }

        public bool? EmailConfirmed
        {
            get { return GetBooleanClaim(type: EmailConfirmedClaimType, issuer: Defaults.ClaimIssuer); }
            internal set { SetClaim(EmailConfirmedClaimType, value: value, issuer: Defaults.ClaimIssuer); }
        }

        public bool Expired
        {
            get { return GetBooleanClaim(type: ClaimTypes.Expired, issuer: Defaults.ClaimIssuer) ?? GetBooleanClaim(type: ClaimTypes.Expired) ?? false; }
            internal set { SetClaim(type: ClaimTypes.Expired, value: value, issuer: Defaults.ClaimIssuer); }
        }

        public DateTime? ExpiresUtc
        {
            get { return GetDateTimeClaim(type: ClaimTypes.Expiration, issuer: Defaults.ClaimIssuer) ?? GetDateTimeClaim(type: ClaimTypes.Expiration); }
            internal set { SetClaim(ClaimTypes.Expiration, input: value, issuer: Defaults.ClaimIssuer); }
        }

        public string Gender
        {
            get { return GetClaim(type: ClaimTypes.Gender, issuer: Defaults.ClaimIssuer) ?? GetClaim(type: ClaimTypes.Gender); }
            internal set { SetClaim(ClaimTypes.Gender, value: value, issuer: Defaults.ClaimIssuer); }
        }

        public string GivenName
        {
            get { return GetClaim(type: ClaimTypes.GivenName, issuer: Defaults.ClaimIssuer) ?? GetClaim(type: ClaimTypes.GivenName); }
            internal set { SetClaim(ClaimTypes.GivenName, value: value, issuer: Defaults.ClaimIssuer); }
        }

        public string Hash
        {
            get { return GetClaim(ClaimTypes.Hash, Defaults.ClaimIssuer) ?? GetClaim(ClaimTypes.Hash); }
            internal set { SetClaim(ClaimTypes.Hash, value: value, issuer: Defaults.ClaimIssuer); }
        }

        public string HomePhone
        {
            get { return GetClaim(type: ClaimTypes.HomePhone, issuer: Defaults.ClaimIssuer) ?? GetClaim(type: ClaimTypes.HomePhone); }
            internal set { SetClaim(ClaimTypes.HomePhone, value: value, issuer: Defaults.ClaimIssuer); }
        }

        public Guid? Identifier
        {
            get
            {
                Guid value;
                var id = NameIdentifier;
                return id != null && Guid.TryParse(id, out value) ? value : (Guid?)null;
            }
            set { NameIdentifier = value != null ? value.ToString() : null; }
        }

        public string IdentityProvider
        {
            get { return GetClaim(type: IdentityProviderClaimType, issuer: Defaults.ClaimIssuer); }
            set { SetClaim(type: IdentityProviderClaimType, value: value, issuer: Defaults.ClaimIssuer); }
        }

        public bool IsPersistent
        {
            get { return GetBooleanClaim(type: ClaimTypes.IsPersistent, issuer: Defaults.ClaimIssuer) ?? GetBooleanClaim(type: ClaimTypes.IsPersistent) ?? false; }
            internal set { SetClaim(ClaimTypes.IsPersistent, value: value, issuer: Defaults.ClaimIssuer); }
        }

        public bool? LockoutEnabled
        {
            get { return GetBooleanClaim(type: LockoutEnabledClaimType, issuer: Defaults.ClaimIssuer); }
            internal set { SetClaim(LockoutEnabledClaimType, value: value, issuer: Defaults.ClaimIssuer); }
        }

        public DateTime? LockoutEndDate
        {
            get { return GetDateTimeClaim(type: LockoutEndDateClaimType, issuer: Defaults.ClaimIssuer); }
            internal set { SetClaim(type: LockoutEndDateClaimType, input: value, issuer: Defaults.ClaimIssuer); }
        }

        public IEnumerable<LoginInfo> Logins
        {
            get { return GetJsonClaims<LoginInfo>(type: AccountInfoClaimType, issuer: Defaults.ClaimIssuer); }
            set
            {
                Clear(type: LoginInfoClaimType, issuer: Defaults.ClaimIssuer);
                if (value != null)
                {
                    foreach (var login in value)
                    {
                        AddLoginInfo(login);
                    }
                }
                //if (value != null) value.ForEach(AddAccountInfo);
            }
        }

        public string NameIdentifier
        {
            get { return GetClaim(ClaimTypes.NameIdentifier, Defaults.ClaimIssuer) ?? GetClaim(ClaimTypes.NameIdentifier); }
            internal set { SetClaim(ClaimTypes.NameIdentifier, value: value, issuer: Defaults.ClaimIssuer); }
        }

        public string MobilePhone
        {
            get { return GetClaim(ClaimTypes.MobilePhone, Defaults.ClaimIssuer) ?? GetClaim(ClaimTypes.MobilePhone); }
            internal set { SetClaim(ClaimTypes.MobilePhone, value: value, issuer: Defaults.ClaimIssuer); }
        }

        public string OtherPhone
        {
            get { return GetClaim(ClaimTypes.OtherPhone, Defaults.ClaimIssuer) ?? GetClaim(ClaimTypes.OtherPhone); }
            internal set { SetClaim(ClaimTypes.OtherPhone, value: value, issuer: Defaults.ClaimIssuer); }
        }

        public string PhoneNumber
        {
            get { return GetClaim(type: PhoneNumberClaimType, issuer: Defaults.ClaimIssuer); }
            internal set { SetClaim(type: PhoneNumberClaimType, value: value, issuer: Defaults.ClaimIssuer); }
        }

        public bool? PhoneNumberConfirmed
        {
            get { return GetBooleanClaim(type: PhoneNumberConfirmedClaimType, issuer: Defaults.ClaimIssuer); }
            internal set { SetClaim(PhoneNumberConfirmedClaimType, value: value, issuer: Defaults.ClaimIssuer); }
        }

        public string PostalCode
        {
            get { return GetClaim(ClaimTypes.PostalCode, Defaults.ClaimIssuer) ?? GetClaim(ClaimTypes.PostalCode); }
            internal set { SetClaim(ClaimTypes.PostalCode, value: value, issuer: Defaults.ClaimIssuer); }
        }

        /// <summary>
        /// Primary group security identifier
        /// </summary>
        public string PrimaryGroupSid
        {
            get { return GetClaim(ClaimTypes.PrimaryGroupSid, Defaults.ClaimIssuer) ?? GetClaim(ClaimTypes.PrimaryGroupSid); }
            internal set { SetClaim(ClaimTypes.PrimaryGroupSid, value: value, issuer: Defaults.ClaimIssuer); }
        }

        /// <summary>
        /// Primary security identifier
        /// </summary>
        public string PrimarySid
        {
            get { return GetClaim(ClaimTypes.PrimarySid, Defaults.ClaimIssuer) ?? GetClaim(ClaimTypes.PrimarySid); }
            internal set { SetClaim(ClaimTypes.PrimarySid, value: value, issuer: Defaults.ClaimIssuer); }
        }

        public AuthenticationProperties Properties
        {
            get
            {
                var issuedUtc = AuthenticationInstant;
                var expiresUtc = ExpiresUtc;

                var properties = new AuthenticationProperties()
                {
                    AllowRefresh = true,
                    IsPersistent = IsPersistent,
                    IssuedUtc = issuedUtc.HasValue ? new DateTimeOffset(issuedUtc.Value) : (DateTimeOffset?)null,
                    RedirectUri = Uri,
                    ExpiresUtc = expiresUtc.HasValue ? new DateTimeOffset(expiresUtc.Value) : (DateTimeOffset?)null
                };

                /// add more values to proverties here
                /// 

                return properties;
            }
        }

        public IEnumerable<string> Roles
        {
            get { return GetClaims(type: RoleClaimType, issuer: Defaults.ClaimIssuer); }
            set
            {
                Clear(type: RoleClaimType, issuer: Defaults.ClaimIssuer);

                if (value != null)
                {
                    foreach (var role in value)
                    {
                        AddRole(role);
                    }
                }
            }
        }

        public string SecurityStamp
        {
            get { return GetClaim(type: SecurityStampClaimType, issuer: Defaults.ClaimIssuer); }
            internal set { SetClaim(type: SecurityStampClaimType, value: value, issuer: Defaults.ClaimIssuer); }
        }

        public string SegmentCode
        {
            get { return GetClaim(type: SegmentCodeClaimType, issuer: Defaults.ClaimIssuer); }
            internal set { SetClaim(type: SegmentCodeClaimType, value: value, issuer: Defaults.ClaimIssuer); }
        }

        /// <summary>
        /// Session identifier
        /// </summary>
        public string Sid
        {
            get { return GetClaim(ClaimTypes.Sid, Defaults.ClaimIssuer) ?? GetClaim(ClaimTypes.Sid); }
            internal set { SetClaim(ClaimTypes.Sid, value: value, issuer: Defaults.ClaimIssuer); }
        }
        
        public string StateOrProvince
        {
            get { return GetClaim(ClaimTypes.StateOrProvince, Defaults.ClaimIssuer) ?? GetClaim(ClaimTypes.StateOrProvince); }
            internal set { SetClaim(ClaimTypes.StateOrProvince, value: value, issuer: Defaults.ClaimIssuer); }
        }

        public string StreetAddress
        {
            get { return GetClaim(ClaimTypes.StreetAddress, Defaults.ClaimIssuer) ?? GetClaim(ClaimTypes.StreetAddress); }
            internal set { SetClaim(ClaimTypes.StreetAddress, value: value, issuer: Defaults.ClaimIssuer); }
        }

        public string Surname
        {
            get { return GetClaim(ClaimTypes.Surname, Defaults.ClaimIssuer) ?? GetClaim(ClaimTypes.Surname); }
            internal set { SetClaim(ClaimTypes.Surname, value: value, issuer: Defaults.ClaimIssuer); }
        }

        public bool? TwoFactorEnabled
        {
            get { return GetBooleanClaim(TwoFactorEnabledClaimType, Defaults.ClaimIssuer); }
            internal set { SetClaim(TwoFactorEnabledClaimType, value: value, issuer: Defaults.ClaimIssuer); }
        }

        public string TwoFactorCode
        {
            get { return GetClaim(TwoFactorCodeClaimType, Defaults.ClaimIssuer); }
            internal set { SetClaim(TwoFactorCodeClaimType, value: value, issuer: Defaults.ClaimIssuer); }
        }

        public bool TwoFactorCompleted
        {
            get { return GetBooleanClaim(TwoFactorConfirmedClaimType, Defaults.ClaimIssuer) ?? false; }
            internal set { SetClaim(TwoFactorConfirmedClaimType, value: value, issuer: Defaults.ClaimIssuer); }
        }

        public DateTime? TwoFactorInstant
        {
            get { return GetDateTimeClaim(TwoFactorInstantClaimType, Defaults.ClaimIssuer); }
            internal set { SetClaim(TwoFactorInstantClaimType, input: value, issuer: Defaults.ClaimIssuer); }
        }

        public string Uri
        {
            get { return GetClaim(ClaimTypes.Uri, Defaults.ClaimIssuer) ?? GetClaim(ClaimTypes.Uri); }
            internal set { SetClaim(ClaimTypes.Uri, value: value, issuer: Defaults.ClaimIssuer); }
        }

        #region - IUser -

        string IUser<string>.Id
        {
            get { return NameIdentifier; }
        }

        string IUser<string>.UserName
        {
            get { return Name; }
            set { AddClaim(new Claim(NameClaimType, value)); }
        }

        #endregion - IUser -

        #region - helpers -

        public Claim AddAccountInfo(AccountInfo account)
        {
            var claim = FindAll(c => c.Type == AccountInfoClaimType).FirstOrDefault(c =>
            {
                var value = JsonConvert.DeserializeObject<AccountInfo>(c.Value);

                return value.CompanyCode == account.CompanyCode && value.SegmentCode == account.SegmentCode;
            });

            if (claim == null)
            {
                var value = JsonConvert.SerializeObject(account);

                AddClaim(type: AccountInfoClaimType, value: value, issuer: Defaults.ClaimIssuer);
            }

            return claim;
        }

        public Claim AddClaim(string type, string value, string valueType = null, string issuer = null)
        {
            if (valueType == null)
            {
                valueType = ClaimValueTypes.String;
            }

            var claim = new Claim(type: type, value: value, valueType: valueType, issuer: issuer ?? Defaults.ClaimIssuer);

            AddClaim(claim);

            return claim;
        }

        public Claim AddClaimExact(Claim claim)
        {
            return AddClaimExact(type: claim.Type, value: claim.Value, valueType: claim.ValueType, issuer: claim.Issuer);
        }

        public Claim AddClaimExact(ClaimInfo claim)
        {
            return AddClaimExact(type: claim.Type, value: claim.Value, valueType: claim.ValueType, issuer: claim.Issuer);
        }

        public Claim AddClaimExact(string type, string value, string valueType = null, string issuer = null)
        {
            var claim = FindFirst(c => c.Type == type && c.Value == value && c.Issuer == issuer);

            if (claim == null)
            {
                claim = AddClaim(type: type, value: value, valueType: valueType, issuer: issuer);
            }
            else if (claim.ValueType != valueType && valueType != null && valueType != ClaimValueTypes.String)
            {
                RemoveClaim(claim);
                claim = AddClaim(type: type, value: value, valueType: valueType, issuer: issuer);
            }

            return claim;
        }

        public Claim AddClaimUnique(string type, string value, string valueType = null, string issuer = null)
        {
            var claim = FindFirst(c => c.Type == type && c.Issuer == issuer);

            if (claim == null)
            {
                claim = AddClaim(type: type, value: value, valueType: valueType, issuer: issuer);
            }
            else if (claim.Value != value || (claim.ValueType != valueType && valueType != null && valueType != ClaimValueTypes.String))
            {
                RemoveClaim(claim);
                claim = AddClaim(type: type, value: value, valueType: valueType, issuer: issuer);
            }

            return claim;
        }

        public Claim AddLoginInfo(LoginInfo login)
        {
            if (login.LoginProvider == null || login.LoginKey == null) return null;

            var claim = FindAll(c => c.Type == LoginInfoClaimType).FirstOrDefault(c =>
            {
                var value = JsonConvert.DeserializeObject<LoginInfo>(c.Value);

                return value.LoginProvider == login.LoginProvider && value.LoginKey == login.LoginKey;
            });

            if (claim == null)
            {
                var value = JsonConvert.SerializeObject(login);

                AddClaim(type: LoginInfoClaimType, value: value, issuer: Defaults.ClaimIssuer);
            }

            return claim;
        }

        public void AddRole(string role)
        {
            AddClaim(type: RoleClaimType, value: role, issuer: Defaults.ClaimIssuer);
        }

        public UserData GetChanges()
        {
            UserData current = this, original = _data;

            if (_data == null) return current;

            UserData delta = new UserData();

            if (current.CompanyCode != original.CompanyCode) delta.CompanyCode = current.CompanyCode;

            if (current.PasswordHash != original.PasswordHash) delta.PasswordHash = current.PasswordHash;

            if (current.SessionId != original.SessionId) delta.SessionId = current.SessionId;

            if (current.SegmentCode != original.SegmentCode) delta.SegmentCode = current.SegmentCode;

            if (current.TwoFactorCode != original.TwoFactorCode) delta.TwoFactorCode = current.TwoFactorCode;

            if (current.TwoFactorConfirmed != original.TwoFactorConfirmed) delta.TwoFactorConfirmed = current.TwoFactorConfirmed;

            if (current.TwoFactorEnabled != original.TwoFactorEnabled) delta.TwoFactorEnabled = current.TwoFactorEnabled;

            return delta;
        }

        public void Clear(string type, string issuer = null)
        {
            (issuer == null ? FindAll(type) : FindAll(c => c.Type == type && c.Issuer == issuer))
                .ToList()
                .ForEach(RemoveClaim);
        }

        public IEnumerable<Claim> FilterClaims(params string[] excludeTypes)
        {
            return Claims.Where(c => !excludeTypes.Contains(c.Type));
        }

        public bool? GetBooleanClaim(string type, string issuer = null)
        {
            var value = GetClaim(type: type, issuer: issuer);

            if (value == null) return null;

            return value == bool.TrueString;
        }

        public string GetClaim(string type, string issuer = null)
        {
            return (issuer == null ? FindAll(type) : FindAll(c => c.Type == type && c.Issuer == issuer))
                .Select(c => c.Value)
                .FirstOrDefault();
        }

        public DateTime? GetDateTimeClaim(string type, string issuer = null)
        {
            var value = GetClaim(type: type, issuer: issuer);

            if (value == null) return null;

            DateTime dt;

            if (DateTime.TryParseExact(value, new[] { Defaults.ShortDateTimeFormat }, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal, out dt)) return dt;

            if (DateTime.TryParse(value, out dt)) return dt;

            return null;
        }

        public IEnumerable<string> GetClaims(string type, string issuer = null)
        {
            return (issuer == null ? FindAll(type) : FindAll(c => c.Type == type && c.Issuer == issuer)).Select(c => c.Value);
        }

        public int? GetInteger32Claim(string type, string issuer = null)
        {
            var value = GetClaim(type: type, issuer: issuer);

            if (value == null) return null;

            int result;

            return int.TryParse(value, out result) ? result : (int?)null;
        }

        public IEnumerable<T> GetJsonClaims<T>(string type, string issuer = null)
        {
            return (issuer == null ? FindAll(type) : FindAll(c => c.Type == type && c.Issuer == issuer)).Select(c => JsonConvert.DeserializeObject<T>(c.Value));
        }

        public AccessLevels QueryAccessLevel()
        {
            var level = AccessLevels.Public;

            var issuers = FindAll(c => c.Type == NameClaimType).Select(c => c.Issuer).ToArray();

            foreach (var issuer in issuers)
            {
                AccessLevels value;

                if (Enum.TryParse(issuer, out value) && value == (value & AccessLevels.Social))
                {
                    level |= value;
                }
            }

            if (issuers.Contains(Defaults.ClaimIssuer))
            {
                level |= AccessLevels.Protected;

                if (!string.IsNullOrEmpty(CompanyCode) && !string.IsNullOrEmpty(SegmentCode))
                {
                    level |= AccessLevels.Private;

                    if (TwoFactorCompleted)
                    {
                        level |= AccessLevels.Secured;
                    }
                }
            }

            return level;
        }

        public void RejectChanges()
        {
            // TODO: also remove added elements
            Update(_data);
        }

        public bool RemoveAccountInfo(AccountInfo account)
        {
            var result = false;
            var claims = FindAll(c => c.Type == AccountInfoClaimType);

            foreach (var claim in claims)
            {
                var value = JsonConvert.DeserializeObject<AccountInfo>(claim.Value);

                if (value.CompanyCode == account.CompanyCode && value.SegmentCode == account.SegmentCode)
                {
                    result |= TryRemoveClaim(claim);
                }
            }

            return result;
        }

        public bool RemoveLoginInfo(UserLoginInfo login)
        {
            var result = false;
            var claims = FindAll(c => c.Type == LoginInfoClaimType);

            foreach (var claim in claims)
            {
                var value = JsonConvert.DeserializeObject<LoginInfo>(claim.Value);

                if (value.LoginProvider == login.LoginProvider && value.LoginKey == login.ProviderKey)
                {
                    result |= TryRemoveClaim(claim);
                }
            }

            return result;
        }

        public bool RemoveRole(string role)
        {
            return FindAll(c => c.Type == RoleClaimType && c.Value == role).ToList().All(TryRemoveClaim);
        }

        public Claim SetClaim(string type, string value, string valueType = null, string issuer = null)
        {
            Clear(type, issuer);

            if (value == null) return null;

            return AddClaimUnique(type: type, value: value, valueType: valueType, issuer: issuer);
        }

        public Claim SetClaim(string type, bool? value, string issuer = null)
        {
            Clear(type, issuer);

            if (!value.HasValue) return null;

            return AddClaimUnique(type: type, value: value.Value ? bool.TrueString : bool.FalseString, valueType: ClaimValueTypes.Boolean, issuer: issuer);
        }

        public Claim SetClaim(string type, DateTime? input, string issuer = null)
        {
            Clear(type, issuer);

            if (!input.HasValue) return null;

            var value = input.Value.ToString(Defaults.ShortDateTimeFormat);

            return AddClaimUnique(type: type, value: value, valueType: ClaimValueTypes.DateTime, issuer: issuer);
        }

        public Claim SetClaim(string type, int? input, string issuer = null)
        {
            Clear(type, issuer);

            if (!input.HasValue) return null;

            string value = Convert.ToString(input.Value);

            return AddClaimUnique(type: type, value: value, valueType: ClaimValueTypes.Integer32, issuer: issuer);
        }

        public void Update(UserData data, bool partial = false)
        {
            _data = data;

            data.Update(this);

            if (partial)
            {
                _data = this;
            }
        }

        #endregion - helpers -

        public static implicit operator UserIdentity(ClaimsPrincipal principal)
        {
            return new UserIdentity(principal);
        }

        public static implicit operator UserIdentity(AuthenticateResult result)
        {
            // TODO: result.Properties;
            return new UserIdentity(result.Identity);
        }

        public static implicit operator UserIdentity(List<Claim> claims)
        {
            return new UserIdentity(claims);
        }

        public static implicit operator List<Claim>(UserIdentity user)
        {
            return user.Claims.ToList();
        }

        public static implicit operator UserData(UserIdentity identity)
        {

            if (identity == null) return null;

            var data = new UserData();

            ClaimExtensions.Update(identity, data);

            return data;
        }

        public static implicit operator UserIdentity(UserData data)
        {
            if (data == null) return null;

            var identity = new UserIdentity(data.Name, DefaultAuthenticationTypes.ApplicationCookie);

            identity.Update(data);
            
            return identity;
        }
    }
}
