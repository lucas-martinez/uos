﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Contracts
{
    using Constants;
    using Models;
    using Results;

    public interface IAuthenticationContext : IDisposable
    {
        string BearerToken { get; }

        DomainInfo CurrentDomain { get; }
        
        SessionInfo CurrentSession { get; set; }

        string IpAddress { get; }

        IAuthenticationManager Manager { get; }

        IOwinContext OwinContext { get; }

        ISessionStore Sessions { get; }

        SignInManager<SessionInfo, string> SignInManager { get; }

        Uri Url { get; }

        Uri UrlReferrer { get; }

        UserManager<SessionInfo, string> UserManager { get; }

        AuthorizeStatus Authorize(string feature, string activity = null, AccessLevels level = AccessLevels.Private, ICollection<string> roles = null);

        UserManager<SessionInfo, string> CreateUserManager();
        
        Task<DomainInfo> FindCurrentDomainAsync();

        void SignIn(SessionInfo session);

        void SignOut();
    }
}
