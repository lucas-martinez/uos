﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Contracts
{
    using Constants;

    [DataContract]
    public class DomainInfo
    {
        public static DomainInfo LocalHost()
        {
            return new DomainInfo
            {
                DomainName = "localhost",
                Environment = Environments.Development
            };
        }

        ICollection<ConsumerInfo> _consumers;
        ICollection<FeatureInfo> _features;

        [DataMember(Name = "consumers", Order = 7)]
        public ICollection<ConsumerInfo> Consumers
        {
            get { return _consumers ?? (_consumers = new List<ConsumerInfo>()); }
            set { _consumers = value; }
        }

        [DataMember(Name = "companyCode", Order = 1)]
        public string CompanyCode { get; set; }

        [DataMember(Name = "companyName", Order = 2)]
        public string CompanyName { get; set; }

        [DataMember(Name = "domainName", Order = 0)]
        public string DomainName { get; set; }

        [DataMember(Name = "environment", Order = 4)]
        public string Environment { get; set; }

        [DataMember(Name = "expiresUtc", Order = 5)]
        public DateTime ExpiresUtc { get; set; }

        [DataMember(Name = "features", Order = 6)]
        public ICollection<FeatureInfo> Features
        {
            get { return _features ?? (_features = new List<FeatureInfo>()); }
            set { _features = value; }
        }

        [DataMember(Name = "instanceId")]
        public Guid? Identity { get; set; }

        [DataMember(Name = "ipAddress")]
        public string IpAddress { get; set; }

        [DataMember(Name = "machineName")]
        public string MachineName { get; set; }

        [DataMember(Name = "processId")]
        public int? ProcessId { get; set; }

        [DataMember(Name = "productName", Order = 3)]
        public string ProductName { get; set; }

        [DataMember(Name = "productVersion")]
        public string ProductVersion { get; set; }

        public bool Refresh(string featureName, string activityName, ref AccessLevels level, ref ICollection<string> roles)
        {
            if (featureName == null || activityName == null) return false;

            var feature = Features.SingleOrDefault(o => o.Name == featureName);

            if (feature == null) return false;

            var activity = feature.Activities.SingleOrDefault(o => o.Name == activityName);

            if (activity == null) return false;

            level = activity.AccessLevel;

            roles = activity.Roles;

            return true;
        }
    }
}
