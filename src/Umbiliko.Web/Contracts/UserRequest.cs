﻿using System.Runtime.Serialization;

namespace Uos.Web.Contracts
{
    using Constants;

    [DataContract]
    public class UserRequest
    {
        public UserRequest()
        {
        }

        public UserRequest(UserInfo user, string action = Actions.Update)
        {
            Action = action;
            Data = user;
        }

        [DataMember(Name = "action")]
        public string Action { get; set; }

        [DataMember(Name = "data")]
        public UserInfo Data { get; set; }

        public static implicit operator UserRequest(UserInfo data)
        {
            return new UserRequest(data);
        }
    }
}
