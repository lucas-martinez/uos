﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Contracts
{
    public class Location
    {
        public virtual string AddressLine { get; set; }

        public virtual string AddressLine2 { get; set; }

        public virtual Country Country { get; set; }

        public virtual string CountryCode { get; set; }

        public virtual decimal? Latitude { get; set; }

        public virtual Guid LocationId { get; set; } = Guid.NewGuid();

        public virtual string LocationCode { get; set; }

        public virtual string LocationName { get; set; }

        public virtual decimal? Longitude { get; set; }

        public virtual string PostalCode { get; set; }

        public virtual Region Region { get; set; }

        public virtual Guid? RegionId { get; set; }
    }
}
