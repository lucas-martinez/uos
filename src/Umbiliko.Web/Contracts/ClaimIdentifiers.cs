﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Umbiliko.Web.Contracts
{
    public enum ClaimIdentifiers : short
    {
        Anonymous = 1,

        #region - Name -

        UserName = ClaimCategories.Name << 8,

        FirstName = ClaimCategories.Name << 8 | 1,

        LastName = ClaimCategories.Name << 8 | 2,

        MiddleName = ClaimCategories.Name << 8 | 3,

        #endregion - Name -

        #region - PhoneNumber -

        MobilePhoneNumber = ClaimCategories.PhoneNumber << 8,

        HomePhoneNumber = ClaimCategories.PhoneNumber << 8 | 1,

        OfficePhoneNumber = ClaimCategories.PhoneNumber << 8 | 2,

        OtherPhoneNumber = ClaimCategories.PhoneNumber << 8 | 3,

        #endregion - PhoneNumber -

        #region - EmailAddress -

        UserEmailAddress = ClaimCategories.EmailAddress << 8,

        AccountEmailAddress = ClaimCategories.EmailAddress << 8 | 1,

        OtherEmailAddress = ClaimCategories.EmailAddress << 8 | 2,

        #endregion - EmailAddress -

        #region - Role -

        AccountRole = ClaimCategories.Role

        #endregion - Role -
    }
}
