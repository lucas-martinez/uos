﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Contracts
{
    public class EmailNotificationRequest : NotificationRequest
    {
        public string EmailAddress { get; set; }
    }
}
