﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Contracts
{
    using Constants;
    using System.Globalization;

    public struct PasswordStrength : IComparable<PasswordStrength>
    {
        const byte StrongMinimumPoints = 7;

        bool _digit;
        byte _length;
        bool _lowercase;
        bool _symbol;
        bool _uppercase;

        /// <summary>
        /// Password is empty
        /// </summary>
        public bool Empty
        {
            get { return Length == 0; }
        }

        /// <summary>
        /// Password has one or more digits.
        /// </summary>
        public bool Digit
        {
            get { return _digit && _length != 0; }
            set { _digit = value; }
        }

        /// <summary>
        /// Password length is fuzzy logic.
        /// </summary>
        public byte Length
        {
            get { return Letter || NonLetterOrDigit ? _length : (byte)0; }
            set { _length = value; }
        }

        /// <summary>
        /// Password has one or more letters.
        /// </summary>
        public bool Letter
        {
            get { return _lowercase || _uppercase; }
        }

        /// <summary>
        /// Password has one or more lowercase letters.
        /// </summary>
        public bool Lowercase
        {
            get { return _lowercase && _length != 0; }
            set { _lowercase = value; }
        }

        /// <summary>
        /// Password has one or more digit or symbol.
        /// </summary>
        public bool NonLetterOrDigit
        {
            get { return _symbol || _digit; }
        }

        /// <summary>
        /// Password strength points. Max value is 31. Values [0..31] 
        /// </summary>
        public byte Points
        {
            get
            {
                int points = _length;

                if (Lowercase && Uppercase) points += 1;

                if (Uppercase) points += Lowercase ? 2 : 1;

                if (Digit) points += 4;

                if (Symbol) points += 8;

                return (byte)points;
            }
        }

        /// <summary>
        /// Password is strong.
        /// </summary>
        public bool Strong
        {
            get { return !Weak; }
        }

        /// <summary>
        /// Password has one or more symbols.
        /// </summary>
        public bool Symbol
        {
            get { return _symbol && _length != 0; }
            set { _symbol = value; }
        }

        /// <summary>
        /// Password has one or more uppercase letters.
        /// </summary>
        public bool Uppercase
        {
            get { return _uppercase && _length != 0; }
            set { _uppercase = value; }
        }

        /// <summary>
        /// Password is weak.
        /// </summary>
        public bool Weak
        {
            get { return Points < StrongMinimumPoints; }
        }

        public int CompareTo(PasswordStrength other)
        {
            return Points.CompareTo(other.Points);
        }

        public bool Equals(PasswordStrength value)
        {
            return Equals((string)value);
        }

        public bool Equals(string value)
        {
            return string.Equals(this, value, StringComparison.Ordinal);
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            if (obj is PasswordStrength)
            {
                return Equals((PasswordStrength)obj);
            }

            if (obj is string)
            {
                return Equals((string)obj);
            }

            return false;
        }

        public override int GetHashCode()
        {
            string value = this;
            return value.GetHashCode();
        }

        public static PasswordStrength FromPassword(string password)
        {
            return string.IsNullOrEmpty(password)
                ? new PasswordStrength
                {
                    Digit = false,
                    Length = 0,
                    Lowercase = false,
                    Uppercase = false,
                    Symbol = false
                }
                : new PasswordStrength
                {
                    Digit = password.All(IsDigit),
                    Length = (byte)Math.Min(password.Length, 16),
                    Lowercase = password.All(IsLower),
                    Uppercase = password.All(IsUpper),
                    Symbol = password.All(IsSymbol)
                };
        }

        public static bool IsDigit(char c)
        {
            return ((c >= '0') && (c <= '9'));
        }

        public static bool IsSymbol(char c)
        {
            return (!IsUpper(c) && !IsLower(c) && !IsDigit(c));
        }

        public static bool IsLower(char c)
        {
            return ((c >= 'a') && (c <= 'z'));
        }

        public static bool IsUpper(char c)
        {
            return ((c >= 'A') && (c <= 'Z'));
        }

        private static bool IsSet(string s, int position, char value)
        {
            return (s != null && s.Length > position && s[position] == value);
        }

        private static byte ParseLength(string s)
        {
            if (s == null || s.Length == 0) return 0;

            var val = s.Substring(0, 1);

            if (val == "L") return PasswordLengths.Large;

            byte len;
            if (byte.TryParse(val, NumberStyles.HexNumber, CultureInfo.InvariantCulture, out len)) return len;

            return 0;
        }

        public static implicit operator PasswordStrength(string value)
        {
            return new PasswordStrength
            {
                Digit = IsSet(value, 3, '1'),
                Length = ParseLength(value),
                Lowercase = IsSet(value, 1, 'y'),
                Symbol = IsSet(value, 4, '+'),
                Uppercase = IsSet(value, 2, 'Y')
            };
        }

        public static implicit operator string (PasswordStrength value)
        {
            return string.Concat(
                value.Length == PasswordLengths.Large ? "L" : value.Length.ToString("X"),
                value.Lowercase ? 'y' : 'n',
                value.Uppercase ? 'Y' : 'N',
                value.Digit ? '1' : '0',
                value.Symbol ? '+' : '-');
        }

        public static bool operator ==(PasswordStrength left, PasswordStrength right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(PasswordStrength left, PasswordStrength right)
        {
            return !left.Equals(right);
        }
    }
}
