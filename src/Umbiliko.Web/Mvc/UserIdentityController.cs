﻿using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Uos.Web.Mvc
{
    using Contracts;

    public abstract class UserIdentityController : Mvc.CustomController
    {
        IAuthenticationContext _authentication;
        
        public virtual IAuthenticationContext AuthenticationContext
        {
            get { return _authentication ?? (_authentication = DependencyResolver.Current.GetService<IAuthenticationContext>()); }
        }

        public virtual UserInfo UserIdentity
        {
            get { return AuthenticationContext.CurrentSession.User; }
        }

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);

            // Grab the user's login information from Identity
        }
    }
}
