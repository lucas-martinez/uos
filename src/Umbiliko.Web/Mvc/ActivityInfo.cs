﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;

namespace Uos.Web.Mvc
{   
    using Utilities;
    using Results;

    public class ActivityInfo
    {
        readonly string _action;
        readonly string _area;
        readonly string _controller;
        readonly string _feature;
        readonly bool _local;
        readonly string _name;
        readonly Uri _uri;
        
        private ActivityInfo(string name = null, string feature = null, string action = null, string controller = null, string area = null, Uri uri = null, bool local = true)
        {
            _action = action;
            _area = area;
            _controller = controller;
            _feature = feature;
            _local = local;
            _name = name;
            _uri = uri;
        }

        public string Action
        {
            get { return _action; }
        }

        public string Area
        {
            get { return _area; }
        }

        public string Controller
        {
            get { return _controller; }
        }

        public string Feature
        {
            get { return _feature; }
        }

        public string Name
        {
            get { return _name; }
        }

        public string Url
        {
            get { return _uri != null ? (_local ? _uri.PathAndQuery : Convert.ToString(_uri)) : null; }
        }

        public Uri Uri
        {
            get { return _uri; }
        }

        public static ActivityInfo FromAction<TController>(Expression<Func<TController, ActionResult>> action, UrlHelper urlHelper)
            where TController : Controller
        {
            var member = action.GetMember();
            return member.MemberType == MemberTypes.Method ? FromMethod(typeof(TController), (MethodInfo)member, urlHelper) : Void();
        }

        public static ActivityInfo FromAction<TController, TResult>(Expression<Func<TController, TResult>> action, UrlHelper urlHelper)
            where TController : Controller
            where TResult : ActivityResult
        {
            var member = action.GetMember();
            return member.MemberType == MemberTypes.Method ? FromMethod(typeof(TController), (MethodInfo)member, urlHelper) : Void();
        }

        public static ActivityInfo FromAction<TController>(Expression<Func<TController, Task<ActionResult>>> action, UrlHelper urlHelper)
            where TController : Controller
        {
            var member = action.GetMember();
            return member.MemberType == MemberTypes.Method ? FromMethod(typeof(TController), (MethodInfo)member, urlHelper) : Void();
        }
        public static ActivityInfo FromAction<TController, TResult>(Expression<Func<TController, Task<TResult>>> action, UrlHelper urlHelper)
            where TController : Controller
            where TResult : ActivityResult
        {
            var member = action.GetMember();
            return member.MemberType == MemberTypes.Method ? FromMethod(typeof(TController), (MethodInfo)member, urlHelper) : Void();
        }

        private static ActivityInfo FromMethod(Type type, MethodInfo method, UrlHelper urlHelper)
        {
            var action = method.Name;
            var area = method.DeclaringType.GetCustomAttributes<RouteAreaAttribute>().Select(attr => attr.AreaName).SingleOrDefault();
            var controller = type.Name.TrimEnd("Controller");
            var feature = method.GetCustomAttributes<ActivityAttribute>().Select(attr => attr.Feature).SingleOrDefault() ??
                method.DeclaringType.GetCustomAttributes<FeatureAttribute>().Select(attr => attr.Name).SingleOrDefault();
            var name = method.GetCustomAttributes<ActivityAttribute>().Select(attr => attr.Name).SingleOrDefault();
            var baseUri = new Uri(urlHelper.RequestContext.HttpContext.Request.Url.GetLeftPart(UriPartial.Scheme | UriPartial.Authority), UriKind.Absolute);
            var path = urlHelper.Action(
                actionName: action,
                controllerName: controller,
                routeValues: new RouteValueDictionary(new Dictionary<string, object> { { "area", area } }));

            return new ActivityInfo(
                action: action,
                area: area,
                controller: controller,
                feature: feature,
                local: urlHelper.IsLocalUrl(Convert.ToString(urlHelper.RequestContext.HttpContext.Request.UrlReferrer)),
                name: name,
                uri: new Uri(baseUri, path));
        }

        public static ActivityInfo Void()
        {
            return new ActivityInfo();
        }
    }
}
