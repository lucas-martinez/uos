﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Uos.Web.Constants;

namespace Uos.Web.Mvc
{
    using Contracts;
    using Providers;

    public abstract class DataController : Controller
    {
        protected new ContentResult Json(object result)
        {
            return Content(JsonConvert.SerializeObject(result, settings: Defaults.JsonSerializerSettings), "application/json");
        }
    }
}
