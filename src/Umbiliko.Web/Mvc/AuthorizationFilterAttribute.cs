﻿using Microsoft.Owin;
using System;
using System.Web;
using System.Web.Mvc;

namespace Uos.Web.Mvc
{
    using Contracts;

    public abstract class AuthorizationFilterAttribute : FilterAttribute, IAuthorizationFilter
    {
        private readonly object _typeId = new object();
        ActionDescriptor _action;
        IAuthenticationContext _authentication;
        AuthorizationContext _authorization;
        ControllerBase _controller;
        IOwinContext _owinContext;

        protected ActionDescriptor Action
        {
            get { return _action; }
        }

        protected IAuthenticationContext Authentication
        {
            get { return _authentication; }
        }

        protected AuthorizationContext Authorization
        {
            get { return _authorization; }
        }

        protected ControllerBase Controller
        {
            get { return _controller; }
        }

        public override object TypeId
        {
            get { return _typeId; }
        }

        private void CacheValidateHandler(HttpContext context, object data, ref HttpValidationStatus validationStatus)
        {
            validationStatus = OnCacheAuthorization(new HttpContextWrapper(context));
        }

        protected virtual void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new HttpUnauthorizedResult();
        }

        protected abstract bool AuthorizeCore(HttpContextBase httpContext);

        protected virtual HttpValidationStatus OnCacheAuthorization(HttpContextBase httpContext)
        {
            if (httpContext == null)
            {
                throw new ArgumentNullException("httpContext");
            }

            if (!AuthorizeCore(httpContext))
            {
                return HttpValidationStatus.IgnoreThisRequest;
            }

            return HttpValidationStatus.Valid;
        }

        void IAuthorizationFilter.OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext == null)
            {
                throw new ArgumentNullException("filterContext");
            }

            if (OutputCacheAttribute.IsChildActionCacheActive(filterContext))
            {
                throw new InvalidOperationException("MvcResources.AuthorizeAttribute_CannotUseWithinChildActionCache");
            }

            _action = filterContext.ActionDescriptor;
            _controller = filterContext.Controller;
            _authentication = ServicesContainer.Default.GetInstance<IAuthenticationContext>();
            _authorization = filterContext;

            if (!filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true) && !filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true))
            {
                if (AuthorizeCore(filterContext.HttpContext))
                {
                    HttpCachePolicyBase cache = filterContext.HttpContext.Response.Cache;

                    cache.SetProxyMaxAge(new TimeSpan(0L));

                    cache.AddValidationCallback(new HttpCacheValidateHandler(CacheValidateHandler), null);
                }
                else
                {
                    HandleUnauthorizedRequest(filterContext);
                }
            }
        }
    }
}
