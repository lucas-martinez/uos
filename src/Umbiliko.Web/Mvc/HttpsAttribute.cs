﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Uos.Web.Mvc
{
    public class HttpsAttribute : AuthorizationFilterAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext.Request.Url.Scheme != Uri.UriSchemeHttps)
            {
                Authorization.Result = new HttpUnauthorizedResult("HTTPS Required");

                return false;
            }

            return true;
        }
    }
}