﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Uos.Web.Mvc
{
    using Controllers;

    public sealed class ProxyActionSet : UmbilikoActionSet<ProxyController>
    {
        private ProxyActionSet(UrlHelper urlHelper)
            : base(urlHelper)
        {
        }

        public static implicit operator ProxyActionSet(UrlHelper urlHelper)
        {
            return new ProxyActionSet(urlHelper);
        }
    }
}
