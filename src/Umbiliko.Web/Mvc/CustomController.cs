﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Helpers;
using System.Web.Mvc;

namespace Uos.Web.Mvc
{
    public abstract class CustomController : DataController
    {
        protected void XFrame(string value = "ALLOW")
        {
            AntiForgeryConfig.SuppressXFrameOptionsHeader = true;
            Response.Headers.Remove("X-Frame-Options");
            //Response.Headers.Add("X-Frame-Options", value);
        }
    }
}
