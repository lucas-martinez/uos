﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Uos.Web.Mvc
{
    using Controllers;
    using Results;

    public class UmbilikoActionSet<TController>
        where TController : UmbilikoController
    {
        ActivityInfo _claim;
        ActivityInfo _domain;
        ActivityInfo _enter;
        ActivityInfo _exit;
        ActivityInfo _jump;
        ActivityInfo _register;
        ActivityInfo _reset;
        ActivityInfo _switch;
        ActivityInfo _token;
        ActivityInfo _user;
        UrlHelper _urlHelper;
        
        protected UmbilikoActionSet(UrlHelper urlHelper)
        {
            _urlHelper = urlHelper;
        }
        
        /// <summary>
        /// Forgot password
        /// </summary>
        public ActivityInfo AccountClaim
        {
            get { return _claim ?? (_claim = ActivityInfo.FromAction<TController, ActivityResult>(controller => controller.AccountClaim(null), this)); }
        }

        /// <summary>
        /// Sign-in
        /// </summary>
        public ActivityInfo AccountEnter
        {
            get { return _enter ?? (_enter = ActivityInfo.FromAction<TController, SessionResult>(controller => controller.AccountEnter(null), this)); }
        }

        /// <summary>
        /// Sign-out
        /// </summary>
        public ActivityInfo AccountExit
        {
            get { return _exit ?? (_exit = ActivityInfo.FromAction<TController, SessionResult>(controller => controller.AccountExit(null), this)); }
        }

        /// <summary>
        /// Sign-up
        /// </summary>
        public ActivityInfo AccountRegister
        {
            get { return _register ?? (_register = ActivityInfo.FromAction<TController, SessionResult>(controller => controller.AccountRegister(null), this)); }
        }

        /// <summary>
        /// Reset password with token
        /// </summary>
        public ActivityInfo AccountReset
        {
            get { return _reset ?? (_reset = ActivityInfo.FromAction<TController, SessionResult>(controller => controller.AccountReset(null), this)); }
        }

        /// <summary>
        /// Switch to a different account in the same domain
        /// </summary>
        public ActivityInfo AccountSwitch
        {
            get { return _switch ?? (_switch = ActivityInfo.FromAction<TController, SessionResult>(controller => controller.AccountSwitch(null), this)); }
        }

        /// <summary>
        /// Get current domain info
        /// </summary>
        public ActivityInfo CurrentDomain
        {
            get { return _domain ?? (_domain = ActivityInfo.FromAction<TController, DomainResult>(controller => controller.CurrentDomain(), this)); }
        }

        /// <summary>
        /// Get current user token
        /// </summary>
        public ActivityInfo CurrentToken
        {
            get { return _token ?? (_token = ActivityInfo.FromAction<TController, ActivityResult<string>>(controller => controller.CurrentToken(), this)); }
        }

        /// <summary>
        /// Get current user info
        /// </summary>
        public ActivityInfo CurrentUser
        {
            get { return _user ?? (_user = ActivityInfo.FromAction<TController, UserResult>(controller => controller.CurrentUser(), this)); }
        }

        /// <summary>
        /// Jump to a different domain using current credentials
        /// </summary>
        public ActivityInfo DomainJump
        {
            get { return _jump ?? (_jump = ActivityInfo.FromAction<TController, SessionResult>(controller => controller.DomainJump(null), this)); }
        }

        public static implicit operator UmbilikoActionSet<TController>(UrlHelper urlHelper)
        {
            return new UmbilikoActionSet<TController>(urlHelper);
        }

        public static implicit operator UrlHelper(UmbilikoActionSet<TController> instance)
        {
            return instance != null ? instance._urlHelper : null;
        }
    }
}
