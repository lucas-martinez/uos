﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Extensions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Uos.Web.Mvc
{
    using Assertion;
    using Constants;
    using Contracts;
    using Models;
    using Providers;
    using Results;

    [RoutePrefix("umbiliko-proxy")]
    public abstract class ProxyController : Mvc.UmbilikoController
    {
        IAuthenticationContext _authentication;
        
        protected override IAuthenticationContext Authentication
        {
            get { return _authentication ?? (_authentication = HttpContext.GetOwinContext().Get<IAuthenticationContext>()); }
        }
        
        //
        // GET: /umbiliko-proxy/current-domain
        [AcceptVerbs(HttpVerbs.Get), Route("current-domain")]
        public override async Task<DomainResult> CurrentDomain()
        {
            var domain = await Authentication.FindCurrentDomainAsync();

            return DomainResult.Assert(domain);
        }

        //
        // GET: /umbiliko-proxy/current-token
        [HttpGet, Route("current-token")]
        public override ActivityResult<string> CurrentToken()
        {
            var session = Authentication.CurrentSession;

            return ActivityResult<string>.Assert(session != null ? session.AccessToken : null);
        }

        //
        // GET: /umbiliko-proxy/current-user
        [HttpGet, Route("current-user")]
        public override UserResult CurrentUser()
        {
            return UserResult.Assert(Authentication.CurrentSession.User);
        }
        
        private ActionResult Return(IResult result, UserInfo data, string returnUrl)
        {
            if (Request.AcceptTypes.Any(type => type.StartsWith("application/json", StringComparison.OrdinalIgnoreCase)))
            {
                return Json(new UserResult
                {
                    Success = result.Success,
                    Messages = result.Messages,
                    Value = data,
                    ReturnUrl = returnUrl
                });
            }
            else
            {
                return RedirectPermanent(returnUrl ?? Request.RawUrl);
            }
        }
    }
}
