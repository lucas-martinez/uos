﻿using System;
using System.Web;

namespace Uos.Web.Mvc
{
    using Constants;

    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public sealed class FeatureAttribute : AuthorizationFilterAttribute
    {
        string _activity;
        readonly string _feature;

        public FeatureAttribute(string name)
        {
            _feature = name;
        }

        public string Name
        {
            get { return _feature; }
        }
        public string Requires
        {
            get { return _activity; }
            set { _activity = value; }
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return Authentication.Authorize(activity: _activity, feature: _feature) == AuthorizeStatus.Success;
        }
    }
}
