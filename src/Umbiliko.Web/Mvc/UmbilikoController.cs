﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Uos.Web.Mvc
{
    using Constants;
    using Contracts;
    using Models;
    using Results;
    using Services;
    using Utilities;

    [RoutePrefix("")]
    public abstract class UmbilikoController : Controller
    {
        protected abstract IAuthenticationContext Authentication { get; }

        protected abstract DomainStoreBase Domains { get; }

        protected abstract ISessionStore Sessions { get; }

        protected abstract IUserStore Users { get; }

        //
        // POST: /{route-prefix}/account-claim
        [HttpPost, Route("account-claim")]
        public virtual async Task<ActivityResult> AccountClaim(AccountClaimRequest request)
        {
            throw new NotImplementedException();
        }

        //
        // POST: /{route-prefix}/account-enter
        [HttpPost, Route("account-enter")]
        public virtual async Task<SessionResult> AccountEnter(AccountEnterRequest request)
        {
            if (!ModelState.IsValid)
            {
                SessionResult.Fail(/*request, */ModelState.Values.SelectMany(value => value.Errors).Select(error => error.ErrorMessage).ToArray());
            }

            var result = await Sessions.CreateSessionAsync(request.Name, request.Password, Authentication.IpAddress, null, Authentication.UrlReferrer.Host);

            if (result.Success) Authentication.SignIn(result.Value);

            return result;
        }

        //
        // POST: /{route-prefix}/account-exit
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post), Route("account-exit")]
        public virtual async Task<SessionResult> AccountExit(AccountExitRequest request)
        {
            if (!ModelState.IsValid)
            {
                SessionResult.Fail(/*request, */ModelState.Values.SelectMany(value => value.Errors).Select(error => error.ErrorMessage).ToArray());
            }

            SessionInfo session = null;

            if (Request.UrlReferrer.Host == Request.Url.Host)
            {
                session = Authentication.CurrentSession;
            }
            else
            {
                var r = await Authentication.Sessions.FindSessionByIdAsync(request.SessionId);

                session = r.Value;
            }

            if (session == null) SessionResult.Fail("NotFound");

            var result = await Sessions.ExpireSessionAsync(session.Id, Authentication.OwinContext.Request.RemoteIpAddress, null, Authentication.UrlReferrer.Host);

            if (result.Success) Authentication.SignOut();

            return result;
        }

        //
        // POST: /{route-prefix}/account-register
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post), Route("account-register")]
        public virtual async Task<SessionResult> AccountRegister(AccountRegisterRequest request)
        {
            throw new NotImplementedException();
        }

        //
        // POST: /{route-prefix}/account-reset
        [HttpPost, Route("account-reset")]
        public virtual async Task<SessionResult> AccountReset(AccountResetRequest request)
        {
            throw new NotImplementedException();
        }

        //
        // POST: /{route-prefix}/account-switch
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post), Route("account-switch")]
        public virtual async Task<SessionResult> AccountSwitch(AccountSwitchRequest request)
        {
            throw new NotImplementedException();
        }

        //
        // GET: /{route-prefix}/current-domain
        [AcceptVerbs(HttpVerbs.Get), Route("current-domain")]
        public virtual async Task<DomainResult> CurrentDomain()
        {
            return DomainResult.Assert(await Authentication.FindCurrentDomainAsync());
        }

        //
        // GET: /{route-prefix}/current-token
        [HttpGet, Route("current-token")]
        public virtual ActivityResult<string> CurrentToken()
        {
            throw new NotImplementedException();
        }

        //
        // GET: /{route-prefix}/current-user
        [HttpGet, Route("current-user")]
        public virtual UserResult CurrentUser()
        {
            var session = Authentication.CurrentSession;

            return session != null ? session.User : UserResult.Fail("NoUser");
        }

        //
        // POST: /{route-prefix}/domain-jump
        [HttpPost, Route("domain-jump")]
        public virtual Task<SessionResult> DomainJump(DomainJumpRequest request)
        {
            throw new NotImplementedException();
        }

        protected virtual async Task<UserResult> FindUserAsync(string name)
        {
            var result = await Users.FindUserAsync(name);

            return result;
        }

        protected new ContentResult Json(object result)
        {
            return Content(JsonConvert.SerializeObject(result, settings: Defaults.JsonSerializerSettings), "application/json");
        }

        /*private ActionResult Return(IResult result, UserData data, string returnUrl)
        {
            if (Request.AcceptTypes.Any(type => type.StartsWith("application/json", StringComparison.OrdinalIgnoreCase)))
            {
                return Json(new UserDataResult
                {
                    Success = result.Success,
                    Messages = result.Messages,
                    Data = data,
                    ReturnUrl = returnUrl
                });
            }
            else
            {
                return RedirectPermanent(returnUrl ?? Request.RawUrl);
            }
        }*/
    }
}
