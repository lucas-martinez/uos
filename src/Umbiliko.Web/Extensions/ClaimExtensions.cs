﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web
{
    using Constants;
    using Contracts;
    using Microsoft.AspNet.Identity;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using System.Collections.ObjectModel;
    using System.Globalization;

    public static class ClaimExtensions
    {
        public static string GetClaimValue(this IEnumerable<Claim> source, string claimType)
        {
            return source.Where(claim => claim.Type == claimType).Select(claim => claim.Value).FirstOrDefault();
        }
        
        #region - helpers -

        public static Claim AddAccountInfo(this ClaimsIdentity identity, AccountInfo account, JsonSerializerSettings settings)
        {
            var claim = identity.FindAll(c => c.Type == UmbilikoClaimTypes.AccountInfo).FirstOrDefault(c =>
            {
                var value = JsonConvert.DeserializeObject<AccountInfo>(c.Value, settings);

                return value.CompanyCode == account.CompanyCode && value.SegmentCode == account.SegmentCode;
            });

            if (claim == null)
            {
                var value = JsonConvert.SerializeObject(account);

                AddClaim(identity, type: UmbilikoClaimTypes.AccountInfo, value: value, issuer: Defaults.IdentityProvider);
            }

            return claim;
        }

        public static Claim AddClaim(this ClaimsIdentity identity, string type, string value, string valueType = null, string issuer = null)
        {
            if (value == null) return null;

            if (valueType == null)
            {
                valueType = ClaimValueTypes.String;
            }

            var claim = new Claim(type: type, value: value, valueType: valueType, issuer: issuer ?? Defaults.IdentityProvider);

            identity.AddClaim(claim);

            return claim;
        }



        public static Claim AddClaimExact(this ClaimsIdentity identity, Claim claim)
        {
            return AddClaimExact(identity, type: claim.Type, value: claim.Value, valueType: claim.ValueType, issuer: claim.Issuer);
        }

        public static Claim AddClaimExact(this ClaimsIdentity identity, ClaimInfo claim)
        {
            return AddClaimExact(identity, type: claim.Type, value: claim.Value, valueType: claim.ValueType, issuer: claim.Issuer);
        }

        public static Claim AddClaimExact(this ClaimsIdentity identity, string type, string value, string valueType = null, string issuer = null)
        {
            if (value == null) return null;

            var claim = identity.FindFirst(c => c.Type == type && c.Value == value && c.Issuer == issuer);

            if (claim == null)
            {
                claim = AddClaim(identity, type: type, value: value, valueType: valueType, issuer: issuer);
            }
            else if (claim.ValueType != valueType && valueType != null && valueType != ClaimValueTypes.String)
            {
                identity.RemoveClaim(claim);
                claim = AddClaim(identity, type: type, value: value, valueType: valueType, issuer: issuer);
            }

            return claim;
        }

        public static Claim AddClaimExact(this ClaimsIdentity identity, string type, DateTime? input, string issuer = null)
        {
            if (!input.HasValue) return null;

            var value = input.Value.ToString(Defaults.ShortDateTimeFormat);

            var valueType = ClaimValueTypes.DateTime;

            var claim = identity.FindFirst(c => c.Type == type && c.Value == value && c.Issuer == issuer);

            if (claim == null)
            {
                claim = AddClaim(identity, type: type, value: value, valueType: valueType, issuer: issuer);
            }
            else if (claim.ValueType != valueType && valueType != null && valueType != ClaimValueTypes.String)
            {
                identity.RemoveClaim(claim);
                claim = AddClaim(identity, type: type, value: value, valueType: valueType, issuer: issuer);
            }

            return claim;
        }

        public static Claim AddClaimExact(this ClaimsIdentity identity, string type, int? input, string issuer = null)
        {
            if (!input.HasValue) return null;

            var value = Convert.ToString(input.Value);

            var valueType = ClaimValueTypes.Integer;

            var claim = identity.FindFirst(c => c.Type == type && c.Value == value && c.Issuer == issuer);

            if (claim == null)
            {
                claim = AddClaim(identity, type: type, value: value, valueType: valueType, issuer: issuer);
            }
            else if (claim.ValueType != valueType && valueType != null && valueType != ClaimValueTypes.String)
            {
                identity.RemoveClaim(claim);
                claim = AddClaim(identity, type: type, value: value, valueType: valueType, issuer: issuer);
            }

            return claim;
        }

        public static Claim AddClaimExact(this ClaimsIdentity identity, string type, bool? input, string issuer = null)
        {
            if (!input.HasValue) return null;

            var value = input.Value == true ? bool.TrueString : bool.FalseString;

            var valueType = ClaimValueTypes.Boolean;

            var claim = identity.FindFirst(c => c.Type == type && c.Value == value && c.Issuer == issuer);

            if (claim == null)
            {
                claim = AddClaim(identity, type: type, value: value, valueType: valueType, issuer: issuer);
            }
            else if (claim.ValueType != valueType && valueType != null && valueType != ClaimValueTypes.String)
            {
                identity.RemoveClaim(claim);
                claim = AddClaim(identity, type: type, value: value, valueType: valueType, issuer: issuer);
            }

            return claim;
        }

        public static Claim AddClaimUnique(this ClaimsIdentity identity, string type, string value, string valueType = null, string issuer = null)
        {
            var claim = identity.FindFirst(c => c.Type == type && c.Issuer == issuer);

            if (claim == null)
            {
                claim = AddClaim(identity, type: type, value: value, valueType: valueType, issuer: issuer);
            }
            else if (claim.Value != value || (claim.ValueType != valueType && valueType != null && valueType != ClaimValueTypes.String))
            {
                identity.RemoveClaim(claim);
                claim = AddClaim(identity, type: type, value: value, valueType: valueType, issuer: issuer);
            }

            return claim;
        }

        public static Claim AddLoginInfo(this ClaimsIdentity identity, LoginInfo login, JsonSerializerSettings settings)
        {
            if (login.LoginProvider == null || login.LoginKey == null) return null;

            var claim = identity.FindAll(c => c.Type == UmbilikoClaimTypes.LoginInfo).FirstOrDefault(c =>
            {
                var value = JsonConvert.DeserializeObject<LoginInfo>(c.Value, settings);

                return value.LoginProvider == login.LoginProvider && value.LoginKey == login.LoginKey;
            });

            if (claim == null)
            {
                var value = JsonConvert.SerializeObject(login);

                AddClaim(identity, type: UmbilikoClaimTypes.LoginInfo, value: value, issuer: Defaults.IdentityProvider);
            }

            return claim;
        }

        public static void AddRole(this ClaimsIdentity identity, string role)
        {
            AddClaim(identity, type: identity.RoleClaimType, value: role, issuer: Defaults.IdentityProvider);
        }

        public static void Clear(this ClaimsIdentity identity, string type, string issuer = null)
        {
            var claims = (issuer == null ? identity.FindAll(type) : identity.FindAll(c => c.Type == type && c.Issuer == issuer)).ToList();

            foreach (var claim in claims)
            {
                identity.RemoveClaim(claim);
            }
        }

        public static IEnumerable<Claim> FilterClaims(this ClaimsIdentity identity, params string[] excludeTypes)
        {
            return identity.Claims.Where(c => !excludeTypes.Contains(c.Type));
        }

        public static bool? GetBooleanClaim(this ClaimsIdentity identity, string type, string issuer = null)
        {
            var value = GetClaim(identity, type: type, issuer: issuer);

            if (value == null) return null;

            return value == bool.TrueString;
        }

        public static string GetClaim(this ClaimsIdentity identity, string type, string issuer = null)
        {
            return (issuer == null ? identity.FindAll(type) : identity.FindAll(c => c.Type == type && c.Issuer == issuer))
                .Select(c => c.Value)
                .FirstOrDefault();
        }

        public static DateTime? GetDateTimeClaim(this ClaimsIdentity identity, string type, string issuer = null)
        {
            var value = GetClaim(identity, type: type, issuer: issuer);

            if (value == null) return null;

            DateTime dt;

            if (DateTime.TryParseExact(value, new[] { Defaults.ShortDateTimeFormat }, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal, out dt)) return dt;

            if (DateTime.TryParse(value, out dt)) return dt;

            return null;
        }

        public static IEnumerable<string> GetClaims(this ClaimsIdentity identity, string type, string issuer = null)
        {
            return (issuer == null ? identity.FindAll(type) : identity.FindAll(c => c.Type == type && c.Issuer == issuer)).Select(c => c.Value);
        }

        public static Guid? GetGuidClaim(this ClaimsIdentity identity, string type, string issuer = null)
        {
            var value = GetClaim(identity, type: type, issuer: issuer);

            if (value == null) return null;

            Guid result;

            return Guid.TryParse(value, out result) ? result : (Guid?)null;
        }

        public static int? GetIntegerClaim(this ClaimsIdentity identity, string type, string issuer = null)
        {
            var value = GetClaim(identity, type: type, issuer: issuer);

            if (value == null) return null;

            int result;

            return int.TryParse(value, out result) ? result : (int?)null;
        }

        public static IEnumerable<T> GetJsonClaims<T>(this ClaimsIdentity identity, string type, JsonSerializerSettings settings, string issuer = null)
        {
            var claims = (issuer == null ? identity.FindAll(type) : identity.FindAll(c => c.Type == type && c.Issuer == issuer)).ToList();

            if (claims.Count == 0) return new T[0];

            Func<Claim, T> selector = (claim) =>
            {
                var value =  JsonConvert.DeserializeObject<T>(claim.Value, settings);

                return value;
            };

            return claims.Select(selector);
        }

        public static void MergeClaims(ICollection<ClaimInfo> list, IEnumerable<ClaimInfo> source)
        {
            foreach (var item in source)
            {
                var claim = list.FirstOrDefault(c => c.Type == item.Type && c.Value == item.Value && c.Issuer == item.Issuer);

                if (claim == null)
                {
                    list.Add(item);
                }
                else if (item.ValueType != null && item.ValueType != ClaimValueTypes.String && item.ValueType != claim.ValueType)
                {
                    claim.ValueType = item.ValueType;
                }
            }
        }

        public static AccessLevels QueryAccessLevel(this ClaimsIdentity identity)
        {
            var level = AccessLevels.Public;

            var issuers = identity.FindAll(c => c.Type == identity.NameClaimType).Select(c => c.Issuer).ToArray();

            foreach (var issuer in issuers)
            {
                AccessLevels value;

                if (Enum.TryParse(issuer, out value) && value == (value & AccessLevels.Social))
                {
                    level |= value;
                }
            }

            if (issuers.Contains(Defaults.IdentityProvider))
            {
                level |= AccessLevels.Protected;

                var companyCode = GetClaim(identity, type: UmbilikoClaimTypes.CompanyCode, issuer: Defaults.IdentityProvider);

                var segmentCode = GetClaim(identity, type: UmbilikoClaimTypes.SegmentCode, issuer: Defaults.IdentityProvider);

                if (!string.IsNullOrEmpty(companyCode) && !string.IsNullOrEmpty(segmentCode))
                {
                    level |= AccessLevels.Private;

                    var twoFactorCompleted = GetBooleanClaim(identity, UmbilikoClaimTypes.TwoFactorConfirmed, Defaults.IdentityProvider) ?? false;

                    if (twoFactorCompleted)
                    {
                        level |= AccessLevels.Secured;
                    }
                }
            }

            return level;
        }

        public static bool RemoveAccountInfo(this ClaimsIdentity identity, AccountInfo account)
        {
            var result = false;
            var claims = identity.FindAll(c => c.Type == UmbilikoClaimTypes.AccountInfo);

            foreach (var claim in claims)
            {
                var value = JsonConvert.DeserializeObject<AccountInfo>(claim.Value);

                if (value.CompanyCode == account.CompanyCode && value.SegmentCode == account.SegmentCode)
                {
                    result |= identity.TryRemoveClaim(claim);
                }
            }

            return result;
        }

        public static bool RemoveLoginInfo(this ClaimsIdentity identity, UserLoginInfo login)
        {
            var result = false;
            var claims = identity.FindAll(c => c.Type == UmbilikoClaimTypes.LoginInfo);

            foreach (var claim in claims)
            {
                var value = JsonConvert.DeserializeObject<LoginInfo>(claim.Value);

                if (value.LoginProvider == login.LoginProvider && value.LoginKey == login.ProviderKey)
                {
                    result |= identity.TryRemoveClaim(claim);
                }
            }

            return result;
        }

        public static bool RemoveRole(this ClaimsIdentity identity, string role)
        {
            var claims = identity.FindAll(c => c.Type == identity.RoleClaimType && c.Value == role).ToList();

            var any = false;

            foreach (var claim in claims)
            {
                any |= identity.TryRemoveClaim(claim);
            }

            return any;
        }

        public static Claim SetClaim(this ClaimsIdentity identity, string type, string value, string valueType = null, string issuer = null)
        {
            Clear(identity, type, issuer);

            if (value == null) return null;

            return AddClaimUnique(identity, type: type, value: value, valueType: valueType, issuer: issuer);
        }

        public static Claim SetClaim(this ClaimsIdentity identity, string type, bool? input, string issuer = null)
        {
            Clear(identity, type, issuer);

            if (!input.HasValue) return null;

            return AddClaimUnique(identity, type: type, value: input.Value ? bool.TrueString : bool.FalseString, valueType: ClaimValueTypes.Boolean, issuer: issuer);
        }

        public static Claim SetClaim(this ClaimsIdentity identity, string type, DateTime? input, string issuer = null)
        {
            Clear(identity, type, issuer);

            if (!input.HasValue) return null;

            var value = input.Value.ToString(Defaults.ShortDateTimeFormat);

            return AddClaimUnique(identity, type: type, value: value, valueType: ClaimValueTypes.DateTime, issuer: issuer);
        }

        public static Claim SetClaim(this ClaimsIdentity identity, string type, Guid? input, string issuer = null)
        {
            Clear(identity, type, issuer);

            if (!input.HasValue) return null;

            var value = Convert.ToString(input.Value);

            return AddClaimUnique(identity, type: type, value: value, valueType: ClaimValueTypes.DateTime, issuer: issuer);
        }

        public static Claim SetClaim(this ClaimsIdentity identity, string type, int? input, string issuer = null)
        {
            Clear(identity, type, issuer);

            if (!input.HasValue) return null;

            string value = Convert.ToString(input.Value);

            return AddClaimUnique(identity, type: type, value: value, valueType: ClaimValueTypes.Integer, issuer: issuer);
        }

        public static ClaimInfo Add(this ICollection<ClaimInfo> collection, string type, string value, string valueType, string issuer)
        {
            var claim = collection.FirstOrDefault(o => o.Type == type && o.Value == value && o.Issuer == issuer);

            if (claim == null)
            {
                claim = new ClaimInfo
                {
                    Issuer = issuer,
                    Type = type,
                    Value = value,
                    ValueType = valueType ?? ClaimValueTypes.String
                };
            }

            return claim;
        }

        public static ClaimInfo Add(this ICollection<ClaimInfo> collection, string type, DateTime? input, string issuer)
        {
            if (!input.HasValue) return null;

            var value = input.Value.ToString(Defaults.ShortDateTimeFormat);

            var claim = collection.FirstOrDefault(o => o.Type == type && o.Value == value && o.Issuer == issuer);

            if (claim == null)
            {
                claim = new ClaimInfo
                {
                    Issuer = issuer,
                    Type = type,
                    Value = value,
                    ValueType = ClaimValueTypes.DateTime
                };
            }

            return claim;
        }

        public static ClaimInfo Add(this ICollection<ClaimInfo> collection, string type, JToken input, string issuer)
        {
            string value = null;
            string valueType = ClaimValueTypes.String;

            switch (input.Type)
            {
                case JTokenType.Date:
                    value = input.Value<DateTime>().ToString(Defaults.ShortDateTimeFormat);
                    valueType = ClaimValueTypes.Date;
                    break;

                case JTokenType.Integer:
                    value = Convert.ToString(input.Value<int>());
                    valueType = ClaimValueTypes.Integer;
                    break;

                case JTokenType.String:
                    value = input.Value<string>();
                    break;
            }

            if (value == null) return null;

            var claim = collection.FirstOrDefault(o => o.Type == type && o.Value == value && o.Issuer == issuer);

            if (claim == null)
            {
                claim = new ClaimInfo
                {
                    Issuer = issuer,
                    Type = type,
                    Value = value,
                    ValueType = valueType
                };
            }
            else if (claim.ValueType != valueType)
            {
                claim.ValueType = valueType;
            }

            return claim;
        }

        public static ClaimInfo Set(this ICollection<ClaimInfo> collection, string type, string value, string issuer)
        {
            var claim = collection.FirstOrDefault(o => o.Type == type && o.Issuer == issuer);

            if (claim != null && value != null)
            {
                claim.Value = value;
            }
            else if (value != null)
            {
                claim = new ClaimInfo
                {
                    Issuer = issuer,
                    Type = type,
                    Value = value
                };
            }
            else if (claim != null)
            {
                collection.Remove(claim);
            }

            return claim;
        }

        public static ClaimInfo Set(this ICollection<ClaimInfo> collection, string type, DateTime? input, string issuer)
        {
            var value = input.HasValue ? input.Value.ToString(Defaults.ShortDateTimeFormat) : null;

            var claim = collection.FirstOrDefault(o => o.Type == type && o.Issuer == issuer);

            if (claim != null && value != null)
            {
                claim.Value = value;
            }
            else if (value != null)
            {
                claim = new ClaimInfo
                {
                    Issuer = issuer,
                    Type = type,
                    Value = value,
                    ValueType = ClaimValueTypes.DateTime
                };
            }
            else if (claim != null)
            {
                collection.Remove(claim);
            }

            return claim;
        }

        #endregion - helpers -

        public static AccountInfo Update(this ClaimsIdentity identity, AccountInfo account)
        {
            account.CompanyCode = GetClaim(identity, type: UmbilikoClaimTypes.CompanyCode, issuer: Defaults.IdentityProvider);

            account.CompanyName = GetClaim(identity, type: UmbilikoClaimTypes.CompanyName, issuer: Defaults.IdentityProvider);

            account.Roles = GetClaims(identity, type: identity.RoleClaimType, issuer: Defaults.IdentityProvider).ToList();

            account.SegmentCode = GetClaim(identity, type: UmbilikoClaimTypes.SegmentCode, issuer: Defaults.IdentityProvider);

            account.SegmentDescription = GetClaim(identity, type: UmbilikoClaimTypes.SegmentDescription, issuer: Defaults.IdentityProvider);

            return account;
        }

        public static SessionInfo Update(this ClaimsIdentity identity, SessionInfo session)
        {
            session.AccessToken = GetClaim(identity, type: UmbilikoClaimTypes.AccessToken, issuer: Defaults.IdentityProvider);
            
            session.DomainName = GetClaim(identity, type: UmbilikoClaimTypes.DomainName, issuer: Defaults.IdentityProvider);
            
            session.Id = GetGuidClaim(identity, type: UmbilikoClaimTypes.SessionId, issuer: Defaults.IdentityProvider) ?? Guid.Empty;

            identity.Update(session.User = new UserInfo());

            var account = new AccountInfo();

            identity.Update(account);

            if (account.CompanyCode != null)
            {
                session.Account = account;
            }

            return session;
        }

        public static UserInfo Update(this ClaimsIdentity identity, UserInfo user)
        {
            user.AccessFailedCount = GetIntegerClaim(identity, type: UmbilikoClaimTypes.AccessFailedCount, issuer: Defaults.IdentityProvider);
            
            user.Accounts = GetJsonClaims<AccountInfo>(identity, type: UmbilikoClaimTypes.AccountInfo, settings: user.JsonSerializerSettings, issuer: Defaults.IdentityProvider).ToList();

            MergeClaims(user.Claims, identity.FilterClaims(
                identity.NameClaimType,
                identity.RoleClaimType,
                MicrosoftClaimTypes.IdentityProvider,
                UmbilikoClaimTypes.AccessFailedCount,
                UmbilikoClaimTypes.AccessToken,
                UmbilikoClaimTypes.AccountInfo,
                UmbilikoClaimTypes.CompanyCode,
                UmbilikoClaimTypes.CompanyName,
                UmbilikoClaimTypes.EmailConfirmed,
                UmbilikoClaimTypes.LockoutEnabled,
                UmbilikoClaimTypes.LockoutEndDate,
                UmbilikoClaimTypes.LoginId,
                UmbilikoClaimTypes.LoginInfo,
                UmbilikoClaimTypes.LoginKey,
                UmbilikoClaimTypes.LoginProvider,
                UmbilikoClaimTypes.PasswordHash,
                UmbilikoClaimTypes.PhoneNumber,
                UmbilikoClaimTypes.PhoneNumberConfirmed,
                UmbilikoClaimTypes.SecurityStamp,
                UmbilikoClaimTypes.SegmentCode,
                UmbilikoClaimTypes.SegmentDescription,
                UmbilikoClaimTypes.SessionId,
                UmbilikoClaimTypes.TwoFactorCode,
                UmbilikoClaimTypes.TwoFactorConfirmed,
                UmbilikoClaimTypes.TwoFactorEnabled,
                UmbilikoClaimTypes.TwoFactorInstant,
                UmbilikoClaimTypes.SessionId).Select(c => (ClaimInfo)c));

            

            user.DisplayName = GetClaim(identity, type: UmbilikoClaimTypes.DisplayName, issuer: Defaults.IdentityProvider);
            
            user.Email = GetClaim(identity, type: ClaimTypes.Email, issuer: Defaults.IdentityProvider);

            user.EmailConfirmed = GetBooleanClaim(identity, type: UmbilikoClaimTypes.EmailConfirmed, issuer: Defaults.IdentityProvider);
            
            user.LockoutEnabled = GetBooleanClaim(identity, type: UmbilikoClaimTypes.LockoutEnabled, issuer: Defaults.IdentityProvider);

            user.LockoutEndDateUtc = GetDateTimeClaim(identity, type: UmbilikoClaimTypes.LockoutEndDate, issuer: Defaults.IdentityProvider);

            user.Logins = GetJsonClaims<LoginInfo>(identity, type: UmbilikoClaimTypes.LoginInfo, settings: user.JsonSerializerSettings, issuer: Defaults.IdentityProvider).ToList();

            user.Name = GetClaim(identity, type: identity.NameClaimType, issuer: Defaults.IdentityProvider);

            user.PasswordHash = GetClaim(identity, type: UmbilikoClaimTypes.PasswordHash, issuer: Defaults.IdentityProvider);

            user.PhoneNumber = GetClaim(identity, type: UmbilikoClaimTypes.PhoneNumber, issuer: Defaults.IdentityProvider);

            user.PhoneNumberConfirmed = GetBooleanClaim(identity, type: UmbilikoClaimTypes.PhoneNumberConfirmed, issuer: Defaults.IdentityProvider);

            user.SecurityStamp = GetClaim(identity, type: UmbilikoClaimTypes.SecurityStamp, issuer: Defaults.IdentityProvider);

            user.TwoFactorCode = GetClaim(identity, type: UmbilikoClaimTypes.TwoFactorCode, issuer: Defaults.IdentityProvider);

            user.TwoFactorEnabled = GetBooleanClaim(identity, type: UmbilikoClaimTypes.TwoFactorEnabled, issuer: Defaults.IdentityProvider);

            user.TwoFactorConfirmed = GetBooleanClaim(identity, type: UmbilikoClaimTypes.TwoFactorConfirmed, issuer: Defaults.IdentityProvider);

            user.TwoFactorInstant = GetDateTimeClaim(identity, type: UmbilikoClaimTypes.TwoFactorInstant, issuer: Defaults.IdentityProvider);

            return user;
        }

        public static string SelectFirstOrDefault(this IEnumerable<Claim> claims, string type)
        {
            return claims.Where(c => c.Type == type).Select(c => c.Value).FirstOrDefault();
        }

        public static T? SelectFirstOrDefault<T>(this IEnumerable<Claim> claims, string type)
            where T : struct
        {
            return claims.Select<T>(type).Where(result => result.HasValue).FirstOrDefault();
        }

        public static Guid? GetGuid(this IEnumerable<Claim> claims, string type)
        {
            Guid result;

            var value = claims.SelectFirstOrDefault(type);

            if (!string.IsNullOrEmpty(value) && Guid.TryParse(value, out result)) return result;

            return null;
        }

        public static IEnumerable<string> Select(this IEnumerable<Claim> claims, string type)
        {
            return claims.Where(c => c.Type == type).Select(c => c.Value);
        }

        public static IEnumerable<T?> Select<T>(this IEnumerable<Claim> claims, string type)
            where T : struct
        {
            var converter = TypeDescriptor.GetConverter(typeof(T));

            Func<string, T?> convert = (input) =>
            {
                T? value;
                try
                {
                    value = (T)converter.ConvertFromString(input);
                }
                catch (Exception)
                {
                    value = null;
                }
                return value;
            };

            return claims.Where(c => c.Type == type).Select(c => convert(c.Value));
        }
    }
}
