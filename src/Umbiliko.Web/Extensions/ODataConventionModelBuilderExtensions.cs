﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.OData;
using System.Web.Http.OData.Builder;
using Microsoft.Data.Edm;

namespace Uos.Web
{
    public static class ODataConventionModelBuilderExtensions
    {
        public static EntitySetConfiguration<TEntity> EntitySet<TEntity, TController>(this ODataConventionModelBuilder builder, string name)
            where TEntity : class, new()
            where TController : ODataController
        {
            var entitySet = builder.EntitySet<TEntity>(name);

            //var navigation = new NavigationPropertyConfiguration(null, EdmMultiplicity.Many, (EntityTypeConfiguration)entitySet.EntityType);

            //entitySet.FindBinding(navigation);
            
            return entitySet;
        }
    }
}
