﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web
{
    using Constants;
    using Contracts;
    using Services;

    public static class UserManagerExtensions
    {
        static readonly PasswordStrength DefaultPasswordStrength = new PasswordStrength();

        public static UserManager<TUser> Initialize<TUser>(this UserManager<TUser> manager)
            where TUser : class, IUser<string>
        {
            manager.UserValidator = new UserValidator<TUser>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Configure user lockout defaults
            manager.UserLockoutEnabledByDefault = true;
            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            manager.MaxFailedAccessAttemptsBeforeLockout = 5;

            manager.SetPasswordValidator();

            // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
            // You can write your own provider and plug it in here.
            /*manager.RegisterTwoFactorProvider(Deventry.Web.Resources.Strings.PhoneCode, new PhoneNumberTokenProvider<ApplicationUser>
            {
                MessageFormat = Deventry.Web.Resources.Strings.PhoneCodeBody
            });

            manager.RegisterTwoFactorProvider(Deventry.Web.Resources.Strings.EmailCode, new EmailTokenProvider<ApplicationUser>
            {
                Subject = Deventry.Web.Resources.Strings.EmailCodeSubject,
                BodyFormat = Deventry.Web.Resources.Strings.EmailCodeBody
            });*/

            manager.EmailService = new SmtpService();

            manager.SmsService = new SmsService();

            return manager;
        }

        public static UserManager<TUser> Initialize<TUser>(this UserManager<TUser> manager, IdentityFactoryOptions<TUser> options)
            where TUser : class, IUser<string>, IDisposable
        {
            var protector = options.DataProtectionProvider;

            if (protector != null)
            {
                // TODO: user derived class from Uos.Web.Providers.UserTokenProviderBase
                manager.UserTokenProvider = new DataProtectorTokenProvider<TUser>(protector.Create("ASP.NET Identity"/*, "Uos Identity"*/));
            }

            return manager;
        }

        public static UserManager<TUser, TKey> SetPasswordValidator<TUser, TKey>(this UserManager<TUser, TKey> manager)
            where TUser : class, IUser<TKey>
            where TKey : IEquatable<TKey>
        {
            manager.PasswordValidator = new PasswordValidator(DefaultPasswordStrength);

            return manager;
        }

        public static UserManager<TUser, TKey> SetPasswordValidator<TUser, TKey>(this UserManager<TUser, TKey> manager, PasswordStrength required)
            where TUser : class, IUser<TKey>
            where TKey : IEquatable<TKey>
        {
            manager.PasswordValidator = new PasswordValidator(required);

            return manager;
        }

        public static UserManager<TUser, TKey> SetPasswordValidator<TUser, TKey>(this UserManager<TUser, TKey> manager, ICompanyContext company)
            where TUser : class, IUser<TKey>
            where TKey : IEquatable<TKey>
        {
            manager.PasswordValidator = new PasswordValidator(company.RequiredPasswordStrength ?? DefaultPasswordStrength);

            return manager;
        }
    }
}
