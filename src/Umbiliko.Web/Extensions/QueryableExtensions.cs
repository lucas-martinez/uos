﻿//using Kendo.Mvc;
//using Kendo.Mvc.Infrastructure;
//using Kendo.Mvc.Infrastructure.Implementation;
//using Kendo.Mvc.Infrastructure.Implementation.Expressions;
//using Kendo.Mvc.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Umbiliko.Web
{
        public static class QueryableExtensions
        {
            /*public static AggregateResultCollection Aggregate(this IQueryable source, IEnumerable<AggregateFunction> aggregateFunctions)
            {
                List<AggregateFunction> list = aggregateFunctions.ToList<AggregateFunction>();
                if (list.Count > 0)
                {
                    foreach (AggregateFunctionsGroup group in new QueryableAggregatesExpressionBuilder(source, list) { Options = { LiftMemberAccessToNull = source.Provider.IsLinqToObjectsProvider() } }.CreateQuery())
                    {
                        return group.GetAggregateResults(list);
                    }
                }
                return new AggregateResultCollection();
            }

            private static IQueryable CallQueryableMethod(this IQueryable source, string methodName, LambdaExpression selector)
            {
                return source.Provider.CreateQuery(Expression.Call(typeof(Queryable), methodName, new Type[] { source.ElementType, selector.Body.Type }, new Expression[] { source.Expression, Expression.Quote(selector) }));
            }

            public static int Count(this IQueryable source)
            {
                if (source == null)
                {
                    throw new ArgumentNullException("source");
                }
                return source.Provider.Execute<int>(Expression.Call(typeof(Queryable), "Count", new Type[] { source.ElementType }, new Expression[] { source.Expression }));
            }*/

            private static DataSourceResult CreateDataSourceResult<TModel, TResult>(this IQueryable queryable, DataSourceRequest request, ModelStateDictionary modelState, Func<TModel, TResult> selector)
            {
                Func<AggregateDescriptor, IEnumerable<AggregateFunction>> func = null;
                Action<GroupDescriptor> action = null;
                Action<GroupDescriptor> action2 = null;
                DataSourceResult result = new DataSourceResult();
                IQueryable source = queryable;
                List<IFilterDescriptor> list = new List<IFilterDescriptor>();
                if (request.Filters != null)
                {
                    list.AddRange(request.Filters);
                }
                if (list.Any<IFilterDescriptor>())
                {
                    source = source.Where(list);
                }
                List<SortDescriptor> sort = new List<SortDescriptor>();
                if (request.Sorts != null)
                {
                    sort.AddRange(request.Sorts);
                }
                List<SortDescriptor> temporarySortDescriptors = new List<SortDescriptor>();
                IList<GroupDescriptor> instance = new List<GroupDescriptor>();
                if (request.Groups != null)
                {
                    instance.AddRange<GroupDescriptor>(request.Groups);
                }
                List<AggregateDescriptor> aggregates = new List<AggregateDescriptor>();
                if (request.Aggregates != null)
                {
                    aggregates.AddRange(request.Aggregates);
                }
                if (aggregates.Any<AggregateDescriptor>())
                {
                    IQueryable queryable3 = source.AsQueryable();
                    IQueryable queryable4 = queryable3;
                    if (list.Any<IFilterDescriptor>())
                    {
                        queryable4 = queryable3.Where(list);
                    }
                    if (func == null)
                    {
                        func = a => a.Aggregates;
                    }
                    result.AggregateResults = queryable4.Aggregate(aggregates.SelectMany<AggregateDescriptor, AggregateFunction>(func));
                    if (instance.Any<GroupDescriptor>() && aggregates.Any<AggregateDescriptor>())
                    {
                        if (action == null)
                        {
                            action = delegate (GroupDescriptor g) {
                                g.AggregateFunctions.AddRange<AggregateFunction>(from a in aggregates select a.Aggregates);
                            };
                        }
                        instance.Each<GroupDescriptor>(action);
                    }
                }
                result.Total = source.Count();
                if (!sort.Any<SortDescriptor>() && queryable.Provider.IsEntityFrameworkProvider())
                {
                    SortDescriptor descriptor = new SortDescriptor
                    {
                        Member = queryable.ElementType.FirstSortableProperty()
                    };
                    sort.Add(descriptor);
                    temporarySortDescriptors.Add(descriptor);
                }
                if (instance.Any<GroupDescriptor>())
                {
                    if (action2 == null)
                    {
                        action2 = delegate (GroupDescriptor groupDescriptor) {
                            SortDescriptor item = new SortDescriptor
                            {
                                Member = groupDescriptor.Member,
                                SortDirection = groupDescriptor.SortDirection
                            };
                            sort.Insert(0, item);
                            temporarySortDescriptors.Add(item);
                        };
                    }
                    instance.Reverse<GroupDescriptor>().Each<GroupDescriptor>(action2);
                }
                if (sort.Any<SortDescriptor>())
                {
                    source = source.Sort(sort);
                }
                IQueryable notPagedData = source;
                source = source.Page(request.Page - 1, request.PageSize);
                if (instance.Any<GroupDescriptor>())
                {
                    source = source.GroupBy(notPagedData, instance);
                }
                result.Data = source.Execute<TModel, TResult>(selector);
                if ((modelState != null) && !modelState.IsValid)
                {
                    result.Errors = modelState.SerializeErrors();
                }
                temporarySortDescriptors.Each<SortDescriptor>(delegate (SortDescriptor sortDescriptor) {
                    sort.Remove(sortDescriptor);
                });
                return result;
            }

            /*
            private static TreeDataSourceResult CreateTreeDataSourceResult<TModel, T1, T2, TResult>(this IQueryable queryable, DataSourceRequest request, Expression<Func<TModel, T1>> idSelector, Expression<Func<TModel, T2>> parentIDSelector, ModelStateDictionary modelState, Func<TModel, TResult> selector, Expression<Func<TModel, bool>> rootSelector)
            {
                TreeDataSourceResult result = new TreeDataSourceResult();
                IQueryable source = queryable;
                List<IFilterDescriptor> list = new List<IFilterDescriptor>();
                if (request.Filters != null)
                {
                    list.AddRange(request.Filters);
                }
                if (list.Any<IFilterDescriptor>())
                {
                    source = source.Where(list).ParentsRecursive<TModel>(queryable, idSelector, parentIDSelector);
                }
                IQueryable allData = source;
                if (rootSelector != null)
                {
                    source = source.Where(rootSelector);
                }
                List<SortDescriptor> list2 = new List<SortDescriptor>();
                if (request.Sorts != null)
                {
                    list2.AddRange(request.Sorts);
                }
                List<AggregateDescriptor> list3 = new List<AggregateDescriptor>();
                if (request.Aggregates != null)
                {
                    list3.AddRange(request.Aggregates);
                }
                if (list3.Any<AggregateDescriptor>())
                {
                    IQueryable queryable4 = source;
                    foreach (IGrouping<T2, TModel> grouping in queryable4.GroupBy(parentIDSelector))
                    {
                        result.AggregateResults.Add(Convert.ToString(grouping.Key), grouping.AggregateForLevel<TModel, T1, T2>(allData, list3, idSelector, parentIDSelector));
                    }
                }
                if (list2.Any<SortDescriptor>())
                {
                    source = source.Sort(list2);
                }
                result.Data = source.Execute<TModel, TResult>(selector);
                if ((modelState != null) && !modelState.IsValid)
                {
                    result.Errors = modelState.SerializeErrors();
                }
                return result;
            }

            public static object ElementAt(this IQueryable source, int index)
            {
                if (source == null)
                {
                    throw new ArgumentNullException("source");
                }
                if (index < 0)
                {
                    throw new ArgumentOutOfRangeException("index");
                }
                return source.Provider.Execute(Expression.Call(typeof(Queryable), "ElementAt", new Type[] { source.ElementType }, new Expression[] { source.Expression, Expression.Constant(index) }));
            }

            private static IEnumerable Execute<TModel, TResult>(this IQueryable source, Func<TModel, TResult> selector)
            {
                if (source == null)
                {
                    throw new ArgumentNullException("source");
                }
                if (source is DataTableWrapper)
                {
                    return source;
                }
                Type elementType = source.ElementType;
                if (selector != null)
                {
                    List<AggregateFunctionsGroup> list = new List<AggregateFunctionsGroup>();
                    if (elementType == typeof(AggregateFunctionsGroup))
                    {
                        foreach (AggregateFunctionsGroup group in source)
                        {
                            group.Items = group.Items.AsQueryable().Execute<TModel, TResult>(selector);
                            list.Add(group);
                        }
                        return list;
                    }
                    List<TResult> list2 = new List<TResult>();
                    foreach (TModel local in source)
                    {
                        list2.Add(selector(local));
                    }
                    return list2;
                }
                IList list3 = (IList)Activator.CreateInstance(typeof(List<>).MakeGenericType(new Type[] { elementType }));
                foreach (object obj2 in source)
                {
                    list3.Add(obj2);
                }
                return list3;
            }

            private static Type GetFieldByTypeFromDataColumn(DataTable dataTable, string memberName)
            {
                if (!dataTable.Columns.Contains(memberName))
                {
                    return null;
                }
                return dataTable.Columns[memberName].DataType;
            }

            public static IQueryable GroupBy(this IQueryable source, IEnumerable<GroupDescriptor> groupDescriptors)
            {
                return source.GroupBy(source, groupDescriptors);
            }

            public static IQueryable GroupBy(this IQueryable source, LambdaExpression keySelector)
            {
                return source.CallQueryableMethod("GroupBy", keySelector);
            }

            public static IQueryable GroupBy(this IQueryable source, IQueryable notPagedData, IEnumerable<GroupDescriptor> groupDescriptors)
            {
                return new GroupDescriptorCollectionExpressionBuilder(source, groupDescriptors, notPagedData) { Options = { LiftMemberAccessToNull = source.Provider.IsLinqToObjectsProvider() } }.CreateQuery();
            }

            public static IQueryable OrderBy(this IQueryable source, LambdaExpression keySelector)
            {
                return source.CallQueryableMethod("OrderBy", keySelector);
            }

            public static IQueryable OrderBy(this IQueryable source, LambdaExpression keySelector, ListSortDirection? sortDirection)
            {
                if (!sortDirection.HasValue)
                {
                    return source;
                }
                if (((ListSortDirection)sortDirection.Value) == ListSortDirection.Ascending)
                {
                    return source.OrderBy(keySelector);
                }
                return source.OrderByDescending(keySelector);
            }

            public static IQueryable OrderByDescending(this IQueryable source, LambdaExpression keySelector)
            {
                return source.CallQueryableMethod("OrderByDescending", keySelector);
            }

            public static IQueryable Page(this IQueryable source, int pageIndex, int pageSize)
            {
                IQueryable queryable = source;
                queryable = queryable.Skip(pageIndex * pageSize);
                if (pageSize > 0)
                {
                    queryable = queryable.Take(pageSize);
                }
                return queryable;
            }

            public static IQueryable Select(this IQueryable source, LambdaExpression selector)
            {
                return source.CallQueryableMethod("Select", selector);
            }

            public static IQueryable Skip(this IQueryable source, int count)
            {
                if (source == null)
                {
                    throw new ArgumentNullException("source");
                }
                return source.Provider.CreateQuery(Expression.Call(typeof(Queryable), "Skip", new Type[] { source.ElementType }, new Expression[] { source.Expression, Expression.Constant(count) }));
            }

            public static IQueryable Sort(this IQueryable source, IEnumerable<SortDescriptor> sortDescriptors)
            {
                SortDescriptorCollectionExpressionBuilder builder = new SortDescriptorCollectionExpressionBuilder(source, sortDescriptors);
                return builder.Sort();
            }

            public static IQueryable Take(this IQueryable source, int count)
            {
                if (source == null)
                {
                    throw new ArgumentNullException("source");
                }
                return source.Provider.CreateQuery(Expression.Call(typeof(Queryable), "Take", new Type[] { source.ElementType }, new Expression[] { source.Expression, Expression.Constant(count) }));
            }

            private static DataSourceResult ToDataSourceResult(this DataTableWrapper enumerable, DataSourceRequest request)
            {
                List<IFilterDescriptor> source = new List<IFilterDescriptor>();
                if (request.Filters != null)
                {
                    source.AddRange(request.Filters);
                }
                if (source.Any<IFilterDescriptor>())
                {
                    DataTable dataTable = enumerable.Table;
                    source.SelectMemberDescriptors().Each<FilterDescriptor>(delegate (FilterDescriptor f) {
                        f.MemberType = GetFieldByTypeFromDataColumn(dataTable, f.Member);
                    });
                }
                List<GroupDescriptor> list2 = new List<GroupDescriptor>();
                if (request.Groups != null)
                {
                    list2.AddRange(request.Groups);
                }
                if (list2.Any<GroupDescriptor>())
                {
                    DataTable dataTable = enumerable.Table;
                    list2.Each<GroupDescriptor>(delegate (GroupDescriptor g) {
                        g.MemberType = GetFieldByTypeFromDataColumn(dataTable, g.Member);
                    });
                }
                DataSourceResult result = enumerable.AsEnumerable<DataRowView>().ToDataSourceResult(request);
                result.Data = result.Data.SerializeToDictionary(enumerable.Table);
                return result;
            }

            public static DataSourceResult ToDataSourceResult(this IEnumerable enumerable, DataSourceRequest request)
            {
                return enumerable.AsQueryable().ToDataSourceResult(request);
            }

            public static DataSourceResult ToDataSourceResult(this DataTable dataTable, DataSourceRequest request)
            {
                return dataTable.WrapAsEnumerable().ToDataSourceResult(request);
            }

            public static DataSourceResult ToDataSourceResult(this IQueryable enumerable, DataSourceRequest request)
            {
                return enumerable.ToDataSourceResult(request, null);
            }

            public static DataSourceResult ToDataSourceResult<TModel, TResult>(this IEnumerable<TModel> enumerable, DataSourceRequest request, Func<TModel, TResult> selector)
            {
                return enumerable.AsQueryable<TModel>().CreateDataSourceResult<TModel, TResult>(request, null, selector);
            }

            public static DataSourceResult ToDataSourceResult(this IEnumerable enumerable, DataSourceRequest request, ModelStateDictionary modelState)
            {
                return enumerable.AsQueryable().ToDataSourceResult(request, modelState);
            }

            public static DataSourceResult ToDataSourceResult(this IQueryable queryable, DataSourceRequest request, ModelStateDictionary modelState)
            {
                return queryable.CreateDataSourceResult<object, object>(request, modelState, null);
            }*/

            public static DataSourceResult ToDataSourceResult<TModel, TResult>(this IQueryable<TModel> enumerable, DataSourceRequest request, Func<TModel, TResult> selector)
            {
                return enumerable.CreateDataSourceResult<TModel, TResult>(request, null, selector);
            }

            /*public static DataSourceResult ToDataSourceResult<TModel, TResult>(this IEnumerable<TModel> enumerable, DataSourceRequest request, ModelStateDictionary modelState, Func<TModel, TResult> selector)
            {
                return enumerable.AsQueryable<TModel>().CreateDataSourceResult<TModel, TResult>(request, modelState, selector);
            }

            public static DataSourceResult ToDataSourceResult<TModel, TResult>(this IQueryable<TModel> enumerable, DataSourceRequest request, ModelStateDictionary modelState, Func<TModel, TResult> selector)
            {
                return enumerable.CreateDataSourceResult<TModel, TResult>(request, modelState, selector);
            }

            public static TreeDataSourceResult ToTreeDataSourceResult(this IEnumerable enumerable, DataSourceRequest request)
            {
                return enumerable.AsQueryable().ToTreeDataSourceResult(request, null);
            }

            public static TreeDataSourceResult ToTreeDataSourceResult<TModel, TResult>(this IEnumerable<TModel> enumerable, DataSourceRequest request, Func<TModel, TResult> selector)
            {
                return enumerable.ToTreeDataSourceResult<TModel, object, object, TResult>(request, null, null, selector);
            }

            public static TreeDataSourceResult ToTreeDataSourceResult(this IEnumerable enumerable, DataSourceRequest request, ModelStateDictionary modelState)
            {
                return enumerable.AsQueryable().CreateTreeDataSourceResult<object, object, object, object>(request, null, null, modelState, null, null);
            }

            public static TreeDataSourceResult ToTreeDataSourceResult<TModel, TResult>(this IQueryable<TModel> enumerable, DataSourceRequest request, Func<TModel, TResult> selector)
            {
                return enumerable.ToTreeDataSourceResult<TModel, object, object, TResult>(request, null, null, selector);
            }

            public static TreeDataSourceResult ToTreeDataSourceResult<TModel, T1, T2>(this IEnumerable<TModel> enumerable, DataSourceRequest request, Expression<Func<TModel, T1>> idSelector, Expression<Func<TModel, T2>> parentIDSelector)
            {
                return enumerable.AsQueryable<TModel>().CreateTreeDataSourceResult<TModel, T1, T2, TModel>(request, idSelector, parentIDSelector, null, null, null);
            }

            public static TreeDataSourceResult ToTreeDataSourceResult<TModel, T1, T2>(this IQueryable<TModel> enumerable, DataSourceRequest request, Expression<Func<TModel, T1>> idSelector, Expression<Func<TModel, T2>> parentIDSelector)
            {
                return enumerable.CreateTreeDataSourceResult<TModel, T1, T2, TModel>(request, idSelector, parentIDSelector, null, null, null);
            }

            public static TreeDataSourceResult ToTreeDataSourceResult<TModel, T1, T2, TResult>(this IEnumerable<TModel> queryable, DataSourceRequest request, Expression<Func<TModel, T1>> idSelector, Expression<Func<TModel, T2>> parentIDSelector, Func<TModel, TResult> selector)
            {
                return queryable.AsQueryable<TModel>().CreateTreeDataSourceResult<TModel, T1, T2, TResult>(request, idSelector, parentIDSelector, null, selector, null);
            }

            public static TreeDataSourceResult ToTreeDataSourceResult<TModel, T1, T2>(this IEnumerable<TModel> enumerable, DataSourceRequest request, Expression<Func<TModel, T1>> idSelector, Expression<Func<TModel, T2>> parentIDSelector, Expression<Func<TModel, bool>> rootSelector)
            {
                return enumerable.AsQueryable<TModel>().CreateTreeDataSourceResult<TModel, T1, T2, TModel>(request, idSelector, parentIDSelector, null, null, rootSelector);
            }

            public static TreeDataSourceResult ToTreeDataSourceResult<TModel, T1, T2>(this IEnumerable<TModel> queryable, DataSourceRequest request, Expression<Func<TModel, T1>> idSelector, Expression<Func<TModel, T2>> parentIDSelector, ModelStateDictionary modelState)
            {
                return queryable.AsQueryable<TModel>().ToTreeDataSourceResult<TModel, T1, T2, TModel>(request, idSelector, parentIDSelector, modelState, null);
            }

            public static TreeDataSourceResult ToTreeDataSourceResult<TModel, T1, T2, TResult>(this IQueryable<TModel> queryable, DataSourceRequest request, Expression<Func<TModel, T1>> idSelector, Expression<Func<TModel, T2>> parentIDSelector, Func<TModel, TResult> selector)
            {
                return queryable.CreateTreeDataSourceResult<TModel, T1, T2, TResult>(request, idSelector, parentIDSelector, null, selector, null);
            }

            public static TreeDataSourceResult ToTreeDataSourceResult<TModel, T1, T2>(this IQueryable<TModel> enumerable, DataSourceRequest request, Expression<Func<TModel, T1>> idSelector, Expression<Func<TModel, T2>> parentIDSelector, Expression<Func<TModel, bool>> rootSelector)
            {
                return enumerable.CreateTreeDataSourceResult<TModel, T1, T2, TModel>(request, idSelector, parentIDSelector, null, null, rootSelector);
            }

            public static TreeDataSourceResult ToTreeDataSourceResult<TModel, T1, T2>(this IQueryable<TModel> queryable, DataSourceRequest request, Expression<Func<TModel, T1>> idSelector, Expression<Func<TModel, T2>> parentIDSelector, ModelStateDictionary modelState)
            {
                return queryable.ToTreeDataSourceResult<TModel, T1, T2, TModel>(request, idSelector, parentIDSelector, modelState, null);
            }

            public static TreeDataSourceResult ToTreeDataSourceResult<TModel, T1, T2, TResult>(this IEnumerable<TModel> queryable, DataSourceRequest request, Expression<Func<TModel, T1>> idSelector, Expression<Func<TModel, T2>> parentIDSelector, Expression<Func<TModel, bool>> rootSelector, Func<TModel, TResult> selector)
            {
                return queryable.AsQueryable<TModel>().CreateTreeDataSourceResult<TModel, T1, T2, TResult>(request, idSelector, parentIDSelector, null, selector, rootSelector);
            }

            public static TreeDataSourceResult ToTreeDataSourceResult<TModel, T1, T2>(this IEnumerable<TModel> enumerable, DataSourceRequest request, Expression<Func<TModel, T1>> idSelector, Expression<Func<TModel, T2>> parentIDSelector, Expression<Func<TModel, bool>> rootSelector, ModelStateDictionary modelState)
            {
                return enumerable.AsQueryable<TModel>().CreateTreeDataSourceResult<TModel, T1, T2, TModel>(request, idSelector, parentIDSelector, modelState, null, rootSelector);
            }

            public static TreeDataSourceResult ToTreeDataSourceResult<TModel, T1, T2, TResult>(this IEnumerable<TModel> queryable, DataSourceRequest request, Expression<Func<TModel, T1>> idSelector, Expression<Func<TModel, T2>> parentIDSelector, ModelStateDictionary modelState, Func<TModel, TResult> selector)
            {
                return queryable.AsQueryable<TModel>().CreateTreeDataSourceResult<TModel, T1, T2, TResult>(request, idSelector, parentIDSelector, modelState, selector, null);
            }

            public static TreeDataSourceResult ToTreeDataSourceResult<TModel, T1, T2, TResult>(this IQueryable<TModel> queryable, DataSourceRequest request, Expression<Func<TModel, T1>> idSelector, Expression<Func<TModel, T2>> parentIDSelector, Expression<Func<TModel, bool>> rootSelector, Func<TModel, TResult> selector)
            {
                return queryable.CreateTreeDataSourceResult<TModel, T1, T2, TResult>(request, idSelector, parentIDSelector, null, selector, rootSelector);
            }

            public static TreeDataSourceResult ToTreeDataSourceResult<TModel, T1, T2>(this IQueryable<TModel> enumerable, DataSourceRequest request, Expression<Func<TModel, T1>> idSelector, Expression<Func<TModel, T2>> parentIDSelector, Expression<Func<TModel, bool>> rootSelector, ModelStateDictionary modelState)
            {
                return enumerable.CreateTreeDataSourceResult<TModel, T1, T2, TModel>(request, idSelector, parentIDSelector, modelState, null, rootSelector);
            }

            public static TreeDataSourceResult ToTreeDataSourceResult<TModel, T1, T2, TResult>(this IQueryable<TModel> queryable, DataSourceRequest request, Expression<Func<TModel, T1>> idSelector, Expression<Func<TModel, T2>> parentIDSelector, ModelStateDictionary modelState, Func<TModel, TResult> selector)
            {
                return queryable.CreateTreeDataSourceResult<TModel, T1, T2, TResult>(request, idSelector, parentIDSelector, modelState, selector, null);
            }

            public static TreeDataSourceResult ToTreeDataSourceResult<TModel, T1, T2, TResult>(this IEnumerable<TModel> queryable, DataSourceRequest request, Expression<Func<TModel, T1>> idSelector, Expression<Func<TModel, T2>> parentIDSelector, Expression<Func<TModel, bool>> rootSelector, ModelStateDictionary modelState, Func<TModel, TResult> selector)
            {
                return queryable.AsQueryable<TModel>().CreateTreeDataSourceResult<TModel, T1, T2, TResult>(request, idSelector, parentIDSelector, modelState, selector, rootSelector);
            }

            public static TreeDataSourceResult ToTreeDataSourceResult<TModel, T1, T2, TResult>(this IQueryable<TModel> queryable, DataSourceRequest request, Expression<Func<TModel, T1>> idSelector, Expression<Func<TModel, T2>> parentIDSelector, Expression<Func<TModel, bool>> rootSelector, ModelStateDictionary modelState, Func<TModel, TResult> selector)
            {
                return queryable.CreateTreeDataSourceResult<TModel, T1, T2, TResult>(request, idSelector, parentIDSelector, modelState, selector, rootSelector);
            }

            public static IQueryable Union(this IQueryable source, IQueryable second)
            {
                return source.Provider.CreateQuery(Expression.Call(typeof(Queryable), "Union", new Type[] { source.ElementType }, new Expression[] { source.Expression, second.Expression }));
            }

            public static IQueryable Where(this IQueryable source, IEnumerable<IFilterDescriptor> filterDescriptors)
            {
                if (filterDescriptors.Any<IFilterDescriptor>())
                {
                    LambdaExpression predicate = new FilterDescriptorCollectionExpressionBuilder(Expression.Parameter(source.ElementType, "item"), filterDescriptors) { Options = { LiftMemberAccessToNull = source.Provider.IsLinqToObjectsProvider() } }.CreateFilterExpression();
                    return source.Where(predicate);
                }
                return source;
            }

            public static IQueryable Where(this IQueryable source, Expression predicate)
            {
                return source.Provider.CreateQuery(Expression.Call(typeof(Queryable), "Where", new Type[] { source.ElementType }, new Expression[] { source.Expression, Expression.Quote(predicate) }));
            }*/
        }
    }
