﻿using Microsoft.Owin;
using System;

namespace Uos.Web
{
    public static class OwinContextExtensions
    {
        private static readonly string IdentityKeyPrefix = @"AspNet.Identity.Owin:";
        
        public static T GetInstance<T>(this IOwinContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }
            return context.Get<T>(GetKey(typeof(T)));
        }

        private static string GetKey(Type t)
        {
            return (IdentityKeyPrefix + t.FullName);
        }

        public static IOwinContext SetInstance<T>(this IOwinContext context, T value)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }
            return context.Set<T>(GetKey(typeof(T)), value);
        }
    }
}
