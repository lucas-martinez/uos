﻿using Uos.Persistence.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Persistence
{
    public abstract class DbQuest<TArguments, TResult, TDbConnection, TDbCommand, TDbParameter> : DbMethod<TArguments, TDbConnection, TDbCommand, TDbParameter>, IDbQuest<TArguments, TResult>
        where TArguments : class, new()
        where TResult : struct
        where TDbConnection : DbConnection, new()
        where TDbCommand : DbCommand, new()
        where TDbParameter : DbParameter, new()
    {
        protected DbQuest(DbConnectionProvider<TDbConnection> connectionProvider, string dbName, ComplexTypeConfiguration<TArguments> argumentsConfiguration)
            : base(connectionProvider, dbName, argumentsConfiguration)
        {
        }

        protected DbQuest(DbLink<TDbConnection, TDbCommand, TDbParameter> parent, string methodName, ComplexTypeConfiguration<TArguments> argumentsConfiguration)
            : base(parent, methodName, argumentsConfiguration)
        {
        }

        public override DbLinkTypes DbLinkType
        {
            get { return DbLinkTypes.Quest; }
        }

        public virtual new TResult DbInvoke(TArguments arguments)
        {
            throw new NotImplementedException();
        }
    }
}
