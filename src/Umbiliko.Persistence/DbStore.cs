﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Persistence
{
    using Uos.Inquiry;
    using Uos.Persistence.Configuration;

    public class DbStore<TEntity, TDbConnection, TDbCommand, TDbDataAdapter, TDbParameter> : DbLink<TDbConnection, TDbCommand, TDbParameter>, IStore<TEntity>, IQueryable<TEntity>, IEnumerable<TEntity>, IEntityStore<TEntity>, IHaveMappingEntity
        where TEntity : class, new()
        where TDbConnection : DbConnection, new()
        where TDbCommand : DbCommand, new()
        where TDbDataAdapter : DbDataAdapter, new()
        where TDbParameter : DbParameter, new()
    {
        private DataTable _dataTable;
        private TDbDataAdapter _dbDataAdapter;
        private TDbCommand _dbCountCommand;
        private TDbCommand _dbDeleteCommand;
        private TDbCommand _dbInsertCommand;
        private TDbCommand _dbSelectCommand;
        private TDbCommand _dbUpdateCommand;
        private readonly IQueryable<TEntity> _query;
        private readonly EntityTypeConfiguration<TEntity> _dbConfiguration = null;

        public DbStore(DbConnectionProvider<TDbConnection> connectionProvider, EntityTypeConfiguration<TEntity> configuration)
            : base(connectionProvider)
        {
            _query = new Query<TEntity>(connectionProvider);
            _dbConfiguration = configuration;
        }

        public DbStore(DbConnectionProvider<TDbConnection> connectionProvider, string dataSetName, EntityTypeConfiguration<TEntity> configuration)
            : base(connectionProvider, dataSetName)
        {
            _query = new Query<TEntity>(connectionProvider);
            _dbConfiguration = configuration;
        }

        public DbStore(DbConnectionProvider<TDbConnection> connectionProvider, DataSet dataSet, EntityTypeConfiguration<TEntity> configuration)
            : base(connectionProvider, dataSet)
        {
            _query = new Query<TEntity>(connectionProvider);
            _dbConfiguration = configuration;
        }

        public DbStore(DbLink<TDbConnection, TDbCommand, TDbParameter> parent, EntityTypeConfiguration<TEntity> configuration)
            : base(parent)
        {
            _query = new Query<TEntity>(parent.ConnectionProvider);
            _dbConfiguration = configuration;
        }

        public EntityTypeConfiguration<TEntity> Configuration
        {
            get { return _dbConfiguration; }
        }

        public virtual DataTable DataTable
        {
            get
            {
                var dataTable = _dataTable;

                if (dataTable != null) return dataTable;

                var dataSet = DataSet;

                dataTable = dataSet.Tables[_dbConfiguration.DbName];

                if (dataTable == null)
                {
                    dataTable = new DataTable(_dbConfiguration.DbName);

                    dataSet.Tables.Add(dataTable);
                }

                _dataTable = dataTable;

                return dataTable;

            }
        }

        public string DbName
        {
            get { return _dbConfiguration.DbName; }
        }

        public TDbDataAdapter DataAdapter
        {
            get
            {
                var dbDataAdapter = _dbDataAdapter;

                if (dbDataAdapter != null) return dbDataAdapter;

                dbDataAdapter = new TDbDataAdapter
                {
                    DeleteCommand = DbDeleteCommand,
                    InsertCommand = DbInsertCommand,
                    SelectCommand = DbSelectCommand,
                    UpdateCommand = DbUpdateCommand
                };

                _dbDataAdapter = dbDataAdapter;

                return dbDataAdapter;
            }
        }

        public TDbCommand DbCountCommand
        {
            get { return _dbCountCommand ?? (_dbCountCommand = CreateCountCommand()); }
        }

        public TDbCommand DbDeleteCommand
        {
            get { return _dbDeleteCommand ?? (_dbDeleteCommand = CreateDeleteCommand()); }
        }

        public TDbCommand DbInsertCommand
        {
            get { return _dbInsertCommand ?? (_dbInsertCommand = CreateInsertCommand()); }
        }

        public TDbCommand DbSelectCommand
        {
            get { return _dbSelectCommand ?? (_dbSelectCommand = CreateSelectCommand()); }
        }

        public TDbCommand DbUpdateCommand
        {
            get { return _dbUpdateCommand ?? (_dbUpdateCommand = CreateUpdateCommand()); }
        }

        public override DbLinkTypes DbLinkType
        {
            get { return DbLinkTypes.Store; }
        }

        protected virtual TDbCommand CreateCountCommand()
        {
            var dbCommand = new TDbCommand();

            throw new NotImplementedException();

            return dbCommand;
        }

        public virtual int Count()
        {
            throw new NotImplementedException();
        }

        public virtual DataRow Create(TEntity entity)
        {
            var dataRow = FromEntity(entity);

            DataTable.Rows.Add(dataRow);

            return dataRow;
        }

        protected virtual TDbCommand CreateDeleteCommand()
        {
            var dbCommand = new TDbCommand();

            dbCommand.CommandText = "DELETE FROM " + _dbConfiguration.DbName;
            dbCommand.CommandType = CommandType.Text;
            dbCommand.Connection = Connection;

            var pkey = DataTable.PrimaryKey;

            Filter(dbCommand, pkey);

            return dbCommand;
        }

        protected virtual TDbCommand CreateInsertCommand()
        {
            var pkey = DataTable.PrimaryKey;
            var columns = DataTable.Columns.OfType<DataColumn>().ToArray();

            var dbCommand = new TDbCommand();

            dbCommand.CommandText = "INSERT INTO " + _dbConfiguration.DbName + "(" + MakeFieldList(columns) + ") VALUES (" + MakeValueList(columns) + ")";
            dbCommand.CommandType = CommandType.Text;
            dbCommand.Connection = Connection;

            AddParameters(dbCommand, columns);

            return dbCommand;
        }

        protected virtual TDbCommand CreateSelectCommand()
        {
            var columns = DataTable.Columns.OfType<DataColumn>().ToArray();

            var dbCommand = new TDbCommand();

            dbCommand.CommandText = "SELECT " + MakeFieldList(columns) + " FROM " + _dbConfiguration.DbName;
            dbCommand.CommandType = CommandType.Text;
            dbCommand.Connection = Connection;

            return dbCommand;
        }

        protected virtual TDbCommand CreateUpdateCommand()
        {
            var pkey = DataTable.PrimaryKey;
            var columns = DataTable.Columns.OfType<DataColumn>().Except(pkey).ToArray();

            var dbCommand = new TDbCommand();

            dbCommand.CommandText = "UPDATE " + _dbConfiguration.DbName + " SET " + MakeSetList(columns);
            dbCommand.CommandType = CommandType.Text;
            dbCommand.Connection = Connection;

            AddParameters(dbCommand, columns);

            Filter(dbCommand, pkey);

            return dbCommand;
        }

        public DataRow Delete(TEntity entity)
        {
            var dataRow = Find(entity);

            if (dataRow != null) dataRow.Delete();

            return dataRow;
        }

        IEnumerator<TEntity> IEnumerable<TEntity>.GetEnumerator()
        {
            if (DataTable.Rows.Count.Equals(0)) Load();

            for (var i = 0; i < DataTable.Rows.Count; i++)
            {
                yield return Configuration.Create(DataTable.Rows[i]);
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<TEntity>)this).GetEnumerator();
        }

        public IEnumerable<TEntity> Query(Expression<Func<TEntity, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public void Load()
        {
            DataAdapter.Fill(DataTable);
        }

        private void OnFillError(object sender, FillErrorEventArgs e)
        {
            var msg = e.Errors.Message;
            e.Continue = true;
        }

        public virtual void SaveChanges()
        {
            DataAdapter.Update(DataTable);
        }

        public void Update(TEntity entity)
        {
            var changes = FromEntity(entity);
            var dataRow = Find(entity);

            foreach (DataColumn column in dataRow.Table.Columns)
            {
                dataRow[column] = changes[column];
            }
        }

        protected virtual void AddParameters(TDbCommand dbCommand, IEnumerable<DataColumn> columns)
        {
            foreach (var col in columns)
            {
                var param = CreateParameter(col.ColumnName);

                dbCommand.Parameters.Add(param);
            }
        }

        protected virtual DataRow Find(TEntity entity)
        {
            DataRow dataRow = null;

            var key = DataTable.PrimaryKey;
            var findRow = FromEntity(entity);

            var match = false;
            for (var i = 0; !match && i < DataTable.Rows.Count; i++)
            {
                dataRow = DataTable.Rows[i];

                match = true;

                foreach (var column in DataTable.PrimaryKey)
                {
                    if (dataRow[column] != findRow[column])
                    {
                        match = false;
                        break;
                    }
                }
            }

            return dataRow;
        }

        protected virtual DataRow FromEntity(TEntity entity)
        {
            var dataRow = DataTable.NewRow();

            Configuration.Update(entity, dataRow);

            return dataRow;
        }

        public DbStore<TEntity, TDbConnection, TDbCommand, TDbDataAdapter, TDbParameter> Page(string orderBy, int limit, int offset)
        {
            var commandText = DataAdapter.SelectCommand.CommandText;

            commandText = string.Format("{0} ORDER BY {1} LIMIT {2} OFFSET {3}", commandText, orderBy, limit, offset);

            DbSelectCommand.CommandText = commandText;

            return this;
        }

        protected virtual object Value(Expression expression)
        {
            object value = null;

            if (expression.NodeType == ExpressionType.Constant)
            {
                var constantExpression = expression as ConstantExpression;

                value = constantExpression.Value;
            }
            else if (expression.NodeType == ExpressionType.MemberAccess)
            {
                var memberExpression = expression as MemberExpression;

                var member = memberExpression.Member;

                var constant = memberExpression.Expression as ConstantExpression;

                if (constant != null)
                {
                    value = constant.Value;

                    switch (member.MemberType)
                    {
                        case MemberTypes.Field:
                            var field = member as FieldInfo;
                            value = field.GetValue(value);
                            break;

                        case MemberTypes.Property:
                            var property = member as PropertyInfo;
                            value = property.GetValue(value);
                            break;
                    }
                }/*
                else
                {
                    var right = memberExpression.Expression as MemberExpression;

                    if (right != null)
                    {
                        var member2 = right.Member;

                        switch (member2.MemberType)
                        {
                            case MemberTypes.Field:
                                var field = member2 as FieldInfo;
                                value = field.GetValue(value);
                                break;

                            case MemberTypes.Property:
                                var property = member2 as PropertyInfo;
                                value = property.GetValue(value);
                                break;
                        }

                    }
                }*/
            }

            return value;
        }

        public DbStore<TEntity, TDbConnection, TDbCommand, TDbDataAdapter, TDbParameter> Where(Expression<Func<TEntity, bool>> predicate)
        {
            MemberExpression memberExpression = null;
            object value = DBNull.Value;

            if (predicate.Body.NodeType == ExpressionType.Convert)
            {
                memberExpression = ((UnaryExpression)predicate.Body).Operand as MemberExpression;
            }
            else if (predicate.Body.NodeType == ExpressionType.MemberAccess)
            {
                memberExpression = predicate.Body as MemberExpression;
            }
            else if (predicate.Body.NodeType == ExpressionType.Equal)
            {
                var binaryExpression = predicate.Body as BinaryExpression;

                memberExpression = binaryExpression.Left as MemberExpression;

                value = Value(binaryExpression.Right);
            }

            if (memberExpression == null) throw new ArgumentException("predicate");

            var propertyInfo = memberExpression.Member;

            if (propertyInfo == null) throw new ArgumentException(string.Format("{0} is not a property in class {1}", memberExpression.Member.Name, typeof(TEntity).FullName), "predicate");

            var propertyName = propertyInfo.Name;

            var columnName = Configuration
                .Where(c => c.PropertyName == propertyName)
                .Select(c => c.DbName)
                .SingleOrDefault();

            if (columnName == null) throw new ArgumentException(string.Format("Property {0} is not mapped to a table column.", propertyName), "predicate");

            if (value == null) value = DBNull.Value;
            else
            {
                var valueType = value.GetType();
                if (valueType.IsGenericType)
                {
                    var valueDefinition = valueType.GetGenericTypeDefinition();
                    if (valueDefinition == typeof(Nullable<>))
                    {
                        if (valueType.GetProperty("HasValue").GetValue(value) == (object)true)
                        {
                            value = valueType.GetProperty("Value").GetValue(value);
                        }
                        else
                        {
                            value = DBNull.Value;
                        }
                    }
                    else
                    {
                        value = DBNull.Value;
                    }
                }
            }

            if (value == (object)Guid.Empty) value = DBNull.Value;

            Filter(DbSelectCommand, columnName).Value = value;

            return this;
        }

        protected virtual TDbParameter CreateParameter(string columnName)
        {
            var param = new TDbParameter();

            param.ParameterName = '@' + columnName;

            param.SourceColumn = columnName;
            param.SourceVersion = DataRowVersion.Current;

            return param;
        }

        protected virtual TDbParameter Filter(TDbCommand dbCommand, string columnName)
        {
            var paramName = '@' + columnName;
            TDbParameter param;

            if (dbCommand.Parameters.Contains(paramName))
                param = (TDbParameter)dbCommand.Parameters[paramName];
            else
            {
                param = CreateParameter(columnName);

                var rightExpr = paramName;

                dbCommand.Parameters.Add(param);

                var text = dbCommand.CommandText;

                var expr = columnName + '=' + rightExpr;

                var idx = text.IndexOf("WHERE", StringComparison.OrdinalIgnoreCase);

                text += (idx == -1 ? " WHERE " : " AND ") + expr;

                dbCommand.CommandText = text;
            }

            return param;
        }

        protected TDbParameter Filter(TDbCommand dbCommand, DataColumn column)
        {
            return Filter(dbCommand, column.ColumnName);
        }

        protected void Filter(TDbCommand dbCommand, IEnumerable<DataColumn> columns)
        {
            foreach (var col in columns)
                Filter(dbCommand, col);
        }

        protected static string MakeFieldList(params DataColumn[] columns)
        {
            return string.Join(", ", columns.Select(col => col.ColumnName));
        }

        protected static string MakeQuestionList(int count)
        {
            return string.Join(", ", new string('?', count).ToCharArray());
        }

        protected static string MakeValueList(params DataColumn[] columns)
        {
            return '@' + string.Join(", @", columns.Select(col => col.ColumnName));
        }

        protected static string MakeSetList(params DataColumn[] columns)
        {
            return string.Join(", ", columns.Select(col => col.ColumnName + "=@" + col.ColumnName));
        }

        void IStore<TEntity>.Create(TEntity entity)
        {
            Create(entity);
        }

        void IStore<TEntity>.Delete(TEntity entity)
        {
            Delete(entity);
        }

        IStore<TEntity> IStore<TEntity>.Page(string orderBy, int limit, int offset)
        {
            return Page(orderBy, limit, offset);
        }

        IStore<TEntity> IStore<TEntity>.Where(Expression<Func<TEntity, bool>> predicate)
        {
            return Where(predicate);
        }

        void IStore<TEntity>.Update(TEntity entity)
        {
            Update(entity);
        }

        public TEntity GetById(object id)
        {
            IQueryProvider provider = ConnectionProvider;

            if (provider != null)
            {
                var keys = id as IEnumerable<object>;

                if (keys == null) keys = new object[] { id };

                Expression query = _dbConfiguration.GetPrimaryKeyQuery(_query.Expression, keys.Select(v => Expression.Constant(v)).ToArray());

                return provider.Execute<TEntity>(query);
            }

            return default(TEntity);
        }

        int IEntityStore<TEntity>.Insert(TEntity instance)
        {
            return Updatable.Insert(this, instance);
        }

        int IEntityStore<TEntity>.Update(TEntity instance)
        {
            return Updatable.Update(this, instance);
        }

        int IEntityStore<TEntity>.Delete(TEntity instance)
        {
            return Updatable.Delete(this, instance);
        }

        int IEntityStore<TEntity>.InsertOrUpdate(TEntity instance)
        {
            return Updatable.InsertOrUpdate(this, instance);
        }

        Type IQueryable.ElementType
        {
            get { return _query.ElementType; }
        }

        Expression IQueryable.Expression
        {
            get { return _query.Expression; }
        }

        IQueryProvider IQueryable.Provider
        {
            get { return _query.Provider; }
        }

        IEntityProvider IEntityStore.Provider
        {
            get { return ConnectionProvider; }
        }

        object IEntityStore.GetById(object id)
        {
            return GetById(id);
        }

        int IEntityStore.Insert(object instance)
        {
            return Updatable.Insert(this, (TEntity)instance);
        }

        int IEntityStore.Update(object instance)
        {
            return Updatable.Update(this, (TEntity)instance);
        }

        int IEntityStore.Delete(object instance)
        {
            return Updatable.Delete(this, (TEntity)instance);
        }

        int IEntityStore.InsertOrUpdate(object instance)
        {
            return Updatable.InsertOrUpdate(this, (TEntity)instance);
        }

        Linq.IEntityTypeMapping IHaveMappingEntity.Entity
        {
            get { return _dbConfiguration; }
        }
    }
}
