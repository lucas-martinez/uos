﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Persistence
{
    using Uos.Inquiry;
    using Uos.Persistence.Configuration;
    using Uos.Persistence.Linq;

    public class EntityStore<TEntity> : Query<TEntity>, IEntityStore<TEntity>, IHaveMappingEntity
        where TEntity : class, new()
    {
        private EntityTypeConfiguration<TEntity> _configuration;
        private DbProvider _provider;
        
        public EntityStore(DbProvider provider, EntityTypeConfiguration<TEntity> configuration)
            : base(provider, typeof(IEntityStore<TEntity>))
        {
            _provider = provider;
            _configuration = configuration;
        }

        public EntityTypeConfiguration<TEntity> Configuration
        {
            get { return _configuration as EntityTypeConfiguration<TEntity>; }
        }

        string IEntityStore.DbName
        {
            get { return _configuration.DbName; }
        }

        IEntityTypeMapping IHaveMappingEntity.Entity
        {
            get { return _configuration; }
        }

        new public IEntityProvider Provider
        {
            get { return _provider; }
        }

        public Type EntityType
        {
            get { return typeof(TEntity); }
        }

        public TEntity GetById(object id)
        {
            var dbProvider = Provider;

            if (dbProvider != null)
            {
                var keys = id as IEnumerable<object>;
                
                if (keys == null) keys = new object[] { id };

                Expression query = _configuration.GetPrimaryKeyQuery(Expression, keys.Select(v => Expression.Constant(v)).ToArray());
                
                return Provider.Execute<TEntity>(query);
            }

            return default(TEntity);
        }

        object IEntityStore.GetById(object id)
        {
            return this.GetById(id);
        }

        public int Insert(TEntity instance)
        {
            return Updatable.Insert(this, instance);
        }

        int IEntityStore.Insert(object instance)
        {
            return this.Insert((TEntity)instance);
        }

        public int Delete(TEntity instance)
        {
            return Updatable.Delete(this, instance);
        }

        int IEntityStore.Delete(object instance)
        {
            return this.Delete((TEntity)instance);
        }

        public int Update(TEntity instance)
        {
            return Updatable.Update(this, instance);
        }

        int IEntityStore.Update(object instance)
        {
            return this.Update((TEntity)instance);
        }

        public int InsertOrUpdate(TEntity instance)
        {
            return Updatable.InsertOrUpdate(this, instance);
        }

        int IEntityStore.InsertOrUpdate(object instance)
        {
            return this.InsertOrUpdate((TEntity)instance);
        }
    }
}
