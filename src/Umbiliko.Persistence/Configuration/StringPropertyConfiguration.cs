﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Persistence.Configuration
{
    using Uos.Reflection;
    
    public class StringPropertyConfiguration<TComplex> : PropertyConfiguration<TComplex>
        where TComplex : class, new()
    {
        private readonly bool _fixedLength;
        private readonly int? _maxLength;
        private readonly bool _unicode;

        public StringPropertyConfiguration(ComplexTypeConfiguration<TComplex> typeConfiguration, Expression<Func<TComplex, char>> property, string dbName, bool unicode)
            : base(typeConfiguration, property.GetComplexPropertyAccess(), dbName, GetDbType(unicode, true), GetSqlDbType(unicode, true), false)
        {
            _fixedLength = true;
            _maxLength = 1;
            _unicode = unicode;
        }

        public StringPropertyConfiguration(ComplexTypeConfiguration<TComplex> typeConfiguration, Expression<Func<TComplex, Nullable<char>>> property, string dbName, bool unicode)
            : base(typeConfiguration, property.GetComplexPropertyAccess(), dbName, GetDbType(unicode, true), GetSqlDbType(unicode, true), true)
        {
            _fixedLength = true;
            _maxLength = 1;
            _unicode = unicode;
        }

        public StringPropertyConfiguration(ComplexTypeConfiguration<TComplex> typeConfiguration, Expression<Func<TComplex, string>> property, string dbName, bool nullable, int? maxLength, bool unicode, bool fixedLength)
            : base(typeConfiguration, property.GetComplexPropertyAccess(), dbName, GetDbType(unicode, fixedLength), GetSqlDbType(unicode, fixedLength), nullable)
        {
            _fixedLength = fixedLength;
            _maxLength = maxLength;
            _unicode = unicode;
        }

        protected override int DbLength
        {
            get { return _maxLength ?? base.DbLength; }
        }

        public bool FixedLength
        {
            get { return _fixedLength; }
        }

        public int? MaxLength
        {
            get { return _maxLength; }
        }

        public override PropertyTypes PropertyFlags
        {
            get { return Nullable ? PropertyTypes.Optional : PropertyTypes.Required; }
        }

        public bool Unicode
        {
            get { return _unicode; }
        }

        private static DbType GetDbType(bool unicode, bool fixedLength)
        {
            return unicode ? (fixedLength ? DbType.StringFixedLength : DbType.String) : (fixedLength ? DbType.AnsiStringFixedLength : DbType.AnsiString);
        }

        private static SqlDbType GetSqlDbType(bool unicode, bool fixedLength)
        {
            return unicode ? (fixedLength ? SqlDbType.NChar : SqlDbType.NVarChar) : (fixedLength ? SqlDbType.Char : SqlDbType.VarChar);
        }
    }
}
