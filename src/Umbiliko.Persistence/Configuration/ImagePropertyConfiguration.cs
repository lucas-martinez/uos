﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Persistence.Configuration
{
    using Uos.Reflection;

    public class ImagePropertyConfiguration<TComplex> : PropertyConfiguration<TComplex>
        where TComplex : class, new()
    {
        public ImagePropertyConfiguration(ComplexTypeConfiguration<TComplex> typeConfiguration, Expression<Func<TComplex, Image>> property, string dbName, bool nullable)
            : base(typeConfiguration, property.GetComplexPropertyAccess(), dbName, "image", nullable)
        {
        }
    }
}
