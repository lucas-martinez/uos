﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Persistence.Configuration
{
    using Uos.Reflection;

    public class NavigationPropertyConfiguration<TEntity, TProperty> : PropertyConfiguration<TEntity>
        where TEntity : class, new()
        where TProperty : class, new()
    {
        public NavigationPropertyConfiguration(EntityTypeConfiguration<TEntity> typeConfiguration, Expression<Func<TEntity, ICollection<TProperty>>> navigationProperty)
            : base(typeConfiguration, navigationProperty.GetComplexPropertyAccess(), null, null, false)
        {
        }

        public override string DbName
        {
            get { return null; }
        }

        public override PropertyTypes PropertyFlags
        {
            get { return PropertyTypes.Many; }
        }
    }
}
