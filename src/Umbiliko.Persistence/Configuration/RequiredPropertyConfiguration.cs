﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Persistence.Configuration
{
    using Uos.Reflection;

    public class RequiredPropertyConfiguration<TComplex, TProperty> : PropertyConfiguration<TComplex>
        where TComplex : class, new()
        where TProperty : struct
    {
        public RequiredPropertyConfiguration(ComplexTypeConfiguration<TComplex> typeConfiguration, Expression<Func<TComplex, TProperty>> property, string dbName)
            : base(typeConfiguration, property.GetComplexPropertyAccess(), dbName, false)
        {
        }

        public override PropertyTypes PropertyFlags
        {
            get { return PropertyTypes.Required; }
        }
    }
}
