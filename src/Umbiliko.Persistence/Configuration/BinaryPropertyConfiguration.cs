﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Persistence.Configuration
{
    using Uos.Reflection;

    public class BinaryPropertyConfiguration<TComplex> : PropertyConfiguration<TComplex>
        where TComplex : class, new()
    {
        private readonly int? _maxLength;
        private readonly bool _fixedLength;

        public BinaryPropertyConfiguration(ComplexTypeConfiguration<TComplex> typeConfiguration, Expression<Func<TComplex, byte[]>> property, string dbName, bool nullable, int? maxLength, bool fixedLength)
            : base(typeConfiguration, property.GetComplexPropertyAccess(), dbName, GetDbType(fixedLength), GetSqlDbType(fixedLength), nullable)
        {
            _fixedLength = fixedLength;
            _maxLength = maxLength;
        }

        protected override int DbLength
        {
            get { return MaxLength; }
        }

        public bool FixedLength
        {
            get { return _fixedLength; }
        }

        public int MaxLength
        {
            get { return _maxLength ?? Int32.MaxValue; }
        }

        private static DbType GetDbType(bool fixedLength)
        {
            return DbType.Binary;
        }

        private static SqlDbType GetSqlDbType(bool fixedLength)
        {
            return fixedLength ? SqlDbType.Binary : SqlDbType.VarBinary;
        }
    }
}
