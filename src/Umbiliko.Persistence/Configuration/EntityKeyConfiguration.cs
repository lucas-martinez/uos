﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Persistence.Configuration
{
    public abstract class EntityKeyConfiguration<TEntity>
        where TEntity : class, new()
    {
        public abstract void Apply(DataTable dataTable);

        public abstract void Apply<TDbConnection, TDbCommand, TDbDataAdapter, TDbParameter>(DbStore<TEntity, TDbConnection, TDbCommand, TDbDataAdapter, TDbParameter> dbStore)
            where TDbConnection : System.Data.Common.DbConnection, new()
            where TDbCommand : System.Data.Common.DbCommand, new()
            where TDbDataAdapter : System.Data.Common.DbDataAdapter, new()
            where TDbParameter : System.Data.Common.DbParameter, new();

        //public abstract void Apply(System.Web.Http.OData.Builder.EntitySetConfiguration<TEntity> target);
    }

    public class EntityKeyConfiguration<TEntity, TProperty> : EntityKeyConfiguration<TEntity>
        where TEntity : class, new()
    {
        private Expression<Func<TEntity, TProperty>> _key;

        public EntityKeyConfiguration(Expression<Func<TEntity, TProperty>> key)
        {
            _key = key;
        }

        public Expression<Func<TEntity, TProperty>> Key
        {
            get { return _key; }
        }

        public override void Apply(DataTable dataTable)
        {
        }

        public override void Apply<TDbConnection, TDbCommand, TDbDataAdapter, TDbParameter>(DbStore<TEntity, TDbConnection, TDbCommand, TDbDataAdapter, TDbParameter> dbStore)
        {
            Apply(dbStore.DataTable);
        }
    }
}
