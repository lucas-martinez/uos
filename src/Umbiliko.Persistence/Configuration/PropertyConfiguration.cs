﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Persistence.Configuration
{
    using Uos.Implementation;
using Uos.Persistence.Linq;
using Uos.Reflection;
    
    public abstract class PropertyConfiguration<TComplex> : IQueryType
        where TComplex : class, new()
    {
        private string _dbName;
        private DbType _dbType;
        private string _dbTypeName;
        //private readonly EntityConfiguration<TEntity> _entityConfig;
        private Delegate _getter;
        private bool _nullable;
        private string _propertyName;
        private string _propertyString;
        private readonly PropertyPath _propertyPath;
        private readonly Type _propertyType;
        private Delegate _setter;
        private readonly SqlDbType _sqlDbType;
        private readonly ComplexTypeConfiguration<TComplex> _typeConfiguration;

        protected PropertyConfiguration(ComplexTypeConfiguration<TComplex> typeConfiguration, PropertyPath propertyPath, string dbName, bool nullable)
        {
            _dbName = dbName;
            _dbTypeName = null;
            _nullable = nullable;
            _propertyPath = propertyPath;
            _sqlDbType = SqlDbType.Udt;
            _typeConfiguration = typeConfiguration;
        }

        protected PropertyConfiguration(ComplexTypeConfiguration<TComplex> typeConfiguration, PropertyPath propertyPath, string dbName, string dbTypeName, bool nullable)
        {
            _dbName = dbName;
            _dbType = DbType.Object;
            _dbTypeName = dbTypeName;
            _nullable = nullable;
            _propertyPath = propertyPath;
            _sqlDbType = SqlDbType.Udt;
            _typeConfiguration = typeConfiguration;
        }

        protected PropertyConfiguration(ComplexTypeConfiguration<TComplex> typeConfiguration, PropertyPath propertyPath, string dbName, DbType dbType, SqlDbType sqlDbType, bool nullable)
        {
            _dbName = dbName;
            _dbType = dbType;
            _dbTypeName = sqlDbType.GetType().ToString();
            _nullable = nullable;
            _propertyPath = propertyPath;
            _typeConfiguration = typeConfiguration;
        }

        protected virtual int DbLength
        {
            get { return -1; }
        }

        public virtual string DbName
        {
            get { return _dbName ?? (_dbName = string.Join("_", _propertyPath.Select(p => p.Name.Flatten('_')))); }
        }

        protected virtual short DbPrecision
        {
            get { return -1; }
        }

        protected virtual short DbScale
        {
            get { return -1; }
        }

        public DbType DbType
        {
            get { return _dbType; }
        }

        public string DbTypeName
        {
            get { return _dbTypeName; }
        }

        protected virtual bool IsVariableLength
        {
            get { return false; }
        }

        public Delegate Getter
        {
            get { return _getter ?? (_getter = BuildPropertyLambda(true).Compile()); }
        }

        public bool Nullable
        {
            get { return _nullable; }
            protected set { _nullable = value; }
        }

        public virtual PropertyTypes PropertyFlags
        {
            get { return _nullable ? PropertyTypes.Optional : PropertyTypes.Required; }
        }

        public PropertyInfo PropertyInfo
        {
            get { return _propertyPath.First(); }
        }

        public string PropertyName
        {
            get { return _propertyName ?? (_propertyName = PropertyInfo.Name); }
        }

        public string GetPropertyString(string separator)
        {
            return string.Join(separator, _propertyPath.Select(p => p.Name));
        }

        public PropertyPath PropertyPath
        {
            get { return _propertyPath; }
        }

        public Type PropertyType
        {
            get { return PropertyInfo.PropertyType; }
        }

        public Delegate Setter
        {
            get { return _getter ?? (_setter = BuildPropertyLambda(false).Compile()); }
        }

        public ComplexTypeConfiguration<TComplex> TypeConfiguration
        {
            get { return _typeConfiguration; }
        }

        public virtual void Set(object instance, object value)
        {
            Setter.DynamicInvoke(instance, value);
        }

        public virtual object Get(object instance)
        {
            return Getter.DynamicInvoke(instance);
        }

        public virtual void Update(object target, DataRow source)
        {
            object value = source[DbName];

            if (value == DBNull.Value) value = null;

            Set(target, source[DbName]);
        }

        /// <summary>
        /// Copy value from object to DataRow
        /// </summary>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="source"></param>
        /// <param name="target"></param>
        public virtual void Update(DataRow target, object source)
        {
            var value = Getter.DynamicInvoke(source);

            target[DbName] = value;
        }

        public override string ToString()
        {
            return _propertyString ?? (_propertyString = GetPropertyString("."));
        }

        private LambdaExpression BuildPropertyLambda(bool getter)
        {
            var entityType = typeof(TComplex);

            ParameterExpression initialObjectParameterExpression = Expression.Parameter(entityType, entityType.Name);

            Expression pathExpression = initialObjectParameterExpression;

            foreach (string property in PropertyPath.Select(p => p.Name))
                pathExpression = Expression.PropertyOrField(pathExpression, property);

            LambdaExpression resultExpression;
            if (getter)
            {
                resultExpression = Expression.Lambda(
                    getGenericGetFunction(entityType, pathExpression.Type), // This makes it work for valueTypes.
                    pathExpression,
                    initialObjectParameterExpression);
            }
            else
            {
                ParameterExpression assignParameterExpression = Expression.Parameter(pathExpression.Type);
                BinaryExpression assginExpression = Expression.Assign(pathExpression, assignParameterExpression);

                resultExpression = Expression.Lambda(
                    getGenericSetAction(entityType, assginExpression.Type), // This makes it work for valueTypes.
                    assginExpression,
                    initialObjectParameterExpression,
                    assignParameterExpression);
            }

            return resultExpression;
        }

        private static Type getGenericGetFunction(Type param1, Type param2)
        {
            return typeof(Func<,>).MakeGenericType(param1, param2);
        }

        private static Type getGenericSetAction(Type param1, Type param2)
        {
            return typeof(Action<,>).MakeGenericType(param1, param2);
        }

        #region IQueryType

        bool IQueryType.IsVariableLength
        {
            get { return IsVariableLength; }
        }

        int IQueryType.Length
        {
            get { return DbLength; }
        }

        IEntityTypeMapping IQueryType.Mapping
        {
            get
            {
                var mapping = _typeConfiguration as IEntityTypeMapping;

                return mapping;
            }
        }

        bool IQueryType.NotNull
        {
            get { return !_nullable; }
        }

        short IQueryType.Precision
        {
            get { return DbPrecision; }
        }

        short IQueryType.Scale
        {
            get { return DbScale; }
        }

        SqlDbType IQueryType.SqlDbType
        {
            get { return _sqlDbType; }
        }

        #endregion
    }
}
