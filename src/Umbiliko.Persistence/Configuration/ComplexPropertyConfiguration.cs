﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Persistence.Configuration
{
    using Uos.Reflection;

    public class ComplexPropertyConfiguration<TEntity, TProperty> : PropertyConfiguration<TEntity>
        where TEntity : class, new()
        where TProperty : class, new()
    {
        private readonly ComplexTypeConfiguration<TProperty> _typeConfiguration;

        public ComplexPropertyConfiguration(EntityTypeConfiguration<TEntity> typeConfiguration, Expression<Func<TEntity, TProperty>> property, string dbName = null)
            : base(typeConfiguration, property.GetComplexPropertyAccess(), dbName, null, false)
        {
            _typeConfiguration = new ComplexTypeConfiguration<TProperty>();
        }

        public override PropertyTypes PropertyFlags
        {
            get { return PropertyTypes.Complex; }
        }

        public ComplexTypeConfiguration<TProperty> TypeConfiguration
        {
            get { return _typeConfiguration; }
        }
    }
}
