﻿// Copyright (c) Microsoft Corporation.  All rights reserved.
// This source code is made available under the terms of the Microsoft Public License (MS-PL)

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace Uos.Persistence.Configuration
{
    using Uos.Persistence.Linq;

    /// <summary>
    /// PropertyConfiguration
    /// </summary>
    public interface IQueryType
    {
        string DbName { get; }

        DbType DbType { get; }

        bool IsVariableLength { get; }

        int Length { get; }

        IEntityTypeMapping Mapping { get; }

        bool NotNull { get; }

        short Precision { get; }

        Type PropertyType { get; }
        
        short Scale { get; }
        
        SqlDbType SqlDbType { get; }
    }
}