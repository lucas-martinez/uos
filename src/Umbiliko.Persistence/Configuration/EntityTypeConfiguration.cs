﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Persistence.Configuration
{
    using Uos.Implementation;
    using Uos.Persistence.Linq;

    public class EntityTypeConfiguration<TEntity> : ComplexTypeConfiguration<TEntity>, IEntityTypeMapping
        where TEntity : class, new()
    {
        private readonly DbConfiguration _db;
        private EntityKeyConfiguration<TEntity> _primaryKey;
        
        private readonly string _entitySetName;
        private readonly string _schema;
        private readonly string _tableName;

        private static string Pluralize(string name)
        {
            return name;
        }
        
        public EntityTypeConfiguration(DbConfiguration db)
            : this(db, Pluralize(typeof(TEntity).Name))
        {
        }

        public EntityTypeConfiguration(DbConfiguration db, string entitySetName)
            : this(db, entitySetName, entitySetName.Flatten('_'), typeof(TEntity).Namespace.Split('.').Last().Flatten('_'))
        {
        }

        public EntityTypeConfiguration(DbConfiguration db, string tableName, string schema)
            : this(db, Pluralize(typeof(TEntity).Name), tableName, schema)
        {
        }

        public EntityTypeConfiguration(DbConfiguration db, string entitySetName, string tableName, string schema)
        {
            _db = db;
            _entitySetName = entitySetName;
            _schema = schema != null ? schema.ToLowerInvariant() : null;
            _tableName = tableName;
        }

        public string EntitySetName
        {
            get { return _entitySetName; }
        }

        public string SchemaName
        {
            get { return _schema; }
        }

        public string DbName
        {
            get { return _tableName; }
        }

        public NavigationPropertyConfiguration<TEntity, TItem> AddMany<TItem>(Expression<Func<TEntity, ICollection<TItem>>> property)
            where TItem : class, new()
        {
            return new NavigationPropertyConfiguration<TEntity, TItem>(this, property);
        }

        public ReferencePropertyConfiguration<TEntity, TTarget, TTargetKey> AddOptional<TTarget, TTargetKey>(Expression<Func<TEntity, TTarget>> property, Expression<Func<TTarget, TTargetKey>> foreignKey)
            where TTarget : class, new()
            where TTargetKey : struct
        {
            return new ReferencePropertyConfiguration<TEntity, TTarget, TTargetKey>(this, referenceProperty: property, keyProperty: foreignKey, nullable: true);
        }

        public ReferencePropertyConfiguration<TEntity, TTarget, TTargetKey> AddRequired<TTarget, TTargetKey>(Expression<Func<TEntity, TTarget>> property, Expression<Func<TTarget, TTargetKey>> foreignKey)
            where TTarget : class, new()
            where TTargetKey : struct
        {
            return new ReferencePropertyConfiguration<TEntity, TTarget, TTargetKey>(this, referenceProperty: property, keyProperty: foreignKey, nullable: false);
        }

        public virtual void Apply(DataTable dataTable)
        {

        }

        public virtual void Apply<TDbConnection, TDbCommand, TDbDataAdapter, TDbParameter>(DbStore<TEntity, TDbConnection, TDbCommand, TDbDataAdapter, TDbParameter> dbStore)
            where TDbConnection : System.Data.Common.DbConnection, new()
            where TDbCommand : System.Data.Common.DbCommand, new()
            where TDbDataAdapter : System.Data.Common.DbDataAdapter, new()
            where TDbParameter : System.Data.Common.DbParameter, new()
        {
        }
        
        public override TEntity Create(DataRow dataRow)
        {
            var entity = base.Create(dataRow);

            return entity;
        }

        public EntityTypeConfiguration<TEntity> SetCompoundKey<TEntityKey>(Expression<Func<TEntity, TEntityKey>> key)
            where TEntityKey : class
        {
            _primaryKey = new CompoundKeyConfiguration<TEntity, TEntityKey>(key);

            return this;
        }

        public EntityTypeConfiguration<TEntity> SetIdentityKey(Expression<Func<TEntity, int>> property, string dbName = null)
        {
            _primaryKey = new IdentityKeyConfiguration<TEntity, int>(property);

            var propertyConfig = new RequiredPropertyConfiguration<TEntity, int>(this, property, dbName);

            return this;
        }

        public EntityTypeConfiguration<TEntity> SetPrimaryKey(Expression<Func<TEntity, string>> property, string dbName = null, int? maxLength = null, bool unicode = true, bool fixedLength = false)
        {
            _primaryKey = new EntityKeyConfiguration<TEntity, string>(property);

            var propertyConfig = new StringPropertyConfiguration<TEntity>(this, property, dbName, false, maxLength, unicode, fixedLength);

            return this;
        }

        public EntityTypeConfiguration<TEntity> SetPrimaryKey<TProperty>(Expression<Func<TEntity, TProperty>> property, string dbName = null)
            where TProperty : struct
        {
            _primaryKey = new EntityKeyConfiguration<TEntity, TProperty>(property);

            var propertyConfig = new RequiredPropertyConfiguration<TEntity, TProperty>(this, property, dbName);

            return this;
        }

        public override void Update(TEntity target, DataRow source)
        {
            foreach (var property in Properties)
            {
                property.Update(target: target, source: source);
            }
        }

        public override void Update(DataRow target, TEntity source)
        {
            foreach (var property in Properties)
            {
                property.Update(target: target, source: source);
            }
        }

        // PK_ for primary keys
        // UK_ for unique keys
        // IX_ for non clustered non unique indexes
        // UX_ for unique indexes
        // <index or key type>_<table name>_<column 1>_<column 2>_<column n>
        private const string PrimaryKeyType = "PK";
        private const string UniqueKeyType = "UK";
        private const string NonUniqueIndexType = "IX";
        private const string UniqueIndexType = "UX";

        string FormatIndex(string type, params string[] dbNames)
        {
            var name = new List<string> { type };

            if (_schema != null) name.Add(_schema);

            name.Add(DbName);

            name.AddRange(dbNames);

            return string.Join("_", name);
        }

        protected virtual TEntity CloneEntity(TEntity instance)
        {
            throw new NotImplementedException();
        }

        protected virtual IEnumerable<MemberInfo> GetPrimaryKeyMembers()
        {
            throw new NotImplementedException();
        }

        #region IEntityTypeMapping

        Type IEntityTypeMapping.ElementType
        {
            get { return typeof(TEntity); }
        }

        Type IEntityTypeMapping.EntityType
        {
            get { return typeof(TEntity); }
        }

        IEnumerable<MemberInfo> IEntityTypeMapping.GetPrimaryKeyMembers()
        {
            return GetPrimaryKeyMembers();
        }

        bool IEntityTypeMapping.IsPrimaryKey(MemberInfo member)
        {
            return GetPrimaryKeyMembers().Any(m => m == member);
        }

        object IEntityTypeMapping.GetPrimaryKey(object instance)
        {
            throw new NotImplementedException();
        }

        public Expression GetPrimaryKeyQuery(Expression source, Expression[] keys)
        {
            throw new NotImplementedException();
        }

        IEnumerable<EntityInfo> IEntityTypeMapping.GetDependentEntities(object instance)
        {
            return new EntityInfo[] { };
        }

        IEnumerable<EntityInfo> IEntityTypeMapping.GetDependingEntities(object instance)
        {
            return new EntityInfo[] { };
        }

        object IEntityTypeMapping.CloneEntity(object instance)
        {
            throw new NotImplementedException();
        }

        IEntityTypeMapper IEntityTypeMapping.CreateMapper()
        {
            return new EntityTypeMapper(this);
        }

        bool IEntityTypeMapping.IsRelationship(MemberInfo member)
        {
            return false;
        }

        IEntityTypeMapping IEntityTypeMapping.GetRelatedEntity(MemberInfo member)
        {
            return null;
        }

        bool IEntityTypeMapping.IsAssociationRelationship(MemberInfo member)
        {
            return false;
        }

        IEnumerable<MemberInfo> IEntityTypeMapping.GetAssociationKeyMembers(MemberInfo member)
        {
            throw new NotImplementedException();
        }

        IEnumerable<MemberInfo> IEntityTypeMapping.GetAssociationRelatedKeyMembers(MemberInfo member)
        {
            throw new NotImplementedException();
        }

        bool IEntityTypeMapping.IsRelationshipSource(MemberInfo member)
        {
            throw new NotImplementedException();
        }

        bool IEntityTypeMapping.IsRelationshipTarget(MemberInfo member)
        {
            throw new NotImplementedException();
        }

        bool IEntityTypeMapping.IsNestedEntity(MemberInfo member)
        {
            throw new NotImplementedException();
        }

        string IEntityTypeMapping.GetAlias()
        {
            return DbName;
        }

        bool IEntityTypeMapping.IsExtensionTable()
        {
            return false;
        }

        string IEntityTypeMapping.GetExtensionRelatedAlias()
        {
            return DbName;
        }

        IEnumerable<string> IEntityTypeMapping.GetExtensionKeyColumnNames()
        {
            return new string[] { };
        }

        IEnumerable<MemberInfo> IEntityTypeMapping.GetExtensionRelatedMembers()
        {
            return new MemberInfo[] { };
        }

        #endregion IEntityTypeMapping

        class EntityTypeMapper : IEntityTypeMapper
        {
            private EntityTypeConfiguration<TEntity> _configuration;

            public EntityTypeMapper(EntityTypeConfiguration<TEntity> configuration)
            {
                _configuration = configuration;
            }
        }
    }
}
