﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Persistence.Configuration
{
    using Uos.Persistence.Linq;

    public interface IHaveMappingEntity
    {
        IEntityTypeMapping Entity { get; }
    }
}
