﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Persistence.Configuration
{
    using Uos.Reflection;

    public class ReferencePropertyConfiguration<TEntity, TTarget, TTargetKey> : PropertyConfiguration<TEntity>
        where TEntity : class, new()
        where TTarget : class, new()
        where TTargetKey : struct
    {
        public ReferencePropertyConfiguration(EntityTypeConfiguration<TEntity> typeConfiguration, Expression<Func<TEntity, TTarget>> referenceProperty, Expression<Func<TTarget, TTargetKey>> keyProperty, bool nullable)
            : base(typeConfiguration, referenceProperty.GetComplexPropertyAccess(), null, null, nullable)
        {
        }

        public override PropertyTypes PropertyFlags
        {
            get { return PropertyTypes.Reference | (Nullable ? PropertyTypes.Optional : PropertyTypes.Required); }
        }
    }
}
