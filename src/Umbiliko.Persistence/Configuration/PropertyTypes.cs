﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Persistence.Configuration
{
    [Flags]
    public enum PropertyTypes
    {
        Optional = 0,

        Required = 1,

        Reference = 2,

        Complex = 4,

        Many = Reference | Complex
    }
}
