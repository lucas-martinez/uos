﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Persistence.Configuration
{
    using Uos.Persistence.Linq;

    public struct EntityInfo
    {
        object _instance;
        IEntityTypeMapping _mapping;

        public EntityInfo(object instance, IEntityTypeMapping mapping)
        {
            _instance = instance;
            _mapping = mapping;
        }

        public object Instance
        {
            get { return _instance; }
        }

        public IEntityTypeMapping Mapping
        {
            get { return _mapping; }
        }
    }
}
