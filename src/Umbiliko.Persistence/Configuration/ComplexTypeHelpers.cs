﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace Uos.Persistence.Configuration
{
    public static class ComplexTypeHelpers
    {
        public static ComplexTypeConfiguration<TComplex> Optional<TComplex>(this ComplexTypeConfiguration<TComplex> configuration, Expression<Func<TComplex, string>> property, string dbName = null, int? maxLength = null, bool unicode = true, bool fixedLength = false)
            where TComplex : class, new()
        {
            configuration.AddOptional(property, dbName, maxLength, unicode, fixedLength);

            return configuration;
        }

        public static ComplexTypeConfiguration<TComplex> Optional<TComplex>(this ComplexTypeConfiguration<TComplex> configuration, Expression<Func<TComplex, char>> property, string dbName = null, bool unicode = true)
            where TComplex : class, new()
        {
            configuration.Add(new StringPropertyConfiguration<TComplex>(configuration, property, dbName, unicode));

            return configuration;
        }

        public static ComplexTypeConfiguration<TComplex> Optional<TComplex>(this ComplexTypeConfiguration<TComplex> configuration, Expression<Func<TComplex, char?>> property, string dbName = null, bool unicode = true)
            where TComplex : class, new()
        {
            configuration.Add(new StringPropertyConfiguration<TComplex>(configuration, property, dbName, unicode));

            return configuration;
        }

        public static ComplexTypeConfiguration<TComplex> Optional<TComplex>(this ComplexTypeConfiguration<TComplex> configuration, Expression<Func<TComplex, DateTime?>> property, string dbName = null, short presicion = 0)
            where TComplex : class, new()
        {
            configuration.Add(new DateTimePropertyConfiguration<TComplex>(configuration, property, dbName, presicion));

            return configuration;
        }

        public static ComplexTypeConfiguration<TComplex> Optional<TComplex>(this ComplexTypeConfiguration<TComplex> configuration, Expression<Func<TComplex, DateTimeOffset?>> property, string dbName = null, short presicion = 0)
            where TComplex : class, new()
        {
            configuration.Add(new DateTimePropertyConfiguration<TComplex>(configuration, property, dbName, presicion));

            return configuration;
        }

        public static ComplexTypeConfiguration<TComplex> Optional<TComplex>(this ComplexTypeConfiguration<TComplex> configuration, Expression<Func<TComplex, TimeSpan?>> property, string dbName = null, short presicion = 0)
            where TComplex : class, new()
        {
            configuration.Add(new DateTimePropertyConfiguration<TComplex>(configuration, property, dbName, presicion));

            return configuration;
        }

        public static ComplexTypeConfiguration<TComplex> Optional<TComplex>(this ComplexTypeConfiguration<TComplex> configuration, Expression<Func<TComplex, Image>> property, string dbName = null)
            where TComplex : class, new()
        {
            configuration.Add(new ImagePropertyConfiguration<TComplex>(configuration, property, dbName, false));

            return configuration;
        }

        public static ComplexTypeConfiguration<TComplex> Optional<TComplex>(this ComplexTypeConfiguration<TComplex> configuration, Expression<Func<TComplex, XmlNode>> property, string dbName = null)
            where TComplex : class, new()
        {
            configuration.Add(new XmlPropertyConfiguration<TComplex>(configuration, property, dbName, false));

            return configuration;
        }

        public static ComplexTypeConfiguration<TComplex> Optional<TComplex>(this ComplexTypeConfiguration<TComplex> configuration, Expression<Func<TComplex, XNode>> property, string dbName = null)
            where TComplex : class, new()
        {
            configuration.Add(new XmlPropertyConfiguration<TComplex>(configuration, property, dbName, false));

            return configuration;
        }

        public static ComplexTypeConfiguration<TComplex> Optional<TComplex, TProperty>(this ComplexTypeConfiguration<TComplex> configuration, Expression<Func<TComplex, TProperty?>> property, string dbName = null)
            where TComplex : class, new()
            where TProperty : struct
        {
            configuration.AddOptional(property, dbName);

            return configuration;
        }

        public static ComplexTypeConfiguration<TComplex> Required<TComplex>(this ComplexTypeConfiguration<TComplex> configuration, Expression<Func<TComplex, string>> property, string dbName = null, int? maxLength = null, bool unicode = true, bool fixedLength = false)
            where TComplex : class, new()
        {
            configuration.AddRequired(property, dbName, maxLength, unicode, fixedLength);

            return configuration;
        }

        public static ComplexTypeConfiguration<TComplex> Required<TComplex>(this ComplexTypeConfiguration<TComplex> configuration, Expression<Func<TComplex, DateTime>> property, string dbName = null, short presicion = 0)
            where TComplex : class, new()
        {
            configuration.Add(new DateTimePropertyConfiguration<TComplex>(configuration, property, dbName, presicion));

            return configuration;
        }

        public static ComplexTypeConfiguration<TComplex> Required<TComplex>(this ComplexTypeConfiguration<TComplex> configuration, Expression<Func<TComplex, DateTimeOffset>> property, string dbName = null, short presicion = 0)
            where TComplex : class, new()
        {
            configuration.Add(new DateTimePropertyConfiguration<TComplex>(configuration, property, dbName, presicion));

            return configuration;
        }

        public static ComplexTypeConfiguration<TComplex> Required<TComplex>(this ComplexTypeConfiguration<TComplex> configuration, Expression<Func<TComplex, TimeSpan>> property, string dbName = null, short presicion = 0)
            where TComplex : class, new()
        {
            configuration.Add(new DateTimePropertyConfiguration<TComplex>(configuration, property, dbName, presicion));

            return configuration;
        }

        public static ComplexTypeConfiguration<TComplex> Required<TComplex>(this ComplexTypeConfiguration<TComplex> configuration, Expression<Func<TComplex, Image>> property, string dbName = null)
            where TComplex : class, new()
        {
            configuration.Add(new ImagePropertyConfiguration<TComplex>(configuration, property, dbName, true));

            return configuration;
        }

        public static ComplexTypeConfiguration<TComplex> Required<TComplex>(this ComplexTypeConfiguration<TComplex> configuration, Expression<Func<TComplex, XmlNode>> property, string dbName = null)
            where TComplex : class, new()
        {
            configuration.Add(new XmlPropertyConfiguration<TComplex>(configuration, property, dbName, true));

            return configuration;
        }

        public static ComplexTypeConfiguration<TComplex> Required<TComplex>(this ComplexTypeConfiguration<TComplex> configuration, Expression<Func<TComplex, XNode>> property, string dbName = null)
            where TComplex : class, new()
        {
            configuration.Add(new XmlPropertyConfiguration<TComplex>(configuration, property, dbName, true));

            return configuration;
        }

        public static ComplexTypeConfiguration<TComplex> Required<TComplex, TProperty>(this ComplexTypeConfiguration<TComplex> configuration, Expression<Func<TComplex, TProperty>> property, string dbName = null)
            where TComplex : class, new()
            where TProperty : struct
        {
            configuration.AddRequired(property, dbName);

            return configuration;
        }
    }
}
