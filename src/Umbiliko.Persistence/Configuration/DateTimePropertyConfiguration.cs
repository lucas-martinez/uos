﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Persistence.Configuration
{
    using Uos.Reflection;

    public class DateTimePropertyConfiguration<TComplex> : PropertyConfiguration<TComplex>
        where TComplex : class, new()
    {
        private short _precision;

        public DateTimePropertyConfiguration(ComplexTypeConfiguration<TComplex> typeConfiguration, Expression<Func<TComplex, TimeSpan>> property, string dbName, short precision)
            : base(typeConfiguration, property.GetComplexPropertyAccess(), dbName, GetTimeSpanDbType(precision), GetTimeSpanSqlDbType(precision), false)
        {
            _precision = precision;
        }

        public DateTimePropertyConfiguration(ComplexTypeConfiguration<TComplex> typeConfiguration, Expression<Func<TComplex, Nullable<TimeSpan>>> property, string dbName, short precision)
            : base(typeConfiguration, property.GetComplexPropertyAccess(), dbName, GetTimeSpanDbType(precision), GetTimeSpanSqlDbType(precision), true)
        {
            _precision = precision;
        }

        public DateTimePropertyConfiguration(ComplexTypeConfiguration<TComplex> typeConfiguration, Expression<Func<TComplex, DateTime>> property, string dbName, short precision)
            : base(typeConfiguration, property.GetComplexPropertyAccess(), dbName, GetDateTimeDbType(precision), GetDateTimeSqlDbType(precision), false)
        {
            _precision = precision;
        }

        public DateTimePropertyConfiguration(ComplexTypeConfiguration<TComplex> typeConfiguration, Expression<Func<TComplex, Nullable<DateTime>>> property, string dbName, short precision)
            : base(typeConfiguration, property.GetComplexPropertyAccess(), dbName, GetDateTimeDbType(precision), GetDateTimeSqlDbType(precision), true)
        {
            _precision = precision;
        }

        public DateTimePropertyConfiguration(ComplexTypeConfiguration<TComplex> typeConfiguration, Expression<Func<TComplex, DateTimeOffset>> property, string dbName, short precision)
            : base(typeConfiguration, property.GetComplexPropertyAccess(), dbName, GetDateTimeOffsetDbType(precision), GetDateTimeOffsetSqlDbType(precision), false)
        {
            _precision = precision;
        }

        public DateTimePropertyConfiguration(ComplexTypeConfiguration<TComplex> typeConfiguration, Expression<Func<TComplex, Nullable<DateTimeOffset>>> property, string dbName, short precision)
            : base(typeConfiguration, property.GetComplexPropertyAccess(), dbName, GetDateTimeOffsetDbType(precision), GetDateTimeOffsetSqlDbType(precision), true)
        {
            _precision = precision;
        }

        public short Presicion
        {
            get { return _precision; }
        }

        protected override short DbPrecision
        {
            get { return _precision; }
        }

        private static DbType GetDateTimeDbType(short presicion)
        {
            return DbType.DateTime2;
        }

        private static SqlDbType GetDateTimeSqlDbType(short presicion)
        {
            return SqlDbType.DateTime2;
        }

        private static DbType GetDateTimeOffsetDbType(short presicion)
        {
            return DbType.DateTimeOffset;
        }

        private static SqlDbType GetDateTimeOffsetSqlDbType(short presicion)
        {
            return SqlDbType.DateTimeOffset;
        }

        private static DbType GetTimeSpanDbType(short presicion)
        {
            return DbType.Time;
        }

        private static SqlDbType GetTimeSpanSqlDbType(short presicion)
        {
            return SqlDbType.Time;
        }
    }
}
