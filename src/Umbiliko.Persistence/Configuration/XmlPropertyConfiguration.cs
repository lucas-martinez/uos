﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace Uos.Persistence.Configuration
{
    using Uos.Reflection;

    public class XmlPropertyConfiguration<TComplex> : PropertyConfiguration<TComplex>
        where TComplex : class, new()
    {
        public XmlPropertyConfiguration(ComplexTypeConfiguration<TComplex> typeConfiguration, Expression<Func<TComplex, XmlNode>> property, string dbName, bool nullable)
            : base(typeConfiguration, property.GetComplexPropertyAccess(), dbName, DbType.Xml, SqlDbType.Xml, nullable)
        {
        }

        public XmlPropertyConfiguration(ComplexTypeConfiguration<TComplex> typeConfiguration, Expression<Func<TComplex, XNode>> property, string dbName, bool nullable)
            : base(typeConfiguration, property.GetComplexPropertyAccess(), dbName, DbType.Xml, SqlDbType.Xml, nullable)
        {
        }
    }
}
