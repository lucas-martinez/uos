﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Persistence.Configuration
{
    public static class EntityTypeHelpers
    {
        public static EntityTypeConfiguration<TEntity> CompoundKey<TEntity, TEntityKey>(this EntityTypeConfiguration<TEntity> configuration, Expression<Func<TEntity, TEntityKey>> key)
            where TEntity : class, new()
            where TEntityKey : class
        {
            configuration.SetCompoundKey(key);

            return configuration;
        }

        public static EntityTypeConfiguration<TEntity> IdentityKey<TEntity>(this EntityTypeConfiguration<TEntity> configuration, Expression<Func<TEntity, int>> property, string columnName = null)
            where TEntity : class, new()
        {
            //configuration.AddRequired(property, columnName);

            configuration.SetIdentityKey(property);

            return configuration;
        }

        public static EntityTypeConfiguration<TEntity> PrimaryKey<TEntity>(this EntityTypeConfiguration<TEntity> configuration, Expression<Func<TEntity, string>> property, string dbName = null, int? maxLength = null, bool unicode = true, bool fixedLength = false)
            where TEntity : class, new()
        {
            //configuration.AddRequired(property, columnName, maxLength, unicode, fixedLength);

            configuration.SetPrimaryKey(property, dbName, maxLength, unicode, fixedLength);

            return configuration;
        }

        public static EntityTypeConfiguration<TEntity> PrimaryKey<TEntity, TProperty>(this EntityTypeConfiguration<TEntity> configuration, Expression<Func<TEntity, TProperty>> property, string dbName = null)
            where TEntity : class, new()
            where TProperty : struct
        {
            //configuration.AddRequired(property, columnName);

            configuration.SetPrimaryKey(property);

            return configuration;
        }

        public static EntityTypeConfiguration<TEntity> Many<TEntity, TItem>(this EntityTypeConfiguration<TEntity> configuration, Expression<Func<TEntity, ICollection<TItem>>> property)
            where TEntity : class, new()
            where TItem : class, new()
        {
            configuration.AddMany(property);

            return configuration;
        }

        public static EntityTypeConfiguration<TEntity> Optional<TEntity, TTarget, TTargetKey>(this EntityTypeConfiguration<TEntity> configuration, Expression<Func<TEntity, TTarget>> property, Expression<Func<TTarget, TTargetKey>> foreighKey)
            where TEntity : class, new()
            where TTarget : class, new()
            where TTargetKey : struct
        {
            configuration.AddOptional(property: property, foreignKey: foreighKey);

            return configuration;
        }

        public static EntityTypeConfiguration<TEntity> Required<TEntity, TTarget, TTargetKey>(this EntityTypeConfiguration<TEntity> configuration, Expression<Func<TEntity, TTarget>> property, Expression<Func<TTarget, TTargetKey>> foreignKey)
            where TEntity : class, new()
            where TTarget : class, new()
            where TTargetKey : struct
        {
            configuration.AddRequired(property: property, foreignKey: foreignKey);

            return configuration;
        }

        public static EntityTypeConfiguration<TEntity> Timestamp<TEntity>(this EntityTypeConfiguration<TEntity> configuration, Expression<Func<TEntity, byte[]>> property, string dbName = null)
            where TEntity : class, new()
        {
            configuration.Add(new TimestampPropertyConfiguration<TEntity>(configuration, property, dbName));

            return configuration;
        }

        public static EntityTypeConfiguration<TEntity> Timestamp<TEntity>(this EntityTypeConfiguration<TEntity> configuration, Expression<Func<TEntity, long>> property, string dbName = null)
            where TEntity : class, new()
        {
            configuration.Add(new TimestampPropertyConfiguration<TEntity>(configuration, property, dbName));

            return configuration;
        }

        public static EntityTypeConfiguration<TEntity> Timestamp<TEntity>(this EntityTypeConfiguration<TEntity> configuration, Expression<Func<TEntity, ulong>> property, string dbName = null)
            where TEntity : class, new()
        {
            configuration.Add(new TimestampPropertyConfiguration<TEntity>(configuration, property, dbName));

            return configuration;
        }
        
    }
}
