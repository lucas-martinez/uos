﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Persistence.Configuration
{
    public class CompoundKeyConfiguration<TEntity, TEntityKey> : EntityKeyConfiguration<TEntity, TEntityKey>
        where TEntity : class, new()
        where TEntityKey: class
    {
        public CompoundKeyConfiguration(Expression<Func<TEntity, TEntityKey>> key)
            : base(key)
        {
        }

        public override void Apply(DataTable dataTable)
        {
            base.Apply(dataTable);
        }

        public override void Apply<TDbConnection, TDbCommand, TDbDataAdapter, TDbParameter>(DbStore<TEntity, TDbConnection, TDbCommand, TDbDataAdapter, TDbParameter> dbStore)
        {
            base.Apply(dbStore);
        }
    }
}
