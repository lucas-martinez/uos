﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Persistence.Configuration
{
    using Uos.Persistence.Linq;
    using System.Reflection;

    public class ComplexTypeConfiguration<TComplex> : IEnumerable<PropertyConfiguration<TComplex>>, IComplexTypeMapping
        where TComplex : class, new()
    {
        private readonly Dictionary<string, PropertyConfiguration<TComplex>> _properties;

        public ComplexTypeConfiguration()
        {
            _properties = new Dictionary<string, PropertyConfiguration<TComplex>>();
        }

        public ICollection<PropertyConfiguration<TComplex>> Properties
        {
            get { return _properties.Values; }
        }

        public virtual TConfiguration Add<TConfiguration>(TConfiguration configuration)
            where TConfiguration : PropertyConfiguration<TComplex>
        {
            _properties.Add(configuration.ToString(), configuration);

            return configuration;
        }

        public StringPropertyConfiguration<TComplex> AddOptional(Expression<Func<TComplex, string>> property, string dbName = null, int? maxLength = null, bool unicode = true, bool fixedLength = false)
        {
            return Add(new StringPropertyConfiguration<TComplex>(this, property, dbName, true, maxLength, unicode, fixedLength));
        }

        public OptionalPropertyConfiguration<TComplex, DateTime> AddOptional(Expression<Func<TComplex, Nullable<DateTime>>> property, string dbName = null, int presicion = 0)
        {
            return Add(new OptionalPropertyConfiguration<TComplex, DateTime>(this, property, dbName));
        }

        public OptionalPropertyConfiguration<TComplex, DateTimeOffset> AddOptional(Expression<Func<TComplex, Nullable<DateTimeOffset>>> property, string dbName = null, int presicion = 0)
        {
            return Add(new OptionalPropertyConfiguration<TComplex, DateTimeOffset>(this, property, dbName));
        }

        public OptionalPropertyConfiguration<TComplex, TProperty> AddOptional<TProperty>(Expression<Func<TComplex, Nullable<TProperty>>> property, string dbName = null)
            where TProperty : struct
        {
            return Add(new OptionalPropertyConfiguration<TComplex, TProperty>(this, property, dbName));
        }

        public StringPropertyConfiguration<TComplex> AddRequired(Expression<Func<TComplex, string>> property, string dbName = null, int? maxLength = null, bool unicode = true, bool fixedLength = false)
        {
            return Add(new StringPropertyConfiguration<TComplex>(this, property, dbName, false, maxLength, unicode, fixedLength));
        }

        public PropertyConfiguration<TComplex> AddRequired(Expression<Func<TComplex, DateTime>> property, string dbName = null, int presicion = 0)
        {
            return Add(new RequiredPropertyConfiguration<TComplex, DateTime>(this, property, dbName));
        }

        public PropertyConfiguration<TComplex> AddRequired(Expression<Func<TComplex, DateTimeOffset>> property, string dbName = null, int presicion = 0)
        {
            return Add(new RequiredPropertyConfiguration<TComplex, DateTimeOffset>(this, property, dbName));
        }

        public PropertyConfiguration<TComplex> AddRequired<TProperty>(Expression<Func<TComplex, TProperty>> property, string dbName = null)
            where TProperty : struct
        {
            return Add(new RequiredPropertyConfiguration<TComplex, TProperty>(this, property, dbName));
        }

        protected virtual bool CanBeEvaluatedLocally(Expression expression)
        {
            throw new NotImplementedException();
        }

        public virtual TComplex Create(DataRow dataRow)
        {
            var entity = new TComplex();

            foreach (var property in _properties.Values)
            {
                property.Update(target: entity, source: dataRow);
            }

            return entity;
        }

        protected virtual IEnumerator<PropertyConfiguration<TComplex>> GetEnumerator()
        {
            return _properties.Values.GetEnumerator();
        }

        IEnumerator<PropertyConfiguration<TComplex>> IEnumerable<PropertyConfiguration<TComplex>>.GetEnumerator()
        {
            return GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public virtual void Update(TComplex target, DataRow source)
        {
            foreach (var property in _properties.Values)
            {
                property.Update(target: target, source: source);
            }
        }

        public virtual void Update(DataRow target, TComplex source)
        {
            foreach (var property in _properties.Values)
            {
                property.Update(target: target, source: source);
            }
        }

        public virtual bool IsModified(TComplex instance, TComplex original)
        {
            throw new NotImplementedException();
        }

        #region - IComplexTypeMapping -

        string IComplexTypeMapping.GetColumnName(MemberInfo member)
        {
            return Properties
                .Where(p => p.PropertyInfo == member)
                .Select(p => p.DbName)
                .SingleOrDefault();
        }

        IEnumerable<MemberInfo> IComplexTypeMapping.GetMappedMembers()
        {
            return Properties.Select(p => p.PropertyInfo);
        }

        bool IComplexTypeMapping.IsSingletonRelationship(MemberInfo member)
        {
            return false;
        }

        bool IComplexTypeMapping.CanBeEvaluatedLocally(Expression expression)
        {
            return CanBeEvaluatedLocally(expression);
        }

        bool IComplexTypeMapping.IsModified(object instance, object original)
        {
            return IsModified(instance as TComplex, original as TComplex);
        }

        bool IComplexTypeMapping.IsMapped(MemberInfo member)
        {
            return Properties.Any(p => p.PropertyInfo == member);
        }

        bool IComplexTypeMapping.IsColumn(MemberInfo member)
        {
            return true;
        }

        bool IComplexTypeMapping.IsComputed(MemberInfo member)
        {
            return false;
        }

        bool IComplexTypeMapping.IsGenerated(MemberInfo member)
        {
            return false;
        }

        bool IComplexTypeMapping.IsUpdatable(MemberInfo member)
        {
            return true;
        }

        #endregion - IComplexTypeMapping -
    }
}
