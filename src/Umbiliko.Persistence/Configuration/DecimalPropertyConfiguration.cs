﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Persistence.Configuration
{
    using Uos.Reflection;
    

    public class DecimalPropertyConfiguration<TComplex> : PropertyConfiguration<TComplex>
        where TComplex : class, new()
    {
        private short _precision;
        private short _scale;

        public DecimalPropertyConfiguration(ComplexTypeConfiguration<TComplex> typeConfiguration, Expression<Func<TComplex, decimal>> property, string dbName, short precision = 29, short scale = 4)
            : base(typeConfiguration, property.GetComplexPropertyAccess(), dbName, DbType.Decimal, SqlDbType.Decimal, false)
        {
        }

        public DecimalPropertyConfiguration(ComplexTypeConfiguration<TComplex> typeConfiguration, Expression<Func<TComplex, decimal?>> property, string dbName, short precision = 29, short scale = 4)
            : base(typeConfiguration, property.GetComplexPropertyAccess(), dbName, DbType.Decimal, SqlDbType.Decimal, true)
        {
        }

        protected override short DbPrecision
        {
            get { return Precision; }
        }

        protected override short DbScale
        {
            get { return Scale; }
        }

        public short Precision
        {
            get { return _precision; }
        }

        public short Scale
        {
            get { return _scale; }
        }
    }
}
