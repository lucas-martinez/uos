﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Uos.Persistence
{
    using Uos.Implementation;
    using Uos.Inquiry;
    using Uos.Lambda.Expressions;
    using Uos.Persistence.Configuration;
    using Uos.Persistence.Linq;
    using Uos.Reflection;
    using System.Data;

    public class DbConfiguration : IAdvancedMapping
    {
        private static readonly char[] _dotSeparator = new char[] { '.' };
        private static readonly char[] _separators = new char[] { ' ', ',', '|' };

        private Type _contextType;
        private readonly Dictionary<string, IEntityTypeMapping> _entities;
        private ReaderWriterLock _lock = new ReaderWriterLock();

        public DbConfiguration()
        {
            _entities = new Dictionary<string, IEntityTypeMapping>();
        }

        public DbConfiguration(Type contextType)
            : this()
        {
            _contextType = contextType;
        }

        public void AddEntity<TEntity>(EntityTypeConfiguration<TEntity> configuration)
            where TEntity : class, new()
        {
            _entities.Add(typeof(TEntity).FullName, configuration);
        }

        public virtual IQueryMapper CreateMapper(QueryTranslator translator)
        {
            return null;// new DbQueryBuilder(this, translator);
        }

        #region - IQueryMapping -

        string IQueryMapping.GetColumnName(IEntityTypeMapping entity, MemberInfo member)
        {
            return entity.GetColumnName(member);
        }

        string IQueryMapping.GetTableId(Type type)
        {
            return type != null ? type.FullName : null;
        }

        string IQueryMapping.GetTableName(IEntityTypeMapping entity)
        {
            return entity.DbName;
        }

        public IEntityTypeMapping GetEntity(Type type)
        {
            IEntityTypeMapping mapping;
            return _entities.TryGetValue(type.FullName, out mapping) ? mapping : null;
        }

        public IEntityTypeMapping GetEntity(Type elementType, string entityTypeId)
        {
            IEntityTypeMapping mapping;
            return _entities.TryGetValue(entityTypeId, out mapping) 
                ? mapping
                : _entities.TryGetValue(elementType.FullName, out mapping) ? mapping : null;
        }

        IEntityTypeMapping IQueryMapping.GetEntity(MemberInfo contextMember)
        {
            throw new NotImplementedException();
        }

        IEnumerable<MemberInfo> IQueryMapping.GetMappedMembers(IEntityTypeMapping entity)
        {
            throw new NotImplementedException();
        }

        bool IQueryMapping.IsPrimaryKey(IEntityTypeMapping entity, MemberInfo member)
        {
            throw new NotImplementedException();
        }

        IEnumerable<MemberInfo> IQueryMapping.GetPrimaryKeyMembers(IEntityTypeMapping entity)
        {
            return entity.GetPrimaryKeyMembers();
        }

        bool IQueryMapping.IsRelationship(IEntityTypeMapping entity, MemberInfo member)
        {
            return entity.IsRelationship(member);
        }

        bool IQueryMapping.IsSingletonRelationship(IEntityTypeMapping entity, MemberInfo member)
        {
            return entity.IsSingletonRelationship(member);
        }

        public bool CanBeEvaluatedLocally(Expression expression)
        {
            throw new NotImplementedException();
        }

        object IQueryMapping.GetPrimaryKey(IEntityTypeMapping entity, object instance)
        {
            return entity.GetPrimaryKey(instance);
        }

        Expression IQueryMapping.GetPrimaryKeyQuery(IEntityTypeMapping entity, Expression source, Expression[] keys)
        {
            return entity.GetPrimaryKeyQuery(source, keys);
        }

        public IEnumerable<EntityInfo> GetDependentEntities(IEntityTypeMapping entity, object instance)
        {
            return entity.GetDependentEntities(instance);
        }

        public IEnumerable<EntityInfo> GetDependingEntities(IEntityTypeMapping entity, object instance)
        {
            return entity.GetDependingEntities(instance);
        }

        object IQueryMapping.CloneEntity(IEntityTypeMapping entity, object instance)
        {
            return entity.CloneEntity(instance);
        }

        bool IQueryMapping.IsModified(IEntityTypeMapping entity, object instance, object original)
        {
            return entity.IsModified(instance, original);
        }

        IQueryMapper IQueryMapping.CreateMapper(QueryTranslator translator)
        {
            throw new NotImplementedException();
        }

        #endregion - IQueryMapping -

        #region - IBasicMapping -

        bool IBasicMapping.IsMapped(IEntityTypeMapping entity, MemberInfo member)
        {
            return entity.IsMapped(member);
        }

        bool IBasicMapping.IsColumn(IEntityTypeMapping entity, MemberInfo member)
        {
            return entity.IsColumn(member);
        }

        string IBasicMapping.GetColumnDbType(IEntityTypeMapping entity, MemberInfo member)
        {
            throw new NotImplementedException();
        }

        bool IBasicMapping.IsPrimaryKey(IEntityTypeMapping entity, MemberInfo member)
        {
            return entity.IsPrimaryKey(member);
        }

        bool IBasicMapping.IsComputed(IEntityTypeMapping entity, MemberInfo member)
        {
            return entity.IsComputed(member);
        }

        bool IBasicMapping.IsGenerated(IEntityTypeMapping entity, MemberInfo member)
        {
            return entity.IsGenerated(member);
        }

        bool IBasicMapping.IsUpdatable(IEntityTypeMapping entity, MemberInfo member)
        {
            return entity.IsUpdatable(member);
        }

        IEntityTypeMapping IBasicMapping.GetRelatedEntity(IEntityTypeMapping entity, MemberInfo member)
        {
            throw new NotImplementedException();
        }

        bool IBasicMapping.IsAssociationRelationship(IEntityTypeMapping entity, MemberInfo member)
        {
            throw new NotImplementedException();
        }

        IEnumerable<MemberInfo> IBasicMapping.GetAssociationKeyMembers(IEntityTypeMapping entity, MemberInfo member)
        {
            throw new NotImplementedException();
        }

        IEnumerable<MemberInfo> IBasicMapping.GetAssociationRelatedKeyMembers(IEntityTypeMapping entity, MemberInfo member)
        {
            throw new NotImplementedException();
        }

        bool IBasicMapping.IsRelationshipSource(IEntityTypeMapping entity, MemberInfo member)
        {
            throw new NotImplementedException();
        }

        bool IBasicMapping.IsRelationshipTarget(IEntityTypeMapping entity, MemberInfo member)
        {
            throw new NotImplementedException();
        }

        string IBasicMapping.GetColumnName(IEntityTypeMapping entity, MemberInfo member)
        {
            return entity.GetColumnName(member);
        }

        #endregion - IBasicMapping -

        #region - IAdvancedMapping -

        bool IAdvancedMapping.IsNestedEntity(IEntityTypeMapping entity, MemberInfo member)
        {
            throw new NotImplementedException();
        }

        IList<IEntityTypeMapping> IAdvancedMapping.GetTables(IEntityTypeMapping entity)
        {
            throw new NotImplementedException();
        }

        string IAdvancedMapping.GetAlias(IEntityTypeMapping table)
        {
            throw new NotImplementedException();
        }

        string IAdvancedMapping.GetAlias(IEntityTypeMapping entity, MemberInfo member)
        {
            throw new NotImplementedException();
        }

        string IAdvancedMapping.GetTableName(IEntityTypeMapping table)
        {
            throw new NotImplementedException();
        }

        bool IAdvancedMapping.IsExtensionTable(IEntityTypeMapping table)
        {
            throw new NotImplementedException();
        }

        string IAdvancedMapping.GetExtensionRelatedAlias(IEntityTypeMapping table)
        {
            throw new NotImplementedException();
        }

        IEnumerable<string> IAdvancedMapping.GetExtensionKeyColumnNames(IEntityTypeMapping table)
        {
            throw new NotImplementedException();
        }

        IEnumerable<MemberInfo> IAdvancedMapping.GetExtensionRelatedMembers(IEntityTypeMapping table)
        {
            throw new NotImplementedException();
        }

        #endregion - IAdvancedMapping -
    }
}
