﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Persistence
{
    using Uos.Persistence.Configuration;

    public abstract class DbQuery<TArguments, TResult, TDbDataReader, TDbConnection, TDbCommand, TDbParameter> : DbMethod<TArguments, TDbConnection, TDbCommand, TDbParameter>
        where TArguments : class, new()
        where TResult : class, new()
        where TDbDataReader : DbDataReader
        where TDbConnection : DbConnection, new()
        where TDbCommand : DbCommand, new()
        where TDbParameter : DbParameter, new()
    {
        protected DbQuery(DbConnectionProvider<TDbConnection> connectionProvider, string dbName, ComplexTypeConfiguration<TArguments> argumentsConfiguration)
            : base(connectionProvider, dbName, argumentsConfiguration)
        {
        }

        protected DbQuery(DbLink<TDbConnection, TDbCommand, TDbParameter> parent, string dbName, ComplexTypeConfiguration<TArguments> argumentsConfiguration)
            : base(parent, dbName, argumentsConfiguration)
        {
        }

        public override DbLinkTypes DbLinkType
        {
            get { return DbLinkTypes.Query; }
        }

        public override void DbInvoke(TArguments arguments)
        {
            throw new NotImplementedException();
        }
    }
}
