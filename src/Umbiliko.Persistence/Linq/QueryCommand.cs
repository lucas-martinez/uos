﻿// Copyright (c) Microsoft Corporation.  All rights reserved.
// This source code is made available under the terms of the Microsoft Public License (MS-PL)

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Uos.Persistence.Linq
{
    using Uos.Implementation;
    using Uos.Persistence.Configuration;

    public class QueryCommand
    {
        string _commandText;
        ReadOnlyCollection<QueryParameter> _parameters;

        public QueryCommand(string commandText, IEnumerable<QueryParameter> parameters)
        {
            _commandText = commandText;
            _parameters = parameters.ToReadOnly();
        }

        public string CommandText
        {
            get { return _commandText; }
        }

        public ReadOnlyCollection<QueryParameter> Parameters
        {
            get { return _parameters; }
        }
    }

    public class QueryParameter
    {
        private string _name;
        private Type _type;
        private IQueryType _queryType;

        public QueryParameter(string name, Type type, IQueryType queryType)
        {
            _name = name;
            _type = type;
            _queryType = queryType;
        }

        public QueryParameter(IQueryType queryType)
        {
            _name = queryType.DbName;
            _type = queryType.PropertyType;
            _queryType = queryType;
        }

        public string Name
        {
            get { return _name; }
        }

        public Type Type
        {
            get { return _type; }
        }

        public IQueryType QueryType
        {
            get { return _queryType; }
        }
    }
}
