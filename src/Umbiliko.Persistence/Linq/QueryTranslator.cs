﻿// Copyright (c) Microsoft Corporation.  All rights reserved.
// This source code is made available under the terms of the Microsoft Public License (MS-PL)

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace Uos.Persistence.Linq
{
    using Uos.Lambda.Expressions;
    using Uos.Persistence.Configuration;
    using Uos.Reflection;

    /// <summary>
    /// Defines query execution & materialization policies. 
    /// </summary>
    public class QueryTranslator
    {
        QueryLinguist _linguist;
        IQueryMapper _mapper;
        QueryPolice _police;

        public QueryTranslator(DbConfiguration dbConfiguration, QueryLanguage language, QueryPolicy policy)
        {
            _linguist = language.CreateLinguist(this);
            _mapper = dbConfiguration.CreateMapper(this);
            _police = policy.CreatePolice(this);
        }

        public QueryLinguist Linguist
        {
            get { return _linguist; }
        }

        public IQueryMapper Mapper
        {
            get { return _mapper; }
        }

        public QueryPolice Police
        {
            get { return _police; }
        }

        public virtual Expression Translate(Expression expression)
        {
            // pre-evaluate local sub-trees
            expression = PartialEvaluator.Eval(expression, _mapper.Mapping.CanBeEvaluatedLocally);

            // apply mapping (binds LINQ operators too)
            expression = _mapper.Translate(expression);

            // any policy specific translations or validations
            expression = _police.Translate(expression);

            // any language specific translations or validations
            expression = _linguist.Translate(expression);

            return expression;
        }
    }
}