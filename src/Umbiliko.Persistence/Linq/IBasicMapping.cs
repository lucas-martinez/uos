﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Threading.Tasks;

namespace Uos.Persistence.Linq
{
    using Uos.Persistence.Configuration;
    
    public interface IBasicMapping : IQueryMapping
    {
        /// <summary>
        /// Deterimines is a property is mapped onto a column or relationship
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        bool IsMapped(IEntityTypeMapping entity, MemberInfo member);

        /// <summary>
        /// Determines if a property is mapped onto a column
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        bool IsColumn(IEntityTypeMapping entity, MemberInfo member);

        /// <summary>
        /// The type declaration for the column in the provider's syntax
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="member"></param>
        string GetColumnDbType(IEntityTypeMapping entity, MemberInfo member);

        /// <summary>
        /// Determines if a property represents or is part of the entities unique identity (often primary key)
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        bool IsPrimaryKey(IEntityTypeMapping entity, MemberInfo member);

        /// <summary>
        /// Determines if a property is computed after insert or update
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        bool IsComputed(IEntityTypeMapping entity, MemberInfo member);

        /// <summary>
        /// Determines if a property is generated on the server during insert
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        bool IsGenerated(IEntityTypeMapping entity, MemberInfo member);

        /// <summary>
        /// Determines if a property can be part of an update operation
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="member"></param>
        /// <returns></returns>
        bool IsUpdatable(IEntityTypeMapping entity, MemberInfo member);

        /// <summary>
        /// The type of the entity on the other side of the relationship
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        IEntityTypeMapping GetRelatedEntity(IEntityTypeMapping entity, MemberInfo member);

        /// <summary>
        /// Determines if the property is an assocation relationship.
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="member"></param>
        /// <returns></returns>
        bool IsAssociationRelationship(IEntityTypeMapping entity, MemberInfo member);

        /// <summary>
        /// Returns the key members on this side of the association
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="member"></param>
        /// <returns></returns>
        IEnumerable<MemberInfo> GetAssociationKeyMembers(IEntityTypeMapping entity, MemberInfo member);

        /// <summary>
        /// Returns the key members on the other side (related side) of the association
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="member"></param>
        /// <returns></returns>
        IEnumerable<MemberInfo> GetAssociationRelatedKeyMembers(IEntityTypeMapping entity, MemberInfo member);

        bool IsRelationshipSource(IEntityTypeMapping entity, MemberInfo member);

        bool IsRelationshipTarget(IEntityTypeMapping entity, MemberInfo member);

        /// <summary>
        /// The name of the corresponding table column
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        string GetColumnName(IEntityTypeMapping entity, MemberInfo member);
    }
}
