﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Persistence.Linq
{
    using Uos.Persistence.Configuration;
    
    public interface IAdvancedMapping : IBasicMapping
    {
        bool IsNestedEntity(IEntityTypeMapping entity, MemberInfo member);
        
        IList<IEntityTypeMapping> GetTables(IEntityTypeMapping entity);
        
        string GetAlias(IEntityTypeMapping table);
        
        string GetAlias(IEntityTypeMapping entity, MemberInfo member);
        
        string GetTableName(IEntityTypeMapping table);
        
        bool IsExtensionTable(IEntityTypeMapping table);
        
        string GetExtensionRelatedAlias(IEntityTypeMapping table);
        
        IEnumerable<string> GetExtensionKeyColumnNames(IEntityTypeMapping table);

        IEnumerable<MemberInfo> GetExtensionRelatedMembers(IEntityTypeMapping table);
    }
}
