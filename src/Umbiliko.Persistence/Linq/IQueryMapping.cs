﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Persistence.Linq
{
    using Uos.Persistence.Configuration;

    public interface IQueryMapping
    {
        /// <summary>
        /// The name of the corresponding table column
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        string GetColumnName(IEntityTypeMapping entity, MemberInfo member);

        /// <summary>
        /// Determines the entity Id based on the type of the entity alone
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        string GetTableId(Type type);

        /// <summary>
        /// The name of the corresponding database table
        /// </summary>
        /// <param name="rowType"></param>
        /// <returns></returns>
        string GetTableName(IEntityTypeMapping entity);

        /// <summary>
        /// Get the meta entity directly corresponding to the CLR type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        IEntityTypeMapping GetEntity(Type type);

        /// <summary>
        /// Get the meta entity that maps between the CLR type 'entityType' and the database table, yet
        /// is represented publicly as 'elementType'.
        /// </summary>
        /// <param name="elementType"></param>
        /// <param name="entityTypeId"></param>
        /// <returns></returns>
        IEntityTypeMapping GetEntity(Type elementType, string entityTypeId);

        /// <summary>
        /// Get the meta entity represented by the IQueryable context member
        /// </summary>
        /// <param name="contextMember"></param>
        /// <returns></returns>
        IEntityTypeMapping GetEntity(MemberInfo contextMember);

        IEnumerable<MemberInfo> GetMappedMembers(IEntityTypeMapping entity);

        bool IsPrimaryKey(IEntityTypeMapping entity, MemberInfo member);

        IEnumerable<MemberInfo> GetPrimaryKeyMembers(IEntityTypeMapping entity);

        /// <summary>
        /// Determines if a property is mapped as a relationship
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="member"></param>
        /// <returns></returns>
        bool IsRelationship(IEntityTypeMapping entity, MemberInfo member);

        /// <summary>
        /// Determines if a relationship property refers to a single entity (as opposed to a collection.)
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        bool IsSingletonRelationship(IEntityTypeMapping entity, MemberInfo member);

        /// <summary>
        /// Determines whether a given expression can be executed locally. 
        /// (It contains no parts that should be translated to the target environment.)
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        bool CanBeEvaluatedLocally(Expression expression);

        object GetPrimaryKey(IEntityTypeMapping entity, object instance);

        Expression GetPrimaryKeyQuery(IEntityTypeMapping entity, Expression source, Expression[] keys);
        
        IEnumerable<EntityInfo> GetDependentEntities(IEntityTypeMapping entity, object instance);
        
        IEnumerable<EntityInfo> GetDependingEntities(IEntityTypeMapping entity, object instance);
        
        object CloneEntity(IEntityTypeMapping entity, object instance);
        
        bool IsModified(IEntityTypeMapping entity, object instance, object original);

        IQueryMapper CreateMapper(QueryTranslator translator);
    }
}
