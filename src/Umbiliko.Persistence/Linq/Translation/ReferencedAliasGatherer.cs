﻿// Copyright (c) Microsoft Corporation.  All rights reserved.
// This source code is made available under the terms of the Microsoft Public License (MS-PL)

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Uos.Persistence.Linq.Translation
{
    using Uos.Persistence.Linq.Expressions;

    /// <summary>
    ///  returns the set of all aliases produced by a query source
    /// </summary>
    public class ReferencedAliasGatherer : DbExpressionVisitor
    {
        HashSet<TableAlias> _aliases;

        private ReferencedAliasGatherer()
        {
            _aliases = new HashSet<TableAlias>();
        }

        public static HashSet<TableAlias> Gather(Expression source)
        {
            var gatherer = new ReferencedAliasGatherer();
            gatherer.Visit(source);
            return gatherer._aliases;
        }

        protected override Expression VisitColumn(ColumnExpression column)
        {
            _aliases.Add(column.Alias);
            return column;
        }
    }
}
