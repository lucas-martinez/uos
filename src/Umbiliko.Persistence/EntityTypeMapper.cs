﻿using Uos.Data.Schema;
using Uos.Persistence.Linq;

namespace Uos.Persistence
{
    internal class EntityTypeMapper<TEntity> : IEntityTypeMapper
        where TEntity : class, new()
    {
        private EntityType<TEntity> entityType;

        public EntityTypeMapper(EntityType<TEntity> entityType)
        {
            this.entityType = entityType;
        }
    }
}