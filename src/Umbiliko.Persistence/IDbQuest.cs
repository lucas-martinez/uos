﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Persistence
{
    public interface IDbQuest<TArguments, TResult>
        where TArguments : class
        where TResult : struct
    {
        TResult DbInvoke(TArguments arguments);
    }
}
