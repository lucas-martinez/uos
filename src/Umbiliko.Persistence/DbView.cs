﻿using Uos.Persistence.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Persistence
{
    using Uos.Persistence.Configuration;

    public abstract class DbView<TArguments, TResult, TDbDataReader, TDbConnection, TDbCommand, TDbParameter> : DbLink<TDbConnection, TDbCommand, TDbParameter>
        where TArguments : class, new()
        where TResult : class, new()
        where TDbConnection : DbConnection, new()
        where TDbCommand : DbCommand, new()
        where TDbDataReader : DbDataReader
        where TDbParameter : DbParameter, new()
    {
        protected DbView(DbConnectionProvider<TDbConnection> connectionProvider, string dbName, ComplexTypeConfiguration<TArguments> argumentsConfiguration)
            : base(connectionProvider)
        {
        }

        protected DbView(DbLink<TDbConnection, TDbCommand, TDbParameter> parent, string dbName, ComplexTypeConfiguration<TArguments> argumentsConfiguration)
            : base(parent)
        {
        }

        public override DbLinkTypes DbLinkType
        {
            get { return DbLinkTypes.View; }
        }
    }
}
