﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Persistence
{
    public abstract class DbLink<TDbConnection, TDbCommand, TDbParameter> : DataLink, IDisposable
        where TDbConnection : DbConnection, new()
        where TDbCommand : DbCommand, new()
        where TDbParameter : DbParameter, new()
    {
        private bool? _ownConnection;
        private TDbConnection _dbConnection;
        private readonly DbConnectionProvider<TDbConnection> _dbConnectionProvider;
        private readonly DbTypeSystem _dbTypeSystem; 
        private readonly DbLink<TDbConnection, TDbCommand, TDbParameter> _parent;

        protected DbLink(DbConnectionProvider<TDbConnection> connectionProvider)
            : base()
        {
            _dbConnectionProvider = connectionProvider;
            _dbTypeSystem = connectionProvider.Language.TypeSystem;
        }

        protected DbLink(DbConnectionProvider<TDbConnection> connectionProvider, string dataSetName)
            : base(dataSetName)
        {
            _dbConnectionProvider = connectionProvider;
            _dbTypeSystem = connectionProvider.Language.TypeSystem;
        }

        protected DbLink(DbConnectionProvider<TDbConnection> connectionProvider, DataSet dataSet)
            : base(dataSet)
        {
            _dbConnectionProvider = connectionProvider;
            _dbTypeSystem = connectionProvider.Language.TypeSystem;
        }

        protected DbLink(DbLink<TDbConnection, TDbCommand, TDbParameter> parent)
            : this(parent.ConnectionProvider)
        {
            _parent = parent;
        }

        protected DbLink(DbLink<TDbConnection, TDbCommand, TDbParameter> parent, string dataSetName)
            : this(parent.ConnectionProvider, dataSetName)
        {
            _parent = parent;
        }

        protected DbLink(DbLink<TDbConnection, TDbCommand, TDbParameter> parent, DataSet dataSet)
            : this(parent.ConnectionProvider, dataSet)
        {
            _parent = parent;
        }

        public TDbConnection Connection
        {
            get
            {
                var dbConnection = _dbConnection;

                if (dbConnection != null) return dbConnection;

                dbConnection = new TDbConnection();

                _dbConnection = dbConnection;

                _ownConnection = true;

                return dbConnection;
            }
        }

        public abstract DbLinkTypes DbLinkType { get; }

        public bool OwnConnection
        {
            get { return _ownConnection ?? true; }
        }

        public DbLink<TDbConnection, TDbCommand, TDbParameter> Parent
        {
            get { return _parent; }
        }

        public DbConnectionProvider<TDbConnection> ConnectionProvider
        {
            get { return _dbConnectionProvider; }
        }

        public DbTypeSystem TypeSystem
        {
            get { return _dbTypeSystem; }
        }

        //protected abstract TDbConnection CreateDbConnection();

        //protected abstract TDbCommand CreateDbCommand();

        //protected abstract TDbParameter CreateDbParameter();

        protected virtual void Dispose(bool disposing)
        {
            var dbConnection = _dbConnection;

            _dbConnection = null;

            if (dbConnection != null && _ownConnection == true)
            {
                dbConnection.Dispose();
            }
        }

        void IDisposable.Dispose()
        {
            Dispose(true);
        }
    }
}
