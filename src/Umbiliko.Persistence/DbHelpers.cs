﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Persistence
{
    using Uos.Persistence.Configuration;

    public static class DbHelpers
    {
        public static EntityTypeConfiguration<TEntity> Entity<TEntity>(this DbConfiguration db)
            where TEntity : class, new()
        {
            var config = new EntityTypeConfiguration<TEntity>(db);
            
            db.AddEntity(config);
            
            return config;
        }

        public static EntityTypeConfiguration<TEntity> Entity<TEntity>(this DbConfiguration db, string entitySetName)
            where TEntity : class, new()
        {
            var config = new EntityTypeConfiguration<TEntity>(db, entitySetName);

            db.AddEntity(config);

            return config;
        }

        public static EntityTypeConfiguration<TEntity> Entity<TEntity>(this DbConfiguration db, string dbName, string schema)
            where TEntity : class, new()
        {
            var config = new EntityTypeConfiguration<TEntity>(db, dbName, schema);

            db.AddEntity(config);

            return config;
        }

        public static EntityTypeConfiguration<TEntity> Entity<TEntity>(this DbConfiguration db, string entitySetName, string dbName, string schema)
            where TEntity : class, new()
        {
            var config = new EntityTypeConfiguration<TEntity>(db, entitySetName, dbName, schema);

            db.AddEntity(config);

            return config;
        }
    }
}
