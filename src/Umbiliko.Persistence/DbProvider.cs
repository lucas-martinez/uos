﻿// Copyright (c) Microsoft Corporation.  All rights reserved.
// This source code is made available under the terms of the Microsoft Public License (MS-PL)

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace Uos.Persistence
{
    using Uos.Inquiry;
    using Uos.Lambda.Expressions;
    using Uos.Persistence.Configuration;
    using Uos.Persistence.Linq;
    using Uos.Persistence.Linq.Expressions;
    using Uos.Reflection;

    /// <summary>
    /// A LINQ IQueryable query provider that executes database queries over a DbConnection
    /// </summary>
    public abstract class DbProvider : QueryProvider, IEntityProvider, ICreateExecutor
    {
        private readonly QueryLanguage _language;
        private readonly DbConfiguration _dbConfiguration;
        private QueryPolicy _policy;
        private TextWriter _log;
        private readonly Dictionary<IEntityTypeMapping, IEntityStore> _tables;
        private QueryCache _cache;

        public DbProvider(DbConfiguration dbConfiguration, QueryLanguage language, QueryPolicy policy)
        {
            if (dbConfiguration == null) throw new ArgumentNullException("dbConfiguration", "Configuration not specified");

            if (language == null) throw new ArgumentNullException("language", "Language not specified");


            if (policy == null) throw new ArgumentNullException("policy", "Policy not specified");

            _language = language;
            _dbConfiguration = dbConfiguration;
            _policy = policy;
            _tables = new Dictionary<IEntityTypeMapping, IEntityStore>();
        }

        public DbConfiguration DbConfiguration
        {
            get { return _dbConfiguration; }
        }

        public QueryLanguage Language
        {
            get { return _language; }
        }

        public QueryPolicy Policy
        {
            get { return _policy; }
            set { _policy = value ?? QueryPolicy.Default; }
        }

        public TextWriter Log
        {
            get { return _log; }
            set { _log = value; }
        }

        public QueryCache Cache
        {
            get { return _cache; }
            set { _cache = value; }
        }

        public IEntityStore GetTable(IEntityTypeMapping entity)
        {
            IEntityStore table;
            if (!_tables.TryGetValue(entity, out table))
            {
                table = this.CreateTable(entity);
                _tables.Add(entity, table);
            }
            return table;
        }

        protected virtual IEntityStore CreateTable(IEntityTypeMapping entity)
        {
            return (IEntityStore) Activator.CreateInstance(
                typeof(EntityStore<>).MakeGenericType(entity.ElementType), 
                new object[] { this, entity }
                );
        }

        public virtual IEntityStore<T> GetTable<T>()
        {
            return GetStore<T>(null);
        }

        public virtual IEntityStore<T> GetStore<T>(string dbName)
        {
            return (IEntityStore<T>)this.GetStore(typeof(T), dbName);
        }

        public virtual IEntityStore GetStore(Type type)
        {
            return GetStore(type);
        }

        public virtual IEntityStore GetStore(Type type, string dbName)
        {
            return this.GetTable(this.DbConfiguration.GetEntity(type, dbName));
        }

        public bool CanBeEvaluatedLocally(Expression expression)
        {
            return this.DbConfiguration.CanBeEvaluatedLocally(expression);
        }

        public virtual bool CanBeParameter(Expression expression)
        {
            Type type = TypeHelper.GetNonNullableType(expression.Type);
            switch (Type.GetTypeCode(type))
            {
                case TypeCode.Object:
                    if (expression.Type == typeof(Byte[]) ||
                        expression.Type == typeof(Char[]))
                        return true;
                    return false;
                default:
                    return true;
            }
        }

        protected abstract QueryExecutor CreateExecutor();

        QueryExecutor ICreateExecutor.CreateExecutor()
        {
            return this.CreateExecutor();
        }

        public override string GetQueryText(Expression expression)
        {
            Expression plan = this.GetExecutionPlan(expression);
            var commands = CommandGatherer.Gather(plan).Select(c => c.CommandText).ToArray();
            return string.Join("\n\n", commands);
        }

        class CommandGatherer : DbExpressionVisitor
        {
            List<QueryCommand> commands = new List<QueryCommand>();

            public static ReadOnlyCollection<QueryCommand> Gather(Expression expression)
            {
                var gatherer = new CommandGatherer();
                gatherer.Visit(expression);
                return gatherer.commands.AsReadOnly();
            }

            protected override Expression VisitConstant(ConstantExpression c)
            {
                QueryCommand qc = c.Value as QueryCommand;
                if (qc != null)
                {
                    this.commands.Add(qc);
                }
                return c;
            }
        }

        public string GetQueryPlan(Expression expression)
        {
            Expression plan = this.GetExecutionPlan(expression);
            return DbExpressionWriter.WriteToString(this.Language, plan);
        }

        protected virtual QueryTranslator CreateTranslator()
        {
            return new QueryTranslator(_dbConfiguration, _language, _policy);
        }

        public abstract void DoTransacted(Action action);

        public abstract void DoConnected(Action action);
        
        public abstract int ExecuteCommand(string commandText);

        /// <summary>
        /// Execute the query expression (does translation, etc.)
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public override object Execute(Expression expression)
        {
            LambdaExpression lambda = expression as LambdaExpression;

            if (lambda == null && _cache != null && expression.NodeType != ExpressionType.Constant)
            {
                return _cache.Execute(expression);
            }

            Expression plan = this.GetExecutionPlan(expression);

            if (lambda != null)
            {
                // compile & return the execution plan so it can be used multiple times
                LambdaExpression fn = Expression.Lambda(lambda.Type, plan, lambda.Parameters);
#if NOREFEMIT
                    return ExpressionEvaluator.CreateDelegate(fn);
#else
                return fn.Compile();
#endif
            }
            else
            {
                // compile the execution plan and invoke it
                Expression<Func<object>> efn = Expression.Lambda<Func<object>>(Expression.Convert(plan, typeof(object)));
#if NOREFEMIT
                    return ExpressionEvaluator.Eval(efn, new object[] { });
#else
                Func<object> fn = efn.Compile();
                return fn();
#endif
            }
        }

        /// <summary>
        /// Convert the query expression into an execution plan
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public virtual Expression GetExecutionPlan(Expression expression)
        {
            // strip off lambda for now
            LambdaExpression lambda = expression as LambdaExpression;
            if (lambda != null)
                expression = lambda.Body;

            QueryTranslator translator = this.CreateTranslator();

            // translate query into client & server parts
            Expression translation = translator.Translate(expression);

            var parameters = lambda != null ? lambda.Parameters : null;
            Expression provider = this.Find(expression, parameters, typeof(DbProvider));
            if (provider == null)
            {
                Expression rootQueryable = this.Find(expression, parameters, typeof(IQueryable));
                provider = Expression.Property(rootQueryable, typeof(IQueryable).GetProperty("Provider"));
            }

            return translator.Police.BuildExecutionPlan(translation, provider);
        }

        private Expression Find(Expression expression, IList<ParameterExpression> parameters, Type type)
        {
            if (parameters != null)
            {
                Expression found = parameters.FirstOrDefault(p => type.IsAssignableFrom(p.Type));
                if (found != null)
                    return found;
            }
            return TypedSubtreeFinder.Find(expression, type);
        }
           
        public static IQueryMapping GetMapping(string mappingId)
        {
            if (mappingId != null)
            {
                Type type = FindLoadedType(mappingId);
                if (type != null)
                {
                    return new DbConfiguration(type);
                }
                /*if (File.Exists(mappingId))
                {
                    return XmlMapping.FromXml(File.ReadAllText(mappingId));
                }*/
            }
            return null;// new ImplicitMapping();
        }

        public static Type GetProviderType(string providerName)
        {
            if (!string.IsNullOrEmpty(providerName))
            {
                var type = FindInstancesIn(typeof(DbProvider), providerName).FirstOrDefault();
                if (type != null)
                    return type;
            }
            return null;
        }

        private static Type FindLoadedType(string typeName)
        {
            foreach (var assem in AppDomain.CurrentDomain.GetAssemblies())
            {
                var type = assem.GetType(typeName, false, true);
                if (type != null)
                    return type;
            }
            return null;
        }

        private static IEnumerable<Type> FindInstancesIn(Type type, string assemblyName)
        {
            Assembly assembly = GetAssemblyForNamespace(assemblyName);
            if (assembly != null)
            {
                foreach (var atype in assembly.GetTypes())
                {
                    if (string.Compare(atype.Namespace, assemblyName, true) == 0
                        && type.IsAssignableFrom(atype))
                    {
                        yield return atype;
                    }
                }
            }
        }

        private static Assembly GetAssemblyForNamespace(string nspace)
        {
            foreach (var assem in AppDomain.CurrentDomain.GetAssemblies())
            {
                if (assem.FullName.Contains(nspace))
                {
                    return assem;
                }
            }

            return Load(nspace + ".dll");
        }

        private static Assembly Load(string name)
        {
            // try to load it.
            try
            {
                return Assembly.LoadFrom(name);
            }
            catch
            {
            }
            return null;
        }
    }
}
