﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Persistence
{
    using Uos.Persistence.Configuration;

    public abstract class DbMethod<TArguments, TDbConnection, TDbCommand, TDbParameter> : DbLink<TDbConnection, TDbCommand, TDbParameter>
        where TArguments : class, new()
        where TDbConnection : DbConnection, new()
        where TDbCommand : DbCommand, new()
        where TDbParameter : DbParameter, new()
    {
        private readonly string _methodName;
        private readonly ComplexTypeConfiguration<TArguments> _argsConfig;

        protected DbMethod(DbConnectionProvider<TDbConnection> connectionProvider, string dbName, ComplexTypeConfiguration<TArguments> argumentsConfiguration)
            : base(connectionProvider)
        {
            _argsConfig = argumentsConfiguration;
            _methodName = dbName;
        }

        protected DbMethod(DbLink<TDbConnection, TDbCommand, TDbParameter> parent, string dbName, ComplexTypeConfiguration<TArguments> argumentsConfiguration)
            : base(parent)
        {
            _argsConfig = argumentsConfiguration;
            _methodName = dbName;
        }

        public override DbLinkTypes DbLinkType
        {
            get { return DbLinkTypes.NonQuery; }
        }

        public virtual void DbInvoke(TArguments arguments)
        {
            throw new NotImplementedException();
        }
    }
}
