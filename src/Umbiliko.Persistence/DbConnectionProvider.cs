﻿// Copyright (c) Microsoft Corporation.  All rights reserved.
// This source code is made available under the terms of the Microsoft Public License (MS-PL)

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Reflection;

namespace Uos.Persistence
{
    using Uos.Implementation;
    using Uos.Persistence.Linq;
    using Uos.Persistence.Configuration;
    using Uos.Reflection;

    public class DbConnectionProvider<TDbConnection> : DbProvider
        where TDbConnection : DbConnection, new()
    {
        private TDbConnection _connection;
        private readonly string _connectionString;
        private DbTransaction _transaction;
        private IsolationLevel _isolation = IsolationLevel.ReadCommitted;

        private int _connectedActions = 0;
        private bool _actionOpenedConnection = false;

        public DbConnectionProvider(DbConfiguration dbConfiguration, QueryLanguage language, QueryPolicy policy, string connectionString)
            : base(dbConfiguration, language, policy)
        {
            if (connectionString == null) throw new ArgumentNullException("connectionString", "Connection string not specified");

            _connectionString = connectionString;
        }

        public DbConnectionProvider(DbConfiguration dbConfiguration, QueryLanguage language, QueryPolicy policy, TDbConnection connection)
            : base(dbConfiguration, language, policy)
        {
            if (connection == null) throw new ArgumentNullException("connection", "Connection not specified");

            _connection = connection;
            _connectionString = connection.ConnectionString;
        }

        public virtual TDbConnection Connection
        {
            get { return _connection; }
        }

        public string ConnectionString
        {
            get { return _connectionString; }
        }

        public virtual DbTransaction Transaction
        {
            get { return _transaction; }
            set
            {
                if (value != null && value.Connection != _connection)
                    throw new InvalidOperationException("Transaction does not match connection.");
                _transaction = value;
            }
        }

        public IsolationLevel Isolation
        {
            get { return _isolation; }
            set { _isolation = value; }
        }

        public virtual DbConnectionProvider<TDbConnection> New(DbConfiguration dbConfiguration, QueryPolicy policy, TDbConnection connection)
        {
            return (DbConnectionProvider<TDbConnection>)Activator.CreateInstance(GetType(), new object[] { dbConfiguration, policy, connection });
        }

        public virtual DbConnectionProvider<TDbConnection> New(TDbConnection connection)
        {
            var n = New(DbConfiguration, Policy, connection);
            n.Log = Log;
            return n;
        }

        public virtual DbConnectionProvider<TDbConnection> New(DbConfiguration dbConfiguration)
        {
            var n = New(dbConfiguration, Policy, Connection);
            n.Log = Log;
            return n;
        }

        public virtual DbConnectionProvider<TDbConnection> New(QueryPolicy policy)
        {
            var n = New(DbConfiguration, policy, Connection);
            n.Log = Log;
            return n;
        }

        public static DbConnectionProvider<TDbConnection> FromApplicationSettings()
        {
            var provider = System.Configuration.ConfigurationManager.AppSettings["Provider"];
            var connection = System.Configuration.ConfigurationManager.AppSettings["Connection"];
            var mapping = System.Configuration.ConfigurationManager.AppSettings["Mapping"];
            return From(provider, connection, mapping);
        }

        public static DbConnectionProvider<TDbConnection> From(string connectionString, string mappingId)
        {
            return From(connectionString, mappingId, QueryPolicy.Default);
        }

        public static DbConnectionProvider<TDbConnection> From(string connectionString, string mappingId, QueryPolicy policy)
        {
            return From(null, connectionString, mappingId, policy);
        }

        public static DbConnectionProvider<TDbConnection> From(string connectionString, IQueryMapping mapping, QueryPolicy policy)
        {
            return From((string)null, connectionString, mapping, policy);
        }

        public static DbConnectionProvider<TDbConnection> From(string provider, string connectionString, string mappingId)
        {
            return From(provider, connectionString, mappingId, QueryPolicy.Default);
        }

        public static DbConnectionProvider<TDbConnection> From(string provider, string connectionString, string mappingId, QueryPolicy policy)
        {
            return From(provider, connectionString, GetMapping(mappingId), policy);
        }

        public static DbConnectionProvider<TDbConnection> From(string provider, string connectionString, IQueryMapping mapping, QueryPolicy policy)
        {
            if (provider == null)
            {
                var clower = connectionString.ToLower();
                // try sniffing connection to figure out provider
                if (clower.Contains(".mdb") || clower.Contains(".accdb"))
                {
                    provider = "Custom.Persistence.Access";
                }
                else if (clower.Contains(".sdf"))
                {
                    provider = "Custom.Persistence.SqlServerCe";
                }
                else if (clower.Contains(".sl3") || clower.Contains(".db3"))
                {
                    provider = "Custom.Persistence.SQLite";
                }
                else if (clower.Contains(".mdf"))
                {
                    provider = "Custom.Persistence.SqlClient";
                }
                else
                {
                    throw new InvalidOperationException(string.Format("Query provider not specified and cannot be inferred."));
                }
            }

            Type providerType = GetProviderType(provider);
            if (providerType == null)
                throw new InvalidOperationException(string.Format("Unable to find query provider '{0}'", provider));

            return From(providerType, connectionString, mapping, policy);
        }

        public static DbConnectionProvider<TDbConnection> From(Type providerType, string connectionString, IQueryMapping mapping, QueryPolicy policy)
        {
            Type adoConnectionType = GetAdoConnectionType(providerType);
            if (adoConnectionType == null)
                throw new InvalidOperationException(string.Format("Unable to deduce ADO provider for '{0}'", providerType.Name));
            TDbConnection connection = (TDbConnection)Activator.CreateInstance(adoConnectionType);

            // is the connection string just a filename?
            if (!connectionString.Contains('='))
            {
                MethodInfo gcs = providerType.GetMethod("GetConnectionString", BindingFlags.Static | BindingFlags.Public, null, new Type[] { typeof(string) }, null);
                if (gcs != null)
                {
                    connectionString = (string)gcs.Invoke(null, new object[] { connectionString });
                }
            }

            connection.ConnectionString = connectionString;

            return (DbConnectionProvider<TDbConnection>)Activator.CreateInstance(providerType, new object[] { connection, mapping, policy });
        }

        private static Type GetAdoConnectionType(Type providerType)
        {
            // sniff constructors 
            foreach (var con in providerType.GetConstructors())
            {
                foreach (var arg in con.GetParameters())
                {
                    if (arg.ParameterType.IsSubclassOf(typeof(TDbConnection)))
                        return arg.ParameterType;
                }
            }
            return null;
        }

        protected bool ActionOpenedConnection
        {
            get { return _actionOpenedConnection; }
        }

        protected void StartUsingConnection()
        {
            if (_connection.State == ConnectionState.Closed)
            {
                _connection.Open();
                _actionOpenedConnection = true;
            }
            _connectedActions++;
        }

        protected void StopUsingConnection()
        {
            System.Diagnostics.Debug.Assert(_connectedActions > 0);
            _connectedActions--;
            if (_connectedActions == 0 && _actionOpenedConnection)
            {
                _connection.Close();
                _actionOpenedConnection = false;
            }
        }

        public override void DoConnected(Action action)
        {
            StartUsingConnection();
            try
            {
                action();
            }
            finally
            {
                StopUsingConnection();
            }
        }

        public override void DoTransacted(Action action)
        {
            StartUsingConnection();
            try
            {
                if (Transaction == null)
                {
                    var trans = Connection.BeginTransaction(Isolation);
                    try
                    {
                        Transaction = trans;
                        action();
                        trans.Commit();
                    }
                    finally
                    {
                        Transaction = null;
                        trans.Dispose();
                    }
                }
                else
                {
                    action();
                }
            }
            finally
            {
                StopUsingConnection();
            }
        }

        public override int ExecuteCommand(string commandText)
        {
            if (Log != null)
            {
                Log.WriteLine(commandText);
            }
            StartUsingConnection();
            try
            {
                DbCommand cmd = Connection.CreateCommand();
                cmd.CommandText = commandText;
                return cmd.ExecuteNonQuery();
            }
            finally
            {
                StopUsingConnection();
            }
        }

        protected override QueryExecutor CreateExecutor()
        {
            return new Executor(this);
        }

        public class Executor : QueryExecutor
        {
            DbConnectionProvider<TDbConnection> _provider;
            int rowsAffected;

            public Executor(DbConnectionProvider<TDbConnection> provider)
            {
                _provider = provider;
            }

            public DbConnectionProvider<TDbConnection> Provider
            {
                get { return _provider; }
            }

            public override int RowsAffected
            {
                get { return rowsAffected; }
            }

            protected virtual bool BufferResultRows
            {
                get { return false; }
            }

            protected bool ActionOpenedConnection
            {
                get { return _provider._actionOpenedConnection; }
            }

            protected void StartUsingConnection()
            {
                _provider.StartUsingConnection();
            }

            protected void StopUsingConnection()
            {
                _provider.StopUsingConnection();
            }

            public override object Convert(object value, Type type)
            {
                if (value == null)
                {
                    return TypeHelper.GetDefault(type);
                }
                type = TypeHelper.GetNonNullableType(type);
                Type vtype = value.GetType();
                if (type != vtype)
                {
                    if (type.IsEnum)
                    {
                        if (vtype == typeof(string))
                        {
                            return Enum.Parse(type, (string)value);
                        }
                        else
                        {
                            Type utype = Enum.GetUnderlyingType(type);
                            if (utype != vtype)
                            {
                                value = System.Convert.ChangeType(value, utype);
                            }
                            return Enum.ToObject(type, value);
                        }
                    }
                    return System.Convert.ChangeType(value, type);
                }
                return value;
            }

            public override IEnumerable<T> Execute<T>(QueryCommand command, Func<FieldReader, T> fnProjector, IEntityTypeMapping entity, object[] paramValues)
            {
                LogCommand(command, paramValues);
                StartUsingConnection();
                try
                {
                    DbCommand cmd = GetCommand(command, paramValues);
                    DbDataReader reader = ExecuteReader(cmd);
                    var result = Project(reader, fnProjector, entity, true);
                    if (_provider.ActionOpenedConnection)
                    {
                        result = result.ToList();
                    }
                    else
                    {
                        result = new EnumerateOnce<T>(result);
                    }
                    return result;
                }
                finally
                {
                    StopUsingConnection();
                }
            }

            protected virtual DbDataReader ExecuteReader(DbCommand command)
            {
                var reader = command.ExecuteReader();
                if (BufferResultRows)
                {
                    // use data table to buffer results
                    var ds = new DataSet();
                    ds.EnforceConstraints = false;
                    var table = new DataTable();
                    ds.Tables.Add(table);
                    ds.EnforceConstraints = false;
                    table.Load(reader);
                    reader = table.CreateDataReader();
                }
                return reader;
            }

            protected virtual IEnumerable<T> Project<T>(DbDataReader reader, Func<FieldReader, T> fnProjector, IEntityTypeMapping entity, bool closeReader)
            {
                var freader = new DbFieldReader(this, reader);
                try
                {
                    while (reader.Read())
                    {
                        yield return fnProjector(freader);
                    }
                }
                finally
                {
                    if (closeReader)
                    {
                        reader.Close();
                    }
                }
            }

            public override int ExecuteCommand(QueryCommand query, object[] paramValues)
            {
                LogCommand(query, paramValues);
                StartUsingConnection();
                try
                {
                    DbCommand cmd = GetCommand(query, paramValues);
                    rowsAffected = cmd.ExecuteNonQuery();
                    return rowsAffected;
                }
                finally
                {
                    StopUsingConnection();
                }
            }

            public override IEnumerable<int> ExecuteBatch(QueryCommand query, IEnumerable<object[]> paramSets, int batchSize, bool stream)
            {
                StartUsingConnection();
                try
                {
                    var result = ExecuteBatch(query, paramSets);
                    if (!stream || ActionOpenedConnection)
                    {
                        return result.ToList();
                    }
                    else
                    {
                        return new EnumerateOnce<int>(result);
                    }
                }
                finally
                {
                    StopUsingConnection();
                }
            }

            private IEnumerable<int> ExecuteBatch(QueryCommand query, IEnumerable<object[]> paramSets)
            {
                LogCommand(query, null);
                DbCommand cmd = GetCommand(query, null);
                foreach (var paramValues in paramSets)
                {
                    LogParameters(query, paramValues);
                    LogMessage("");
                    SetParameterValues(query, cmd, paramValues);
                    rowsAffected = cmd.ExecuteNonQuery();
                    yield return rowsAffected;
                }
            }

            public override IEnumerable<T> ExecuteBatch<T>(QueryCommand query, IEnumerable<object[]> paramSets, Func<FieldReader, T> fnProjector, IEntityTypeMapping entity, int batchSize, bool stream)
            {
                StartUsingConnection();
                try
                {
                    var result = ExecuteBatch(query, paramSets, fnProjector, entity);
                    if (!stream || ActionOpenedConnection)
                    {
                        return result.ToList();
                    }
                    else
                    {
                        return new EnumerateOnce<T>(result);
                    }
                }
                finally
                {
                    StopUsingConnection();
                }
            }

            private IEnumerable<T> ExecuteBatch<T>(QueryCommand query, IEnumerable<object[]> paramSets, Func<FieldReader, T> fnProjector, IEntityTypeMapping entity)
            {
                LogCommand(query, null);
                DbCommand cmd = GetCommand(query, null);
                cmd.Prepare();
                foreach (var paramValues in paramSets)
                {
                    LogParameters(query, paramValues);
                    LogMessage("");
                    SetParameterValues(query, cmd, paramValues);
                    var reader = ExecuteReader(cmd);
                    var freader = new DbFieldReader(this, reader);
                    try
                    {
                        if (reader.HasRows)
                        {
                            reader.Read();
                            yield return fnProjector(freader);
                        }
                        else
                        {
                            yield return default(T);
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
            }

            public override IEnumerable<T> ExecuteDeferred<T>(QueryCommand query, Func<FieldReader, T> fnProjector, IEntityTypeMapping entity, object[] paramValues)
            {
                LogCommand(query, paramValues);
                StartUsingConnection();
                try
                {
                    DbCommand cmd = GetCommand(query, paramValues);
                    var reader = ExecuteReader(cmd);
                    var freader = new DbFieldReader(this, reader);
                    try
                    {
                        while (reader.Read())
                        {
                            yield return fnProjector(freader);
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
                finally
                {
                    StopUsingConnection();
                }
            }

            /// <summary>
            /// Get an ADO command object initialized with the command-text and parameters
            /// </summary>
            /// <param name="commandText"></param>
            /// <param name="paramNames"></param>
            /// <param name="paramValues"></param>
            /// <returns></returns>
            protected virtual DbCommand GetCommand(QueryCommand query, object[] paramValues)
            {
                // create command object (and fill in parameters)
                DbCommand cmd = _provider.Connection.CreateCommand();
                cmd.CommandText = query.CommandText;
                if (_provider.Transaction != null)
                    cmd.Transaction = _provider.Transaction;
                SetParameterValues(query, cmd, paramValues);
                return cmd;
            }

            protected virtual void SetParameterValues(QueryCommand query, DbCommand command, object[] paramValues)
            {
                if (query.Parameters.Count > 0 && command.Parameters.Count == 0)
                {
                    for (int i = 0, n = query.Parameters.Count; i < n; i++)
                    {
                        AddParameter(command, query.Parameters[i], paramValues != null ? paramValues[i] : null);
                    }
                }
                else if (paramValues != null)
                {
                    for (int i = 0, n = command.Parameters.Count; i < n; i++)
                    {
                        DbParameter p = command.Parameters[i];
                        if (p.Direction == System.Data.ParameterDirection.Input
                         || p.Direction == System.Data.ParameterDirection.InputOutput)
                        {
                            p.Value = paramValues[i] ?? DBNull.Value;
                        }
                    }
                }
            }

            protected virtual void AddParameter(DbCommand command, QueryParameter parameter, object value)
            {
                DbParameter p = command.CreateParameter();
                p.ParameterName = parameter.Name;
                p.Value = value ?? DBNull.Value;
                command.Parameters.Add(p);
            }

            protected virtual void GetParameterValues(DbCommand command, object[] paramValues)
            {
                if (paramValues != null)
                {
                    for (int i = 0, n = command.Parameters.Count; i < n; i++)
                    {
                        if (command.Parameters[i].Direction != System.Data.ParameterDirection.Input)
                        {
                            object value = command.Parameters[i].Value;
                            if (value == DBNull.Value)
                                value = null;
                            paramValues[i] = value;
                        }
                    }
                }
            }

            protected virtual void LogMessage(string message)
            {
                if (_provider.Log != null)
                {
                    _provider.Log.WriteLine(message);
                }
            }

            /// <summary>
            /// Write a command & parameters to the log
            /// </summary>
            /// <param name="command"></param>
            /// <param name="paramValues"></param>
            protected virtual void LogCommand(QueryCommand command, object[] paramValues)
            {
                if (_provider.Log != null)
                {
                    _provider.Log.WriteLine(command.CommandText);
                    if (paramValues != null)
                    {
                        LogParameters(command, paramValues);
                    }
                    _provider.Log.WriteLine();
                }
            }

            protected virtual void LogParameters(QueryCommand command, object[] paramValues)
            {
                if (_provider.Log != null && paramValues != null)
                {
                    for (int i = 0, n = command.Parameters.Count; i < n; i++)
                    {
                        var p = command.Parameters[i];
                        var v = paramValues[i];

                        if (v == null || v == DBNull.Value)
                        {
                            _provider.Log.WriteLine("-- {0} = NULL", p.Name);
                        }
                        else
                        {
                            _provider.Log.WriteLine("-- {0} = [{1}]", p.Name, v);
                        }
                    }
                }
            }
        }

        protected class DbFieldReader : FieldReader
        {
            QueryExecutor _executor;
            DbDataReader _reader;

            public DbFieldReader(QueryExecutor executor, DbDataReader reader)
            {
                _executor = executor;
                _reader = reader;
                Init();
            }

            protected override int FieldCount
            {
                get { return _reader.FieldCount; }
            }

            protected override Type GetFieldType(int ordinal)
            {
                return _reader.GetFieldType(ordinal);
            }

            protected override bool IsDBNull(int ordinal)
            {
                return _reader.IsDBNull(ordinal);
            }

            protected override T GetValue<T>(int ordinal)
            {
                return (T)_executor.Convert(_reader.GetValue(ordinal), typeof(T));
            }

            protected override Byte GetByte(int ordinal)
            {
                return _reader.GetByte(ordinal);
            }

            protected override Char GetChar(int ordinal)
            {
                return _reader.GetChar(ordinal);
            }

            protected override DateTime GetDateTime(int ordinal)
            {
                return _reader.GetDateTime(ordinal);
            }

            protected override Decimal GetDecimal(int ordinal)
            {
                return _reader.GetDecimal(ordinal);
            }

            protected override Double GetDouble(int ordinal)
            {
                return _reader.GetDouble(ordinal);
            }

            protected override Single GetSingle(int ordinal)
            {
                return _reader.GetFloat(ordinal);
            }

            protected override Guid GetGuid(int ordinal)
            {
                return _reader.GetGuid(ordinal);
            }

            protected override Int16 GetInt16(int ordinal)
            {
                return _reader.GetInt16(ordinal);
            }

            protected override Int32 GetInt32(int ordinal)
            {
                return _reader.GetInt32(ordinal);
            }

            protected override Int64 GetInt64(int ordinal)
            {
                return _reader.GetInt64(ordinal);
            }

            protected override String GetString(int ordinal)
            {
                return _reader.GetString(ordinal);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                var dbConnection = _connection;

                _connection = null;

                if (dbConnection != null) dbConnection.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}
