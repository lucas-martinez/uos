﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Persistence
{
    public enum DbLinkTypes
    {
        /// <summary>
        /// Db connection context
        /// </summary>
        Context = 0,

        /// <summary>
        /// Non-query stored procedure
        /// </summary>
        NonQuery = 1,

        /// <summary>
        /// Query stored procedure
        /// </summary>
        Query = 2,

        /// <summary>
        /// (question) Scalar stored procedure
        /// </summary>
        Quest = 3,

        /// <summary>
        /// Single table store
        /// </summary>
        Store = 4,

        /// <summary>
        /// Single or multiple tables view
        /// </summary>
        View = 5
    }
}
