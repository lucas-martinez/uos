﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Persistence
{
    public interface IStore : IDisposable
    {
        void SaveChanges();
    }

    public interface IStore<T> : IStore, IEnumerable<T> where T : class, new()
    {
        void Create(T entity);

        int Count();

        void Delete(T entity);

        IStore<T> Page(string orderBy, int limit, int offset);

        IStore<T> Where(Expression<Func<T, bool>> predicate);

        void Update(T entity);
    }
}
