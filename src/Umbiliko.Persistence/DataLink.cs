﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Persistence
{
    using Uos.Inquiry;

    public class DataLink
    {
        private readonly DataSet _dataSet;

        public DataLink()
            : this(new DataSet())
        {
        }

        public DataLink(string dataSetName)
            : this(new DataSet(dataSetName))
        {
        }

        public DataLink(DataSet dataSet)
        {
            _dataSet = dataSet;
        }

        public DataSet DataSet
        {
            get { return _dataSet; }
        }
    }
}
