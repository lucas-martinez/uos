﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Persistence
{
    using Data.Schema;
    using Linq;

    public static class EntityTypeExtensions
    {
        public static void Apply<TEntity, TDbConnection, TDbCommand, TDbDataAdapter, TDbParameter>(this EntityType<TEntity> entityType, DbStore<TEntity, TDbConnection, TDbCommand, TDbDataAdapter, TDbParameter> dbStore)
            where TEntity : class, new()
            where TDbConnection : System.Data.Common.DbConnection, new()
            where TDbCommand : System.Data.Common.DbCommand, new()
            where TDbDataAdapter : System.Data.Common.DbDataAdapter, new()
            where TDbParameter : System.Data.Common.DbParameter, new()
        {
        }

        public static IEntityTypeMapper CreateMapper<TEntity>(this EntityType<TEntity> entityType)
            where TEntity : class, new()
        {
            return new EntityTypeMapper<TEntity>(entityType);
        }
    }
}
