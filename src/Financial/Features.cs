﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Financial
{
    /// <summary>
    /// Financial Applications features
    /// </summary>
    public class Features
    {
        /// <summary>
        /// Financial Accounting
        /// </summary>
        public const string Accounting = @"FI";

        /// <summary>
        /// Controlling
        /// </summary>
        public const string Controlling = @"CO";

        /// <summary>
        /// Enterprise Controlling
        /// </summary>
        public const string Enterprise = @"EC";

        /// <summary>
        /// Capital Investment Management
        /// </summary>
        public const string Investment = @"IM";

        /// <summary>
        /// Treasury
        /// </summary>
        public const string Treasury = @"TR";
    }
}
