﻿using Microsoft.AspNet.Identity.Owin;
using System.Net.Http;
using System.Web.Http;

namespace Uos.Web.Http
{
    using Contracts;
    using Providers;

    public abstract class DataController : ApiController
    {
        IAuthenticationContext _authentication;

        public IAuthenticationContext Authentication
        {
            get { return _authentication ?? (_authentication = Request.GetOwinContext().Get<IAuthenticationContext>()); }
        }
    }
}
