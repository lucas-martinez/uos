define(['jquery', 'settings', 'q'], function($, settings, Q) {

	function authenticate() {
		var deferred = Q.defer();
		
		$.ajax({
			url: settings.baseUrl + '/authentication/sign-in',
			type: "POST",
	        dataType: "json",
	        success: function (data, status, jqXHR) {
	        },
	        error: function (jqXHR, status, error) {
	        }
		});
		
		return deferred.promise;
	};

	function initialize() {
		var account = $.cookie('user-account'),
			email = $.cookie('user-email'),
			token = $.cookie('_Token');

		console.log('email', email);
		console.log('account', account);

		if (account) {
			$('#header .user-credentials .account').val(account);
			$('#header .hidden.user-anonymous').removeClass('hidden');
		}
		else if (email) {
			$('#header .user-anonymous .email').val(email);
			$('#header .hidden.user-anonymous').removeClass('hidden');
		}
		else {
			$('#header .hidden.user-anonymous').removeClass('hidden');
		}
	};

	function register(identifier) {
		var deferred = Q.defer();

		$.ajax({
			url: settings.baseUrl + '/authentication/ckeck-in',
			type: "POST",
	        dataType: "json",
	        statusCode: {
			    404: function() {
			    	alert( "page not found" );
			    }
			},
	        success: function (data, status, jqXHR) {
	        },
	        error: function (jqXHR, status, error) {
	        	deferred.reject();
	        }
		});

		return deferred.promise;
	};

	initialize();

	$('#header .email').on('input', function (e) {
		if ($(e.target).val()) {
			$('#header .email-go').removeAttr('disabled');
		}
		else {
			$('#header .email-go').attr('disabled', 'true');
		}
	})

	$('#header .email-go').click(function () {
		var id = $('#header .email').val();

		$('#header .email-go').attr('disabled', 'true');

		register(id)
			.then(function (data) {
				$('#header .user-anonymous').addClass('hidden');
				$('#header .email-go').removeAttr('disabled');
			}, function (error) {
			});
	});

	return {
	};
});