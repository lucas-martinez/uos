﻿(function (undefined) {

    var umbiliko = window.umbiliko || {};

    window.umbiliko = umbiliko;

    var _enterFrame;
    var _events = {
        'enter-widget-active': {
            listeners: []
        },
        'enter-widget-hover': {
            listeners: []
        },
        'error': {
            listeners: []
        }
    };
    var _hOP = _events.hasOwnProperty;

    function doNothing() {
    };

    umbiliko.setupEnterFrame = function (id) {
        _enterFrame = window.frames[id];
        _enterFrame.show();
    };

    umbiliko.event.publish = function(event) {
        if (!_hOP.call(_events, event)) {
            _events[event] = {};

            return;
        }

        if (!_events[event].listeners) {
            return;
        }

        var args = [].splice.call(arguments, 1);

        // Cycle through topics queue, fire!
        _events[event].listeners.forEach(function (listener) {
            listener.apply(umbiliko, args);
        });
    };

    umbiliko.subscribe = function (event, listener) {
        if (!_hOP.call(_events, event)) {
            publish('error', { message: "Can not subscribe to event " + event + ". It does not exist." });

            return {
                remove: doNothing
            };
        }

        if (!_events[event].listeners) {
            _events[event].listeners = [];
        }

        var index = _events[event].listeners.push(listener) - 1;

        return {
            remove: function () {
                delete _events[event][index];

            }
        };
    };
})();