﻿'use strict';

// Crockford's supplant method (poor man's templating)
if (!String.prototype.supplant) {
    String.prototype.supplant = function (o) {
        return this.replace(/{([^{}]*)}/g,
            function (a, b) {
                var r = o[b];
                return typeof r === 'string' || typeof r === 'number' ? r : a;
            }
        );
    };
}

// A simple background color flash effect that uses jQuery Color plugin
jQuery.fn.flash = function (color, duration) {
    var current = this.css('backgroundColor');
    this.animate({ backgroundColor: 'rgb(' + color + ')' }, duration / 2)
        .animate({ backgroundColor: current }, duration / 2);
};

$(function () {

    var ticker = $.connection.stockTicker, // the generated client-side hub proxy
        up = '▲',
        down = '▼',
        $stockTable = $('#stockTable'),
        $stockTableBody = $stockTable.find('tbody'),
        rowTemplate = '<tr data-symbol="{Symbol}"><td>{Symbol}</td><td>{Price}</td><td>{DayOpen}</td><td>{DayHigh}</td><td>{DayLow}</td><td><span class="dir {DirectionClass}">{Direction}</span> {Change}</td><td>{PercentChange}</td></tr>',
        $stockTicker = $('#stockTicker'),
        $stockTickerUl = $stockTicker.find('ul'),
        liTemplate = '<li data-symbol="{Symbol}"><span class="symbol">{Symbol}</span> <span class="price">{Price}</span> <span class="change"><span class="dir {DirectionClass}">{Direction}</span> {Change} ({PercentChange})</span></li>';

    function formatStock(stock) {
        return $.extend(stock, {
            Price: stock.Price.toFixed(2),
            PercentChange: (stock.PercentChange * 100).toFixed(2) + '%',
            Direction: stock.Change === 0 ? '' : stock.Change >= 0 ? up : down,
            DirectionClass: stock.Change === 0 ? 'even' : stock.Change >= 0 ? 'up' : 'down'
        });
    }

    function scrollTicker() {
        var w = $stockTickerUl.width();
        $stockTickerUl.css({ marginLeft: w });
        $stockTickerUl.animate({ marginLeft: -w }, 15000, 'linear', scrollTicker);
    }

    function stopTicker() {
        $stockTickerUl.stop();
    }

    function init() {
        return ticker.server.getAllStocks().done(function (stocks) {
            $stockTableBody.empty();
            $stockTickerUl.empty();
            $.each(stocks, function () {
                var stock = formatStock(this);
                $stockTableBody.append(rowTemplate.supplant(stock));
                $stockTickerUl.append(liTemplate.supplant(stock));
            });
        });
    }

    // Add client-side hub methods that the server will call
    $.extend(ticker.client, {
        updateStockPrice: function (stock) {
            var displayStock = formatStock(stock),
                $row = $(rowTemplate.supplant(displayStock)),
                $li = $(liTemplate.supplant(displayStock)),
                bg = stock.LastChange < 0
                        ? '255,148,148' // red
                        : '154,240,117'; // green

            $stockTableBody.find('tr[data-symbol=' + stock.Symbol + ']')
                .replaceWith($row);
            $stockTickerUl.find('li[data-symbol=' + stock.Symbol + ']')
                .replaceWith($li);

            $row.flash(bg, 1000);
            $li.flash(bg, 1000);
        },

        marketOpened: function () {
            $("#open").prop("disabled", true);
            $("#close").prop("disabled", false);
            $("#reset").prop("disabled", true);
            scrollTicker();
        },

        marketClosed: function () {
            $("#open").prop("disabled", false);
            $("#close").prop("disabled", true);
            $("#reset").prop("disabled", false);
            stopTicker();
        },

        marketReset: function () {
            return init();
        }
    });

    // Start the connection
    $.connection.hub.start()
        .then(init)
        .then(function () {
            return ticker.server.getMarketState();
        })
        .done(function (state) {
            if (state === 'Open') {
                ticker.client.marketOpened();
            } else {
                ticker.client.marketClosed();
            }

            // Wire up the buttons
            $("#open").click(function () {
                ticker.server.openMarket();
            });

            $("#close").click(function () {
                ticker.server.closeMarket();
            });

            $("#reset").click(function () {
                ticker.server.reset();
            });
        });
});

(function () {

    var dashboard = angular.module('dashboard', ['dev-dashboard', 'dev-gauge', 'ng.epoch']);

    dashboard.value('backendServerUrl', 'http://localhost:55665/');

    // service
    dashboard.factory('backendHubProxy', ['$rootScope', 'backendServerUrl',
        function ($rootScope, backendServerUrl) {

            function backendFactory(serverUrl, hubName) {
                var connection = $.hubConnection(backendServerUrl);
                var proxy = connection.createHubProxy(hubName);

                connection.start().done(function () { });

                return {
                    on: function (eventName, callback) {
                        proxy.on(eventName, function (result) {
                            $rootScope.$apply(function () {
                                if (callback) {
                                    callback(result);
                                }
                            });
                        });
                    },
                    invoke: function (methodName, callback) {
                        proxy.invoke(methodName)
                        .done(function (result) {
                            $rootScope.$apply(function () {
                                if (callback) {
                                    callback(result);
                                }
                            });
                        });
                    }
                };
            };

            return backendFactory;
        }]);

    // controller
    dashboard.controller('PerformanceDataController', ['$scope', 'backendHubProxy',
        function ($scope, backendHubProxy) {
            console.log('trying to connect to service')
            var performanceDataHub = backendHubProxy(backendHubProxy.defaultServer, 'performanceHub');
            console.log('connected to service')

            //Placeholders until data is pushed.
            $scope.currentRamNumber = 68;

            $scope.realtimeArea = [
                { label: 'Layer 1', values: [] },
                { label: 'Layer 2', values: [] },
                { label: 'Layer 3', values: [] },
                { label: 'Layer 4', values: [] },
                { label: 'Layer 5', values: [] },
                { label: 'Layer 6', values: [] }
            ];

            $scope.options = { thickness: 10, mode: "gauge", total: 100 };
            $scope.data = [
                { label: "CPU", value: 78, color: "#d62728", suffix: "%" }
            ];

            performanceDataHub.on('broadcastPerformance', function (data) {
                var timestamp = ((new Date()).getTime() / 1000) | 0;
                var chartEntry = [];

                data.forEach(function (dataItem) {

                    switch (dataItem.categoryName) {
                        case "Processor":
                            $scope.cpuData = dataItem.value;
                            chartEntry.push({ time: timestamp, y: dataItem.value });
                            $scope.data = [
                                { label: "CPU", value: dataItem.value, color: "#d62728", suffix: "%" }
                            ];
                            console.log($scope.data)
                            break;

                        case "Memory":
                            $scope.currentRamNumber = dataItem.value;
                            chartEntry.push({ time: timestamp, y: dataItem.value });
                            break;

                        case "Network In":
                            $scope.netInData = dataItem.value.toFixed(2);
                            chartEntry.push({ time: timestamp, y: dataItem.value });
                            break;

                        case "Network Out":
                            $scope.netOutData = dataItem.value.toFixed(2);
                            chartEntry.push({ time: timestamp, y: dataItem.value });
                            break;

                        case "Disk Read Bytes/Sec":
                            $scope.diskReaddData = dataItem.value.toFixed(3);
                            chartEntry.push({ time: timestamp, y: dataItem.value });
                            break;

                        case "Disk Write Bytes/Sec":
                            $scope.diskWriteData = dataItem.value.toFixed(3);
                            chartEntry.push({ time: timestamp, y: dataItem.value });
                            break;

                        default:
                            break;
                            //default code block
                    }
                });

                $scope.realtimeAreaFeed = chartEntry;
            });

            $scope.areaAxes = ['left', 'right', 'bottom'];
        }]);

    return dashboard;
})();