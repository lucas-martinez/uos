﻿require.config({
    baseUrl: '/scripts',
    paths: {
        jquery: 'jquery-1.10.2.min',
        knockout: 'knockout-3.3.0',
        toastr: 'vendor/toastr/toastr'
    }
});

require(['jquery', 'knockout', 'toastr', ], function ($, ko, toastr, undefined) {

    function array() {
        var arr = [];
        for (var i in arguments) {
            if (arguments[i]) {
                arr.push(arguments[i]);
            }
        }
        return arr;
    }

    function ContactViewModel() {

        var vm = this;

        function reset() {
            vm.validated(false);
            vm.data.Company(null);
            vm.data.Email(null);
            vm.data.Interests([]);
            vm.data.Message(null);
            vm.data.Name(null);
            vm.data.Website(null);
        };

        function send() {

            var errors = vm.error.any();

            vm.validated(true);

            if (errors) {

                for (var i in errors) {
                    toastr.error(errors[i]);
                }

                return $.Deferred().reject().promise();
            }

            var $indicator = $('<span class="send-indicator" title="sending message..."><i class="fa fa-envelope fa-2x active hover-icon-pulse-grow"></i></span>');

            $indicator.appendTo('#contact .send-indicator-container');

            return $.ajax({
                type: 'POST',
                url: '/contact',
                contentType: 'application/json; charset=utf-8',
                data: ko.toJSON(vm.data),
                timeout: 30000
            }).always(function () {
                $('i', $indicator).removeClass('active');
                setTimeout(function () {
                    $indicator.remove();
                    delete $indicator;
                }, 2000);
            }).done(function (response) {
                if (response.success) {
                    $indicator.addClass('success');
                    toastr.success("Message sent.");
                    vm.reset();
                }
                else {
                    $indicator.addClass('error');
                    toastr.error(response.message);
                }
            }).error(function (request, error) {
                if (error == "timeout") {
                    $indicator.addClass('error');
                    toastr.error("Timed out when contacting server.");
                }
                else {
                    $indicator.addClass('error');
                    toastr.error("Something is not working. Please try again.");
                }
            });
        };

        function validate() {
            var name = vm.data.Name();
            if (!name || !name.trim()) {
                vm.error.name("Your name is required.");
            } else {
                vm.error.name(undefined);
            }

            var checkEmail = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            var email = vm.data.Email();
            if (!email || !email.trim()) {
                vm.error.email("Your email address is required.");
            } else if (!checkEmail.test(email)) {
                vm.error.email("Your email address is not valid.");
            } else {
                vm.error.email(undefined);
            }

            var message = vm.data.Message();
            if (!message || !message.trim()) {
                vm.error.message("Your project description is required.");
            } else {
                vm.error.message(undefined);
            }
        };

        vm.data = {
            Company: ko.observable(null),
            Email: ko.observable(null),
            Interests: ko.observableArray([]),
            Message: ko.observable(null),
            Name: ko.observable(null),
            Website: ko.observable(null)
        };

        vm.dirty = ko.computed(function () {
            return ko.toJSON(vm.data) !== vm.original;
        });

        vm.error = {
            company: ko.observable(false),
            email: ko.observable(false),
            interests: ko.observable(false),
            message: ko.observable(false),
            name: ko.observable(false),
            website: ko.observable(false),
        };

        vm.error.any = ko.computed(function () {

            validate();

            var arr = array(vm.error.company(), vm.error.email(), vm.error.interests(), vm.error.message(), vm.error.name(), vm.error.website());

            return arr.length > 0 ? arr : undefined;
        });

        vm.error.info = ko.computed(function () {

            validate();

            var arr = array(vm.error.company() || vm.error.email() || vm.error.name() || vm.error.website());

            return arr.length > 0 ? arr : undefined;
        });

        vm.original = ko.toJSON(vm.data);

        vm.reset = reset;

        vm.send = send;

        vm.validate = validate;

        vm.validated = ko.observable(false);
    };

    $(document).ready(function () {
        ko.applyBindings({
            contact: new ContactViewModel()
        });
    });
});


/*
    The function below is used to:
    1. enable smooth scrolling
    2. in the collapsed mode: close the main menu when an item is clicked
*/
$(".scroll").click(function (event) {
    event.preventDefault();
    $("html,body").animate({ scrollTop: $(this.hash).offset().top }, 500);
    if ($('.navbar-collapse').hasClass('in')) {
        $('.navbar-collapse').removeClass('in').addClass('collapse');
    }
});

/*
    The function below is used to:
    1. enable the MixItUp plugin for filtering work items
    2. refresh the scrollspy when the filters are applied
*/
$(function () {
    $('#gallery').mixitup({
        onMixEnd: function () {
            $('[data-spy="scroll"]').each(function () {
                var $spy = $('body').scrollspy('refresh');
            });
        }
    });
});

$(document).ready(function () {
    /*
        The function below is used to:
        1. start the ticker on the home page
    */
    var current = 1;
    var height = $('.ticker').height();
    var numberDivs = $('.ticker').children().length;
    var first = $('.ticker h3:nth-child(1)');
    setInterval(function () {
        var number = current * -height;
        first.css('margin-top', number + 'px');
        if (current === numberDivs) {
            first.css('margin-top', '0px');
            current = 1;
        } else current++;
    }, 2500);

    /*
        The function below is used to:
        1. start the carousel in the about us section
    */
    $('.carousel').carousel({
        interval: 2500
    });

    /*
        The function below is used to:
        1. start the gallery in the work section
    */
    $("#gallery a[data-gal^='prettyPhoto']").prettyPhoto({ slideshow: false, allow_expand: false, allow_resize: true, social_tools: false, theme: 'light_square', deeplinking: false });

    //.parallax(xPosition, speedFactor, outerHeight) options:
    //xPosition - Horizontal position of the element
    //inertia - speed to move relative to vertical scroll. Example: 0.1 is one tenth the speed of scrolling, 2 is twice the speed of scrolling
    //outerHeight (true/false) - Whether or not jQuery should use it's outerHeight option to determine when a section is in the viewport
    // parallax background
    $('#header').parallax("50%", 0.1);
    $('#services').parallax("50%", 0.2);
    $('#clients').parallax("50%", 0.1);
});