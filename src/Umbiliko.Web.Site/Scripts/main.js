require.config({
    baseUrl: '',
    paths: {
    	angular: '//ajax.googleapis.com/ajax/libs/angularjs/1.3.3/angular.min',
        app: 'scripts/app',
    	Bootstrap: '//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min',
        cookie: 'scripts/vendor/cookie/jquery.cookie',
    	jquery: '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min',
        imagescroll: 'scripts/vendor/image-scroll/jquery.imagescroll.min',
        parallax: 'scripts/vendor/parallax/jquery.parallax.min',
        q: 'scripts/vendor/q/q',
        settings: 'scripts/settings',
        scrollmagic: 'scripts/vendor/scroll-magic/jquery.scrollmagic.min',
    	Uos: 'scripts/umbiliko'
    },
    shim: {
        app: ['jquery', 'q', 'cookie'],
        Bootstrap: ['jquery'],
        'scripts/vendor/parallax/jquery.event.frame': ['jquery'],
        parallax: ['jquery', 'imagescroll', 'scripts/vendor/parallax/jquery.event.frame'],
    	Uos: ['jquery']
    }
});

define(['jquery', 'Uos', 'Bootstrap', 'parallax'], function ($, Uos) {
    'use strict'

    /* The Angular application cannot be bootstrapped 
    using the ng-app directive as the required script 
    files are loaded asynchronously.

    The right approach here is to use manual bootstrapping.*/
    require(['app'], function() {
    });

    /* affix the navbar after scroll below header */
    $('#navbar').affix({
        offset: {
            top: $('#header').height()
        }
    });
    /* highlight the top nav as scrolling occurs */
    $('body').scrollspy({ target: '#navbar' });

    /* smooth scrolling for scroll to top */
    $('.scroll-top').click(function(){
        $('body,html').animate({ scrollTop: 0 }, 1000);
    });

    /*
        The function below is used to:
        1. enable smooth scrolling
        2. in the collapsed mode: close the main menu when an item is clicked
    */
    $(".scroll").click(function(event){
        event.preventDefault();
        $('body,html').animate({ scrollTop: $(this.hash).offset().top }, 500);
        if ($('.navbar-collapse').hasClass('in')){
            $('.navbar-collapse').removeClass('in').addClass('collapse');
        }
    });

    $(document).ready(function() {
        /*
            The function below is used to:
            1. start the ticker on the home page
        */
        var current = 1;
        var height = $('.ticker').height();
        var numberDivs = $('.ticker').children().length;
        var first = $('.ticker h3:nth-child(1)');
        setInterval(function() {
            var number = current * -height;
            first.css('margin-top', number + 'px');
            if (current === numberDivs) {
                first.css('margin-top', '0px');
                current = 1;
            } else current++;
        }, 2500);

        /*
            The function below is used to:
            1. start the carousel in the about us section
        */
        $('.carousel').carousel({
            interval: 2500
        });

        /*
            The function below is used to:
            1. start the gallery in the work section
        */
        //$("#gallery a[data-gal^='prettyPhoto']").prettyPhoto({slideshow: false, allow_expand: false, allow_resize: true, social_tools: false, theme: 'light_square', deeplinking: false});

        //.parallax(xPosition, speedFactor, outerHeight) options:
        //xPosition - Horizontal position of the element
        //inertia - speed to move relative to vertical scroll. Example: 0.1 is one tenth the speed of scrolling, 2 is twice the speed of scrolling
        //outerHeight (true/false) - Whether or not jQuery should use it's outerHeight option to determine when a section is in the viewport
        // parallax background
        //$('#header').parallax("50%", 0.1);
        //$('#services').parallax("50%", 0.2);
        //$('#clients').parallax("50%", 0.1);
        /*
        $('#gallery').mixitup({
            onMixEnd: function(){
                $('[data-spy="scroll"]').each(function () {
                    var $spy = $('body').scrollspy('refresh');
                });
            }
        });*/
    });

    var canvas = $('.umbiliko canvas'),
        width = canvas.get(0).width,
        radio = Math.round(width / 2),
        umbiliko = new Uos({
        center: { x: radio, y: radio },
        radio: radio,
        split: radio / 20
    });

    //canvas.css('bottom', (30 * width / 100) + 'px');
    canvas.get(0).height = width;

    umbiliko.rotate(
        (umbiliko.x().x != NaN ? umbiliko.x().x / 7 : 0),
        (umbiliko.y().y != NaN ? umbiliko.y().y / 5 : 0),
        Math.PI / 3);
    
    setInterval(function () {
        umbiliko.rotate(
            (umbiliko.x().x != NaN ? umbiliko.x().x / 666 : 0),
            (umbiliko.y().y != NaN ? umbiliko.y().y / 666 : 0),
            Math.PI / 666);
        var ctx = canvas.get(0).getContext("2d");
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        umbiliko.paint(ctx);
    }, 66);

    /*if (!Modernizr.svg) {
    }*/
});