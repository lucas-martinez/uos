﻿'use strict';

describe('accounts.reset-password module', function () {

    beforeEach(module('accounts.reset-password'));

    describe('reset-password controller', function () {

        it('should ....', inject(function ($controller) {
            //spec body
            var ctrl = $controller('AccountResetPassword');
            expect(ctrl).toBeDefined();
        }));

    });
});