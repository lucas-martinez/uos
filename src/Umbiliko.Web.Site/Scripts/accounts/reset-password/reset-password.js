﻿'use strict';

angular.module('accounts.reset-password', ['ngRoute'])

.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/reset-password', {
        templateUrl: '/accounts/views/administration/reset-password',
        controller: 'AccountResetPassword'
    });
}])

.controller('AccountResetPassword', [function () {

}]);