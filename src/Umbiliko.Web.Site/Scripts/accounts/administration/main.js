﻿'use strict';

angular.module('accounts.administration', ['ngRoute'])

.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/accounts/administration', {
        templateUrl: '/accounts/views/administration',
        controller: 'AccountsAdministration'
    });
}])

.controller('AccountsAdministration', [function () {

}]);