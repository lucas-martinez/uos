﻿'use strict';
//http://demos.telerik.com/kendo-ui/grid/angular
angular.module('accounts.administration.users', ['ngRoute', "kendo.directives"])

.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/accounts/administration/users', {
        templateUrl: '/accounts/administration/views/users',
        controller: 'AccountsAdministrationUsers'
    });
}])

.controller('AccountsAdministrationUsers', [function ($scope) {

    $scope.mainGridOptions = {
        dataSource: {
            type: "odata",
            transport: {
                read: "/accounts/administration/odata/users"
            },
            pageSize: 5,
            serverPaging: true,
            serverSorting: true
        },
        sortable: true,
        pageable: true,
        dataBound: function () {
            this.expandRow(this.tbody.find("tr.k-master-row").first());
        },
        columns: [{
            field: "FirstName",
            title: "First Name",
            width: "120px"
        }, {
            field: "LastName",
            title: "Last Name",
            width: "120px"
        }, {
            field: "Country",
            width: "120px"
        }, {
            field: "City",
            width: "120px"
        }, {
            field: "Title"
        }]
    };

    $scope.detailGridOptions = function (dataItem) {
        return {
            dataSource: {
                type: "odata",
                transport: {
                    read: "/accounts/administration/odata/users"
                },
                serverPaging: true,
                serverSorting: true,
                serverFiltering: true,
                pageSize: 5,
                filter: { field: "UserId", operator: "eq", value: dataItem.UserId }
            },
            scrollable: false,
            sortable: true,
            pageable: true,
            columns: [
            { field: "OrderID", title: "ID", width: "56px" },
            { field: "ShipCountry", title: "Ship Country", width: "110px" },
            { field: "ShipAddress", title: "Ship Address" },
            { field: "ShipName", title: "Ship Name", width: "190px" }
            ]
        };
    };

}]);