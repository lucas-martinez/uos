﻿'use strict';

angular.module('accounts.authentication', ['ngRoute'])

.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/accounts/authentication', {
        templateUrl: '/accounts/views/authentication',
        controller: 'AccountAuthentication'
    });
}])

.controller('AccountAuthentication', [function () {

}]);