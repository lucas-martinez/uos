﻿'use strict';

angular.module('accounts.verify-code', ['ngRoute'])

.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/verify-code', {
        templateUrl: '/accounts/views/administration/verify-code',
        controller: 'AccountVerifyCode'
    });
}])

.controller('AccountVerifyCode', [function () {

}]);