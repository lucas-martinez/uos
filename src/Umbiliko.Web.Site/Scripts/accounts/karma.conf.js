﻿module.exports = function (config) {
    config.set({

        basePath: './',

        files: [
          '../vendors/angular/angular.js',
          '../vendors/angular-route/angular-route.js',
          '../vendors/angular-mocks/angular-mocks.js',
          'components/**/*.js',
          '*/**/*.js',
          'accounts.js'
        ],

        autoWatch: true,

        frameworks: ['jasmine'],

        browsers: ['Chrome'],

        plugins: [
                'karma-chrome-launcher',
                'karma-firefox-launcher',
                'karma-jasmine',
                'karma-junit-reporter'
        ],

        junitReporter: {
            outputFile: 'test_out/unit.xml',
            suite: 'unit'
        }

    });
};