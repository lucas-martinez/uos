﻿'use strict';

// Declare app level module which depends on views, and components
angular.module('accounts', [
  'ngRoute',
  'accounts.administration',
  'accounts.authentication',
  'accounts.confirm-email',
  'accounts.forgort-password',
  'accounts.registration',
  'accounts.reset-password',
  'accounts.verify-code',
  'accounts.version'
]).
config(['$routeProvider', function ($routeProvider) {
    $routeProvider.otherwise({ redirectTo: '/accounts/authentication' });
}]);