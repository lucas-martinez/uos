﻿'use strict';

angular.module('accounts.forgort-password', ['ngRoute'])

.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/forgort-password', {
        templateUrl: '/accounts/views/authentication/forgort-password',
        controller: 'AccountForgortPassword'
    });
}])

.controller('AccountForgortPassword', [function () {

}]);