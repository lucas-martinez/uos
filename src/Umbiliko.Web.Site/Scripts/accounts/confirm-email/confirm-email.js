﻿'use strict';

angular.module('accounts.confirm-email', ['ngRoute'])

.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/accounts/administration/confirm-email', {
        templateUrl: '/accounts/views/administration/confirm-email',
        controller: 'AccountConfirmEmail'
    });
}])

.controller('AccountConfirmEmail', [function () {

}]);