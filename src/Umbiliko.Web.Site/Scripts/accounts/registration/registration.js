﻿'use strict';

angular.module('accounts.registration', ['ngRoute'])

.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('accounts/registration', {
        templateUrl: 'accounts/views/registration',
        controller: 'AccountRegistration'
    });
}])

.controller('AccountRegistration', [function () {

}]);