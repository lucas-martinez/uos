﻿'use strict';

angular.module('accounts.version', [
  'accounts.version.interpolate-filter',
  'accounts.version.version-directive'
])

.value('version', '0.1');