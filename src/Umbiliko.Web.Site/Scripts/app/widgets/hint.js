﻿define('umbiliko.widget.hint', ['jquery', 'require', 'umbiliko'], function ($, require, U) {

    U.widget = U.widget || {};

    U.widget.hint = function () {

        return {

        };
    };

    require(['angular'], function () {
        U.hint.controller = U.module.controller('UmbilikoHintController', ['$scope', function ($scope) {
            $scope.greeting = 'Hola!';
        }]);
    });

    return U.widget.hint;
});