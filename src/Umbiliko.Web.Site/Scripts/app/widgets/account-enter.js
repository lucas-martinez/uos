﻿define('umbiliko.widget.account-enter', ['jquery', 'require', 'umbiliko', 'umbiliko.email', 'umbiliko.password', 'umbiliko.phone', 'umbiliko.user'], function ($, require, U) {
    var _config = {
        options: {
            'show-password': {
                hide: ['#umbiliko-sign-up-confirm-password', '#umbiliko-reset-confirm-password'],
                text: ['#umbiliko-sign-in-password', '#umbiliko-sign-up-password', '#umbiliko-reset-new-password']
            }
        },
        views: {
            'sign-in': {
                hide: ['[value=sign-in]', '[value=reset-password]'],
                show: ['.external-logins', '[name=persist]', '[name=show-password]', '[value=recover-password]', '[value=sign-up]']
            },
            'recover-password': {
                hide: [/*'.external-logins',*/ '[name=persist]', '[name=show-password]', '[value=recover-password]'],
                show: ['[value=sign-in]', '[value=sign-up]', '[value=reset-password]']
            },
            'sign-up': {
                hide: [/*'.external-logins',*/ '[value=sign-up]', '[value=reset-password]'],
                show: ['[value=sign-in]', '[name=persist]', '[name=show-password]', '[value=recover-password]']
            },
            'reset-password': {
                hide: [/*'.external-logins'*/, '[value=reset-password]'],
                show: ['[name=persist]', '[name=show-password]', '[value=sign-in]', '[value=recover-password]', '[value=sign-up]']
            }
        }
    };

    var factory = function ($html, options) {
        var $el,
            $modal,
            $placeholder,
            _dfd,
            _completed = false,
            _done = [],
            _ready = [],
            _view = 'sign-in',
            _returnUrl = !!options ? options.returnUrl || null : null;

        if (typeof options.replace === 'string' && ($placeholder = $(options.replace)).length === 1) {

            var returnUrl = $placeholder.data('return');

            if (returnUrl) {
                _returnUrl = returnUrl;
            }

            $el = $html.replaceAll($placeholder);
        }
        else {
            $modal = $('<div class="umbiliko hidden modal-backdrop"></div>').appendTo($('body'));

            $el = $html.appendTo($modal);
        }

        /*if (!_returnUrl) {
            _returnUrl = _params.returnurl;
        }

        U.event.subscribe('enter', function (user) {
            console.log('event enter handled on enter widget')
            if ($el) {
                $el.replaceWith($placeholder);
                $.each(_done, function (index, fn) {
                    fn($el);
                });
            }
            if (_returnUrl) {
                location = _returnUrl;
            } else {
                location.reload();
            }
        });

        U.auth.setup();*/

        var $form = $('form', $el).first(),
            _baseUrl = $form.prop('action'),
            _mehtod = $form.prop('method');

        $('input[name=view]', $el).on('change', function () {
            if (!!$(this).prop('checked')) {
                _view = $(this).prop('value');
            }
            refresh();
        });

        $('input[name=show-password]', $el).on('change', function () {
            showPassword($(this).prop('checked'));
        });

        $('.btn-link', $el).click(function (e) {
            $.each(_done, function (index, fn) {
                fn($el);
            });
        });

        $('[name=cmd-enter]', $el).click(function (e) {

            e.preventDefault();

            if (!_dfd) {
                return;
            }

            // validate

            var data = {},
                $view = $('[name=' + _view + ']', $el);

            $.each($view.serializeArray(), function () {
                data[this.name] = this.value || null;
            });

            var result = {};

            result[_view] = data;

            _dfd.resolve(result);

            _dfd = null;
        });

        /*
        function done(fn) {
            if (typeof fn !== 'function') {
                return this;
            }
            if (_completed) {
                fn($el);
            } else {
                _done.push(fn);
            }
            return this;
        };

        function ready(fn) {
            if (typeof fn !== 'function') {
                return this;
            }
            if ($el) {
                fn($el);
            } else {
                _ready.push(fn);
            }
            return this;
        };*/

        var refresh = function refresh() {
            $.each(_config.views, function (name) {
                var $view = $('fieldset[name=' + name + ']', $el);
                if (name !== _view) {
                    $view.addClass('hidden');
                }
                else {
                    $view.removeClass('hidden');
                    var view = _config.views[_view];
                    $.each(view.hide, function (index, value) {
                        U.find.group($(value, $el)).addClass('hidden');
                    });
                    $.each(view.show, function (index, value) {
                        U.find.group($(value, $el)).removeClass('hidden');
                    });
                }
            });
        };

        var showPassword = function showPassword(show) {
            var o = _config.options['show-password'];
            if (show) {
                for (var i = 0; i < o.hide.length; i++) {
                    U.find.group($(o.hide[i], $el)).addClass('hidden');
                }
                for (var i = 0; i < o.text.length; i++) {
                    $(o.text[i], $el).prop('type', 'text');
                }
            } else {
                for (var i = 0; i < o.hide.length; i++) {
                    U.find.group($(o.hide[i], $el)).removeClass('hidden');
                }
                for (var i = 0; i < o.text.length; i++) {
                    $(o.text[i], $el).prop('type', 'password');
                }
            }
        };

        refresh();

        U.blend($el);

        return {
            $el: $el,
            $html: $html,
            $modal: $modal,
            $placeholder: $placeholder,
            /*blend: function () {
                if ($el) {
                    blend();
                } else {
                    _ready.push(blend);
                }
                return this;
            },*/
            //done: done,
            hide: function () {
                if (!!$modal) {
                    $modal.addClass('hidden');
                }
                else {
                    $el.addClass('hidden');
                }
            },
            input: function () {
                _dfd = $.Deferred();

                return _dfd.promise();
            },
            options: options,
            //ready: ready,
            returnUrl: _returnUrl,
            show: function () {
                if (!!$modal) {
                    $modal.removeClass('hidden');
                }
                else {
                    $el.removeClass('hidden');
                }
            }
        };
    };

    /*if (U.module) {
        require(['angular'], function () {

            factory.controller = U.module.controller('UmbilikoEnterController', ['$scope', function ($scope) {
                $scope.greeting = 'Hola!';
            }]);
        });
    }*/

    return factory;
});