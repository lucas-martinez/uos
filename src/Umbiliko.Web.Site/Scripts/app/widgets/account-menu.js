﻿define('umbiliko.widget.account-menu', ['jquery', 'require', 'umbiliko'], function ($, require, U) {

    U.widget = U.widget || {};

    var factory = function ($html, options) {
        var $el,
            $float,
            $placeholder,
            _dfd;

        if (!!options && typeof options.replace === 'string' && ($placeholder = $(options.replace)).length === 1) {

            //_returnUrl = $placeholder.data('return');

            $el = $html.replaceAll($placeholder);
        }
        else {
            $float = $('<div class="umbiliko hidden drag-float"></div>').appendTo($('body'));

            $el = $html.appendTo($float);
        }

        $el.find('.btn').click(function (e) {

            e.preventDefault();

            e.stopPropagation();

            var $btn = $(e.target),
                name = $btn.prop('name');

            U.event.publish(name + '-click');
        });
    };

    /*U.widget.menu = function () {

        return {

        };
    };*/

    /*require(['angular'], function () {
        U.menu.controller = U.module.controller('UmbilikoMenuController', ['$scope', function ($scope) {
            $scope.greeting = 'Hola!';
        }]);
    });*/

    return factory;
});