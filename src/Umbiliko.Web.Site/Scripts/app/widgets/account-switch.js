﻿define('umbiliko.widget.account-switch', ['jquery', 'umbiliko'], function ($, U) {

    var factory = function ($html, options) {
        var $el,
            $modal,
            $placeholder,
            _dfd;

        if (typeof options.replace === 'string' && ($placeholder = $(options.replace)).length === 1) {

            var returnUrl = $placeholder.data('return');

            if (returnUrl) {
                _returnUrl = returnUrl;
            }

            $el = $html.replaceAll($placeholder);
        }
        else {
            $modal = $('<div class="umbiliko hidden modal-backdrop"></div>').appendTo($('body'));

            $el = $html.appendTo($modal);
        }

        var $form = $('form', $el).first(),
           _baseUrl = $form.prop('action'),
           _mehtod = $form.prop('method');

        $('[name=cmd-switch]', $el).click(function (e) {

            e.preventDefault();

            if (!_dfd) {
                return;
            }

            // validate

            var data = {},
                $view = $('[name=account-switch]', $el);

            $.each($view.serializeArray(), function () {
                data[this.name] = this.value || null;
            });

            var result = {};

            result[_view] = data;

            _dfd.resolve(result);

            _dfd = null;
        });

        return {
            $el: $el,
            $html: $html,
            $modal: $modal,
            $placeholder: $placeholder,
            hide: function () {
                if (!!$modal) {
                    $modal.addClass('hidden');
                }
                else {
                    $el.addClass('hidden');
                }
            },
            input: function () {
                _dfd = $.Deferred();

                return _dfd.promise();
            },
            options: options,
            returnUrl: _returnUrl,
            show: function () {
                if (!!$modal) {
                    $modal.removeClass('hidden');
                }
                else {
                    $el.removeClass('hidden');
                }
            }
        };
    };

    return factory;
});