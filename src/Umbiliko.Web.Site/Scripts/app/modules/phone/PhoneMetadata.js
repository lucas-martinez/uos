﻿define('umbiliko.phone.PhoneMetadata', [], function () {
    /**
     * Message PhoneMetadata.
     * @constructor
     * @extends {goog.proto2.Message}
     */
    PhoneMetadata = function () {
        //goog.proto2.Message.call(this);
    };

    //goog.inherits(PhoneMetadata, goog.proto2.Message);

    /**
     * Overrides {@link goog.proto2.Message#clone} to specify its exact return type.
     * @return {!PhoneMetadata} The cloned message.
     * @override
     */
    PhoneMetadata.prototype.clone;


    /**
     * Gets the value of the general_desc field.
     * @return {PhoneNumberDesc} The value.
     */
    PhoneMetadata.prototype.getGeneralDesc = function () {
        return /** @type {PhoneNumberDesc} */ (this.get$Value(1));
    };


    /**
     * Gets the value of the general_desc field or the default value if not set.
     * @return {!PhoneNumberDesc} The value.
     */
    PhoneMetadata.prototype.getGeneralDescOrDefault = function () {
        return /** @type {!PhoneNumberDesc} */ (this.get$ValueOrDefault(1));
    };


    /**
     * Sets the value of the general_desc field.
     * @param {!PhoneNumberDesc} value The value.
     */
    PhoneMetadata.prototype.setGeneralDesc = function (value) {
        this.set$Value(1, value);
    };


    /**
     * @return {boolean} Whether the general_desc field has a value.
     */
    PhoneMetadata.prototype.hasGeneralDesc = function () {
        return this.has$Value(1);
    };


    /**
     * @return {number} The number of values in the general_desc field.
     */
    PhoneMetadata.prototype.generalDescCount = function () {
        return this.count$Values(1);
    };


    /**
     * Clears the values in the general_desc field.
     */
    PhoneMetadata.prototype.clearGeneralDesc = function () {
        this.clear$Field(1);
    };


    /**
     * Gets the value of the fixed_line field.
     * @return {PhoneNumberDesc} The value.
     */
    PhoneMetadata.prototype.getFixedLine = function () {
        return /** @type {PhoneNumberDesc} */ (this.get$Value(2));
    };


    /**
     * Gets the value of the fixed_line field or the default value if not set.
     * @return {!PhoneNumberDesc} The value.
     */
    PhoneMetadata.prototype.getFixedLineOrDefault = function () {
        return /** @type {!PhoneNumberDesc} */ (this.get$ValueOrDefault(2));
    };


    /**
     * Sets the value of the fixed_line field.
     * @param {!PhoneNumberDesc} value The value.
     */
    PhoneMetadata.prototype.setFixedLine = function (value) {
        this.set$Value(2, value);
    };


    /**
     * @return {boolean} Whether the fixed_line field has a value.
     */
    PhoneMetadata.prototype.hasFixedLine = function () {
        return this.has$Value(2);
    };


    /**
     * @return {number} The number of values in the fixed_line field.
     */
    PhoneMetadata.prototype.fixedLineCount = function () {
        return this.count$Values(2);
    };


    /**
     * Clears the values in the fixed_line field.
     */
    PhoneMetadata.prototype.clearFixedLine = function () {
        this.clear$Field(2);
    };


    /**
     * Gets the value of the mobile field.
     * @return {PhoneNumberDesc} The value.
     */
    PhoneMetadata.prototype.getMobile = function () {
        return /** @type {PhoneNumberDesc} */ (this.get$Value(3));
    };


    /**
     * Gets the value of the mobile field or the default value if not set.
     * @return {!PhoneNumberDesc} The value.
     */
    PhoneMetadata.prototype.getMobileOrDefault = function () {
        return /** @type {!PhoneNumberDesc} */ (this.get$ValueOrDefault(3));
    };


    /**
     * Sets the value of the mobile field.
     * @param {!PhoneNumberDesc} value The value.
     */
    PhoneMetadata.prototype.setMobile = function (value) {
        this.set$Value(3, value);
    };


    /**
     * @return {boolean} Whether the mobile field has a value.
     */
    PhoneMetadata.prototype.hasMobile = function () {
        return this.has$Value(3);
    };


    /**
     * @return {number} The number of values in the mobile field.
     */
    PhoneMetadata.prototype.mobileCount = function () {
        return this.count$Values(3);
    };


    /**
     * Clears the values in the mobile field.
     */
    PhoneMetadata.prototype.clearMobile = function () {
        this.clear$Field(3);
    };


    /**
     * Gets the value of the toll_free field.
     * @return {PhoneNumberDesc} The value.
     */
    PhoneMetadata.prototype.getTollFree = function () {
        return /** @type {PhoneNumberDesc} */ (this.get$Value(4));
    };


    /**
     * Gets the value of the toll_free field or the default value if not set.
     * @return {!PhoneNumberDesc} The value.
     */
    PhoneMetadata.prototype.getTollFreeOrDefault = function () {
        return /** @type {!PhoneNumberDesc} */ (this.get$ValueOrDefault(4));
    };


    /**
     * Sets the value of the toll_free field.
     * @param {!PhoneNumberDesc} value The value.
     */
    PhoneMetadata.prototype.setTollFree = function (value) {
        this.set$Value(4, value);
    };


    /**
     * @return {boolean} Whether the toll_free field has a value.
     */
    PhoneMetadata.prototype.hasTollFree = function () {
        return this.has$Value(4);
    };


    /**
     * @return {number} The number of values in the toll_free field.
     */
    PhoneMetadata.prototype.tollFreeCount = function () {
        return this.count$Values(4);
    };


    /**
     * Clears the values in the toll_free field.
     */
    PhoneMetadata.prototype.clearTollFree = function () {
        this.clear$Field(4);
    };


    /**
     * Gets the value of the premium_rate field.
     * @return {PhoneNumberDesc} The value.
     */
    PhoneMetadata.prototype.getPremiumRate = function () {
        return /** @type {PhoneNumberDesc} */ (this.get$Value(5));
    };


    /**
     * Gets the value of the premium_rate field or the default value if not set.
     * @return {!PhoneNumberDesc} The value.
     */
    PhoneMetadata.prototype.getPremiumRateOrDefault = function () {
        return /** @type {!PhoneNumberDesc} */ (this.get$ValueOrDefault(5));
    };


    /**
     * Sets the value of the premium_rate field.
     * @param {!PhoneNumberDesc} value The value.
     */
    PhoneMetadata.prototype.setPremiumRate = function (value) {
        this.set$Value(5, value);
    };


    /**
     * @return {boolean} Whether the premium_rate field has a value.
     */
    PhoneMetadata.prototype.hasPremiumRate = function () {
        return this.has$Value(5);
    };


    /**
     * @return {number} The number of values in the premium_rate field.
     */
    PhoneMetadata.prototype.premiumRateCount = function () {
        return this.count$Values(5);
    };


    /**
     * Clears the values in the premium_rate field.
     */
    PhoneMetadata.prototype.clearPremiumRate = function () {
        this.clear$Field(5);
    };


    /**
     * Gets the value of the shared_cost field.
     * @return {PhoneNumberDesc} The value.
     */
    PhoneMetadata.prototype.getSharedCost = function () {
        return /** @type {PhoneNumberDesc} */ (this.get$Value(6));
    };


    /**
     * Gets the value of the shared_cost field or the default value if not set.
     * @return {!PhoneNumberDesc} The value.
     */
    PhoneMetadata.prototype.getSharedCostOrDefault = function () {
        return /** @type {!PhoneNumberDesc} */ (this.get$ValueOrDefault(6));
    };


    /**
     * Sets the value of the shared_cost field.
     * @param {!PhoneNumberDesc} value The value.
     */
    PhoneMetadata.prototype.setSharedCost = function (value) {
        this.set$Value(6, value);
    };


    /**
     * @return {boolean} Whether the shared_cost field has a value.
     */
    PhoneMetadata.prototype.hasSharedCost = function () {
        return this.has$Value(6);
    };


    /**
     * @return {number} The number of values in the shared_cost field.
     */
    PhoneMetadata.prototype.sharedCostCount = function () {
        return this.count$Values(6);
    };


    /**
     * Clears the values in the shared_cost field.
     */
    PhoneMetadata.prototype.clearSharedCost = function () {
        this.clear$Field(6);
    };


    /**
     * Gets the value of the personal_number field.
     * @return {PhoneNumberDesc} The value.
     */
    PhoneMetadata.prototype.getPersonalNumber = function () {
        return /** @type {PhoneNumberDesc} */ (this.get$Value(7));
    };


    /**
     * Gets the value of the personal_number field or the default value if not set.
     * @return {!PhoneNumberDesc} The value.
     */
    PhoneMetadata.prototype.getPersonalNumberOrDefault = function () {
        return /** @type {!PhoneNumberDesc} */ (this.get$ValueOrDefault(7));
    };


    /**
     * Sets the value of the personal_number field.
     * @param {!PhoneNumberDesc} value The value.
     */
    PhoneMetadata.prototype.setPersonalNumber = function (value) {
        this.set$Value(7, value);
    };


    /**
     * @return {boolean} Whether the personal_number field has a value.
     */
    PhoneMetadata.prototype.hasPersonalNumber = function () {
        return this.has$Value(7);
    };


    /**
     * @return {number} The number of values in the personal_number field.
     */
    PhoneMetadata.prototype.personalNumberCount = function () {
        return this.count$Values(7);
    };


    /**
     * Clears the values in the personal_number field.
     */
    PhoneMetadata.prototype.clearPersonalNumber = function () {
        this.clear$Field(7);
    };


    /**
     * Gets the value of the voip field.
     * @return {PhoneNumberDesc} The value.
     */
    PhoneMetadata.prototype.getVoip = function () {
        return /** @type {PhoneNumberDesc} */ (this.get$Value(8));
    };


    /**
     * Gets the value of the voip field or the default value if not set.
     * @return {!PhoneNumberDesc} The value.
     */
    PhoneMetadata.prototype.getVoipOrDefault = function () {
        return /** @type {!PhoneNumberDesc} */ (this.get$ValueOrDefault(8));
    };


    /**
     * Sets the value of the voip field.
     * @param {!PhoneNumberDesc} value The value.
     */
    PhoneMetadata.prototype.setVoip = function (value) {
        this.set$Value(8, value);
    };


    /**
     * @return {boolean} Whether the voip field has a value.
     */
    PhoneMetadata.prototype.hasVoip = function () {
        return this.has$Value(8);
    };


    /**
     * @return {number} The number of values in the voip field.
     */
    PhoneMetadata.prototype.voipCount = function () {
        return this.count$Values(8);
    };


    /**
     * Clears the values in the voip field.
     */
    PhoneMetadata.prototype.clearVoip = function () {
        this.clear$Field(8);
    };


    /**
     * Gets the value of the pager field.
     * @return {PhoneNumberDesc} The value.
     */
    PhoneMetadata.prototype.getPager = function () {
        return /** @type {PhoneNumberDesc} */ (this.get$Value(21));
    };


    /**
     * Gets the value of the pager field or the default value if not set.
     * @return {!PhoneNumberDesc} The value.
     */
    PhoneMetadata.prototype.getPagerOrDefault = function () {
        return /** @type {!PhoneNumberDesc} */ (this.get$ValueOrDefault(21));
    };


    /**
     * Sets the value of the pager field.
     * @param {!PhoneNumberDesc} value The value.
     */
    PhoneMetadata.prototype.setPager = function (value) {
        this.set$Value(21, value);
    };


    /**
     * @return {boolean} Whether the pager field has a value.
     */
    PhoneMetadata.prototype.hasPager = function () {
        return this.has$Value(21);
    };


    /**
     * @return {number} The number of values in the pager field.
     */
    PhoneMetadata.prototype.pagerCount = function () {
        return this.count$Values(21);
    };


    /**
     * Clears the values in the pager field.
     */
    PhoneMetadata.prototype.clearPager = function () {
        this.clear$Field(21);
    };


    /**
     * Gets the value of the uan field.
     * @return {PhoneNumberDesc} The value.
     */
    PhoneMetadata.prototype.getUan = function () {
        return /** @type {PhoneNumberDesc} */ (this.get$Value(25));
    };


    /**
     * Gets the value of the uan field or the default value if not set.
     * @return {!PhoneNumberDesc} The value.
     */
    PhoneMetadata.prototype.getUanOrDefault = function () {
        return /** @type {!PhoneNumberDesc} */ (this.get$ValueOrDefault(25));
    };


    /**
     * Sets the value of the uan field.
     * @param {!PhoneNumberDesc} value The value.
     */
    PhoneMetadata.prototype.setUan = function (value) {
        this.set$Value(25, value);
    };


    /**
     * @return {boolean} Whether the uan field has a value.
     */
    PhoneMetadata.prototype.hasUan = function () {
        return this.has$Value(25);
    };


    /**
     * @return {number} The number of values in the uan field.
     */
    PhoneMetadata.prototype.uanCount = function () {
        return this.count$Values(25);
    };


    /**
     * Clears the values in the uan field.
     */
    PhoneMetadata.prototype.clearUan = function () {
        this.clear$Field(25);
    };


    /**
     * Gets the value of the emergency field.
     * @return {PhoneNumberDesc} The value.
     */
    PhoneMetadata.prototype.getEmergency = function () {
        return /** @type {PhoneNumberDesc} */ (this.get$Value(27));
    };


    /**
     * Gets the value of the emergency field or the default value if not set.
     * @return {!PhoneNumberDesc} The value.
     */
    PhoneMetadata.prototype.getEmergencyOrDefault = function () {
        return /** @type {!PhoneNumberDesc} */ (this.get$ValueOrDefault(27));
    };


    /**
     * Sets the value of the emergency field.
     * @param {!PhoneNumberDesc} value The value.
     */
    PhoneMetadata.prototype.setEmergency = function (value) {
        this.set$Value(27, value);
    };


    /**
     * @return {boolean} Whether the emergency field has a value.
     */
    PhoneMetadata.prototype.hasEmergency = function () {
        return this.has$Value(27);
    };


    /**
     * @return {number} The number of values in the emergency field.
     */
    PhoneMetadata.prototype.emergencyCount = function () {
        return this.count$Values(27);
    };


    /**
     * Clears the values in the emergency field.
     */
    PhoneMetadata.prototype.clearEmergency = function () {
        this.clear$Field(27);
    };


    /**
     * Gets the value of the voicemail field.
     * @return {PhoneNumberDesc} The value.
     */
    PhoneMetadata.prototype.getVoicemail = function () {
        return /** @type {PhoneNumberDesc} */ (this.get$Value(28));
    };


    /**
     * Gets the value of the voicemail field or the default value if not set.
     * @return {!PhoneNumberDesc} The value.
     */
    PhoneMetadata.prototype.getVoicemailOrDefault = function () {
        return /** @type {!PhoneNumberDesc} */ (this.get$ValueOrDefault(28));
    };


    /**
     * Sets the value of the voicemail field.
     * @param {!PhoneNumberDesc} value The value.
     */
    PhoneMetadata.prototype.setVoicemail = function (value) {
        this.set$Value(28, value);
    };


    /**
     * @return {boolean} Whether the voicemail field has a value.
     */
    PhoneMetadata.prototype.hasVoicemail = function () {
        return this.has$Value(28);
    };


    /**
     * @return {number} The number of values in the voicemail field.
     */
    PhoneMetadata.prototype.voicemailCount = function () {
        return this.count$Values(28);
    };


    /**
     * Clears the values in the voicemail field.
     */
    PhoneMetadata.prototype.clearVoicemail = function () {
        this.clear$Field(28);
    };


    /**
     * Gets the value of the no_international_dialling field.
     * @return {PhoneNumberDesc} The value.
     */
    PhoneMetadata.prototype.getNoInternationalDialling = function () {
        return /** @type {PhoneNumberDesc} */ (this.get$Value(24));
    };


    /**
     * Gets the value of the no_international_dialling field or the default value if not set.
     * @return {!PhoneNumberDesc} The value.
     */
    PhoneMetadata.prototype.getNoInternationalDiallingOrDefault = function () {
        return /** @type {!PhoneNumberDesc} */ (this.get$ValueOrDefault(24));
    };


    /**
     * Sets the value of the no_international_dialling field.
     * @param {!PhoneNumberDesc} value The value.
     */
    PhoneMetadata.prototype.setNoInternationalDialling = function (value) {
        this.set$Value(24, value);
    };


    /**
     * @return {boolean} Whether the no_international_dialling field has a value.
     */
    PhoneMetadata.prototype.hasNoInternationalDialling = function () {
        return this.has$Value(24);
    };


    /**
     * @return {number} The number of values in the no_international_dialling field.
     */
    PhoneMetadata.prototype.noInternationalDiallingCount = function () {
        return this.count$Values(24);
    };


    /**
     * Clears the values in the no_international_dialling field.
     */
    PhoneMetadata.prototype.clearNoInternationalDialling = function () {
        this.clear$Field(24);
    };


    /**
     * Gets the value of the id field.
     * @return {?string} The value.
     */
    PhoneMetadata.prototype.getId = function () {
        return /** @type {?string} */ (this.get$Value(9));
    };


    /**
     * Gets the value of the id field or the default value if not set.
     * @return {string} The value.
     */
    PhoneMetadata.prototype.getIdOrDefault = function () {
        return /** @type {string} */ (this.get$ValueOrDefault(9));
    };


    /**
     * Sets the value of the id field.
     * @param {string} value The value.
     */
    PhoneMetadata.prototype.setId = function (value) {
        this.set$Value(9, value);
    };


    /**
     * @return {boolean} Whether the id field has a value.
     */
    PhoneMetadata.prototype.hasId = function () {
        return this.has$Value(9);
    };


    /**
     * @return {number} The number of values in the id field.
     */
    PhoneMetadata.prototype.idCount = function () {
        return this.count$Values(9);
    };


    /**
     * Clears the values in the id field.
     */
    PhoneMetadata.prototype.clearId = function () {
        this.clear$Field(9);
    };


    /**
     * Gets the value of the country_code field.
     * @return {?number} The value.
     */
    PhoneMetadata.prototype.getCountryCode = function () {
        return /** @type {?number} */ (this.get$Value(10));
    };


    /**
     * Gets the value of the country_code field or the default value if not set.
     * @return {number} The value.
     */
    PhoneMetadata.prototype.getCountryCodeOrDefault = function () {
        return /** @type {number} */ (this.get$ValueOrDefault(10));
    };


    /**
     * Sets the value of the country_code field.
     * @param {number} value The value.
     */
    PhoneMetadata.prototype.setCountryCode = function (value) {
        this.set$Value(10, value);
    };


    /**
     * @return {boolean} Whether the country_code field has a value.
     */
    PhoneMetadata.prototype.hasCountryCode = function () {
        return this.has$Value(10);
    };


    /**
     * @return {number} The number of values in the country_code field.
     */
    PhoneMetadata.prototype.countryCodeCount = function () {
        return this.count$Values(10);
    };


    /**
     * Clears the values in the country_code field.
     */
    PhoneMetadata.prototype.clearCountryCode = function () {
        this.clear$Field(10);
    };


    /**
     * Gets the value of the international_prefix field.
     * @return {?string} The value.
     */
    PhoneMetadata.prototype.getInternationalPrefix = function () {
        return /** @type {?string} */ (this.get$Value(11));
    };


    /**
     * Gets the value of the international_prefix field or the default value if not set.
     * @return {string} The value.
     */
    PhoneMetadata.prototype.getInternationalPrefixOrDefault = function () {
        return /** @type {string} */ (this.get$ValueOrDefault(11));
    };


    /**
     * Sets the value of the international_prefix field.
     * @param {string} value The value.
     */
    PhoneMetadata.prototype.setInternationalPrefix = function (value) {
        this.set$Value(11, value);
    };


    /**
     * @return {boolean} Whether the international_prefix field has a value.
     */
    PhoneMetadata.prototype.hasInternationalPrefix = function () {
        return this.has$Value(11);
    };


    /**
     * @return {number} The number of values in the international_prefix field.
     */
    PhoneMetadata.prototype.internationalPrefixCount = function () {
        return this.count$Values(11);
    };


    /**
     * Clears the values in the international_prefix field.
     */
    PhoneMetadata.prototype.clearInternationalPrefix = function () {
        this.clear$Field(11);
    };


    /**
     * Gets the value of the preferred_international_prefix field.
     * @return {?string} The value.
     */
    PhoneMetadata.prototype.getPreferredInternationalPrefix = function () {
        return /** @type {?string} */ (this.get$Value(17));
    };


    /**
     * Gets the value of the preferred_international_prefix field or the default value if not set.
     * @return {string} The value.
     */
    PhoneMetadata.prototype.getPreferredInternationalPrefixOrDefault = function () {
        return /** @type {string} */ (this.get$ValueOrDefault(17));
    };


    /**
     * Sets the value of the preferred_international_prefix field.
     * @param {string} value The value.
     */
    PhoneMetadata.prototype.setPreferredInternationalPrefix = function (value) {
        this.set$Value(17, value);
    };


    /**
     * @return {boolean} Whether the preferred_international_prefix field has a value.
     */
    PhoneMetadata.prototype.hasPreferredInternationalPrefix = function () {
        return this.has$Value(17);
    };


    /**
     * @return {number} The number of values in the preferred_international_prefix field.
     */
    PhoneMetadata.prototype.preferredInternationalPrefixCount = function () {
        return this.count$Values(17);
    };


    /**
     * Clears the values in the preferred_international_prefix field.
     */
    PhoneMetadata.prototype.clearPreferredInternationalPrefix = function () {
        this.clear$Field(17);
    };


    /**
     * Gets the value of the national_prefix field.
     * @return {?string} The value.
     */
    PhoneMetadata.prototype.getNationalPrefix = function () {
        return /** @type {?string} */ (this.get$Value(12));
    };


    /**
     * Gets the value of the national_prefix field or the default value if not set.
     * @return {string} The value.
     */
    PhoneMetadata.prototype.getNationalPrefixOrDefault = function () {
        return /** @type {string} */ (this.get$ValueOrDefault(12));
    };


    /**
     * Sets the value of the national_prefix field.
     * @param {string} value The value.
     */
    PhoneMetadata.prototype.setNationalPrefix = function (value) {
        this.set$Value(12, value);
    };


    /**
     * @return {boolean} Whether the national_prefix field has a value.
     */
    PhoneMetadata.prototype.hasNationalPrefix = function () {
        return this.has$Value(12);
    };


    /**
     * @return {number} The number of values in the national_prefix field.
     */
    PhoneMetadata.prototype.nationalPrefixCount = function () {
        return this.count$Values(12);
    };


    /**
     * Clears the values in the national_prefix field.
     */
    PhoneMetadata.prototype.clearNationalPrefix = function () {
        this.clear$Field(12);
    };


    /**
     * Gets the value of the preferred_extn_prefix field.
     * @return {?string} The value.
     */
    PhoneMetadata.prototype.getPreferredExtnPrefix = function () {
        return /** @type {?string} */ (this.get$Value(13));
    };


    /**
     * Gets the value of the preferred_extn_prefix field or the default value if not set.
     * @return {string} The value.
     */
    PhoneMetadata.prototype.getPreferredExtnPrefixOrDefault = function () {
        return /** @type {string} */ (this.get$ValueOrDefault(13));
    };


    /**
     * Sets the value of the preferred_extn_prefix field.
     * @param {string} value The value.
     */
    PhoneMetadata.prototype.setPreferredExtnPrefix = function (value) {
        this.set$Value(13, value);
    };


    /**
     * @return {boolean} Whether the preferred_extn_prefix field has a value.
     */
    PhoneMetadata.prototype.hasPreferredExtnPrefix = function () {
        return this.has$Value(13);
    };


    /**
     * @return {number} The number of values in the preferred_extn_prefix field.
     */
    PhoneMetadata.prototype.preferredExtnPrefixCount = function () {
        return this.count$Values(13);
    };


    /**
     * Clears the values in the preferred_extn_prefix field.
     */
    PhoneMetadata.prototype.clearPreferredExtnPrefix = function () {
        this.clear$Field(13);
    };


    /**
     * Gets the value of the national_prefix_for_parsing field.
     * @return {?string} The value.
     */
    PhoneMetadata.prototype.getNationalPrefixForParsing = function () {
        return /** @type {?string} */ (this.get$Value(15));
    };


    /**
     * Gets the value of the national_prefix_for_parsing field or the default value if not set.
     * @return {string} The value.
     */
    PhoneMetadata.prototype.getNationalPrefixForParsingOrDefault = function () {
        return /** @type {string} */ (this.get$ValueOrDefault(15));
    };


    /**
     * Sets the value of the national_prefix_for_parsing field.
     * @param {string} value The value.
     */
    PhoneMetadata.prototype.setNationalPrefixForParsing = function (value) {
        this.set$Value(15, value);
    };


    /**
     * @return {boolean} Whether the national_prefix_for_parsing field has a value.
     */
    PhoneMetadata.prototype.hasNationalPrefixForParsing = function () {
        return this.has$Value(15);
    };


    /**
     * @return {number} The number of values in the national_prefix_for_parsing field.
     */
    PhoneMetadata.prototype.nationalPrefixForParsingCount = function () {
        return this.count$Values(15);
    };


    /**
     * Clears the values in the national_prefix_for_parsing field.
     */
    PhoneMetadata.prototype.clearNationalPrefixForParsing = function () {
        this.clear$Field(15);
    };


    /**
     * Gets the value of the national_prefix_transform_rule field.
     * @return {?string} The value.
     */
    PhoneMetadata.prototype.getNationalPrefixTransformRule = function () {
        return /** @type {?string} */ (this.get$Value(16));
    };


    /**
     * Gets the value of the national_prefix_transform_rule field or the default value if not set.
     * @return {string} The value.
     */
    PhoneMetadata.prototype.getNationalPrefixTransformRuleOrDefault = function () {
        return /** @type {string} */ (this.get$ValueOrDefault(16));
    };


    /**
     * Sets the value of the national_prefix_transform_rule field.
     * @param {string} value The value.
     */
    PhoneMetadata.prototype.setNationalPrefixTransformRule = function (value) {
        this.set$Value(16, value);
    };


    /**
     * @return {boolean} Whether the national_prefix_transform_rule field has a value.
     */
    PhoneMetadata.prototype.hasNationalPrefixTransformRule = function () {
        return this.has$Value(16);
    };


    /**
     * @return {number} The number of values in the national_prefix_transform_rule field.
     */
    PhoneMetadata.prototype.nationalPrefixTransformRuleCount = function () {
        return this.count$Values(16);
    };


    /**
     * Clears the values in the national_prefix_transform_rule field.
     */
    PhoneMetadata.prototype.clearNationalPrefixTransformRule = function () {
        this.clear$Field(16);
    };


    /**
     * Gets the value of the same_mobile_and_fixed_line_pattern field.
     * @return {?boolean} The value.
     */
    PhoneMetadata.prototype.getSameMobileAndFixedLinePattern = function () {
        return /** @type {?boolean} */ (this.get$Value(18));
    };


    /**
     * Gets the value of the same_mobile_and_fixed_line_pattern field or the default value if not set.
     * @return {boolean} The value.
     */
    PhoneMetadata.prototype.getSameMobileAndFixedLinePatternOrDefault = function () {
        return /** @type {boolean} */ (this.get$ValueOrDefault(18));
    };


    /**
     * Sets the value of the same_mobile_and_fixed_line_pattern field.
     * @param {boolean} value The value.
     */
    PhoneMetadata.prototype.setSameMobileAndFixedLinePattern = function (value) {
        this.set$Value(18, value);
    };


    /**
     * @return {boolean} Whether the same_mobile_and_fixed_line_pattern field has a value.
     */
    PhoneMetadata.prototype.hasSameMobileAndFixedLinePattern = function () {
        return this.has$Value(18);
    };


    /**
     * @return {number} The number of values in the same_mobile_and_fixed_line_pattern field.
     */
    PhoneMetadata.prototype.sameMobileAndFixedLinePatternCount = function () {
        return this.count$Values(18);
    };


    /**
     * Clears the values in the same_mobile_and_fixed_line_pattern field.
     */
    PhoneMetadata.prototype.clearSameMobileAndFixedLinePattern = function () {
        this.clear$Field(18);
    };


    /**
     * Gets the value of the number_format field at the index given.
     * @param {number} index The index to lookup.
     * @return {NumberFormat} The value.
     */
    PhoneMetadata.prototype.getNumberFormat = function (index) {
        return /** @type {NumberFormat} */ (this.get$Value(19, index));
    };


    /**
     * Gets the value of the number_format field at the index given or the default value if not set.
     * @param {number} index The index to lookup.
     * @return {!NumberFormat} The value.
     */
    PhoneMetadata.prototype.getNumberFormatOrDefault = function (index) {
        return /** @type {!NumberFormat} */ (this.get$ValueOrDefault(19, index));
    };


    /**
     * Adds a value to the number_format field.
     * @param {!NumberFormat} value The value to add.
     */
    PhoneMetadata.prototype.addNumberFormat = function (value) {
        this.add$Value(19, value);
    };


    /**
     * Returns the array of values in the number_format field.
     * @return {!Array.<!NumberFormat>} The values in the field.
     */
    PhoneMetadata.prototype.numberFormatArray = function () {
        return /** @type {!Array.<!NumberFormat>} */ (this.array$Values(19));
    };


    /**
     * @return {boolean} Whether the number_format field has a value.
     */
    PhoneMetadata.prototype.hasNumberFormat = function () {
        return this.has$Value(19);
    };


    /**
     * @return {number} The number of values in the number_format field.
     */
    PhoneMetadata.prototype.numberFormatCount = function () {
        return this.count$Values(19);
    };


    /**
     * Clears the values in the number_format field.
     */
    PhoneMetadata.prototype.clearNumberFormat = function () {
        this.clear$Field(19);
    };


    /**
     * Gets the value of the intl_number_format field at the index given.
     * @param {number} index The index to lookup.
     * @return {NumberFormat} The value.
     */
    PhoneMetadata.prototype.getIntlNumberFormat = function (index) {
        return /** @type {NumberFormat} */ (this.get$Value(20, index));
    };


    /**
     * Gets the value of the intl_number_format field at the index given or the default value if not set.
     * @param {number} index The index to lookup.
     * @return {!NumberFormat} The value.
     */
    PhoneMetadata.prototype.getIntlNumberFormatOrDefault = function (index) {
        return /** @type {!NumberFormat} */ (this.get$ValueOrDefault(20, index));
    };


    /**
     * Adds a value to the intl_number_format field.
     * @param {!NumberFormat} value The value to add.
     */
    PhoneMetadata.prototype.addIntlNumberFormat = function (value) {
        this.add$Value(20, value);
    };


    /**
     * Returns the array of values in the intl_number_format field.
     * @return {!Array.<!NumberFormat>} The values in the field.
     */
    PhoneMetadata.prototype.intlNumberFormatArray = function () {
        return /** @type {!Array.<!NumberFormat>} */ (this.array$Values(20));
    };


    /**
     * @return {boolean} Whether the intl_number_format field has a value.
     */
    PhoneMetadata.prototype.hasIntlNumberFormat = function () {
        return this.has$Value(20);
    };


    /**
     * @return {number} The number of values in the intl_number_format field.
     */
    PhoneMetadata.prototype.intlNumberFormatCount = function () {
        return this.count$Values(20);
    };


    /**
     * Clears the values in the intl_number_format field.
     */
    PhoneMetadata.prototype.clearIntlNumberFormat = function () {
        this.clear$Field(20);
    };


    /**
     * Gets the value of the main_country_for_code field.
     * @return {?boolean} The value.
     */
    PhoneMetadata.prototype.getMainCountryForCode = function () {
        return /** @type {?boolean} */ (this.get$Value(22));
    };


    /**
     * Gets the value of the main_country_for_code field or the default value if not set.
     * @return {boolean} The value.
     */
    PhoneMetadata.prototype.getMainCountryForCodeOrDefault = function () {
        return /** @type {boolean} */ (this.get$ValueOrDefault(22));
    };


    /**
     * Sets the value of the main_country_for_code field.
     * @param {boolean} value The value.
     */
    PhoneMetadata.prototype.setMainCountryForCode = function (value) {
        this.set$Value(22, value);
    };


    /**
     * @return {boolean} Whether the main_country_for_code field has a value.
     */
    PhoneMetadata.prototype.hasMainCountryForCode = function () {
        return this.has$Value(22);
    };


    /**
     * @return {number} The number of values in the main_country_for_code field.
     */
    PhoneMetadata.prototype.mainCountryForCodeCount = function () {
        return this.count$Values(22);
    };


    /**
     * Clears the values in the main_country_for_code field.
     */
    PhoneMetadata.prototype.clearMainCountryForCode = function () {
        this.clear$Field(22);
    };


    /**
     * Gets the value of the leading_digits field.
     * @return {?string} The value.
     */
    PhoneMetadata.prototype.getLeadingDigits = function () {
        return /** @type {?string} */ (this.get$Value(23));
    };


    /**
     * Gets the value of the leading_digits field or the default value if not set.
     * @return {string} The value.
     */
    PhoneMetadata.prototype.getLeadingDigitsOrDefault = function () {
        return /** @type {string} */ (this.get$ValueOrDefault(23));
    };


    /**
     * Sets the value of the leading_digits field.
     * @param {string} value The value.
     */
    PhoneMetadata.prototype.setLeadingDigits = function (value) {
        this.set$Value(23, value);
    };


    /**
     * @return {boolean} Whether the leading_digits field has a value.
     */
    PhoneMetadata.prototype.hasLeadingDigits = function () {
        return this.has$Value(23);
    };


    /**
     * @return {number} The number of values in the leading_digits field.
     */
    PhoneMetadata.prototype.leadingDigitsCount = function () {
        return this.count$Values(23);
    };


    /**
     * Clears the values in the leading_digits field.
     */
    PhoneMetadata.prototype.clearLeadingDigits = function () {
        this.clear$Field(23);
    };


    /**
     * Gets the value of the leading_zero_possible field.
     * @return {?boolean} The value.
     */
    PhoneMetadata.prototype.getLeadingZeroPossible = function () {
        return /** @type {?boolean} */ (this.get$Value(26));
    };


    /**
     * Gets the value of the leading_zero_possible field or the default value if not set.
     * @return {boolean} The value.
     */
    PhoneMetadata.prototype.getLeadingZeroPossibleOrDefault = function () {
        return /** @type {boolean} */ (this.get$ValueOrDefault(26));
    };


    /**
     * Sets the value of the leading_zero_possible field.
     * @param {boolean} value The value.
     */
    PhoneMetadata.prototype.setLeadingZeroPossible = function (value) {
        this.set$Value(26, value);
    };


    /**
     * @return {boolean} Whether the leading_zero_possible field has a value.
     */
    PhoneMetadata.prototype.hasLeadingZeroPossible = function () {
        return this.has$Value(26);
    };


    /**
     * @return {number} The number of values in the leading_zero_possible field.
     */
    PhoneMetadata.prototype.leadingZeroPossibleCount = function () {
        return this.count$Values(26);
    };


    /**
     * Clears the values in the leading_zero_possible field.
     */
    PhoneMetadata.prototype.clearLeadingZeroPossible = function () {
        this.clear$Field(26);
    };
});


/** @override */
i18n.phonenumbers.PhoneMetadata.prototype.getDescriptor = function () {
    if (!i18n.phonenumbers.PhoneMetadata.descriptor_) {
        // The descriptor is created lazily when we instantiate a new instance.
        var descriptorObj = {
            0: {
                name: 'PhoneMetadata',
                fullName: 'i18n.phonenumbers.PhoneMetadata'
            },
            1: {
                name: 'general_desc',
                fieldType: goog.proto2.Message.FieldType.MESSAGE,
                type: i18n.phonenumbers.PhoneNumberDesc
            },
            2: {
                name: 'fixed_line',
                fieldType: goog.proto2.Message.FieldType.MESSAGE,
                type: i18n.phonenumbers.PhoneNumberDesc
            },
            3: {
                name: 'mobile',
                fieldType: goog.proto2.Message.FieldType.MESSAGE,
                type: i18n.phonenumbers.PhoneNumberDesc
            },
            4: {
                name: 'toll_free',
                fieldType: goog.proto2.Message.FieldType.MESSAGE,
                type: i18n.phonenumbers.PhoneNumberDesc
            },
            5: {
                name: 'premium_rate',
                fieldType: goog.proto2.Message.FieldType.MESSAGE,
                type: i18n.phonenumbers.PhoneNumberDesc
            },
            6: {
                name: 'shared_cost',
                fieldType: goog.proto2.Message.FieldType.MESSAGE,
                type: i18n.phonenumbers.PhoneNumberDesc
            },
            7: {
                name: 'personal_number',
                fieldType: goog.proto2.Message.FieldType.MESSAGE,
                type: i18n.phonenumbers.PhoneNumberDesc
            },
            8: {
                name: 'voip',
                fieldType: goog.proto2.Message.FieldType.MESSAGE,
                type: i18n.phonenumbers.PhoneNumberDesc
            },
            21: {
                name: 'pager',
                fieldType: goog.proto2.Message.FieldType.MESSAGE,
                type: i18n.phonenumbers.PhoneNumberDesc
            },
            25: {
                name: 'uan',
                fieldType: goog.proto2.Message.FieldType.MESSAGE,
                type: i18n.phonenumbers.PhoneNumberDesc
            },
            27: {
                name: 'emergency',
                fieldType: goog.proto2.Message.FieldType.MESSAGE,
                type: i18n.phonenumbers.PhoneNumberDesc
            },
            28: {
                name: 'voicemail',
                fieldType: goog.proto2.Message.FieldType.MESSAGE,
                type: i18n.phonenumbers.PhoneNumberDesc
            },
            24: {
                name: 'no_international_dialling',
                fieldType: goog.proto2.Message.FieldType.MESSAGE,
                type: i18n.phonenumbers.PhoneNumberDesc
            },
            9: {
                name: 'id',
                required: true,
                fieldType: goog.proto2.Message.FieldType.STRING,
                type: String
            },
            10: {
                name: 'country_code',
                fieldType: goog.proto2.Message.FieldType.INT32,
                type: Number
            },
            11: {
                name: 'international_prefix',
                fieldType: goog.proto2.Message.FieldType.STRING,
                type: String
            },
            17: {
                name: 'preferred_international_prefix',
                fieldType: goog.proto2.Message.FieldType.STRING,
                type: String
            },
            12: {
                name: 'national_prefix',
                fieldType: goog.proto2.Message.FieldType.STRING,
                type: String
            },
            13: {
                name: 'preferred_extn_prefix',
                fieldType: goog.proto2.Message.FieldType.STRING,
                type: String
            },
            15: {
                name: 'national_prefix_for_parsing',
                fieldType: goog.proto2.Message.FieldType.STRING,
                type: String
            },
            16: {
                name: 'national_prefix_transform_rule',
                fieldType: goog.proto2.Message.FieldType.STRING,
                type: String
            },
            18: {
                name: 'same_mobile_and_fixed_line_pattern',
                fieldType: goog.proto2.Message.FieldType.BOOL,
                defaultValue: false,
                type: Boolean
            },
            19: {
                name: 'number_format',
                repeated: true,
                fieldType: goog.proto2.Message.FieldType.MESSAGE,
                type: i18n.phonenumbers.NumberFormat
            },
            20: {
                name: 'intl_number_format',
                repeated: true,
                fieldType: goog.proto2.Message.FieldType.MESSAGE,
                type: i18n.phonenumbers.NumberFormat
            },
            22: {
                name: 'main_country_for_code',
                fieldType: goog.proto2.Message.FieldType.BOOL,
                defaultValue: false,
                type: Boolean
            },
            23: {
                name: 'leading_digits',
                fieldType: goog.proto2.Message.FieldType.STRING,
                type: String
            },
            26: {
                name: 'leading_zero_possible',
                fieldType: goog.proto2.Message.FieldType.BOOL,
                defaultValue: false,
                type: Boolean
            }
        };
        i18n.phonenumbers.PhoneMetadata.descriptor_ =
            goog.proto2.Message.createDescriptor(
                 i18n.phonenumbers.PhoneMetadata, descriptorObj);
    }
    return i18n.phonenumbers.PhoneMetadata.descriptor_;
};


// Export getDescriptor static function robust to minification.
i18n.phonenumbers.PhoneMetadata['ctor'] = i18n.phonenumbers.PhoneMetadata;
i18n.phonenumbers.PhoneMetadata['ctor'].getDescriptor =
    i18n.phonenumbers.PhoneMetadata.prototype.getDescriptor;