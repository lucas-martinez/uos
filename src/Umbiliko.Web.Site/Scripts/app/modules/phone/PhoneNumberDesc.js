﻿




/**
 * Message PhoneNumberDesc.
 * @constructor
 * @extends {goog.proto2.Message}
 */
PhoneNumberDesc = function () {
    goog.proto2.Message.call(this);
};
goog.inherits(PhoneNumberDesc, goog.proto2.Message);


/**
 * Overrides {@link goog.proto2.Message#clone} to specify its exact return type.
 * @return {!PhoneNumberDesc} The cloned message.
 * @override
 */
PhoneNumberDesc.prototype.clone;


/**
 * Gets the value of the national_number_pattern field.
 * @return {?string} The value.
 */
PhoneNumberDesc.prototype.getNationalNumberPattern = function () {
    return /** @type {?string} */ (this.get$Value(2));
};


/**
 * Gets the value of the national_number_pattern field or the default value if not set.
 * @return {string} The value.
 */
PhoneNumberDesc.prototype.getNationalNumberPatternOrDefault = function () {
    return /** @type {string} */ (this.get$ValueOrDefault(2));
};


/**
 * Sets the value of the national_number_pattern field.
 * @param {string} value The value.
 */
PhoneNumberDesc.prototype.setNationalNumberPattern = function (value) {
    this.set$Value(2, value);
};


/**
 * @return {boolean} Whether the national_number_pattern field has a value.
 */
PhoneNumberDesc.prototype.hasNationalNumberPattern = function () {
    return this.has$Value(2);
};


/**
 * @return {number} The number of values in the national_number_pattern field.
 */
PhoneNumberDesc.prototype.nationalNumberPatternCount = function () {
    return this.count$Values(2);
};


/**
 * Clears the values in the national_number_pattern field.
 */
PhoneNumberDesc.prototype.clearNationalNumberPattern = function () {
    this.clear$Field(2);
};


/**
 * Gets the value of the possible_number_pattern field.
 * @return {?string} The value.
 */
PhoneNumberDesc.prototype.getPossibleNumberPattern = function () {
    return /** @type {?string} */ (this.get$Value(3));
};


/**
 * Gets the value of the possible_number_pattern field or the default value if not set.
 * @return {string} The value.
 */
PhoneNumberDesc.prototype.getPossibleNumberPatternOrDefault = function () {
    return /** @type {string} */ (this.get$ValueOrDefault(3));
};


/**
 * Sets the value of the possible_number_pattern field.
 * @param {string} value The value.
 */
PhoneNumberDesc.prototype.setPossibleNumberPattern = function (value) {
    this.set$Value(3, value);
};


/**
 * @return {boolean} Whether the possible_number_pattern field has a value.
 */
PhoneNumberDesc.prototype.hasPossibleNumberPattern = function () {
    return this.has$Value(3);
};


/**
 * @return {number} The number of values in the possible_number_pattern field.
 */
PhoneNumberDesc.prototype.possibleNumberPatternCount = function () {
    return this.count$Values(3);
};


/**
 * Clears the values in the possible_number_pattern field.
 */
PhoneNumberDesc.prototype.clearPossibleNumberPattern = function () {
    this.clear$Field(3);
};


/**
 * Gets the value of the example_number field.
 * @return {?string} The value.
 */
PhoneNumberDesc.prototype.getExampleNumber = function () {
    return /** @type {?string} */ (this.get$Value(6));
};


/**
 * Gets the value of the example_number field or the default value if not set.
 * @return {string} The value.
 */
PhoneNumberDesc.prototype.getExampleNumberOrDefault = function () {
    return /** @type {string} */ (this.get$ValueOrDefault(6));
};


/**
 * Sets the value of the example_number field.
 * @param {string} value The value.
 */
PhoneNumberDesc.prototype.setExampleNumber = function (value) {
    this.set$Value(6, value);
};


/**
 * @return {boolean} Whether the example_number field has a value.
 */
PhoneNumberDesc.prototype.hasExampleNumber = function () {
    return this.has$Value(6);
};


/**
 * @return {number} The number of values in the example_number field.
 */
PhoneNumberDesc.prototype.exampleNumberCount = function () {
    return this.count$Values(6);
};


/**
 * Clears the values in the example_number field.
 */
PhoneNumberDesc.prototype.clearExampleNumber = function () {
    this.clear$Field(6);
};


/**
 * Gets the value of the national_number_matcher_data field.
 * @return {?string} The value.
 */
PhoneNumberDesc.prototype.getNationalNumberMatcherData = function () {
    return /** @type {?string} */ (this.get$Value(7));
};


/**
 * Gets the value of the national_number_matcher_data field or the default value if not set.
 * @return {string} The value.
 */
PhoneNumberDesc.prototype.getNationalNumberMatcherDataOrDefault = function () {
    return /** @type {string} */ (this.get$ValueOrDefault(7));
};


/**
 * Sets the value of the national_number_matcher_data field.
 * @param {string} value The value.
 */
PhoneNumberDesc.prototype.setNationalNumberMatcherData = function (value) {
    this.set$Value(7, value);
};


/**
 * @return {boolean} Whether the national_number_matcher_data field has a value.
 */
PhoneNumberDesc.prototype.hasNationalNumberMatcherData = function () {
    return this.has$Value(7);
};


/**
 * @return {number} The number of values in the national_number_matcher_data field.
 */
PhoneNumberDesc.prototype.nationalNumberMatcherDataCount = function () {
    return this.count$Values(7);
};


/**
 * Clears the values in the national_number_matcher_data field.
 */
PhoneNumberDesc.prototype.clearNationalNumberMatcherData = function () {
    this.clear$Field(7);
};


/**
 * Gets the value of the possible_number_matcher_data field.
 * @return {?string} The value.
 */
PhoneNumberDesc.prototype.getPossibleNumberMatcherData = function () {
    return /** @type {?string} */ (this.get$Value(8));
};


/**
 * Gets the value of the possible_number_matcher_data field or the default value if not set.
 * @return {string} The value.
 */
PhoneNumberDesc.prototype.getPossibleNumberMatcherDataOrDefault = function () {
    return /** @type {string} */ (this.get$ValueOrDefault(8));
};


/**
 * Sets the value of the possible_number_matcher_data field.
 * @param {string} value The value.
 */
PhoneNumberDesc.prototype.setPossibleNumberMatcherData = function (value) {
    this.set$Value(8, value);
};


/**
 * @return {boolean} Whether the possible_number_matcher_data field has a value.
 */
PhoneNumberDesc.prototype.hasPossibleNumberMatcherData = function () {
    return this.has$Value(8);
};


/**
 * @return {number} The number of values in the possible_number_matcher_data field.
 */
PhoneNumberDesc.prototype.possibleNumberMatcherDataCount = function () {
    return this.count$Values(8);
};


/**
 * Clears the values in the possible_number_matcher_data field.
 */
PhoneNumberDesc.prototype.clearPossibleNumberMatcherData = function () {
    this.clear$Field(8);
};

/** @override */
PhoneNumberDesc.prototype.getDescriptor = function () {
    if (!PhoneNumberDesc.descriptor_) {
        // The descriptor is created lazily when we instantiate a new instance.
        var descriptorObj = {
            0: {
                name: 'PhoneNumberDesc',
                fullName: 'PhoneNumberDesc'
            },
            2: {
                name: 'national_number_pattern',
                fieldType: goog.proto2.Message.FieldType.STRING,
                type: String
            },
            3: {
                name: 'possible_number_pattern',
                fieldType: goog.proto2.Message.FieldType.STRING,
                type: String
            },
            6: {
                name: 'example_number',
                fieldType: goog.proto2.Message.FieldType.STRING,
                type: String
            },
            7: {
                name: 'national_number_matcher_data',
                fieldType: goog.proto2.Message.FieldType.BYTES,
                type: String
            },
            8: {
                name: 'possible_number_matcher_data',
                fieldType: goog.proto2.Message.FieldType.BYTES,
                type: String
            }
        };
        PhoneNumberDesc.descriptor_ =
            goog.proto2.Message.createDescriptor(
                 PhoneNumberDesc, descriptorObj);
    }
    return PhoneNumberDesc.descriptor_;
};


// Export getDescriptor static function robust to minification.
PhoneNumberDesc['ctor'] = PhoneNumberDesc;
PhoneNumberDesc['ctor'].getDescriptor =
    PhoneNumberDesc.prototype.getDescriptor;


