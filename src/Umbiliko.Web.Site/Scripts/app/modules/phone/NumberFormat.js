﻿/**
 * @license
 * Protocol Buffer 2 Copyright 2008 Google Inc.
 * All other code copyright its respective owners.
 * Copyright (C) 2010 The Libphonenumber Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Message NumberFormat.
 * @constructor
 * @extends {goog.proto2.Message}
 */
function NumberFormat() {
    //goog.proto2.Message.call(this);
};
//goog.inherits(NumberFormat, goog.proto2.Message);


/**
 * Overrides {@link goog.proto2.Message#clone} to specify its exact return type.
 * @return {!NumberFormat} The cloned message.
 * @override
 */
NumberFormat.prototype.clone;


/**
 * Gets the value of the pattern field.
 * @return {?string} The value.
 */
NumberFormat.prototype.getPattern = function () {
    return /** @type {?string} */ (this.get$Value(1));
};


/**
 * Gets the value of the pattern field or the default value if not set.
 * @return {string} The value.
 */
NumberFormat.prototype.getPatternOrDefault = function () {
    return /** @type {string} */ (this.get$ValueOrDefault(1));
};


/**
 * Sets the value of the pattern field.
 * @param {string} value The value.
 */
NumberFormat.prototype.setPattern = function (value) {
    this.set$Value(1, value);
};


/**
 * @return {boolean} Whether the pattern field has a value.
 */
NumberFormat.prototype.hasPattern = function () {
    return this.has$Value(1);
};


/**
 * @return {number} The number of values in the pattern field.
 */
NumberFormat.prototype.patternCount = function () {
    return this.count$Values(1);
};


/**
 * Clears the values in the pattern field.
 */
NumberFormat.prototype.clearPattern = function () {
    this.clear$Field(1);
};


/**
 * Gets the value of the format field.
 * @return {?string} The value.
 */
NumberFormat.prototype.getFormat = function () {
    return /** @type {?string} */ (this.get$Value(2));
};


/**
 * Gets the value of the format field or the default value if not set.
 * @return {string} The value.
 */
NumberFormat.prototype.getFormatOrDefault = function () {
    return /** @type {string} */ (this.get$ValueOrDefault(2));
};


/**
 * Sets the value of the format field.
 * @param {string} value The value.
 */
NumberFormat.prototype.setFormat = function (value) {
    this.set$Value(2, value);
};


/**
 * @return {boolean} Whether the format field has a value.
 */
NumberFormat.prototype.hasFormat = function () {
    return this.has$Value(2);
};


/**
 * @return {number} The number of values in the format field.
 */
NumberFormat.prototype.formatCount = function () {
    return this.count$Values(2);
};


/**
 * Clears the values in the format field.
 */
NumberFormat.prototype.clearFormat = function () {
    this.clear$Field(2);
};


/**
 * Gets the value of the leading_digits_pattern field at the index given.
 * @param {number} index The index to lookup.
 * @return {?string} The value.
 */
NumberFormat.prototype.getLeadingDigitsPattern = function (index) {
    return /** @type {?string} */ (this.get$Value(3, index));
};


/**
 * Gets the value of the leading_digits_pattern field at the index given or the default value if not set.
 * @param {number} index The index to lookup.
 * @return {string} The value.
 */
NumberFormat.prototype.getLeadingDigitsPatternOrDefault = function (index) {
    return /** @type {string} */ (this.get$ValueOrDefault(3, index));
};


/**
 * Adds a value to the leading_digits_pattern field.
 * @param {string} value The value to add.
 */
NumberFormat.prototype.addLeadingDigitsPattern = function (value) {
    this.add$Value(3, value);
};


/**
 * Returns the array of values in the leading_digits_pattern field.
 * @return {!Array.<string>} The values in the field.
 */
NumberFormat.prototype.leadingDigitsPatternArray = function () {
    return /** @type {!Array.<string>} */ (this.array$Values(3));
};


/**
 * @return {boolean} Whether the leading_digits_pattern field has a value.
 */
NumberFormat.prototype.hasLeadingDigitsPattern = function () {
    return this.has$Value(3);
};


/**
 * @return {number} The number of values in the leading_digits_pattern field.
 */
NumberFormat.prototype.leadingDigitsPatternCount = function () {
    return this.count$Values(3);
};


/**
 * Clears the values in the leading_digits_pattern field.
 */
NumberFormat.prototype.clearLeadingDigitsPattern = function () {
    this.clear$Field(3);
};


/**
 * Gets the value of the national_prefix_formatting_rule field.
 * @return {?string} The value.
 */
NumberFormat.prototype.getNationalPrefixFormattingRule = function () {
    return /** @type {?string} */ (this.get$Value(4));
};


/**
 * Gets the value of the national_prefix_formatting_rule field or the default value if not set.
 * @return {string} The value.
 */
NumberFormat.prototype.getNationalPrefixFormattingRuleOrDefault = function () {
    return /** @type {string} */ (this.get$ValueOrDefault(4));
};


/**
 * Sets the value of the national_prefix_formatting_rule field.
 * @param {string} value The value.
 */
NumberFormat.prototype.setNationalPrefixFormattingRule = function (value) {
    this.set$Value(4, value);
};


/**
 * @return {boolean} Whether the national_prefix_formatting_rule field has a value.
 */
NumberFormat.prototype.hasNationalPrefixFormattingRule = function () {
    return this.has$Value(4);
};


/**
 * @return {number} The number of values in the national_prefix_formatting_rule field.
 */
NumberFormat.prototype.nationalPrefixFormattingRuleCount = function () {
    return this.count$Values(4);
};


/**
 * Clears the values in the national_prefix_formatting_rule field.
 */
NumberFormat.prototype.clearNationalPrefixFormattingRule = function () {
    this.clear$Field(4);
};


/**
 * Gets the value of the national_prefix_optional_when_formatting field.
 * @return {?boolean} The value.
 */
NumberFormat.prototype.getNationalPrefixOptionalWhenFormatting = function () {
    return /** @type {?boolean} */ (this.get$Value(6));
};


/**
 * Gets the value of the national_prefix_optional_when_formatting field or the default value if not set.
 * @return {boolean} The value.
 */
NumberFormat.prototype.getNationalPrefixOptionalWhenFormattingOrDefault = function () {
    return /** @type {boolean} */ (this.get$ValueOrDefault(6));
};


/**
 * Sets the value of the national_prefix_optional_when_formatting field.
 * @param {boolean} value The value.
 */
NumberFormat.prototype.setNationalPrefixOptionalWhenFormatting = function (value) {
    this.set$Value(6, value);
};


/**
 * @return {boolean} Whether the national_prefix_optional_when_formatting field has a value.
 */
NumberFormat.prototype.hasNationalPrefixOptionalWhenFormatting = function () {
    return this.has$Value(6);
};


/**
 * @return {number} The number of values in the national_prefix_optional_when_formatting field.
 */
NumberFormat.prototype.nationalPrefixOptionalWhenFormattingCount = function () {
    return this.count$Values(6);
};


/**
 * Clears the values in the national_prefix_optional_when_formatting field.
 */
NumberFormat.prototype.clearNationalPrefixOptionalWhenFormatting = function () {
    this.clear$Field(6);
};


/**
 * Gets the value of the domestic_carrier_code_formatting_rule field.
 * @return {?string} The value.
 */
NumberFormat.prototype.getDomesticCarrierCodeFormattingRule = function () {
    return /** @type {?string} */ (this.get$Value(5));
};


/**
 * Gets the value of the domestic_carrier_code_formatting_rule field or the default value if not set.
 * @return {string} The value.
 */
NumberFormat.prototype.getDomesticCarrierCodeFormattingRuleOrDefault = function () {
    return /** @type {string} */ (this.get$ValueOrDefault(5));
};


/**
 * Sets the value of the domestic_carrier_code_formatting_rule field.
 * @param {string} value The value.
 */
NumberFormat.prototype.setDomesticCarrierCodeFormattingRule = function (value) {
    this.set$Value(5, value);
};


/**
 * @return {boolean} Whether the domestic_carrier_code_formatting_rule field has a value.
 */
NumberFormat.prototype.hasDomesticCarrierCodeFormattingRule = function () {
    return this.has$Value(5);
};


/**
 * @return {number} The number of values in the domestic_carrier_code_formatting_rule field.
 */
NumberFormat.prototype.domesticCarrierCodeFormattingRuleCount = function () {
    return this.count$Values(5);
};


/**
 * Clears the values in the domestic_carrier_code_formatting_rule field.
 */
NumberFormat.prototype.clearDomesticCarrierCodeFormattingRule = function () {
    this.clear$Field(5);
};

/** @override */
NumberFormat.prototype.getDescriptor = function () {
    if (!NumberFormat.descriptor_) {
        // The descriptor is created lazily when we instantiate a new instance.
        var descriptorObj = {
            0: {
                name: 'NumberFormat',
                fullName: 'NumberFormat'
            },
            1: {
                name: 'pattern',
                required: true,
                fieldType: goog.proto2.Message.FieldType.STRING,
                type: String
            },
            2: {
                name: 'format',
                required: true,
                fieldType: goog.proto2.Message.FieldType.STRING,
                type: String
            },
            3: {
                name: 'leading_digits_pattern',
                repeated: true,
                fieldType: goog.proto2.Message.FieldType.STRING,
                type: String
            },
            4: {
                name: 'national_prefix_formatting_rule',
                fieldType: goog.proto2.Message.FieldType.STRING,
                type: String
            },
            6: {
                name: 'national_prefix_optional_when_formatting',
                fieldType: goog.proto2.Message.FieldType.BOOL,
                type: Boolean
            },
            5: {
                name: 'domestic_carrier_code_formatting_rule',
                fieldType: goog.proto2.Message.FieldType.STRING,
                type: String
            }
        };
        NumberFormat.descriptor_ =
            goog.proto2.Message.createDescriptor(
                 NumberFormat, descriptorObj);
    }
    return NumberFormat.descriptor_;
};


// Export getDescriptor static function robust to minification.
NumberFormat['ctor'] = NumberFormat;
NumberFormat['ctor'].getDescriptor =
    NumberFormat.prototype.getDescriptor;