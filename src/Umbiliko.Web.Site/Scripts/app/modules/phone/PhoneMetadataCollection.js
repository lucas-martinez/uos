﻿
/**
 * Message PhoneMetadataCollection.
 * @constructor
 * @extends {goog.proto2.Message}
 */
PhoneMetadataCollection = function() {
  //goog.proto2.Message.call(this);
};
//goog.inherits(PhoneMetadataCollection, goog.proto2.Message);


/**
 * Overrides {@link goog.proto2.Message#clone} to specify its exact return type.
 * @return {!PhoneMetadataCollection} The cloned message.
 * @override
 */
PhoneMetadataCollection.prototype.clone;


/**
 * Gets the value of the metadata field at the index given.
 * @param {number} index The index to lookup.
 * @return {PhoneMetadata} The value.
 */
PhoneMetadataCollection.prototype.getMetadata = function(index) {
  return /** @type {PhoneMetadata} */ (this.get$Value(1, index));
};


/**
 * Gets the value of the metadata field at the index given or the default value if not set.
 * @param {number} index The index to lookup.
 * @return {!PhoneMetadata} The value.
 */
PhoneMetadataCollection.prototype.getMetadataOrDefault = function(index) {
  return /** @type {!PhoneMetadata} */ (this.get$ValueOrDefault(1, index));
};


/**
 * Adds a value to the metadata field.
 * @param {!PhoneMetadata} value The value to add.
 */
PhoneMetadataCollection.prototype.addMetadata = function(value) {
  this.add$Value(1, value);
};


/**
 * Returns the array of values in the metadata field.
 * @return {!Array.<!PhoneMetadata>} The values in the field.
 */
PhoneMetadataCollection.prototype.metadataArray = function() {
  return /** @type {!Array.<!PhoneMetadata>} */ (this.array$Values(1));
};


/**
 * @return {boolean} Whether the metadata field has a value.
 */
PhoneMetadataCollection.prototype.hasMetadata = function() {
  return this.has$Value(1);
};


/**
 * @return {number} The number of values in the metadata field.
 */
PhoneMetadataCollection.prototype.metadataCount = function() {
  return this.count$Values(1);
};


/**
 * Clears the values in the metadata field.
 */
PhoneMetadataCollection.prototype.clearMetadata = function() {
  this.clear$Field(1);
};



/** @override */
PhoneMetadataCollection.prototype.getDescriptor = function () {
    if (!PhoneMetadataCollection.descriptor_) {
        // The descriptor is created lazily when we instantiate a new instance.
        var descriptorObj = {
            0: {
                name: 'PhoneMetadataCollection',
                fullName: 'PhoneMetadataCollection'
            },
            1: {
                name: 'metadata',
                repeated: true,
                fieldType: goog.proto2.Message.FieldType.MESSAGE,
                type: PhoneMetadata
            }
        };
        PhoneMetadataCollection.descriptor_ =
            goog.proto2.Message.createDescriptor(
                 PhoneMetadataCollection, descriptorObj);
    }
    return PhoneMetadataCollection.descriptor_;
};


// Export getDescriptor static function robust to minification.
PhoneMetadataCollection['ctor'] = PhoneMetadataCollection;
PhoneMetadataCollection['ctor'].getDescriptor =
    PhoneMetadataCollection.prototype.getDescriptor;
