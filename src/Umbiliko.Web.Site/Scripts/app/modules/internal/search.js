﻿define('umbiliko.search', ['jquery', 'umbiliko', 'kendo', 'umbiliko.data'], function ($, U, kendo) {
    U.search = kendo.observable({
        filter: null
    });

    return U.search;
});