﻿define('umbiliko.data', ['jquery', 'umbiliko', 'kendo'], function ($, U, kendo) {
    U.data = {
        state: kendo.observable({
            changeCount: 0,
        }),
        change: function (e) {
            var count = 0;
            $('.k-grid[data-role="grid"]').map(function () {
                return { id: this.id, dataSource: $(this).data().kendoGrid.dataSource };
            }).each(function () {
                count += this.dataSource.destroyed().length;
                for (var data = this.dataSource.data(), idx = 0; idx < data.length; idx++) {
                    if (data[idx].isNew() || data[idx].dirty) {
                        count++;
                    }
                }
                U.data.state.set('changeCount', count);
            });
        },
        commit: function (e) {
            $('.k-grid[data-role="grid"]').map(function () {
                return { id: this.id, dataSource: $(this).data().kendoGrid.dataSource };
            }).each(function () {
                this.dataSource.sync();
            });
        },
        date: function (timestamp) {
            //var m = moment('x', timestamp);
            return new Date(timestamp);
        },
        nodata: function () {
            return {};
        },
        rollback: function (e) {
            $('.k-grid[data-role="grid"]').map(function () {
                return { id: this.id, dataSource: $(this).data().kendoGrid.dataSource };
            }).each(function () {
                this.dataSource.cancelChanges();
                this.dataSource.sync();
            });
        },
        search: {
            logic: 'or',
            filters: []
        },
        setDataSource: function ($el, data) {
            var kendoGrid = $el.data('kendoGrid'),
                schema = kendoGrid.dataSource.options.schema;

            $el.kendoGrid({
                columns: kendoGrid.columns,
                dataSource: new kendo.data.DataSource({
                    data: { 'Data': data, 'Total': data.length },
                    schema: kendoGrid.dataSource.options.schema,
                    /*transport: {
                        read: function (operation) {
                            var data = operation.data || [];
                            operation.success(data);
                        }
                    }*/
                })
            });

            kendoGrid.dataSource.read();

            kendoGrid.refresh();
        }
    };

    $(document).ready(function () {
        kendo.bind($('.navbar [name=edit]'), U.data.state);
    });

    return U.data;
});