﻿define('umbiliko.wizard', ['jquery', 'umbiliko'], function ($, U) {
    var _classes = ['first', 'second', 'third', 'fourth'];

    U.wizard = function ($el, _modal) {
        var _index = -1,
            _nodes = [],
            _current;

        function apply(frame, forward, callee, arguments) {
            _current = frame;
            var $frame = $(frame.el);
            var title = $frame.data('title');
            _modal.title(title);
            if (umbiliko.view.state) {
                umbiliko.view.state.modal = frame.el.id;
            }
            if (forward && frame.handler) {
                frame.handler.apply(callee, arguments);
            }
            return $frame;
        };

        function open() {
            _modal.open();
            $el.one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function (e) {
                console.log('transition ended');
                resize();
            });
        };

        function close() {
            _modal.close();
            $el.one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function (e) {
                console.log('transition ended');
                resize();
            });
        };

        function inc() {
            if (_index === _nodes.length - 1) {
                return false;
            }
            $el.addClass(_classes[_index + 1]);
            if (_index === -1) {
                open();
            }
            else {
                $el.removeClass(_classes[_index]);
            }
            _index++;
            return true;
        }

        function dec() {
            if (_index === -1) {
                return false;
            }
            _index--;
            if (_index === -1) {
                close();
            } else {
                $el.addClass(_classes[_index]);
            }
            $el.removeClass(_classes[_index + 1]);
            return true;
        }

        function mismatch($frame) {
            return !!$frame && !!_current && $frame.length === 1 && $frame[0] !== _current.el;
        }

        function flip($frame) {
            if (mismatch($frame) || !inc()) {
                return false;
            }
            apply(_nodes[_index].hidden, !$frame || !$frame.length, this, arguments).removeClass('hidden');
            resize();
            return true;
        };

        function flop($frame) {
            if (!_current || mismatch($frame) || !dec()) {
                return false;
            }
            if (_index > -1) {
                apply(_nodes[_index].front, !$frame || !$frame.length, this, arguments);
            }
            $(_nodes[_index + 1].hidden.el).addClass('hidden');
            resize();
            return true;
        };

        function pop($frame) {
            if (!_current || mismatch($frame) || !dec()) {
                return false;
            }
            if (_index > -1) {
                apply(_nodes[_index].front, !$frame || !$frame.length, this, arguments);
            }
            resize();
            return true;
        };

        function push($frame) {
            if (mismatch($frame) || !inc()) {
                return false;
            }
            apply(_nodes[_index].front, !$frame || !$frame.length, this, arguments);
            resize();
            return true;
        };

        function overflow() {
            var $frame = $(this), fixedHeight = 0;
            $frame.siblings().not('script').each(function () {
                fixedHeight += $(this).outerHeight(true);
            });
            var clientHeight = Math.min($frame.parent().height(), $frame.parent().innerHeight());
            $frame.height(clientHeight - fixedHeight - $frame.outerHeight(true) + $frame.height());
        };

        function overflowExact($frame, selector) {
            var fixedHeight = 0;
            $(selector, $frame.parent()).each(function () {
                fixedHeight += $(this).outerHeight(true);
            });
            var clientHeight = Math.min($frame.parent().height(), $frame.parent().innerHeight());
            $frame.height(clientHeight - fixedHeight - $frame.outerHeight(true) + $frame.height());
        };

        function resize() {
            overflowExact($('body > .overflow'), '> .navbar, > footer');

            if (_modal) {
                $('.overflow', _modal.wrapper).each(overflow);
                $('.k-grid', _modal.wrapper).each(overflow);
                _modal.center();
            }

            $('.k-grid-content').each(overflow);
        };

        $(window).resize(resize);

        $('> div', $el).each(function () {
            var $frame = $(this),
                controller = $frame.data('controller'),
                handler,
                node;

            if (controller && typeof window[controller] === 'function') {
                handler = window[controller]($frame);
            }

            var frame = { el: this, handler: handler };

            if ($frame.hasClass('hidden')) {
                _nodes.push({ hidden: frame });
            }
            else if (_nodes.length && !(node = _nodes[_nodes.length - 1]).front) {
                node.front = frame;
            }
            else {
                _nodes.push({ front: frame });
            }
        });

        resize();

        return {
            $el: $el,
            el: $el.get(0),
            flip: flip,
            flop: flop,
            overflow: overflow,
            pop: pop,
            push: push,
            resize: resize
        }
    };

    return U.wizard;
});