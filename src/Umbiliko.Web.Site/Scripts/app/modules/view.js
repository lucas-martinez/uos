﻿define('umbiliko.view', ['jquery', 'toastr', 'umbiliko'], function ($, toastr, U) {

    U.event.subscribe('error', function (msg) {
        toastr.error(msg);
    });

    U.event.subscribe('info', function (msg) {
        toastr.info(msg);
    });

    U.event.subscribe('warning', function (msg) {
        toastr.warning(msg);
    });

    var _modal, _wizard;

    function kendoWindow($el, $template, i18n) {
        var window = $el.data('kendoWindow');
        if (!window) {
            window = $el.kendoWindow({
            }).data('kendoWindow');
        }
        window.wrapper.addClass('large');
        var template = kendo.template($template.html());
        window.content(template(i18n || {}));
        return window;
    };

    U.view = {
        kendoWindow: kendoWindow
    };

    return U.view;
});