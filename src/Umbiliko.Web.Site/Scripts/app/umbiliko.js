﻿define('umbiliko', ['jquery', 'require', 'umbiliko.options'], function ($, require, options) {

    //
    // preconditions

    if (typeof options !== 'object') {
        console.log('umbiliko.options must be an object.');
        return;
    }

    var _browser = {
        chrome: /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor),
        msie8: /MSIE 8.0/.test(navigator.userAgent),
        msie9: /MSIE 9.0/.test(navigator.userAgent),
        safari: /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor)
    },
        _params = {},
        _token = options.token,
        _user = options.user,
        _origin = window.location.origin.toLocaleLowerCase(),
        _protocol = typeof options.protocol == 'string' ? options.protocol.toLocaleLowerCase() : 'https:',
        _server = typeof options.server == 'string' ? 'umbiliko.com' : options.server.toLocaleLowerCase(),
        _baseUrl = _protocol + '//' + _server,
        _spinner,
        _widgets = {
            'account-enter': {
                name: 'account-enter',
                options: {
                },
                texts: {
                    title: "Account Enter"
                },
                url: 'https://umbiliko.com/widgets/account-enter'
            },
            'account-exit': {
                name: 'account-exit',
                options: {
                },
                texts: {
                    title: "Account Exit"
                },
                url: 'https://umbiliko.com/widgets/account-exit'
            },
            'account-menu': {
            },
            'account-switch': {
                options: {
                },
                texts: {
                    title: "Account Switch"
                },
                url: 'https://umbiliko.com/widgets/account-switch'
            },
            'domain-jump': {
                options: {
                },
                texts: {
                    title: "Domain Jump"
                },
                url: 'https://umbiliko.com/widgets/domain-jump'
            },
            'product-feedback': {
                options: {
                },
                texts: {
                    title: "Product Feedback"
                },
                url: 'https://umbiliko.com/widgets/product-feedbck'
            }
        },
        _umbiliko = window.umbiliko || {};

    $.each(options.widgets, function (name, options) {
        widget = _widgets[name];
        if (!widget) {
            _widgets[name] = widget = {};
        }
        $.extend(widget, { name: name, options: options });
    });

    _umbiliko.widget = _umbiliko.widget || {};

    window.umbiliko = _umbiliko;

    _browser.webkit = _browser.chrome || _browser.safari;

    //
    // Load query params

    $.each(decodeURIComponent(window.location.search.substring(1)).split('&'), function (i, item) {
        var arr = item.split('=');
        if (arr[0].length) {
            _params[arr[0].toLowerCase()] = arr.length > 1 ? arr[1] : true;
        }
    });

    //
    // Initialize events

    var _events = {},
        _hOP = _events.hasOwnProperty;

    function publish(event) {
        if (!_hOP.call(_events, event)) {
            _events[event] = {};
            return true;
        }

        if (!_events[event].listeners) {
            return true;
        }

        var args = [].splice.call(arguments, 1);

        var result = true;

        _events[event].listeners.forEach(function (listener) {
            var ret = listener.apply(_umbiliko, args);
            if (ret === false) {
                result = false;
            }
        });

        return result;
    };

    function subscribe(event, listener) {

        if (!_hOP.call(_events, event)) {
            _events[event] = {
                dfd: $.Deferred()
            };
        }

        if (!_events[event].listeners) {
            _events[event].listeners = [];
        }

        var index = _events[event].listeners.push(listener) - 1;

        return {
            remove: function () {
                delete _events[event].listeners[index];
            }
        };
    };

    //
    // Spinner

    function getSpinner() {

        var dfd = $.Deferred();

        if (!_spinner) {
            _spinner = [];

            _spinner.push(dfd);

            require(['umbiliko.widget.spinner'], function (factory) {

                $(document).ready(function () {

                    var queue = _spinner;

                    _spinner = factory.create(.05);

                    _spinner.opacity(.25);

                    $.each(queue, function (i, dfd) {
                        dfd.resolve(_spinner);
                    })
                });

            });
        }
        else if (typeof _spinner.spin === 'function') {

            dfd.resolve(_spinner);
        } else {

            _spinner.push(dfd)
        }

        return dfd.promise();
    };

    function spin() {
        getSpinner().then(function (spinner) {
            spinner.spin();
        }).done();
    };

    function stop() {
        getSpinner().then(function (spinner) {
            spinner.stop();
        }).done();
    };

    //
    // Helpers

    function blend($el, skip) {
        var $mask = $('<div class="mask-image"></div>').prependTo($el);
        if ($el.css('border-radio')) $mask.css('border-radio', $el.css('border-radio'));
        for (var count = 0, $ancestor = $el.parent() ; $ancestor.length && $ancestor.get(0) !== document; $ancestor = $ancestor.parent(), count++) {
            if (count < (skip || 0)) continue;
            var img = $ancestor.css('background-image');
            if (img.match('^url')) {
                $mask.css('background-image', img);
                break;
            }
        }

        if (_browser.webkit) {
            $mask.addClass('webkit');
        }
        else if (_browser.mozilla) {
            $mask.addClass('moz');
        }
    };

    var doNothing = function () {
    };

    function findGroup($item) {
        var $parent = $item.parent();
        if ($parent.hasClass('form-group') || $parent.hasClass('input-group') || $parent.hasClass('control-label') || $parent.prop("tagName") == 'LABEL') {
            $item = findGroup($parent);
        }
        return $item;
    };

    $.extend(_umbiliko, {
        blend: blend,
        doNothing: doNothing,
        event: {
            publish: publish,
            subscribe: subscribe
        },
        find: {
            group: findGroup
        }
    });

    //
    // AJAX

    function ajaxError(dfd) {

        return function (xhr, status, error) {
            dfd.reject();
            publish('error', error + ". Status: " + status, "AJAX Error");
        };
    };

    function ajaxSuccess(dfd, silent) {

        return function (response, status, xhr) {

            var firstToLower = function (str) {
                return str.charAt(0).toLowerCase() + str.slice(1);
            };

            // convert to camel case in case serialization failed to.
            var result = (function (o) {
                var r = {};
                $.map(o, function (value, name) {
                    var c = name.charAt(0),
                        lower = c.toLowerCase();
                    if (c !== lower) {
                        name = lower + name.slice(1);
                    }
                    r[name] = value;
                });
                return r;
            })(response);

            var complete = function () {
                if (response.success) {
                    dfd.resolve(result);
                }
                else {
                    dfd.reject(result);
                }
            }

            if (silent) {
                complete()
            }
            else {
                var messages = result.messages || [],
                    success = result.success || true;

                $.each(messages, function (i, msg) {
                    publish(success ? 'info' : 'error', msg);
                });

                setTimeout(complete, 1000 * messages.length);
            }

            return result;
        };
    };

    var jsonGet = function (url) {
        var dfd = $.Deferred();

        $.ajax({
            dataType: 'json',
            error: ajaxError(dfd),
            headers: {
                'Accept': 'application/json; charset=utf-8'
            },
            method: 'GET',
            success: ajaxSuccess(dfd, true),
            url: url
        });

        return dfd.promise();
    };

    var jsonPost = function (data, url) {
        var dfd = $.Deferred();

        $.ajax({
            contentType: 'json',
            data: JSON.stringify(data),
            dataType: 'json',
            error: ajaxError(dfd),
            headers: {
                'Accept': 'application/json; charset=utf-8',
                'Content-Type': 'application/json; charset=utf-8'
            },
            method: 'POST',
            success: ajaxSuccess(dfd),
            url: url
        });

        return dfd.promise();
    };

    //
    // Templates

    function getTemplate(widget) {

        var dfd = $.Deferred(),
            $html = widget.template;

        if (!!$html) {

            dfd.resolve($html);

            return dfd.promise();
        }

        $.ajax({
            error: ajaxError(dfd),
            method: 'GET',
            success: function (response, status, xhr) {
                $html = $(response);

                widget.template = $html;

                dfd.resolve($html);

                return $html;
            },
            url: widget.options.url
        });

        return dfd.promise();
    }

    //
    // Widgets

    function createWidget(name) {
        var dfd = $.Deferred(),
            widget = _widgets[name],
            template = getTemplate(widget);
        require(['umbiliko.widget.' + name], function (factory) {
            widget.factory = factory;
            template.then(function ($html) {
                // custom creation?
                widget.instance = factory($html, widget.options);
                dfd.resolve(widget);
            }).fail(function () {
                publish('error', "Uos failed to open widget " + name, "We are embarraced");
                dfd.reject();
            }).done();
        });
        return dfd.promise();
    };

    function getWidget(name) {

        var dfd = $.Deferred(),
            widget = _widgets[name];

        if (typeof widget.instance === 'object') {

            dfd.resolve(widget);

            return dfd.promise();
        }

        createWidget(name, options.widgets[name]).then(function (widget) {

            dfd.resolve(widget);

        });

        return dfd.promise();
    };

    //
    // Dynamic initialization

    spin();

    if (options.auto === true || (typeof options.auto === 'function' && options.auto() === true)) {
        startUp().always(stop).done();
    }
    else {
        stop();
    }
    //
    // Enter Account

    function enterAccount() {

        var dfd = $.Deferred(),
            widget;

        loop = function () {

            if (arguments.length > 0) {
                widget = arguments[0];
            }

            if (typeof widget !== 'object') {
                dfd.reject();
            }

            widget.instance.input().then(function (input) {

                var count = 0;

                $.map(input, function (data, endPoint) {

                    if (count == 0) {
                        spin();
                    }

                    count++;

                    jsonPost(data, options.endpoints['account-enter']).then(function (response) {

                        // there are two escenarios:
                        // 1. We did succesfully sign-in, sign-up or reset-password
                        // 2. We did succesfully send the token.

                        // in case 2 we can't enter, but maybe continue looping?
                        // for case 2 response.data eq null/undefined

                        if (validate(response)) {

                            /*if (location.host === _server) {
    
                                console.log('entering server', response);
    
                                dfd.resolve(response.data);
    
                                return;
                            }
    
                            //
                            // call proxy
                            /*jsonPost(response, widget.options.url).then(function (proxyResponse) {
                                $.extend(response, proxyResponse);
    
                                dfd.resolve(response);
                            })*/
                            dfd.resolve(response);
                        }
                        else {
                            loop();
                        }

                    }).fail(loop).always(function () {

                        count--;

                        if (count == 0) {
                            stop();
                        }

                    }).done();
                });
            });
        };

        getWidget('account-enter').then(loop).fail(dfd.reject);

        return dfd.promise().then(function (response) {

            _user = response.data;
            _token = _user ? _user.token : null;

            publish('enter', _user);

            // redirect?
            if (typeof response.returnUrl === 'string') {
                window.location = response.returnUrl;
            }
            else if (widget.options.reload) {
                location.reload();
            }

            return response;
        });
    };

    function exitAccount() {

    };

    function feedProduct() {

    };

    function jumpDomain() {

    };

    function showMenu() {
        var dfd = $.Deferred(),
            widget;

        getWidget('account-menu').then(function (widget) {
            dfd.resolve(widget);

        }).fail(function () {
            dfd.reject();
        });

        return dfd.promise();
    };

    function switchAccount() {

        var dfd = $.Deferred(),
           widget;

        loop = function () {

            if (arguments.length > 0) {
                widget = arguments[0];
            }

            if (typeof widget !== 'object') {
                dfd.reject();
            }

            widget.instance.input().then(function (input) {

                var count = 0;

                $.map(input, function (data, endPoint) {

                    if (count == 0) {
                        spin();
                    }

                    count++;

                    ///

                    var client = $('#umbiliko-my-account-client').val(),
                        form = $(e.target).closest('form'),
                        url = form.prop('action'),
                        method = form.prop('method');

                    if (!client) return;

                    var s = client.split('/'),
                        data = {
                            CompanyCode: s[0],
                            SegmentCode: s.length > 1 ? s[1] : null
                        };

                    jsonPost(data, options.endpoints['account-switch']).then(function (response) {

                        if (validate(response)) {

                            setTimeout(function () {
                                //$el.remove();
                                if (response.ReturnUrl) {
                                    location = response.ReturnUrl;
                                } else {
                                    location.reload();
                                }
                            }, 1000 * messages.length);

                            dfd.resolve(response);
                        }
                        else {
                            loop();
                        }
                    }).fail(loop).always(function () {

                        count--;

                        if (count == 0) {
                            stop();
                        }

                    }).done();

                });
            });
        };

        getWidget('account-enter').then(loop).fail(dfd.reject);

        return dfd.promise().then(function (response) {

            _user = response.data;
            _token = _user ? _user.token : null;

            publish('enter', _user);

            // redirect?
            if (typeof response.returnUrl === 'string') {
                window.location = response.returnUrl;
            }
            else if (widget.options.reload) {
                location.reload();
            }

            return response;
        });
    };

    function startUp() {

        // 1. Chech if we are already logged.
        // Get the user from the token.
        return jsonGet(options.endpoints['current-user']).then(function (response) {

            if (validate(response)) {
                // 2. Are we really authenticated? Great! 
                // Spread the word!
                _user = response.data;
                _token = _user.token;
                publish('enter', _user);

                showMenu();
            }
            else {
                // 3. Not autenticated? No worries,
                // the enter widget will do the job
                enterAccount();
            }
        }).fail(function () {
            // 3.1. Even the startUp failed to call 
            // the userUri enpoint, the show must go on!
            enterAccount();
        });
    };

    function validate(response) {
        return (response && response.success == true && typeof response.data === 'object');
    };


    subscribe('account-exit-click', function (e) {
        if (publish('account-exit-prompt')) {
            publish('account-exit');
        }
    });

    subscribe('account-exit', function (e) {
        jsonGet(options.endpoints['account-exit']).then(function (response) {

            if (typeof options.widget['account-exit'].redirectUrl === 'string') {
                window.location = options.widget['account-exit'].redirectUrl;
            }
        });
    });


    if (typeof _umbiliko.hello === 'function') {
        _umbiliko.hello(_umbiliko);
    }

    _umbiliko.notifyValidationErrors = function (el) {
        $('ul li', el).each(function () {
            publish('error', $(this).text());
        });
    };

    /*require(['angular'], function () {
        _umbiliko.module = angular.module('_umbiliko', []);
    });*/

    _umbiliko.enterAccount = enterAccount;
    _umbiliko.exitAccount = exitAccount;
    _umbiliko.feedProduct = feedProduct;
    _umbiliko.jumpDomain = jumpDomain;
    _umbiliko.switchAccount = switchAccount;

    return _umbiliko;
}, function () {
    // umbiliko failed to load
    console.log('Uos failed to load. You should specify some umbiliko.options using require js.');
});