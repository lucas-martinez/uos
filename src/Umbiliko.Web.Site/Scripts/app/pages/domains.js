﻿define(['jquery', 'require', 'umbiliko', 'umbiliko.data', 'umbiliko.message', 'umbiliko.view', 'umbiliko.wizard', 'umbiliko.collections'], function ($, require, U) {
    'use strict'

    var _modal, _wizard;

    $.extend(U.view, {
        state: {
            modal: null
        },
        domain: {
            change: function () {
                var select = this.select();
                U.view.domain.dataItem = select.length === 1 ? this.dataItem(select[0]) : null;
            },
            dataItem: null,
            remove: function (e) {
                console.log('domain remove', e);
            }
        },
        instance: {
            change: function () {
                var select = this.select();
                U.view.instance.dataItem = select.length === 1 ? this.dataItem(select[0]) : null;
            },
            dataItem: null
        },
        product: {
            item: null,
            select: function (e) {
                U.view.product.item = e.item != null ? this.dataItem(e.item) : null;
            }
        }
    });

    U.layout = {
        flip: function () {
            _wizard.flip.apply(this, arguments);
        },
        flop: function () {
            _wizard.flop.apply(this, arguments);
        },
        overflow: function () {
            _wizard.overflow.apply(this, arguments);
        },
        pop: function () {
            _wizard.pop.apply(this, arguments);
        },
        push: function () {
            _wizard.push.apply(this, arguments);
        },
        resize: function () {
            _wizard.resize.apply(this, arguments);
        }
    };

    require(['angular', 'deferred'], function () {
        U.controller = U.module.controller('PageController', ['$scope', function ($scope) {
            $scope.greeting = 'Hola!';
        }]);
        _modal = U.view.kendoWindow($('#modal-window'), $("#modal-template"));
        _wizard = U.wizard(_modal.wrapper.find('.wizard'), _modal);
        U.layout = _wizard;
    });

    return U.view;
});