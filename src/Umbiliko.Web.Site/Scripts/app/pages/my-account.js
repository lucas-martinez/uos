﻿define(['jquery', 'require', 'umbiliko', 'umbiliko.data', 'umbiliko.message', 'umbiliko.view', 'umbiliko.wizard'], function ($, require, U) {
    'use strict'

    var _modal, _wizard;

    $.extend(U.view, {
        state: {
            modal: null
        },
        account: {
            change: function () {
                var select = this.select();
                U.view.account.dataItem = select.length === 1 ? this.dataItem(select[0]) : null;
            },
            dataItem: null,
            filter: function () {
                return {
                    client: U.view.segment.dataItem.Client
                }
            }
        },
        client: {
            item: null,
            select: function (e) {
                U.view.client.item = e.item != null ? this.dataItem(e.item) : null
            }
        },
        company: {
            change: function () {
                var select = this.select();
                U.view.company.dataItem = select.length === 1 ? this.dataItem(select[0]) : null;
            },
            dataItem: null,
            filter: function () {
                var value = $('#company-autocomplete').val();

                if (value) {
                    value = value.toUpperCase();
                }

                return {
                    startsWith: value
                };
            },
            remove: function (e) {
                console.log('company remove', e);
            },
            select: function (item) {
                var kendoAutoComplete = this;
                if (item) {
                    toastr.warning("You can't create a new company with a trademark that already exists!");
                }
            }
        },
        login: {
            filter: function () {
                return {
                    user: U.view.user.dataItem.UserName
                };
            }
        },
        roles: {
            filter: function () {
                return {
                };
            }
        },
        segment: {
            change: function () {
                var select = this.select();
                U.view.segment.dataItem = select.length === 1 ? this.dataItem(select[0]) : null;
            },
            dataItem: null,
            remove: function (e) {
                console.log('segment remove', e);
            }
        },
        user: {
            filter: function () {
                return {
                    startsWith: $('#account-add-user-name').val()
                };
            },
            item: null,
            select: function (e) {
                U.view.user.item = e.item != null ? this.dataItem(e.item) : null;
            }
        }
    });

    U.layout = {
        push: function () {
            _wizard.push.apply(this, arguments);
        }
    }

    require(['angular', 'deferred'], function (angular) {
        U.controller = U.module.controller('PageController', ['$scope', function ($scope) {
            $scope.greeting = 'Hola!';
        }]);
        _modal = U.view.kendoWindow($('#modal-window'), $("#modal-template"));
        _wizard = U.wizard(_modal.wrapper.find('.wizard'), _modal);
        U.layout = _wizard;
        _wizard.push();
    });


    return U.view;
});