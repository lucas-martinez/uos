﻿define(['jquery', 'require', 'umbiliko', 'umbiliko.data', 'umbiliko.message', 'umbiliko.view', 'umbiliko.wizard'], function ($, require, U) {
    'use strict'

    var _modal, _wizard;

    $.extend(U.view, {
        state: {
            modal: null
        },
        feature: {
            filter: function () {
                return {
                    product: U.view.product.item != null ? U.view.product.item.Name : null
                };
            }
        },
        product: {
            item: null,
            select: function (e) {
                U.view.product.item = e.item != null ? this.dataItem(e.item) : null;
            }
        },
        segment: {
            item: null,
            select: function (e) {
                U.view.segment.item = e.item != null ? this.dataItem(e.item) : null;
            }
        },
        subscription: {
            change: function () {
                var select = this.select();
                U.view.subscription.dataItem = select.length === 1 ? this.dataItem(select[0]) : null;
            },
            dataItem: null,
            remove: function (e) {
                console.log('subscription remove', e);
            }
        }
    });

    require(['angular', 'deferred'], function () {
        U.controller = U.module.controller('PageController', ['$scope', function ($scope) {
            $scope.greeting = 'Hola!';
        }]);
        _modal = U.view.kendoWindow($('#modal-window'), $("#modal-template"));
        _wizard = U.wizard(_modal.wrapper.find('.wizard'), _modal);
        U.layout = _wizard;
    });

    return U.view;
});