﻿define(['jquery', 'require', 'kendo', 'umbiliko', 'umbiliko.data', 'umbiliko.message', 'umbiliko.view'], function ($, require, kendo, U) {
    'use strict'

    var _modal, _wizard;

    $.extend(U.view, {
        state: {
            modal: null
        },
        account: {
            add: function () {
                U.view.account.handlers.add.apply(this, arguments);
            },
            change: function () {
                var select = this.select();
                U.view.account.dataItem = select.length === 1 ? this.dataItem(select[0]) : null;
            },
            dataItem: null
        },
        client: {
            item: null,
            select: function (e) {
                U.view.client.item = e.item != null ? this.dataItem(e.item) : null
            }
        },
        roles: {
            filter: function () {
                return {
                };
            }
        },
        user: {
            filter: function () {
                return {
                    startsWith: $('#account-add-user-name').val()
                };
            },
            item: null,
            select: function (e) {
                U.view.user.item = e.item != null ? this.dataItem(e.item) : null;
            }
        }
    });

    require(['angular', 'deferred'], function () {
        U.controller = U.module.controller('PageController', ['$scope', function ($scope) {
            $scope.greeting = 'Hola!';
        }]);
        _modal = U.view.kendoWindow($('#modal-window'), $("#modal-template"));
        _wizard = U.wizard(_modal.wrapper.find('.wizard'), _modal);
        U.layout = _wizard;
    });

    return U.view;
});