﻿define(['jquery', 'require', 'umbiliko', 'umbiliko.data', 'umbiliko.message', 'umbiliko.view', 'umbiliko.wizard'], function ($, require, U) {
    'use strict'

    var _modal, _wizard;

    $.extend(U.view, {
        state: {
            modal: null
        },
        company: {
            CompanyCode: null,
            select: function () {
                U.view.company.CompanyCode = e.item != null ? this.dataItem(e.item) : null
            }
        },
        consumer: {
            change: function () {
                var select = this.select();
                U.view.consumer.dataItem = select.length === 1 ? this.dataItem(select[0]) : null;
            },
            dataItem: null,
            remove: function (e) {
                console.log('consumer remove', e);
            }
        },
        segment: {
            filter: function () {
                return {
                    companyCode: view.company.CompanyCode
                };
            }
        }
    });

    require(['angular', 'deferred'], function () {
        U.controller = U.module.controller('PageController', ['$scope', function ($scope) {
            $scope.greeting = 'Hola!';
        }]);
        _modal = U.view.kendoWindow($('#modal-window'), $("#modal-template"));
        _wizard = U.wizard(_modal.wrapper.find('.wizard'), _modal);
        U.layout = _wizard;
    });

    return U.view;
});