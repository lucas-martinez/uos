﻿define(['jquery', 'require', 'umbiliko', 'umbiliko.data', 'umbiliko.message', 'umbiliko.view', 'umbiliko.wizard'], function ($, require, U) {
    'use strict'

    var _modal, _wizard;

    $.extend(U.view, {
        state: {
            modal: null
        },
        company: {
            code: null,
            select: function () {
                U.view.company.code = e.item != null ? this.dataItem(e.item) : null
            }
        },
        consumer: {
            change: function () {
                var select = this.select();
                U.view.consumer.dataItem = select.length === 1 ? this.dataItem(select[0]) : null;
            },
            dataItem: null,
            filter: function () {
                var customer = view.customer.dataItem;
                return {
                    customer: customer !== null ? customer.CustomerCode : null,
                };
            },
            remove: function (e) {
                console.log('consumer remove', e);
            }
        },
        customer: {
            change: function () {
                var select = this.select();
                U.view.consumer.dataItem = select.length === 1 ? this.dataItem(select[0]) : null;
            },
            dataItem: null,
            remove: function (e) {
                console.log('consumer remove', e);
            }
        },
        feature: {
            filter: function () {
                return {
                    product: view.product.item != null ? U.view.product.item.Name : null
                };
            }
        },
        product: {
            item: null,
            select: function (e) {
                U.view.product.item = e.item != null ? this.dataItem(e.item) : null;
            }
        },
        segment: {
            filter: function () {
                return {
                    companyCode: U.view.company.code
                };
            }
        },
        subscription: {
            change: function () {
                var select = this.select();
                U.view.subscription.dataItem = select.length === 1 ? this.dataItem(select[0]) : null;
            },
            dataItem: null,
            filter: function () {
                var consumer = U.view.consumer.dataItem;
                return {
                    segment: consumer !== null ? consumer.SegmentCode : null,
                    product: consumer !== null ? consumer.ProductName : null,
                    customer: consumer !== null ? consumer.CustomerCoode : null
                };
            },
            remove: function (e) {
                console.log('subscription remove', e);
            }
        }
    });

    U.layout = {
        flip: function () {
            _wizard.flip.apply(this, arguments);
        },
        flop: function () {
            _wizard.flop.apply(this, arguments);
        },
        overflow: function () {
            _wizard.overflow.apply(this, arguments);
        },
        pop: function () {
            _wizard.pop.apply(this, arguments);
        },
        push: function () {
            _wizard.push.apply(this, arguments);
        },
        resize: function () {
            _wizard.resize.apply(this, arguments);
        }
    };

    require(['angular', 'deferred'], function () {
        U.controller = U.module.controller('PageController', ['$scope', function ($scope) {
            $scope.greeting = 'Hola!';
        }]);
        _modal = U.view.kendoWindow($('#modal-window'), $("#modal-template"));
        _wizard = U.wizard(_modal.wrapper.find('.wizard'), _modal);
        U.layout = _wizard;
    });

    return U.view;
});