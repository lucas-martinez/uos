﻿define(['jquery', 'require', 'umbiliko', 'umbiliko.message'], function ($, require, U) {
    'use strict'

    $.extend(U.view, {

    });

    $(document).ready(function () {
        $('#umbiliko-my-account-enter-cmd').click(function (e) {

            e.preventDefault();

            var client = $('#umbiliko-my-account-client').val(),
                form = $(e.target).closest('form'),
                url = form.prop('action'),
                method = form.prop('method');

            if (!client) return;

            var s = client.split('/'),
                data = {
                    CompanyCode: s[0],
                    SegmentCode: s.length > 1 ? s[1] : null
                };
                
            U.ajax.jsonPost({
                data: data,
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('error', arguments)
                    U.event.publish('error', errorThrown + ". Status: " + textStatus, "AJAX Error");
                },
                method: method,
                success: function (response) {
                    var messages = response.Messages || [],
                        success = response.Success;

                    $.each(messages, function (i, msg) {
                        U.event.publish(success ? 'info' : 'error', msg);
                    });

                    if (success) {
                        /*$.each(_done, function (index, fn) {
                            fn($el);
                        });*/
                        setTimeout(function () {
                            //$el.remove();
                            if (response.ReturnUrl) {
                                location = response.ReturnUrl;
                            } else {
                                location.reload();
                            }
                        }, 1000 * messages.length);
                    }
                },
                url: url
            });
        });
    });

    require(['deferred'], function () {
    });

    /*require(['angular'], function () {
        U.controller = U.module.controller('PageController', ['$scope', function ($scope) {
            $scope.greeting = 'Hola!';
        }]);
    });*/

    return U.view;
});