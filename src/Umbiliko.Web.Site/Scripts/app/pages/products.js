﻿define(['jquery', 'require', 'umbiliko', 'umbiliko.data', 'umbiliko.message', 'umbiliko.view', 'umbiliko.wizard'], function ($, require, U) {
    'use strict'

    var _modal, _wizard;

    $.extend(U.view, {
        state: {
            modal: null
        },
        activity: {
            change: function () {
                var select = this.select();
                U.view.activity.dataItem = select.length === 1 ? this.dataItem(select[0]) : null;
            },
            dataItem: null,
            filter: function () {
                var feature = U.view.feature.dataItem,
                    product = U.view.product.dataItem;
                return {
                    feature: feature != null ? feature.FeatureName : null,
                    product: product != null ? product.ProductName : null
                };
            }
        },
        feature: {
            change: function () {
                var select = this.select();
                U.view.feature.dataItem = select.length === 1 ? this.dataItem(select[0]) : null;
            },
            dataItem: null,
            filter: function () {
                var product = U.view.product.dataItem;
                return {
                    product: product != null ? product.ProductName : null
                };
            }
        },
        modal: null,
        product: {
            change: function () {
                var select = this.select();
                U.view.product.dataItem = select.length === 1 ? this.dataItem(select[0]) : null;
            },
            dataItem: null,
            remove: function (e) {
                console.log('product remove', e);
            }
        },
        roles: {
            filter: function () {
                return {
                };
            }
        }
    });

    U.layout = {
        flip: function () {
            _wizard.flip.apply(this, arguments);
        },
        flop: function () {
            _wizard.flop.apply(this, arguments);
        },
        overflow: function () {
            _wizard.overflow.apply(this, arguments);
        },
        pop: function () {
            _wizard.pop.apply(this, arguments);
        },
        push: function () {
            _wizard.push.apply(this, arguments);
        },
        resize: function () {
            _wizard.resize.apply(this, arguments);
        }
    };

    require(['angular', 'deferred'], function () {
        U.controller = U.module.controller('PageController', ['$scope', function ($scope) {
            $scope.greeting = 'Hola!';
        }]);
        _modal = U.view.kendoWindow($('#modal-window'), $("#modal-template"));
        _wizard = U.wizard(_modal.wrapper.find('.wizard'), _modal);
        U.layout = _wizard;
    });

    return U.view;
});