//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "14.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Configuration {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Configuration() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.Configuration", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add Activity.
        /// </summary>
        internal static string AddActivityTitle {
            get {
                return ResourceManager.GetString("AddActivityTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add Domain.
        /// </summary>
        internal static string AddDomainTitle {
            get {
                return ResourceManager.GetString("AddDomainTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add Feature.
        /// </summary>
        internal static string AddFeatureTitle {
            get {
                return ResourceManager.GetString("AddFeatureTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add Product.
        /// </summary>
        internal static string AddProductTitle {
            get {
                return ResourceManager.GetString("AddProductTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Manage Activity.
        /// </summary>
        internal static string ManageActivityTitle {
            get {
                return ResourceManager.GetString("ManageActivityTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Manage Domain.
        /// </summary>
        internal static string ManageDomainTitle {
            get {
                return ResourceManager.GetString("ManageDomainTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Manage Feature.
        /// </summary>
        internal static string ManageFeatureTitle {
            get {
                return ResourceManager.GetString("ManageFeatureTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Manage Product.
        /// </summary>
        internal static string ManageProductTitle {
            get {
                return ResourceManager.GetString("ManageProductTitle", resourceCulture);
            }
        }
    }
}
