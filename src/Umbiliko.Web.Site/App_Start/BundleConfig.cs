﻿using System.Web;
using System.Web.Optimization;

namespace Uos.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region - fonts -

            bundles.Add(new StyleBundle("~/content/css/fontawesome").Include(
                "~/Content/stylesheets/fonts/fontawesome.css"));

            bundles.Add(new StyleBundle("~/content/css/ionicons").Include(
                "~/Content/stylesheets/fonts/ionicons.css"));

            #endregion - fonts -

            #region - internal -

            bundles.Add(new ScriptBundle("~/js/internal").Include(
                "~/Scripts/app/modules/internal/data.js",
                "~/Scripts/app/modules/internal/filter.js",
                "~/Scripts/app/modules/internal/search.js"));

            #endregion - internal -

            #region - modules -

            bundles.Add(new ScriptBundle("~/js/umbiliko").Include(
                "~/Scripts/app/umbiliko.js",
                "~/Scripts/app/modules/email.js",
                "~/Scripts/app/modules/message.js",
                "~/Scripts/app/modules/password.js",
                "~/Scripts/app/modules/phone.js",
                "~/Scripts/app/modules/user.js",
                "~/Scripts/app/modules/view.js",
                "~/Scripts/app/modules/wizard.js"));

            bundles.Add(new StyleBundle("~/content/css/umbiliko").Include(
                "~/Content/stylesheets/umbiliko.css"
                ));

            #endregion - modules -

            #region - pages -

            bundles.Add(new ScriptBundle("~/js/accounts").Include(
                "~/Scripts/app/pages/accounts.js"));

            bundles.Add(new ScriptBundle("~/js/companies").Include(
                "~/Scripts/app/pages/companies.js"));

            bundles.Add(new ScriptBundle("~/js/contact").Include(
                "~/Scripts/app/widgets/contact.js"));

            bundles.Add(new ScriptBundle("~/js/contracts").Include(
                "~/Scripts/app/pages/contracts.js"));

            bundles.Add(new ScriptBundle("~/js/customers").Include(
                "~/Scripts/app/pages/customers.js"));

            bundles.Add(new ScriptBundle("~/js/dashboard").Include(
                "~/Scripts/app/pages/dashboard.js"));

            bundles.Add(new ScriptBundle("~/js/domains").Include(
                "~/Scripts/app/pages/domains.js"));

            bundles.Add(new ScriptBundle("~/js/home").Include(
                "~/Scripts/app/pages/home.js"));

            bundles.Add(new ScriptBundle("~/js/intro").Include(
                "~/Scripts/app/pages/intro.js"));

            bundles.Add(new ScriptBundle("~/js/my-account").Include(
                "~/Scripts/app/pages/my-account.js"));

            bundles.Add(new ScriptBundle("~/js/my-company").Include(
                "~/Scripts/app/pages/my-company.js"));

            bundles.Add(new ScriptBundle("~/js/products").Include(
                "~/Scripts/app/pages/products.js"));

            bundles.Add(new ScriptBundle("~/js/subscriptions").Include(
                "~/Scripts/app/pages/subscriptions.js"));

            bundles.Add(new ScriptBundle("~/js/users").Include(
                "~/Scripts/app/pages/users.js"));

            bundles.Add(new ScriptBundle("~/js/vendors").Include(
                "~/Scripts/app/pages/vendors.js"));

            #endregion - pages -

            #region - phone -

            bundles.Add(new ScriptBundle("~/js/phone").Include(
                "~/Scripts/app/modules/phone/AsYouTypeFormatter.js",
                "~/Scripts/app/modules/phone/Error.js",
                "~/Scripts/app/modules/phone/metadata.js",
                "~/Scripts/app/modules/phone/NumberFormat.js",
                "~/Scripts/app/modules/phone/PhoneMetadata.js",
                "~/Scripts/app/modules/phone/PhoneMetadataCollection.js",
                "~/Scripts/app/modules/phone/PhoneNumber.js",
                "~/Scripts/app/modules/phone/PhoneNumberDesc.js",
                "~/Scripts/app/modules/phone/PhoneNumberUtil.js"));

            #endregion - internal -

            #region - widgets -

            bundles.Add(new ScriptBundle("~/js/widgets").Include(
                "~/Scripts/app/widgets/account-enter.js",
                "~/Scripts/app/widgets/account-exit.js",
                "~/Scripts/app/widgets/account-menu.js",
                "~/Scripts/app/widgets/account-switch.js",
                "~/Scripts/app/widgets/domain-jump.js",
                "~/Scripts/app/widgets/product-feedback.js",
                "~/Scripts/app/widgets/hint.js",
                "~/Scripts/app/widgets/spinner.js"));

            bundles.Add(new StyleBundle("~/content/css/widgets").Include(
                "~/Content/stylesheets/widgets/account-enter.css",
                "~/Content/stylesheets/widgets/account-exit.css",
                "~/Content/stylesheets/widgets/account-menu.css",
                "~/Content/stylesheets/widgets/account-switch.css",
                "~/Content/stylesheets/widgets/domain-jump.css",
                "~/Content/stylesheets/widgets/product-feedback.css",
                "~/Content/stylesheets/widgets/hint.css",
                "~/Content/stylesheets/widgets/spinner.css"));

            #endregion - widgets -

            #region - bootstrap -

            bundles.Add(new ScriptBundle("~/js/bootstrap").Include(
                "~/Scripts/bootstrap.js",
                "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/content/css/bootstrap").Include(
                "~/Content/stylesheets/bootstrap.css"));

            #endregion - bootstrap -

            #region - jquery -

            bundles.Add(new ScriptBundle("~/js/jquery").Include(
                "~/Scripts/jquery-{version}.js"));

            #endregion - jquery -

            #region - kendo ui -

            bundles.Add(new ScriptBundle("~/js/kendo").Include(
                "~/Scripts/kendo/2015.3.930/jquery.min.js",
                //"~/Scripts/kendo/2015.3.930/kendo.all.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.core.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.aspnetmvc.min"));
                //"~/Scripts/kendo/2015.3.930/kendo.binder.min",
                //"~/Scripts/kendo/2015.3.930/kendo.angular.min",
                /*"~/Scripts/kendo/2015.3.930/kendo.ui.core.min*/

            bundles.Add(new ScriptBundle("~/js/kendo/data").Include(
                "~/Scripts/kendo/2015.3.930/kendo.data.min",
                "~/Scripts/kendo/2015.3.930/kendo.data.odata.min",
                "~/Scripts/kendo/2015.3.930/kendo.data.signalr.min",
                "~/Scripts/kendo/2015.3.930/kendo.data.xml.min"
                ));

            bundles.Add(new ScriptBundle("~/js/kendo/controls").Include(
                "~/Scripts/kendo/2015.3.930/kendo.autocomplete.min",
                "~/Scripts/kendo/2015.3.930/kendo.button.min",
                "~/Scripts/kendo/2015.3.930/kendo.calendar.min",
                "~/Scripts/kendo/2015.3.930/kendo.color.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.colorpicker.min",
                "~/Scripts/kendo/2015.3.930/kendo.combobox.min",
                "~/Scripts/kendo/2015.3.930/kendo.core.min",
                "~/Scripts/kendo/2015.3.930/kendo.datetimepicker.min",
                "~/Scripts/kendo/2015.3.930/kendo.draganddrop.min",
                "~/Scripts/kendo/2015.3.930/kendo.dropdownlist.min",
                "~/Scripts/kendo/2015.3.930/kendo.editable.min",
                "~/Scripts/kendo/2015.3.930/kendo.fx.min",
                "~/Scripts/kendo/2015.3.930/kendo.grid.min",
                "~/Scripts/kendo/2015.3.930/kendo.list.min",
                "~/Scripts/kendo/2015.3.930/kendo.listview.min",
                "~/Scripts/kendo/2015.3.930/kendo.maskedtextbox.min",
                "~/Scripts/kendo/2015.3.930/kendo.menu.min",
                "~/Scripts/kendo/2015.3.930/kendo.multiselect.min",
                "~/Scripts/kendo/2015.3.930/kendo.notification.min",
                "~/Scripts/kendo/2015.3.930/kendo.numerictextbox.min",
                "~/Scripts/kendo/2015.3.930/kendo.pager.min",
                "~/Scripts/kendo/2015.3.930/kendo.panelbar.min",
                "~/Scripts/kendo/2015.3.930/kendo.popup.min",
                "~/Scripts/kendo/2015.3.930/kendo.progressbar.min",
                "~/Scripts/kendo/2015.3.930/kendo.resizable.min",
                "~/Scripts/kendo/2015.3.930/kendo.responsivepanel.min",
                "~/Scripts/kendo/2015.3.930/kendo.router.min",
                "~/Scripts/kendo/2015.3.930/kendo.selectable.min",
                "~/Scripts/kendo/2015.3.930/kendo.slider.min",
                "~/Scripts/kendo/2015.3.930/kendo.sortable.min",
                "~/Scripts/kendo/2015.3.930/kendo.splitter.min",
                "~/Scripts/kendo/2015.3.930/kendo.tabstrip.min",
                "~/Scripts/kendo/2015.3.930/kendo.timepicker.min",
                "~/Scripts/kendo/2015.3.930/kendo.timezones.min",
                "~/Scripts/kendo/2015.3.930/kendo.toolbar.min",
                "~/Scripts/kendo/2015.3.930/kendo.tooltip.min",
                "~/Scripts/kendo/2015.3.930/kendo.touch.min",
                "~/Scripts/kendo/2015.3.930/kendo.userevents.min",
                "~/Scripts/kendo/2015.3.930/kendo.validator.min",
                "~/Scripts/kendo/2015.3.930/kendo.view.min",
                "~/Scripts/kendo/2015.3.930/kendo.virtuallist.min",
                "~/Scripts/kendo/2015.3.930/kendo.webcomponents.min",
                "~/Scripts/kendo/2015.3.930/kendo.window.min"));

            bundles.Add(new ScriptBundle("~/js/kendo/mobile").Include(
                "~/Scripts/kendo/2015.3.930/kendo.mobile.actionsheet.min",
                "~/Scripts/kendo/2015.3.930/kendo.mobile.application.min",
                "~/Scripts/kendo/2015.3.930/kendo.mobile.button.min",
                "~/Scripts/kendo/2015.3.930/kendo.mobile.buttongroup.min",
                "~/Scripts/kendo/2015.3.930/kendo.mobile.collapsible.min",
                "~/Scripts/kendo/2015.3.930/kendo.mobile.drawer.min",
                "~/Scripts/kendo/2015.3.930/kendo.mobile.listview.min",
                "~/Scripts/kendo/2015.3.930/kendo.mobile.loader.min",
                "~/Scripts/kendo/2015.3.930/kendo.mobile.modalview.min",
                "~/Scripts/kendo/2015.3.930/kendo.mobile.navbar.min",
                "~/Scripts/kendo/2015.3.930/kendo.mobile.pane.min",
                "~/Scripts/kendo/2015.3.930/kendo.mobile.popover.min",
                "~/Scripts/kendo/2015.3.930/kendo.mobile.scroller.min",
                "~/Scripts/kendo/2015.3.930/kendo.mobile.scrollview.min",
                "~/Scripts/kendo/2015.3.930/kendo.mobile.shim.min",
                "~/Scripts/kendo/2015.3.930/kendo.mobile.splitview.min",
                "~/Scripts/kendo/2015.3.930/kendo.mobile.switch.min",
                "~/Scripts/kendo/2015.3.930/kendo.mobile.tabstrip.min",
                "~/Scripts/kendo/2015.3.930/kendo.mobile.view.min"));

            bundles.Add(new StyleBundle("~/content/css/kendo").Include(
               "~/Content/kendo/2015.2.902/kendo.common.min.css",
               "~/Content/kendo/2015.2.902/kendo.common.core.min.css",
               "~/Content/kendo/2015.2.902/kendo.common-bootstrap.min.css",
               "~/Content/kendo/2015.2.902/kendo.common-bootstrap.core.min.css",
               "~/Content/kendo/2015.2.902/kendo.bootstrap.min.css"
               ));

            #endregion - kendo ui -

            #region - modernizer -

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/js/modernizr").Include(
                "~/Scripts/modernizr-*"));

            #endregion - modernizer -

            #region - toastr -

            bundles.Add(new StyleBundle("~/content/css/toastr").Include(
                "~/Content/stylesheets/toastr.css"));

            #endregion - toastr -

            BundleTable.EnableOptimizations = true;
        }
    }
}
