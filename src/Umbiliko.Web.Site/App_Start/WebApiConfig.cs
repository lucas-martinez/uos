﻿using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Uos.Web
{
    using Providers;
    using Constants;

    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.SetCorsPolicyProviderFactory(new DomainPolicyProvider());
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));
            config.EnableCors();

            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Web API routes
            config.MapHttpAttributeRoutes();
//#if DEBUG
            // Make Json the default formatter
            var jsonFormatter = new JsonMediaTypeFormatter();
            jsonFormatter.Indent = true;
            jsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
            jsonFormatter.MediaTypeMappings.Add(new QueryStringMapping("json", "true", "application/json"));
            jsonFormatter.SerializerSettings = Defaults.JsonSerializerSettings;

            var xmlFormatter = new XmlMediaTypeFormatter();
            xmlFormatter.MediaTypeMappings.Add(new QueryStringMapping("xml", "true", "application/xml"));

            config.Formatters.Clear();
            config.Formatters.Add(jsonFormatter);
            config.Formatters.Add(xmlFormatter);
//#endif
        }
    }
}
