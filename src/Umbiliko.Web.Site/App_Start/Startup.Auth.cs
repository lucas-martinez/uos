﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Facebook;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.MicrosoftAccount;
using Microsoft.Owin.Security.OAuth;
using Microsoft.Owin.Security.Provider;
using Microsoft.Owin.Security.Twitter;
using Microsoft.Owin.Security.WeChat;
using Owin;
using Owin.Security.Providers.Amazon;
using Owin.Security.Providers.ArcGISOnline;
using Owin.Security.Providers.Asana;
using Owin.Security.Providers.Backlog;
using Owin.Security.Providers.BattleNet;
using Owin.Security.Providers.Buffer;
using Owin.Security.Providers.DeviantArt;
using Owin.Security.Providers.Dropbox;
//using Owin.Security.Providers.EveOnline;
using Owin.Security.Providers.Fitbit;
using Owin.Security.Providers.Flickr;
using Owin.Security.Providers.Foursquare;
using Owin.Security.Providers.GitHub;
using Owin.Security.Providers.Gitter;
using Owin.Security.Providers.GooglePlus;
using Owin.Security.Providers.HealthGraph;
using Owin.Security.Providers.Imgur;
using Owin.Security.Providers.Instagram;
using Owin.Security.Providers.LinkedIn;
using Owin.Security.Providers.Onshape;
using Owin.Security.Providers.OpenID;
using Owin.Security.Providers.PayPal;
using Owin.Security.Providers.Reddit;
using Owin.Security.Providers.Salesforce;
using Owin.Security.Providers.Shopify;
using Owin.Security.Providers.Slack;
using Owin.Security.Providers.SoundCloud;
using Owin.Security.Providers.Spotify;
using Owin.Security.Providers.StackExchange;
using Owin.Security.Providers.Steam;
using Owin.Security.Providers.TripIt;
using Owin.Security.Providers.Twitch;
using Owin.Security.Providers.Untappd;
using Owin.Security.Providers.Vimeo;
using Owin.Security.Providers.VisualStudio;
using Owin.Security.Providers.Wargaming;
using Owin.Security.Providers.WordPress;
using Owin.Security.Providers.Yahoo;
using Owin.Security.Providers.Yammer;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Web.Helpers;

namespace Uos.Web
{
    using Constants;
    using Contracts;
    using Entities;
    using Providers;
    using Services;
    
    public partial class Startup
    {
        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }

        public static string PublicClientId { get; private set; }

        public NameValueCollection AppSettings
        {
            get { return ConfigurationManager.AppSettings; }
        }

        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            // Configure the db context and user manager to use a single instance per request
            app.CreatePerOwinContext(UmbilikoDbContext.Create);
            app.CreatePerOwinContext<IAuthenticationContext>(AuthenticationProvider.Create);
            app.CreatePerOwinContext<UserManager<SessionInfo, string>>(AuthenticationProvider.Create);

            // Enable the application to use a cookie to store information for the signed in user
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/")
            });

            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Configure the application for OAuth based flow
            PublicClientId = "self";
            OAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/token"),
                Provider = new OAuthAuthorizationProvider(PublicClientId),
                AuthorizeEndpointPath = new PathString("/account/external-login"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                // In production mode set AllowInsecureHttp = false
                AllowInsecureHttp = true
            };

            // Enable the application to use bearer tokens to authenticate users
            app.UseOAuthBearerTokens(OAuthOptions);

            // https://developers.facebook.com/apps/
            /*app.UseFacebookAuthentication(
               appId: AppSettings["facebook:AppId"],
               appSecret: AppSettings["facebook:AppSecret"]
               );*/

            app.UseFacebookAuthentication(new FacebookAuthenticationOptions
            {
                AppId = AppSettings["facebook:AppId"],
                AppSecret = AppSettings["facebook:AppSecret"],
                Provider = new FacebookAuthenticationProvider()
            });

            // https://github.com/settings/applications
            /*app.UseGitHubAuthentication(
                clientId: App.Secrets.GitHubClientId,
                clientSecret: App.Secrets.GitHubClientSecret);*/

            app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            {
                ClientId = AppSettings["google:ClientId"],
                ClientSecret = AppSettings["google:ClientSecret"],
                Provider = new GoogleAuthenticationProvider()
            });

            // https://account.live.com/developers/applications
            /*app.UseMicrosoftAccountAuthentication(
                clientId: AppSettings["microsoft:CliendId"],
                clientSecret: AppSettings["microsoft:CliendSecret"]
                );*/

            app.UseMicrosoftAccountAuthentication(new MicrosoftAccountAuthenticationOptions
            {
                ClientId = AppSettings["microsoft:CliendId"],
                ClientSecret = AppSettings["microsoft:CliendSecret"],
                Provider = new MicrosoftAccountAuthenticationProvider()
            });

            // https://apps.twitter.com/
            /*.UseTwitterAuthentication(
                consumerKey: AppSettings["twitter:ConsumerKey"],
                consumerSecret: AppSettings["twitter:ConsumerSecret"]);*/
            app.UseTwitterAuthentication(new TwitterAuthenticationOptions
            {
                ConsumerKey = AppSettings["twitter:ConsumerKey"],
                ConsumerSecret = AppSettings["twitter:ConsumerSecret"],
                Provider = new TwitterAuthenticationProvider(),
                BackchannelCertificateValidator = new Microsoft.Owin.Security.CertificateSubjectKeyIdentifierValidator(new[]
                {
                    "A5EF0B11CEC04103A34A659048B21CE0572D7D47", // VeriSign Class 3 Secure Server CA - G2
                    "0D445C165344C1827E1D20AB25F40163D8BE79A5", // VeriSign Class 3 Secure Server CA - G3
                    "7FD365A7C2DDECBBF03009F34339FA02AF333133", // VeriSign Class 3 Public Primary Certification Authority - G5
                    "39A55D933676616E73A761DFA16A7E59CDE66FAD", // Symantec Class 3 Secure Server CA - G4
                    "4eb6d578499b1ccf5f581ead56be3d9b6744a5e5", // VeriSign Class 3 Primary CA - G5
                    "5168FF90AF0207753CCCD9656462A212B859723B", // DigiCert SHA2 High Assurance Server C‎A 
                    "B13EC36903F8BF4701D498261A0802EF63642BC3" // DigiCert High Assurance EV Root CA
                })
            });

            var corsProvider = new DomainPolicyProvider();

            // For more information on how to configure your application using OWIN startup, visit http://go.microsoft.com/fwlink/?LinkID=316888
            app.UseCors(new CorsOptions
            {
                CorsEngine = corsProvider,
                PolicyProvider = corsProvider
            });

            var hubConfiguration = new HubConfiguration();
            hubConfiguration.EnableDetailedErrors = true;
            app.MapSignalR(hubConfiguration);

            AntiForgeryConfig.UniqueClaimTypeIdentifier = Defaults.IdentityProvider;
        }
    }
}
