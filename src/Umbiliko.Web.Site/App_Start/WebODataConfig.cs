﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.OData.Builder;
using System.Web.Http.OData.Extensions;
using Microsoft.OData.Edm;

namespace Uos.Web
{
    using Controllers;

    public static class WebODataConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var builder = new ODataConventionModelBuilder();

            //builder.EntitySet<Agent, Agents2ODataController>("agents");

            //builder.EntitySet<Client, Clients2ODataController>("clients");

            //builder.EntitySet<User, Users2ODataController>("users");

            //builder.EnableLowerCamelCase();

            config.Routes.MapODataServiceRoute(
                routeName: "odata",
                routePrefix: "odata",
                model: builder.GetEdmModel());

            config.AddODataQueryFilter();
        }
    }
}
