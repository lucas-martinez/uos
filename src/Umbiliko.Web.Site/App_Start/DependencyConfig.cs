﻿using Microsoft.AspNet.Http;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Compilation;
using System.Web.Http;

namespace Uos.Web
{
    using Contracts;
    using Providers;
    using Services;
    
    public class DependencyConfig
    {
        public static void RegisterServices()
        {
            ServicesContainer.Default.Register(GlobalConfiguration.Configuration);
            ServicesContainer.Default.Register<IPasswordHasher, PasswordHasher>();
            ServicesContainer.Default.Register<ISmsService, TwilioSmsService>();
            ServicesContainer.Default.Register<ISmtpService, SmtpService>();
            ServicesContainer.Default.Register<IUmbilikoDbInitialization, UmbilikoDbInitialization>();
            ServicesContainer.Default.Register(() => System.Web.HttpContext.Current.GetOwinContext());
            ServicesContainer.Default.Register<IAuthenticationContext>(() => System.Web.HttpContext.Current.GetOwinContext().Get<IAuthenticationContext>());
            ServicesContainer.Default.Register<ProductContext>((Func<ProductContext>)(() =>
            {
                var owinContext = System.Web.HttpContext.Current.GetOwinContext();
                var dbContext = owinContext.Get<Entities.UmbilikoDbContext>();
                var assembly = BuildManager.GetGlobalAsaxType().BaseType.Assembly;
                var companyCode = assembly.GetCustomAttributes<AssemblyTrademarkAttribute>().Select(attr => attr.Trademark).FirstOrDefault();
                var productName = assembly.GetCustomAttributes<AssemblyProductAttribute>().Select(attr => attr.Product).FirstOrDefault();

                var company = CompanyContext.FindAsync(dbContext, companyCode).Result;

                return ProductContext.FindAsync(company, productName).Result;
            }));
        }
    }
}
