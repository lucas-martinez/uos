﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Uos.Web
{
    using Binders;

    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            //ModelBinders.Binders.DefaultBinder = new SmartModelBinder(new[] { new NewtonsoftJsonModelBinder() });

            DependencyConfig.RegisterServices();
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            GlobalConfiguration.Configure(WebODataConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            /*var service = ServiceContainer.Default.GetInstance<Entities.IUmbilikoDbInitialization>();

            if (service != null)
            {
                using (var dbContext = new Entities.UmbilikoDbContext())
                {
                    service.Seed(dbContext);
                }
            }*/
        }

        protected void Page_Load()
        {
            if (!Request.IsSecureConnection)
            {
                HttpContext context = HttpContext.Current;

                UriBuilder secureUrl = new UriBuilder(context.Request.Url);
                secureUrl.Scheme = "https";

                if (Request.IsLocal)
                {
                    secureUrl.Port = 44300;
                }

                Response.Redirect(secureUrl.ToString(), false);
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
        }
    }
}
