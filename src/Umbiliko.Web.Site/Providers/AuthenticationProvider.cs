﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataProtection;
using Microsoft.Owin.Security.Provider;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Compilation;

namespace Uos.Web.Providers
{
    using Assertion;
    using Constants;
    using Contracts;
    using Entities;
    using Models;
    using Results;
    using Services;

    public sealed class AuthenticationProvider : AuthenticationContextBase
    {
        const string XsrfKey = "XsrfId";

        DbContext _dbContext;
        UserSessionStore _userStore;

        public AuthenticationProvider(DomainStore domainStore, UserSessionStore userStore, HttpContext httpContext)
            : base(domainStore, httpContext)
        {
            _dbContext = userStore.DbContext;
            _userStore = userStore;
        }

        public AuthenticationProvider(DomainStore domainStore, UserSessionStore userStore, HttpContextBase httpContext)
            : base(domainStore, httpContext)
        {
            _dbContext = userStore.DbContext;
            _userStore = userStore;
        }

        private AuthenticationProvider(DomainStore domainStore, UserSessionStore userStore, IOwinContext owinContext, IDataProtectionProvider protectionProvider)
            : base(domainStore, owinContext, protectionProvider)
        {
            _dbContext = userStore.DbContext;
            _userStore = userStore;
        }

        public static AuthenticationProvider Current
        {
            get { return (HttpContext.Current.GetOwinContext().Get<IAuthenticationContext>() as AuthenticationProvider) ?? Create(HttpContext.Current); }
        }

        public DbContext DbContext
        {
            get { return _dbContext ?? OwinContext.Get<UmbilikoDbContext>() ?? (_dbContext = new UmbilikoDbContext()); }
        }

        public override IOwinContext OwinContext
        {
            get { return _owinContext ?? (_owinContext = HttpContext.GetOwinContext()); }
        }

        public override ISessionStore Sessions
        {
            get { return UserStore; }
        }

        public UserSessionStore UserStore
        {
            get { return _userStore; }
        }

        IResult Fail(params string[] message)
        {
            return Result.Fail(message);
        }

        private async Task<SessionEntity> FindSessionAsync(Guid sessionId)
        {
            var dbContext = DbContext;

            var dbSet = dbContext.Set<SessionEntity>();

            var entity = await dbSet
                .Include(e => e.Account)
                .Include(e => e.Domain)
                //.Include(e => e.Domain.Company)
                //.Include(e => e.Domain.Consumer)
                //.Include(e => e.Domain.Customer)
                //.Include(e => e.Domain.Product)
                //.Include(e => e.Domain.Product.Subscriptions)
                .Include(e => e.User)
                .SingleOrDefaultAsync(e => e.Id == sessionId);

            return entity;
        }

        private async Task<SessionEntity> FindSessionAsync(string accessToken)
        {
            var dbContext = DbContext;

            var dbSet = dbContext.Set<SessionEntity>();

            var entity = await dbSet
                .Include(e => e.Account)
                .Include(e => e.Domain)
                //.Include(e => e.Domain.Company)
                //.Include(e => e.Domain.Consumer)
                //.Include(e => e.Domain.Customer)
                //.Include(e => e.Domain.Product)
                //.Include(e => e.Domain.Product.Subscriptions)
                .Include(e => e.User)
                .SingleOrDefaultAsync(e => e.AccessToken == accessToken);

            return entity;
        }

        

        public IResult ValidateDomain(SessionInfo session, DomainEntity domain)
        {
            if (domain.DomainName != session.DomainName)
            {
                return Result.Fail("InvalidDomain");
            }

            //
            // Validate domain customer
            if (session.Account == null)
            {
                IEnumerable<AccountInfo> accounts;

                if (domain.CustomerCode != null)
                {
                    AccountInfo account = null;
                    
                    if (domain.ConsumerCode != null)
                    {
                        account = session.User.Accounts.Where(a => a.CompanyCode == domain.CustomerCode).FirstOrDefault();
                    }
                    else
                    {
                        account = session.User.Accounts.Where(a => a.CompanyCode == domain.CustomerCode && a.SegmentCode == domain.ConsumerCode).FirstOrDefault();
                    }
                    
                    if (account != null)
                    {
                        session.Account = account;
                    }
                    else
                    {
                        return Result.Fail("InvalidAccount");
                    }
                }
            }
            else if (domain.CustomerCode != null && session.Account.CompanyCode != domain.CustomerCode)
            {
                return Result.Fail("InvalidAccount");
            }
            else if (domain.ConsumerCode != null && session.Account.SegmentCode != domain.CustomerCode)
            {
                return Result.Fail("InvalidAccount");
            }

            return Result.Default;
        }

        public async Task<IResult> AuthenticateAsync(SessionInfo session, SessionEntity entity)
        {
            var dbContext = DbContext;

            Debug.Assert(!string.IsNullOrEmpty(session.DomainName));

            //
            // Validate domain

            var result = ValidateDomain(session, entity.Domain);

            if (!result.Success) return result;

            //
            // Validate account
            session.Account = entity.Account;
            if (session.Account == null)
            {
                session.Account = entity.Account;
            }
            else if (session.Account.CompanyCode == entity.Account.CompanyCode && session.Account.SegmentCode == entity.Account.SegmentCode)
            {
                session.Account = entity.Account;
            }
            else
            {
                var account = dbContext.Set<AccountEntity>()
                    .Include(e => e.Company)
                    .SingleOrDefault(e => e.CompanyCode == session.Account.CompanyCode && e.SegmentCode == session.Account.SegmentCode && e.UserName == entity.UserName);

                if (account == null) return Result.Fail("NoMatch");

                session.Account = account;
            }

            //
            // Validate IP address

            if (entity.IpAddress != session.IpAddress)
            {
                // what happens with dynamic IP addresses?
            }

            //
            // Validate user name

            if (session.User.Name == null)
            {
                session.User.Name = entity.UserName;
            }
            else if (session.User.Name != entity.UserName)
            {
                return Result.Fail("InvalidUser");
            }

            //
            // Validate product subscription
            // better move to site final
            //var consumers = await GetConsumersAsync(domain.CustomerCode, domain.ConsumerCode, domain.Environment);

            //var consumer = data.Accounts.Intersect(consumers.Select(o => new { o.CompanyCode, o.SegmentCode }));

            session.User.PasswordHash = entity.User.PasswordHash;

            return await AuthenticateAsync(session, entity);
        }

        public async Task<IResult> AuthenticateAsync(SessionInfo session, string password)
        {
            AccountEntity account = null;
            UserEntity user = null;
            var dbContext = DbContext;

            Debug.Assert(session.DomainName != null);

            var domain = await FindDomainAsync(session.DomainName);

            if (domain == null) return Result.Fail("InvalidDomain");

            if (session.User.Name != null)
            {
                user = await dbContext.Set<UserEntity>()
                    .Include(e => e.Accounts)
                    .Include(e => e.Accounts.Select(x => x.Company))
                    .Include(e => e.Accounts.Select(x => x.Segment))
                    .Include(e => e.Logins)
                    .FirstOrDefaultAsync(e => e.UserName == e.UserName);
            }
            else if (session.User.Email != null)
            {
                user = await dbContext.Set<UserEntity>()
                    .Include(e => e.Accounts)
                    .Include(e => e.Accounts.Select(x => x.Company))
                    .Include(e => e.Accounts.Select(x => x.Segment))
                    .FirstOrDefaultAsync(e => e.EmailAddress == session.User.Email);
            }

            if (user == null)
            {
                return Result.Fail("NoMatch");
            }

            //
            // Validate account

            if (session.Account != null)
            {
                account = dbContext.Set<AccountEntity>()
                    .Include(e => e.Company)
                    .SingleOrDefault(e => e.CompanyCode == session.Account.CompanyCode && e.SegmentCode == session.Account.SegmentCode && e.UserName == user.UserName);

                if (account == null) return Result.Fail("NoMatch");

                session.Account = account;
            }

            session.User = user;

            return await Sessions.CreateSessionAsync(user.UserName, password, IpAddress, null, UrlReferrer.Host);
        }

        public async Task SignInAsync(SessionInfo session, string returnUrl, bool isPersistent)
        {
            var properties = new AuthenticationProperties()
            {
                AllowRefresh = true,
                IsPersistent = isPersistent,
                IssuedUtc = session.IssuedUtc,
                RedirectUri = returnUrl,
                ExpiresUtc = session.ExpiresUtc
            };

            ClaimsIdentity rememberBrowserIdentity = null;

            if (session.User.TwoFactorEnabled == true)
            {
                IList<string> providers = await UserManager.GetValidTwoFactorProvidersAsync(session.User.Name);

                if (providers.Count > 0)
                {
                    bool rememberBrowser = await Manager.TwoFactorBrowserRememberedAsync(session.User.Name);

                    if (rememberBrowser)
                    {
                        session.User.TwoFactorConfirmed = true;

                        rememberBrowserIdentity = Manager.CreateTwoFactorRememberBrowserIdentity(session.User.Name);
                    }
                }
            }

            if (rememberBrowserIdentity != null)
            {
                Manager.SignOut(DefaultAuthenticationTypes.ApplicationCookie, rememberBrowserIdentity.AuthenticationType);

                Manager.SignIn(properties, session, rememberBrowserIdentity);
            }
            else
            {
                Manager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

                Manager.SignIn(properties, session);
            }
        }

        public async Task<IResult> AuthenticateAsync(LoginInfo login, AuthenticationProperties properties, ClaimsIdentity external, BaseContext context)
        {
            var session = new SessionInfo
            {
                User = new UserInfo()
            };

            Uri redirectUri;
            IResult result;
            Guid sessionId;
            string xsrfKey;

            _dbContext = new UmbilikoDbContext();

            // Set data.DomainName and redirectUri
            //
            if (string.IsNullOrEmpty(properties.RedirectUri))
            {
                redirectUri = new Uri("/", UriKind.Relative);
            }
            else if (Uri.TryCreate(properties.RedirectUri, UriKind.Absolute, out redirectUri))
            {
                session.DomainName = redirectUri.Host;

                if (redirectUri.Host == context.Request.Host.Value)
                {
                    redirectUri = new Uri(redirectUri.PathAndQuery, UriKind.Relative);
                }
            }
            else
            {
                session.DomainName = context.Request.Host.Value;

                redirectUri = new Uri(properties.RedirectUri, UriKind.Relative);
            }

            // Set data.Id
            //
            if (properties.Dictionary.TryGetValue(XsrfKey, out xsrfKey) && Guid.TryParse(xsrfKey, out sessionId))
            {
                var entity = await FindSessionAsync(sessionId);

                if (entity == null) return Fail("NoMatch");

                result = await AuthenticateAsync(session, entity);
            }
            else
            {
                var entity = await UserStore.FirstConfirmedLoginsAsync(session.User);

                if (entity == null) return Result.Fail("NoMatch");

                session.User.PasswordHash = entity.User.PasswordHash;

                session.User = entity.User;

                return await Sessions.CreateSessionAsync(login, IpAddress, null, UrlReferrer.Host);
            }

            if (!result.Success) return result;

            session.User.Logins.Add(login);

            result = await UserStore.RefreshLoginsAsync(session.User);

            if (!result.Success) return result;

            result = await UserStore.RefreshClaimsAsync(session.User, login, external);

            if (redirectUri.IsAbsoluteUri && redirectUri.Host != context.Request.Host.Value)
            {
                // Post
                var client = new HttpClient();

                var response = await client.PostAsJsonAsync(redirectUri, session);

                //var content = new HttpContent();

                /*using (var jsonWriter = new JsonTextWriter(new StreamWriter(stream)))
                {
                    var serializer = new JsonSerializer();

                    serializer.Serialize(jsonWriter, data);
                }*/
            }
            else
            {
                await SignInAsync(session, returnUrl: Convert.ToString(redirectUri), isPersistent: false);
            }

            return Result.Default;
        }

        public AuthenticationProperties GetAuthenticationProperties(string returnUrl)
        {
            Uri returnUri;

            if (!Uri.TryCreate(returnUrl, UriKind.Absolute, out returnUri))
            {
                var origin = new Uri(HttpContext.Request.UrlReferrer.GetLeftPart(UriPartial.Scheme | UriPartial.Authority));

                returnUri = new Uri(origin, returnUrl);
            }

            var session = CurrentSession;

            var properties = new AuthenticationProperties
            {
                RedirectUri = Convert.ToString(returnUri)
            };

            var token = BearerToken;

            if (session.Id != Guid.Empty)
            {
                properties.Dictionary[XsrfKey] = Convert.ToString(session.Id);
            }
            else if (token != null)
            {
                properties.Dictionary[XsrfKey] = token;
            }
            else if (session.User.Name != null)
            {
                properties.Dictionary[XsrfKey] = session.User.Name;
            }

            return properties;
        }
        
        public static AuthenticationProvider Create(HttpContext httpContext)
        {
            var dbContext = new UmbilikoDbContext();

            var domainStore = new DomainStore(dbContext);

            var userStore = new UserSessionStore(dbContext, new PasswordStrength());

            return new AuthenticationProvider(domainStore, userStore, httpContext);
        }

        public static IAuthenticationContext Create(IdentityFactoryOptions<IAuthenticationContext> options, IOwinContext owinContext)
        {
            var dbContext = owinContext.Get<UmbilikoDbContext>();

            var domainStore = new DomainStore(dbContext);

            var userStore = new UserSessionStore(dbContext, new PasswordStrength());

            return new AuthenticationProvider(domainStore, userStore, owinContext, options.DataProtectionProvider);
        }

        public static UserManager<SessionInfo, string> Create(IdentityFactoryOptions<UserManager<SessionInfo, string>> options, IOwinContext owinContext)
        {
            var current = owinContext.Get<IAuthenticationContext>();

            return current.CreateUserManager();
        }

        public override UserManager<SessionInfo, string> CreateUserManager()
        {
            var userManager = base.CreateUserManager();

            // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
            // You can write your own provider and plug it in here.
            userManager.RegisterTwoFactorProvider("PhoneCode", new PhoneNumberTokenProvider(DbContext));

            userManager.RegisterTwoFactorProvider("EmailCode", new EmailTokenProvider(DbContext));

            return userManager;
        }
    }
}
