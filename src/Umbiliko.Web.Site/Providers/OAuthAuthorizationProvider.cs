﻿using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace Uos.Web.Providers
{
    using Contracts;
    using Entities;

    public class OAuthAuthorizationProvider : OAuthAuthorizationProviderBase
    {
        public OAuthAuthorizationProvider(string publicClientId)
            : base(publicClientId)
        {
        }

        protected override UserManager<SessionInfo, string> GetUserManager(IOwinContext owinContext)
        {
            return owinContext.GetUserManager<UserManager<SessionInfo, string>>();
        }
    }
}