﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Twitter;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Providers
{
    using Assertion;
    using Constants;
    using Contracts;
    using Services;

    public class TwitterAuthenticationProvider : Microsoft.Owin.Security.Twitter.TwitterAuthenticationProvider
    {
        public override async Task Authenticated(TwitterAuthenticatedContext context)
        {
            var nameIdentifier = context.Identity.FindFirst(ClaimTypes.NameIdentifier);
            
            Debug.Assert(context.UserId == nameIdentifier.Value);

            var login = new LoginInfo
            {
                AccessToken = context.AccessToken,
                Label = context.ScreenName,
                LoginKey = nameIdentifier.Value,
                LoginProvider = nameIdentifier.Issuer,
                LoginUtc = DateTime.UtcNow
            };

            context.Identity.AddClaimExact(type: UmbilikoClaimTypes.AccessToken, value: context.AccessToken, issuer: login.LoginProvider);
            context.Identity.AddClaimExact(type: TwitterClaimTypes.AccessTokenSecret, value: context.AccessTokenSecret, issuer: login.LoginProvider);
            context.Identity.AddClaimExact(type: ClaimTypes.Expiration, input: login.ExpiresUtc, issuer: login.LoginProvider);
            context.Identity.AddClaimExact(type: UmbilikoClaimTypes.DisplayName, value: context.ScreenName, issuer: login.LoginProvider);
            context.Identity.AddClaimExact(type: UmbilikoClaimTypes.SessionId, value: context.UserId, issuer: login.LoginProvider);

            IResult result;

            try
            {
                result = await AuthenticationProvider.Current.AuthenticateAsync(login, context.Properties, context.Identity, context);
            }
            catch (Exception e)
            {
                result = Result.Fail(e);
            }

            await base.Authenticated(context);
        }
    }
}
