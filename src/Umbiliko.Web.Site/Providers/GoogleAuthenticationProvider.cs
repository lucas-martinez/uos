﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Google;
using Newtonsoft.Json.Linq;
using System;
using System.Diagnostics;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Uos.Web.Providers
{
    using Assertion;
    using Constants;
    using Contracts;
    using Newtonsoft.Json;
    using Providers;
    using Services;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net.Http.Headers;

    public sealed class GoogleAuthenticationProvider : Microsoft.Owin.Security.Google.GoogleOAuth2AuthenticationProvider
    {
        public override async Task Authenticated(GoogleOAuth2AuthenticatedContext context)
        {
            var nameIdentifier = context.Identity.FindFirst(ClaimTypes.NameIdentifier);
            
            Debug.Assert(context.Id == nameIdentifier.Value);

            var login = new LoginInfo
            {
                AccessToken = context.AccessToken,
                Label = context.GivenName ?? context.FamilyName,
                LoginKey = nameIdentifier.Value,
                LoginProvider = nameIdentifier.Issuer,
                LoginUtc = DateTime.UtcNow
            };

            if (context.ExpiresIn.HasValue) login.ExpiresUtc = DateTime.UtcNow + context.ExpiresIn.Value;

            context.Identity.AddClaimExact(type: UmbilikoClaimTypes.AccessToken, value: context.AccessToken, issuer: login.LoginProvider);
            context.Identity.AddClaimExact(type: ClaimTypes.Email, value: context.Email, issuer: login.LoginProvider);
            context.Identity.AddClaimExact(type: ClaimTypes.Expiration, input: login.ExpiresUtc, issuer: login.LoginProvider);
            context.Identity.AddClaimExact(type: ClaimTypes.GivenName, value: context.GivenName, issuer: login.LoginProvider);
            context.Identity.AddClaimExact(type: ClaimTypes.Surname, value: context.FamilyName, issuer: login.LoginProvider);
            context.Identity.AddClaimExact(type: ClaimTypes.Name, value: context.Name, issuer: login.LoginProvider);
            context.Identity.AddClaimExact(type: ClaimTypes.Uri, value: context.Profile, issuer: login.LoginProvider);

            if (context.User != null)
            {
                var errors = new List<string>();

                var serializer = new JsonSerializer();

                serializer.Error += new EventHandler<Newtonsoft.Json.Serialization.ErrorEventArgs>((currentObject, args) =>
                {
                    errors.Add(args.ErrorContext.Path);
                    args.ErrorContext.Handled = true;
                });

                var userData = context.User.ToObject<GoogleUserInfo>(serializer);

                context.Identity.AddClaimExact(type: GoogleClaimTypes.CircledByCount, input: userData.CircledByCount, issuer: login.LoginProvider);
                context.Identity.AddClaimExact(type: UmbilikoClaimTypes.DisplayName, value: userData.DisplayName, issuer: login.LoginProvider);

                if (userData.Emails != null)
                {
                    foreach (var email in userData.Emails)
                    {
                        context.Identity.AddClaimExact(type: ClaimTypes.Email, value: email.Value, issuer: login.LoginProvider);
                    }
                }

                context.Identity.AddClaimExact(type: GoogleClaimTypes.Etag, value: userData.Etag, issuer: login.LoginProvider);
                context.Identity.AddClaimExact(type: ClaimTypes.Gender, value: userData.Gender, issuer: login.LoginProvider);
                context.Identity.AddClaimExact(type: GoogleClaimTypes.Id, value: userData.Id, issuer: login.LoginProvider);

                if (userData.Image != null)
                {
                    context.Identity.AddClaimExact(type: UmbilikoClaimTypes.Picture, value: userData.Image.Url, issuer: login.LoginProvider);
                }

                context.Identity.AddClaimExact(type: GoogleClaimTypes.IsPlusUser, input: userData.IsPlusUser, issuer: login.LoginProvider);
                context.Identity.AddClaimExact(type: GoogleClaimTypes.Kind, value: userData.Kind, issuer: login.LoginProvider);
                context.Identity.AddClaimExact(type: UmbilikoClaimTypes.Language, value: userData.Language, issuer: login.LoginProvider);
                context.Identity.AddClaimExact(type: GoogleClaimTypes.ObjectType, value: userData.ObjectType, issuer: login.LoginProvider);
                context.Identity.AddClaimExact(type: GoogleClaimTypes.Tagline, value: userData.Tagline, issuer: login.LoginProvider);

                if (userData.Urls != null)
                {
                    foreach (var url in userData.Urls)
                    {
                        context.Identity.AddClaimExact(type: UmbilikoClaimTypes.DisplayName, value: url.Label, issuer: login.LoginProvider);
                        context.Identity.AddClaimExact(type: ClaimTypes.Uri, value: url.Value, issuer: login.LoginProvider);
                    }
                }
            }

            IResult result;

            try
            {
                result = await AuthenticationProvider.Current.AuthenticateAsync(login, context.Properties, context.Identity, context);
            }
            catch (Exception e)
            {
                result = Result.Fail(e);
            }

            await base.Authenticated(context);
        }
    }
}
