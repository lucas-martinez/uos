﻿using Microsoft.Owin.Security;
using System.Web;
using System.Web.Mvc;

namespace Uos.Web.Results
{
    internal class ChallengeResult : HttpUnauthorizedResult
    {
        public ChallengeResult(string provider, AuthenticationProperties properties)
        {
            LoginProvider = provider;
            Properties = properties;
        }

        public AuthenticationProperties Properties { get; set; }

        public string LoginProvider { get; set; }

        public override void ExecuteResult(ControllerContext context)
        {
            context.HttpContext.GetOwinContext().Authentication.Challenge(Properties, LoginProvider);
        }
    }
}
