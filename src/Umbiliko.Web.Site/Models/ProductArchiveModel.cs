﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Models
{
    [DataContract]
    public class ProductArchiveModel
    {
        [DataMember(Name = "alpha", IsRequired = true, Order = 3)]
        public string AlphaVersion { get; set; }

        [DataMember(Name = "beta", IsRequired = true, Order = 4)]
        public string BetaVersion { get; set; }

        [DataMember(Name = "description", IsRequired = true, Order = 1)]
        public string Description { get; set; }

        [DataMember(Name = "features", IsRequired = true, Order = 2)]
        public IEnumerable<FeatureArchiveModel> Features { get; set; }
        
        [DataMember(Name = "name", IsRequired = true, Order = 0)]
        public string ProductName { get; set; }

        [DataMember(Name = "release", IsRequired = true, Order = 5)]
        public string ReleaseVersion { get; set; }
    }
}
