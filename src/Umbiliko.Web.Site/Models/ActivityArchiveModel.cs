﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Models
{
    [DataContract]
    public class ActivityArchiveModel
    {
        [DataMember(Name = "name", IsRequired = true, Order = 0)]
        public string ActivityName { get; set; }

        [DataMember(Name = "description", IsRequired = false, Order = 1)]
        public string Description { get; set; }

        [DataMember(Name = "roles", IsRequired = false, Order = 2)]
        public string Roles { get; set; }
    }
}
