﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Models
{
    [DataContract]
    public class DomainArchiveModel
    {
        [DataMember(Name = "description", IsRequired = true, Order = 3)]
        public string Description { get; set; }

        [DataMember(Name = "name", IsRequired = true, Order = 0)]
        public string DomainName { get; set; }

        [DataMember(Name = "environment", IsRequired = true, Order = 1)]
        public string Environment { get; set; }

        [DataMember(Name = "product", IsRequired = true, Order = 2)]
        public string ProductName { get; set; }
    }
}
