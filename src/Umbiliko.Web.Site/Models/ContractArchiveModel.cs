﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Models
{
    [DataContract]
    public class ContractArchiveModel
    {
        [DataMember(Name = "product", IsRequired = true, Order = 0)]
        public string ProductName { get; set; }

        [DataMember(Name = "subscritpions")]
        public IEnumerable<SubscriptionArchiveModel> Subscriptions { get; set; }

        [DataMember(Name = "segment", IsRequired = true, Order = 1)]
        public string SegmentCode { get; set; }
    }
}
