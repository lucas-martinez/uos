﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Models
{
    [DataContract]
    public class UserArchiveModel
    {
        [DataMember(Name = "email", IsRequired = true, Order = 1)]
        public string EmailAddress { get; set; }

        [DataMember(Name = "name", IsRequired = true, Order = 0)]
        public string UserName { get; set; }

        [DataMember(Name = "mobile", IsRequired = true, Order = 2)]
        public string MobileNumber { get; set; }
    }
}
