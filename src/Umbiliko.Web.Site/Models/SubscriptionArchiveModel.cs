﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Models
{
    [DataContract]
    public class SubscriptionArchiveModel
    {
        [DataMember(Name = "feature", IsRequired = true, Order = 1)]
        public string FeatureName { get; set; }

        [DataMember(Name = "since", IsRequired = true, Order = 3)]
        public long SinceUtc { get; set; }

        [DataMember(Name = "status", IsRequired = true, Order = 2)]
        public string Status { get; set; }

        [DataMember(Name = "until", IsRequired = true, Order = 4)]
        public long? UntilUtc { get; set; }
    }
}
