﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Models
{
    [DataContract]
    public class AccountArchiveModel
    {
        [DataMember(Name = "claims", IsRequired = false, Order = 4)]
        public IDictionary<string, string> Claims { get; set; }

        [DataMember(Name = "roles", IsRequired = true, Order = 3)]
        public string Roles { get; set; }

        [DataMember(Name = "segment", IsRequired = true, Order = 0)]
        public string SegmentCode { get; set; }

        [DataMember(Name = "user", IsRequired = true, Order = 1)]
        public string UserName { get; set; }
    }
}
