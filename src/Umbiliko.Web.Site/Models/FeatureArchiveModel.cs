﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Models
{
    [DataContract]
    public class FeatureArchiveModel
    {
        [DataMember(Name = "activities", IsRequired = true, Order = 2)]
        public IEnumerable<ActivityArchiveModel> Activities { get; set; }

        [DataMember(Name = "description", IsRequired = true, Order = 1)]
        public string Description { get; set; }

        [DataMember(Name = "name", IsRequired = true, Order = 0)]
        public string FeatureName { get; set; }
    }
}
