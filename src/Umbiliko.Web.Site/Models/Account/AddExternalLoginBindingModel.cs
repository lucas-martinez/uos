﻿using System.ComponentModel.DataAnnotations;

namespace Uos.Web.Models.Account
{
    public class AddExternalLoginBindingModel
    {
        [Required]
        [Display(Name = "External access token")]
        public string ExternalAccessToken { get; set; }
    }
}
