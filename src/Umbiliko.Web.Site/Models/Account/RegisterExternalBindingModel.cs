﻿using System.ComponentModel.DataAnnotations;

namespace Uos.Web.Models.Account
{
    public class RegisterExternalBindingModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
