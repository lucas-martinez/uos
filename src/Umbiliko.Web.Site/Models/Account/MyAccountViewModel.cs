﻿using Microsoft.AspNet.Identity;
using System.Collections.Generic;

namespace Uos.Web.Models.Account
{
    public class MyAccountViewModel
    {
        public bool BrowserRemembered { get; set; }

        public string Email { get; set; }

        public bool EmailConfirmed { get; set; }
        
        public bool HasPassword { get; set; }

        public IList<UserLoginInfo> Logins { get; set; }

        public string PhoneNumber { get; set; }

        public string PhoneNumberConfirmed { get; set; }

        public bool TwoFactor { get; set; }

        public string UserName { get; set; }
    }
}
