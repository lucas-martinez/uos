﻿using System.ComponentModel.DataAnnotations;

namespace Uos.Web.Models.Account
{
    public class AddPhoneNumberViewModel
    {
        [Required, Phone, Display(Name = "Phone Number")]
        public string Number { get; set; }
    }
}
