﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Models
{
    [DataContract]
    public class FilterRequestModel
    {
        [DataMember(Name = "filters"), Required]
        public IEnumerable<FilterModel> Filters { get; set; }

        [DataMember(Name = "logic"), Required]
        public string Logic { get; set; }
    }
}
