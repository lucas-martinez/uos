﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Models
{
    public class SearchResultModel
    {
        public string Token { get; set; }

        public string Type { get; set; }

        public ICollection<string> Values { get; set; }
    }
}
