﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Models
{
    [DataContract]
    public class SegmentArchiveModel
    {
        [DataMember(Name = "accounts")]
        public IEnumerable<AccountArchiveModel> Accounts { get; set; }
        
        [DataMember(Name = "code", Order = 0)]
        public string SegmentCode { get; set; }

        [DataMember(Name = "description", IsRequired = false, Order = 2)]
        public string Description { get; set; }

        [DataMember(Name = "environment", IsRequired = true, Order = 1)]
        public string Environment { get; set; }
    }
}
