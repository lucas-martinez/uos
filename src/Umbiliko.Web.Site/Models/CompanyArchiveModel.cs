﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Models
{
    public class CompanyArchiveModel
    {
        [DataMember(Name = "code", IsRequired = true, Order = 0)]
        public string CompanyCode { get; set; }

        [DataMember(Name = "name", IsRequired = true, Order = 1)]
        public string CompanyName { get; set; }

        [DataMember(Name = "type", IsRequired = true, Order = 2)]
        public string CompanyType { get; set; }

        [DataMember(Name = "segments")]
        public IEnumerable<SegmentArchiveModel> Segments { get; set; }
    }
}
