﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Models
{
    public class ContactBindingModel
    {
        [Required]
        public string Email { get; set; }

        public string Company { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Message { get; set; }

        public string Website { get; set; }

        public string[] Interests { get; set; }
    }
}
