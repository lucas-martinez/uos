﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Models
{
    using Areas.Domain.Models;
    using Areas.Authentication.Models;
    using Areas.Authorization.Models;
    using Areas.Configuration.Models;

    [DataContract]
    public class DownloadModel
    {
        [DataMember(Name = "companies")]
        public IEnumerable<CompanyArchiveModel> Companies { get; set; }

        [DataMember(Name = "domains")]
        public IEnumerable<DomainArchiveModel> Domains { get; set; }

        [DataMember(Name = "products")]
        public IEnumerable<ProductArchiveModel> Products { get; set; }

        [DataMember(Name = "contracts")]
        public IEnumerable<ContractArchiveModel> Contracts { get; set; }
        
        [DataMember(Name = "users")]
        public IEnumerable<UserArchiveModel> Users { get; set; }
    }
}
