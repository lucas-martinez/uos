﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Models
{
    public class EnterPostModel
    {
        public string SegmentCode { get; set; }

        public ICollection<string> Clients { get; set; }
        
        public string Code { get; set; }

        public Dictionary<string, string> Companies { get; set; }

        public string CompanyCode { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        public string Ip { get; set; }

        public bool ForgotPassword { get; set; }

        public bool ForgotUser { get; set; }
        
        public string Identity { get; set; }

        [Required]
        public string Origin { get; set; }

        public string Provider { get; set; }

        [DataType(DataType.Password)]
        public string Password { get; set; }

        public bool Register { get; set; }

        public bool RememberMe { get; set; }

        public bool ShowPassword { get; set; }

        public string Token { get; set; }

        public string User { get; set; }

        public bool WithClient { get; set; }

        public bool WithCode { get; set; }

        public bool WithEmail { get; set; }

        public bool WithIdentity { get; set; }

        public bool WithPassword { get; set; }

        public bool WithToken { get; set; }

        public bool WithUser { get; set; }
    }
}
