﻿using Kendo.Mvc.UI.Fluent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Routing;

namespace Uos.Web
{
    public static class DataSourceBuilderExtensions
    {
        public static AjaxDataSourceBuilder<T> WebApi<T>(this DataSourceBuilder<T> ds, Mvc.ActionSet api, string data = null, object values = null)
            where T : class, new()
        {
            var ajax = ds.WebApi();

            ajax.Create(create => create.Url(api.Create.Url));
            ajax.Destroy(destroy => destroy.Url(api.Destroy.Url));
            if (values != null)
            {
                //ajax.Read(read => read.Url(api.Read.Url).Route(new RouteValueDictionary(readValues)));
                var routeValues = new RouteValueDictionary(values);
                routeValues.Add("area", api.Read.Area);
                ajax.Read(read => read.Action(api.Read.Action, api.Read.Controller, routeValues));
            }
            else
            {
                ajax.Read(read =>
                {
                    read.Url(api.Read.Url);

                    if (data != null)
                    {
                        read.Data(data);
                    }
                });
            }
            ajax.Update(update => update.Url(api.Update.Url));

            return ajax;
        }
    }
}
