﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web
{
    public static class UrlExtensions
    {
        const string localhost = @"localhost";

        public static string GetDomainName(this Uri url)
        {
            return string.Equals(url.Host, localhost, StringComparison.InvariantCultureIgnoreCase) ? localhost + ':' + url.Port : url.Host.ToLowerInvariant();
        }
    }
}
