﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Entities
{ 
    using Models;
    using Utilities;

    public static class ArchiveExtensions
    {
        public static AccountArchiveModel Archive(this AccountEntity entity)
        {
            return entity == null ? null :
                new AccountArchiveModel
                {
                    SegmentCode = entity.SegmentCode,
                    Roles = entity.Roles,
                    UserName = entity.UserName
                    /*Status = string.Join("|", Enum.GetValues(typeof(UserStatuses))
                        .OfType<UserStatuses>()
                        .Where(flag => entity.UserStatus.HasFlag(flag))
                        .Select(flag => flag.ToString()))*/
                };
        }

        public static ActivityArchiveModel Archive(this ActivityEntity entity)
        {
            return entity == null ? null :
                new ActivityArchiveModel
                {
                    ActivityName = entity.ActivityName,
                    Description = entity.Description,
                    Roles = entity.Roles
                };
        }

        public static CompanyArchiveModel Archive(this CompanyEntity entity)
        {
            return entity == null ? null :
                new CompanyArchiveModel
                {
                    CompanyName = entity.CompanyName,
                    CompanyType = entity.CompanyType,
                    CompanyCode = entity.CompanyCode
                };
        }

        public static ContractArchiveModel Archive(this ContractEntity entity)
        {
            Func<DateTime, long> dateToUtcTimestamp = (d) => { return (long)Math.Floor((d - new DateTime()).TotalMilliseconds); };

            return entity == null ? null :
                new ContractArchiveModel
                {
                    SegmentCode = entity.ConsumerCode,
                    ProductName = entity.ProductName,
                    Subscriptions = entity.Subscriptions.Select(e => e.Archive())
                };
        }

        public static DomainArchiveModel Archive(this DomainEntity entity)
        {
            return entity == null ? null :
                new DomainArchiveModel
                {
                    Description = entity.Description,
                    DomainName = entity.DomainName,
                    Environment = entity.Environment,
                    ProductName = entity.ProductName
                };
        }

        public static FeatureArchiveModel Archive(this FeatureEntity entity)
        {
            return entity == null ? null :
                new FeatureArchiveModel
                {
                    Activities = entity.Activities.OrderBy(e => e.ActivityName).Select(e => e.Archive()),
                    Description = entity.Description,
                    FeatureName = entity.FeatureName
                };
        }

        public static ProductArchiveModel Archive(this ProductEntity entity)
        {
            return entity == null ? null :
                new ProductArchiveModel
                {
                    AlphaVersion = entity.AlphaVersion,
                    BetaVersion = entity.BetaVersion,
                    Description = entity.Description,
                    Features = entity.Features.OrderBy(e => e.FeatureName).Select(e => e.Archive()),
                    ProductName = entity.ProductName,
                    ReleaseVersion = entity.ReleaseVersion
                };
        }

        public static SegmentArchiveModel Archive(this SegmentEntity entity)
        {
            return entity == null ? null :
                new SegmentArchiveModel
                {
                    SegmentCode = entity.SegmentCode,
                    Description = entity.Description,
                    Environment = entity.Environment
                };
        }

        public static SubscriptionArchiveModel Archive(this SubscriptionEntity entity)
        {
            return entity == null ? null :
                new SubscriptionArchiveModel
                {
                    FeatureName = entity.FeatureName,
                    SinceUtc = entity.SinceUtc.ToUnixMoment(),
                    Status = entity.Status,
                    UntilUtc = entity.UntilUtc.ToUnixMoment()
                };
        }

        public static UserArchiveModel ToFileModel(this UserEntity entity)
        {
            return entity == null ? null :
                new UserArchiveModel
                {
                    EmailAddress = entity.EmailAddress,
                    MobileNumber = entity.PhoneNumber,
                    UserName = entity.UserName,
                    //Status = entity.UserStatus
                };
        }
    }
}
