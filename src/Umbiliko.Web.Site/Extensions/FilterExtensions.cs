﻿using Kendo.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web
{
    using Models;

    public static class FilterExtensions
    {
        public static Filter ToFilter(this ICollection<FilterDescriptor> descriptors)
        {
            Func<object, string[]> split = (o) => o != null ? o.ToString().Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries) : new string[0];

            return new Filter
            {
                Activities = descriptors.Where(d => d.Member == "ActivityName").SelectMany(d => split(d.Value)).ToArray(),
                Clients = descriptors.Where(d => d.Member == "Client").SelectMany(d => split(d.Value)).ToArray(),
                Domains = descriptors.Where(d => d.Member == "DomainName").SelectMany(d => split(d.Value)).ToArray(),
                Emails = descriptors.Where(d => d.Member == "EmailAddress").SelectMany(d => split(d.Value)).ToArray(),
                Features = descriptors.Where(d => d.Member == "FeatureName").SelectMany(d => split(d.Value)).ToArray(),
                Roles = descriptors.Where(d => d.Member == "GroupName").SelectMany(d => split(d.Value)).ToArray(),
                Machines = descriptors.Where(d => d.Member == "MachineName").SelectMany(d => split(d.Value)).ToArray(),
                Phones = descriptors.Where(d => d.Member == "MobileNumber" || d.Member == "PhoneNumber").SelectMany(d => split(d.Value)).ToArray(),
                Products = descriptors.Where(d => d.Member == "ProductName").SelectMany(d => split(d.Value)).ToArray(),
                Users = descriptors.Where(d => d.Member == "UserName").SelectMany(d => split(d.Value)).ToArray()
            };
        }

        public static Filter ToFilter(this ICollection<SearchResultModel> tokens)
        {
            return new Filter
            {
                Activities = tokens.Where(t => t.Type == "activity").SelectMany(t => t.Values).ToArray(),
                Clients = tokens.Where(t => t.Type == "client").SelectMany(t => t.Values).ToArray(),
                Domains = tokens.Where(t => t.Type == "domain").SelectMany(t => t.Values).ToArray(),
                Emails = tokens.Where(t => t.Type == "email").SelectMany(t => t.Values).ToArray(),
                Features = tokens.Where(t => t.Type == "feature").SelectMany(t => t.Values).ToArray(),
                Roles = tokens.Where(t => t.Type == "group").SelectMany(t => t.Values).ToArray(),
                Machines = tokens.Where(t => t.Type == "machine").SelectMany(t => t.Values).ToArray(),
                Products = tokens.Where(t => t.Type == "product").SelectMany(t => t.Values).ToArray(),
                Terms = tokens.Where(t => t.Type == "term").SelectMany(t => t.Values).ToArray(),
                Users = tokens.Where(t => t.Type == "user").SelectMany(t => t.Values).ToArray()
            };
        }
    }
}
