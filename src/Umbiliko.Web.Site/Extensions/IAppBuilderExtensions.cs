﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.IdentityModel.Protocols;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.OAuth;
using Owin;
using System;
using System.Configuration;

namespace Umbiliko.Web.Owin
{
    public static class IAppBuilderExtensions
    {
        const string DefaultLoginPath = "/account/login";
        const string DefaultPublicClientId = "self";
        const string DefaultTokenEndpointPath = "/oauth/token";
        
        /// <summary>
        /// Enable the application to use bearer tokens to authenticate users
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        public static IAppBuilder UseOAuthBearerTokens(this IAppBuilder app)
        {
            // In production mode set AllowInsecureHttp = false
            bool allowInsecureHttp;
            if (!bool.TryParse(ConfigurationManager.AppSettings["oauth:PublicClientId"], out allowInsecureHttp))
                allowInsecureHttp = true;

            var publicClientId = ConfigurationManager.AppSettings["oauth:PublicClientId"] ?? DefaultPublicClientId;
            var tokenEndpointPath = ConfigurationManager.AppSettings["oauth:TokenEndpointPath"] ?? DefaultTokenEndpointPath;
            
            var options = new OAuthAuthorizationServerOptions
            {
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(14),
                AuthorizeEndpointPath = new PathString("/oauth/external-login"),
                AllowInsecureHttp = allowInsecureHttp,
                Provider = new OAuthProvider(publicClientId),
                TokenEndpointPath = new PathString("/oauth/token"),
            };

            app.UseOAuthBearerTokens(options);

            return app;
        }

        public static IAppBuilder UseFacebookAuthentication(this IAppBuilder app)
        {
            var appId = ConfigurationManager.AppSettings["facebook:AppId"];
            var appSecret = ConfigurationManager.AppSettings["facebook:AppSecret"];
            if (appId != null && appSecret != null)
            {
                app.UseFacebookAuthentication(appId: appId, appSecret: appSecret);
            }

            return app;
        }

        public static IAppBuilder UseGoogleAuthentication(this IAppBuilder app)
        {
            var clientId = ConfigurationManager.AppSettings["google:ClientId"];
            var clientSecret = ConfigurationManager.AppSettings["google:ClientSecret"];

            if (clientId != null && clientSecret != null)
            {
                app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
                {
                    ClientId = clientId,
                    ClientSecret = clientSecret
                });
            }

            return app;
        }

        /// <summary>
        /// Setup application at https://account.live.com/developers/applications
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        public static IAppBuilder UseMicrosoftAccountAuthentication(this IAppBuilder app)
        {
            var clientId = ConfigurationManager.AppSettings["microsoft:ClientId"];
            var clientSecret = ConfigurationManager.AppSettings["microsoft:ClientSecret"];

            if (clientId != null && clientSecret != null)
            {   
                app.UseMicrosoftAccountAuthentication(
                clientId: clientId,
                clientSecret: clientSecret);
            }

            return app;
        }

        /// <summary>
        /// Setup application at https://apps.twitter.com/
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        public static IAppBuilder UseTwitterAuthentication(this IAppBuilder app)
        {   
            var consumerKey = ConfigurationManager.AppSettings["twitter:ClientId"];
            var consumerSecret = ConfigurationManager.AppSettings["twitter:ClientSecret"];

            if (consumerKey != null && consumerSecret != null)
            { 
                app.UseTwitterAuthentication(consumerKey: consumerKey, consumerSecret: consumerSecret);
            }

            return app;
        }
    }
}
