﻿using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;


namespace Uos.Web
{
    public static class DataSourceRequestExtensions
    {
        static readonly ICollection<FilterDescriptor> NoFilterDescriptors = Array.AsReadOnly(new FilterDescriptor[0]);

        public static DataSourceRequest DefaultSort(this DataSourceRequest request, params string[] members)
        {
            if (request == null)
            {
                return null;
            }

            if (request.Sorts == null)
            {
                request.Sorts = new List<SortDescriptor>();
            }

            if (request.Sorts.Count == 0)
            {
                members.ForEach(member => request.Sorts.Add(new SortDescriptor(member, ListSortDirection.Ascending)));
            }

            return request;
        }

        public static ICollection<FilterDescriptor> ResetFilters(this DataSourceRequest request)
        {
            if (request == null || request.Filters == null || request.Filters.Count == 0) return NoFilterDescriptors;

            var filters = request.Filters.OfType<FilterDescriptor>()
                .Union(request.Filters.OfType<CompositeFilterDescriptor>().SelectMany(f => f.FilterDescriptors.OfType<FilterDescriptor>()))
                .ToList();

            request.Filters.Clear();

            return filters;
        }
    }
}
