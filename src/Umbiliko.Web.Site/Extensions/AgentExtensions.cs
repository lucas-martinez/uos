﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web
{
    using Entities;
    using Models;

    public static class AgentExtensions
    {
        public static RobotRowModel ToRowModel(this RobotEntity entity)
        {
            var taskType = typeof(AgentTasks);
            var taskValues = Enum.GetValues(taskType);

            return entity == null ? null : new RobotRowModel
            {
                AgentId = entity.Id,
                MachineName = entity.MachineName,
                ProcessId = entity.ProcessId,
                CompletedTasksCount = taskValues.OfType<AgentTasks>().Where(value => entity.TasksCompleted.HasFlag(value)).Count(),
                PendingTasksCount = taskValues.OfType<AgentTasks>().Where(value => entity.TasksPending.HasFlag(value)).Count(),
                RunningTasksCount = taskValues.OfType<AgentTasks>().Where(value => entity.TasksRunning.HasFlag(value)).Count()
            };
        }

        public static RobotViewModel ToViewModel(this RobotEntity entity)
        {
            var taskType = typeof(AgentTasks);
            var taskValues = Enum.GetValues(taskType);

            return entity == null ? null : new RobotViewModel
            {
                AgentId = entity.Id,
                ProcessId = entity.ProcessId,
                MachineName = entity.MachineName,
                CompletedTasks = taskValues.OfType<AgentTasks>().Where(value => entity.TasksCompleted.HasFlag(value)).Select(value => Enum.GetName(taskType, value)).ToArray(),
                PendingTasks = taskValues.OfType<AgentTasks>().Where(value => entity.TasksPending.HasFlag(value)).Select(value => Enum.GetName(taskType, value)).ToArray(),
                RunningTasks = taskValues.OfType<AgentTasks>().Where(value => entity.TasksRunning.HasFlag(value)).Select(value => Enum.GetName(taskType, value)).ToArray(),
            };
        }
    }
}
