﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Uos.Web.Models;

namespace Uos.Web.Controllers
{
    using Constants;
    using Contracts;
    using Models;
    using Providers;
    using Results;
    using Services;

    [RoutePrefix("")]
    public class DefaultController : Mvc.UmbilikoController
    {
        protected override IAuthenticationContext Authentication
        {
            get { return AuthenticationProvider.Current; }
        }

        protected override DomainStoreBase Domains
        {
            get { return AuthenticationProvider.Current.DomainStore; }
        }

        protected override ISessionStore Sessions
        {
            get { return AuthenticationProvider.Current.UserStore; }
        }

        protected override IUserStore Users
        {
            get { return AuthenticationProvider.Current.UserStore; }
        }

        //
        // POST: /account-claim
        [HttpPost, Route("account-claim")]
        public override Task<ActivityResult> AccountClaim(AccountClaimRequest request)
        {
            return base.AccountClaim(request);
        }

        //
        // POST: /account-enter
        [HttpPost, Route("account-enter")]
        public override Task<SessionResult> AccountEnter(AccountEnterRequest request)
        {
            return base.AccountEnter(request);
        }

        //
        // POST: /account-exit
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post), Route("account-exit")]
        public override Task<SessionResult> AccountExit(AccountExitRequest request)
        {
            return base.AccountExit(request);
        }

        //
        // POST: /account-register
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post), Route("account-register")]
        public override Task<SessionResult> AccountRegister(AccountRegisterRequest request)
        {
            return base.AccountRegister(request);
        }

        //
        // POST: /account-reset
        [HttpPost, Route("account-reset")]
        public override Task<SessionResult> AccountReset(AccountResetRequest request)
        {
            return base.AccountReset(request);
        }

        //
        // POST: /account-switch
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post), Route("account-switch")]
        public override Task<SessionResult> AccountSwitch(AccountSwitchRequest request)
        {
            return base.AccountSwitch(request);
        }

        //
        // GET: /current-domain
        [AcceptVerbs(HttpVerbs.Get), Route("current-domain")]
        public override Task<DomainResult> CurrentDomain()
        {
            return base.CurrentDomain();
        }

        //
        // GET: /current-token
        [HttpGet, Route("current-token")]
        public override ActivityResult<string> CurrentToken()
        {
            return base.CurrentToken();
        }

        //
        // GET: /current-user
        [HttpGet, Route("current-user")]
        public override UserResult CurrentUser()
        {
            return base.CurrentUser();
        }

        // GET: /dashboard
        [HttpGet, Mvc.Activity("dashboard", Level = AccessLevels.Alleged), Route("dashboard")]
        public ViewResult Dashboard()
        {
            throw new NotImplementedException();
        }

        //
        // POST: /domain-jump
        [HttpPost, Route("domain-jump")]
        public override Task<SessionResult> DomainJump(DomainJumpRequest request)
        {
            return base.DomainJump(request);
        }

        // GET: /intro
        [HttpGet, Mvc.Activity("intro", Level = AccessLevels.Public), Route("intro")]
        public ViewResult Intro()
        {
            throw new NotImplementedException();
        }

        // GET: /
        [HttpGet, Mvc.Activity("home", Level = AccessLevels.Public), Route(Actions.Default)]
        public ViewResult Index()
        {
            ViewBag.Title = "Uos";

            var user = Authentication.CurrentSession;

            var level = user != null ? user.AccessLevel : AccessLevels.Public;

            ViewBag.Level = level;

            switch (level)
            {
                case AccessLevels.Secured:
                    break;

                case AccessLevels.Internal:
                    break;

                case AccessLevels.Private:
                    break;

                case AccessLevels.Protected:
                    break;

                default:
                    break;
            }

            if (!User.Identity.IsAuthenticated)
            {

            }
            if (level >= AccessLevels.Protected)
            {
                return View("Dashboard");
            }

            return View("Intro");
        }

        //
        // POST: /login
        [AllowAnonymous, HttpPost, Mvc.Activity("login", Level = AccessLevels.Public), Route("login")/*, ValidateAntiForgeryToken*/]
        public ActionResult Login(string provider, string returnUrl)
        {
            var request = HttpContext.ApplicationInstance.Context.Request;

            var authentication = Authentication as AuthenticationProvider;

            Uri uri = request.Url,
                referrerUri = request.UrlReferrer,
                redirectUri = referrerUri;

            if (returnUrl == null || Uri.TryCreate(returnUrl, UriKind.RelativeOrAbsolute, out redirectUri))
            {
                redirectUri = referrerUri;
            }

            if (referrerUri.Host != uri.Host)
            {
                // validate domain
                var domain = authentication.FindDomainAsync(referrerUri.Host);

                if (domain == null) return HttpNotFound();

                if (!redirectUri.IsAbsoluteUri)
                {
                    redirectUri = new Uri(new Uri(referrerUri.GetLeftPart(UriPartial.Scheme | UriPartial.Authority), UriKind.Absolute), referrerUri.PathAndQuery);
                }

                // validate ConsumerKey and ConsumerSecret?
            }
            else if (redirectUri.IsAbsoluteUri)
            {
                redirectUri = new Uri(redirectUri.PathAndQuery, UriKind.Relative);
            }

            var properties = authentication.GetAuthenticationProperties(Convert.ToString(redirectUri));

            return new ChallengeResult(provider, properties);
        }
    }
}