﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Uos.Web.Controllers
{
    using Constants;
    using Utilities;

    [Http.Feature(Features.Localization), RoutePrefix("api/" + Features.Localization)]
    public class ConstantsApiController : Http.HttpController
    {
        [HttpGet, Http.Activity("read-company-types", Level = AccessLevels.Public), Route(Collections.Companies)]
        public IHttpActionResult Companies()
        {
            return Ok(Strings.CompanyTypes.ResourceManager.ToDictionary<CompanyTypes>());
        }

        [HttpGet, Http.Activity("read-countries", Level = AccessLevels.Public), Route(Collections.Countries)]
        public IHttpActionResult Countries()
        {
            return Ok(Strings.Countries.ResourceManager.ToDictionary<Countries>());
        }

        [HttpGet, Http.Activity("read-currencies", Level = AccessLevels.Public), Route(Collections.Currencies)]
        public IHttpActionResult Currencies()
        {
            return Ok(Strings.Currencies.ResourceManager.ToDictionary<Currencies>().Select(entry => new { Name = entry.Key, Text = entry.Value }));
        }
    }
}
