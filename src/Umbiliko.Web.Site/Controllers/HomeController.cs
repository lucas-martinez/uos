﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace Umbiliko.Web.Controllers
{
    using Assertion;
    using Constants;
    using Core;
    using Core.Configuration;
    using Email;
    using Models;
    using Services;

    [Mvc.Feature(Features.Application), RequireHttps, RoutePrefix("")]
    public class HomeController : Mvc.EntityController
    {
        [HttpGet, Mvc.Activity("home", Level = AccessLevels.Public), Route(Actions.Default)]
        public ActionResult Index()
        {
            ViewBag.Title = "Umbiliko";

            var user = Authentication.CurrentSession;

            var level = user != null ? user.AccessLevel : AccessLevels.Public;

            ViewBag.Level = level;

            switch (level)
            {
                case AccessLevels.Secured:
                    break;

                case AccessLevels.Internal:
                    break;

                case AccessLevels.Private:
                    break;

                case AccessLevels.Protected:
                    break;

                default:
                    break;
            }

            if (!User.Identity.IsAuthenticated)
            {
                
            }
            if (level >= AccessLevels.Protected)
            {
                return View("Dashboard");
            }

            return View("Intro");
        }

        [HttpPost, Mvc.Activity("contact", Level = AccessLevels.Public), Route("contact")]
        public async Task<ActionResult> Contact(ContactBindingModel model)
        {
            if (model.Interests == null) model.Interests = new string[] { };

            if (!ModelState.IsValid)
            {
            }

            var smtpService = Umbiliko.ServicesContainer.Default.GetInstance<ISmtpService>();

            if (smtpService == null) return Json(new { success = false, message = "Not implemented" });

            var culture = CultureInfo.CurrentCulture;

            if (culture.Parent != CultureInfo.InvariantCulture) culture = culture.Parent;

            IResult result;

            try
            {
                ITemplateEngine engine = new RazorTemplateEngineBuilder()
                    .FindTemplatesInDirectory(HostingEnvironment.MapPath("~/Templates/Email"))
                    .CacheExpiresIn(TimeSpan.FromSeconds(20))
                    .UseSharedCache()
                    .Build();

                ViewBag.Company = model.Company;
                ViewBag.Email = model.Email;
                ViewBag.Interests = model.Interests;
                ViewBag.Name = model.Name;
                ViewBag.Website = model.Website;

                MailMessage message = engine.RenderEmail("ContactEmail.cshtml", model, culture);

                message.To.Add(new MailAddress("admin@umbiliko.com"));
                message.CC.Add(new MailAddress("lucas.martinez@deventry.com"));

                result = await smtpService.SendAsync(message);
            }
            catch (Exception ex)
            {
                result = Result.Fail(ex.Message);
            }

            return Json(new { success = result.Success, message = result.Messages.FirstOrDefault() });
        }

        [HttpGet, Mvc.Activity("dashboard", Level = AccessLevels.Protected), Route("dashboard")]
        public ActionResult Dashboard()
        {
            ViewBag.Title = "Umbiliko";

            return View();
        }

        [HttpGet, Mvc.Activity("intro", Level = AccessLevels.Public), Route("intro")]
        public ActionResult Intro()
        {
            ViewBag.Title = "Umbiliko";

            return View();
        }
        /*
        [HttpGet, Mvc.Activity("register"), Route("register")]
        public ActionResult Register()
        {
            ViewBag.Title = "Umbiliko";

            return View();
        }*/
    }
}
