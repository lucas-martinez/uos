﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace Umbiliko.Web.Controllers
{
    using Entities;
    using Models;

    [RoutePrefix("api/agents")]
    public class Agents2ApiController : ApiController
    {
        private UmbilikoDbContext DbContext = new UmbilikoDbContext();

        // GET: api/agents
        [HttpGet, Route("")]
        public IQueryable<AgentRowModel> Get()
        {
            return DbContext.Agents.Select(e => new AgentRowModel
            {
            });
        }

        // GET: api/agents/5
        [HttpGet, ResponseType(typeof(AgentEntity))]
        public async Task<IHttpActionResult> Get(Guid id)
        {
            AgentEntity agentEntity = await DbContext.Agents.FindAsync(id);
            if (agentEntity == null)
            {
                return NotFound();
            }

            return Ok(agentEntity);
        }

        // PUT: api/agents/5
        [HttpPut, ResponseType(typeof(void))]
        public async Task<IHttpActionResult> Put(Guid id, AgentEntity agentEntity)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != agentEntity.AgentId)
            {
                return BadRequest();
            }

            DbContext.Entry(agentEntity).State = EntityState.Modified;

            try
            {
                await DbContext.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AgentEntityExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/agents
        [HttpPost, ResponseType(typeof(AgentEntity))]
        public async Task<IHttpActionResult> Post(AgentEntity agentEntity)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            DbContext.Agents.Add(agentEntity);

            try
            {
                await DbContext.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (AgentEntityExists(agentEntity.AgentId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = agentEntity.AgentId }, agentEntity);
        }

        // DELETE: api/agents/5
        [HttpDelete, ResponseType(typeof(AgentEntity))]
        public async Task<IHttpActionResult> Delete(Guid id)
        {
            AgentEntity agentEntity = await DbContext.Agents.FindAsync(id);
            if (agentEntity == null)
            {
                return NotFound();
            }

            DbContext.Agents.Remove(agentEntity);
            await DbContext.SaveChangesAsync();

            return Ok(agentEntity);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                DbContext.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AgentEntityExists(Guid id)
        {
            return DbContext.Agents.Count(e => e.AgentId == id) > 0;
        }
    }
}