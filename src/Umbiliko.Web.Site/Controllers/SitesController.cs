﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Umbiliko.Web.Controllers
{
    using Constants;
    using Contracts;
    using Entities;

    [Mvc.Feature(Features.Authentication), RoutePrefix("sites")]
    public class SitesController : Mvc.EntityController
    {
        // GET: sites
        [HttpGet, Mvc.Activity("display-sites"), Route(Actions.Default)]
        public ActionResult Index()
        {
            return View();
        }

        // GET: sites/{id:int}
        [HttpGet, Mvc.Activity("read-sites"), Route(Actions.Default)]
        public async Task<ActionResult> GetDomain()
        {
            var domain = await Authentication.FindReferredDomainAsync();

            if (domain == null) return HttpNotFound();

            return Json(domain, JsonRequestBehavior.AllowGet);
        }

        // GET: sites/{id:guid}
        [HttpGet, Mvc.Activity("read-site"), Route("{id:guid}")]
        public async Task<ActionResult> GetDomainInstance(Guid id)
        {
            var instance = await InstanceContext.FindAsync(DbContext, id);

            if (instance == null) return HttpNotFound();

            var site = await Authentication.FindDomainAsync(instance.Domain.Name);
            
            site.Identity = instance.Entity.Id;
            site.IpAddress = instance.Entity.IpAddress;
            site.MachineName = instance.Entity.MachineName;
            site.ProcessId = instance.Entity.ProcessId;
            site.ProductName = instance.Domain.Entity.ProductName;
            site.ProductVersion = instance.Entity.ProductVersion;
            
            return Json(site, JsonRequestBehavior.AllowGet);
        }

        // POST: sites
        [HttpGet, Mvc.Activity("create-site"), Route(Actions.Default)]
        public async Task<ActionResult> PostSite(Instance model)
        {
            string origin = HttpContext.Request.UrlReferrer.Host.ToLowerInvariant(),
                remote = HttpContext.Request.ServerVariables["REMOTE_ADDR"].ToLowerInvariant(),
                ipAddress = HttpContext.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToLowerInvariant();

            if (string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',').Select(s => s.Trim()).ToArray();

                Regex ip = new Regex(@"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b");
                MatchCollection matches = ip.Matches(ipAddress);
                if (matches.Count > 0) ipAddress = matches[0].Value;
            }

            var domain = DbContext.Set<DomainEntity>()
                .Include(e => e.Product)
                .Include(e => e.Product.Features)
                .Include(e => e.Product.Features.First().Activities)
                .SingleOrDefault(e => e.DomainName == origin || e.DomainName == remote);

            if (domain == null) return HttpNotFound();

            var updateUtc = DateTime.UtcNow;

            InstanceEntity instance = null;

            if (model.Identity.HasValue)
            {
                instance = domain.Instances.SingleOrDefault(e => e.Id == model.Identity.Value);

                if (instance.MachineName != model.MachineName) instance = null;
            }

            if (instance == null)
            {
                instance = domain.Instances.SingleOrDefault(e => e.MachineName == model.MachineName);
            }

            if (instance == null)
            {
                instance = DbContext.Set<InstanceEntity>().Create();

                instance.DomainName = domain.DomainName;
                instance.MachineName = model.MachineName;
            }

            instance.IpAddress = ipAddress;
            instance.ProcessId = model.ProcessId;
            instance.ProductVersion = model.ProductVersion;
            instance.UpdateUtc = updateUtc;

            var site = await Authentication.FindDomainAsync(instance.DomainName);

            site.Identity = instance.Id;
            site.IpAddress = ipAddress;
            site.MachineName = instance.MachineName;
            site.ProcessId = instance.ProcessId;
            site.ProductName = domain.ProductName;
            site.ProductVersion = instance.ProductVersion;

            var result = await SaveChangesAsync(() =>
            {
                return Json(site, JsonRequestBehavior.AllowGet);
            });
            
            foreach (var featureModel in model.Features)
            {
                var featureEntity = domain.Product.Features.SingleOrDefault(e => e.FeatureName == featureModel.Name);

                if (featureEntity == null)
                {
                    featureEntity = DbContext.Set<FeatureEntity>().Create();

                    featureEntity.FeatureName = featureModel.Name;
                    featureEntity.ProductName = domain.ProductName;

                    domain.Product.Features.Add(featureEntity);

                    foreach (var activity in featureModel.Activities)
                    {
                        var activityEntity = DbContext.Set<ActivityEntity>().Create();

                        activityEntity.ActivityName = activity.Name;
                        activityEntity.FeatureName = featureEntity.FeatureName;
                        activityEntity.ProductName = domain.ProductName;

                        featureEntity.Activities.Add(activityEntity);
                    }
                }
                else
                {
                    foreach (var activity in featureModel.Activities)
                    {
                        var activityEntity = featureEntity.Activities.FirstOrDefault(e => e.ActivityName == activity.Name);

                        if (activityEntity == null)
                        {
                            activityEntity = DbContext.Set<ActivityEntity>().Create();

                            activityEntity.ActivityName = activity.Name;
                            activityEntity.FeatureName = featureEntity.FeatureName;
                            activityEntity.ProductName = domain.ProductName;

                            featureEntity.Activities.Add(activityEntity);
                        }
                    }
                }

                featureEntity.UpdateUtc = updateUtc;
            }

            domain.Product.UpdateUtc = updateUtc;

            return await SaveChangesAsync(() =>
            {
                return result;
            });
        }
    }
}
