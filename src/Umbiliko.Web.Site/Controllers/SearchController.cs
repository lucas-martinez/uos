﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Uos.Web.Controllers
{
    using Areas.Domain.Models;
    using Areas.Authentication.Models;
    using Areas.Authorization.Models;
    using Areas.Configuration.Models;
    using Constants;
    using Entities;
    using Inquiry;
    using Models;
    using Services;

    [Mvc.Feature(Features.Search), RoutePrefix("search")]
    public class SearchController : Mvc.EntityController
    {
        // GET: search
        [HttpGet, Mvc.Activity("search"), Route(Actions.Default)]
        public async Task<ActionResult> Index(string token)
        {
            var session = Authentication.CurrentSession;

            if (session.Account == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var account = await AccountContext.FindAsync(DbContext, session.User.Name, session.Account.CompanyCode, session.Account.SegmentCode);

            var company = account.Company;

            var results = new List<SearchResultModel>();

            /*var service = new SearchService(DbContext);

            var filter = service.ToFilter(token.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Select(value => service.Parse(value)).ToList());

            if (filter.HasClients || filter.HasEmails || filter.HasPhones || filter.HasUsers)
            {
                IQueryable<AccountEntity> source = DbContext.Accounts;

                if (service.FilterAccounts(ref source, filter))
                {
                    var values = source.ToList().Select(e => e.AsSearchResultValue()).ToList();

                    if (values.Count > 0) results.Add(new SearchResultModel { Type = "Accounts", Values = values });
                }
            }

            if (filter.HasActivities)
            {
                IQueryable<ActivityEntity> source = DbContext.Activities;

                if (service.FilterActivities(ref source, filter))
                {
                    var values = source.ToList().Select(e => e.AsSearchResultValue()).ToList();

                    if (values.Count > 0) results.Add(new SearchResultModel { Type = "Activities", Values = values });
                }
            }
            

            if (filter.HasClients)
            {
                IQueryable<SegmentEntity> source = DbContext.Segments;

                if (service.FilterSegments(ref source, filter))
                {
                    var values = source.ToList().Select(e => e.AsSearchResultValue()).ToList();

                    if (values.Count > 0) results.Add(new SearchResultModel { Type = "Units", Values = values });
                }
            }

            if (filter.HasFeatures)
            {
                IQueryable<FeatureEntity> source = DbContext.Features;

                if (service.FilterFeatures(ref source, filter))
                {
                    var values = source.ToList().Select(e => e.AsSearchResultValue()).ToList();

                    if (values.Count > 0) results.Add(new SearchResultModel { Type = "Features", Values = values });
                }
            }

            if (filter.HasRoles)
            {
            }

            if (filter.HasProducts)
            {
                IQueryable<ProductEntity> source = DbContext.Products;

                if (service.FilterProducts(ref source, filter))
                {
                    var values = source.ToList().Select(e => e.AsSearchResultValue()).ToList();

                    if (values.Count > 0) results.Add(new SearchResultModel { Type = "Products", Values = values });
                }
            }

            if (filter.HasClients && (filter.HasProducts || filter.HasFeatures))
            {
                IQueryable<SubscriptionEntity> source = DbContext.Subscriptions;

                if (service.FilterSubscriptions(ref source, filter))
                {
                    var values = source.ToList().Select(e => e.AsSearchResultValue()).ToList();

                    if (values.Count > 0) results.Add(new SearchResultModel { Type = "Subscriptions", Values = values });
                }
            }

            if (filter.HasUsers)
            {
                IQueryable<UserEntity> source = DbContext.Users;

                if (service.FilterUsers(ref source, filter))
                {
                    var values = source.ToList().Select(e => e.AsSearchResultValue()).ToList();

                    if (values.Count > 0) results.Add(new SearchResultModel { Type = "Users", Values = values });
                }
            }*/

            return View(results);
        }

        // POST: search
        [HttpPost, Mvc.Activity("filter"), Route(Actions.Default)]
        public async Task<ActionResult> Filter([Bind(Prefix = "filter")]FilterRequestModel model)
        {
            if (!ModelState.IsValid)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var results = new List<SearchResultModel>();

            var service = new SearchService(DbContext);

            foreach (var filter in model.Filters.Where(f => f.Value != null))
            {
                var result = await service.FilterAsync(service.Parse(filter.Value));
                results.Add(result);
            }

            return Json(results);
        }
    }
}