﻿using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Uos.Web.Controllers
{
    using Constants;

    [Mvc.Feature(Features.Authentication), RoutePrefix("widgets")]
    public class WidgetsController : Controller
    {
        // GET: /widgets/account-enter
        [Mvc.Activity("widget-account-enter", Level = AccessLevels.Public), Route("account-enter")]
        public PartialViewResult AccountEnter()
        {
            ViewBag.LoginProviders = HttpContext.GetOwinContext().Authentication.GetExternalAuthenticationTypes().ToList();
            ViewBag.ReturnUrl = null;// returnUrl;
            ViewBag.Persist = false;

            return PartialView();
        }

        // GET: /widgets/account-exit
        [Mvc.Activity("widget-account-exit"), Route("account-exit")]
        public PartialViewResult AccountExit()
        {
            return PartialView();
        }

        // GET: /widgets/account-menu
        [Mvc.Activity("widget-account-menu"), Route("account-menu")]
        public PartialViewResult AccountMenu()
        {
            return PartialView();
        }

        // GET: /widgets/account-switch
        [Mvc.Activity("widget-account-switch", Level = AccessLevels.Public), Route("account-switch")]
        public PartialViewResult AccountSwitch()
        {
            return PartialView();
        }

        // GET: /widgets/domain-jump
        [Mvc.Activity("widget-domain-jump", Level = AccessLevels.Public), Route("domain-jump")]
        public PartialViewResult DomainJump()
        {
            return PartialView();
        }

        // GET: /widgets/product-feedback
        [Mvc.Activity("widget-product-feedback", Level = AccessLevels.Public), Route("product-feedback")]
        public PartialViewResult ProductFeedback()
        {
            return PartialView();
        }
    }
}