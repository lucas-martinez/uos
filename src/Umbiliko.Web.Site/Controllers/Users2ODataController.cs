﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;
using System.Web.Http.OData.Routing;
using System.Web.OData.Routing;
using Microsoft.Data.OData;

namespace Umbiliko.Web.Controllers
{
    using Entities;
    using System.Threading.Tasks;

    [ODataRouting(), ODataRoutePrefix("users")]
    public class Users2ODataController : ODataController
    {
        private static ODataValidationSettings _validationSettings = new ODataValidationSettings();

        private IUsersService UsersService = new UsersService();

        // GET: odata/users
        [EnableQuery, ODataRoute("")]
        public IHttpActionResult GetUsers(ODataQueryOptions<User> queryOptions)
        {
            // validate the query.
            try
            {
                queryOptions.Validate(_validationSettings);
            }
            catch (ODataException ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok<IQueryable<User>>(UsersService.Users);
        }

        // GET: odata/users(5)
        [EnableQuery, ODataRoute("({userName})")]
        public async Task<IHttpActionResult> GetUser([FromODataUri] string userName, ODataQueryOptions<User> queryOptions)
        {
            // validate the query.
            try
            {
                queryOptions.Validate(_validationSettings);
            }
            catch (ODataException ex)
            {
                return BadRequest(ex.Message);
            }
            
            return Ok<User>(await UsersService.GetUserAsync(userName));
        }

        // PUT: odata/users(5)
        [EnableQuery, ODataRoute("({userName})")]
        public async Task<IHttpActionResult> Put([FromODataUri] string userName, Delta<User> delta)
        {
            Validate(delta.GetEntity());

            if (!ModelState.IsValid) return BadRequest(ModelState);

            return Updated(await UsersService.UpdateUserAsync(userName, (u) => delta.Put(u)));
        }

        // POST: odata/users
        [HttpPost]
        public async Task<IHttpActionResult> Post(User user)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            user = await UsersService.CreateUserAsync(user.Name);
            
            return Created(user);
        }

        // PATCH: odata/users(5)
        [AcceptVerbs("PATCH", "MERGE"), ODataRoute("({userName})")]
        public async Task<IHttpActionResult> Patch([FromODataUri] string userName, Delta<User> delta)
        {
            Validate(delta.GetEntity());

            if (!ModelState.IsValid) return BadRequest(ModelState);

            var user = await UsersService.UpdateUserAsync(userName, (value) => delta.Patch(value));

            return Updated(user);
        }

        // DELETE: odata/users(5)
        [HttpDelete, ODataRoute("({userName})")]
        public async Task<IHttpActionResult> Delete([FromODataUri] string userName)
        {
            var user = await UsersService.RemoveUserAsync(userName);

            if (user == null) return NotFound();

            return StatusCode(HttpStatusCode.NoContent);
        }
    }
}
