﻿using System.Threading.Tasks;
using System.Web.Http;

namespace Uos.Web.Controllers
{
    using Constants;
    using Contracts;
    using Http;
    using Providers;
    using Results;

    [Feature(Features.Application), RoutePrefix("api/domain")]
    public class DomainDataController : DataController
    {
        // GET: api/domains/
        [Activity(Activities.ReadUsers, Level = AccessLevels.Public), Route(Actions.Default)]
        public async Task<DomainResult> Get()
        {
            var domain = await AuthenticationProvider.Current.FindReferredDomainAsync();

            return DomainResult.Assert(domain);
        }
    }
}
