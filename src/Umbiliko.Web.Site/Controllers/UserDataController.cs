﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Uos.Web.Controllers
{
    using Constants;
    using Contracts;
    using Http;

    [Feature(Features.Application), RoutePrefix("api/users")]
    public class UserDataController : DataController
    {
        // GET: api/users/{id:guid}
        [Activity(Activities.ReadUsers, Level = AccessLevels.Private), Route("{id:guid}")]
        public UserInfo Get(Guid id)
        {
            return new UserInfo();
        }

        // GET: api/users/{id}
        [Activity(Activities.ReadUsers, Level = AccessLevels.Private), Route("{id}")]
        public UserInfo Get(string name)
        {
            return new UserInfo();
        }

        // POST: api/users/{id:guid}
        public void Post([FromBody]UserInfo value)
        {
        }

        // PUT: api/users/{id:guid}
        public void Put(Guid id, [FromBody]UserInfo value)
        {
        }

        // DELETE: api/users/{id:guid}
        public void Delete(Guid id)
        {
        }
    }
}
