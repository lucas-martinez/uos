﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Uos.Web.Entities;

namespace Uos.Web.Controllers
{
    using Constants;

    [Http.Feature(Features.Assertion), RoutePrefix("api/" + Collections.Indicators)]
    public class IndicatorsApiController : ApiController
    {
        private UmbilikoDbContext DbContext = new UmbilikoDbContext();

        // GET: api/indicators
        [HttpGet, Http.Activity("read-indicators"), Route(Actions.Default)]
        public IQueryable<IndicatorEntity> GetIndicators()
        {
            return DbContext.Set<IndicatorEntity>();
        }

        // GET: api/indicators/5
        [HttpGet, Http.Activity("read-indicators"), ResponseType(typeof(IndicatorEntity)), Route(Actions.Default)]
        public async Task<IHttpActionResult> GetIndicatorEntity(Guid id)
        {
            IndicatorEntity indicatorEntity = await DbContext.Set<IndicatorEntity>().FindAsync(id);
            if (indicatorEntity == null)
            {
                return NotFound();
            }

            return Ok(indicatorEntity);
        }

        // PUT: api/Indicators/5
        [HttpPut, Http.Activity("update-indicators"), ResponseType(typeof(void)), Route(Actions.Default)]
        public async Task<IHttpActionResult> PutIndicatorEntity(Guid id, IndicatorEntity indicatorEntity)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != indicatorEntity.AgentId)
            {
                return BadRequest();
            }

            DbContext.Entry(indicatorEntity).State = EntityState.Modified;

            try
            {
                await DbContext.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!IndicatorEntityExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/indicators
        [HttpPost, Http.Activity("create-indicators"), ResponseType(typeof(IndicatorEntity)), Route(Actions.Default)]
        public async Task<IHttpActionResult> PostIndicatorEntity(IndicatorEntity indicatorEntity)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            DbContext.Set<IndicatorEntity>().Add(indicatorEntity);

            try
            {
                await DbContext.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (IndicatorEntityExists(indicatorEntity.AgentId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = indicatorEntity.AgentId }, indicatorEntity);
        }

        // DELETE: api/indicators/5
        [HttpDelete, Http.Activity("destroy-indicators"), ResponseType(typeof(IndicatorEntity)), Route(Actions.Default)]
        public async Task<IHttpActionResult> DeleteIndicatorEntity(Guid id)
        {
            IndicatorEntity indicatorEntity = await DbContext.Set<IndicatorEntity>().FindAsync(id);
            if (indicatorEntity == null)
            {
                return NotFound();
            }

            DbContext.Set<IndicatorEntity>().Remove(indicatorEntity);
            await DbContext.SaveChangesAsync();

            return Ok(indicatorEntity);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                DbContext.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool IndicatorEntityExists(Guid id)
        {
            return DbContext.Set<IndicatorEntity>().Count(e => e.AgentId == id) > 0;
        }
    }
}