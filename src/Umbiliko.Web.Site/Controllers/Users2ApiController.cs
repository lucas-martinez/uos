﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace Umbiliko.Web.Controllers
{
    using Entities;
    using Models;

    [RoutePrefix("api/users")]
    public class Users2ApiController : ApiController
    {
        private IUsersService UsersService = new UsersService();

        #region - api/users -

        // GET: api/users
        public IQueryable<User> GetUsers()
        {
            return UsersService.Users;
        }

        // GET: api/users/5
        [ResponseType(typeof(UserViewModel)), Route("{userName}")]
        public async Task<IHttpActionResult> GetUser(string userName)
        {
            var user = await UsersService.GetUserAsync(userName);

            if (user == null) return NotFound();

            return Ok(user);
        }

        // PUT: api/users/5
        [ResponseType(typeof(UserViewModel)), Route("{userName}")]
        public async Task<IHttpActionResult> PutUser(string userName, UserPutModel userEntity)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            if (userName != userEntity.UserName) return BadRequest();

            try
            {
                await UsersService.UpdateUserAsync(userName, (user) => { });
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UsersService.UserExists(userName))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/users
        [ResponseType(typeof(UserViewModel))]
        public async Task<IHttpActionResult> PostUser(UserPostModel model)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            User user;

            try
            {
                user = await UsersService.CreateUserAsync(model.UserName);
            }
            catch (DbUpdateException)
            {
                if (UsersService.UserExists(model.UserName))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(new Uri(Request.RequestUri, user.Name), user);
        }

        // DELETE: api/users/5
        [ResponseType(typeof(User)), Route("{userName}")]
        public async Task<IHttpActionResult> DeleteUser(string userName)
        {
            var user = await UsersService.RemoveUserAsync(userName);

            if (user == null) return NotFound();

            return Ok(user);
        }

        #endregion - api/users -

        #region - api/clients/{code}/groups/{name}/roles -

        #endregion - api/clients/{code}/groups/{name}/roles -

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                UsersService.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}