﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Umbiliko.Web.Entities;

namespace Umbiliko.Web.Controllers
{
    using Constants;

    [Mvc.Feature(Features.Automation), RoutePrefix(Collections.Agents)]
    public class AgentsController : Mvc.EntityController<AgentEntity, Guid>
    {
        // GET: Agents
        [Mvc.Activity("view-agents"), Route(Actions.Default)]
        public async Task<ActionResult> Index()
        {
            var agents = DbContext.Agents;
            return View(await agents.ToListAsync());
        }

        // GET: Agents/Details/5
        [Mvc.Activity("view-agents"), Route("{id}")]
        public async Task<ActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AgentEntity agentEntity = await DbContext.Agents.FindAsync(id);
            if (agentEntity == null)
            {
                return HttpNotFound();
            }
            return View(agentEntity);
        }

        // GET: Agents/Create
        [Mvc.Activity("edit-agents"), Route(Actions.Default)]
        public ActionResult Create()
        {
            ViewBag.Client = new SelectList(DbContext.Segments, "Client", "ApplicationName");
            return View();
        }

        // POST: Agents/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "AgentId,AgentSecret,ApplicationName,Client,MachineName,ProcessId,TasksCompleted,TasksPending,TasksRunning")] AgentEntity agentEntity)
        {
            if (ModelState.IsValid)
            {
                agentEntity.AgentId = Guid.NewGuid();
                DbContext.Agents.Add(agentEntity);
                await DbContext.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            //ViewBag.Client = new SelectList(db.Clients, "Client", "ApplicationName", agentEntity.Client);
            return View(agentEntity);
        }

        // GET: Agents/Edit/5
        public async Task<ActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AgentEntity agentEntity = await DbContext.Agents.FindAsync(id);
            if (agentEntity == null)
            {
                return HttpNotFound();
            }
            //ViewBag.Client = new SelectList(db.Clients, "Client", "ApplicationName", agentEntity.Client);
            return View(agentEntity);
        }

        // POST: Agents/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "AgentId,AgentSecret,ApplicationName,Client,MachineName,ProcessId,TasksCompleted,TasksPending,TasksRunning")] AgentEntity agentEntity)
        {
            if (ModelState.IsValid)
            {
                DbContext.Entry(agentEntity).State = EntityState.Modified;
                await DbContext.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            //ViewBag.Client = new SelectList(db.Clients, "Client", "ApplicationName", agentEntity.Client);
            return View(agentEntity);
        }

        // GET: Agents/Delete/5
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AgentEntity agentEntity = await DbContext.Agents.FindAsync(id);
            if (agentEntity == null)
            {
                return HttpNotFound();
            }
            return View(agentEntity);
        }

        // POST: Agents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            AgentEntity agentEntity = await DbContext.Agents.FindAsync(id);
            DbContext.Agents.Remove(agentEntity);
            await DbContext.SaveChangesAsync();
            return RedirectToAction("Index");
        }
    }
}
