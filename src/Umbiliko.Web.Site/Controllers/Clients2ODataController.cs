﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;
using System.Web.Http.OData.Routing;
using System.Web.OData.Routing;
using Microsoft.Data.OData;

namespace Umbiliko.Web.Controllers
{
    [ODataRouting(), ODataRoutePrefix("clients")]
    public class Clients2ODataController : ODataController
    {
        private static ODataValidationSettings _validationSettings = new ODataValidationSettings();

        // GET: odata/Clients
        public IHttpActionResult GetClients(ODataQueryOptions<Client> queryOptions)
        {
            // validate the query.
            try
            {
                queryOptions.Validate(_validationSettings);
            }
            catch (ODataException ex)
            {
                return BadRequest(ex.Message);
            }

            // return Ok<IEnumerable<Client>>(clients);
            return StatusCode(HttpStatusCode.NotImplemented);
        }

        // GET: odata/Clients(5)
        public IHttpActionResult GetClient([FromODataUri] string key, ODataQueryOptions<Client> queryOptions)
        {
            // validate the query.
            try
            {
                queryOptions.Validate(_validationSettings);
            }
            catch (ODataException ex)
            {
                return BadRequest(ex.Message);
            }

            // return Ok<Client>(client);
            return StatusCode(HttpStatusCode.NotImplemented);
        }

        // PUT: odata/Clients(5)
        public IHttpActionResult Put([FromODataUri] string key, Delta<Client> delta)
        {
            Validate(delta.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // TODO: Get the entity here.

            // delta.Put(client);

            // TODO: Save the patched entity.

            // return Updated(client);
            return StatusCode(HttpStatusCode.NotImplemented);
        }

        // POST: odata/Clients
        public IHttpActionResult Post(Client client)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // TODO: Add create logic here.

            // return Created(client);
            return StatusCode(HttpStatusCode.NotImplemented);
        }

        // PATCH: odata/Clients(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] string key, Delta<Client> delta)
        {
            Validate(delta.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // TODO: Get the entity here.

            // delta.Patch(client);

            // TODO: Save the patched entity.

            // return Updated(client);
            return StatusCode(HttpStatusCode.NotImplemented);
        }

        // DELETE: odata/Clients(5)
        public IHttpActionResult Delete([FromODataUri] string key)
        {
            // TODO: Add delete logic here.

            // return StatusCode(HttpStatusCode.NoContent);
            return StatusCode(HttpStatusCode.NotImplemented);
        }
    }
}
