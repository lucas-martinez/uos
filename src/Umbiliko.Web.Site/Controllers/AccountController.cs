﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Uos.Web.Controllers
{
    using Assertion;
    using Constants;
    using Contracts;
    using Entities;
    using Models;
    using Models.Account;
    using Results;
    using Services;
    using Providers;
    using Utilities;

    [Authorize]
    [Mvc.Feature(Features.Authentication), RoutePrefix("account")]
    public class AccountController : Mvc.EntityController<AccountEntity, int>
    {
        public AccountController()
        {
        }

        public SignInManager<SessionInfo, string> SignInManager
        {
            get { return Authentication.SignInManager; }
        }

        public UserManager<SessionInfo, string> UserManager
        {
            get { return Authentication.UserManager; }
        }

        //
        // GET: /account
        [HttpGet, Route(Actions.Default)]
        public async Task<System.Web.Mvc.ActionResult> Index(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.SetTwoFactorSuccess ? "Your two-factor authentication provider has been set."
                : message == ManageMessageId.Error ? "An error has occurred."
                : message == ManageMessageId.AddPhoneSuccess ? "Your phone number was added."
                : message == ManageMessageId.RemovePhoneSuccess ? "Your phone number was removed."
                : "";

            var userId = User.Identity.GetUserId();
            var model = new MyAccountViewModel
            {
                BrowserRemembered = await Authentication.Manager.TwoFactorBrowserRememberedAsync(userId),
                Email = await UserManager.GetEmailAsync(userId),
                //EmailConfirmed = await UserManager.GetEmailConformedAsync(userId),
                HasPassword = HasPassword(),
                Logins = await UserManager.GetLoginsAsync(userId),
                PhoneNumber = await UserManager.GetPhoneNumberAsync(userId),
                //PhoneNumberConfirmed = await UserManager.GetPhoneNumberConfirmedAsync(userId),
                TwoFactor = await UserManager.GetTwoFactorEnabledAsync(userId),
                UserName = User.Identity.Name
            };

            return View(model);
        }

        //
        // GET: /account/add-phone-number
        [HttpGet, Route("add-phone-number")]
        public ActionResult AddPhoneNumber()
        {
            return View();
        }

        //
        // POST: /account/add-phone-number
        [HttpPost, Route("add-phone-number")/*, ValidateAntiForgeryToken*/]
        public async Task<System.Web.Mvc.ActionResult> AddPhoneNumber(AddPhoneNumberViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            // Generate the token and send it
            var code = await UserManager.GenerateChangePhoneNumberTokenAsync(User.Identity.GetUserId(), model.Number);
            if (UserManager.SmsService != null)
            {
                var message = new IdentityMessage
                {
                    Destination = model.Number,
                    Body = "Your security code is: " + code
                };
                await UserManager.SmsService.SendAsync(message);
            }
            return RedirectToAction("VerifyPhoneNumber", new { PhoneNumber = model.Number });
        }

        //
        // GET: /account/change-password
        [HttpGet, Route("change-password")]
        public ActionResult ChangePassword()
        {
            return View();
        }

        //
        // POST: /account/change-password
        [HttpPost, Route("change-password")/*, ValidateAntiForgeryToken*/]
        public async Task<System.Web.Mvc.ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                return RedirectToAction("Index", new { Message = ManageMessageId.ChangePasswordSuccess });
            }
            AddErrors(result);
            return View(model);
        }

        //
        // POST: /account/enable-two-factor-Authentication
        [HttpPost, Route("enable-two-factor-Authentication")/*, ValidateAntiForgeryToken*/]
        public async Task<System.Web.Mvc.ActionResult> EnableTwoFactorAuthentication()
        {
            await UserManager.SetTwoFactorEnabledAsync(User.Identity.GetUserId(), true);
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user != null)
            {
                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            }
            return RedirectToAction("Index", "Account");
        }

        //
        // GET: /account/enter
        [AllowAnonymous, HttpGet, Route("enter")]
        public ActionResult Enter(string returnUrl)
        {
            ViewBag.LoginProviders = HttpContext.GetOwinContext().Authentication.GetExternalAuthenticationTypes().ToList();
            ViewBag.ReturnUrl = returnUrl;

            XFrame();

            return PartialView(new EnterPostModel { WithIdentity = true, Origin = Request.Url.GetDomainName() });
        }

        //
        // POST: /account/enter
        [AllowAnonymous, HttpPost, Route("enter")/*, ValidateAntiForgeryToken*/]
        public async Task<System.Web.Mvc.ActionResult> Enter(EnterPostModel model, string returnUrl)
        {
            var providers = HttpContext.GetOwinContext().Authentication.GetExternalAuthenticationTypes().ToList();

            ViewBag.LoginProviders = HttpContext.GetOwinContext().Authentication.GetExternalAuthenticationTypes().ToList();

            if (!ModelState.IsValid)
            {
                XFrame();

                return PartialView(model);
            }

            if (!string.IsNullOrEmpty(model.Provider))
            {
                var provider = providers.SingleOrDefault(e => e.AuthenticationType == model.Provider);
                var properties = AuthenticationProvider.Current.GetAuthenticationProperties(Url.Action("ExternalLoginCallback", new { ReturnUrl = returnUrl }));

                // Request a redirect to the external login provider
                return new ChallengeResult(model.Provider, properties);
            }

            var result = Result.Ok<EnterResponses>(EnterResponses.Allow);// await Authentication.Enter(model);

            foreach (var msg in result.Messages)
            {
                ModelState.AddModelError("", msg);
            }

            switch (result.Value)
            {
                case EnterResponses.Allow:
                    // This doesn't count login failures towards account lockout
                    // To enable password failures to trigger account lockout, change to shouldLockout: true
                    //var status = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);

                    //SignInManager.Authentication.Authentication.SignIn(new System.Security.Claims.ClaimsIdentity());

                    /*switch (status)
                    {
                        case SignInStatus.Success:
                            return View("Login"); // RedirectToLocal(returnUrl);
                        case SignInStatus.LockedOut:
                            return View("Lockout");
                        case SignInStatus.RequiresVerification:
                            return PartialView(model);//  RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                        case SignInStatus.Failure:
                        default:
                            ModelState.AddModelError("", "Invalid login attempt.");
                            break;
                    
                    }*/
                    return PartialView("Login", new LoginViewModel { Token = model.Token, ReturnUrl = returnUrl }); // RedirectToLocal(returnUrl);

                case EnterResponses.LockedOut:
                    return PartialView("Lockout");

                default:
                    break;
            }

            XFrame();

            return PartialView(model);
        }

        //
        // POST: /account/disable-two-factor-authentication
        [HttpPost, Route("disable-two-factor-authentication") ValidateAntiForgeryToken]
        public async Task<System.Web.Mvc.ActionResult> DisableTwoFactorAuthentication()
        {
            await UserManager.SetTwoFactorEnabledAsync(User.Identity.GetUserId(), false);
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user != null)
            {
                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            }
            return RedirectToAction("Index", "Account");
        }

        //
        // GET: /account/login-fail
        [AllowAnonymous, HttpGet, Route("login-fail")]
        public ActionResult LoginFail(string returnUrl)
        {
            return View();
        }

        //
        // GET: /account/manage-logins
        [HttpGet, Route("manage-logins")]
        public async Task<System.Web.Mvc.ActionResult> ManageLogins(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : message == ManageMessageId.Error ? "An error has occurred."
                : "";
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user == null)
            {
                return View("Error");
            }
            var userLogins = await UserManager.GetLoginsAsync(User.Identity.GetUserId());
            var otherLogins = Authentication.Manager.GetExternalAuthenticationTypes().Where(auth => userLogins.All(ul => auth.AuthenticationType != ul.LoginProvider)).ToList();
            ViewBag.ShowRemoveButton = userLogins.Count > 1;
            return View(new ManageLoginsViewModel
            {
                CurrentLogins = userLogins,
                OtherLogins = otherLogins
            });
        }

        //
        // GET: /account/menu
        [Route("menu")]
        public PartialViewResult Menu()
        {
            return PartialView();
        }

        //
        // GET: /account/register
        [AllowAnonymous, HttpGet, Route("register")]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /account/register
        [AllowAnonymous, HttpPost, Route("register")/*, ValidateAntiForgeryToken*/]
        public async Task<System.Web.Mvc.ActionResult> Register(RegisterViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Account");
            }

            if (ModelState.IsValid)
            {
                IdentityResult result;

                var data = new UserInfo
                {
                    Name = model.UserName,
                    Email = model.Email
                };
                
                if (string.IsNullOrEmpty(model.Password))
                {
                    result = await UserManager.CreateAsync(data);
                }
                else
                {
                    result = await UserManager.CreateAsync(data, model.Password);
                }

                // Get the information about the user from the external login provider
                var info = await Authentication.Manager.GetExternalLoginInfoAsync();

                if (result.Succeeded && info != null)
                {
                    // Link external login provider to account
                    result = await UserManager.AddLoginAsync(data.Name, info.Login);
                }

                if (result.Succeeded)
                {
                    await SignInManager.SignInAsync(data, isPersistent: false, rememberBrowser: false);

                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    string code = await UserManager.GenerateEmailConfirmationTokenAsync(data.Name);
                    var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = data.Name, code = code }, protocol: Request.Url.Scheme);
                    await UserManager.SendEmailAsync(data.Name, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                    return string.IsNullOrEmpty(returnUrl) ? RedirectToAction("Index", "Account") : RedirectToLocal(returnUrl);
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // POST: /account/remove-login
        [HttpPost, Route("remove-login")/*, ValidateAntiForgeryToken*/]
        public async Task<System.Web.Mvc.ActionResult> RemoveLogin(string loginProvider, string providerKey)
        {
            ManageMessageId? message;
            var result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(), new UserLoginInfo(loginProvider, providerKey));
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                message = ManageMessageId.RemoveLoginSuccess;
            }
            else
            {
                message = ManageMessageId.Error;
            }
            return RedirectToAction("ManageLogins", new { Message = message });
        }

        //
        // GET: /account/remove-phone-number
        [HttpGet, Route("remove-phone-number")]
        public async Task<System.Web.Mvc.ActionResult> RemovePhoneNumber()
        {
            var result = await UserManager.SetPhoneNumberAsync(User.Identity.GetUserId(), null);
            if (!result.Succeeded)
            {
                return RedirectToAction("Index", new { Message = ManageMessageId.Error });
            }
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user != null)
            {
                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            }
            return RedirectToAction("Index", new { Message = ManageMessageId.RemovePhoneSuccess });
        }

        //
        // GET: /account/set-password
        [HttpGet, Route("set-password")]
        public ActionResult SetPassword()
        {
            return View();
        }

        //
        // POST: /account/set-password
        [HttpPost, Route("set-password")/*, ValidateAntiForgeryToken*/]
        public async Task<System.Web.Mvc.ActionResult> SetPassword(SetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);
                if (result.Succeeded)
                {
                    var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                    if (user != null)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                    }
                    return RedirectToAction("Index", new { Message = ManageMessageId.SetPasswordSuccess });
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /account/verify-code
        [AllowAnonymous, HttpGet, Route("verify-code")]
        public async Task<System.Web.Mvc.ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /account/verify-code
        [AllowAnonymous, HttpPost, Route("verify-code")/*, ValidateAntiForgeryToken*/]
        public async Task<System.Web.Mvc.ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        //
        // GET: /account/verify-phone-number
        [HttpGet, Route("verify-phone-number")]
        public async Task<System.Web.Mvc.ActionResult> VerifyPhoneNumber(string phoneNumber)
        {
            var code = await UserManager.GenerateChangePhoneNumberTokenAsync(User.Identity.GetUserId(), phoneNumber);
            // Send an SMS through the SMS provider to verify the phone number
            return phoneNumber == null ? View("Error") : View(new VerifyPhoneNumberViewModel { PhoneNumber = phoneNumber });
        }

        //
        // POST: /account/verify-phone-number
        [HttpPost, Route("verify-phone-number")/*, ValidateAntiForgeryToken*/]
        public async Task<System.Web.Mvc.ActionResult> VerifyPhoneNumber(VerifyPhoneNumberViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await UserManager.ChangePhoneNumberAsync(User.Identity.GetUserId(), model.PhoneNumber, model.Code);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                return RedirectToAction("Index", new { Message = ManageMessageId.AddPhoneSuccess });
            }
            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "Failed to verify phone");
            return View(model);
        }

        //
        // POST: /account/external-login-confirmation
        //[Deprecated]
        [AllowAnonymous, HttpPost, Route("external-login-confirmation")/*, ValidateAntiForgeryToken*/]
        public async Task<System.Web.Mvc.ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Account");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await Authentication.Manager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }

                var user = new UserInfo
                {
                    Name = model.EmailAddress,
                    Email = model.EmailAddress
                };
                
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Name, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // GET: /account/confirm-email
        [AllowAnonymous, HttpGet, Route("confirm-email")]
        public async Task<System.Web.Mvc.ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /account/forgot-password
        [AllowAnonymous, HttpGet, Route("forgot-password")]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /account/forgot-password
        [AllowAnonymous, HttpPost, Route("forgot-password")/*, ValidateAntiForgeryToken*/]
        public async Task<System.Web.Mvc.ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var session = await UserManager.FindByNameAsync(model.Email);
                if (session == null || !(await UserManager.IsEmailConfirmedAsync(session.User.Name)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }

                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                // string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                // var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);		
                // await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                // return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /account/forgot-password-confirmation
        [AllowAnonymous, HttpGet, Route("forgot-password-confirmation")]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /account/reset-password
        [AllowAnonymous, HttpGet, Route("reset-password")]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /account/reset-password
        [AllowAnonymous, HttpPost, Route("reset-password")/*, ValidateAntiForgeryToken*/]
        public async Task<System.Web.Mvc.ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var session = await UserManager.FindByNameAsync(model.Email);
            if (session == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(session.User.Name, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /account/reset-password-confirmation
        [AllowAnonymous, HttpGet, Route("reset-password-confirmation")]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /account/send-code
        [AllowAnonymous, HttpGet, Route("send-code")]
        public async Task<System.Web.Mvc.ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /account/send-code
        [AllowAnonymous, HttpPost, Route("send-code")/*, ValidateAntiForgeryToken*/]
        public async Task<System.Web.Mvc.ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /account/external-login-callback
        [AllowAnonymous, HttpGet, Route("external-login-callback")]
        public async Task<System.Web.Mvc.ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await Authentication.Manager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    if (string.IsNullOrEmpty(returnUrl))
                    {
                        return RedirectToAction("Index", "Account");
                    }
                    else
                    {
                        return RedirectToLocal(returnUrl);
                    }
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;

                    var model = new RegisterViewModel
                    {
                        UserName = loginInfo.DefaultUserName,
                        Email = loginInfo.Email
                    };

                    return View("Register", model);

                    //return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /account/logout
        [HttpPost, Route("logout")/*, ValidateAntiForgeryToken*/]
        public ActionResult Logout()
        {
            Authentication.Manager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /account/external-login-failure
        [AllowAnonymous, HttpGet, Route("external-login-failure")]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        //
        // GET: /account/widget
        [AllowAnonymous, HttpGet, Route("widget")]
        public ActionResult Widget()
        {
            return PartialView();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }

            base.Dispose(disposing);
        }

        #region Helpers



        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            return UserManager.HasPassword(User.Identity.GetUserId());
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }


        #endregion
    }
}