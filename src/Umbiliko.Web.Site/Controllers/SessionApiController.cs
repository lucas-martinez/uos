﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace Uos.Web.Controllers
{
    using Constants;
    using Contracts;

    [Http.Feature(Features.Authentication), RoutePrefix("api/session")]
    public class SessionApiController : ApiController
    {
        // POST: api/session/login
        [HttpPost, Http.Activity("login", Level = AccessLevels.Public), ResponseType(typeof(UserInfo)), Route("login")]
        public IHttpActionResult Login(LoginRequest request)
        {
            return Ok();
        }

        // POST: api/session/logout
        [HttpPost, Http.Activity("logout"), ResponseType(typeof(bool)), Route("logout")]
        public IHttpActionResult Logout(LoginRequest request)
        {
            return Ok();
        }
    }
}
