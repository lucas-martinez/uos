﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;
using System.Web.Http.OData.Routing;
using System.Web.OData.Routing;
using Microsoft.Data.OData;

namespace Umbiliko.Web.Controllers
{
    [ODataRouting, ODataRoutePrefix("agents")]
    public class Agents2ODataController : ODataController
    {
        private static ODataValidationSettings _validationSettings = new ODataValidationSettings();

        // GET: odata/agents
        [EnableQuery, HttpGet, ODataRoute()]
        public IHttpActionResult Get(ODataQueryOptions<Agent> queryOptions)
        {
            // validate the query.
            try
            {
                queryOptions.Validate(_validationSettings);
            }
            catch (ODataException ex)
            {
                return BadRequest(ex.Message);
            }

            // return Ok<IEnumerable<Agent>>(agents);
            return StatusCode(HttpStatusCode.NotImplemented);
        }


        // GET: odata/agents(5)
        [HttpGet, ODataRoute("({agentId})")]
        public IHttpActionResult Get([FromODataUri] Guid agentId, ODataQueryOptions<Agent> queryOptions)
        {
            // validate the query.
            try
            {
                queryOptions.Validate(_validationSettings);
            }
            catch (ODataException ex)
            {
                return BadRequest(ex.Message);
            }

            // return Ok<Agent>(agent);
            return StatusCode(HttpStatusCode.NotImplemented);
        }

        // PUT: odata/agents(5)
        [ODataRoute("({agentId})")]
        public IHttpActionResult Put([FromODataUri] Guid agentId, Delta<Agent> delta)
        {
            Validate(delta.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // TODO: Get the entity here.

            // delta.Put(agent);

            // TODO: Save the patched entity.

            // return Updated(agent);
            return StatusCode(HttpStatusCode.NotImplemented);
        }

        // POST: odata/agents
        public IHttpActionResult Post(Agent agent)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // TODO: Add create logic here.

            // return Created(agent);
            return StatusCode(HttpStatusCode.NotImplemented);
        }

        // PATCH: odata/agents(5)
        [AcceptVerbs("PATCH", "MERGE"), ODataRoute("({agentId})")]
        public IHttpActionResult Patch([FromODataUri] System.Guid agentId, Delta<Agent> delta)
        {
            Validate(delta.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // TODO: Get the entity here.

            // delta.Patch(agent);

            // TODO: Save the patched entity.

            // return Updated(agent);
            return StatusCode(HttpStatusCode.NotImplemented);
        }

        // DELETE: odata/agents(5)
        [ODataRoute("({agentId})")]
        public IHttpActionResult Delete([FromODataUri] Guid agentId)
        {
            // TODO: Add delete logic here.

            // return StatusCode(HttpStatusCode.NoContent);
            return StatusCode(HttpStatusCode.NotImplemented);
        }
    }
}
