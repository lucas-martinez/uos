﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Umbiliko.Web.Entities;

namespace Umbiliko.Web.Controllers
{
    using Models;

    [RoutePrefix("api/sites")]
    public class Clients2ApiController : ApiController
    {
        private UmbilikoDbContext DbContext = new UmbilikoDbContext();

        #region - api/clients -

        // GET: api/clients
        [HttpGet, Route("")]
        public IQueryable<ClientRowModel> GetClients()
        {
            return DbContext.Clients.Select(e => new ClientRowModel
            {
            });
        }

        // GET: api/clients/5
        [HttpGet, ResponseType(typeof(ClientViewModel)), Route("{clientCode}")]
        public async Task<IHttpActionResult> GetClient(string clientCode)
        {
            var clientEntity = await DbContext.Clients.FindAsync(clientCode);

            if (clientEntity == null)
            {
                return NotFound();
            }

            return Ok(clientEntity);
        }

        // PUT: api/clients/5
        [HttpPut, ResponseType(typeof(ClientViewModel)), Route("{clientCode}")]
        public async Task<IHttpActionResult> PutClient(string clientCode, ClientPutModel model)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            //if (clientCode != model.ClientCode) return BadRequest();


            //DbContext.Entry(model).State = EntityState.Modified;

            try
            {
                await ClientsService.UpdateClientAsync(clientCode, (client) => { });
                //await DbContext.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClientEntityExists(clientCode))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/clients
        [HttpPost, ResponseType(typeof(ClientViewModel)), Route("")]
        public async Task<IHttpActionResult> PostClient(ClientPostModel model)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            //DbContext.Clients.Add(model);

            try
            {
                await ClientsService.CreateClientAsync(model.ClientCode, (client) => { });

                await DbContext.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ClientEntityExists(model.ClientCode))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = model.ClientCode }, model);
        }

        // DELETE: api/clients/5
        [HttpDelete, ResponseType(typeof(ClientViewModel)), Route("{clientCode}")]
        public async Task<IHttpActionResult> DeleteClient(string clientCode)
        {
            ClientEntity clientEntity = await DbContext.Clients.FindAsync(clientCode);

            if (clientEntity == null)
            {
                return NotFound();
            }

            DbContext.Clients.Remove(clientEntity);
            await DbContext.SaveChangesAsync();

            return Ok(clientEntity);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                DbContext.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ClientEntityExists(string clientCode)
        {
            return DbContext.Clients.Count(e => e.ClientCode == clientCode) > 0;
        }
    }
}