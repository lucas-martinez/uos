﻿using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Umbiliko.Web.Mvc
{
    using Contracts;
    using Inquiry;
    
    public abstract class UserEntityController<TEntity, TIdentifier> : EntityController<TEntity, TIdentifier>
        where TEntity : class, IUserEntity<TIdentifier>
        where TIdentifier : IComparable<TIdentifier>, IEquatable<TIdentifier>
    {
        protected IQueryable<TEntity> Entities
        {
            get { return User.Select<TEntity, TIdentifier>(); }
        }

        public override Expression<Func<TEntity, bool>> Predicate
        {
            get
            {
                string name = User;

                return base.Predicate.And(e => e.UserName == name);
            }
        }

        protected override async Task<ActionResult> InsertAsync<TModel, TViewModel>(DataSourceRequest request, IEnumerable<TModel> models, Func<TEntity, TViewModel> toViewModel, Action<TEntity, TModel> updater)
        {
            if (!ModelState.IsValid)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var entities = new List<TEntity>();

            foreach (var model in models)
            {
                var entity = DbSet.Create();

                entity.User = User;
                entity.UserName = User;

                updater(entity, model);

                DbSet.Add(entity);

                entities.Add(entity);
            }

            return await SaveChangesAsync(() =>
            {
                Response.StatusCode = (int)HttpStatusCode.Created;
                Response.StatusDescription = Request.RawUrl;

                return Json(entities.ToDataSourceResult(request, toViewModel));
            });
        }

        protected override IQueryable<TEntity> Select(TIdentifier identifier)
        {
            return User.Select<TEntity, TIdentifier>(identifier);
        }

        protected override IQueryable<TEntity> Select(List<TIdentifier> identifiers)
        {
            return User.Select<TEntity, TIdentifier>(identifiers);
        }
    }
}