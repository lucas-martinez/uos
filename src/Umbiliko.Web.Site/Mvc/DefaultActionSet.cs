﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Uos.Web.Mvc
{
    using Controllers;
    
    public sealed class DefaultActionSet : UmbilikoActionSet<DefaultController>
    {
        ActivityInfo _dashboard;
        ActivityInfo _index;
        ActivityInfo _intro;
        ActivityInfo _login;

        private DefaultActionSet(UrlHelper urlHelper)
            : base(urlHelper)
        {
        }

        public ActivityInfo Dashboard
        {
            get { return _dashboard ?? (_dashboard = ActivityInfo.FromAction<DefaultController>(controller => controller.Dashboard(), this)); }
        }

        public ActivityInfo Intro
        {
            get { return _intro ?? (_intro = ActivityInfo.FromAction<DefaultController>(controller => controller.Intro(), this)); }
        }

        public ActivityInfo Index

        {
            get { return _index ?? (_index = ActivityInfo.FromAction<DefaultController>(controller => controller.Index(), this)); }
        }

        public ActivityInfo Login
        {
            get { return _login ?? (_login = ActivityInfo.FromAction<DefaultController>(controller => controller.Login(null, null), this)); }
        }

        public static implicit operator DefaultActionSet(UrlHelper urlHelper)
        {
            return new DefaultActionSet(urlHelper);
        }
    }
}
