﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Mvc
{
    public class WidgetSet
    {
        ActivityInfo _contact;
        ActivityInfo _enter;

        public WidgetSet(ActivityInfo contact, ActivityInfo enter)
        {
            _contact = contact;
            _enter = enter;
        }

        public ActivityInfo Contact
        {
            get { return _contact; }
        }

        public ActivityInfo Enter
        {
            get { return _enter; }
        }
    }
}
