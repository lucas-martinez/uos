﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Uos.Web.Mvc
{
    using Contracts;
    using Entities;
    using Inquiry;
    using Providers;

    public abstract class EntityController : Mvc.UserIdentityController
    {
        DbContext _dbContext;
        IAuthenticationContext _authentication;

        public virtual DbContext DbContext
        {
            get { return _dbContext ?? (_dbContext = Request.GetOwinContext().Get<UmbilikoDbContext>()); }
        }

        public IAuthenticationContext Authentication
        {
            get { return _authentication ?? (_authentication = Request.GetOwinContext().Get<IAuthenticationContext>() ?? AuthenticationProvider.Create(System.Web.HttpContext.Current)); }
        }

        protected async Task<System.Web.Mvc.ActionResult> SaveChangesAsync(Func<System.Web.Mvc.ActionResult> success)
        {
            var result = await DbContext.TrySaveChangesAsync();

            if (!result.Success) return new HttpStatusCodeResult(HttpStatusCode.BadRequest, string.Join(". ", result.Messages));

            return success();
        }
    }

    public abstract class EntityController<TEntity, TIdentifier> : EntityController
        where TEntity : class, IEntity<TIdentifier>
        where TIdentifier : IComparable<TIdentifier>, IEquatable<TIdentifier>
    {
        DbSet<TEntity> _dbSet;
        protected EntityController()
        {
            ViewBag.PageButtonCount = 5;
            ViewBag.PageSize = 25;
            ViewBag.PageSizes = new[] { 10, 25, 50, 100, 500, 1000 };
        }

        protected DbSet<TEntity> DbSet
        {
            get { return _dbSet ?? (_dbSet = DbContext.Set<TEntity>()); }
        }

        public virtual Expression<Func<TEntity, bool>> Predicate
        {
            get { return PredicateBuilder.True<TEntity>(); }
        }

        protected virtual async Task<System.Web.Mvc.ActionResult> DeleteAsync<TModel, TViewModel>(DataSourceRequest request, IEnumerable<TModel> models, Func<TModel, TIdentifier> toIdentifier, Func<TEntity, TViewModel> toViewModel, Func<IEnumerable<TModel>, string> missingFormatter = null)
        {
            if (!ModelState.IsValid)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var session = Authentication.CurrentSession;

            if (session.Account == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var account = await AccountContext.FindAsync(DbContext, session.User.Name, session.Account.CompanyCode, session.Account.SegmentCode);

            var entities = await Select(models.Select(toIdentifier).ToList()).ToListAsync();

            var missing = new List<TModel>();
            
            foreach (var model in models)
            {
                var identifier = toIdentifier(model);

                var entity = entities.SingleOrDefault(e => identifier.Equals(e.Id));

                if (entity != null)
                {
                    DbSet.Remove(entity);
                }
                else
                {
                    missing.Add(model);
                }
            }

            return await SaveChangesAsync(() =>
            {
                if (missing.Count > 0)
                {
                    Func<IEnumerable<TModel>, string> defaultFormatter = (items) => string.Format("One or more entities were not found: [{0}]", string.Join(", ", items.Select(item => toIdentifier(item))));
                    Response.StatusCode = (int)HttpStatusCode.Found;
                    Response.StatusDescription = (missingFormatter ?? defaultFormatter)(missing);
                }

                return Json(entities.ToDataSourceResult(request, toViewModel));
            });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                DbContext.Dispose();
            }
            base.Dispose(disposing);
        }

        /*protected async Task<JsonResult> ListAsync(IQueryable<TEntity> source, Func<TEntity, string> selector)
        {
            var results = await source.OrderBy(selector).Select(selector).AsQueriable().ToListAsync();

            return Json(results);
        }*/
        
        protected virtual async Task<System.Web.Mvc.ActionResult> InsertAsync<TModel, TViewModel>(DataSourceRequest request, IEnumerable<TModel> models, Func<TEntity, TViewModel> toViewModel, Action<TEntity, TModel> updater)
        {
            if (!ModelState.IsValid)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var entities = new List<TEntity>();

            foreach (var model in models)
            {
                var entity = DbSet.Create();

                updater(entity, model);

                DbSet.Add(entity);

                entities.Add(entity);
            }

            return await SaveChangesAsync(() =>
            {
                Response.StatusCode = (int)HttpStatusCode.Created;
                Response.StatusDescription = Request.RawUrl;
                
                return Json(entities.ToDataSourceResult(request, toViewModel));
            });
        }
        
        protected async Task<System.Web.Mvc.ActionResult> UpdateAsync<TModel, TViewModel>(DataSourceRequest request, IEnumerable<TModel> models, Func<TModel, TIdentifier> toIdentifier, Func<TEntity, TViewModel> toViewModel, Action<TEntity, TModel> updater)
        {
            if (!ModelState.IsValid)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var session = Authentication.CurrentSession;

            if (session.Account == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var account = await AccountContext.FindAsync(DbContext, session.User.Name, session.Account.CompanyCode, session.Account.SegmentCode);

            var entities = await Select(models.Select(toIdentifier).ToList()).ToListAsync();

            var missing = new List<TModel>();

            foreach (var model in models)
            {
                var identifier = toIdentifier(model);

                var entity = entities.SingleOrDefault(e => identifier.Equals(e.Id));

                if (entity != null)
                {
                    updater(entity, model);
                }
                else
                {
                    missing.Add(model);
                }
            }

            return await SaveChangesAsync(() =>
            {
                return Json(entities.ToDataSourceResult(request, toViewModel));
            });
        }

        protected virtual IQueryable<TEntity> Select(TIdentifier identifier)
        {
            return DbSet.Select(identifier);
        }

        protected virtual IQueryable<TEntity> Select(List<TIdentifier> identifiers)
        {
            return DbSet.Select(identifiers);
        }

        protected virtual async Task<System.Web.Mvc.ActionResult> SelectAsync<TRowModel>(DataSourceRequest request, Expression<Func<TEntity, bool>> predicate, Func<TEntity, TRowModel> toRowModel, params string[] defaultSort)
        {
            var entities = await DbSet.AsExpandableAsync().Where(predicate).AsExpandableAsync().ToListAsync();
            
            object result = request != null ? entities.ToDataSourceResult(request.DefaultSort(defaultSort), toRowModel) : (object)entities.Select(e => toRowModel(e)).ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);

            var error = string.Format("{0} [{1}]", 
                filterContext.Exception.Message, 
                filterContext.RouteData.Values.Select(v => string.Join(", ", string.Format("{0}: \"{1}\"", v.Key, v.Value))));

            System.Diagnostics.Trace.TraceError(error);
        }
    }
}