﻿using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Umbiliko.Web.Mvc
{
    using Contracts;
    using Inquiry;
    
    public abstract class CompanyEntityController<TEntity, TIdentifier> : EntityController<TEntity, TIdentifier>
        where TEntity : class, ICompanyEntity<TIdentifier>
        where TIdentifier : IComparable<TIdentifier>, IEquatable<TIdentifier>
    {
        protected async Task<IQueryable<TEntity>> GetEntitiesAsync()
        {
            var userData = Authentication.User;

            var account = await AccountContext.FindAsync(Authentication.DbContext, userData);
            
            return account.Company.Select<TEntity, TIdentifier>();
        }

        public override Expression<Func<TEntity, bool>> Predicate
        {
            get
            {
                var userData = Authentication.User;

                var account = await AccountContext.FindAsync(Authentication.DbContext, userData);
                
                string trademark = account.Company;

                return base.Predicate.And(e => e.CompanyCode == trademark);
            }
        }

        protected override async Task<ActionResult> InsertAsync<TModel, TViewModel>(DataSourceRequest request, IEnumerable<TModel> models, Func<TEntity, TViewModel> toViewModel, Action<TEntity, TModel> updater)
        {
            if (!ModelState.IsValid)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var entities = new List<TEntity>();

            var userData = Authentication.User;

            var account = await AccountContext.FindAsync(Authentication.DbContext, userData);

            foreach (var model in models)
            {
                var entity = DbSet.Create();

                entity.Company = account.Company;
                entity.CompanyCode = account.Company;

                updater(entity, model);

                DbSet.Add(entity);

                entities.Add(entity);
            }

            return await SaveChangesAsync(() =>
            {
                Response.StatusCode = (int)HttpStatusCode.Created;
                Response.StatusDescription = Request.RawUrl;

                return Json(entities.ToDataSourceResult(request, toViewModel));
            });
        }

        protected IQueryable<TEntity> Select(CompanyContext company, TIdentifier identifier)
        {
            return company.Select<TEntity, TIdentifier>(identifier);
        }

        protected IQueryable<TEntity> Select(CompanyContext company, List<TIdentifier> identifiers)
        {
            return company.Select<TEntity, TIdentifier>(identifiers);
        }
    }
}