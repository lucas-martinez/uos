﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Uos.Web.Mvc
{
    using Areas.Domain.Controllers;
    using Areas.Authentication.Controllers;
    using Areas.Authorization.Controllers;
    using Areas.Configuration.Controllers;
    using Areas.Production.Controllers;
    using Controllers;

    public class ActivityHelper
    {
        ActionSet _accounts;
        ActionSet _activities;
        ActionSet _charges;
        ActionSet _companies;
        ActionSet _consumers;
        ActionSet _customers;
        ActionSet _domains;
        ActionSet _features;
        ActionSet _instances;
        ActionSet _logins;
        ActionSet _myAccount;
        ActionSet _myCompany;
        ActionSet _products;
        ActionSet _roles;
        ActionSet _search;
        ActionSet _segments;
        ActionSet _sessions;
        ActionSet _subscriptions;
        ActionSet _suppliers;
        ActionSet _users;
        WidgetSet _widgets;

        readonly UrlHelper _urlHelper;

        public ActivityHelper(UrlHelper urlHelper)
        {
            _urlHelper = urlHelper;
        }

        

        public ActionSet Accounts
        {
            get
            {
                return _accounts ?? (_accounts = new ActionSet(
                    create: ActivityInfo.FromAction<AccountsController>(controller => controller.PostAccounts(null, null), this),
                    destroy: ActivityInfo.FromAction<AccountsController>(controller => controller.DeleteAccounts(null, null), this),
                    display: ActivityInfo.FromAction<AccountsController>(controller => controller.Index(), this),
                    read: ActivityInfo.FromAction<AccountsController>(controller => controller.GetAccounts(null, null, null), this),
                    update: ActivityInfo.FromAction<AccountsController>(controller => controller.PutAccounts(null, null), this)));
            }
        }

        public ActionSet Activities
        {
            get
            {
                return _activities ?? (_activities = new ActionSet(
                    create: ActivityInfo.FromAction<ActivitiesController>(controller => controller.PostActivities(null, null), this),
                    destroy: ActivityInfo.FromAction<ActivitiesController>(controller => controller.DeleteActivities(null, null), this),
                    read: ActivityInfo.FromAction<ActivitiesController>(controller => controller.GetActivities(null, null, null), this),
                    update: ActivityInfo.FromAction<ActivitiesController>(controller => controller.PutActivities(null, null), this)));
            }
        }

        public ActionSet Charges
        {
            get
            {
                return _charges ?? (_charges = new ActionSet(
                    display: ActivityInfo.FromAction<ChargesController>(controller => controller.Index(), this),
                    download: ActivityInfo.FromAction<ChargesController>(controller => controller.DownloadCharges(), this),
                    read: ActivityInfo.FromAction<ChargesController>(controller => controller.GetCharges(null), this)));
            }
        }

        public ActionSet Companies
        {
            get
            {
                return _companies ?? (_companies = new ActionSet(
                    create: ActivityInfo.FromAction<CompaniesController>(controller => controller.PostCompanies(null, null), this),
                    destroy: ActivityInfo.FromAction<CompaniesController>(controller => controller.DeleteCompanies(null, null), this),
                    detail: ActivityInfo.FromAction<CompaniesController>(controler => controler.DetailCompany(null), this),
                    display: ActivityInfo.FromAction<CompaniesController>(controller => controller.Index(), this),
                    download: ActivityInfo.FromAction<CompaniesController>(controller => controller.DownloadCompanies(), this),
                    list: ActivityInfo.FromAction<CompaniesController>(controller => controller.ListCompanies(null, null), this),
                    read: ActivityInfo.FromAction<CompaniesController>(controller => controller.GetCompanies(null), this),
                    update: ActivityInfo.FromAction<CompaniesController>(controller => controller.PutCompanies(null, null), this),
                    upload: ActivityInfo.FromAction<CompaniesController>(controller => controller.UploadCompanies(null), this)));
            }
        }

        public ActionSet Consumers
        {
            get
            {
                return _consumers ?? (_consumers = new ActionSet(
                    create: ActivityInfo.FromAction<ContractsController>(controller => controller.PostConsumers(null, null), this),
                    destroy: ActivityInfo.FromAction<ContractsController>(controller => controller.DeleteConsumers(null, null), this),
                    display: ActivityInfo.FromAction<ContractsController>(controller => controller.Index(), this),
                    //download: ActivityInfo.FromAction<ContractsController>(controller => controller.DownloadConsumers(), this),
                    read: ActivityInfo.FromAction<ContractsController>(controller => controller.GetConsumers(null, null, null, null), this),
                    update: ActivityInfo.FromAction<ContractsController>(controller => controller.PutConsumers(null, null), this)));
                //upload: ActivityInfo.FromAction<ContractsController>(controller => controller.UploadConsumers(null), this)));
            }
        }

        public ActionSet Customers
        {
            get
            {
                return _customers ?? (_customers = new ActionSet(
                    //create: ActivityInfo.FromAction<ConsumersController>(controller => controller.PostConsumers(null, null), this),
                    display: ActivityInfo.FromAction<CustomersController>(controller => controller.Index(), this),
                    //download: ActivityInfo.FromAction<ConsumersController>(controller => controller.DownloadConsumers(), this),
                    read: ActivityInfo.FromAction<CustomersController>(controller => controller.GetCustomers(null, null, null, null), this)));
                //upload: ActivityInfo.FromAction<ConsumersController>(controller => controller.UploadConsumers(null), this)));
            }
        }

        public ActionSet Domains
        {
            get
            {
                return _domains ?? (_domains = new ActionSet(
                    create: ActivityInfo.FromAction<DomainsController>(controller => controller.PostDomains(null, null), this),
                    destroy: ActivityInfo.FromAction<DomainsController>(controller => controller.DeleteDomains(null, null), this),
                    display: ActivityInfo.FromAction<DomainsController>(controller => controller.Index(), this),
                    download: ActivityInfo.FromAction<DomainsController>(controller => controller.DownloadDomains(), this),
                    list: ActivityInfo.FromAction<DomainsController>(controller => controller.ListDomains(), this),
                    read: ActivityInfo.FromAction<DomainsController>(controller => controller.GetDomains(null), this),
                    update: ActivityInfo.FromAction<DomainsController>(controller => controller.PutDomains(null, null), this),
                    upload: ActivityInfo.FromAction<DomainsController>(controller => controller.UploadDomains(null), this)));
            }
        }

        public ActionSet Features
        {
            get
            {
                return _features ?? (_features = new ActionSet(
                    create: ActivityInfo.FromAction<FeaturesController>(controller => controller.PostFeatures(null, null), this),
                    destroy: ActivityInfo.FromAction<FeaturesController>(controller => controller.DeleteFeatures(null, null), this),
                    display: ActivityInfo.FromAction<FeaturesController>(controller => controller.Index(), this),
                    list: ActivityInfo.FromAction<FeaturesController>(controller => controller.ListFeatures(null), this),
                    read: ActivityInfo.FromAction<FeaturesController>(controller => controller.GetFeatures(null, null), this),
                    update: ActivityInfo.FromAction<FeaturesController>(controller => controller.PutFeatures(null, null), this)));
            }
        }

        public ActionSet Instances
        {
            get
            {
                return _instances ?? (_instances = new ActionSet());
            }
        }

        public ActionSet Logins
        {
            get
            {
                return _logins ?? (_logins = new ActionSet(
                    //create: ActivityInfo.FromAction<LoginsController>(controller => controller.PostLogins(null, null), this),
                    destroy: ActivityInfo.FromAction<LoginsController>(controller => controller.DeleteLogins(null, null), this),
                    display: ActivityInfo.FromAction<LoginsController>(controller => controller.Index(), this),
                    read: ActivityInfo.FromAction<LoginsController>(controller => controller.GetLogins(null, null, null), this),
                    update: ActivityInfo.FromAction<LoginsController>(controller => controller.PutLogins(null, null), this)));
            }
        }

        public ActionSet MyAccount
        {
            get
            {
                return _myAccount ?? (_myAccount = new ActionSet(
                    //destroy: ActivityInfo.FromAction<MyAccountController>(controller => controller.DestroyAccount(), this),
                    display: ActivityInfo.FromAction<MyAccountController>(controller => controller.Index(), this),
                    read: ActivityInfo.FromAction<MyAccountController>(controller => controller.GetAccount(), this)));
            }
        }

        public ActionSet MyCompany
        {
            get
            {
                return _myCompany ?? (_myCompany = new ActionSet(
                    display: ActivityInfo.FromAction<MyCompanyController>(controller => controller.Index(), this),
                    read: ActivityInfo.FromAction<MyCompanyController>(controller => controller.GetCompany(), this)));
            }
        }

        public ActionSet Products
        {
            get
            {
                return _products ?? (_products = new ActionSet(
                    create: ActivityInfo.FromAction<ProductsController>(controller => controller.PostProducts(null, null), this),
                    destroy: ActivityInfo.FromAction<ProductsController>(controller => controller.DeleteProducts(null, null), this),
                    display: ActivityInfo.FromAction<ProductsController>(controller => controller.Index(), this),
                    download: ActivityInfo.FromAction<ProductsController>(controller => controller.DownloadProducts(), this),
                    list: ActivityInfo.FromAction<ProductsController>(controller => controller.ListProducts(), this),
                    read: ActivityInfo.FromAction<ProductsController>(controller => controller.GetProducts(null), this),
                    update: ActivityInfo.FromAction<ProductsController>(controller => controller.PutProducts(null, null), this),
                    upload: ActivityInfo.FromAction<ProductsController>(controller => controller.UploadProducts(null), this)));
            }
        }

        public ActionSet Roles
        {
            get
            {
                return _roles ?? (_roles = new ActionSet(
                    list: ActivityInfo.FromAction<RolesController>(controller => controller.ListRoles(null, null, null, null), this)));
            }
        }

        public ActionSet Search
        {
            get
            {
                return _search ?? (_search = new ActionSet(
                    read: ActivityInfo.FromAction<SearchController>(controller => controller.Index(null), this)));
            }
        }

        public ActionSet Segments
        {
            get
            {
                return _segments ?? (_segments = new ActionSet(
                    create: ActivityInfo.FromAction<SegmentsController>(controller => controller.PostSegments(null, null), this),
                    destroy: ActivityInfo.FromAction<SegmentsController>(controller => controller.DeleteSegments(null, null), this),
                    display: ActivityInfo.FromAction<SegmentsController>(controller => controller.Index(), this),
                    download: ActivityInfo.FromAction<SegmentsController>(controller => controller.DownloadSegments(), this),
                    list: ActivityInfo.FromAction<SegmentsController>(controller => controller.ListSegments(), this),
                    read: ActivityInfo.FromAction<SegmentsController>(controller => controller.GetSegments(null), this),
                    update: ActivityInfo.FromAction<SegmentsController>(controller => controller.PutSegments(null, null), this),
                    upload: ActivityInfo.FromAction<SegmentsController>(controller => controller.UploadSegments(null), this)));
            }
        }

        public ActionSet Session
        {
            get
            {
                return _sessions ?? (_sessions = new ActionSet(
                    create: ActivityInfo.FromAction<SessionsController>(controller => controller.PostSessions(null, null), this),
                    destroy: ActivityInfo.FromAction<SessionsController>(controller => controller.DeleteSessions(null, null), this),
                    read: ActivityInfo.FromAction<SessionsController>(controller => controller.GetSessions(null, null), this),
                    update: ActivityInfo.FromAction<SessionsController>(controller => controller.PutSessions(null, null), this)));
            }
        }

        public ActionSet Subscriptions
        {
            get
            {
                return _subscriptions ?? (_subscriptions = new ActionSet(
                    create: ActivityInfo.FromAction<SubscriptionsController>(controller => controller.PostSubscriptions(null, null), this),
                    destroy: ActivityInfo.FromAction<SubscriptionsController>(controller => controller.DeleteSubscriptions(null, null), this),
                    display: ActivityInfo.FromAction<SubscriptionsController>(controller => controller.Index(), this),
                    download: ActivityInfo.FromAction<SubscriptionsController>(controller => controller.DownloadSubscriptions(), this),
                    read: ActivityInfo.FromAction<SubscriptionsController>(controller => controller.GetSubscriptions(null, null, null, null, null), this),
                    update: ActivityInfo.FromAction<SubscriptionsController>(controller => controller.PutSubscriptions(null, null), this),
                    upload: ActivityInfo.FromAction<SubscriptionsController>(controller => controller.UploadSubscriptions(null), this)));
            }
        }

        public ActionSet Suppliers
        {
            get
            {
                return _suppliers ?? (_suppliers = new ActionSet(
                    //create: ActivityInfo.FromAction<SuppliersController>(controller => controller.PostSuppliers(null, null), this),
                    display: ActivityInfo.FromAction<SuppliersController>(controller => controller.Index(), this),
                    //download: ActivityInfo.FromAction<SuppliersController>(controller => controller.DownloadSuppliers(), this),
                    read: ActivityInfo.FromAction<SuppliersController>(controller => controller.GetSuppliers(null, null, null, null), this)));
                //upload: ActivityInfo.FromAction<SuppliersController>(controller => controller.UploadSuppliers(null), this)));
            }
        }

        public ActionSet Users
        {
            get
            {
                return _users ?? (_users = new ActionSet(
                    create: ActivityInfo.FromAction<UsersController>(controller => controller.PostUsers(null, null), this),
                    destroy: ActivityInfo.FromAction<UsersController>(controller => controller.DeleteUsers(null, null), this),
                    display: ActivityInfo.FromAction<UsersController>(controller => controller.Index(), this),
                    download: ActivityInfo.FromAction<UsersController>(controller => controller.DownloadUsers(), this),
                    list: ActivityInfo.FromAction<UsersController>(controller => controller.ListUsers(null, null), this),
                    read: ActivityInfo.FromAction<UsersController>(controller => controller.GetUsers(null), this),
                    update: ActivityInfo.FromAction<UsersController>(controller => controller.PutUsers(null, null), this),
                    upload: ActivityInfo.FromAction<UsersController>(controller => controller.UploadUsers(null), this)));
            }
        }

        public WidgetSet Widgets
        {
            get
            {
                return _widgets ?? (_widgets = new WidgetSet(
                    contact: ActivityInfo.FromAction<WidgetsController>(controller => controller.ProductFeedback(), this),
                    enter: ActivityInfo.FromAction<WidgetsController>(controller => controller.AccountEnter(), this)));
            }
        }

        public static implicit operator ActivityHelper(UrlHelper urlHelper)
        {
            return urlHelper != null ? new ActivityHelper(urlHelper) : null;
        }

        public static implicit operator UrlHelper(ActivityHelper activityHelper)
        {
            return activityHelper != null ? activityHelper._urlHelper : null;
        }
    }

    public class ActionSet
    {
        static readonly ActivityInfo Void = ActivityInfo.Void();

        readonly ActivityInfo _create;
        readonly ActivityInfo _destroy;
        readonly ActivityInfo _detail;
        readonly ActivityInfo _display;
        readonly ActivityInfo _download;
        readonly ActivityInfo _list;
        readonly ActivityInfo _read;
        readonly ActivityInfo _update;
        readonly ActivityInfo _upload;

        public ActionSet(ActivityInfo create = null, ActivityInfo destroy = null, ActivityInfo detail = null, ActivityInfo display = null, ActivityInfo download = null, ActivityInfo list = null, ActivityInfo read = null, ActivityInfo update = null, ActivityInfo upload = null)
        {
            _create = create ?? Void;
            _destroy = destroy ?? Void;
            _detail = detail ?? Void;
            _display = display ?? Void;
            _download = download ?? Void;
            _list = list ?? Void;
            _read = read ?? Void;
            _update = update ?? Void;
            _upload = upload ?? Void;
        }

        public ActivityInfo Create { get { return _create; } }

        public ActivityInfo Destroy { get { return _destroy; } }

        public ActivityInfo Detail { get { return _detail; } }

        public ActivityInfo Display { get { return _display; } }

        public ActivityInfo Download { get { return _download; } }

        public ActivityInfo List { get { return _list; } }

        public ActivityInfo Read { get { return _read; } }

        public ActivityInfo Update { get { return _update; } }

        public ActivityInfo Upload { get { return _upload; } }

        public bool CanCreate()
        {
            return _create != null && _create.Url != null;
        }

        public bool CanDestroy()
        {
            return _destroy != null && _destroy.Url != null;
        }

        public bool CanDisplay()
        {
            return _display != null && _display.Url != null;
        }

        public bool CanDownload()
        {
            return _download != null && _download.Url != null;
        }

        public bool CanEdit()
        {
            return CanCreate() || CanDestroy() || CanUpdate();
        }

        public bool CanUpdate()
        {
            return _update != null && _update.Url != null;
        }

        public bool CanUpload()
        {
            return _upload != null && _upload.Url != null;
        }
    }
}