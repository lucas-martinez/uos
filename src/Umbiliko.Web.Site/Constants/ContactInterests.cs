﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Constants
{
    public static class ContactInterests
    {
        public const string CreateCompany = "create-company";

        public static string Complain = "complain";

        public static string Consulting = "consulting";

        public const string ReportError = "report-error";

        public static string Other = "other";
    }
}
