﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Authorization.Models
{
    using Constants;

    public class ContractRowModel
    {
        [MaxLength(Lengths.CompanyCodeLength)]
        public string CustomerCode { get; set; }

        [Required]
        public int Identifier { get; set; }

        [MaxLength(Lengths.DomainNameLength)]
        public string ProductName { get; set; }

        [MaxLength(Lengths.SegmentCodeLength)]
        public string SegmentCode { get; set; }

        [Required]
        public long Since { get; set; }

        [MaxLength(Lengths.CompanyCodeLength)]
        public string VendorCode { get; set; }
    }
}
