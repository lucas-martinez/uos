﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Authorization.Models
{
    using Constants;

    public class CustomerPostModel
    {
        [MaxLength(Lengths.CompanyCodeLength)]
        public string CompanyCode { get; set; }

        [MaxLength(Lengths.CompanyNameLength)]
        public string CompanyName { get; set; }

        [MaxLength(Lengths.CompanyTypeLength)]
        public string CompanyType { get; set; }

        [Required]
        public string ConsumerToken { get; set; }

        [MaxLength(Lengths.CountryCodeLength), Required]
        public string CountryCode { get; set; }

        [Required]
        public decimal Credit { get; set; }

        [MaxLength(Lengths.CurrencyCodeLength), Required]
        public string CurrencyCode { get; set; }

        [MaxLength(Lengths.ProductNameLength), Required]
        public string ProductName { get; set; }

        [MaxLength(Lengths.SegmentCodeLength), Required]
        public string SegmentCode { get; set; }
    }
}
