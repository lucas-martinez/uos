﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Uos.Web.Areas.Authorization.Models
{
    using Constants;

    public class SubscriptionPostModel
    {
        [MaxLength(Lengths.FeatureNameLength), Required]
        public string FeatureName { get; set; }

        [MaxLength(Lengths.DomainNameLength), Required]
        public string ProductName { get; set; }

        [MaxLength(Lengths.SegmentCodeLength), Required]
        public string SegmentCode { get; set; }

        [Required]
        public DateTimeOffset? Since { get; set; }

        [Required]
        public string Status { get; set; }

        public DateTimeOffset? Until { get; set; }
    }
}
