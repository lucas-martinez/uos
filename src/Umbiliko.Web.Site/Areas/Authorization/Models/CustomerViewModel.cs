﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Authorization.Models
{
    using Constants;

    public class CustomerViewModel
    {
        [MaxLength(Lengths.CompanyNameLength), Required]
        public string CompanyName { get; set; }

        [MaxLength(Lengths.CompanyTypeLength), Required]
        public string CompanyType { get; set; }
    }
}
