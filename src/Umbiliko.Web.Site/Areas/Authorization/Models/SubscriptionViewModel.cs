﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Uos.Web.Areas.Authorization.Models
{
    using Constants;

    public class SubscriptionViewModel
    {
        [MaxLength(Lengths.CompanyCodeLength)]
        public string CompanyCode { get; set; }

        [MaxLength(Lengths.FeatureNameLength)]
        public string FeatureName { get; set; }

        [Required]
        public int Identifier { get; set; }

        [MaxLength(Lengths.DomainNameLength)]
        public string ProductName { get; set; }

        [MaxLength(Lengths.SegmentCodeLength)]
        public string SegmentCode { get; set; }

        public long SinceUtc { get; set; }

        public string Status { get; set; }

        public long? UntilUtc { get; set; }
    }
}
