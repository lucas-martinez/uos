﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Authorization.Models
{
    public class ContractDeleteModel
    {
        public int Identifier { get; set; }
    }
}
