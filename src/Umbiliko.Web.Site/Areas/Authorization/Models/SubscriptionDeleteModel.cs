﻿using System.ComponentModel.DataAnnotations;

namespace Uos.Web.Areas.Authorization.Models
{
    public class SubscriptionDeleteModel
    {
        [Required]
        public int Identifier { get; set; }
    }
}
