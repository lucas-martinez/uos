﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Authorization.Models
{
    using Constants;

    public class ContractPutModel
    {
        [MaxLength(Lengths.CountryCodeLength), Required]
        public string CountryCode { get; set; }

        [MaxLength(Lengths.CompanyCodeLength), Required]
        public string CustomerCode { get; set; }

        [Required]
        public decimal Credit { get; set; }

        [MaxLength(Lengths.CurrencyCodeLength), Required]
        public string CurrencyCode { get; set; }

        public int Identifier { get; set; }

        [MaxLength(Lengths.ProductNameLength), Required]
        public string ProductName { get; set; }

        [MaxLength(Lengths.SegmentCodeLength), Required]
        public string SegmentCode { get; set; }

        [MaxLength(Lengths.CompanyCodeLength), Required]
        public string VendorCode { get; set; }
    }
}
