﻿using System.ComponentModel.DataAnnotations;

namespace Uos.Web.Areas.Authorization.Models
{
    using Constants;

    public class ContractViewModel
    {
        [MaxLength(Lengths.CompanyNameLength), Required]
        public string CompanyName { get; set; }

        [Required]
        public string CompanyType { get; set; }

        [MaxLength(Lengths.CountryCodeLength), Required]
        public string CountryCode { get; set; }

        [Required]
        public decimal Credit { get; set; }


        [MaxLength(Lengths.CurrencyCodeLength), Required]
        public string CurrencyCode { get; set; }

        [MaxLength(Lengths.CompanyCodeLength), Required]
        public string CustomerCode { get; set; }

        [Required]
        public int Identifier { get; set; }

        [MaxLength(Lengths.SegmentCodeLength), Required]
        public string SegmentCode { get; set; }

        public string Since { get; set; }

        [MaxLength(Lengths.ProductNameLength), Required]
        public string ProductName { get; set; }

        [MaxLength(Lengths.CompanyCodeLength), Required]
        public string VendorCode { get; set; }
    }
}
