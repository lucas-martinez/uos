﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Uos.Web.Areas.Authorization.Models
{
    using Constants;

    public class SubscriptionRowModel
    {
        [MaxLength(Lengths.CompanyCodeLength)]
        public string CustomerCode { get; set; }

        [MaxLength(Lengths.FeatureNameLength)]
        public string FeatureName { get; set; }

        [Required]
        public int Identifier { get; set; }

        [MaxLength(Lengths.ProductNameLength)]
        public string ProductName { get; set; }

        [MaxLength(Lengths.SegmentCodeLength)]
        public string SegmentCode { get; set; }

        [Required]
        public string Since { get; set; }

        [Required]
        public string Status { get; set; }

        public string Until { get; set; }

        [MaxLength(Lengths.CompanyCodeLength)]
        public string VendorCode { get; set; }
    }
}
