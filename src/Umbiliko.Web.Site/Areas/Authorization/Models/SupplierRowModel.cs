﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Authorization.Models
{
    using Constants;

    public class SupplierRowModel
    {
        [MaxLength(Lengths.CompanyNameLength), Required]
        public string CompanyName { get; set; }

        [MaxLength(Lengths.CompanyTypeLength), Required]
        public string CompanyType { get; set; }

        [Required]
        public string Identifier { get; set; }

        [Required]
        public decimal TotalCredit { get; set; }
        
        public long Since { get; set; }
    }
}
