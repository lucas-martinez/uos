﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Uos.Web.Areas.Authorization.Controllers
{
    using Constants;
    using Entities;
    using Models;
    using Services;
    using Web.Models;

    [Mvc.Feature(Features.Authorization), RouteArea(Features.Authorization), RoutePrefix(Collections.Subscriptions)]
    public class SubscriptionsController : Mvc.EntityController<SubscriptionEntity, int>
    {
        // GET: authorization/subscriptions
        [HttpGet, Mvc.Activity(Activities.DisplaySubscriptions), Route(Actions.Default)]
        public ActionResult Index()
        {
            return View();
        }

        // GET: authorization/subscriptions/download
        [HttpGet, Mvc.Activity(Activities.DownloadSubscriptions), Route(Actions.Download)]
        public async Task<ActionResult> DownloadSubscriptions()
        {
            var model = new DownloadModel();

            //

            var json = JsonConvert.SerializeObject(model, Formatting.Indented);

            var content = System.Text.Encoding.Unicode.GetBytes(json);

            return File(content, "application/json", string.Format("Uos-{0:yyyyMMddHHmm}.accounts.json", DateTime.Now));
        }

        // GET|POST: authorization/subscriptions/upload
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post), Mvc.Activity(Activities.UploadSubscriptions), Route(Actions.Upload)]
        public async Task<ActionResult> UploadSubscriptions(UploadModel model)
        {
            if (!ModelState.IsValid)
            {
                return PartialView("Upload");
            }

            switch (model.File.ContentType)
            {
                case "application/json":
                case "application/octet-stream":

                    var content = new byte[model.File.InputStream.Length];

                    model.File.InputStream.Read(content, 0, content.Length);

                    var json = System.Text.Encoding.Unicode.GetString(content);

                    var data = JsonConvert.DeserializeObject<DownloadModel>(json);

                    //

                    return await SaveChangesAsync(() =>
                    {
                        return PartialView("Upload");
                    });
            }

            return PartialView("Upload");
        }

        #region - authorization/subscriptions/api -

        // DELETE: authorization/subscriptions/api
        [HttpDelete, Mvc.Activity(Activities.DestroySubscriptions), Route(Actions.Api)]
        public async Task<ActionResult> DeleteSubscriptions([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<SubscriptionDeleteModel> models)
        {
            return await DeleteAsync(request, models, m => m.Identifier, e => e.ToViewModel(),
                (missing) => string.Format("One or more subscriptions were not found: [{0}]", missing.Select(item => item.Identifier)));
        }

        // GET: authorization/subscriptions/api
        [HttpGet, Mvc.Activity(Activities.ReadSubscriptions), Route(Actions.Api)]
        public async Task<ActionResult> GetSubscriptions(string customer, string client, string product, string feature, [DataSourceRequest]DataSourceRequest request)
        {
            return await SelectAsync(request, request.ResetFilters().ToFilter().ApplyTo(Predicate).Apply(client: client, product: product, feature: feature), e => e.ToRowModel(),
                "CustomerCode", "SegmentCode", "VendorCode", "ProductName", "FeatureName");
        }

        // POST: authorization/subscriptions/api
        [HttpPost, Mvc.Activity(Activities.CreateSubscriptions), Route(Actions.Api)]
        public async Task<ActionResult> PostSubscriptions([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<SubscriptionPostModel> models)
        {
            return await InsertAsync(request, models, e=> e.ToViewModel(), (entity, model) =>
            {
                entity.FeatureName = model.FeatureName;
                entity.ConsumerCode = model.SegmentCode;
                entity.ProductName = model.ProductName;
                entity.SinceUtc = model.Since.HasValue ? model.Since.Value.UtcDateTime : DateTime.UtcNow;
                entity.Status = model.Status;
                entity.UntilUtc = model.Until.HasValue ? model.Until.Value.UtcDateTime : (DateTime?)null;
            });
        }

        // PUT: authorization/subscriptions/api
        [HttpPut, Mvc.Activity(Activities.UpdateSubscriptions), Route(Actions.Api)]
        public async Task<ActionResult> PutSubscriptions([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<SubscriptionPutModel> models)
        {
            return await UpdateAsync(request, models, m => m.Identifier, e => e.ToViewModel(), updater: (entity, model) =>
            {
                entity.SinceUtc = DateTime.UtcNow;
                entity.Status = model.Status;
                entity.UntilUtc = model.Until.HasValue ? model.Until.Value.UtcDateTime : (DateTime?)null;
            });
        }

        #endregion - authorization/subscriptions/api -
    }
}