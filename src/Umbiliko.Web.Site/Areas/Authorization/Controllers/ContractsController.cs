﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Uos.Web.Areas.Authorization.Controllers
{
    using Constants;
    using Entities;
    using Models;
    using Services;
    using Web.Models;

    [Mvc.Feature(Features.Authorization), RouteArea(Features.Authorization), RoutePrefix(Collections.Contracts)]
    public class ContractsController : Mvc.EntityController<ContractEntity, int>
    {
        // GET: authorization/contracts
        [HttpGet, Mvc.Activity(Activities.DisplayConsumers), Route(Actions.Default)]
        public ActionResult Index()
        {
            return View();
        }
        
        #region - authorization/contracts/api -

        // DELETE: authorization/contracts/api
        [HttpDelete, Mvc.Activity(Activities.DestroyConsumers), Route(Actions.Api)]
        public async Task<ActionResult> DeleteConsumers([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<ContractDeleteModel> models)
        {
            return await DeleteAsync(request, models, m => m.Identifier, e => e.ToViewModel(),
                (missing) => string.Format("One or more consumers were not found: [{0}]", missing.Select(item => item.Identifier)));
        }

        // GET: authorization/contracts/api
        [HttpGet, Mvc.Activity(Activities.ReadConsumers), Route(Actions.Api)]
        public async Task<ActionResult> GetConsumers(string customer, string client, string product, [DataSourceRequest]DataSourceRequest request)
        {
            return await SelectAsync(request, request.ResetFilters().ToFilter().ApplyTo(Predicate).Apply(customer: customer, client: client, product: product), e => e.ToRowModel(),
                "CustomerCode", "SegmentCode", "VendorCode", "ProductName");
        }

        // POST: authorization/contracts/api
        [HttpPost, Mvc.Activity(Activities.CreateConsumers), Route(Actions.Api)]
        public async Task<ActionResult> PostConsumers([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<ContractPostModel> models)
        {
            return await InsertAsync(request, models, e => e.ToViewModel(), (entity, model) =>
            {
                //ChangeUtc = now,
                //CreatedUtc = now
            });
        }

        // PUT: authorization/contracts/api
        [HttpPut, Mvc.Activity(Activities.UpdateConsumers), Route(Actions.Api)]
        public async Task<ActionResult> PutConsumers([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<ContractPutModel> models)
        {
            return await UpdateAsync(request, models, m => m.Identifier, e => e.ToViewModel(), updater: (entity, model) =>
            {
                if (model.CountryCode != null) entity.CountryCode = model.CountryCode;
                if (model.CountryCode != null) entity.CurrencyCode = model.CurrencyCode;
            });
        }

        #endregion - authorization/contracts/api -
    }
}