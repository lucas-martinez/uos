﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Uos.Web.Areas.Authorization.Controllers
{
    using Constants;
    using Entities;
    using Models;
    using Services;
    using System.Net;
    using Utilities;
    using Web.Models;

    [Mvc.Feature(Features.Authorization), RouteArea(Features.Authorization), RoutePrefix(Collections.Customers)]
    public class CustomersController : Mvc.EntityController<ContractEntity, int>
    {
        // GET: authorization/customers
        [HttpGet, Mvc.Activity(Activities.DisplayCustomers), Route(Actions.Default)]
        public ActionResult Index()
        {
            return View();
        }

        // GET: authorization/customers/download
        [HttpGet, Mvc.Activity(Activities.DownloadCustomers), Route(Actions.Download)]
        public async Task<ActionResult> DownloadCustomers()
        {
            var model = new DownloadModel();

            //

            var json = JsonConvert.SerializeObject(model, Formatting.Indented);

            var content = System.Text.Encoding.Unicode.GetBytes(json);

            return File(content, "application/json", string.Format("Uos-{0:yyyyMMddHHmm}.consumers.json", DateTime.Now));
        }

        // GET|POST: authorization/customers/upload
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post), Mvc.Activity(Activities.UploadConsumers, Level = AccessLevels.Internal), Route(Actions.Upload)]
        public async Task<ActionResult> UploadCustomers(UploadModel model)
        {
            if (!ModelState.IsValid)
            {
                return PartialView("Upload");
            }

            switch (model.File.ContentType)
            {
                case "application/json":
                case "application/octet-stream":

                    var content = new byte[model.File.InputStream.Length];

                    model.File.InputStream.Read(content, 0, content.Length);

                    var json = System.Text.Encoding.Unicode.GetString(content);

                    var data = JsonConvert.DeserializeObject<DownloadModel>(json);

                    //

                    return await SaveChangesAsync(() =>
                    {
                        return PartialView("Upload");
                    });
            }

            return PartialView("Upload");
        }

        // GET: authorization/customers/api
        [HttpGet, Mvc.Activity(Activities.ReadCustomers), Route(Actions.Api)]
        public async Task<ActionResult> GetCustomers(string customer, string client, string product, [DataSourceRequest]DataSourceRequest request)
        {
            var session = Authentication.CurrentSession;

            if (session.Account == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var account = await AccountContext.FindAsync(DbContext, session.User.Name, session.Account.CompanyCode, session.Account.SegmentCode);

            string vendor = account.Company;

            var predicate = Predicate.Apply(client: client, customer: customer, product: product, vendor: vendor);

            var groups = await DbSet
                .Include(e => e.Customer)
                .Where(predicate)
                .AsExpandableAsync()
                .Select(e => new { e.CustomerCode, e.Customer, e.Credit, e.SinceUtc })
                .GroupBy(e => e.CustomerCode)
                .AsExpandableAsync()
                .ToListAsync();
            
            var result = new List<CustomerRowModel>();

            foreach (var group in groups)
            {
                var company = group.First().Customer;
                result.Add(new CustomerRowModel
                {
                    CompanyName = company.CompanyName,
                    CompanyType = company.CompanyType,
                    CustomerTrademark = company.CompanyCode,
                    Identifier = group.Key,
                    TotalCredit = group.Sum(e => e.Credit),
                    Since = group.Min(e => e.SinceUtc).ToUnixMoment()
                });
            }

            return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
    }
}