﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Uos.Web.Areas.Authorization.Controllers
{
    using Constants;
    using Entities;
    using Models;
    using Services;
    using Web.Models;
    using Utilities;
    using System.Net;

    [Mvc.Feature(Features.Authorization), RouteArea(Features.Authorization), RoutePrefix(Collections.Suppliers)]
    public class SuppliersController : Mvc.EntityController<ContractEntity, int>
    {
        // GET: authorization/suppliers
        [HttpGet, Mvc.Activity(Activities.DisplayCustomers), Route(Actions.Display)]
        public ActionResult Index()
        {
            return View();
        }

        // GET: authorization/suppliers/api
        [HttpGet, Mvc.Activity(Activities.ReadCustomers), Route(Actions.Api)]
        public async Task<ActionResult> GetSuppliers(string supplier, string client, string product, [DataSourceRequest]DataSourceRequest request)
        {
            var session = Authentication.CurrentSession;

            if (session.Account == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var account = await AccountContext.FindAsync(DbContext, session.User.Name, session.Account.CompanyCode, session.Account.SegmentCode);

            string customer = account.Company;

            var predicate = Predicate.Apply(client: client, customer: supplier, product: product, vendor: customer);

            var groups = await DbSet
                .Include(e => e.Customer)
                .Where(predicate)
                .AsExpandableAsync()
                .Select(e => new { e.CompanyCode, e.Company, e.Credit, e.SinceUtc })
                .GroupBy(e => e.CompanyCode)
                .AsExpandableAsync()
                .ToListAsync();

            var result = new List<CustomerRowModel>();

            foreach (var group in groups)
            {
                var company = group.First().Company;
                result.Add(new CustomerRowModel
                {
                    CompanyName = company.CompanyName,
                    CompanyType = company.CompanyType,
                    CustomerTrademark = company.CompanyCode,
                    Identifier = group.Key,
                    TotalCredit = group.Sum(e => e.Credit),
                    Since = group.Min(e => e.SinceUtc).ToUnixMoment()
                });
            }

            return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
    }
}