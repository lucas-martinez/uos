﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Uos.Web.Controllers
{
    using Constants;
    using Entities;
    using Inquiry;
    using Models;

    [Mvc.Feature(Features.Authorization), RouteArea(Features.Authorization), RoutePrefix(Collections.Roles)]
    public class RolesController : Mvc.EntityController<ActivityEntity, int>
    {
        // GET: authorzation/roles
        [HttpGet, Mvc.Activity(Activities.DisplayRoles), Route(Actions.Default)]
        public ActionResult Index()
        {
            return View();
        }

        // POST: authorization/roles
        [HttpPost, Mvc.Activity(Activities.ListRoles), Route(Actions.Default)]
        public async Task<ActionResult> ListRoles(string product, string feature, string activity, [Bind(Prefix = "filter")]FilterRequestModel model)
        {
            var roles = await DbSet.Where(Predicate.Apply(product: product, feature: feature, activity: activity)).AsExpandableAsync().Select(e => e.Roles).AsExpandableAsync().ToListAsync();

            roles = roles.SelectMany(s => s.Split(Characters.Space))
                .OrderBy(role => role)
                .Distinct()
                .ToList();

            if (activity == null)
            {
                roles = roles.Select(role => role.TrimEnd(Characters.Exclamation)).Distinct().ToList();
            }

            if (model != null && model.Filters != null)
            {
                var values = model.Filters.Where(f => f.Value != null).Select(f => f.Value).ToList();

                if (values.Count > 0)
                {
                    roles = roles.Where(role => values.Any(value => role.StartsWith(value))).ToList();
                }
            }

            return Json(roles);
        }
        /*
        // DELETE: roles/api
        [HttpDelete, Route(Actions.Api)]
        public async Task<ActionResult> DeleteRoles([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<RoleDeleteModel> models)
        {
            List<RoleEntity> entities;

            var identities = models.Select(m => m.Identity).ToList();

            var result = Delete<RoleEntity, int>(DbContext.Roles, identities, out entities);

            return result ?? await SaveChangesAsync(() =>
            {
                if (identities.Count > 0)
                {
                    Response.StatusCode = (int)HttpStatusCode.Found;
                    Response.StatusDescription = string.Format("One or more entities were not found: [{0}]", string.Join(", ", identities));
                }

                Func<RoleEntity, RoleViewModel> selector = e => e.ToViewModel();

                return Json(entities.ToDataSourceResult(request, selector));
            });
        }

        // GET: roles/api
        [HttpGet, Route(Actions.Api)]
        public ActionResult GetRoles(string client, string user, [DataSourceRequest]DataSourceRequest request)
        {
            Func<RoleEntity, RoleRowModel> selector = e => e.ToRowModel();

            IQueryable<RoleEntity> source = DbContext.Roles;

            if (!string.IsNullOrEmpty(client)) source = source.Where(e => e.SegmentCode == client);

            if (!string.IsNullOrEmpty(client)) source = source.Where(e => e.UserName == user);

            request.DefaultSort("SegmentCode", "UserName", "GroupName");

            return Json(source.ToDataSourceResult(request, selector), JsonRequestBehavior.AllowGet);
        }

        // POST: roles/api
        [HttpPost, Route(Actions.Api)]
        public async Task<ActionResult> PostRoles([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<RolePostModel> models)
        {
            if (!ModelState.IsValid)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var entities = new List<RoleEntity>();
            var conflicts = new List<RolePostModel>();
            var missing = new List<AccountKeyModel>();
            var errors = new List<string>();

            foreach (var account in models.GroupBy(m => new AccountKeyModel { Client = m.Client, UserName = m.UserName }))
            {
                var accountEntity = await DbContext.Accounts.Include(e => e.Roles).SingleOrDefaultAsync(e => e.Client == account.Key.Client && e.UserName == account.Key.UserName);

                if (accountEntity == null)
                {
                    missing.Add(account.Key);
                    continue;
                }

                foreach (var model in account)
                {
                    if (accountEntity.Roles.Any(e => e.GroupName == model.GroupName))
                    {
                        conflicts.Add(model);

                        continue;
                    }

                    var roleEntity = DbContext.Roles.Create();

                    roleEntity.Client = model.Client;
                    roleEntity.GroupName = model.GroupName;
                    roleEntity.UserName = model.UserName;

                    accountEntity.Roles.Add(roleEntity);

                    entities.Add(roleEntity);
                }
            }

            if (missing.Count > 0)
            {
                errors.Add(string.Format("These accounts were not found: {0}.", string.Join(", ", JsonConvert.SerializeObject(missing, Formatting.None))));
            }

            if (conflicts.Count > 0)
            {
                errors.Add(string.Format("These roles already exist: {0}." + string.Join(", ", JsonConvert.SerializeObject(conflicts, Formatting.None))));
            }

            return await SaveChangesAsync(() =>
            {
                if (errors.Count > 0)
                {
                    Response.StatusDescription = string.Format("There were one or more errors: ", string.Join(". ", errors));
                }

                Response.StatusCode = (int)HttpStatusCode.Created;
                Response.StatusDescription = Request.RawUrl;

                Func<RoleEntity, RoleRowModel> selector = e => e.ToRowModel();

                return Json(entities.ToDataSourceResult(request, selector));
            });
        }

        // PUT: roles/api
        [HttpPut, Route(Actions.Api)]
        public async Task<ActionResult> PutRoles([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<RolePutModel> models)
        {
            var entities = new List<RoleEntity>();
            var missing = new List<RolePutModel>();
            var errors = new List<string>();

            foreach (var account in models.GroupBy(m => new AccountKeyModel { Client = m.Client, UserName = m.UserName }))
            {
                var accountEntity = await DbContext.Accounts.Include(e => e.Roles).SingleOrDefaultAsync(e => e.Client == account.Key.Client && e.UserName == account.Key.UserName);

                if (accountEntity == null)
                {
                    foreach (var model in account)
                    {
                        missing.Add(model);
                    }

                    continue;
                }

                foreach (var model in account)
                {
                    var entity = accountEntity.Roles.SingleOrDefault(e => e.Identity == model.Identity);

                    if (entity != null)
                    {
                        //if (!string.IsNullOrEmpty(model.Role)) entity.RoleName = model.Role;
                        //if (!string.IsNullOrEmpty(model.Desc)) entity.Description = model.Desc;

                        entities.Add(entity);
                    }
                    else
                    {
                        missing.Add(model);
                    }
                }
            }

            if (missing.Count > 0)
            {
                errors.Add(string.Format("These roles were not found: {0}.", string.Join(", ", JsonConvert.SerializeObject(missing, Formatting.None))));
            }

            return await SaveChangesAsync(() =>
            {
                if (errors.Count > 0)
                {
                    Response.StatusDescription = string.Format("There were one or more errors: ", string.Join(". ", errors));
                }

                Response.StatusDescription = Request.RawUrl;

                Func<RoleEntity, RoleViewModel> selector = e => e.ToViewModel();

                return Json(entities.ToDataSourceResult(request, selector));
            });
        }*/
    }
}