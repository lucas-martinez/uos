﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Authorization.Models
{
    using Entities;
    using Utilities;

    public static class ContractExtensions
    {
        public static ContractRowModel ToRowModel(this ContractEntity entity)
        {
            return entity == null ? null :
                new ContractRowModel
                {
                    SegmentCode = entity.ConsumerCode,
                    CustomerCode = entity.CustomerCode,
                    Identifier = entity.Id,
                    ProductName = entity.ProductName,
                    Since = entity.SinceUtc.ToUnixMoment(),
                    VendorCode = entity.CompanyCode
                };
        }

        public static string AsSearchResultValue(this ContractEntity entity)
        {
            return string.Format("Customer {0}, SegmentCode: {2}", entity.Customer.CompanyName, entity.ConsumerCode);
        }

        public static ContractViewModel ToViewModel(this ContractEntity entity)
        {
            return entity == null ? null :
                new ContractViewModel
                {
                    SegmentCode = entity.ConsumerCode,
                    CountryCode = entity.CountryCode,
                    CurrencyCode = entity.CurrencyCode,
                    Identifier = entity.Id,
                    ProductName = entity.ProductName,
                    Since = entity.SinceUtc.ToString(),
                    CustomerCode = entity.CustomerCode
                };
        }
    }
}
