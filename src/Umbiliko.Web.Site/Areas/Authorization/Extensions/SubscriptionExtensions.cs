﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Authorization.Models
{
    using Entities;
    using Utilities;

    public static class SubscriptionExtensions
    {
        public static SubscriptionRowModel ToRowModel(this SubscriptionEntity entity)
        {
            Func<DateTime, long> dateToUtcTimestamp = (d) => { return (long)Math.Floor((d - new DateTime()).TotalMilliseconds); };

            return entity == null ? null :
                new SubscriptionRowModel
                {
                    SegmentCode = entity.ConsumerCode,
                    FeatureName = entity.FeatureName,
                    Identifier = entity.Id,
                    ProductName = entity.ProductName,
                    Since = new DateTimeOffset(entity.SinceUtc, TimeSpan.FromHours(0)).ToString("yyyy-MM-dd HH:mm:ss zzz"),
                    Status = entity.Status,
                    Until = entity.UntilUtc.HasValue ? new DateTimeOffset(entity.UntilUtc.Value, TimeSpan.FromHours(0)).ToString("yyyy-MM-dd HH:mm:ss zzz") : null
                };
        }

        public static string AsSearchResultValue(this SubscriptionEntity entity)
        {
            return string.Format("SegmentCode: {0}, Product: {1}, Feature: {2}", entity.ConsumerCode, entity.ProductName, entity.FeatureName);
        }

        public static SubscriptionViewModel ToViewModel(this SubscriptionEntity entity)
        {
            return entity == null ? null :
                new SubscriptionViewModel
                {
                    SegmentCode = entity.ConsumerCode,
                    FeatureName = entity.FeatureName,
                    Identifier = entity.Id,
                    ProductName = entity.ProductName,
                    SinceUtc = entity.SinceUtc.ToUnixMoment(),
                    Status = entity.Status,
                    UntilUtc = entity.UntilUtc.ToUnixMoment()
                };
        }
    }
}
