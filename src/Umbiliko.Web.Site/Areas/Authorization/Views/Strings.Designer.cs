﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Uos.Web.Areas.Authorization.Views {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Strings {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Strings() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Uos.Web.Areas.Authorization.Views.Strings", typeof(Strings).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add Consumer.
        /// </summary>
        public static string AddConsumerCmd {
            get {
                return ResourceManager.GetString("AddConsumerCmd", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add Consumer.
        /// </summary>
        public static string AddConsumerTitle {
            get {
                return ResourceManager.GetString("AddConsumerTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add Customer.
        /// </summary>
        public static string AddCustomerCmd {
            get {
                return ResourceManager.GetString("AddCustomerCmd", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add Subscription.
        /// </summary>
        public static string AddSubscriptionCmd {
            get {
                return ResourceManager.GetString("AddSubscriptionCmd", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add Subscription.
        /// </summary>
        public static string AddSubscriptionTitle {
            get {
                return ResourceManager.GetString("AddSubscriptionTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancel.
        /// </summary>
        public static string CancelCmd {
            get {
                return ResourceManager.GetString("CancelCmd", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Company Code.
        /// </summary>
        public static string CompanyCodeColumn {
            get {
                return ResourceManager.GetString("CompanyCodeColumn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Company code.
        /// </summary>
        public static string CompanyCodeField {
            get {
                return ResourceManager.GetString("CompanyCodeField", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Company Name.
        /// </summary>
        public static string CompanyNameColumn {
            get {
                return ResourceManager.GetString("CompanyNameColumn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Company name.
        /// </summary>
        public static string CompanyNameField {
            get {
                return ResourceManager.GetString("CompanyNameField", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Company.
        /// </summary>
        public static string CompanyTitle {
            get {
                return ResourceManager.GetString("CompanyTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Company Type.
        /// </summary>
        public static string CompanyTypeColumn {
            get {
                return ResourceManager.GetString("CompanyTypeColumn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Company type.
        /// </summary>
        public static string CompanyTypeField {
            get {
                return ResourceManager.GetString("CompanyTypeField", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Consumer Details.
        /// </summary>
        public static string ConsumerDetailsTitle {
            get {
                return ResourceManager.GetString("ConsumerDetailsTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Consumers.
        /// </summary>
        public static string ConsumersSubtitle {
            get {
                return ResourceManager.GetString("ConsumersSubtitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Consumers.
        /// </summary>
        public static string ConsumersTitle {
            get {
                return ResourceManager.GetString("ConsumersTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Consumer Token.
        /// </summary>
        public static string ConsumerTokenField {
            get {
                return ResourceManager.GetString("ConsumerTokenField", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Country Code.
        /// </summary>
        public static string CountryCodeColumn {
            get {
                return ResourceManager.GetString("CountryCodeColumn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Country code.
        /// </summary>
        public static string CountryCodeField {
            get {
                return ResourceManager.GetString("CountryCodeField", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Credit.
        /// </summary>
        public static string CreditColumn {
            get {
                return ResourceManager.GetString("CreditColumn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Credit.
        /// </summary>
        public static string CreditField {
            get {
                return ResourceManager.GetString("CreditField", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Currency Code.
        /// </summary>
        public static string CurrencyCodeColumn {
            get {
                return ResourceManager.GetString("CurrencyCodeColumn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Currency code.
        /// </summary>
        public static string CurrencyCodeField {
            get {
                return ResourceManager.GetString("CurrencyCodeField", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Customers.
        /// </summary>
        public static string CustomersSubtitle {
            get {
                return ResourceManager.GetString("CustomersSubtitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Uos Customers.
        /// </summary>
        public static string CustomersTitle {
            get {
                return ResourceManager.GetString("CustomersTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Customer.
        /// </summary>
        public static string CustomerTitle {
            get {
                return ResourceManager.GetString("CustomerTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Destroy Customer.
        /// </summary>
        public static string DestroyCustomerCmd {
            get {
                return ResourceManager.GetString("DestroyCustomerCmd", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Destroy Susbcription.
        /// </summary>
        public static string DestroySubscriptionCmd {
            get {
                return ResourceManager.GetString("DestroySubscriptionCmd", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Done.
        /// </summary>
        public static string DoneCmd {
            get {
                return ResourceManager.GetString("DoneCmd", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Feature.
        /// </summary>
        public static string FeatureNameColumn {
            get {
                return ResourceManager.GetString("FeatureNameColumn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Field.
        /// </summary>
        public static string FeatureNameField {
            get {
                return ResourceManager.GetString("FeatureNameField", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Find.
        /// </summary>
        public static string FindCmd {
            get {
                return ResourceManager.GetString("FindCmd", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Manage.
        /// </summary>
        public static string ManageCmd {
            get {
                return ResourceManager.GetString("ManageCmd", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Manage Consumer.
        /// </summary>
        public static string ManageConsumerTitle {
            get {
                return ResourceManager.GetString("ManageConsumerTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Manage Customer.
        /// </summary>
        public static string ManageCustomerTitle {
            get {
                return ResourceManager.GetString("ManageCustomerTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Manage Subscription.
        /// </summary>
        public static string ManageSubscriptionTitle {
            get {
                return ResourceManager.GetString("ManageSubscriptionTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product.
        /// </summary>
        public static string ProductNameColumn {
            get {
                return ResourceManager.GetString("ProductNameColumn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product name.
        /// </summary>
        public static string ProductNameField {
            get {
                return ResourceManager.GetString("ProductNameField", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product.
        /// </summary>
        public static string ProductTitle {
            get {
                return ResourceManager.GetString("ProductTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Segment Code.
        /// </summary>
        public static string SegmentCodeColumn {
            get {
                return ResourceManager.GetString("SegmentCodeColumn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Segment code.
        /// </summary>
        public static string SegmentCodeField {
            get {
                return ResourceManager.GetString("SegmentCodeField", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Since.
        /// </summary>
        public static string SinceColumn {
            get {
                return ResourceManager.GetString("SinceColumn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Since.
        /// </summary>
        public static string SinceField {
            get {
                return ResourceManager.GetString("SinceField", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Status.
        /// </summary>
        public static string StatusColumn {
            get {
                return ResourceManager.GetString("StatusColumn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Status.
        /// </summary>
        public static string StatusField {
            get {
                return ResourceManager.GetString("StatusField", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Subscription Details.
        /// </summary>
        public static string SubscriptionDetailsTitle {
            get {
                return ResourceManager.GetString("SubscriptionDetailsTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Subscriptions.
        /// </summary>
        public static string SubscriptionsSubtitle {
            get {
                return ResourceManager.GetString("SubscriptionsSubtitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Subscriptions.
        /// </summary>
        public static string SubscriptionsTitle {
            get {
                return ResourceManager.GetString("SubscriptionsTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Total Credit.
        /// </summary>
        public static string TotalCreditColumn {
            get {
                return ResourceManager.GetString("TotalCreditColumn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Total credit.
        /// </summary>
        public static string TotalCreditField {
            get {
                return ResourceManager.GetString("TotalCreditField", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Until.
        /// </summary>
        public static string UntilColumn {
            get {
                return ResourceManager.GetString("UntilColumn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ultil.
        /// </summary>
        public static string UntilField {
            get {
                return ResourceManager.GetString("UntilField", resourceCulture);
            }
        }
    }
}
