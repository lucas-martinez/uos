﻿using System.Web.Mvc;

namespace Uos.Web.Areas.Authorization
{
    using Constants;

    public class AuthorizationAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get { return Features.Authorization; }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            /*context.MapRoute(
                "Authorization_default",
                "Authorization/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );*/
        }
    }
}