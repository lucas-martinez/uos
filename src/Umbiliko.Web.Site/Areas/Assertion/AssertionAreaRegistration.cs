﻿using System.Web.Mvc;

namespace Uos.Web.Areas.Assertion
{
    using Constants;

    public class AssertionAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get { return Features.Assertion; }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            /*context.MapRoute(
                "Assertion_default",
                "Assertion/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );*/
        }
    }
}