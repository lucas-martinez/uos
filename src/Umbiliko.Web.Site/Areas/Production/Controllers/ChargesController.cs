﻿using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Uos.Web.Areas.Production.Controllers
{
    using Constants;
    using Entities;
    using Models;
    using Services;
    using Web.Models;

    [Mvc.Feature(Features.Production), RouteArea(Features.Production), RoutePrefix(Collections.Charges)]
    public class ChargesController : Mvc.EntityController<ChargeEntity, long>
    {
        // GET: production/charges
        [HttpGet, Mvc.Activity(Activities.DestroyCharges), Route(Actions.Default)]
        public ActionResult Index()
        {
            return View();
        }

        // GET: production/charges/download
        [HttpGet, Mvc.Activity(Activities.DownloadCharges), Route(Actions.Download)]
        public async Task<ActionResult> DownloadCharges()
        {
            var model = new DownloadModel();

            //

            var json = JsonConvert.SerializeObject(model, Formatting.Indented);

            var content = System.Text.Encoding.Unicode.GetBytes(json);

            return File(content, "application/json", string.Format("Uos-{0:yyyyMMddHHmm}.segments.json", DateTime.Now));
        }

        #region - production/charges/api -

        // GET: production/charges/api
        [HttpGet, Mvc.Activity(Activities.ReadSegments), Route(Actions.Api)]
        public async Task<ActionResult> GetCharges([DataSourceRequest]DataSourceRequest request)
        {
            Func<ChargeEntity, ChargeViewModel> selector = e => e.ToViewModel();

            IQueryable<ChargeEntity> source = DbSet;

            var filters = request.ResetFilters();

            if (filters.Count > 0)
            {
                using (var service = new SearchService(DbContext))
                {
                    //service.FilterCharges(ref source, service.ToFilter(filters));
                }
            }

            request.DefaultSort("CompanyCode", "CustomerCode", "ConsumerCode");

            return Json(source.ToDataSourceResult(request, selector), JsonRequestBehavior.AllowGet);
        }

        #endregion - production/charges/api -
    }
}