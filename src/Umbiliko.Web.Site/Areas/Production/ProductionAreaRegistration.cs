﻿using System.Web.Mvc;

namespace Uos.Web.Areas.Production
{
    using Constants;

    public class ProductionAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get { return Features.Production; }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            /*context.MapRoute(
                "Production_default",
                "Production/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );*/
        }
    }
}