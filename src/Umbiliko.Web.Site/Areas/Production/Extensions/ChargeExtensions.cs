﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Production.Models
{
    using Entities;
    using Models;

    public static class ChargeExtensions
    {
        /*public static ChargeFileModel ToFileModel(this ChargeEntity entity)
        {
            return entity == null ? null :
                new ChargeFileModel
                {
                    Description = entity.Description,
                    ChargeName = entity.ChargeName
                };
        }*/

        public static ChargeRowModel ToRowModel(this ChargeEntity entity)
        {
            return entity == null ? null :
                new ChargeRowModel
                {
                };
        }
        
        public static ChargeViewModel ToViewModel(this ChargeEntity entity)
        {
            return entity == null ? null :
                new ChargeViewModel
                {
                };
        }
    }
}
