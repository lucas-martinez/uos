﻿using System.ComponentModel.DataAnnotations;

namespace Uos.Web.Areas.Configuration.Models
{
    using Constants;

    public class ProductSelectModel
    {
        [MaxLength(Lengths.ProductNameLength), Required]
        public string ProductName { get; set; }
    }
}
