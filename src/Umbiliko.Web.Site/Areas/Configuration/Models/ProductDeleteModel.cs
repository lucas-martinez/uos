﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Configuration.Models
{
    public class ProductDeleteModel
    {
        [Required]
        public int Identifier { get; set; }
    }
}
