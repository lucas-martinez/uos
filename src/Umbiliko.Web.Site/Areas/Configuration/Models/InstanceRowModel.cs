﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Configuration.Models
{
    public class InstanceRowModel
    {
        public string DomainName { get; set; }

        public Guid Identity { get; set; }

        public string IpAddress { get; set; }

        public string MachineName { get; set; }

        public int ProcessId { get; set; }

        public string ProductVersion { get; set; }
    }
}
