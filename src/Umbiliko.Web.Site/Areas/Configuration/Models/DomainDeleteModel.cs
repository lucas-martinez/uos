﻿using System.ComponentModel.DataAnnotations;

namespace Uos.Web.Areas.Configuration.Models
{
    using Constants;

    public class DomainDeleteModel
    {
        [MaxLength(Lengths.DomainNameLength), Required]
        public string DomainName { get; set; }
    }
}
