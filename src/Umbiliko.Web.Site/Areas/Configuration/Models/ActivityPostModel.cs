﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Uos.Web.Areas.Configuration.Models
{
    using Constants;

    public class ActivityPostModel
    {
        [MaxLength(Lengths.ActivityNameLength), Required]
        public string ActivityName { get; set; }

        [MaxLength(Lengths.DescriptionLength)]
        public string Description { get; set; }

        [MaxLength(Lengths.FeatureNameLength), Required]
        public string FeatureName { get; set; }

        public string NewRoles { get; set; }

        [MaxLength(Lengths.ProductNameLength), Required]
        public string ProductName { get; set; }

        public ICollection<string> Roles { get; set; }
    }
}
