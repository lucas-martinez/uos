﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Uos.Web.Areas.Configuration.Models
{
    using Constants;
    
    public class ActivityViewModel
    {
        [MaxLength(Lengths.ActivityNameLength)]
        public string ActivityName { get; set; }

        [MaxLength(Lengths.DescriptionLength)]
        public string Description { get; set; }

        [MaxLength(Lengths.FeatureNameLength)]
        public string FeatureName { get; set; }

        [Required]
        public int Identifier { get; set; }

        public string NewRoles { get; set; }

        [MaxLength(Lengths.ProductNameLength)]
        public string ProductName { get; set; }

        public ICollection<string> Roles { get; set; }
    }
}
