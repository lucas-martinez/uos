﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Configuration.Models
{
    public class FeatureListModel
    {
        public string FeatureName { get; set; }
    }
}
