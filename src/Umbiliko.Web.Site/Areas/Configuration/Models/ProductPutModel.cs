﻿using System.ComponentModel.DataAnnotations;

namespace Uos.Web.Areas.Configuration.Models
{
    using Constants;

    public class ProductPutModel
    {
        [MaxLength(Lengths.VersionLength)]
        public string AlphaVersion { get; set; }

        [MaxLength(Lengths.VersionLength)]
        public string BetaVersion { get; set; }

        [MaxLength(Lengths.DescriptionLength)]
        public string Description { get; set; }

        [Required]
        public int Identifier { get; set; }

        [MaxLength(Lengths.ProductNameLength)]
        public string ProductName { get; set; }

        [MaxLength(Lengths.VersionLength)]
        public string ReleaseVersion { get; set; }
    }
}
