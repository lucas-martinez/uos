﻿using System.ComponentModel.DataAnnotations;

namespace Uos.Web.Areas.Configuration.Models
{
    using Constants;

    public class FeatureRowModel
    {
        [MaxLength(Lengths.FeatureNameLength), Required]
        public string FeatureName { get; set; }

        [MaxLength(Lengths.DescriptionLength)]
        public string Description { get; set; }

        [Required]
        public int Identifier { get; set; }

        [MaxLength(Lengths.ProductNameLength), Required]
        public string ProductName { get; set; }
    }
}
