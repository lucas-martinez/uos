﻿using System.ComponentModel.DataAnnotations;

namespace Uos.Web.Areas.Configuration.Models
{
    using Constants;

    public class DomainViewModel
    {
        [MaxLength(Lengths.DescriptionLength)]
        public string Description { get; set; }

        [MaxLength(Lengths.DomainNameLength), Required]
        public string DomainName { get; set; }

        [Required]
        public string Environment { get; set; }

        [Required]
        public string Identifier { get; set; }

        [MaxLength(Lengths.ProductNameLength), Required]
        public string ProductName { get; set; }
    }
}
