﻿using System.ComponentModel.DataAnnotations;

namespace Uos.Web.Areas.Configuration.Models
{
    using Constants;

    public class FeaturePostModel
    {
        [MaxLength(Lengths.FeatureNameLength), Required]
        public string FeatureName { get; set; }

        [MaxLength(Lengths.DescriptionLength)]
        public string Description { get; set; }

        [MaxLength(Lengths.ProductNameLength), Required]
        public string ProductName { get; set; }
    }
}
