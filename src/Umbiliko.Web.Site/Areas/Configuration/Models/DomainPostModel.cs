﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Configuration.Models
{
    using Constants;

    public class DomainPostModel
    {
        [MaxLength(Lengths.DescriptionLength)]
        public string Description { get; set; }

        [MaxLength(Lengths.DomainNameLength), Required]
        public string DomainName { get; set; }

        [MaxLength(Lengths.EnvironmentLength), Required]
        public string Environment { get; set; }

        [MaxLength(Lengths.ProductNameLength), Required]
        public string ProductName { get; set; }
    }
}
