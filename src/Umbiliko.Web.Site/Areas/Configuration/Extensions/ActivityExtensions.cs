﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Configuration.Models
{
    using Constants;
    using Entities;

    public static class ActivityExtensions
    {
        public static ActivityRowModel ToRowModel(this ActivityEntity entity)
        {
            return entity == null ? null :
                new ActivityRowModel
                {
                    ActivityName = entity.ActivityName,
                    FeatureName = entity.FeatureName,
                    Description = entity.Description,
                    Identifier = entity.Id,
                    ProductName = entity.ProductName,
                    Roles = entity.Roles.Split(Characters.Space)
                };
        }

        public static string AsSearchResultValue(this ActivityEntity entity)
        {
            return string.Format("Product: {2}, Feature: {1}, Activity: {0}", entity.ActivityName, entity.FeatureName, entity.ProductName);
        }

        public static ActivityViewModel ToViewModel(this ActivityEntity entity)
        {
            return entity == null ? null :
                new ActivityViewModel
                {
                    ActivityName = entity.ActivityName,
                    FeatureName = entity.FeatureName,
                    Description = entity.Description,
                    Identifier = entity.Id,
                    ProductName = entity.ProductName,
                    Roles = entity.Roles.Split(Characters.Space)
                };
        }
    }
}
