﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Configuration.Models
{
    using Entities;

    public static class FeatureExtensions
    {
        public static FeatureRowModel ToRowModel(this FeatureEntity entity)
        {
            return entity == null ? null :
                new FeatureRowModel
                {
                    Description = entity.Description,
                    Identifier = entity.Id,
                    FeatureName = entity.FeatureName,
                    ProductName = entity.ProductName
                };
        }

        public static string AsSearchResultValue(this FeatureEntity entity)
        {
            return string.Format("Product: {1}, Feature: {0}", entity.FeatureName, entity.ProductName);
        }

        public static FeatureViewModel ToViewModel(this FeatureEntity entity)
        {
            return entity == null ? null :
                new FeatureViewModel
                {
                    Description = entity.Description,
                    Identifier = entity.Id,
                    FeatureName = entity.FeatureName,
                    ProductName = entity.ProductName,
                };
        }
    }
}
