﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Configuration.Models
{
    using Entities;

    public static class ProductExtensions
    {
        public static ProductRowModel ToRowModel(this ProductEntity entity)
        {
            return entity == null ? null :
                new ProductRowModel
                {
                    AlphaVersion = entity.AlphaVersion,
                    BetaVersion = entity.BetaVersion,
                    Description = entity.Description,
                    Identifier = entity.Id,
                    ProductName = entity.ProductName,
                    ReleaseVersion = entity.ReleaseVersion
                };
        }

        public static string AsSearchResultValue(this ProductEntity entity)
        {
            return string.Format("Product: {0}", entity.ProductName);
        }

        public static ProductViewModel ToViewModel(this ProductEntity entity)
        {
            return entity == null ? null :
                new ProductViewModel
                {
                    AlphaVersion = entity.AlphaVersion,
                    BetaVersion = entity.BetaVersion,
                    Description = entity.Description,
                    Identifier = entity.Id,
                    ProductName = entity.ProductName,
                    ReleaseVersion = entity.ReleaseVersion
                };
        }
    }
}
