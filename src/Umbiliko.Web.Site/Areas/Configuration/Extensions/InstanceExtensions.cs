﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Configuration.Models
{
    using Entities;

    public static class InstanceExtensions
    {
        public static InstanceRowModel ToRowModel(this InstanceEntity entity)
        {
            return entity == null ? null :
                new InstanceRowModel
                {
                    DomainName = entity.DomainName,
                    Identity = entity.Id,
                    IpAddress = entity.IpAddress,
                    MachineName = entity.MachineName,
                    ProcessId = entity.ProcessId,
                    ProductVersion = entity.ProductVersion
                };
        }
    }
}
