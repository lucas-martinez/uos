﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Configuration.Models
{
    using Entities;

    public static class DomainExtensions
    {
        public static DomainRowModel ToRowModel(this DomainEntity entity)
        {
            return entity == null ? null :
                new DomainRowModel
                {
                    Description = entity.Description,
                    DomainName = entity.DomainName,
                    Environment = entity.Environment,
                    Identifier = entity.DomainName,
                    ProductName = entity.ProductName
                };
        }

        public static string AsSearchResultValue(this DomainEntity entity)
        {
            return string.Format("Domain: {0}, Product: {1}", entity.DomainName, entity.ProductName);
        }

        public static DomainViewModel ToViewModel(this DomainEntity entity)
        {
            return entity == null ? null :
                new DomainViewModel
                {
                    Description = entity.Description,
                    DomainName = entity.DomainName,
                    Environment = entity.Environment,
                    Identifier = entity.DomainName,
                    ProductName = entity.ProductName
                };
        }
    }
}
