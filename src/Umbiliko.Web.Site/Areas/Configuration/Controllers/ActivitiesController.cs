﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Uos.Web.Areas.Configuration.Controllers
{
    using Constants;
    using Entities;
    using Models;
    using Services;

    [Mvc.Feature(Features.Configuration), RouteArea(Features.Configuration), RoutePrefix(Collections.Activities)]
    public class ActivitiesController : Mvc.EntityController<ActivityEntity, int>
    {
        // POST: configuration/activities
        [HttpPost, Route(Actions.Default)]
        public async Task<ActionResult> ListActivities(string product, string feature)
        {
            var activities = await DbSet.Where(Predicate.Apply(product: product, feature: feature)).AsExpandableAsync().OrderBy(e => e.ActivityName).Select(e => e.ActivityName).ToListAsync();

            return Json(activities);
        }

        #region - configuration/activities/api -

        // DELETE: configuration/activities/api
        [HttpDelete, Route(Actions.Api)]
        public async Task<ActionResult> DeleteActivities([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<ActivityDeleteModel> models)
        {
            return await DeleteAsync(request, models, m => m.Identifier, e => e.ToViewModel(),
                (missing) => string.Format("One or more activities were not found: [{0}]", missing.Select(item => item.Identifier)));
        }

        // GET: configuration/activities/api
        [HttpGet, Route(Actions.Api)]
        public async Task<ActionResult> GetActivities(string product, string feature, [DataSourceRequest]DataSourceRequest request)
        {
            return await SelectAsync(request, request.ResetFilters().ToFilter().ApplyTo(Predicate).Apply(product: product, feature: feature), e => e.ToRowModel(), "ProductName", "FeatureName", "ActivityName");
        }

        // POST: configuration/activities/api
        [HttpPost, Route(Actions.Api)]
        public async Task<ActionResult> PostActivities([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<ActivityPostModel> models)
        {
            if (!ModelState.IsValid)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var entities = new List<ActivityEntity>();
            var dbActivities = DbContext.Set<ActivityEntity>();

            foreach (var model in models)
            {
                var entity = dbActivities.Create();

                entity.ActivityName = model.ActivityName;
                entity.FeatureName = model.FeatureName;
                entity.Description = model.Description;
                entity.ProductName = model.ProductName;

                dbActivities.Add(entity);

                entities.Add(entity);
            }

            return await SaveChangesAsync(() =>
            {
                Response.StatusCode = (int)HttpStatusCode.Created;
                Response.StatusDescription = Request.RawUrl;

                Func<ActivityEntity, ActivityViewModel> selector = e => e.ToViewModel();

                return Json(entities.ToDataSourceResult(request, selector));
            });
        }

        // PUT: configuration/activities/api
        [HttpPut, Route(Actions.Api)]
        public async Task<ActionResult> PutActivities([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<ActivityPutModel> models)
        {
            if (!ModelState.IsValid)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return await UpdateAsync(request, models, m => m.Identifier, e => e.ToViewModel(), updater: (entity, model) =>
            {
                if (!string.IsNullOrEmpty(model.Description)) entity.Description = model.Description;
            });
        }

        #endregion - configuration/activities/api -
    }
}