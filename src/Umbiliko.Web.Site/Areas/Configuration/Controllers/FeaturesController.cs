﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Uos.Web.Areas.Configuration.Controllers
{
    using Constants;
    using Entities;
    using Models;
    using Services;

    [Mvc.Feature(Features.Configuration), RouteArea(Features.Configuration), RoutePrefix(Collections.Features)]
    public class FeaturesController : Mvc.EntityController<FeatureEntity, int>
    {
        // GET: configuration/features
        public ActionResult Index()
        {
            return View();
        }

        // POST: configuration/features
        [HttpPost, Route(Actions.Default)]
        public async Task<ActionResult> ListFeatures(string product)
        {
            var activities = await DbSet.Where(Predicate.Apply(product: product)).AsExpandableAsync().OrderBy(e => e.FeatureName).Select(e => e.FeatureName).ToListAsync();

            return Json(activities);
        }

        #region - configuration/features/api -

        // DELETE: configuration/features/api
        [HttpDelete, Route(Actions.Api)]
        public async Task<ActionResult> DeleteFeatures([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<FeatureDeleteModel> models)
        {
            return await DeleteAsync(request, models, m => m.Identifier, e => e.ToViewModel(),
                (missing) => string.Format("One or more features were not found: [{0}]", missing.Select(item => item.Identifier)));
        }

        // GET: configuration/features/api
        [HttpGet, Route(Actions.Api)]
        public async Task<ActionResult> GetFeatures(string product, [DataSourceRequest]DataSourceRequest request)
        {
            return await SelectAsync(request, request.ResetFilters().ToFilter().ApplyTo(Predicate).Apply(product: product), e => e.ToRowModel(), "ProductName", "FeatureName");
        }

        // POST: configuration/features/api
        [HttpPost, Route(Actions.Api)]
        public async Task<ActionResult> PostFeatures([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<FeaturePostModel> models)
        {
            if (!ModelState.IsValid)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var entities = new List<FeatureEntity>();
            var conflicts = new List<string>();
            var missing = new List<string>();
            var errors = new List<string>();
            var dbProducts = DbContext.Set<ProductEntity>();
            foreach (var group in models.GroupBy(m => m.ProductName))
            {
                var site = await dbProducts.Include(e => e.Features).SingleOrDefaultAsync(e => e.ProductName == group.Key);

                if (site == null)
                {
                    missing.Add(group.Key);
                    continue;
                }

                foreach (var model in group)
                {
                    if (site.Features.Any(e => e.FeatureName == model.FeatureName))
                    {
                        conflicts.Add(string.Format("{0} ({1})", model.FeatureName, model.ProductName));

                        continue;
                    }

                    var entity = DbSet.Create();

                    entity.FeatureName = model.FeatureName ?? string.Empty;
                    entity.Description = model.Description;
                    entity.ProductName = model.ProductName;

                    DbSet.Add(entity);

                    entities.Add(entity);
                }
            }

            if (missing.Count > 0)
            {
                errors.Add(string.Format("One or more sites were not found: [{0}]", string.Join(", ", missing)));
            }
            
            if (conflicts.Count > 0)
            {
                errors.Add("Feature name conflicts: " + string.Join(", ", conflicts));
            }
            
            return await SaveChangesAsync(() =>
            {
                if (errors.Count > 0)
                {
                    Response.StatusDescription = string.Format("There were one or more errors: ", string.Join(". ", errors));
                }

                Response.StatusCode = (int)HttpStatusCode.Created;
                Response.StatusDescription = Request.RawUrl;

                Func<FeatureEntity, FeatureViewModel> selector = e => e.ToViewModel();

                return Json(entities.ToDataSourceResult(request, selector));
            });
        }

        // PUT: configuration/features/api
        [HttpPut, Route(Actions.Api)]
        public async Task<ActionResult> PutFeatures([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<FeaturePutModel> models)
        {
            if (!ModelState.IsValid)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return await UpdateAsync(request, models, m => m.Identifier, e => e.ToViewModel(), updater: (entity, model) =>
            {
                if (!string.IsNullOrEmpty(model.Description)) entity.Description = model.Description;
            });
        }

        #endregion - configuration/features/api -
    }
}