﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Uos.Web.Areas.Configuration.Controllers
{
    using Constants;
    using Entities;
    using Models;

    [Mvc.Feature(Features.Configuration), RouteArea(Features.Configuration), RoutePrefix(Collections.Instances)]
    public class InstancesController : Mvc.EntityController<InstanceEntity, Guid>
    {
        // GET: configurationinstances
        [HttpGet, Mvc.Activity(Activities.DisplayInstances), Route(Actions.Default)]
        public ActionResult Index()
        {
            return View();
        }

        #region - configuration/instances/api -

        // GET: configuration/instances/api
        [HttpGet, Mvc.Activity(Activities.ReadInstances), Route(Actions.Api)]
        public async Task<ActionResult> GetInstances(string domain, [DataSourceRequest]DataSourceRequest request)
        {
            Func<InstanceEntity, InstanceRowModel> selector = e => e.ToRowModel();

            IQueryable<InstanceEntity> query = DbSet;// await GetEntitiesAsync();

            if (!string.IsNullOrEmpty(domain)) query = query.Where(e => e.DomainName == domain);
            
            query = query.OrderBy(e => e.DomainName).ThenBy(e => e.IpAddress);

            return Json(query.ToDataSourceResult(request, selector), JsonRequestBehavior.AllowGet);
        }

        #endregion - configuration/instances/api -
    }
}