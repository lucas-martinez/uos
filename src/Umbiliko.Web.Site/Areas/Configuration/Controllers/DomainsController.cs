﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Uos.Web.Areas.Configuration.Controllers
{
    using Constants;
    using Entities;
    using Inquiry;
    using Models;
    using Services;
    using System.Linq.Expressions;
    using Web.Models;

    [Mvc.Feature(Features.Configuration), RouteArea(Features.Configuration), RoutePrefix(Collections.Domains)]
    public class DomainsController : Mvc.EntityController<DomainEntity, string>
    {
        // GET: configuration/domains
        [HttpGet, Route(Actions.Default)]
        public ActionResult Index()
        {
            return View();
        }

        // GET: configuration/domains/download
        [HttpGet, Route(Actions.Download)]
        public async Task<ActionResult> DownloadDomains()
        {
            var model = new DownloadModel();

            //

            var json = JsonConvert.SerializeObject(model, Formatting.Indented);

            var content = System.Text.Encoding.Unicode.GetBytes(json);

            return File(content, "application/json", string.Format("Uos-{0:yyyyMMddHHmm}.domains.json", DateTime.Now));
        }

        // POST: configuration/domains
        [HttpPost, Route(Actions.Default)]
        public async Task<ActionResult> ListDomains()
        {
            return Json(await DbSet.OrderBy(e => e.DomainName).Select(e => e.DomainName).ToListAsync(), JsonRequestBehavior.AllowGet);
        }

        // GET|POST: configuration/domains/upload
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post), Route(Actions.Upload)]
        public async Task<ActionResult> UploadDomains(UploadModel model)
        {
            if (!ModelState.IsValid)
            {
                return PartialView("Upload");
            }

            switch (model.File.ContentType)
            {
                case "application/json":
                case "application/octet-stream":

                    var content = new byte[model.File.InputStream.Length];

                    model.File.InputStream.Read(content, 0, content.Length);

                    var json = System.Text.Encoding.Unicode.GetString(content);

                    var data = JsonConvert.DeserializeObject<DownloadModel>(json);

                    //

                    return await SaveChangesAsync(() =>
                    {
                        return PartialView("Upload");
                    });
            }

            return PartialView("Upload");
        }

        #region - configuration/domains/api -

        // DELETE: domains/api
        [HttpDelete, Route(Actions.Api)]
        public async Task<ActionResult> DeleteDomains([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<DomainDeleteModel> models)
        {
            return await DeleteAsync(request, models, m => m.DomainName, e => e.ToViewModel(),
                (missing) => string.Format("One or more domains were not found: [{0}]", missing.Select(item => item.DomainName)));
        }

        // GET: domains/api
        [HttpGet, Route(Actions.Api)]
        public async Task<ActionResult> GetDomains([DataSourceRequest]DataSourceRequest request)
        {
            return await SelectAsync(request, request.ResetFilters().ToFilter().ApplyTo(Predicate), e => e.ToRowModel(), "DomainName");
        }

        // POST: domains/api
        [HttpPost, Route(Actions.Api)]
        public async Task<ActionResult> PostDomains([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<DomainPostModel> models)
        {
            var account = Authentication.CurrentSession.Account;
            var comapanyCode = account != null ? account.CompanyCode : null;

            return await InsertAsync(request, models, e => e.ToViewModel(), (entity, model) =>
            {
                entity.Description = model.Description;
                entity.DomainName = model.DomainName;
                entity.CompanyCode = comapanyCode;
                entity.Environment = model.Environment;
                entity.ProductName = model.ProductName;
            });
        }

        // PUT: domains/api
        [HttpPut, Route(Actions.Api)]
        public async Task<ActionResult> PutDomains([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<DomainPutModel> models)
        {
            return await UpdateAsync(request, models, m => m.DomainName, e => e.ToViewModel(), updater: (entity, model) =>
            {
                if (!string.IsNullOrEmpty(model.Description)) entity.Description = model.Description;
                if (!string.IsNullOrEmpty(model.ProductName)) entity.ProductName = model.ProductName;
                if (!string.IsNullOrEmpty(model.Environment)) entity.Environment = model.Environment;
            });
        }

        #endregion - configuration/domains/api -
    }
}