﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Uos.Web.Areas.Configuration.Controllers
{
    using Constants;
    using Entities;
    using Models;
    using Services;
    using Web.Models;

    [Mvc.Feature(Features.Configuration), RouteArea(Features.Configuration), RoutePrefix(Collections.Products)]
    public class ProductsController : Mvc.EntityController<ProductEntity, int>
    {
        // GET: configuration/products
        [HttpGet, Mvc.Activity(Activities.DisplayProducts), Route(Actions.Default)]
        public ActionResult Index()
        {
            return View();
        }

        // POST: configuration/products/download
        [HttpGet, Mvc.Activity(Activities.DownloadProducts), Route(Actions.Download)]
        public async Task<ActionResult> DownloadProducts()
        {
            var model = new DownloadModel();

            //

            var json = JsonConvert.SerializeObject(model, Formatting.Indented);

            var content = System.Text.Encoding.Unicode.GetBytes(json);

            return File(content, "application/json", string.Format("Uos-{0:yyyyMMddHHmm}.products.json", DateTime.Now));
        }

        // POST: configuration/products
        [HttpPost, Mvc.Activity(Activities.ListProducts), Route(Actions.Default)]
        public async Task<ActionResult> ListProducts()
        {
            var products = await DbSet.OrderBy(e => e.ProductName).Select(e => e.ProductName).AsExpandableAsync().ToListAsync();

            return Json(products);
        }

        // POST: configuration/products/upload
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post), Mvc.Activity(Activities.UploadProducts), Route(Actions.Upload)]
        public async Task<ActionResult> UploadProducts(UploadModel model)
        {
            if (!ModelState.IsValid)
            {
                return PartialView("Upload");
            }

            switch (model.File.ContentType)
            {
                case "application/json":
                case "application/octet-stream":

                    var content = new byte[model.File.InputStream.Length];

                    model.File.InputStream.Read(content, 0, content.Length);

                    var json = System.Text.Encoding.Unicode.GetString(content);

                    var data = JsonConvert.DeserializeObject<DownloadModel>(json);

                    //

                    return await SaveChangesAsync(() =>
                    {
                        return PartialView("Upload");
                    });
            }

            return PartialView("Upload");
        }

        #region - configuration/products/api -

        // DELETE: configuration/products/api
        [HttpDelete, Mvc.Activity(Activities.DestroyProducts), Route(Actions.Api)]
        public async Task<ActionResult> DeleteProducts([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<ProductDeleteModel> models)
        {
            return await DeleteAsync(request, models, m => m.Identifier, e => e.ToViewModel(),
                (missing) => string.Format("One or more products were not found: [{0}]", missing.Select(item => item.Identifier)));
        }

        // GET: configuration/products/api
        [HttpGet, Mvc.Activity(Activities.ReadProducts), Route(Actions.Api)]
        public async Task<ActionResult> GetProducts([DataSourceRequest]DataSourceRequest request)
        {
            return await SelectAsync(request, request.ResetFilters().ToFilter().ApplyTo(Predicate), e => e.ToRowModel(), "ProductName");
        }

        // POST: configuration/products/api
        [HttpPost, Mvc.Activity(Activities.CreateProducts), Route(Actions.Api)]
        public async Task<ActionResult> PostProducts([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<ProductPostModel> models)
        {
            return await InsertAsync(request, models, e => e.ToViewModel(), (entity, model) =>
            {
                entity.AlphaVersion = model.AlphaVersion;
                entity.BetaVersion = model.BetaVersion;
                entity.ReleaseVersion = model.ReleaseVersion;
            });
        }

        // PUT: configuration/products/api
        [HttpPut, Mvc.Activity(Activities.UpdateProducts), Route(Actions.Api)]
        public async Task<ActionResult> PutProducts([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<ProductPutModel> models)
        {
            if (!ModelState.IsValid)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return await UpdateAsync(request, models, m => m.Identifier, e => e.ToViewModel(), updater: (entity, model) =>
            {
                if (!string.IsNullOrEmpty(model.AlphaVersion)) entity.AlphaVersion = model.AlphaVersion;
                if (!string.IsNullOrEmpty(model.BetaVersion)) entity.BetaVersion = model.BetaVersion;
                if (!string.IsNullOrEmpty(model.Description)) entity.Description = model.Description;
                if (!string.IsNullOrEmpty(model.ReleaseVersion)) entity.ReleaseVersion = model.ReleaseVersion;
            });
        }

        #endregion - configuration/products/api -
    }
}