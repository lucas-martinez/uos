﻿using System.Web.Mvc;

namespace Uos.Web.Areas.Configuration
{
    using Constants;

    public class ConfigurationAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get { return Features.Configuration; }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            /*context.MapRoute(
                "Configuration_default",
                "Configuration/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );*/
        }
    }
}