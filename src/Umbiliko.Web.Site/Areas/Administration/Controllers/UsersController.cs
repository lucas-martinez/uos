﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Uos.Web.Areas.Domain.Controllers
{
    using Constants;
    using Entities;
    using Inquiry;
    using Models;
    using Services;
    using Web.Models;

    [/*Authorize(Roles = Roles.Administrator), */Mvc.Feature(Features.Administration), RouteArea(Features.Administration), RoutePrefix(Collections.Users)]
    public class UsersController : Mvc.EntityController<UserEntity, string>
    {
        // GET: administration/users
        [HttpGet, Mvc.Activity(Activities.DisplayUsers, Level = AccessLevels.Internal), Route(Actions.Default)]
        public ActionResult Index()
        {
            return View();
        }

        // GET: administration/users/download
        [HttpGet, Mvc.Activity(Activities.DownloadUsers, Level = AccessLevels.Internal), Route(Actions.Download)]
        public async Task<ActionResult> DownloadUsers()
        {
            var model = new DownloadModel();

            //

            var json = JsonConvert.SerializeObject(model, Formatting.Indented);

            var content = System.Text.Encoding.Unicode.GetBytes(json);

            return File(content, "application/json", string.Format("Uos-{0:yyyyMMddHHmm}.users.json", DateTime.Now));
        }

        // POST: administration/users
        [Mvc.Activity(Activities.ListUsers, Level = AccessLevels.Internal), HttpPost, Route(Actions.Default)]
        public async Task<ActionResult> ListUsers(string startsWith, string[] exclude)
        {
            if (string.IsNullOrEmpty(startsWith))
            {
                return Json(new string[] { });
            }
            
            var predicate = Predicate.And(e => e.UserName.StartsWith(startsWith));

            if (exclude != null && exclude.Length > 0)
            {
                predicate = predicate.And(e => !exclude.Contains(e.UserName));
            }

            var source = DbSet.Where(predicate).AsExpandableAsync();
            
            var result = await source.OrderBy(e => e.UserName).Select(e => e.UserName).AsExpandableAsync().ToListAsync();

            return Json(result);
        }

        // GET|POST: administration/users/upload
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post), Mvc.Activity(Activities.UploadUsers, Level = AccessLevels.Internal), Route(Actions.Upload)]
        public async Task<ActionResult> UploadUsers(UploadModel model)
        {
            if (!ModelState.IsValid)
            {
                return PartialView("Upload");
            }

            switch (model.File.ContentType)
            {
                case "application/json":
                case "application/octet-stream":

                    var content = new byte[model.File.InputStream.Length];

                    model.File.InputStream.Read(content, 0, content.Length);

                    var json = System.Text.Encoding.Unicode.GetString(content);

                    var data = JsonConvert.DeserializeObject<DownloadModel>(json);

                    //

                    return await SaveChangesAsync(() =>
                    {
                        return PartialView("Upload");
                    });
            }

            return PartialView("Upload");
        }

        #region - administration/users/api -

        // DELETE: administration/users/api
        [HttpDelete, Mvc.Activity(Activities.DestroyUsers, Level = AccessLevels.Internal), Route(Actions.Api)]
        public async Task<ActionResult> DeleteUsers([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<UserDeleteModel> models)
        {
            var names = models.Select(m => m.UserName).ToArray();

            var entities = await DbSet.Where(e => names.Contains(e.UserName)).ToListAsync();

            var missing = new List<string>();

            foreach (var name in names)
            {
                var entity = entities.SingleOrDefault(e => e.UserName == name);

                if (entity != null)
                {
                    DbSet.Remove(entity);
                }
                else
                {
                    missing.Add(name);
                }
            }

            return await SaveChangesAsync(() =>
            {
                if (missing.Count > 0)
                {
                    Response.StatusCode = (int)HttpStatusCode.Found;
                    Response.StatusDescription = string.Format("One or more entities were not found: [{0}]", string.Join(", ", missing));
                }

                Func<UserEntity, UserViewModel> selector = e => e.ToViewModel();

                return Json(entities.ToDataSourceResult(request, selector));
            });
        }

        // GET: administration/users/api
        [HttpGet, Mvc.Activity(Activities.ReadUsers, Level = AccessLevels.Internal), Route(Actions.Api)]
        public async Task<ActionResult> GetUsers([DataSourceRequest]DataSourceRequest request)
        {
            return await SelectAsync(request, request.ResetFilters().ToFilter().ApplyTo(Predicate), e => e.ToRowModel(), "UserName");
        }

        // POST: administration/users/api
        [HttpPost, Mvc.Activity(Activities.CreateUsers, Level = AccessLevels.Internal), Route(Actions.Api)]
        public async Task<ActionResult> PostUsers([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<UserPostModel> models)
        {
            if (!ModelState.IsValid)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var now = DateTime.UtcNow;

            var entities = new List<UserEntity>();

            foreach (var model in models)
            {
                var entity = new UserEntity
                {
                    ChangeUtc = now,
                    CreatedUtc = now,

                    EmailAddress = model.EmailAddress,
                    PhoneNumber = model.PhoneNumber,
                    UserName = model.UserName,
                    UserStatus = UserStatuses.Initial
                };

                entities.Add(entity);

                DbSet.Add(entity);
            }

            return await SaveChangesAsync(() =>
            {
                Response.StatusCode = (int)HttpStatusCode.Created;
                Response.StatusDescription = Request.RawUrl;

                Func<UserEntity, UserViewModel> selector = e => e.ToViewModel();

                return Json(entities.ToDataSourceResult(request, selector));
            });
        }

        // PUT: administration/users/api
        [HttpPut, Mvc.Activity(Activities.UpdateUsers, Level = AccessLevels.Internal), Route(Actions.Api)]
        public async Task<ActionResult> PutUsers([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<UserPutModel> models)
        {
            if (!ModelState.IsValid)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var names = models.Select(m => m.UserName).ToList();

            var entities = await DbSet.Where(e => names.Contains(e.UserName)).ToListAsync();

            var missing = new List<UserPutModel>();

            foreach (var model in models)
            {
                var entity = entities.SingleOrDefault(e => e.UserName == model.UserName);

                if (entity != null)
                {
                    entity.EmailAddress = model.EmailAddress;
                    entity.PhoneNumber = model.PhoneNumber;
                }
                else
                {
                    missing.Add(model);
                }
            }

            return await SaveChangesAsync(() =>
            {
                Func<UserEntity, UserViewModel> selector = e => e.ToViewModel();

                return Json(entities.ToDataSourceResult(request, selector));
            });
        }

        #endregion - administration/users/api -
    }
}