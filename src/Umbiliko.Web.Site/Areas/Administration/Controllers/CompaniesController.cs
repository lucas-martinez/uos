﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Uos.Web.Areas.Domain.Controllers
{
    using Constants;
    using Entities;
    using Models;
    using Inquiry;
    using Services;
    using Web.Models;

    [/*Authorize(Roles = Roles.Administrator), */Mvc.Feature(Features.Administration), RouteArea(Features.Administration), RoutePrefix(Collections.Companies)]
    public class CompaniesController : Mvc.EntityController<CompanyEntity, string>
    {
        // GET: administration/companies
        [HttpGet, Mvc.Activity(Activities.DisplayCompanies, Level = AccessLevels.Internal), Route(Actions.Default)]
        public ActionResult Index()
        {
            return View();
        }

        // GET: administration/companies/download
        [HttpGet, Mvc.Activity(Activities.DownloadCompanies, Level = AccessLevels.Internal), Route(Actions.Download)]
        public async Task<ActionResult> DownloadCompanies()
        {
            var model = new DownloadModel();

            //

            var json = JsonConvert.SerializeObject(model, Formatting.Indented);

            var content = System.Text.Encoding.Unicode.GetBytes(json);

            return File(content, "application/json", string.Format("Umbiliko-{0:yyyyMMddHHmm}.companies.json", DateTime.Now));
        }

        // GET: administration/companies/detail
        [HttpGet, Mvc.Activity(Activities.DetailCompany, Level = AccessLevels.Internal), Route(Actions.Detail)]
        public async Task<ActionResult> DetailCompany(string companyCode)
        {
            if (companyCode == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            companyCode = companyCode.ToUpperInvariant();

            var company = await DbSet
                .Include(e => e.Contracts)
                .Include(e => e.Contracts.Select(c => c.Subscriptions))
                .Include(e => e.Contracts.Select(c => c.Subscriptions.Select(s => s.Feature)))
                .Include(e => e.Providers)
                .Include(e => e.Providers.Select(c => c.Subscriptions))
                .Include(e => e.Providers.Select(c => c.Subscriptions.Select(s => s.Feature)))
                .SingleOrDefaultAsync(e => e.CompanyCode == companyCode);
            
            if (company == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }
            
            var result = company.ToViewModel();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        // POST: administration/companies
        [HttpPost, Mvc.Activity(Activities.ListCompanies, Level = AccessLevels.Internal), Route(Actions.Default)]
        public async Task<ActionResult> ListCompanies(string startsWith, [DataSourceRequest]DataSourceRequest request)
        {
            var predicate = Predicate;

            if (!string.IsNullOrEmpty(startsWith))
            {
                startsWith = startsWith.ToUpperInvariant();
                predicate = predicate.And(e => e.CompanyCode.StartsWith(startsWith));
            }

            var result = await DbSet.Where(Predicate).AsExpandableAsync().OrderBy(e => e.CompanyCode).Select(e => new CompanyListModel { CompanyCode = e.CompanyCode, CompanyName = e.CompanyName }).AsExpandableAsync().ToListAsync();
            
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        // GET|POST: administration/companies/upload
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post), Mvc.Activity(Activities.UploadCompanies, Level = AccessLevels.Internal), Route(Actions.Upload)]
        public async Task<ActionResult> UploadCompanies(UploadModel model)
        {
            if (!ModelState.IsValid)
            {
                return PartialView("Upload");
            }

            switch (model.File.ContentType)
            {
                case "application/json":
                case "application/octet-stream":

                    var content = new byte[model.File.InputStream.Length];

                    model.File.InputStream.Read(content, 0, content.Length);

                    var json = System.Text.Encoding.Unicode.GetString(content);

                    var data = JsonConvert.DeserializeObject<DownloadModel>(json);

                    //

                    return await SaveChangesAsync(() =>
                    {
                        return PartialView("Upload");
                    });
            }

            return PartialView("Upload");
        }

        #region - companies/api -

        // DELETE: administration/companies/api
        [HttpDelete, Mvc.Activity(Activities.DestroyCompanies, Level = AccessLevels.Internal), Route(Actions.Api)]
        public async Task<ActionResult> DeleteCompanies([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<CompanyDeleteModel> models)
        {
            return await DeleteAsync(request, models, m => m.CompanyCode, e => e.ToRowModel(),
                (missing) => string.Format("One or more companies were not found: [{0}]", missing.Select(item => item.CompanyCode)));
        }

        // GET: administration/companies/api
        [HttpGet, Mvc.Activity(Activities.ReadCompanies, Level = AccessLevels.Internal), Route(Actions.Api)]
        public async Task<ActionResult> GetCompanies([DataSourceRequest]DataSourceRequest request)
        {
            return await SelectAsync(request, request.ResetFilters().ToFilter().ApplyTo(Predicate), e => e.ToRowModel(), "CompanyName");
        }

        // POST: administration/companies/api
        [HttpPost, Mvc.Activity(Activities.CreateCompanies, Level = AccessLevels.Internal), Route(Actions.Api)]
        public async Task<ActionResult> PostCompanies([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<CompanyPostModel> models)
        {
            var now = DateTime.UtcNow;

            var company = await CompanyContext.FindAsync(DbContext, Authentication.CurrentDomain.CompanyCode);

            var product = await ProductContext.FindAsync(company, Authentication.CurrentDomain.ProductName);
            
            return await InsertAsync(request, models, e => e.ToRowModel(), async (entity, model) =>
            {
                entity.CompanyName = model.CompanyName;
                entity.CompanyType = model.CompanyType;
                entity.CompanyCode = model.CompanyCode;

                var customer = CompanyContext.FromEntity(entity, DbContext);

                var segment = SegmentContext.Create(customer, segment: model.SegmentCode, environment: model.Environment, description: model.Description);

                var consumer = ConsumerContext.Create(product, segment, credit: model.Credit, country: model.CountryCode, currency: model.CurrencyCode);

                foreach (var name in model.UserNames)
                {
                    var user = await UserContext.FindAsync(product.DbContext, name);

                    if (user != null)
                    {
                        var account = AccountContext.Create(segment, user, model.Roles.ToArray());
                    }
                }
            });
        }

        // PUT: administration/companies/api
        [HttpPut, Mvc.Activity(Activities.UpdateCompanies, Level = AccessLevels.Internal), Route(Actions.Api)]
        public async Task<ActionResult> PutCompanies([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<CompanyPutModel> models)
        {
            return await UpdateAsync(request, models, m => m.CompanyCode, e => e.ToRowModel(), (entity, model) =>
            {
                if (!string.IsNullOrEmpty(model.CompanyName)) entity.CompanyName = model.CompanyName;
            });
        }

        #endregion - companies/api -

        #region - helpers -

        /*protected override IQueryable<CompanyEntity> Select(List<string> identifiers)
        {
            return DbSet.Where(e => identifiers.Contains(e.CompanyCode));
        }*/

        #endregion - helpers -
    }
}