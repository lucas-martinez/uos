﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Domain.Models
{
    using Constants;

    public class CompanyPutModel
    {
        [MaxLength(Lengths.CompanyCodeLength), Required]
        public string CompanyCode { get; set; }

        [MaxLength(Lengths.CompanyNameLength)]
        public string CompanyName { get; set; }

        [MaxLength(Lengths.CompanyTypeLength)]
        public string CompanyType { get; set; }
    }
}
