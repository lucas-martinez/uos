﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Domain.Models
{
    public class UserDeleteModel
    {
        [Required]
        public string UserName { get; set; }
    }
}
