﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Domain.Models
{
    public class UserListModel
    {
        public string EmailAddress { get; set; }

        public string UserName { get; set; }
    }
}
