﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Domain.Models
{
    using Constants;

    public class CompanyViewModel
    {
        [MaxLength(Lengths.CompanyCodeLength), Required]
        public string CompanyCode { get; set; }

        [MaxLength(Lengths.CompanyNameLength), Required]
        public string CompanyName { get; set; }

        [Required]
        public string CompanyType { get; set; }

        public ICollection<Contract> Customers { get; set; }

        public ICollection<Contract> Suppliers { get; set; }

        public class Contract
        {
            public string CompanyName { get; set; }

            [MaxLength(Lengths.CountryCodeLength), Required]
            public string CountryCode { get; set; }

            public decimal Credit { get; set; }

            [MaxLength(Lengths.CurrencyCodeLength), Required]
            public string CurrencyCode { get; set; }

            [Required]
            public int Identifier { get; set; }
            
            public string ProductName { get; set; }

            public string SegmentCode { get; set; }

            public long Since { get; set; }

            public ICollection<Subscription> Subscriptions { get; set; }
        }

        public class Subscription
        {
            public string FeatureName { get; set; }

            public string Description { get; set; }

            [Required]
            public int Identifier { get; set; }

            public long Since { get; set; }

            public long? Until { get; set; }
        }
    }
}
