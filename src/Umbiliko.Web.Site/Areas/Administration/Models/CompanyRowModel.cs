﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Domain.Models
{
    using Constants;

    public class CompanyRowModel
    {
        [MaxLength(Lengths.CompanyCodeLength), Required]
        public string CompanyCode { get; set; }

        [MaxLength(Lengths.CompanyNameLength), Required]
        public string CompanyName { get; set; }

        [Required]
        public string CompanyType { get; set; }

        [Required]
        public string Identifier { get; set; }
    }
}
