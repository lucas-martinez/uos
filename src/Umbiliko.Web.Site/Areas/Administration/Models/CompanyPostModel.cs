﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Domain.Models
{
    using Constants;

    public class CompanyPostModel
    {
        [MaxLength(Lengths.CompanyCodeLength), Required]
        public string CompanyCode { get; set; }
        
        [MaxLength(Lengths.CompanyNameLength), Required]
        public string CompanyName { get; set; }

        [MaxLength(Lengths.CompanyTypeLength), Required]
        public string CompanyType { get; set; }

        [MaxLength(Lengths.CountryCodeLength), Required]
        public string CountryCode { get; set; }

        [Required]
        public decimal Credit { get; set; }

        [MaxLength(Lengths.CurrencyCodeLength), Required]
        public string CurrencyCode { get; set; }

        [MaxLength(Lengths.DescriptionLength), Required]
        public string Description { get; set; }

        [MaxLength(Lengths.EnvironmentLength), Required]
        public string Environment { get; set; }

        public string[] Roles { get; set; }

        [MaxLength(Lengths.SegmentCodeLength), Required]
        public string SegmentCode { get; set; }

        [MaxLength(Lengths.UserNameLength), Required]
        public string[] UserNames { get; set; }
    }
}
