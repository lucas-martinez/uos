﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Domain.Models
{
    using Constants;

    public class UserPostModel
    {
        [MaxLength(Lengths.EmailAddressLength)]
        public string EmailAddress { get; set; }

        [MaxLength(Lengths.PhoneNumberLength)]
        public string PhoneNumber { get; set; }

        [MaxLength(Lengths.UserNameLength)]
        public string UserName { get; set; }
    }
}
