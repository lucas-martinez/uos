﻿namespace Uos.Web.Areas.Domain.Models
{
    public class CompanyListModel
    {
        public string CompanyCode { get; set; }
        
        public string CompanyName { get; set; }
    }
}
