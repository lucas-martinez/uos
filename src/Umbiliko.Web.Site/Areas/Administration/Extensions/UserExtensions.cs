﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Domain.Models
{
    using Constants;
    using Entities;

    public static class UserExtensions
    {
        public static UserRowModel ToRowModel(this UserEntity entity)
        {
            return entity == null ? null :
                new UserRowModel
                {
                    EmailAddress = entity.EmailAddress,
                    PhoneNumber = entity.PhoneNumber,
                    UserName = entity.UserName,
                    Status = string.Join("|", Enum.GetValues(typeof(UserStatuses))
                        .OfType<UserStatuses>()
                        .Where(flag => entity.UserStatus.HasFlag(flag))
                        .Select(flag => flag.ToString()))
                };
        }

        public static string AsSearchResultValue(this UserEntity entity)
        {
            return string.Format("User: {0}, Email: {1}", entity.UserName, entity.EmailAddress);
        }

        public static UserViewModel ToViewModel(this UserEntity entity)
        {
            return entity == null ? null :
                new UserViewModel
                {
                    EmailAddress = entity.EmailAddress,
                    PhoneNumber = entity.PhoneNumber,
                    UserName = entity.UserName,
                    Status = string.Join("|", Enum.GetValues(typeof(UserStatuses))
                        .OfType<UserStatuses>()
                        .Where(flag => entity.UserStatus.HasFlag(flag))
                        .Select(flag => flag.ToString()))
                };
        }
    }
}
