﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Domain.Models
{
    using Constants;
    using Entities;
    using Utilities;

    public static class CompanyExtensions
    {
        public static CompanyRowModel ToRowModel(this CompanyEntity entity)
        {
            return entity == null ? null :
                new CompanyRowModel
                {
                    CompanyName = entity.CompanyName,
                    CompanyType = entity.CompanyType,
                    Identifier = entity.CompanyCode,
                    CompanyCode = entity.CompanyCode
                };
        }

        public static string AsSearchResultValue(this CompanyEntity entity)
        {
            return string.Format("Customer: {0}, Number: {1}", entity.CompanyName, entity.CompanyCode);
        }

        public static CompanyViewModel ToViewModel(this CompanyEntity entity)
        {
            return entity == null ? null :
                new CompanyViewModel
                {
                    CompanyName = entity.CompanyName,
                    CompanyType = entity.CompanyType,
                    Customers = entity.Contracts.Select(c => new CompanyViewModel.Contract
                    {
                        CompanyName = c.Customer.CompanyName,
                        CountryCode = c.CountryCode,
                        Credit = c.Credit,
                        CurrencyCode = c.CurrencyCode,
                        Identifier = c.Id,
                        ProductName = c.ProductName,
                        SegmentCode = c.ConsumerCode,
                        Since = c.SinceUtc.ToUnixMoment(),
                        Subscriptions = c.Subscriptions.Select(s => new CompanyViewModel.Subscription
                        {
                            FeatureName = s.FeatureName,
                            Description = s.Feature.Description,
                            Since = s.SinceUtc.ToUnixMoment(),
                            Until = s.UntilUtc.ToUnixMoment()
                        }).OrderBy(s => s.FeatureName).ToList()
                    }).OrderBy(c => c.CompanyName).ThenBy(c => c.ProductName).ToList(),
                    CompanyCode = entity.CompanyCode,
                    Suppliers = entity.Providers.Select(c => new CompanyViewModel.Contract
                    {
                        CompanyName = c.Company.CompanyName,
                        CountryCode = c.CountryCode,
                        Credit = c.Credit,
                        CurrencyCode = c.CurrencyCode,
                        Identifier = c.Id,
                        ProductName = c.ProductName,
                        SegmentCode = c.ConsumerCode,
                        Since = c.SinceUtc.ToUnixMoment(),
                        Subscriptions = c.Subscriptions.Select(s => new CompanyViewModel.Subscription
                        {
                            FeatureName = s.FeatureName,
                            Description = s.Feature.Description,
                            Since = s.SinceUtc.ToUnixMoment(),
                            Until = s.UntilUtc.ToUnixMoment()
                        }).OrderBy(s => s.FeatureName).ToList()
                    }).OrderBy(c => c.CompanyName).ThenBy(c => c.ProductName).ToList()
                };
        }
    }
}
