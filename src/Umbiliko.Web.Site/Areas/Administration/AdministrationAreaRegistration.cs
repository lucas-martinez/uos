﻿using System.Web.Mvc;

namespace Uos.Web.Areas.Domain
{
    using Constants;

    public class AdministrationAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get { return Features.Administration; }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            /*.MapRoute(
                "Administration_default",
                "Administration/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );*/
        }
    }
}