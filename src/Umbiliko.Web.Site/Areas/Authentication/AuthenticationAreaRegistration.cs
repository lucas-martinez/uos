﻿using System.Web.Mvc;

namespace Uos.Web.Areas.Authentication
{
    using Constants;

    public class AuthenticationAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get { return Features.Authentication; }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            /*context.MapRoute(
                "Authentication_default",
                "Authentication/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );*/
        }
    }
}