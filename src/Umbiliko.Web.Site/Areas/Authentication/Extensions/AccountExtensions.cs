﻿namespace Uos.Web.Areas.Authentication.Models
{
    using Constants;
    using Entities;

    public static class AccountExtensions
    {
        public static AccountRowModel ToRowModel(this AccountEntity entity)
        {
            return entity == null ? null :
                new AccountRowModel
                {
                    SegmentCode = entity.SegmentCode,
                    Identifier = entity.Id,
                    UserName = entity.UserName,
                    Roles = entity.Roles.Split(Characters.Space)
                    /*Status = string.Join("|", Enum.GetValues(typeof(UserStatuses))
                        .OfType<UserStatuses>()
                        .Where(flag => entity.UserStatus.HasFlag(flag))
                        .Select(flag => flag.ToString()))*/
                };
        }

        public static string AsSearchResultValue(this AccountEntity entity)
        {
            return string.Format("SegmentCode: {0}, User: {1}, Email: {2}", entity.SegmentCode, entity.UserName);
        }

        public static AccountViewModel ToViewModel(this AccountEntity entity)
        {
            return entity == null ? null :
                new AccountViewModel
                {
                    SegmentCode = entity.SegmentCode,
                    Identifier = entity.Id,
                    UserName = entity.UserName,
                    Roles = entity.Roles.Split(Characters.Space)
                    /*Status = string.Join("|", Enum.GetValues(typeof(UserStatuses))
                        .OfType<UserStatuses>()
                        .Where(flag => entity.UserStatus.HasFlag(flag))
                        .Select(flag => flag.ToString()))*/
                };
        }
    }
}
