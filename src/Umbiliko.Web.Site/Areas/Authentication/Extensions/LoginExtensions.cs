﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Authentication.Models
{
    using Models;
    using Entities;

    public static class LoginExtensions
    {
        public static LoginRowModel ToRowModel(this LoginEntity entity)
        {
            return entity.IsNull() ? null :
                new LoginRowModel
                {
                    Identifier = entity.Id,
                    LoginProvider = entity.LoginProvider,
                    UserName = entity.UserName
                };
        }

        public static string AsSearchResultValue(this LoginEntity entity)
        {
            return string.Format("Provider: {0}, User: {1}", entity.LoginProvider, entity.UserName);
        }

        public static LoginViewModel ToViewModel(this LoginEntity entity)
        {
            return entity.IsNull() ? null :
                new LoginViewModel
                {
                    Identifier = entity.Id,
                    LoginProvider = entity.LoginProvider,
                    UserName = entity.UserName
                };
        }
    }
}
