﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Authentication.Models
{
    using Entities;
    using Models;

    public static class SegmentExtensions
    {
        public static SegmentRowModel ToRowModel(this SegmentEntity entity)
        {
            return entity == null ? null :
                new SegmentRowModel
                {
                    SegmentCode = entity.SegmentCode,
                    Description = entity.Description,
                    Environment = entity.Environment,
                    Identifier = entity.Id,
                };
        }

        public static string AsSearchResultValue(this SegmentEntity entity)
        {
            return string.Format("SegmentCode: {0}", entity.SegmentCode);
        }

        public static SegmentSelectModel AsSelectModel(this SegmentEntity entity)
        {
            return entity == null ? null :
                new SegmentSelectModel
                {
                    SegmentCode = entity.SegmentCode
                };
        }

        public static SegmentViewModel ToViewModel(this SegmentEntity entity)
        {
            return entity == null ? null :
                new SegmentViewModel
                {
                    SegmentCode = entity.SegmentCode,
                    Description = entity.Description,
                    Environment = entity.Environment,
                    Identifier = entity.Id
                };
        }

        public static SegmentViewModel ToViewModel(this SegmentContext context)
        {
            return context == null ? null : context.Entity.ToViewModel();
        }
    }
}
