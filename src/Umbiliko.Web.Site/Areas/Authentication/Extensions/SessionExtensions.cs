﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Authentication.Models
{
    using Contracts;
    using Entities;
    using Models;
    using Utilities;

    public static class SessionExtensions
    {
        public static SessionRowModel ToRowModel(this SessionEntity entity)
        {
            return entity == null ? null :
                new SessionRowModel
                {
                    CreatedUtc = entity.CreatedUtc.ToUnixMoment(),
                    ExpiresUtc = entity.ExpiresUtc.ToUnixMoment(),
                    Identity = entity.Id,
                    IpAddress = entity.IpAddress,
                    SegmentCode = entity.SegmentCode,
                    UserName = entity.UserName
                };
        }

        public static SessionViewModel ToViewModel(this SessionEntity entity)
        {
            return entity == null ? null :
                new SessionViewModel
                {
                    CreatedUtc = entity.CreatedUtc.ToShortTimeString(),
                    ExpiresUtc = entity.ExpiresUtc.HasValue ? entity.ExpiresUtc.Value.ToShortTimeString() : null,
                    Id = entity.Id,
                    IpAddress = entity.IpAddress,
                    SegmentCode = entity.SegmentCode,
                    UserName = entity.UserName
                };
        }
    }
}
