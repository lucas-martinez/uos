﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Authentication.Models
{
    public class SessionPostModel
    {
        public string SegmentCode { get; set; }

        public string UserName { get; set; }
    }
}
