﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Authentication.Models
{
    using Constants;

    public class AccountViewModel
    {
        [MaxLength(Lengths.CompanyCodeLength), Required]
        public string CompanyCode { get; set; }

        [MaxLength(Lengths.CompanyNameLength), Required]
        public string CompanyName { get; set; }

        public string CompanyType { get; set; }

        [MaxLength(Lengths.EmailAddressLength)]
        public string EmailAddress { get; set; }

        public string Environment { get; set; }

        public int Identifier { get; set; }

        public ICollection<string> Roles { get; set; }

        [MaxLength(Lengths.SegmentCodeLength), Required]
        public string SegmentCode { get; set; }

        [MaxLength(Lengths.UserNameLength), Required]
        public string UserName { get; set; }
    }
}
