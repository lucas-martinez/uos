﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Uos.Web.Areas.Authentication.Models
{
    public class SessionDeleteModel
    {
        [Required]
        public Guid Identifier { get; set; }
    }
}
