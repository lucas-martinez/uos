﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Authentication.Models
{
    using Constants;

    public class UserViewModel
    {
        [MaxLength(Lengths.EmailAddressLength)]
        public string EmailAddress { get; set; }
        
        [MaxLength(Lengths.PhoneNumberLength)]
        public string PhoneNumber { get; set; }

        [MaxLength(Lengths.PasswordLength)]
        public string Password { get; set; }

        [ReadOnly(true)]
        public string Status { get; set; }

        [MaxLength(Lengths.UserNameLength), ReadOnly(true)]
        public string UserName { get; set; }
    }
}
