﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Authentication.Models
{
    using Constants;

    public class SegmemtMigrateModel
    {
        [MaxLength(Lengths.SegmentCodeLength), Required]
        public string SegmentCode { get; set; }
    }
}
