﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Authentication.Models
{
    using Constants;

    public class LoginPutModel
    {
        [Required]
        public int Identifier { get; set; }

        [MaxLength(Lengths.LoginProviderLength), Required]
        public string LoginProvider { get; set; }

        [MaxLength(Lengths.UserNameLength), Required]
        public string UserName { get; set; }
    }
}
