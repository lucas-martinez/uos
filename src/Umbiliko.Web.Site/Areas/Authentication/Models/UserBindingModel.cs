﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Authentication.Models
{
    using Binders;

    [DataContract, DataContractBinder]
    public class UserBindingModel
    {
        [DataMember(Name = "client")]
        public string Client { get; set; }

        [DataMember(Name = "company")]
        public string CompanyCode { get; set; }

        [DataMember(Name = "email"), EmailAddress]
        public string Email { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }
        
        [DataMember(Name = "password"), DataType(DataType.Password)]
        public string Password { get; set; }

        [DataMember(Name = "persist")]
        public bool Persist { get; set; }
        
        [DataMember(Name = "segment")]
        public string SegmentCode { get; set; }

        [DataMember(Name = "token")]
        public string Token { get; set; }
    }
}
