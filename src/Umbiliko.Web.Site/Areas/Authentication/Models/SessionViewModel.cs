﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Authentication.Models
{
    public class SessionViewModel
    {
        public string SegmentCode { get; set; }

        public Guid Id { get; set; }

        public string IpAddress { get; set; }
        
        public string CreatedUtc { get; set; }

        public string ExpiresUtc { get; set; }

        public string UserName { get; set; }
    }
}
