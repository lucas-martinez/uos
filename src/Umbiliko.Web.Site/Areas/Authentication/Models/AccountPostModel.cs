﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Authentication.Models
{
    using Constants;

    public class AccountPostModel
    {
        [MaxLength(Lengths.EmailAddressLength)]
        public string EmailAddress { get; set; }

        [MaxLength(Lengths.PhoneNumberLength)]
        public string OfficePhoneNumber { get; set; }

        public ICollection<string> Roles { get; set; }

        [MaxLength(Lengths.SegmentCodeLength), Required]
        public string SegmentCode { get; set; }

        [MaxLength(Lengths.UserNameLength), Required]
        public string UserName { get; set; }
    }
}
