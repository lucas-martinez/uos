﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Authentication.Models
{
    using Constants;

    public class CompanyViewModel
    {
        [MaxLength(Lengths.CompanyCodeLength), Required]
        public string CompanyCode { get; set; }

        [MaxLength(Lengths.CompanyNameLength), Required]
        public string CompanyName { get; set; }

        [Required]
        public string CompanyType { get; set; }

        public string ConsumerKey { get; set; }

        public string ConsumerSecret { get; set; }
    }
}
