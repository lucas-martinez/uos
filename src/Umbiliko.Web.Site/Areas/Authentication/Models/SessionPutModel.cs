﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Uos.Web.Areas.Authentication.Models
{
    public class SessionPutModel
    {
        public string SegmentCode { get; set; }

        [Required]
        public Guid Identifier { get; set; }

        public string UserName { get; set; }
    }
}
