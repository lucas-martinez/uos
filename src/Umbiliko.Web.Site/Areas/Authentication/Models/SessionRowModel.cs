﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Authentication.Models
{
    public class SessionRowModel
    {
        public string CompanyCode { get; set; }

        public long CreatedUtc { get; set; }

        public long? ExpiresUtc { get; set; }

        public Guid Identity { get; set; }

        public string IpAddress { get; set; }
        
        public string SegmentCode { get; set; }

        public string UserName { get; set; }
    }
}
