﻿using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Uos.Web.Areas.Authentication.Controllers
{
    using Constants;
    using Entities;
    using Models;

    [Mvc.Feature(Features.Authentication), RouteArea(Features.Authentication), RoutePrefix(My.Company)]
    public class MyCompanyController : Mvc.EntityController<CompanyEntity, string>
    {
        // GET: authentication/my-company
        [HttpGet, Mvc.Activity(Activities.DisplayMyCompany, Roles = Roles.Administrator), Route(Actions.Default)]
        public ActionResult Index()
        {
            return View();
        }

        // GET|POST: authentication/my-company/upload
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post), Mvc.Activity(Activities.UploadMyCompany), Route(Actions.Upload)]
        public async Task<ActionResult> UploadMyCompany(Web.Models.UploadModel model)
        {
            if (!ModelState.IsValid)
            {
                return PartialView("Upload");
            }

            switch (model.File.ContentType)
            {
                case "application/json":
                case "application/octet-stream":

                    var content = new byte[model.File.InputStream.Length];

                    model.File.InputStream.Read(content, 0, content.Length);

                    var json = System.Text.Encoding.Unicode.GetString(content);

                    var data = JsonConvert.DeserializeObject<Web.Models.DownloadModel>(json);
                    /*
                    using (var service = new Services.UploadService(Authentication.Account))
                    {
                        CompanyContext.FindAsync(DbContext, )
                        var trademarks = data.Companies.Select(e => e.Client).ToList();

                        var entities = await DbContext.Accounts
                            .Where(e => trademarks.Contains(e.Client)).ToListAsync();

                        foreach (var account in data.Companies)
                        {
                            var user = UserContext.Find(DbContext, account.UserName);

                            var context = AccountContext.Find(segment: null, user: user);

                            if (context == null)
                            {
                                context = AccountContext.Create(Account.Segment, user);
                            }

                            if (account.Roles != null)
                            {
                                context.Update(account.Roles.ToArray());
                            }

                            if (account.Claims != null)
                            {
                                context.Update(account.Claims);
                            }
                        }
                    }*/

                    return await SaveChangesAsync(() =>
                    {
                        return PartialView("Upload");
                    });
            }

            return PartialView("Upload");
        }

        // GET: authentication/my-company/api
        [HttpGet, Mvc.Activity(Activities.ReadMyCompany, Roles = Roles.Administrator, Level = AccessLevels.Private), Route(Actions.Api)]
        public async Task<ActionResult> GetCompany()
        {
            var session = Authentication.CurrentSession;

            string companyCode = session.Account != null ? session.Account.CompanyCode : null;

            if (session.Account == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var account = await AccountContext.FindAsync(DbContext, session.User.Name, session.Account.CompanyCode, session.Account.SegmentCode);

            if (account == null || account.Company == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            var customer = account.Company;
            var consumer = account.Segment;
            
            string customerCode = account.CompanyCode;
            string consumerCode = account.SegmentCode;
            
            var contract = await DbContext.Set<ContractEntity>().Where(e => e.CompanyCode == companyCode && e.CustomerCode == customerCode && e.ConsumerCode == consumerCode).FirstOrDefaultAsync();
            
            var result = new CompanyViewModel
            {
                CompanyName = customer.CompanyName,
                CompanyType = customer.CompanyType,
                ConsumerKey = contract != null ? contract.ClientId.ToString("N") : null,
                ConsumerSecret = contract != null ? contract.ClientSecret : null,
                CompanyCode = customer.CompanyCode
            };

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}