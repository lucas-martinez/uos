﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Uos.Web.Areas.Authentication.Controllers
{
    using Constants;
    using Entities;
    using Models;

    [Mvc.Feature(Features.Authentication), RouteArea(Features.Authentication), RoutePrefix(Collections.Sessions)]
    public class SessionsController : Mvc.EntityController<SessionEntity, Guid>
    {
        // GET: authentication/sessions
        [HttpGet, Mvc.Activity(Activities.DisplaySessions), Route(Actions.Default)]
        public ActionResult Index()
        {
            return View();
        }

        #region - authentication/sessions/api -

        // DELETE: authentication/sessions/api
        [HttpDelete, Mvc.Activity(Activities.DestroySessions), Route(Actions.Api)]
        public async Task<ActionResult> DeleteSessions([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<SessionDeleteModel> models)
        {
            return await DeleteAsync(request, models, m => m.Identifier, e => e.ToViewModel(),
                (missing) => string.Format("One or more sessions were not found: [{0}]", missing.Select(item => item.Identifier)));
        }

        // GET: authentication/sessions/api
        [HttpGet, Mvc.Activity(Activities.ReadSessions), Route(Actions.Api)]
        public async Task<ActionResult> GetSessions(string segmentCode, [DataSourceRequest]DataSourceRequest request)
        {
            Func<SessionEntity, SessionRowModel> selector = e => e.ToRowModel();

            var session = Authentication.CurrentSession;

            if (session.Account == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var account = await AccountContext.FindAsync(DbContext, session.User.Name, session.Account.CompanyCode, session.Account.SegmentCode);

            IQueryable<SessionEntity> query = DbSet;// account.Company.Select<SessionEntity, int>();

            if (!string.IsNullOrEmpty(segmentCode)) query = query.Where(e => e.SegmentCode == segmentCode);

            var result = await query.ToListAsync();

            return Json(result.ToDataSourceResult(request, selector), JsonRequestBehavior.AllowGet);
        }

        // POST: authentication/sessions/api
        [HttpPost, Mvc.Activity(Activities.CreateSessions), Route(Actions.Api)]
        public async Task<ActionResult> PostSessions([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<SessionPostModel> models)
        {
            return await InsertAsync(request, models, e => e.ToViewModel(), (entity, model) =>
            {
                //entity.SessionName = model.Session;
                //entity.Description = model.Desc;
                //entity.Client = model.Client;
            });
        }

        // PUT: authentication/sessions/api
        [HttpPut, Mvc.Activity(Activities.UpdateSessions), Route(Actions.Api)]
        public async Task<ActionResult> PutSessions([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<SessionPutModel> models)
        {
            return await UpdateAsync(request, models, m => m.Identifier, e => e.ToViewModel(), updater: (entity, model) =>
            {
            });
        }

        #endregion - authentication/sessions/api -
    }
}