﻿using Kendo.Mvc.Extensions;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Data;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Http.Cors;
using System.Web.Mvc;

namespace Uos.Web.Areas.Authentication.Controllers
{
    using Assertion;
    using Constants;
    using Contracts;
    using Entities;
    using Models;
    using Results;
    using Uos.Assertion;
    using Providers;
    using Utilities;

    [Mvc.Feature(Features.Authentication), RouteArea(Features.Authentication), RoutePrefix(My.Account)]
    public class MyAccountController : Mvc.EntityController<AccountEntity, int>
    {
        // GET: authentication/my-account
        [HttpGet, Mvc.Activity(Activities.DisplayMyAccount, Level = AccessLevels.Private), Route(Actions.Default)]
        public async Task<System.Web.Mvc.ActionResult> Index()
        {
            //var userData = Authentication.User;
            
            var user = await UserContext.FindAsync(DbContext, Authentication.Manager.User.Identity.Name);

            var logins = user.Entity.Logins.ToList();

            var authentications = Authentication.Manager.GetExternalAuthenticationTypes();

            ViewBag.Authentications = authentications.Where(auth => logins.All(e => auth.AuthenticationType != e.LoginProvider)).ToList();

            return View();
        }

        // GET: authentication/my-account/api
        [HttpGet, Mvc.Activity(Activities.ReadMyAccount, Roles = Roles.Administrator), Route(Actions.Api)]
        public async Task<System.Web.Mvc.ActionResult> GetAccount()
        {
            var session = Authentication.CurrentSession;

            var authentication = Authentication as AuthenticationProvider;

            if (session.Account == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var account = await AccountContext.FindAsync(DbContext, session.User.Name, session.Account.CompanyCode, session.Account.SegmentCode);

            if (account == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var company = account.Company.Entity;
            var segment = account.Entity.Segment;
            var user = account.Entity.User;

            var result = new AccountViewModel
            {
                SegmentCode = segment.SegmentCode,
                CompanyName = company.CompanyName,
                CompanyType = company.CompanyType,
                EmailAddress = user.EmailAddress, // <- TODO
                Environment = segment.Environment,
                UserName = account.Entity.UserName,
                Roles = account.Entity.Roles.Split(Characters.Space),
                CompanyCode = company.CompanyCode
            };

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}