﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Uos.Web.Areas.Authentication.Controllers
{
    using Constants;
    using Entities;
    using Models;
    using Services;
    using Web.Models;

    [Mvc.Feature(Features.Authentication), RouteArea(Features.Authentication), RoutePrefix(Collections.Accounts)]
    public class AccountsController : Mvc.EntityController<AccountEntity, int>
    {
        // GET: authentication/accounts
        [HttpGet, Mvc.Activity(Activities.DisplayAccounts), Route(Actions.Default)]
        public ActionResult Index()
        {
            return View();
        }

        #region - authentication/accounts/api -

        // DELETE: authentication/accounts/api
        [HttpDelete, Mvc.Activity(Activities.DestroyAccounts), Route(Actions.Api)]
        public async Task<ActionResult> DeleteAccounts([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<AccountDeleteModel> models)
        {
            return await DeleteAsync(request, models, m => m.Identifier, e => e.ToViewModel(),
                (missing) => string.Format("One or more accounts were not found: [{0}]", missing.Select(item => item.Identifier)));
        }

        // GET: accounts/api
        [HttpGet, Mvc.Activity(Activities.ReadAccounts), Route(Actions.Api)]
        public async Task<ActionResult> GetAccounts(string client, string user, [DataSourceRequest]DataSourceRequest request)
        {
            return await SelectAsync(request, request.ResetFilters().ToFilter().ApplyTo(Predicate).Apply(user: user, client: client), e => e.ToRowModel(), "Client", "UserName");
        }

        // POST: accounts/api
        [HttpPost, Mvc.Activity(Activities.CreateAccounts), Route(Actions.Api)]
        public async Task<ActionResult> PostAccounts([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<AccountPostModel> models)
        {
            if (!ModelState.IsValid)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var now = DateTime.UtcNow;

            var entities = new List<AccountEntity>();

            var session = Authentication.CurrentSession;

            if (session.Account == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var creator = await AccountContext.FindAsync(DbContext, session.User.Name, session.Account.CompanyCode, session.Account.SegmentCode);
            
            var users = await UserContext.SelectAsync(DbContext, models.Select(model => model.UserName).Distinct().ToArray());
            
            var segments = await DbContext.Set<SegmentEntity>().Where(creator.Company.Predicate<SegmentEntity, int>().Apply(clients: models.Select(model => model.SegmentCode).Distinct().ToArray())).ToListAsync();

            foreach (var model in models)
            {
                var user = UserContext.FromEntity(users.SingleOrDefault(e => e.UserName == model.UserName), DbContext);

                if (user == null) continue;

                var segment = SegmentContext.FromEntity(segments.SingleOrDefault(e => e.SegmentCode == model.SegmentCode), DbContext);

                if (segment == null) continue;

                var account = AccountContext.Create(segment, user, model.Roles.ToArray());

                entities.Add(account.Entity);
            }

            return await SaveChangesAsync(() =>
            {
                Response.StatusCode = (int)HttpStatusCode.Created;

                Response.StatusDescription = Request.RawUrl;

                Func<AccountEntity, AccountViewModel> selector = e => e.ToViewModel();

                return Json(entities.ToDataSourceResult(request, selector));
            });
        }

        // PUT: accounts/api
        [HttpPut, Mvc.Activity(Activities.UpdateAccounts), Route(Actions.Api)]
        public async Task<ActionResult> PutAccounts([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<AccountPutModel> models)
        {
            if (!ModelState.IsValid)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return await UpdateAsync(request, models, m => m.Identifier, e => e.ToViewModel(), updater: (entity, model) =>
            {
                entity.Roles = string.Join(" ", model.Roles.Distinct().OrderBy(r => r));
            });
        }

        #endregion - authentication/accounts/api -
    }
}