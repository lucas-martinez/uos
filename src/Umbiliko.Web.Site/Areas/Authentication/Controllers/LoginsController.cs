﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Uos.Web.Areas.Authentication.Controllers
{
    using Constants;
    using Entities;
    using Models;
    using Services;
    using Web.Models;

    [Mvc.Feature(Features.Authentication), RouteArea(Features.Authentication), RoutePrefix(Collections.Logins)]
    public class LoginsController : Mvc.EntityController<LoginEntity, int>
    {
        // GET: authentication/logins
        [HttpGet, Mvc.Activity(Activities.DisplayLogins), Route(Actions.Default)]
        public ActionResult Index()
        {
            return View();
        }

        #region - authentication/logins/api -

        // DELETE: authentication/logins/api
        [HttpDelete, Mvc.Activity(Activities.DestroyLogins), Route(Actions.Api)]
        public async Task<ActionResult> DeleteLogins([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<LoginDeleteModel> models)
        {
            return await DeleteAsync(request, models, m => m.Identifier, e => e.ToViewModel(),
                (missing) => string.Format("One or more accounts were not found: [{0}]", missing.Select(item => item.Identifier)));
        }

        // GET: accounts/api
        [HttpGet, Mvc.Activity(Activities.ReadLogins), Route(Actions.Api)]
        public async Task<ActionResult> GetLogins(string provider, string user, [DataSourceRequest]DataSourceRequest request)
        {
            return await SelectAsync(request, request.ResetFilters().ToFilter().ApplyTo(Predicate).Apply(user: user, provider: provider), e => e.ToRowModel(), "UserName", "LoginProvider");
        }

        /*/ POST: accounts/api
        [HttpPost, Mvc.Activity(Activities.CreateLogins), Route(Actions.Api)]
        public async Task<ActionResult> PostLogins([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<LoginPostModel> models)
        {
            if (!ModelState.IsValid)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var now = DateTime.UtcNow;

            var entities = new List<LoginEntity>();

            var users = await UserContext.SelectAsync(DbContext, models.Select(model => model.UserName).Distinct().ToArray());

            var segments = await DbContext.Set<SegmentEntity>().Where(Company.Predicate<SegmentEntity, int>().Apply(clients: models.Select(model => model.Client).Distinct().ToArray())).ToListAsync();

            foreach (var model in models)
            {
                var user = UserContext.FromEntity(users.SingleOrDefault(e => e.UserName == model.UserName), DbContext);

                if (user == null) continue;

                var segment = SegmentContext.FromEntity(segments.SingleOrDefault(e => e.Client == model.Client), DbContext);

                if (segment == null) continue;

                var account = LoginContext.Create(segment, user, model.Roles.ToArray());

                entities.Add(account.Entity);
            }

            return await SaveChangesAsync(() =>
            {
                Response.StatusCode = (int)HttpStatusCode.Created;

                Response.StatusDescription = Request.RawUrl;

                Func<LoginEntity, LoginViewModel> selector = e => e.ToViewModel();

                return Json(entities.ToDataSourceResult(request, selector));
            });
        }*/

        // PUT: accounts/api
        [HttpPut, Mvc.Activity(Activities.UpdateLogins), Route(Actions.Api)]
        public async Task<ActionResult> PutLogins([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<LoginPutModel> models)
        {
            if (!ModelState.IsValid)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return await UpdateAsync(request, models, m => m.Identifier, e => e.ToViewModel(), updater: (entity, model) =>
            {
                //entity.Roles = string.Join(" ", model.Roles.Distinct().OrderBy(r => r));
            });
        }

        #endregion - authentication/logins/api -
    }
}