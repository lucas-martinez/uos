﻿using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Uos.Web.Areas.Authentication.Controllers
{
    using Constants;
    using Entities;
    using Models;
    using Services;
    using Web.Models;

    [Mvc.Feature(Features.Authentication), RouteArea(Features.Authentication), RoutePrefix(Collections.Segments)]
    public class SegmentsController : Mvc.EntityController<SegmentEntity, int>
    {
        // GET: authentication/segments
        [HttpGet, Mvc.Activity(Activities.DisplaySegments), Route(Actions.Default)]
        public ActionResult Index()
        {
            return View();
        }

        // GET: authentication/segments/download
        [HttpGet, Mvc.Activity(Activities.DownloadSegments), Route(Actions.Download)]
        public async Task<ActionResult> DownloadSegments()
        {
            var model = new DownloadModel();

            //

            var json = JsonConvert.SerializeObject(model, Formatting.Indented);

            var content = System.Text.Encoding.Unicode.GetBytes(json);

            return File(content, "application/json", string.Format("Uos-{0:yyyyMMddHHmm}.segments.json", DateTime.Now));
        }

        // POST: authentication/segments
        [HttpPost, Mvc.Activity(Activities.ListSegments), Route(Actions.Default)]
        public async Task<ActionResult> ListSegments()
        {
            var session = Authentication.CurrentSession;

            if (session.Account == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var account = await AccountContext.FindAsync(DbContext, session.User.Name, session.Account.CompanyCode, session.Account.SegmentCode);

            return Json(account.Company.GetClients(), JsonRequestBehavior.AllowGet);
        }

        // GET|POST: authentication/segments/upload
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post), Mvc.Activity(Activities.UploadSegments), Route(Actions.Upload)]
        public async Task<ActionResult> UploadSegments(UploadModel model)
        {
            if (!ModelState.IsValid)
            {
                return PartialView("Upload");
            }

            switch (model.File.ContentType)
            {
                case "application/json":
                case "application/octet-stream":

                    var content = new byte[model.File.InputStream.Length];

                    model.File.InputStream.Read(content, 0, content.Length);

                    var json = System.Text.Encoding.Unicode.GetString(content);

                    var data = JsonConvert.DeserializeObject<DownloadModel>(json);

                    //

                    return await SaveChangesAsync(() =>
                    {
                        return PartialView("Upload");
                    });
            }

            return PartialView("Upload");
        }

        #region - authentication/segments/api -

        // DELETE: authentication/segments/api
        [HttpDelete, Mvc.Activity(Activities.DestroySegments), Route(Actions.Api)]
        public async Task<ActionResult> DeleteSegments([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<SegmentDeleteModel> models)
        {
            return await DeleteAsync(request, models, m => m.Identifier, e => e.ToViewModel(),
                (missing) => string.Format("One or more segments were not found: [{0}]", missing.Select(item => item.Identifier)));
        }

        // GET: authentication/segments/api
        [HttpGet, Mvc.Activity(Activities.ReadSegments), Route(Actions.Api)]
        public async Task<ActionResult> GetSegments([DataSourceRequest]DataSourceRequest request)
        {
            return await SelectAsync(request, request.ResetFilters().ToFilter().ApplyTo(Predicate), e => e.ToRowModel(),
                "SegmentCode");
        }

        // POST: authentication/segments/api
        [HttpPost, Mvc.Activity(Activities.CreateSegments), Route(Actions.Api)]
        public async Task<ActionResult> PostSegments([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<SegmentPostModel> models)
        {
            if (!ModelState.IsValid)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var now = DateTime.UtcNow;

            var entities = new List<SegmentEntity>();

            var session = Authentication.CurrentSession;

            if (session.Account == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var account = await AccountContext.FindAsync(DbContext, session.User.Name, session.Account.CompanyCode, session.Account.SegmentCode);

            foreach (var model in models)
            {
                var segment = SegmentContext.Create(account.Company, 
                    segment: model.SegmentCode,
                    environment: model.Environment,
                    description: model.Description);

                segment.Entity.ChangedUtc = now;
                segment.Entity.CreatedUtc = now;
            }

            return await SaveChangesAsync(() =>
            {
                Response.StatusCode = (int)HttpStatusCode.Created;
                Response.StatusDescription = Request.RawUrl;

                Func<SegmentEntity, SegmentViewModel> selector = e => e.ToViewModel();

                return Json(entities.ToDataSourceResult(request, selector));
            });
        }

        // PUT: authentication/segments/api
        [ HttpPut, Mvc.Activity(Activities.UpdateSegments), Route(Actions.Api)]
        public async Task<ActionResult> PutSegments([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<SegmentPutModel> models)
        {
            if (!ModelState.IsValid)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return await UpdateAsync(request, models, m => m.Identifier, e => e.ToViewModel(), updater: (entity, model) =>
            {
                if (model.Environment != null) entity.Environment = model.Environment;
                if (model.Description != null) entity.Description = model.Description;
            });
        }

        #endregion - authentication/segments/api -
    }
}
