﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Uos.Web.Areas.Authentication.Controllers
{
    using Constants;
    using Mvc;

    [Feature(Features.Authentication), RouteArea(Features.Authentication), RoutePrefix("login")]
    public class LoginController : Controller
    {
        // GET: authentication/login
        [HttpGet, Route(Actions.Default)]
        public ActionResult Index()
        {
            return View();
        }
    }
}