﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Uos.Web.Areas.Authentication.Controllers
{
    using Constants;

    [Mvc.Feature(Features.Authentication), RouteArea(Features.Authentication), RoutePrefix("password")]
    public class PasswordController : Mvc.CustomController
    {
        // GET: authentication/password
        [HttpGet, Route(Actions.Default)]
        public ActionResult Index()
        {
            return View();
        }
    }
}