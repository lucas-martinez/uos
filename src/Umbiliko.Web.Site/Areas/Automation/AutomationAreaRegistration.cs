﻿using System.Web.Mvc;

namespace Uos.Web.Areas.Automation
{
    using Constants;

    public class AutomationAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get { return Features.Automation; }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            /*context.MapRoute(
                "Automation_default",
                "Automation/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );*/
        }
    }
}