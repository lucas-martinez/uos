﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Uos.Web.Areas.Formulation.Controllers
{
    using Constants;

    [Mvc.Feature(Features.Formulation), RouteArea(Features.Formulation), RoutePrefix(Collections.Constants)]
    public class ConstantsController : Controller
    {
        // GET: Formulation/Constants
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet, Mvc.Activity("read-phone-codes"), Route("phone-codes")]
        public ActionResult InternationalPhoneCodes()
        {
            return Json(null);
        }
    }
}