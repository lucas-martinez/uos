﻿using System.Web.Mvc;

namespace Uos.Web.Areas.Formulation
{
    using Constants;

    public class FormulationAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get { return Features.Formulation; }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            /*context.MapRoute(
                "Formulation_default",
                "Formulation/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );*/
        }
    }
}