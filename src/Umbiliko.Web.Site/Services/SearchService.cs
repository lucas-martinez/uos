﻿using Kendo.Mvc;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Services
{
    using Constants;
    using Entities;
    using Inquiry;
    using Models;

    public class SearchService : IDisposable
    {
        static readonly string[] Keywords = new[] { "account", "activity", "client", "email", "group", "product", "user" };

        bool _ownContext;
        DbContext _dbContext;

        public SearchService()
        {
        }

        public SearchService(DbContext dbContext)
        {
            _dbContext = dbContext;
            _ownContext = false;
        }

        public DbContext DbContext
        {
            get
            {
                Func<DbContext> create = () =>
                {
                    _ownContext = true;
                    return (_dbContext = new UmbilikoDbContext());
                };

                return _dbContext ?? create();
            }
            set
            {
                _ownContext = false;
                _dbContext = value;
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing && _ownContext && _dbContext != null)
            {
                _dbContext.Dispose();
            }
        }

        void IDisposable.Dispose()
        {
            Dispose(true);
        }

        public async Task<SearchResultModel> FilterAsync(SearchResultModel model)
        {
            switch (model.Type)
            {
                case "activity":
                    {
                        var predicate = PredicateBuilder.False<ActivityEntity>();

                        model.Values.ForEach(value => { predicate = predicate.Or(e => e.ActivityName.StartsWith(value)); });

                        model.Values = await DbContext.Set<ActivityEntity>()
                            .AsExpandableAsync()
                            .Where(predicate)
                            .OrderBy(e => e.ActivityName)
                            .Select(e => e.ActivityName)
                            .Distinct()
                            .ToListAsync();

                        switch (model.Values.Count)
                        {
                            case 0:
                                break;

                            case 1:
                                model.Token = string.Format("Activity eq {0}", model.Values.First());
                                break;

                            default:
                                model.Token = string.Format("Activity in [{0}]", string.Join(", ", model.Values));
                                break;
                        }
                    }
                    break;

                case "client":
                    {
                        var predicate = PredicateBuilder.False<SegmentEntity>();

                        model.Values.ForEach(value => { predicate = predicate.Or(e => e.SegmentCode.StartsWith(value)); });

                        model.Values = DbContext.Set<SegmentEntity>()
                            .AsExpandableAsync()
                            .Where(predicate)
                            .OrderBy(e => e.SegmentCode)
                            .Select(e => e.SegmentCode)
                            .ToList();

                        switch (model.Values.Count)
                        {
                            case 0:
                                break;

                            case 1:
                                model.Token = string.Format("Client eq {0}", model.Values.First());
                                break;

                            default:
                                model.Token = string.Format("Client in [{0}]", string.Join(", ", model.Values));
                                break;
                        }
                    }
                    break;

                case "email":
                    {
                        var predicate = PredicateBuilder.False<UserEntity>();

                        model.Values.ForEach(value => { predicate = predicate.Or(e => e.EmailAddress.Contains(value)); });

                        model.Values = DbContext.Set<UserEntity>()
                            .AsExpandableAsync()
                            .Where(predicate)
                            .OrderBy(e => e.EmailAddress)
                            .Select(e => e.EmailAddress)
                            .Distinct()
                            .ToList();

                        switch (model.Values.Count)
                        {
                            case 0:
                                break;

                            case 1:
                                model.Token = string.Format("Email eq {0}", model.Values.First());
                                break;

                            default:
                                model.Token = string.Format("Email in [{0}]", string.Join(", ", model.Values));
                                break;
                        }
                    }
                    break;

                case "phone":
                    {
                        var predicate = PredicateBuilder.False<UserEntity>();

                        model.Values.ForEach(value => { predicate = predicate.Or(e => e.PhoneNumber.StartsWith(value)); });

                        model.Values = DbContext.Set<UserEntity>()
                            .AsExpandableAsync()
                            .Where(predicate)
                            .OrderBy(e => e.EmailAddress)
                            .Select(e => e.EmailAddress)
                            .Distinct()
                            .ToList();

                        switch (model.Values.Count)
                        {
                            case 0:
                                break;

                            case 1:
                                model.Token = string.Format("Email eq {0}", model.Values.First());
                                break;

                            default:
                                model.Token = string.Format("Email in [{0}]", string.Join(", ", model.Values));
                                break;
                        }
                    }
                    break;

                case "feature":
                    {
                        var predicate = PredicateBuilder.False<FeatureEntity>();

                        model.Values.ForEach(value => { predicate = predicate.Or(e => e.FeatureName.StartsWith(value)); });

                        model.Values = DbContext.Set<FeatureEntity>()
                            .AsExpandableAsync()
                            .Where(predicate)
                            .OrderBy(e => e.FeatureName)
                            .Select(e => e.FeatureName)
                            .Distinct()
                            .ToList();

                        switch (model.Values.Count)
                        {
                            case 0:
                                break;

                            case 1:
                                model.Token = string.Format("Feature eq {0}", model.Values.First());
                                break;

                            default:
                                model.Token = string.Format("Feature in [{0}]", string.Join(", ", model.Values));
                                break;
                        }
                    }
                    break;

                case "product":
                    {
                        var predicate = PredicateBuilder.False<ProductEntity>();

                        model.Values.ForEach(value => { predicate = predicate.Or(e => e.ProductName.StartsWith(value)); });

                        model.Values = DbContext.Set<ProductEntity>()
                            .AsExpandableAsync()
                            .Where(predicate)
                            .OrderBy(e => e.ProductName)
                            .Select(e => e.ProductName)
                            .ToList();

                        switch (model.Values.Count)
                        {
                            case 0:
                                break;

                            case 1:
                                model.Token = string.Format("Product eq {0}", model.Values.First());
                                break;

                            default:
                                model.Token = string.Format("Product in [{0}]", string.Join(", ", model.Values));
                                break;
                        }
                    }
                    break;

                case "user":
                    {
                        var predicate = PredicateBuilder.False<UserEntity>();

                        model.Values.ForEach(value => { predicate = predicate.Or(e => e.UserName.StartsWith(value)); });

                        model.Values = DbContext.Set<UserEntity>()
                            .AsExpandableAsync()
                            .Where(predicate)
                            .OrderBy(e => e.UserName)
                            .Select(e => e.UserName)
                            .ToList();

                        switch (model.Values.Count)
                        {
                            case 0:
                                break;

                            case 1:
                                model.Token = string.Format("User eq {0}", model.Values.First());
                                break;

                            default:
                                model.Token = string.Format("User in [{0}]", string.Join(", ", model.Values));
                                break;
                        }
                    }
                    break;

                default:
                    model.Token = "Unrecognized prefix";
                    model.Values = new[] { model.Type }.Union(model.Values).ToArray();
                    model.Type = "keyword";
                    break;
            }

            return model;
        }

        public SearchResultModel Parse(string token)
        {
            var keywords = token.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);

            SearchResultModel result;

            if (keywords.Length.Equals(1))
            {
                result = new SearchResultModel
                {
                    Token = token,
                    Type = "term",
                    Values = keywords
                };
            }
            else if (!Keywords.Contains(keywords[0]))
            {
                result = new SearchResultModel
                {
                    Token = "Error: unrecognized prefix",
                    Type = "term",
                    Values = keywords
                };
            }
            else
            {
                result = new SearchResultModel()
                {
                    Token = token,
                    Type = keywords[0],
                    Values = keywords.Skip(1).ToArray()
                };
            }

            return result;
        }
    }
}
