﻿using Twilio;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Services
{
    using Assertion;
    
    public class TwilioSmsService : ISmsService
    {
        public NameValueCollection AppSettings
        {
            get { return ConfigurationManager.AppSettings; }
        }

        public IResult Send(string body)
        {
            return Send(body, null, null);
        }

        public IResult Send(string body, string recipient)
        {
            return Send(body, recipient, null);
        }

        public IResult Send(string body, string to, string origin)
        {
            var client = new TwilioRestClient(AppSettings["twilio:Sid"], AppSettings["twilio:Token"]);

            var result = client.SendMessage(AppSettings["twilio:PhoneNumber"],
                to ?? AppSettings["twilio:PhoneNumber"],
                body);

            // Status is one of Queued, Sending, Sent, Failed or null if the number is not valid
            //Trace.Info(result.Status);

            // Twilio doesn't currently have an async API, so return success.
            return result.RestException == null ? Result.Default : Result.Fail(result.RestException.Message, result.RestException.MoreInfo);
        }

        public Task<IResult> SendAsync(string body)
        {
            return SendAsync(body, null, null);
        }

        public Task<IResult> SendAsync(string body, string recipient)
        {
            return SendAsync(body, recipient, null);
        }

        public Task<IResult> SendAsync(string body, string recipient, string origin)
        {
            var result = Send(body, recipient, origin);

            // Twilio doesn't currently have an async API, so return success.
            return Task<IResult>.FromResult(result);
        }
    }
}
