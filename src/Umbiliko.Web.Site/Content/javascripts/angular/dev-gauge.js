﻿'use strict';

(function (angular, undefined) {

    var module = angular.module('dev-gauge', []);

    module.controller('DevGaugeController', ['$scope', function ($scope) {
        $scope.model = $scope.$parent.indicators[$scope.indicator];
        $scope.model.id = $scope.indicator;
        $scope.model.Status = 'Idle';

        $scope.options = { thickness: 5 };
    }]);

    module.factory('$gauge', [function () {
        return {

            create: function (el, dim) {
                var svg = d3.select(el).append('svg')
                    .attr('width', dim.width)
                    .attr('height', dim.height)
                    .append('g')
                    .attr('transform', 'translate(' + (dim.width * .5) + ',' + (dim.height * .5) + ')');

                return svg;
            },

            updatePaths: function (svg, data, dimensions, thickness) {
                var outerRadius = Math.min(dimensions.width, dimensions.height) * .5;
                var innerRadius = Math.max(outerRadius - thickness, 0);

                var arc = d3.svg.arc()
                  .outerRadius(outerRadius)
                  .innerRadius(innerRadius);

                var pie = d3.layout.pie()
                  .value(function (d) { return d.value; });

                pie.sort(null);

                var paths = svg.selectAll('.arc').data(pie(data));

                paths.enter().append('path').attr({
                    'class': 'arc',
                    'id': function (d, i) { return 'arc_' + i; }
                });

                var tween = function (d) {
                    var oldAngles = this.__current ? this.__current : { startAngle: d.startAngle, endAngle: d.startAngle };
                    var newAngles = { startAngle: d.startAngle, endAngle: d.endAngle };

                    var i = d3.interpolate(oldAngles, newAngles);

                    return function (t) { return arc(i(t)); };
                };

                paths
                  .transition()
                    .duration(250)
                    .attrTween('d', tween)
                    .each('end', function (d) {
                        this.__current = { startAngle: d.startAngle, endAngle: d.endAngle };
                    });

                paths.exit().remove();

                return this;
            }
        };
    }]);

    module.factory('$chart', [function () {

        var color = d3.scale.category10();

        return {
            create: function (el, data) {

                var svg = d3.select(el).append('svg')
                    .attr('width', data.width)
                    .attr('height', data.height);

                svg.append('line')
                   .attr('class', 'zero')
                   .attr('x1', '0')
                   .attr('x2', '100%')
                   .attr('y1', '0')
                   .attr('y2', '0');

                svg.append('path')
                    .attr('class', 'area');

                return svg;
            },

            init: function (options) {

                var newRangeArray = function (length) {
                    var arr = new Array(length);
                    for (var i = 0; i < length; i++) {
                        arr[i] = {
                            buckets: new Array(10)
                        };
                    }
                    return arr;
                }

                var now = Date.now(),
                    data = {
                        history: {
                            milisecons: [],
                            days: {
                                count: 0,
                                duration: 24 * 60 * 60 * 1000, // ms
                                steps: 60, // 60 days
                                version: parseInt(now / 10080000),
                                values: newRangeArray(60)
                            },
                            hours: {
                                count: 0,
                                duration: 60 * 60 * 1000, // ms
                                steps: 60, // 60 hours
                                version: parseInt(now / 1440000),
                                values: newRangeArray(60)
                            },
                            minutes: {
                                count: 0,
                                duration: 60 * 1000, // ms
                                steps: 60, // 60 mins
                                version: parseInt(now / 60000),
                                values: newRangeArray(60)
                            },
                            seconds: {
                                count: 0,
                                duration: 1000, // ms
                                steps: 60, // 60 seconds
                                version: parseInt(now / 1000),
                                values: newRangeArray(60)
                            }
                        },
                        maxValue: options.max || 100,
                        minValue: options.min || 0,
                        height: options.height,
                        range: {
                            max: options.max || 100,
                            min: options.min || 0
                        },
                        view: options.view,
                        width: options.width,
                        x: d3.time.scale().range([0, options.width - 60]),
                        y: d3.scale.linear().range([options.height / 2, 0])
                    };

                // Compute the minimum and maximum date across values.
                data.x.domain([0, data.width - 1]);

                return data;
            },

            update: function (history, feed) {

                function fix(values, value) {
                    for (var i in values) {
                        var vmax = values[i].max,
                            vmin = values[i].min;
                        if (typeof vmax === 'number' && vmax > value.max) value.max = vmax;
                        if (typeof vmin === 'number' && vmin < value.min) value.min = vmin;
                    }
                    return value;
                };

                function slice(arr, count) {
                    for (var i = count; i < arr.length; i++) {
                        arr[i - count] = arr[i];
                    }
                };

                function tick(range, value) {

                    var version = parseInt(feed.timestamp / range.duration),
                        next = (version !== range.version);

                    if (!!next) {
                        range.count = Math.min(range.count + 1, range.values.length);
                        range.version = version;
                        slice(range.values, 1);
                    }

                    range.values[range.values.length - 1] = value;

                    return next;
                };

                var current = {
                    feed: { max: feed.value, min: feed.value }
                };
                current.seconds = fix(history.milisecons, { max: current.feed.max, min: current.feed.min });
                //console.log('feed: [', current.seconds.min, ' - ', current.seconds.max, ']');
                if (tick(history.seconds, current.seconds)) {
                    current.minutes = fix(history.seconds.values, { max: current.seconds.max, min: current.seconds.min });
                    //console.log('seconds: [', current.minutes.min, ' - ', current.minutes.max, ']');
                    history.milisecons = [];
                    if (tick(history.minutes, current.minutes)) {
                        //console.log('seconds: [', history.minutes.values[history.minutes.values.length - 2].min, ' - ', history.minutes.values[history.minutes.values.length - 2].max, '] ', history.minutes.version);
                        current.hours = fix(history.minutes.values, { max: current.minutes.max, min: current.minutes.min });
                        current.days = fix(history.minutes.values, { max: current.minutes.max, min: current.minutes.min });
                        tick(history.hours, current.hours);
                        tick(history.days, current.days);
                    }
                }
                else {
                    history.milisecons.push(current.feed);
                }
            },

            tick: function (svg, data, feed) {

                for (var i in data.columns) {
                    var col = data.columns[i],
                        version = parseInt(feed.timestamp / data.steps);

                    var max = feed.value;
                    for (var i in col.ticks) {
                        if (max < ticks[i]) {
                            max = ticks[i];
                        }
                    }

                    if (version === col.version) {
                        col.ticks.push(feed.value);
                    }
                    else {
                        for (var i = 1; i < col.values.length; i++) {
                            col.values[i - 1] = col.values[i];
                        }
                        col.ticks = [];
                    }
                    col.values[col.values.length - 1] = max;
                    //console.log('count: ', col.values.length, 'feed: ', feed.value, 'last value: ', col.values[col.values.length - 1]);
                }

                var g = svg.selectAll('.col');

                /*var axis = d3.svg.line()
                    .interpolate('basis')
                    .x(function (d) { return x(d.date); })
                    .y(h);*/

                var plotRadio = 0,//7,
                    now = Date.now(),
                    xs = (data.width - plotRadio) / data.steps,
                    ys = (data.height - 2 * plotRadio) / (data.range.max - data.range.min),
                    max = plotRadio,
                    min = data.height - plotRadio,
                    zero = data.height - plotRadio + ys * data.minValue;

                svg.select('line.zero')
                   .attr('y1', zero)
                   .attr('y2', zero);

                (function (view) {
                    var range = data.history[view],
                        count = range.count,
                        length = range.values.length,
                        xs = (data.width - plotRadio) / length;

                    svg.select('path.area')
                        .attr('d', function (d) {
                            var draw = d3.svg.line()
                                .interpolate('basis')//'linear')
                                .x(function (d, i) {
                                    var pos = count - i + 1;
                                    return xs * (length - (i < count ? pos : 1 - pos));
                                })
                                .y(function (d, i) {
                                    var values = range.values,
                                        v = i < count ? values[length - count + i].max : values[length + count - i - 1].min;
                                    return data.height - plotRadio - ys * (v || 0 - data.minValue);
                                });

                            return draw({ length: 2 * count });
                        });
                })(data.view || 'seconds');
            }
        };
    }]);

    module.factory('$heatmap', [function () {
        return {
            init: function (el, options) {
                var canvas = d3.select(el).append('canvas');

                return canvas();
            },

            update: function (canvas, heatmap) {

            }
        };
    }]);

    function sanitizeOptions(options) {

        if (typeof options.thickness === 'number') {
            options.thickness = parseInt(options.thickness);
        } else {
            options.thickness = parseInt($header.css('border-width'), 10) || 2;
        }

        return options;
    };

    module.directive('devGauge', ['$rootScope', '$chart', '$gauge', '$heatmap', function ($rootScope, $chart, $gauge, $heatmap) {

        function link($scope, $element, $attrs, $ctrl) {
            var dim = { top: 0, right: 0, bottom: 0, left: 0 };
            var $content = $('content', $element);
            var $header = $('header', $element);

            var updateDimensions = function (dimensions) {
                dimensions.width = $element[0].offsetWidth || 80;
                dimensions.height = $element[0].offsetHeight || 80;
            };

            sanitizeOptions($scope.options);
            updateDimensions(dim);

            d3.select($content[0]).select('svg').remove();
            d3.select($header[0]).select('svg').remove();

            var gauge = $gauge.create($header[0], dim);
            var chartData = $chart.init({
                limit: 60000,
                max: 100,
                min: 0,
                name: 'CPU?',
                height: '100%',
                steps: 60,
                type: 'line',
                view: $scope.range || 'seconds',
                width: '100%'
            });
            var chart = $chart.create($content[0], chartData);

            $scope.onRange = function (newValue, oldValue) {
                chartData.view = newValue;
            };
            //$chart.load(chart, {});

            $gauge.updatePaths(gauge, [{ value: 100 }], dim, $scope.options.thickness);

            $scope.$watch('range', function (newValue, oldValue) {
                chartData.view = newValue;
            });

            $scope.$watch('model', function (newValue, oldValue) {

                chartData.value = newValue.Value;
                if (newValue.Timestamp) {
                    $chart.update(chartData.history, {
                        timestamp: newValue.Timestamp,
                        value: newValue.Value
                    });
                }

                if ($rootScope.focus === $scope.indicator && !!newValue && newValue.Timestamp !== oldValue.Timestamp) {
                    chartData.height = $content.height();
                    chartData.width = $content.width();
                    $chart.tick(chart, chartData, {
                        timestamp: newValue.Timestamp,
                        value: newValue.Value
                    });
                }

                if (!!newValue.Value && newValue.Value !== oldValue.Value) {

                    var state;
                    for (var max in newValue.Type.states) {
                        if (max > newValue.Value) {
                            state = newValue.Type.states[max];
                            break;
                        }
                    }
                    if (!!state) {
                        $attrs.$set('data-state', state.name);
                        newValue.Status = state.text || (parseInt(newValue.Value) + newValue.Suffix);
                    }
                    var data = [{
                        value: newValue.Value - newValue.Min
                    }, {
                        value: newValue.Max - newValue.Value
                    }];
                    if (gauge) {
                        $gauge.updatePaths(gauge, data, dim, $scope.options.thickness);
                    } else {
                        forceUpdate(data, $scope.options);
                    }
                }
            }, true);

            $rootScope.$watch('focus', function (newValue, oldValue) {
                if (newValue === $scope.indicator) {
                    $scope.focus = true;
                    $element.addClass('expanded');
                    var element = $element.parent();
                    var parent = element.parent();
                    var parentWidth = parent.width();
                    var parentHeight = parent.height();
                    var elementPosition = element.position();
                    var elementWidth = element.outerWidth(true);
                    var elementHeight = element.outerHeight(true);

                    var vclass, hclass, vcount, hcount, hmax = 4;
                    var right = 1 + parseInt((parentWidth - elementPosition.left) / elementWidth);
                    var left = parseInt(elementPosition.left / elementWidth);

                    if (right < hmax && left > right) {
                        hclass = 'left';
                        hcount = left;
                    }
                    else {
                        hclass = 'right';
                        hcount = right;
                    }

                    var elementClass = 'sqr-span-bottom-' + hclass + ' sqr-span-v4 sqr-span-h' + Math.min(hmax, hcount);
                    $scope.focusClass = elementClass;

                    $('content', $element).addClass(elementClass);
                }
                else if (oldValue == $scope.indicator) {
                    $scope.focus = false;
                    $element.removeClass('expanded');
                    $('content', $element).removeClass($scope.focusClass);
                }
            }, true);

            $header.bind('click', function () {
                $rootScope.$apply(function () {
                    if ($rootScope.focus !== $scope.indicator) {
                        $rootScope.focus = $scope.indicator;
                    } else {
                        $rootScope.focus = undefined;
                    }
                });
            });

            $('content .contract', $element).bind('click', function () {
                $rootScope.$apply(function () {
                    if ($rootScope.focus === $scope.indicator) {
                        $rootScope.focus = undefined;
                    }
                });
            });
        };

        return {
            controller: 'DevGaugeController',
            link: link,
            replace: false,
            restrict: 'E',//'CA';// <-class
            scope: { indicator: '=', range: '=', state: '=' },
            transclude: false/*,
            template: '<div></div>'*/
        };
    }])

    module.service('devGaugeService', function ($document) {
    });

    return module;
})(angular);