﻿'use strict';

(function (angular) {

    var module = angular.module('dev-dashboard', ['dev-gauge', 'ng.epoch'])

    // services
    module.factory('$proxy', ['$rootScope', function ($rootScope) {

        function factory(hubName, hubUrl) {
            var connection = $.hubConnection(hubUrl);
            var proxy = connection.createHubProxy(hubName);

            return {
                invoke: function (methodName, callback) {
                    proxy.invoke(methodName)
                    .done(function (result) {
                        $rootScope.$apply(function () {
                            if (callback) {
                                callback(result);
                            }
                        });
                    });
                },
                on: function (eventName, callback) {
                    proxy.on(eventName, function (result) {
                        $rootScope.$apply(function () {
                            if (callback) {
                                callback(result);
                            }
                        });
                    });
                },
                // you can't assign the $.connection.start method to a property in a different scope.
                start: function () {
                    return connection.start();
                }
            };
        };

        return factory;
    }]);

    module.factory('$dashboard', [function () {

        var _scope;

        return {
            init: function ($scope, $attr) {
                _scope = $scope;

                $scope.options = {};
            },
            focus: function ($element) {

            }
        };
    }]);

    module.controller('DevDashboardController', ['$scope', '$attrs', '$proxy', '$dashboard', function ($scope, $attrs, $proxy, $dashboard) {
        var hubName = $attrs.hubName;
        var hubUrl = $attrs.hubUrl;
        var hub = $proxy(hubName, hubUrl);

        console.log('dev-dashboard: {', 'hub-name: "', hubName, '", hub-url: "', hubUrl, '" }');

        $dashboard.init($scope, $attrs);

        hub.start()
            .then(function () {
                console.log("Hub started.");
            })
            .done(function () {
                console.log("Hub initialization completed.");

                hub.invoke('connect', function () {
                    console.log('after connect');
                });
            });

        hub.on('connect', function (data) {
            hub.invoke('define', function () {
                console.log('after define');
            });
        });

        hub.on('define', function (data) {
            var indicators = {}, sections = {};

            for (var id in data) {
                var item = data[id];

                indicators[id] = item;

                var section = sections[item.Type.name];

                var states = {};
                for (var name in item.Type.states) {
                    var value = item.Type.states[name];
                    value.name = name;
                    states[value.max || item.Max] = value;
                }
                item.Type.states = states;

                if (!section) {
                    sections[item.Type.name] = section = [];
                }

                section.push(id);
            }

            $scope.indicators = indicators;
            $scope.sections = sections;

            console.log('sections: ', sections);

            hub.invoke('start', function () {
                console.log('after start');
            });
        });

        hub.on('tick', function (samples) {
            for (var id in samples) {
                $scope.indicators[id].Value = samples[id].Value;
                $scope.indicators[id].Timestamp = Date.parse(samples[id].DateUtc);
            }
        });
    }]);

    return module;

})(angular);