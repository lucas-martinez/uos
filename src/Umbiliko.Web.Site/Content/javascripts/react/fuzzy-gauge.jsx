﻿var HelloWorld = React.createClass({
    render: function() {
        return <div>Hello world!</div>;
    }
});

angular.module('dashboardUtils', [])
.controller('Controller', ['$scope', function ($scope) {
    $scope.customer = {
        name: 'Naomi',
        address: '1600 Amphitheatre'
    };
}])
.directive('fuzzyGauge', function () {
    return {
        template: 'Name: {{customer.name}} Address: {{customer.address}}'
    };
});