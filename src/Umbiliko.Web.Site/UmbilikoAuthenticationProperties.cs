﻿using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web
{
    public class UmbilikoAuthenticationProperties : AuthenticationProperties
    {
        // Used for XSRF protection when adding external logins
        public const string XsrfKey = "XsrfId";

        public UmbilikoAuthenticationProperties()
        {
        }

        public UmbilikoAuthenticationProperties(IDictionary<string, string> dictionary)
            : base(dictionary)
        {
        }

        public UmbilikoAuthenticationProperties(AuthenticationProperties properties)
            : this(properties.Dictionary)
        {
        }

        public Guid? Id
        {
            get { return GetGuid(XsrfKey); }
            set { SetValue(XsrfKey, value); }
        }
        
        private Guid? GetGuid(string name)
        {
            Guid guid;
            string value;
            if (Dictionary.TryGetValue(name, out value) && Guid.TryParse(value, out guid)) return guid;
            return null;
        }

        private string GetValue(string name)
        {
            string value;
            Dictionary.TryGetValue(name, out value);
            return value;
        }

        private void SetValue(string name, string value)
        {
            if (value != null)
            {
                Dictionary[name] = value;
            }
            else if (Dictionary.ContainsKey(name))
            {
                Dictionary.Remove(name);
            }
        }

        private void SetValue(string name, Guid? guid)
        {
            if (guid.HasValue)
            {
                Dictionary[name] = Convert.ToString(guid.Value);
            }
            else if (Dictionary.ContainsKey(name))
            {
                Dictionary.Remove(name);
            }
        }
    }
}
