﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Projects
{
    /// <summary>
    /// Projects System features
    /// </summary>
    public class Features
    {
        /// <summary>
        /// Basic Data
        /// </summary>
        public const string Basic = @"PS-BD";

        /// <summary>
        /// Project Budgeting
        /// </summary>
        public const string Budgeting = @"PS-APP";

        /// <summary>
        /// Project Execution Integration
        /// </summary>
        public const string Execution = @"PS-EXE";

        /// <summary>
        /// Information System
        /// </summary>
        public const string Information = @"PS-IS";

        /// <summary>
        /// Project Planning
        /// </summary>
        public const string Planning = @"PS-PLN";
    }
}
