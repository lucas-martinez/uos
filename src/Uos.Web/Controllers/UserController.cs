﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Uos.Cognito;

namespace Uos.Web.Controllers
{
    using Models;
    using Helpers;

    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    [Authorize, RoutePrefix("user")]
    public class UserController : Controller
    {
        //
        // GET: /user
        [HttpGet, Route("")]
        public async Task<ViewResult> Index(ManageMessageId? message)
        {
            var result = await HttpContext.GetOwinContext().GetIdentityAsync(User.Identity.GetUserId());

            if (result.Succeeded && message.HasValue)
            {
                string text;
                var type = MessageTypes.Success;

                switch (message.Value)
                {
                    case ManageMessageId.SetPhoneSuccess:
                        text = Resources.Account.AddPhoneSuccess;
                        break;

                    case ManageMessageId.ChangePasswordSuccess:
                        text = Resources.Account.ChangePasswordSuccess;
                        break;

                    case ManageMessageId.Error:
                        text = Resources.Account.Error;
                        type = MessageTypes.Error;
                        break;

                    case ManageMessageId.SetPasswordSuccess:
                        text = Resources.Account.SetPasswordSuccess;
                        break;

                    case ManageMessageId.RemovePhoneSuccess:
                        text = Resources.Account.RemovePhoneSuccess;
                        break;

                    case ManageMessageId.SetTwoFactorSuccess:
                        text = Resources.Account.SetTwoFactorSuccess;
                        break;

                    default:
                        text = Convert.ToString(message.Value);
                        break;
                }

                result.Messages.Add(new Message(type, text, name: Convert.ToString(message.Value)));
            }
            
            return View(result);
        }

        //
        // POST: /user/remove-login
        [HttpPost, Route("remove-login")/*, ValidateAntiForgeryToken*/]
        public async Task<ActionResult> RemoveLogin(string loginProvider, string providerKey)
        {
            var result = await HttpContext.GetOwinContext().RemoveLoginAsync(User.Identity.GetUserId(), loginProvider, providerKey);

            return RedirectToAction("ManageLogins", new { message = (result.Succeeded ? ManageMessageId.RemoveLoginSuccess : ManageMessageId.Error) });
        }

        //
        // GET: /user/phone-number
        [HttpGet, Route("phone-number")]
        public ViewResult SetPhoneNumber()
        {
            return View();
        }

        //
        // POST: /user/phone-number
        [HttpPost, Route("phone-number")/*, ValidateAntiForgeryToken*/]
        public async Task<ActionResult> SetPhoneNumber(PhoneNumberModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(Result.Fail(model, ModelState.GetErrorMessages()));
            }

            // Send an SMS through the SMS provider to verify the phone number
            var result = await HttpContext.GetOwinContext().SetPhoneNumberAsync(model.Number);
            
            if (!result.Succeeded)
            {
                return View(Result.Fail(model, result.Messages));
            }

            var code = result.OfType<string>().SingleOrDefault();

            if (code == null)
            {
                return RedirectToAction("Index", new { Message = ManageMessageId.SetPhoneSuccess });
            }

            result = await Twilio.SendAsync(string.Format(Uos.Cognito.Strings.ChangePhoneNumberMessage, code), model.Number);
            
            if (!result.Succeeded)
            {
                return View(Result.Fail(model, result.Messages));
            }

            return RedirectToAction("VerifyPhoneNumber");
        }

        //
        // POST: /user/enable-two-factor-authentication
        [HttpPost, Route("enable-two-factor-authentication")/*, ValidateAntiForgeryToken*/]
        public async Task<ActionResult> EnableTwoFactorAuthentication()
        {
            var result = await HttpContext.GetOwinContext().SetTwoFactorEnabledAsync(User.Identity.GetUserId(), true);

            return RedirectToAction("Index", "User");
        }

        //
        // POST: /user/disable-two-factor-authentication
        [HttpPost, Route("disable-two-factor-authentication")/*, ValidateAntiForgeryToken*/]
        public async Task<ActionResult> DisableTwoFactorAuthentication()
        {
            var result = await HttpContext.GetOwinContext().SetTwoFactorEnabledAsync(User.Identity.GetUserId(), false);

            return RedirectToAction("Index", "User");
        }

        //
        // GET: /user/verify-phone-mumber
        [HttpGet, Route("verify-phone-mumber")]
        public ViewResult VerifyPhoneNumber()
        {
            return View(Result.Ok(new VerifyPhoneNumberModel()));
        }

        //
        // POST: /user/verify-phone-mumber
        [HttpPost, Route("verify-phone-mumber")/*, ValidateAntiForgeryToken*/]
        public async Task<ActionResult> VerifyPhoneNumber(VerifyPhoneNumberModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(Result.Fail(model, ModelState.GetErrorMessages()));
            }

            var result = await HttpContext.GetOwinContext().VerifyPhoneNumberAsync(model.Code);
            
            if (!result.Succeeded)
            {
                // If we got this far, something failed, redisplay form
                return View(Result.Fail(model, result.Messages));
            }

            return RedirectToAction("Index", new { Message = ManageMessageId.SetPhoneSuccess });
        }

        //
        // GET: /user/remove-phone-mumber
        [HttpGet, Route("remove-phone-mumber")]
        public async Task<ActionResult> RemovePhoneNumber()
        {
            var result = await HttpContext.GetOwinContext().SetPhoneNumberAsync(null);

            if (!result.Succeeded)
            {
                return RedirectToAction("Index", new { Message = ManageMessageId.Error });
            }

            return RedirectToAction("Index", new { Message = ManageMessageId.RemovePhoneSuccess });
        }

        //
        // GET: /user/change-password
        [HttpGet, Route("change-password")]
        public ViewResult ChangePassword()
        {
            return View();
        }

        //
        // POST: /user/change-password
        [HttpPost, Route("change-password")/*, ValidateAntiForgeryToken*/]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(Result.Fail(model, ModelState.GetErrorMessages()));
            }

            var result = await HttpContext.GetOwinContext().ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);

            if (!result.Succeeded)
            {
                // If we got this far, something failed, redisplay form
                return View(Result.Ok(model, result.Messages));
            }

            return RedirectToAction("Index", new { Message = ManageMessageId.ChangePasswordSuccess });
        }

        //
        // GET: /user/set-password
        [HttpGet, Route("set-password")]
        public ActionResult SetPassword()
        {
            return View();
        }

        //
        // POST: /user/set-password
        [HttpPost, Route("set-password")/*, ValidateAntiForgeryToken*/]
        public async Task<ActionResult> SetPassword(SetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(Result.Fail(model, ModelState.GetErrorMessages()));
            }

            var result = await HttpContext.GetOwinContext().SetPasswordAsync(User.Identity.GetUserId(), model.NewPassword);

            if (!result.Succeeded)
            {
                // If we got this far, something failed, redisplay form
                return View(Result.Ok(model, result.Messages));
            }

            return RedirectToAction("Index", new { Message = ManageMessageId.SetPasswordSuccess });
        }

        //
        // GET: /user/logins
        [HttpGet, Route("logins")]
        public async Task<ViewResult> ManageLogins(ManageMessageId? message)
        {
            var result = await HttpContext.GetOwinContext().GetExternalLoginsAsync(User.Identity.GetUserId());

            if (result.Succeeded && message.HasValue)
            {
                string text;
                var type = MessageTypes.Success;

                switch (message.Value)
                {
                    case ManageMessageId.Error:
                        text = Resources.Account.Error;
                        type = MessageTypes.Error;
                        break;

                    case ManageMessageId.RemoveLoginSuccess:
                        text = Resources.Account.RemoveLoginSuccess;
                        break;

                    default:
                        text = Convert.ToString(message.Value);
                        break;
                }

                result.Messages.Add(new Message(type, text, name: Convert.ToString(message.Value)));
            }

            return View(result);
        }

        //
        // POST: /user/link-login
        [HttpPost, Route("link-login")/*, ValidateAntiForgeryToken*/]
        public ActionResult LinkLogin(string provider)
        {
            // Request a redirect to the external login provider to link a login for the current user
            return new HomeController.ChallengeResult(provider, Url.Action("LinkLoginCallback", "User"), User.Identity.GetUserId());
        }

        //
        // GET: /user/link-login-callback
        [HttpGet, Route("link-login-callback")]
        public async Task<ActionResult> LinkLoginCallback()
        {
            var result = await HttpContext.GetOwinContext().AddExternalLoginAsync(User.Identity.GetUserId());

            return result.Succeeded ? RedirectToAction("ManageLogins") : RedirectToAction("ManageLogins", new { Message = ManageMessageId.Error });
        }

        #region Helpers

        public enum ManageMessageId
        {
            SetPhoneSuccess,
            ChangePasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            Error
        }

        #endregion
    }
}