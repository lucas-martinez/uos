﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Uos.Cognito;

namespace Uos.Web.Controllers
{
    using Models;
    using Helpers;

    [RoutePrefix("")]
    public class HomeController : Controller
    {
        //
        // GET: /
        [Route("")]
        public ViewResult Index()
        {
            //if (!User.Identity.IsAuthenticated) RedirectToActionPermanent("Dashboard");

            return View();
        }

        //
        // GET: /about
        [HttpGet, Route("about")]
        public ViewResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        //
        // GET: /contact
        [HttpGet, Route("contact")]
        public ViewResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        //
        // GET: /confirm-email
        [HttpGet, AllowAnonymous, Route("confirm-email")]
        public async Task<ActionResult> ConfirmEmail(string email, string code)
        {
            var result = await HttpContext.GetOwinContext().ConfirmEmailAsync(email, code);

            return result.Succeeded ? View("ConfirmEmail") : View("Error", result);
        }

        //
        // GET: /dashboard
        [Route("dashboard")]
        public ViewResult Dashboard()
        {
            if (!User.Identity.IsAuthenticated) return View("Index");

            return View();
        }

        //
        // GET: /enter
        [HttpGet, Route("enter")]
        public ViewResult Enter(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;

            return View(Result.Ok(new EnterModel()));
        }

        //
        // POST: /enter
        [AllowAnonymous, HttpPost, Route("enter")/*, ValidateAntiForgeryToken*/]
        public ViewResult Enter(EnterModel model, string provider, string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            /*
            var request = HttpContext.ApplicationInstance.Context.Request;

            var authentication = Authentication as AuthenticationProvider;

            Uri uri = request.Url,
                referrerUri = request.UrlReferrer,
                redirectUri = referrerUri;

            if (returnUrl == null || Uri.TryCreate(returnUrl, UriKind.RelativeOrAbsolute, out redirectUri))
            {
                redirectUri = referrerUri;
            }

            if (referrerUri.Host != uri.Host)
            {
                // validate domain
                var domain = authentication.FindDomainAsync(referrerUri.Host);

                if (domain == null) return HttpNotFound();

                if (!redirectUri.IsAbsoluteUri)
                {
                    redirectUri = new Uri(new Uri(referrerUri.GetLeftPart(UriPartial.Scheme | UriPartial.Authority), UriKind.Absolute), referrerUri.PathAndQuery);
                }

                // validate ConsumerKey and ConsumerSecret?
            }
            else if (redirectUri.IsAbsoluteUri)
            {
                redirectUri = new Uri(redirectUri.PathAndQuery, UriKind.Relative);
            }

            var properties = authentication.GetAuthenticationProperties(Convert.ToString(redirectUri));

            return new ChallengeResult(provider, properties);
            */
            return View(Result.Ok(model));
        }

        //
        // POST: /external-login
        [HttpPost, AllowAnonymous, Route("external-login")/*, ValidateAntiForgeryToken*/]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Format(Web.Activities.User.ExternalLoginCallback, new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /external-login-callback
        [HttpGet, AllowAnonymous, Route("external-login-callback")]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            IResult result;

            // Google always redirects to /signin-google which is route mapped to this action.
            if (User.Identity.IsAuthenticated)
            {
                result = await HttpContext.GetOwinContext().AddExternalLoginAsync(User.Identity.GetUserId());

                return result.Succeeded ? RedirectToAction("ManageLogins", "User") : RedirectToAction("ManageLogins", "User", new { Message = UserController.ManageMessageId.Error });
            }

            // Sign in the user with this external login provider if the user already has a login
            result = await HttpContext.GetOwinContext().ExternalSignInAsync();

            if (result.Succeeded)
            {
                return RedirectToLocal(returnUrl);
            }

            if (result.Is(() => Cognito.Strings.ExternalLoginFailed))
            {   
                return RedirectToAction("Login");
            }

            if (result.Is(() => Cognito.Strings.LockedOut))
            {
                return View("Lockout", result);
            }

            if (result.Is(() => Cognito.Strings.RequiresVerification))
            {
                return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
            }

            if (result.Is(() => Cognito.Strings.AccountNotFound))
            {
                var loginInfo = result.OfType<ExternalLoginInfo>().FirstOrDefault();

                // If the user does not have an account, then prompt the user to create an account
                ViewBag.ReturnUrl = returnUrl;
                ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                return View("ExternalLoginConfirmation", Result.Ok(new ExternalLoginConfirmationViewModel { Email = loginInfo.Email }, result.Messages));
            }

            return RedirectToAction("Login");
        }

        //
        // POST: /external-login-confirmation
        [HttpPost, AllowAnonymous, Route("external-login-confirmation")/*, ValidateAntiForgeryToken*/]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }

            IResult result;

            if (!ModelState.IsValid)
            {
                result = Helpers.ModelState.GetResult(ModelState);
            }
            else if ((result = await HttpContext.GetOwinContext().RegisterExternalLoginAsync(model.Email)).Succeeded)
            {
                return RedirectToLocal(returnUrl);
            }

            ViewBag.ReturnUrl = returnUrl;

            return View(Helpers.ModelState.GetResult(ModelState, model));
        }

        //
        // GET: /external-login-failure
        [AllowAnonymous, Route("external-login-failure")]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        //
        // GET: /forgot-password
        [HttpGet, AllowAnonymous, Route("forgot-password")]
        public ViewResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /forgot-password
        [HttpPost, AllowAnonymous, Route("forgot-password")/*, ValidateAntiForgeryToken*/]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await HttpContext.GetOwinContext().IsEmailConfirmedAsync(model.Email);

                if (!result.Succeeded)
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }

                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                result = await HttpContext.GetOwinContext().GeneratePasswordResetTokenAsync(model.Email);

                if (!result.Succeeded)
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }

                var token = result.OfType<string>().Single();
                
                result = await Email.SendAsync(Resources.Domain.ResetPasswordTitle, "Email/Outgoing/ResetPassword.cshtml", new { callbackUrl = Url.Action("ResetPassword", "Home", new { email = model.Email, code = token }, protocol: Request.Url.Scheme) }, model.Email);
                
                return RedirectToAction("ForgotPasswordConfirmation", "Home");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /forgot-password-confirmation
        [HttpGet, AllowAnonymous, Route("forgot-password-confirmation")]
        public ViewResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /login
        [AllowAnonymous]
        [HttpGet, Route("login")]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;

            var model = new LoginViewModel();

            var result = Result.Ok(model);

            return View(result);
        }

        //
        // POST: /login
        [HttpPost, AllowAnonymous, Route("login")/*, ValidateAntiForgeryToken*/]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(Result.Fail(model, ModelState.GetErrorMessages()));
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var status = await HttpContext.GetOwinContext().LoginAsync(email: model.Email, password: model.Password, rememberMe: model.RememberMe);

            switch (status)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);

                case SignInStatus.LockedOut:
                    return View("Lockout");

                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });

                case SignInStatus.Failure:

                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(Result.Fail(model, new List<Message> { new Message(MessageTypes.Error, "Invalid login attempt.") }));
            }
        }

        //
        // POST: /logout
        [HttpPost, Route("logout")/*, ValidateAntiForgeryToken*/]
        public ActionResult Logout(string code)
        {
            var result = HttpContext.GetOwinContext().Logout(DefaultAuthenticationTypes.ApplicationCookie);

            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /register
        [HttpGet, AllowAnonymous, Route("register")]
        public ViewResult Register()
        {
            var model = new RegisterViewModel();

            return View(Result.Ok(model));
        }

        //
        // POST: /register
        [HttpPost, AllowAnonymous, Route("register")/*, ValidateAntiForgeryToken*/]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            IResult result;

            if (!ModelState.IsValid)
            {
                result = Helpers.ModelState.GetResult(ModelState);
            }
            else if ((result = await HttpContext.GetOwinContext().RegisterAsync(email: model.Email, password: model.Password, urlFormat: (email, code) => Url.Action("ConfirmEmail", "User", new { email = email, code = code }, protocol: Request.Url.Scheme))).Succeeded)
            {
                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                result = await HttpContext.GetOwinContext().GenerateEmailConfirmationTokenAsync(model.Email);

                if (!result.Succeeded)
                {
                    return View(Result.Fail(model, result.Messages));
                }

                var token = result.OfType<string>().Single();

                result = await Email.SendAsync(Resources.Domain.ConfirmEmailTitle, "Email/Outgoing/ConfirmEmail.cshtml", new { callbackUrl = Url.Action("ConfirmEmail", "Home", new { email = model.Email, code = token }, protocol: Request.Url.Scheme) }, model.Email);

                if (!result.Succeeded)
                {
                    return View(Result.Fail(model, result.Messages));
                }

                return RedirectToAction("Index", "Home");
            }

            // If we got this far, something failed, redisplay form
            return View(Result.Fail(model, result.Messages));
        }

        //
        // GET: /reset-password
        [HttpGet, AllowAnonymous, Route("reset-password")]
        public ViewResult ResetPassword(string email, string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /reset-password
        [HttpPost, AllowAnonymous, Route("reset-password")/*, ValidateAntiForgeryToken*/]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var result = await HttpContext.GetOwinContext().ResetPasswordAsync(model.Email, model.Code, model.Password);

            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "User");
            }

            if (result.Is(() => Cognito.Strings.CodeArgumentRequired, MessageTypes.Error))
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "User");
            }

            return View(result);
        }

        //
        // GET: /reset-password-confirmation
        [HttpGet, AllowAnonymous, Route("reset-password-confirmation")]
        public ViewResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /send-code
        [HttpGet, AllowAnonymous, Route("send-code")]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var result = await HttpContext.GetOwinContext().SendCode();

            if (!result.Succeeded)
            {
                return View("Error", result);
            }

            var factorOptions = result.OfType<string>().Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();

            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /send-code
        [HttpPost, AllowAnonymous, Route("send-code")/*, ValidateAntiForgeryToken*/]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            var result = await HttpContext.GetOwinContext().SendTwoFactorCodeAsync(model.SelectedProvider);

            if (!result.Succeeded)
            {
                return View("Error", result);
            }

            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET|POST: /switch/{id}
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post), OutputCache(NoStore = true, Duration = 0, VaryByParam = "id", Location = System.Web.UI.OutputCacheLocation.None), Route("switch/{id?}")]
        public async Task<ActionResult> Switch(string id, string returnUrl)
        {
            await HttpContext.GetOwinContext().Switch(id, DefaultAuthenticationTypes.ApplicationCookie);

            if (returnUrl != null)
            {
                return RedirectPermanent(returnUrl);
            }
            
            var localRoot = new Uri(Request.Url.GetLeftPart(UriPartial.Scheme | UriPartial.Authority), UriKind.Absolute);

            var returnUri = Request.UrlReferrer;

            if (returnUri == null)
            {
                return RedirectPermanent(localRoot.AbsoluteUri);
            }

            if (localRoot.Host == returnUri.Host)
            {
                return RedirectPermanent(returnUri.PathAndQuery);
            }

            return RedirectPermanent(returnUri.AbsoluteUri);
        }

        //
        // GET: /verify-code
        [HttpGet, AllowAnonymous, Route("verify-code")]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            var result = await HttpContext.GetOwinContext().HasBeenVerified();

            // Require that the user has already logged in via username/password or external login
            if (!result.Succeeded)
            {
                return View("Error", result);
            }

            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /verify-code
        [HttpPost, AllowAnonymous, Route("verify-code")/*, ValidateAntiForgeryToken*/]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(Helpers.ModelState.GetResult(ModelState, model));
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await HttpContext.GetOwinContext().TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);

            if (result.Succeeded)
            {
                return RedirectToLocal(model.ReturnUrl);
            }

            if (result.Is(() => Cognito.Strings.LockedOut, MessageTypes.Error))
            {
                return View("Lockout", result);
            }

            foreach (var message in result.Messages)
            {
                ModelState.AddModelError("", message);
            }

            return View(Result.Fail(model, result.Messages));
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };

                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }

                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}