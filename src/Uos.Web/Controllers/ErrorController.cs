﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Uos.Web.Controllers
{
    [RoutePrefix("error")]
    public class ErrorController : Controller
    {
        //
        // GET: /error/
        [HttpGet, Route("")]
        public ActionResult Index()
        {
            return RedirectToAction("GenericError", new HandleErrorInfo(new HttpException(403, "Dont allow access the error pages"), "ErrorController", "Index"));
        }

        //
        // GET: /error/generic
        [HttpGet, Route("generic")]
        public ViewResult GenericError(HandleErrorInfo info)
        {
            return View("Error", Result.Fail(info.Exception));
        }

        //
        // GET: /error/404
        [HttpGet, Route("404")]
        public ViewResult NotFound(HandleErrorInfo info)
        {
            ViewBag.Title = "Page Not Found";

            return View("Error", new Result(info.Exception));
        }
    }
}