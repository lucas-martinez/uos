﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace Uos.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        /*protected void Application_Error(object sender, EventArgs e)
        {
            Web.Mvc.ErrorHandler.Handle((MvcApplication)sender);
        }*/

        protected void Application_Start()
        {
            DependencyConfig.RegisterDependencies();
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
