//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "14.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Financial {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Financial() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.Financial", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Financial Accounting.
        /// </summary>
        internal static string Accounting {
            get {
                return ResourceManager.GetString("Accounting", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;span class=&quot;visible-lg-inline&quot;&gt;Financial &lt;/span&gt;Accounting.
        /// </summary>
        internal static string AccountingNav {
            get {
                return ResourceManager.GetString("AccountingNav", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Financial Accounting.
        /// </summary>
        internal static string AccountingTitle {
            get {
                return ResourceManager.GetString("AccountingTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Activity-Based Costing.
        /// </summary>
        internal static string Activity {
            get {
                return ResourceManager.GetString("Activity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Activity-Based Costing.
        /// </summary>
        internal static string ActivityNav {
            get {
                return ResourceManager.GetString("ActivityNav", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Activity-Based Costing.
        /// </summary>
        internal static string ActivityTitle {
            get {
                return ResourceManager.GetString("ActivityTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sales and Profitability Analysis.
        /// </summary>
        internal static string Analysis {
            get {
                return ResourceManager.GetString("Analysis", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sales and Profitability Analysis.
        /// </summary>
        internal static string AnalysisNav {
            get {
                return ResourceManager.GetString("AnalysisNav", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sales and Profitability Analysis.
        /// </summary>
        internal static string AnalysisTitle {
            get {
                return ResourceManager.GetString("AnalysisTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Asset Accounting.
        /// </summary>
        internal static string Asset {
            get {
                return ResourceManager.GetString("Asset", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Asset Accounting.
        /// </summary>
        internal static string AssetNav {
            get {
                return ResourceManager.GetString("AssetNav", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Asset Accounting.
        /// </summary>
        internal static string AssetTitle {
            get {
                return ResourceManager.GetString("AssetTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Capital Investment Management.
        /// </summary>
        internal static string Capital {
            get {
                return ResourceManager.GetString("Capital", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Capital Investment Management (programs).
        /// </summary>
        internal static string CapitalNav {
            get {
                return ResourceManager.GetString("CapitalNav", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Capital Investment Management.
        /// </summary>
        internal static string CapitalTitle {
            get {
                return ResourceManager.GetString("CapitalTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cash Management.
        /// </summary>
        internal static string Cash {
            get {
                return ResourceManager.GetString("Cash", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cash Management.
        /// </summary>
        internal static string CashNav {
            get {
                return ResourceManager.GetString("CashNav", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cash Management.
        /// </summary>
        internal static string CashTitle {
            get {
                return ResourceManager.GetString("CashTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Consolidation.
        /// </summary>
        internal static string Consolidation {
            get {
                return ResourceManager.GetString("Consolidation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Consolidation.
        /// </summary>
        internal static string ConsolidationNav {
            get {
                return ResourceManager.GetString("ConsolidationNav", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Consolidation.
        /// </summary>
        internal static string ConsolidationTitle {
            get {
                return ResourceManager.GetString("ConsolidationTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Controlling.
        /// </summary>
        internal static string Controlling {
            get {
                return ResourceManager.GetString("Controlling", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Controlling.
        /// </summary>
        internal static string ControllingNav {
            get {
                return ResourceManager.GetString("ControllingNav", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Controlling.
        /// </summary>
        internal static string ControllingTitle {
            get {
                return ResourceManager.GetString("ControllingTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enterprise Controlling.
        /// </summary>
        internal static string Enterprise {
            get {
                return ResourceManager.GetString("Enterprise", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enterprise&lt;span class=&quot;visible-lg-inline&quot;&gt; Controlling&lt;/span&gt;.
        /// </summary>
        internal static string EnterpriseNav {
            get {
                return ResourceManager.GetString("EnterpriseNav", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enterprise Controlling.
        /// </summary>
        internal static string EnterpriseTitle {
            get {
                return ResourceManager.GetString("EnterpriseTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Executive Information System.
        /// </summary>
        internal static string Executive {
            get {
                return ResourceManager.GetString("Executive", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Executive Information System.
        /// </summary>
        internal static string ExecutiveNav {
            get {
                return ResourceManager.GetString("ExecutiveNav", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Executive Information System.
        /// </summary>
        internal static string ExecutiveTitle {
            get {
                return ResourceManager.GetString("ExecutiveTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Financial.
        /// </summary>
        internal static string FinancialNav {
            get {
                return ResourceManager.GetString("FinancialNav", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Financial Services.
        /// </summary>
        internal static string FinancialTitle {
            get {
                return ResourceManager.GetString("FinancialTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tangible Fixed Assets.
        /// </summary>
        internal static string Fixed {
            get {
                return ResourceManager.GetString("Fixed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tangible Fixed Assets (measures).
        /// </summary>
        internal static string FixedNav {
            get {
                return ResourceManager.GetString("FixedNav", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tangible Fixed Assets.
        /// </summary>
        internal static string FixedTitle {
            get {
                return ResourceManager.GetString("FixedTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Founds Management.
        /// </summary>
        internal static string Founds {
            get {
                return ResourceManager.GetString("Founds", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Founds Management.
        /// </summary>
        internal static string FoundsNav {
            get {
                return ResourceManager.GetString("FoundsNav", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Founds Management.
        /// </summary>
        internal static string FoundsTitle {
            get {
                return ResourceManager.GetString("FoundsTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to General Ledger Accounting.
        /// </summary>
        internal static string General {
            get {
                return ResourceManager.GetString("General", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to General Ledger Accounting.
        /// </summary>
        internal static string GeneralNav {
            get {
                return ResourceManager.GetString("GeneralNav", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to General Ledger Accounting.
        /// </summary>
        internal static string GeneralTitle {
            get {
                return ResourceManager.GetString("GeneralTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Overview.
        /// </summary>
        internal static string HomeNav {
            get {
                return ResourceManager.GetString("HomeNav", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Overview.
        /// </summary>
        internal static string HomeTitle {
            get {
                return ResourceManager.GetString("HomeTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Capital Investment Management.
        /// </summary>
        internal static string Investment {
            get {
                return ResourceManager.GetString("Investment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;span class=&quot;visible-lg-inline&quot;&gt;Capital &lt;/span&gt;Investment&lt;span class=&quot;visible-lg-inline&quot;&gt; Management&lt;/span&gt;.
        /// </summary>
        internal static string InvestmentNav {
            get {
                return ResourceManager.GetString("InvestmentNav", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Capital Investment Management.
        /// </summary>
        internal static string InvestmentTitle {
            get {
                return ResourceManager.GetString("InvestmentTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Management Consolidation.
        /// </summary>
        internal static string Management {
            get {
                return ResourceManager.GetString("Management", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Management Consolidation.
        /// </summary>
        internal static string ManagementNav {
            get {
                return ResourceManager.GetString("ManagementNav", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Management Consolidation.
        /// </summary>
        internal static string ManagementTitle {
            get {
                return ResourceManager.GetString("ManagementTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Overhead Cost Control.
        /// </summary>
        internal static string Overhead {
            get {
                return ResourceManager.GetString("Overhead", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Overhead Cost Control.
        /// </summary>
        internal static string OverheadNav {
            get {
                return ResourceManager.GetString("OverheadNav", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Overhead Cost Control.
        /// </summary>
        internal static string OverheadTitle {
            get {
                return ResourceManager.GetString("OverheadTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Accounts Payable.
        /// </summary>
        internal static string Payable {
            get {
                return ResourceManager.GetString("Payable", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Accounts Payable.
        /// </summary>
        internal static string PayableNav {
            get {
                return ResourceManager.GetString("PayableNav", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Accounts Payable.
        /// </summary>
        internal static string PayableTitle {
            get {
                return ResourceManager.GetString("PayableTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product Cost Controlling.
        /// </summary>
        internal static string Product {
            get {
                return ResourceManager.GetString("Product", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product Cost Controlling.
        /// </summary>
        internal static string ProductNav {
            get {
                return ResourceManager.GetString("ProductNav", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product Cost Controlling.
        /// </summary>
        internal static string ProductTitle {
            get {
                return ResourceManager.GetString("ProductTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Profit Center Accounting.
        /// </summary>
        internal static string Profit {
            get {
                return ResourceManager.GetString("Profit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Profit Center Accounting.
        /// </summary>
        internal static string ProfitNav {
            get {
                return ResourceManager.GetString("ProfitNav", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Profit Center Accounting.
        /// </summary>
        internal static string ProfitTitle {
            get {
                return ResourceManager.GetString("ProfitTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Accounts Receivable.
        /// </summary>
        internal static string Receivable {
            get {
                return ResourceManager.GetString("Receivable", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Accounts Receivable.
        /// </summary>
        internal static string ReceivableNav {
            get {
                return ResourceManager.GetString("ReceivableNav", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Accounts Receivable.
        /// </summary>
        internal static string ReceivableTitle {
            get {
                return ResourceManager.GetString("ReceivableTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Market Risk Management.
        /// </summary>
        internal static string Risk {
            get {
                return ResourceManager.GetString("Risk", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Market Risk Management.
        /// </summary>
        internal static string RiskNav {
            get {
                return ResourceManager.GetString("RiskNav", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Market Risk Management.
        /// </summary>
        internal static string RiskTitle {
            get {
                return ResourceManager.GetString("RiskTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Special Purpose Ledger.
        /// </summary>
        internal static string Special {
            get {
                return ResourceManager.GetString("Special", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Special Purpose Ledger.
        /// </summary>
        internal static string SpecialNav {
            get {
                return ResourceManager.GetString("SpecialNav", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Special Purpose Ledger.
        /// </summary>
        internal static string SpecialTitle {
            get {
                return ResourceManager.GetString("SpecialTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Treasury.
        /// </summary>
        internal static string Treasury {
            get {
                return ResourceManager.GetString("Treasury", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Treasury Management.
        /// </summary>
        internal static string TreasuryMan {
            get {
                return ResourceManager.GetString("TreasuryMan", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Treasury Management.
        /// </summary>
        internal static string TreasuryManNav {
            get {
                return ResourceManager.GetString("TreasuryManNav", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Treasury Management.
        /// </summary>
        internal static string TreasuryManTitle {
            get {
                return ResourceManager.GetString("TreasuryManTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Treasury.
        /// </summary>
        internal static string TreasuryNav {
            get {
                return ResourceManager.GetString("TreasuryNav", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Treasury.
        /// </summary>
        internal static string TreasuryTitle {
            get {
                return ResourceManager.GetString("TreasuryTitle", resourceCulture);
            }
        }
    }
}
