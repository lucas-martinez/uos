﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Constants
{
    [Flags]
    public enum ControlSections
    {
        Batch = Browse | Detail | Edit | Entry | Review,

        Browse = 1,

        Detail = 2,

        Edit = 4,

        Entry = 8,

        Inline = Browse | Detail | Edit | Entry,

        Review = 0x10
    }
}
