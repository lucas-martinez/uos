﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Constants
{
    [Flags]
    public enum ControlTools
    {
        All = Email | Filter | History | Moment | Pdf,

        Download = 0x40,

        Email = 0x20,

        Filter = 1,

        History = 2,

        Moment = 0x10,

        Pdf = 4,

        Print = 8,

        Upload = 0x80
    }
}
