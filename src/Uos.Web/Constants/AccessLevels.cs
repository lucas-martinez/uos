﻿using System;
using System.Runtime.Serialization;

namespace Uos.Web.Constants
{
    [Flags, DataContract]
    public enum AccessLevels : short
    {
        /// <summary>
        /// A.K.A Pending. Nor the emails addresss or phone number have been confirmed.
        /// If the user login with an exxternal provider sucha s Google and the provider
        /// claims the user email is verified, the the user is recognized as authenticated.
        /// </summary>
        Alleged = 0x0100, // 1 << 8


        /// <summary>
        /// The user is authenticated, eiher using Uos cedentials or an external 
        /// provider;
        /// </summary>
        [EnumMember(Value = AccessibilityLevels.Authenticated)]
        Authenticated = 0x0200, // 1 << 9

        /// <summary>
        /// The user is an authenticated Uos user with a company segment account 
        /// where the company of the segment is the same as the company of the product
        /// and the segment registered as an active consumer of the product.
        /// </summary>
        [EnumMember(Value = AccessibilityLevels.Internal)]
        Internal = Private | 0x2000, // 1 << 13

        /// <summary>
        /// The user is an authenticated Uos user with a company segment account
        /// where the segment registered as an active consumer of the product. 
        /// </summary>
        [EnumMember(Value = AccessibilityLevels.Private)]
        Private = 0x1000, // 1 << 12

        /// <summary>
        /// The user is an authenticated Uos user and passed the google reCAPTCHA
        /// or the user used an exteral provider and a multifactor authentication.
        /// </summary>
        [EnumMember(Value = AccessibilityLevels.Protected)]
        Protected = Authenticated | 0x0800, // 1 << 11

        /// <summary>
        /// User may or not be authenticated (with an external provider) but not as 
        /// an Uos user.
        /// </summary>
        [EnumMember(Value = AccessibilityLevels.Public)]
        Public = 0,

        /// <summary>
        /// The user is an authenticated Uos user and went thru the two factor 
        /// authentication.
        /// </summary>
        [EnumMember(Value = AccessibilityLevels.Secured)]
        Secured = Protected | 0x4000, // 1 << 14

        /// <summary>
        /// The user is autenticated by an external provider. However, it does mean
        /// the user is an authenticated Uos user unless the Protected flag is 
        /// present.
        /// </summary>
        [EnumMember(Value = AccessibilityLevels.Social)]
        Social = 0x00FF,

        #region - social -

        [EnumMember(Value = AccessibilityLevels.Social)]
        Facebook = 1,

        [EnumMember(Value = AccessibilityLevels.Social)]
        Google = 2,

        [EnumMember(Value = AccessibilityLevels.Social)]
        Instagram = 4,

        [EnumMember(Value = AccessibilityLevels.Social)]
        Microsoft = 8,

        [EnumMember(Value = AccessibilityLevels.Social)]
        Twitter = 16

        #endregion - social -
    }
}
