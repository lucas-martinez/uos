﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uos.Domain;

namespace Uos.Web.Constants
{
    class RoleProfiles
    {
        public const string Administration = Roles.Administrator;

        public const string Management = CompanyRoles.Manager + "," + Support;

        public const string Production = CompanyRoles.Producer + "," + Management;

        public const string Transportation = CompanyRoles.Driver + "," + Management;
#if (DEBUG)
        const string Support = Roles.Developer + "," + Roles.Tester;
#elif (RELEASE)
        const string Support = SystemRoles.Debuger;
#else
        const string Support = Roles.Debuger + "," + Roles.Developer + "," + Roles.Tester;
#endif
    }
}
