﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Constants
{
    public class Environments
    {
        public const string Assurance = @"Q&A";

        public const string Development = @"DEV";

        public const string Production = @"PRO";
    }
}
