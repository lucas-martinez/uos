﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Constants
{
    public static class AreaNames
    {
        /// <summary>
        /// Hospitality accoumodation business area
        /// </summary>
        public const string Accommodation = "accommodation";
        
        /// <summary>
        /// Application domain area
        /// </summary>
        public const string Domain = "domain";

        /// <summary>
        /// Logistics distribution business area
        /// </summary>
        public const string Distribution = "distribution";

        /// <summary>
        /// Finance business area
        /// </summary>
        public const string Financial = "financial";

        /// <summary>
        /// Hospitality business area
        /// </summary>
        public const string Hospitality = "hospitality";
        
        /// <summary>
        /// Logistics business area
        /// </summary>
        public const string Logistics = "logistics";

        /// <summary>
        /// User level administration area
        /// </summary>
        public const string Management = "management";

        /// <summary>
        /// 
        /// </summary>
        public const string Materials = "materials";

        /// <summary>
        /// Personnel Administration
        /// </summary>
        public const string Personnel = "personnel";

        /// <summary>
        /// Logistics products planning business area
        /// </summary>
        public const string Products = "products";

        /// <summary>
        /// Logistics project
        /// </summary>
        public const string Projects = "projects";

        /// <summary>
        /// Hospitality restaurant business area
        /// </summary>
        public const string Restaurant = "restaurant";

        /// <summary>
        /// Logistics production retail area
        /// </summary>
        public const string Retail = "retail";

        /// <summary>
        /// Hospitality tourism business area
        /// </summary>
        public const string Tourism = "tourism";
        
        /// <summary>
        /// Hospitality travel business area
        /// </summary>
        public const string Travel = "travel";

        /// <summary>
        /// Transportation Management area
        /// </summary>
        public const string Transpportation = "transportation";

        /// <summary>
        /// Tutorial area
        /// </summary>
        public const string Tutorial = "tutorial";
    }
}
