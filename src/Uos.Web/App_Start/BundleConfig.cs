﻿using System.Web;
using System.Web.Optimization;

namespace Uos.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/css/site").Include(
                "~/Content/Site.css"));

            #region - fonts -

            bundles.Add(new StyleBundle("~/content/css/fontawesome").Include(
                "~/Content/stylesheets/fonts/fontawesome.css"));

            bundles.Add(new StyleBundle("~/content/css/ionicons").Include(
                "~/Content/stylesheets/fonts/ionicons.css"));

            #endregion - fonts -

            #region - bootstrap -

            bundles.Add(new ScriptBundle("~/js/bootstrap").Include(
                "~/Scripts/bootstrap.js",
                "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/content/css/bootstrap").Include(
                "~/Content/stylesheets/bootstrap.css"));

            #endregion - bootstrap -

            #region - jquery -

            bundles.Add(new ScriptBundle("~/js/jquery").Include(
                "~/Scripts/jquery-{version}.js"/*,
                "~/Scripts/jquery-migrate-1.4.1.js"*/));

            bundles.Add(new ScriptBundle("~/js/jquery-ui").Include(
                "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/js/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            /*var jqueryVer = "1.9.1";

            System.Web.UI.ScriptManager.ScriptResourceMapping.AddDefinition("jquery", new System.Web.UI.ScriptResourceDefinition
            {
                Path = "~/Scripts/jquery-" + jqueryVer + ".min.js",
                DebugPath = "~/Scripts/jquery-" + jqueryVer + ".js",
                CdnPath = "http://ajax.aspnetcdn.com/ajax/jQuery/jquery-" + jqueryVer + ".min.js",
                CdnDebugPath = "http://ajax.aspnetcdn.com/ajax/jQuery/jquery-" + jqueryVer + ".js",
                CdnSupportsSecureConnection = true,
                LoadSuccessExpression = "window.jQuery"
            });*/

            #endregion - jquery -

            #region - kendo ui -

            bundles.Add(new ScriptBundle("~/js/kendo").Include(
                "~/Scripts/kendo/2015.3.930/kendo.all.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.aspnetmvc.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.combobox.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.core.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.data.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.data.odata.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.data.xml.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.draganddrop.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.dropdownlist.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.fx.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.list.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.mobile.scroller.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.multiselect.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.popup.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.userevents.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.validator.min.js"));

            bundles.Add(new ScriptBundle("~/js/kendo/data").Include(
                "~/Scripts/kendo/2015.3.930/kendo.data.min",
                "~/Scripts/kendo/2015.3.930/kendo.data.odata.min",
                "~/Scripts/kendo/2015.3.930/kendo.data.signalr.min",
                "~/Scripts/kendo/2015.3.930/kendo.data.xml.min"
                ));

            bundles.Add(new ScriptBundle("~/js/kendo/controls").Include(
                "~/Scripts/kendo/2015.3.930/kendo.autocomplete.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.button.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.calendar.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.color.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.colorpicker.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.combobox.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.core.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.datetimepicker.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.draganddrop.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.dropdownlist.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.editable.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.fx.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.grid.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.list.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.listview.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.maskedtextbox.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.menu.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.multiselect.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.notification.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.numerictextbox.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.pager.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.panelbar.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.popup.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.progressbar.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.resizable.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.responsivepanel.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.router.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.selectable.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.slider.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.sortable.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.splitter.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.tabstrip.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.timepicker.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.timezones.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.toolbar.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.tooltip.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.touch.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.userevents.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.validator.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.view.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.virtuallist.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.webcomponents.min.js",
                "~/Scripts/kendo/2015.3.930/kendo.window.min.js"));

            bundles.Add(new ScriptBundle("~/js/kendo/mobile").Include(
                "~/Scripts/kendo/2015.3.930/kendo.mobile.actionsheet.min",
                "~/Scripts/kendo/2015.3.930/kendo.mobile.application.min",
                "~/Scripts/kendo/2015.3.930/kendo.mobile.button.min",
                "~/Scripts/kendo/2015.3.930/kendo.mobile.buttongroup.min",
                "~/Scripts/kendo/2015.3.930/kendo.mobile.collapsible.min",
                "~/Scripts/kendo/2015.3.930/kendo.mobile.drawer.min",
                "~/Scripts/kendo/2015.3.930/kendo.mobile.listview.min",
                "~/Scripts/kendo/2015.3.930/kendo.mobile.loader.min",
                "~/Scripts/kendo/2015.3.930/kendo.mobile.modalview.min",
                "~/Scripts/kendo/2015.3.930/kendo.mobile.navbar.min",
                "~/Scripts/kendo/2015.3.930/kendo.mobile.pane.min",
                "~/Scripts/kendo/2015.3.930/kendo.mobile.popover.min",
                "~/Scripts/kendo/2015.3.930/kendo.mobile.scroller.min",
                "~/Scripts/kendo/2015.3.930/kendo.mobile.scrollview.min",
                "~/Scripts/kendo/2015.3.930/kendo.mobile.shim.min",
                "~/Scripts/kendo/2015.3.930/kendo.mobile.splitview.min",
                "~/Scripts/kendo/2015.3.930/kendo.mobile.switch.min",
                "~/Scripts/kendo/2015.3.930/kendo.mobile.tabstrip.min",
                "~/Scripts/kendo/2015.3.930/kendo.mobile.view.min"));

            bundles.Add(new StyleBundle("~/content/css/kendo").Include(
               "~/Content/kendo/2015.2.902/kendo.common.min.css",
               "~/Content/kendo/2015.2.902/kendo.common.core.min.css",
               "~/Content/kendo/2015.2.902/kendo.common-bootstrap.min.css",
               "~/Content/kendo/2015.2.902/kendo.common-bootstrap.core.min.css",
               "~/Content/kendo/2015.2.902/kendo.bootstrap.min.css"
               ));

            #endregion - kendo ui -

            #region - modernizer -

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/js/modernizr").Include(
                "~/Scripts/modernizr-*"));

            #endregion - modernizer -

            #region - toastr -

            bundles.Add(new StyleBundle("~/content/css/site").Include(
                "~/Content/Site.css"));

            #endregion - toastr -

            #region - /js/domain/company -

            bundles.Add(new ScriptBundle("~/js/domain/company").Include(
                "~/Scripts/Components/Domain/company-browse.js",
                "~/Scripts/Components/Domain/company-detail.js",
                "~/Scripts/Components/Domain/company-edit.js",
                "~/Scripts/Components/Domain/company-entry.js",
                "~/Scripts/Components/Domain/company-features.js",
                "~/Scripts/Components/Domain/company-filter.js",
                "~/Scripts/Components/Domain/company-form.js",
                "~/Scripts/Components/Domain/company-review.js"));

            #endregion - /js/domain/company -

            bundles.Add(new StyleBundle("~/content/css/toastr").Include(
                "~/Content/stylesheets/toastr.css"));

            BundleTable.EnableOptimizations = true;
        }
    }
}
