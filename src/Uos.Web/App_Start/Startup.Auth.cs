﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Facebook;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.MicrosoftAccount;
using Microsoft.Owin.Security.Twitter;
using Owin;
using System;
using System.Collections.Specialized;
using System.Configuration;

namespace Uos.Web
{
    using Providers;

    public partial class Startup
    {
        public NameValueCollection Settings
        {
            get { return ConfigurationManager.AppSettings; }
        }

        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            // Configure the db context, user manager and signin manager to use a single instance per request
            app.CreatePerOwinContext(Cognito.IdentityDbContext.Create);
            app.CreatePerOwinContext(DefaultDbContext.Create);
            app.CreatePerOwinContext<Cognito.IdentityManager>(Cognito.IdentityManager.Create);
            app.CreatePerOwinContext<Cognito.SignInManager>(Cognito.SignInManager.Create);

            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            // Configure the sign in cookie
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/login"),
                Provider = new CookieAuthenticationProvider
                {
                    // Enables the application to validate the security stamp when the user logs in.
                    // This is a security feature which is used when you change a password or add an external login to your account.  
                    OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<Cognito.IdentityManager, Cognito.IdentityEntity>(
                        validateInterval: TimeSpan.FromMinutes(30),
                        // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
                        regenerateIdentity: (manager, user) => manager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie))
                }
            });            
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Enables the application to temporarily store user information when they are verifying the second factor in the two-factor authentication process.
            app.UseTwoFactorSignInCookie(DefaultAuthenticationTypes.TwoFactorCookie, TimeSpan.FromMinutes(5));

            // Enables the application to remember the second login verification factor such as phone or email.
            // Once you check this option, your second step of verification during the login process will be remembered on the device where you logged in from.
            // This is similar to the RememberMe option when you log in.
            app.UseTwoFactorRememberBrowserCookie(DefaultAuthenticationTypes.TwoFactorRememberBrowserCookie);

            // https://developers.facebook.com/apps/
            /*app.UseFacebookAuthentication(
               appId: AppSettings["facebook:AppId"],
               appSecret: AppSettings["facebook:AppSecret"]
               );*/

            app.UseFacebookAuthentication(new FacebookAuthenticationOptions
            {
                AppId = Settings["facebook:AppId"],
                AppSecret = Settings["facebook:AppSecret"],
                Provider = new FacebookAuthenticationProvider()
            });

            // https://github.com/settings/applications
            /*app.UseGitHubAuthentication(
                clientId: AppSettings["gitbub:ClientId"],
                clientSecret: AppSettings["github:ClientSecret"]);*/

            //app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            //{
            //    ClientId = Settings["google:ClientId"],
            //    ClientSecret = Settings["google:ClientSecret"],
            //    Provider = new GoogleAuthenticationProvider()
            //});

            // https://account.live.com/developers/applications
            /*app.UseMicrosoftAccountAuthentication(
                clientId: AppSettings["microsoft:CliendId"],
                clientSecret: AppSettings["microsoft:CliendSecret"]
                );*/

            /*app.UseMicrosoftAccountAuthentication(new MicrosoftAccountAuthenticationOptions
            {
                ClientId = AppSettings["microsoft:ClientId"],
                ClientSecret = AppSettings["microsoft:ClientSecret"],
                Provider = new MicrosoftAccountAuthenticationProvider()
            });*/

            // https://apps.twitter.com/
            /*.UseTwitterAuthentication(
                consumerKey: AppSettings["twitter:ConsumerKey"],
                consumerSecret: AppSettings["twitter:ConsumerSecret"]);*/
            app.UseTwitterAuthentication(new TwitterAuthenticationOptions
            {
                ConsumerKey = Settings["twitter:ConsumerKey"],
                ConsumerSecret = Settings["twitter:ConsumerSecret"],
                Provider = new TwitterAuthenticationProvider(),
                BackchannelCertificateValidator = new Microsoft.Owin.Security.CertificateSubjectKeyIdentifierValidator(new[]
                {
                    "A5EF0B11CEC04103A34A659048B21CE0572D7D47", // VeriSign Class 3 Secure Server CA - G2
                    "0D445C165344C1827E1D20AB25F40163D8BE79A5", // VeriSign Class 3 Secure Server CA - G3
                    "7FD365A7C2DDECBBF03009F34339FA02AF333133", // VeriSign Class 3 Public Primary Certification Authority - G5
                    "39A55D933676616E73A761DFA16A7E59CDE66FAD", // Symantec Class 3 Secure Server CA - G4
                    "4eb6d578499b1ccf5f581ead56be3d9b6744a5e5", // VeriSign Class 3 Primary CA - G5
                    "5168FF90AF0207753CCCD9656462A212B859723B", // DigiCert SHA2 High Assurance Server C‎A 
                    "B13EC36903F8BF4701D498261A0802EF63642BC3" // DigiCert High Assurance EV Root CA
                })
            });
        }
    }
}