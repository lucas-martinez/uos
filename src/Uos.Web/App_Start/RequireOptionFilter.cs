﻿using RequireJsNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Routing;
using System.Web.Mvc;

namespace Uos.Web
{
    public static class RenderHelper
    {
        public static string RenderAppVersion()
        {
            return "v=" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }
    }
}