﻿using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Compilation;
using System.Web.Http;
using Uos.Cognito;
using Uos.Distribution;
using Uos.Domain;
using Uos.Logistics;
using Uos.Personnel;
using Uos.Purchasing;
using Uos.Transportation;

namespace Uos.Web
{
    public class DependencyConfig
    {
        public static void RegisterDependencies()
        {
            DependencyResolver.Default.Register(GlobalConfiguration.Configuration);
            DependencyResolver.Default.Register<IPasswordHasher, PasswordHasher>();
            //DependencyResolver.Default.Register<IUserStore, EntityStore>();
            //DependencyResolver.Default.Register<ISmsService, TwilioSmsService>();
            //DependencyResolver.Default.Register<ISmtpService, SmtpService>();
            //DependencyResolver.Default.Register<IUmbilikoDbInitialization, UmbilikoDbInitialization>();
            //DependencyResolver.Default.Register(() => System.Web.HttpContext.Current.GetOwinContext());
            //DependencyResolver.Default.Register<IAuthenticationContext>(() => System.Web.HttpContext.Current.GetOwinContext().Get<IAuthenticationContext>());
            /* DependencyResolver.Default.Register<ProductContext>((Func<ProductContext>)(() =>
             {
                 var owinContext = System.Web.HttpContext.Current.GetOwinContext();
                 var dbContext = owinContext.Get<EntityStore>();
                 var assembly = BuildManager.GetGlobalAsaxType().BaseType.Assembly;
                 var companyCode = assembly.GetCustomAttributes<AssemblyTrademarkAttribute>().Select(attr => attr.Trademark).FirstOrDefault();
                 var productName = assembly.GetCustomAttributes<AssemblyProductAttribute>().Select(attr => attr.Product).FirstOrDefault();

                 var company = CompanyContext.FindAsync(dbContext, companyCode).Result;

                 return ProductContext.FindAsync(company, productName).Result;
             }));*/

            /*DependencyResolver.Default.Register<IIdentityStore>(() =>
            {
                var owinContext = HttpContext.Current.GetOwinContext();
                
                return new IdentityStore(owinContext, owinContext.Get<IdentityDbContext>());
            });*/
            
            DependencyResolver.Default.Register<ICompanyStore>(() => new CompanyStore(HttpContext.Current.GetOwinContext().Get<DefaultDbContext>()));
            DependencyResolver.Default.Register<ICustomerStore>(() => new CustomerStore(HttpContext.Current.GetOwinContext().Get<DefaultDbContext>()));
            DependencyResolver.Default.Register<IIdentityStore>(() => new IdentityStore(HttpContext.Current.GetOwinContext()));
            DependencyResolver.Default.Register<ILocationStore>(() => new LocationStore(HttpContext.Current.GetOwinContext().Get<DefaultDbContext>()));
            DependencyResolver.Default.Register<IStafferStore>(() => new StafferStore(HttpContext.Current.GetOwinContext().Get<DefaultDbContext>()));
            DependencyResolver.Default.Register<ITemplateEngine>(((Func<ITemplateEngine>)(() =>
            {
                var builder = new Uos.Web.Configuration.RazorTemplateEngineBuilder();

                var entine = builder.Build(System.Web.Hosting.HostingEnvironment.MapPath("~/Templates"));

                return entine;
            }))());
        }
    }
}
