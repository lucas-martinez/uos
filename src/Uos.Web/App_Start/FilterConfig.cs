﻿using System.Web;
using System.Web.Mvc;

namespace Uos.Web
{
    using Filters;

    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());

            // Handle JsonResult using JsonNetResult which usese Newtonsoft Json serialization
            // So that IgnoreDataMemberAttribute works propertly.
            filters.Add(new JsonHandlerAttribute());
        }
    }
}
