﻿using System.ComponentModel.DataAnnotations;
using System.Web;

namespace Uos.Web.Models
{
    public class UploadModel
    {
        [Required]
        public HttpPostedFileBase File { get; set; }
    }
}
