﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Models
{
    [DataContract]
    public class EnterModel
    {
        [DataMember()]
        public string Code { get; set; }

        [DataMember()]
        public string EmailAddress { get; set; }

        [DataMember()]
        public string Option { get; set; }

        [DataMember()]
        public string Password { get; set; }

        [DataMember()]
        public bool Persist { get; set; }

        [DataMember()]
        public string Token { get; set; }

    }
}
