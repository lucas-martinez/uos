﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Models
{
    [DataContract]
    public class FilterModel
    {
        [DataMember(Name = "field")]
        public string Field { get; set; }

        [DataMember(Name = "ignoreCase")]
        public bool IgnoreCase { get; set; }

        [DataMember(Name = "operator")]
        public string Operator { get; set; }

        [DataMember(Name = "value")]
        public string Value { get; set; }
    }
}
