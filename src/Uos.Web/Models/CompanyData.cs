﻿using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;
using Uos.Cognito;
using Uos.Domain;

namespace Uos.Web.Models
{
    public class CompanyData
    {
        readonly string _companyCode;
        readonly IList<string> _features;

        public CompanyData(HttpContextBase httpContext)
        {
            var identity = httpContext.User.Identity as ClaimsIdentity;

            _companyCode = identity.Claims.Actor().Select(c => c.Value).SingleOrDefault();

            if (_companyCode == null)
            {
                _features = Array.Empty<string>();
            }
            else
            {
                var cacheKey = "COMPANY_" + _companyCode + "_FEATURES";
                var cacheItem = httpContext.Cache[cacheKey];

                _features = cacheItem as IList<string> ?? Array.Empty<string>();

                if (cacheItem == null)
                {
                    var store = DependencyResolver.Default.GetInstance<ICompanyStore>();

                    _features = store.GetCompanyFeatures(_companyCode);

                    httpContext.Cache.Insert(cacheKey, _features, null, DateTime.Now.AddSeconds(60), TimeSpan.Zero);
                }
            }
        }

        public string CompanyCode
        {
            get { return _companyCode; }
        }

        public IList<string> Features
        {
            get { return _features; }
        }

        public bool Any(params string[] features)
        {
            return !Match((IList<string>)features).Count.Equals(0);
        }

        public bool Any(IList<string> features)
        {
            return !Match(features).Count.Equals(0);
        }

        public IList<string> Match(params string[] features)
        {
            return Match((IList<string>)features);
        }

        public IList<string> Match(IList<string> features)
        {
            if (features.Count > 0 && _features.Count == 0)
            {
                return Array.Empty<string>();
            }

            if (_features.Count == 0)
            {
                return Array.Empty<string>();
            }

            return _features.Where(f => features.Contains(f) || features.Any(f2 => f2.StartsWith(f))).ToList();
        }
    }
}