﻿using System.ComponentModel.DataAnnotations;

namespace Uos.Web.Models
{
    public class VerifyPhoneNumberModel
    {
        [Required, Display(Name = "Code")]
        public string Code { get; set; }
    }
}
