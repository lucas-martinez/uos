﻿using System.ComponentModel.DataAnnotations;

namespace Uos.Web.Models
{
    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
