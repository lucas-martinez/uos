﻿using System.ComponentModel.DataAnnotations;

namespace Uos.Web.Models
{
    public class PhoneNumberModel
    {
        [Required]
        [Phone]
        [Display(Name = "Phone Number")]
        public string Number { get; set; }
    }
}
