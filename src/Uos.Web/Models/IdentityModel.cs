﻿using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using Uos.Cognito;

namespace Uos.Web.Models
{
    public class IdentityModel
    {
        public string EmailAddress { get; set; }

        public bool EmailConfirmed { get; set; }

        public bool HasPassword { get; set; }

        public IList<ExternalLogin> Logins { get; set; }

        public string PasswordStrength { get; set; }

        public bool PhoneConfirmed { get; set; }

        public string PhoneNumber { get; set; }

        public bool TwoFactor { get; set; }

        public bool BrowserRemembered { get; set; }
    }
}
