﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Uos.Web.Models
{
    public class NavigationModel
    {
        public class Option
        {   
            readonly Activity _link;
            readonly string _groupText;
            readonly IGrouping<string, string>[] _references;
            readonly string _text;
            
            public Option(string text, string groupText, Activity link, IGrouping<string, string>[] references)
            {
                _groupText = groupText;
                _link = link;
                _references = references;
                _text = text;
            }

            public string GroupText
            {
                get { return _groupText; }
            }

            public Activity Link
            {
                get { return _link; }
            }

            public string Text
            {
                get { return _text; }
            }

            public IGrouping<string, string>[] References
            {
                get { return _references; }
            }
        }

        readonly List<Option> _options = new List<Option>();

        public Option[] Options
        {
            get { return _options.ToArray(); }
        }

        public NavigationModel Add(string text, Activity link, params Activity[] references)
        {
            _options.Add(new Option(text, null, link, references.GroupBy(o => o.Controller, o => o.Action).ToArray()));
            
            return this;
        }

        public NavigationModel Add(string text, string groupText, Activity link, params Activity[] references)
        {
            _options.Add(new Option(text, groupText, link, references.GroupBy(o => o.Controller, o => o.Action).ToArray()));

            return this;
        }
    }
}
