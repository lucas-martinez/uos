﻿using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Uos.Cognito;
using Uos.Domain;

namespace Uos.Web.Filters
{
    using Models;

    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public sealed class FeatureAttribute : AuthorizationFilterAttribute
    {
        readonly string _name;

        public FeatureAttribute(string name)
        {
            _name = name;
        }

        public string Name
        {
            get { return _name; }
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (string.IsNullOrEmpty(_name))
            {
                return true;
            }

            var company = new CompanyData(httpContext);

            return company.Any(_name);
        }
    }
}
