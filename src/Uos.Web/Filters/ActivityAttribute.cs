﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Filters
{
    using Constants;
    using System.Web;

    [AttributeUsage(AttributeTargets.Method, Inherited = false, AllowMultiple = false)]
    public class ActivityAttribute : AuthorizationFilterAttribute
    {
        readonly string _activity;
        string _feature;
        AccessLevels? _level;
        string _roles;

        public ActivityAttribute(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException("name");
            }

            _activity = name;
        }

        public string Feature
        {
            get { return _feature; }
            set { _feature = value; }
        }

        public AccessLevels Level
        {
            get { return _level ?? AccessLevels.Private; }
            set { _level = value; }
        }

        public string Name
        {
            get { return _activity; }
        }

        public string Roles
        {
            get { return _roles ?? string.Empty; }
            set { _roles = value; }
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var feature = _feature ?? Controller.GetType()
                .GetCustomAttributes(typeof(FeatureAttribute), inherit: false)
                .OfType<FeatureAttribute>()
                .Select(attr => attr.Name)
                .SingleOrDefault();

            var roles = Roles.Split(new[] { Characters.Space, Characters.Exclamation, Characters.Comma, Characters.Bar }, StringSplitOptions.RemoveEmptyEntries);

            return true;// Authentication.Authorize(feature: feature, activity: _activity, level: Level, roles: roles) == AuthorizeStatus.Success;
        }
    }
}
