﻿define('uos.domain.staffer.controller', ['jquery', 'uos.component.base', 'uos.domain.staffer.control', 'uos.control.navbar'], function ($, Component, StafferControl) {

    class Controller extends Component {
        
        constructor() {
            super(document.getElementsByTagName('body')[0]);

            this.control = new StafferControl();
        }

        init() {
            if (this.control.init()) {
                // Publish the control is ready so other components initialize.
                this.fire('control:ready', this.control);
            }
            else {
                // Something is wrong. Shuld we alert?
            }
        }
    }

    return Controller;
});