﻿define('uos.domain.identity.controller', ['jquery', 'uos.component.base', 'uos.domain.identity.control', 'uos.control.navbar'], function ($, Component, IdentityControl) {

    class Controller extends Component {

        constructor() {
            super(document.getElementsByTagName('body')[0]);

            this.control = new IdentityControl();
        }

        init() {
            if (this.control.init()) {
                // Publish the control is ready so other components initialize.
                this.fire('control:ready', this.control);
            }
            else {
                // Something is wrong. Shuld we alert?
            }
        }
    }

    return Controller;
});