﻿require(['jquery', 'uos.bootstrap', 'uos.enter', 'bootstrap'], function ($, U, enter) {

    var indexScript = function () {
        this.init();
    };

    indexScript.prototype.init = function () {
        //do stuff with bootstrap
    };

    //create object on DOM ready
    $(function () {
        var entryPoint = new indexScript();

        enter(document.getElementById('enter')).then(function (token) {
            // save token on client cookie
            $('#enter').submit();

            var returnUrl = U.params.returnurl;

            if (typeof returnUrl !== 'string') {
                location.reload();
            }
            else if (returnUrl.indexOf('http') === 0) {
                location.href = returnUrl;
            }
            else {
                location.href = location.host + returnUrl;W
            }
        });
    });
});