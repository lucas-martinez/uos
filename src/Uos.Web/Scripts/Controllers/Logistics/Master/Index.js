﻿require(['jquery', 'bootstrap'], function ($) {

    var indexScript = function () {
        this.init();
    };

    indexScript.prototype.init = function () {
        //do stuff with bootstrap
    };

    //create object on DOM ready
    $(function () {
        var entryPoint = new indexScript();
    });
});