﻿define('uos.component.base', ['jquery', 'uos.bootstrap'], function ($, U) {

    class Component {
        constructor(el) {

            this.element = null;

            if (typeof el === 'string') {
                this.id = el;
            }
            else if (typeof el === 'object') {
                this.element = el;
            }
        }

        fire() {
            U.publish.apply(this, arguments);
        }

        hide() {
            $(this.element).addClass('hidden');
        }

        init() {
            if (typeof this.id === 'string') {
                this.element = document.getElementById(this.id);
            }

            if (!this.element) {
                return false;
            }

            return true;
        }

        on(observable, event, handler) {
            var self = this;
            observable.bind(event, function () {
                    handler.apply(self, arguments);
                });
        }

        show() {
            $(this.element).removeClass('hidden');
        }

        when(event, handler) {
            U.subscribe(event, handler, this);
        }
    }

    return Component;
});