﻿define('kendo.data.contenteditable', ['kendo'], function (kendo) {

    kendo.data.binders.contentEditable = kendo.data.Binder.extend({
        init: function (element, bindings, options) {
            kendo.data.Binder.fn.init.call(this, element, bindings, options);

            var that = this;
            var value = this.bindings.contentEditable.get();

            $(that.element).on("focusin", function () {
                value = that.bindings.contentEditable.get();
            }).on("focusout", function () {
                if (that.element.innerHTML != value) {
                    that.bindings.contentEditable.set(that.element.innerHTML);
                }
            });
        },

        refresh: function () {
            this.element.innerHTML = this.bindings.contentEditable.get();
        }
    });
});