﻿define('uos.enter', ['jquery', 'require', 'knockout', 'uos.bootstrap', 'bootstrap'], function ($, require, ko, U) {

    // spinner preload
    require(['uos.spinner']);

    var _config = {
        options: {
            'show-password': {
                hide: ['#sign-up-confirm-password', '#reset-confirm-password'],
                text: ['#sign-in-password', '#sign-up-password', '#reset-new-password']
            }
        },
        views: {
            'sign-in': {
                hide: ['[value=sign-in]', '[value=reset-password]'],
                show: ['.external-logins', '[name=persist]', '[name=show-password]', '[value=recover-password]', '[value=sign-up]']
            },
            'recover-password': {
                hide: [/*'.external-logins',*/ '[name=persist]', '[name=show-password]', '[value=recover-password]'],
                show: ['[value=sign-in]', '[value=sign-up]', '[value=reset-password]']
            },
            'sign-up': {
                hide: [/*'.external-logins',*/ '[value=sign-up]', '[value=reset-password]'],
                show: ['[value=sign-in]', '[name=persist]', '[name=show-password]', '[value=recover-password]']
            },
            'reset-password': {
                hide: [/*'.external-logins'*/, '[value=reset-password]'],
                show: ['[name=persist]', '[name=show-password]', '[value=sign-in]', '[value=recover-password]', '[value=sign-up]']
            }
        }
    };

    function ViewModel() {
        this.code = ko.observable();
        this.confirm = ko.observable();
        this.email = ko.observable();
        this.password = ko.observable();
        this.token = ko.observable();
    };

    return function (el) {
        var _count = 0,
            _model = new ViewModel(),
            _view = 'sign-in',
            _login = $.Deferred();

        ko.applyBindings(_model, el);

        var $form = $('form', el).first(),
            _baseUrl = $form.prop('action'),
            _mehtod = $form.prop('method');

        function enter(data) {

            if (_count == 0) {
                U.spin();
            }

            _count++;

            U.post(data, '/api/enter').then(function (response) {
                _login.resolve(response);
            }).always(function () {
                if (--_count == 0) {
                    U.stop();
                }
            });
        };

        $('input[name=view]', el).on('change', function () {
            if (!!$(this).prop('checked')) {
                _view = $(this).prop('value');
            }
            refresh();
        });

        $('input[name=show-password]', el).on('change', function () {
            showPassword($(this).prop('checked'));
        });

        $('.btn-link', el).click(function (e) {
            // external login begins
        });

        $('[name=cmd-enter]', el).click(function (e) {

            e.preventDefault();

            var data = {
                Code: _model.code(),
                EmailAddress: _model.email(),
                Option: _view,
                Password: _model.password(),
                Token: _model.token()
            };

            // validate
            switch (_view) {
                case 'sign-in':
                    break;

                case 'sign-up':
                    break;

                case 'recover-password':
                    break;

                case 'reset-password':
                    break;
            }

            enter(data);
        });

        var refresh = function refresh() {
            $.each(_config.views, function (name) {
                var $view = $('fieldset[name=' + name + ']', el);
                if (name !== _view) {
                    $view.addClass('hidden');
                }
                else {
                    $view.removeClass('hidden');
                    var view = _config.views[_view];
                    $.each(view.hide, function (index, value) {
                        U.find.group($(value, el)).addClass('hidden');
                    });
                    $.each(view.show, function (index, value) {
                        U.find.group($(value, el)).removeClass('hidden');
                    });
                }
            });
        };

        var showPassword = function showPassword(show) {
            var o = _config.options['show-password'];
            if (show) {
                for (var i = 0; i < o.hide.length; i++) {
                    U.find.group($(o.hide[i], el)).addClass('hidden');
                }
                for (var i = 0; i < o.text.length; i++) {
                    $(o.text[i], el).prop('type', 'text');
                }
            } else {
                for (var i = 0; i < o.hide.length; i++) {
                    U.find.group($(o.hide[i], el)).removeClass('hidden');
                }
                for (var i = 0; i < o.text.length; i++) {
                    $(o.text[i], el).prop('type', 'password');
                }
            }
        };

        refresh();

        return _login.promise();
    };
});