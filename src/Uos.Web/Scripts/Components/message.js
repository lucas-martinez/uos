﻿define('uos.message', ['jquery', 'toastr', 'uos.bootstrap'], function ($, toastr, U) {

    U.event.subscribe('error', function (msg) {
        toastr.error(msg);
    });

    U.event.subscribe('info', function (msg) {
        toastr.info(msg);
    });

    U.event.subscribe('warning', function (msg) {
        toastr.warning(msg);
    });

    U.message = function ($el) {

        return {
            error: function (body, title) {
                toastr.error(body);
            },
            info: function (body, title) {
                toastr.info(body);
            },
            warning: function (body, title) {
                toastr.warning(body);
            }
        };
    };

    return U.message;
});