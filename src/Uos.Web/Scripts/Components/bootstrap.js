﻿define('uos.bootstrap', ['jquery'], function ($) {

    var U = window.u;

    if (!U) {
        window.u = U = class {
        };
    }

    /*window.onerror = function (message, file, line, col, error) {
        alert("Error occured: " + error.message);
        return true;
    };
    window.addEventListener("error", function (e) {
        alert("Error occured: " + e.error.message);
        return true;
    })*/

    // ES6 cross-browser detection
    function es6() {
        if (typeof SpecialObject == "undefined") return false;
        try { specialFunction(); }
        catch (e) { return false; }

        return true;
    }

    //
    // preconditions

    var options = requireConfig.websiteOptions;

    if (typeof options !== 'object') {
        console.log('uos.options must be an object. Initializing default.');
        options = {
        };
    }

    var _browser = {
            chrome: /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor),
            msie8: /MSIE 8.0/.test(navigator.userAgent),
            msie9: /MSIE 9.0/.test(navigator.userAgent),
            safari: /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor)
        },
        _params = {},
        _token = options.token,
        _user = options.user,
        _origin = window.location.origin.toLocaleLowerCase(),
        _protocol = typeof options.protocol == 'string' ? options.protocol.toLocaleLowerCase() : 'https:',
        _server = typeof options.server == 'string' ? options.server.toLocaleLowerCase() : window.location.host,
        _baseUrl = _protocol + '//' + _server,
        _spinner;

    _browser.webkit = _browser.chrome || _browser.safari;

    //
    // Load query params

    $.each(decodeURIComponent(window.location.search.substring(1)).split('&'), function (i, item) {
        var arr = item.split('=');
        if (arr[0].length) {
            _params[arr[0].toLowerCase()] = arr.length > 1 ? arr[1] : true;
        }
    });

    U.params = _params;

    //
    // Initialize events

    var _events = {},
        _hOP = _events.hasOwnProperty;

    function publish(event) {
        if (!_hOP.call(_events, event)) {
            _events[event] = {};
            return true;
        }

        if (!_events[event].listeners) {
            return true;
        }

        var args = [].splice.call(arguments, 1);

        var result = [];

        _events[event].listeners.forEach(function (entry) {
            var ret = entry.listener.apply(entry.ctx || U, args);
            result.push(ret);
        });

        return result;
    };

    function subscribe(event, listener, ctx) {

        if (!_hOP.call(_events, event)) {
            _events[event] = {
                dfd: $.Deferred()
            };
        }

        if (!_events[event].listeners) {
            _events[event].listeners = [];
        }

        var index = _events[event].listeners.push({
            ctx: ctx || this,
            listener: listener
        }) - 1;

        return {
            remove: function () {
                delete _events[event].listeners[index];
            }
        };
    };

    //
    // Spinner

    function getSpinner() {

        var dfd = $.Deferred();

        if (!_spinner) {
            _spinner = [];

            _spinner.push(dfd);

            require(['uos.spinner'], function (factory) {

                $(document).ready(function () {

                    var queue = _spinner;

                    _spinner = factory.create(.05);

                    _spinner.opacity(.25);

                    $.each(queue, function (i, dfd) {
                        dfd.resolve(_spinner);
                    })
                });

            });
        }
        else if (typeof _spinner.spin === 'function') {

            dfd.resolve(_spinner);
        } else {

            _spinner.push(dfd)
        }

        return dfd.promise();
    };

    U.spin = function spin() {
        getSpinner().then(function (spinner) {
            spinner.spin();
        }).done();
    };

    U.stop = function stop() {
        getSpinner().then(function (spinner) {
            spinner.stop();
        }).done();
    };

    //
    // Helpers

    function blend($el, skip) {
        var $mask = $('<div class="mask-image"></div>').prependTo($el);
        if ($el.css('border-radio')) $mask.css('border-radio', $el.css('border-radio'));
        for (var count = 0, $ancestor = $el.parent() ; $ancestor.length && $ancestor.get(0) !== document; $ancestor = $ancestor.parent(), count++) {
            if (count < (skip || 0)) continue;
            var img = $ancestor.css('background-image');
            if (img.match('^url')) {
                $mask.css('background-image', img);
                break;
            }
        }

        if (_browser.webkit) {
            $mask.addClass('webkit');
        }
        else if (_browser.mozilla) {
            $mask.addClass('moz');
        }
    };

    var doNothing = function () {
    };

    function findGroup($item) {
        var $parent = $item.parent();
        if ($parent.hasClass('form-group') || $parent.hasClass('input-group') || $parent.hasClass('control-label') || $parent.prop("tagName") == 'LABEL') {
            $item = findGroup($parent);
        }
        return $item;
    };

    $.extend(U, {
        blend: blend,
        doNothing: doNothing,
        publish: publish,
        subscribe: subscribe,
        find: {
            group: findGroup
        }
    });

    //
    // AJAX

    function ajaxError(dfd) {

        return function (xhr, status, error) {
            dfd.reject();
            publish('error', error + ". Status: " + status, "AJAX Error");
        };
    };

    function ajaxSuccess(dfd, silent) {

        return function (response, status, xhr) {

            var firstToLower = function (str) {
                return str.charAt(0).toLowerCase() + str.slice(1);
            };

            // convert to camel case in case serialization failed to.
            var result = (function (o) {
                var r = {};
                $.map(o, function (value, name) {
                    var c = name.charAt(0),
                        lower = c.toLowerCase();
                    if (c !== lower) {
                        name = lower + name.slice(1);
                    }
                    r[name] = value;
                });
                return r;
            })(response);

            var complete = function () {
                if (typeof response === 'object' && response.success === false) {
                    dfd.reject(result);
                }
                else {
                    dfd.resolve(result);
                }
            }

            if (silent) {
                complete()
            }
            else {
                var messages = result.messages || [],
                    success = result.success || true;

                $.each(messages, function (i, msg) {
                    publish(success ? 'info' : 'error', msg);
                });

                setTimeout(complete, 1000 * messages.length);
            }

            return result;
        };
    };

    var jsonGet = function (url) {
        var dfd = $.Deferred();

        $.ajax({
            dataType: 'json',
            error: ajaxError(dfd),
            headers: {
                'Accept': 'application/json; charset=utf-8'
            },
            method: 'GET',
            success: ajaxSuccess(dfd, true),
            url: url
        });

        return dfd.promise();
    };

    var jsonPost = U.post = function (data, url) {
        var dfd = $.Deferred();

        $.ajax({
            contentType: 'json',
            data: JSON.stringify(data),
            dataType: 'json',
            error: ajaxError(dfd),
            headers: {
                'Accept': 'application/json; charset=utf-8',
                'Content-Type': 'application/json; charset=utf-8'
            },
            method: 'POST',
            success: ajaxSuccess(dfd),
            url: url
        });

        return dfd.promise();
    };

    return U;

}, function () {
    // umbiliko failed to load
    console.log('uos .bootstrap failed to load. You should specify some uos.options using RequireJS.');
});