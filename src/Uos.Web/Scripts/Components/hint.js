﻿define('uuos.hint', ['jquery', 'require', 'uos.bootstrap'], function ($, require, U) {

    U.widget = U.widget || {};

    U.widget.hint = function () {

        return {

        };
    };

    require(['angular'], function () {
        U.hint.controller = U.module.controller('HintController', ['$scope', function ($scope) {
            $scope.greeting = 'Hola!';
        }]);
    });

    return U.widget.hint;
});