﻿define('uos.data.store', ['jquery', 'uos.bootstrap'], function ($, U) {

    _collections = {};

    function factory(map) {
        var _map = map;
        return function (value) {
            return ((arguments.length > 0) ? _map[value] : Object.getOwnPropertyNames(_map)) || "";
        }
    };

    class Store {

        constructor() {
        }

        static get(name) {
            var values = _collections[name];

            if (!values) {
                var data = requireConfig.pageOptions[name];

                if (!data) {
                    return {};
                }

                values = factory(data);

                _collections[name] = values;
            }

            values.map = function () {
                return $.map(values(), function (value) {
                    return {
                        text: values(value),
                        value: value
                    };
                });
            };

            return values;
        }

        static kendoAutoComplete($el, col, allowNew) {

            var autoComplete = $el.data('kendoAutoComplete');

            if (typeof autoComplete !== 'object') {
                autoComplete = $el.kendoAutoComplete({
                    dataSource: Store.get(col).map(),
                    dataTextField: 'text',
                    minLength: 0
                }).data('kendoAutoComplete');
            }
            else {
                autoComplete.setDataSource(Store.get(col).map());
            }

            return autoComplete;
        }

        static kendoDropDownList($el, col) {

            var dropDownList = $el.data('kendoDropDownList');

            if (typeof dropDownList !== 'object') {
                dropDownList = $el.kendoDropDownList({
                    dataSource: Store.get(col).map(),
                    dataTextField: 'text',
                    dataValueField: 'value'
                }).data('kendoDropDownList');
            }
            else {
                dropDownList.setDataSource(Store.get(col).map());
            }

            return dropDownList;
        }

        static kendoMultiSelect($el, col, allowNew) {

            var multiSelect = $el.data('kendoMultiSelect');

            if (typeof multiSelect !== 'object') {
                console.log('failed to initialize kendoMultiSelect');
                return;
            }

            if (typeof col === 'string') {
                multiSelect.setDataSource(this.getTextValueArray(col));/*
                    animation: false,
                    dataSource: Store.get(col).map(),
                    dataTextField: "text",
                    dataValueField: "value",
                    template: '<span>#: value #</span><span class="list-desc">#: text #</span>'
                });*/
            }

            if (allowNew === true) {

                var _newItem;
                var _newItemText;
                var newSuffix = " (Add New)";

                // When the value of the widget is changed by the user.
                multiSelect.bind('change', function () {
                    if (_newItemText) {
                        var $select = this.element;
                        var $wrapper = $select.prev('div.k-multiselect-wrap', $select);
                        var $listbox = $('ul[role=listbox]', $wrapper);
                        $('li span:first-child', $listbox).each(function () {
                            var text = $(this).text();
                            if (text === (_newItemText + newSuffix)) {
                                $(this).text(_newItemText);
                            }
                        });
                    }
                    _newItemText = null;
                });

                multiSelect.bind('close', function (e) {
                    if (!!this._prev) {
                        e.preventDefault();
                    }
                });

                multiSelect.bind('filtering', function (e) {
                    var filter = e.filter;

                    if (filter && filter.value) {
                        filter = filter.value;
                    } else {
                        filter = null;

                        //prevent filtering if the filter does not value
                        e.preventDefault();
                    }

                    var newItemText = _newItemText, dataItems = this.dataSource.data(), newItem = true;

                    _newItemText = null;

                    for (var i = 0; i < dataItems.length; i++) {
                        var dataItem = dataItems[i];
                        if (newItemText && dataItem.value == newItemText && dataItem.text === (newItemText + newSuffix)) {
                            this.dataSource.remove(dataItem);
                        } else if (filter && dataItem.value && dataItem.value.toLowerCase() === filter || dataItem.text.toLowerCase() === filter) {
                            newItem = false;
                        }
                    }

                    if (newItem) {
                        _newItemText = filter;
                    }
                });

                multiSelect.bind('select', function (e) {
                    if (_newItemText) {
                        var index = e.item.data().offsetIndex;
                        var $item = $($('li', this.ul)[index]);
                        var text = $item.text();
                        if (text === (_newItemText + newSuffix)) {
                            $item.text(_newItemText);
                        } else {
                            // find and remove
                            var dataItems = this.dataSource.data();
                            for (var i = 0; i < dataItems.length; i++) {
                                var dataItem = dataItems[i];
                                if (dataItem.value === _newItemText) {
                                    this.dataSource.remove(dataItem);
                                }
                            }
                        }
                    }
                });

                // When the widget is bound to data from its data source.
                multiSelect.bind('dataBound', function (e) {
                    if (_newItemText && this._prev && _newItemText !== this._prev) {
                        _newItemText = this._prev;
                        this.dataSource.add({ text: _newItemText + newSuffix, value: _newItemText });
                    } else {
                        e.preventDefault();
                    }
                    if (!this._open) this.open();
                });
            }

            return multiSelect;
        }

        static translate(col, value) {
            var values = Store.get(col);
            return values(value);
        }
    }

    return Store;
});