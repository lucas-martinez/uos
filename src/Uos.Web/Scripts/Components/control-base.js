﻿define('uos.control.base', ['jquery', 'kendo', 'uos.bootstrap', 'uos.component.base'], function ($, kendo, U, Component) {

    class Control extends Component {

        constructor() {
            super('control-panel');

            this.defaults = {};

            this.mapping = {};

            this.browse = new Control.Browse(this);
            this.detail = new Control.Detail(this);
            this.edit = new Control.Edit(this);
            this.entry = new Control.Entry(this);
            this.review = new Control.Review(this);
        }

        init() {

            if (!super.init()) {
                return false;
            }

            if (!this.browse.init()) {
                this.browse = null;
            }

            if (!this.detail.init()) {
                this.detail = null;
            }

            if (!this.edit.init()) {
                this.edit = null;
            }

            if (!this.entry.init()) {
                this.entry = null;
            }

            if (!this.review.init()) {
                this.review = null;
            }

            (function (control) {

                function resize() {
                    control.resize();
                };

                resize();

                $(window).resize(resize);

                window.setInterval(resize, 1000);

            })(this);

            return true;
        }

        inverseMap(name) {
            if (!this.inverseMapping) {
                this.inverseMapping = {};
                for (var key in this.mapping) {
                    if (this.mapping.hasOwnProperty(key) && typeof key === 'string') {
                        var value = this.mapping[key];
                        if (typeof value === 'string') {
                            this.inverseMapping[value] = key;
                        }
                    }
                }
            }
            return this.inverseMapping[name];
        }

        resize() {

            function overflow($el) {
                
                function clientHeight($el) {
                    var height = document.documentElement.clientHeight, padding = 0;
                    for (var $parent = $el.parent(); $parent.length === 1 && $parent[0].tagName != 'BODY'; $parent = $parent.parent()) {
                        if ($parent[0].style.height) {
                            var h = $parent.height(), i = $parent.innerHeight();
                            height = (h > 0 && i > 0) ? Math.min(h, i) : h || i;
                            break;
                        }
                        padding += $parent.outerHeight(true) - $parent.height();
                    }
                    return height - padding;
                }

                var fixedHeight = 0;
                return $el.siblings().not('script').each(function () {
                    if (this.style.position !== 'absolute') {
                        fixedHeight += $(this).outerHeight(true);
                    }
                }).promise().then(function () {
                    var current = $el.height();
                    var padding = $el.outerHeight(true) - current;
                    var target = clientHeight($el) - fixedHeight - padding;
                    if (current != target) {
                        $el.height(target);
                    }
                });
            };

            var control = this;

            return overflow($(this.element)).then(function () {
                if (control.browse) {
                    $('.k-grid', control.browse.element).each(function () {
                        var $grid = $(this);
                        overflow($grid).then(function () {
                            $grid.children('.k-grid-content').each(function () {
                                overflow($(this));
                            });
                        });
                    });
                }
            });
        }
    }

    Control.Browse = class Browse extends Component {

        constructor(control, id) {
            super(id || 'tab-browse');

            this.addedItems = new kendo.data.ObservableArray([]);
            this.control = control;
            this.dataSource = null;
            this.changedItems = new kendo.data.ObservableArray([]);
            this.kendoGrid = null;
            this.element = null;
            this.removedItems = new kendo.data.ObservableArray([]);
            this.selectedItems = new kendo.data.ObservableArray([]);

            this.when('control:review:discard-cmd', this.discard);
            this.when('control:browse:refresh-cmd', this.refresh);
            this.when('control:review:save-cmd', this.save);
        }

        add(model) {

            var dataItem = this.dataSource.add(model);

            row = this.kendoGrid.tbody.find("tr[data-uid='" + dataItem.uid + "']");

            this.selectedDataItems = [dataItem];

            this.kendoGrid.select(row);

            return row;
        }

        discard() {
            $('.k-grid[data-role="grid"]', this.element).map(function () {
                return { id: this.id, dataSource: $(this).data().kendoGrid.dataSource };
            }).each(function () {
                this.dataSource.cancelChanges();
                this.dataSource.sync();
            }).promise.apply(this).done(function () {
                this.fire('control:browse:pristine');
                this.fire('control:browse');
            });
        }

        filter() {
            var value = $('#company-autocomplete').val();

            if (value) {
                value = value.toUpperCase();
            }

            return {
                startsWith: value
            };
        };

        init() {

            this.element = document.getElementById('browse');

            if (!this.element) {
                return false;
            }

            var $grid = $(this.element).children().first();

            this.kendoGrid = $grid.data('kendoGrid');

            this.dataSource = this.kendoGrid.dataSource;

            this.on(this.kendoGrid, "change", this.onSelectionChanged);

            this.on(this.dataSource, "change", this.onDataSourceChanged);

            this.on(this.dataSource, "push", this.onDataSourcePushed);

            this.on(this.dataSource, "sync", this.onDataSourceSynced);

            return true;
        };

        refresh() {
        }

        refreshChanges() {

            var addedItems = [], changedItems = [], removedItems = [];

            $('.k-grid[data-role="grid"]', this.element).map(function () {
                return { id: this.id, dataSource: $(this).data().kendoGrid.dataSource };
            }).each(function () {

                removedItems.push.apply(removedItems, this.dataSource.destroyed());

                for (var data = this.dataSource.data(), idx = 0; idx < data.length; idx++) {

                    var rec = data[idx];

                    if (rec.isNew()) {
                        addedItems.push(rec);
                    }
                    else if (rec.dirty) {
                        changedItems.push(rec);
                    }
                }

            }).promise.call(this).done(function () {

                this.addedItems.length = 0;

                this.addedItems.push.apply(this.addedItems, addedItems);

                this.changedItems.length = 0;

                this.changedItems.push.apply(this.changedItems, changedItems);

                this.removedItems.splice.lenth = 0;

                this.removedItems.push.apply(this.removedItems, removedItems);

                var count = removedItems.length + addedItems.length + changedItems.length;

                if (count > 0) {
                    this.fire('control:browse:dirty', this, count);
                }
                else {
                    this.fire('control:browse:pristine', this, count);
                }
            });
        }

        onDataSourceChanged(e) {
            this.refreshChanges();
            this.fire('control:browse:data-changed');
        }

        onDataSourcePushed(e) {
            this.refreshChanges();
            this.fire('control:browse:data-added');
        }

        onDataSourceSynced(e) {
            this.refreshChanges();
            this.fire('control:browse:data-refreshed');
        }

        onDetailExpanded(e) {
            this.fire('control:browse:detail-expanded');
        }

        onSelectionChanged(e) {
            var kendoGrid = e.sender;
            var select = kendoGrid.select();
            var dataItems = [];
            for (var i = 0; i < select.length; i++) {
                var el = select[i];
                // in case we are using cell selection
                if (el.tagName === 'TD') {
                    el = el.parentNode;
                }
                var dataItem = kendoGrid.dataItem(el);
                if (!dataItem) {
                    console.log('error on getting selected item data');
                }
                else if ($.inArray(dataItem, dataItems) < 0) {
                    dataItems.push(dataItem);
                }
            }
            this.selection = dataItems;

            this.fire('control:browse:selection-changed', this.selection);
        }

        save() {
            (function (component, tables) {
                var count = 0;
                tables.map(function (index, grid) {
                    return { id: grid.id, dataSource: $(grid).data().kendoGrid.dataSource };
                }).each(function (index, kendoGrid) {
                    kendoGrid.dataSource.sync().done(function () {
                        if (--count === 0) {
                            component.fire('control:browse:pristine');
                        }
                    });
                }).promise().done(function () {
                    count++;
                });
            })(this, $('.k-grid[data-role="grid"]', this.element));
        }

        // Updates a single row in a kendo grid without firing a databound event.
        // This is needed since otherwise the entire grid will be redrawn.
        /*update(dataItem) {
            var grid = this.kendoGrid, row = this.kendoGrid.tbody.find("tr[data-uid='" + dataItem.uid + "']");
            
            var rowChildren = $(row).children('td[role="gridcell"]');

            for (var i = 0; i < this.kendoGrid.columns.length; i++) {

                var column = this.kendoGrid.columns[i];
                var template = column.template;
                var cell = rowChildren.eq(i);

                if (template !== undefined) {
                    var kendoTemplate = kendo.template(template);

                    // Render using template
                    cell.html(kendoTemplate(dataItem));
                } else {
                    var fieldValue = dataItem[column.field];

                    var format = column.format;
                    var values = column.values;

                    if (values !== undefined && values != null) {
                        // use the text value mappings (for enums)
                        for (var j = 0; j < values.length; j++) {
                            var value = values[j];
                            if (value.value == fieldValue) {
                                cell.html(value.text);
                                break;
                            }
                        }
                    } else if (format !== undefined) {
                        // use the format
                        cell.html(kendo.format(format, fieldValue));
                    } else {
                        // Just dump the plain old value
                        cell.html(fieldValue);
                    }
                }
            }
        }*/
    };

    Control.Detail = class Detail extends Component {

        constructor(control, id) {
            super(id || 'detail');

            this.control = control;

            this.when('control:browse:selection-changed', function (selection) {
                if (selection && selection.length === 1) {
                    this.reset(selection[0]);
                }
                else {
                    this.reset();
                }
            });

            this.when('control:detail:active', function () {
                if (!!this.original) {
                    this.data = this.original;
                }
            });

            this.when('control:detail:edit-cmd', this.edit);

            this.when('control:detail:retrive-cmd', this.retrive);
        }

        set data(data) {
            console.log('control detail set data not implemented');
        }

        edit() {
            this.fire('control:edit');
        }

        reset(record) {
            this.original = record;
            this.data = null;
        }

        retrive() {
            consol.log('control:detail:retrive not implemented');
        }
    }

    Control.Edit = class Edit extends Component {
        constructor(control, id) {
            super(id || 'edit');

            this.control = control;

            this.when('control:browse:selection-changed', function (selection) {
                if (selection && selection.length === 1) {
                    this.reset(selection[0]);
                }
                else {
                    this.reset();
                }
            });

            this.when('control:edit:revert-cmd', this.revert);
            this.when('control:edit:update-cmd', this.update);
        }

        get data() {
            console.log('control edit get data not implemented');
        }

        set data(data) {
            console.log('control edit set data not implemented');
        }

        init() {

            if (!super.init()) {
                return false;
            }

            this.original = this.data;

            return true;
        }

        reset(record) {
            this.original = record;
            this.data = record;
        }

        revert() {
            this.data = this.original;
        }

        update() {
            var data = this.data;
            for (var name in data) {
                if (name in this.original) {
                    this.original.set(name, data[name]);
                }
            }
            /*if (this.control.browse) {
                this.control.browse.update(this.original);
            }*/
        }
    }

    Control.Entry = class Entry extends Component {
        constructor(control, id) {
            super(id || 'entry');

            this.control = control;

            this.when('control:entry:insert-cmd', this.insert);
            this.when('control:entry:reset-cmd', this.reset);
        }

        get data() {
            console.log('control entry get data not implemented');
        }

        set data(data) {
            console.log('control entry set data not implemented');
        }

        init() {

            if (!super.init()) {
                return false;
            }

            return true;
        }

        insert() {
            consol.log('control:entry:add-cmd not implemented')
        }

        reset() {
            consol.log('control:entry:reset-cmd not implemented')
        }
    }

    Control.Review = class Review extends Component {
        constructor(control, id) {
            super(id || 'review');

            this.control = control;
        }

        init() {

            if (!super.init()) {
                return false;
            }

            return true;
        }
    }

    return Control;
});