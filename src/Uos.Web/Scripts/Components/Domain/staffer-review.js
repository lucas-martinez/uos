﻿define('uos.domain.staffer.review', ['jquery', 'uos.control.base'], function ($, Control) {

    Control.Review = class StafferReview extends Control.Review {

        constructor(control, id) {
            super(control, id);
        }
    }

})