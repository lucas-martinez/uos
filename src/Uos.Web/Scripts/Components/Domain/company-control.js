﻿define('uos.domain.company.control', ['jquery', 'uos.control.base', 'uos.domain.company.browse', 'uos.domain.company.detail', 'uos.domain.company.edit', 'uos.domain.company.entry', 'uos.domain.company.review'], function ($, Control) {

    class CompanyControl extends Control {

        constructor(id) {
            super(id);
        }

        init() {
            if (!super.init()) {
                return false;
            }

            return true;
        }
    }

    return CompanyControl;

});