﻿define('uos.domain.identity.browse', ['jquery', 'uos.control.base', 'uos.data.store'], function ($, Control, Store) {

    Control.Browse = class IdentityBrowse extends Control.Browse {

        constructor(control, id) {
            super(control, id);
        }

        init() {

            if (!super.init()) {
                return false;
            }

            return true;
        }

        onManageClicked(e) {
        }
    }

})