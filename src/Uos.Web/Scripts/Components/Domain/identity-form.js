﻿define('uos.domain.identity.form', ['jquery', 'kendo', 'uos.form.base', 'uos.data.store'], function ($, kendo, Form, Store) {

    class IdentityForm extends Form {

        constructor(control, id) {
            super(control, id);


        }

        get data() {

            var data = super.data;

            if (!data.Remarks) {
                data.Remarks = $(document.getElementById(this.id + '-remarks')).html();
            }

            return data;
        }

        set data(data) {

            super.data = data;
        }

        get pristine() {
            return this.changes.length === 0;
        }

        get valid() {

            var valid = true;

            if (!this.data.EmailAddress) {
                valid = false;
            }

            return valid;
        }

        init() {

            if (!super.init()) {
                return false;
            }

            //this.original = this.data;

            //this.validator = $(this.element).kendoValidator().data("kendoValidator");

            kendo.bind(this.element, this.model);

            Store.kendoDropDownList($('#' + this.id + '-language'), 'Languages');
            Store.kendoAutoComplete($('#' + this.id + '-country'), 'Countries');

            return true;
        }

        onCodeSelect(item) {
            var kendoAutoComplete = this;
            if (item) {
                toastr.warning("You can't create a new identity with a identity code that already exists!");
            }
        };

        reset(record) {
            if (typeof record === 'object') {
                this.original = record;
                this.data = record;
            } else {
                this.data = {
                    Country: this.model.data.country,
                    Currency: this.model.data.currency
                };
            }
        }

        validate(show) {

            var valid = true;

            if (!this.data.EmailAddress) {
                valid = false;
            }

            return valid;
        }
    }

    return IdentityForm;
})