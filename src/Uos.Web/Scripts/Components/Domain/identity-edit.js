﻿define('uos.domain.identity.edit', ['jquery', 'uos.control.base', 'uos.domain.identity.form', 'uos.domain.identity.roles', 'kendo.data.contenteditable'], function ($, Control, IdentityForm, IdentityFeatures) {

    var token = requireConfig.websiteOptions.token;
    var saveUrl = requireConfig.pageOptions.saveUrl;

    Control.Edit = class IdentityEdit extends Control.Edit {

        constructor(control, id) {
            super(control, id);

            this.form = new IdentityForm(control, 'identity-edit-form');

            this.roles = new IdentityFeatures('identity-edit-roles');
        }

        get data() {
            var data = this.form.data;
            data.Roles = this.roles.data;
            return data;
        }

        set data(data) {
            this.form.reset(data);
            this.roles.data = data ? (data.Roles || []) : [];
        }

        init() {

            if (!super.init()) {
                return false;
            }

            this.form.init();

            this.roles.init();

            this.fire('control:entry:pristine');

            this.on(this.form.model.data, 'change', this.refresh);

            this.on(this.roles.model, 'change', this.refresh);

            return true;
        }

        refresh() {
            var current = this.form.data;

            if (this.form.pristine && this.roles.pristine) {
                this.fire('control:edit:pristine');
            } else {
                this.fire('control:edit:dirty');

                this.validate();
            }
        }

        revert() {
            this.form.reset();
            this.roles.reset();
        }

        update() {
            super.update();

            this.original.set('Features', this.roles.data);

            this.fire('control:edit:pristine');

            this.fire('control:detail');

            //this.refresh();
        }

        validate() {
            var current = this.form.data;

            if (!current.EmailAddress) {
                this.fire('control:edit:invalid');
            }
            else if (this.form.validate()) {
                this.fire('control:edit:valid');
            }
            else {
                this.fire('control:edit:invalid');
            }
        }
    }
});