﻿define('uos.domain.staffer.detail', ['jquery', 'uos.control.base', 'uos.data.store', 'kendo.data.contenteditable'], function ($, Control, Store) {

    Control.Detail = class StafferDetail extends Control.Detail {

        constructor(control, id) {
            super(control, id);
        }

        set data(data) {

            if (!data || typeof data !== 'object') {
                kendo.bind(this.element, {});
                return;
            }

            var model = {
                companyCode: data.CompanyCode || null,
                companyName: data.CompanyName || null,
                since: data.CreatedOn || null,
                firstName: data.FirstName || null,
                fullName: data.FullName || null,
                identity: data.Identity || null,
                lastName: data.LastName || null,
                remarks: data.Remarks || null,
                roles: data.Roles || [],
            };

            if (typeof data.isNew === 'function' && data.isNew()) {
                model.label = "NEW";
            }
            else if (data.dirty === true) {
                model.label = "modified";
            }

            if (!!data.Roles) {
                var roles = [];
                var collections = ['ManagementRoles', 'FinancialRoles', 'LogisticsRoles', 'PersonnelRoles']
                for (var i = 0; i < data.Roles.length; i++) {
                    var value = data.Roles[i], text = null;
                    for (var j = 0; j < collections.length; j++) {
                        text = Store.translate(collections[j], value);
                        if (!!text) {
                            break;
                        }
                    }
                    roles.push(text || value);
                }
                roles.sort();
                model.roles = roles.join(', ');
            }

            kendo.bind(this.element, model);
        }
    }
})