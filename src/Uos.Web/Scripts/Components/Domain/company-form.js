﻿define('uos.domain.company.form', ['jquery', 'kendo', 'uos.component.base', 'uos.data.store'], function ($, kendo, Component, Store) {

    class CompanyForm extends Component {

        constructor(id) {
            super(id);

            var self = this;

            this.model = {
                data: kendo.observable({
                    code: "",
                    country: "",
                    currency: "",
                    industry: "",
                    metric: "",
                    name: "",
                    remarks: "",
                    size: "",
                    type: ""
                }),
                messages: {
                    code: "this field is requierd"
                },
                status: {
                    code: {
                        error: function () {
                            return !this.pristine;
                        },
                        pristine: (function (component) {
                            return function () {
                                return component.changes().length === 0;
                            };
                        })(this),
                        success: function () {
                            return !this.pristine;
                        },
                        warning: function () {
                            return !this.pristine;
                        }
                    }
                },
                texts: {

                }
            };

            this.filter = {
                code: function () {
                    var el = document.getElementById(id + '-code');
                    return {
                        startsWith: $(el).val()
                    }
                },
                name: function () {
                    var el = document.getElementById(id + '-name');
                    return {
                        startsWith: $(el).val()
                    }
                },
                roles: function () {
                    var el = document.getElementById(id + '-roles');
                    var kendoMultiSelect = $(el).data('kendoMultiSelect');
                    return {
                        startsWith: kendoMultiSelect != null ? kendoMultiSelect.input.val() : null,
                        exclude: kendoMultiSelect != null ? kendoMultiSelect.value() : null
                    };
                },
                users: function () {
                    var el = document.getElementById(id + '-users');
                    var kendoMultiSelect = $(el).data('kendoMultiSelect');
                    return {
                        startsWith: kendoMultiSelect != null ? kendoMultiSelect.input.val() : null,
                        exclude: kendoMultiSelect != null ? kendoMultiSelect.value() : null
                    };
                }
            };

            this.properties = ['CompanyCode', 'CompanyName', 'CompanySize', 'CompanyType', 'Country', 'Currency', 'Industry', 'MetricSystem', 'Remarks'];

            this.rules = {
                codeUnique: function () {
                    return true;
                }
            };
        }

        get data() {

            var data = this.model.data;

            data = {
                CompanyCode: data.get('code') || null,
                CompanyName: data.get('name') || null,
                CompanySize: data.get('size') || null,
                CompanyType: data.get('type') || null,
                Country: data.get('country') || null,
                Currency: data.get('currency') || null,
                Industry: data.get('industry') || null,
                MetricSystem: data.get('metric') || null,
                Remarks: data.get('remarks') || null
            };

            if (data.CompanySize && typeof data.CompanySize === 'object') {
                data.CompanySize = data.CompanySize.value;
            }

            if (data.CompanyType && typeof data.CompanyType === 'object') {
                data.CompanyType = data.CompanyType.value;
            }

            if (data.Industry && typeof data.Industry === 'object') {
                data.Industry = data.Industry.value;
            }

            if (!data.Remarks) {
                data.Remarks = $(document.getElementById(this.id + '-remarks')).html();
            }

            return data;
        }

        set data(data) {

            if (typeof data !== 'object') {
                data = {};
            }

            $(document.getElementById(this.id + '-remarks')).html(data.Remarks || "");

            this.model.data.set('code', data.CompanyCode || this.original.CompanyCode);
            this.model.data.set('name', data.CompanyName || this.original.CompanyName);
            this.model.data.set('size', data.CompanySize || this.original.CompanySize);
            this.model.data.set('type', data.CompanyType || this.original.CompanyType);
            this.model.data.set('industry', data.Industry || this.original.Industry);
            this.model.data.set('country', data.Country || this.original.Country);
            this.model.data.set('currency', data.Currency || this.original.Currency);
            this.model.data.set('metric', data.MetricSystem || this.original.MetricSystem);
            this.model.data.set('remarks', data.Remarks || this.original.Remarks);
        }

        get pristine() {
            return this.changes(this.data).length === 0;
        }

        get valid() {
            return false;
        }

        changes(current) {
            if (!current) {
                current = this.data;
            }

            var diff = [], original = this.original;

            for (var k in original) {
                if (this.properties.indexOf(k) === -1) {
                }
                else if (!(k in current)) {
                    // property gone
                    diff.push(k)
                }
                else if (!original[k] && !current[k]) {
                    // reset to default
                    current[k] = original[k];
                }
                else if (!!original[k] && !current[k]) {
                    // reset to default
                    current[k] = original[k];
                }
                else if (!original[k] && !!current[k]) {
                    // property has changed
                    diff.push(k);
                }
                else if (original[k].toString().trim() != current[k].toString().trim()) {
                    // property has changed
                    diff.push(k);
                }
            }

            for (k in current) {
                if (this.properties.indexOf(k) !== -1 && !(k in original) && !!k && current[k].toString().trim() != 0) {
                    // property is new
                    diff.push(k)
                }
            }

            return diff;
        }

        init() {

            if (!super.init()) {
                return false;
            }

            this.original = this.data;

            //this.validator = $(this.element).kendoValidator().data("kendoValidator");

            kendo.bind(this.element, this.model);

            Store.kendoDropDownList($('#' + this.id + '-size'), 'CompanySizes');
            Store.kendoDropDownList($('#' + this.id + '-type'), 'CompanyTypes');
            Store.kendoAutoComplete($('#' + this.id + '-country'), 'Countries');
            Store.kendoAutoComplete($('#' + this.id + '-currency'), 'Currencies');
            Store.kendoDropDownList($('#' + this.id + '-industry'), 'Industries');
            Store.kendoDropDownList($('#' + this.id + '-metric'), 'MetricSystems');

            return true;
        }

        onCodeSelect(item) {
            var kendoAutoComplete = this;
            if (item) {
                toastr.warning("You can't create a new company with a company code that already exists!");
            }
        };

        reset(record) {
            if (typeof record === 'object') {
                this.original = record;
                this.data = record;
            } else {
                this.data = {
                    Country: this.model.data.country,
                    Currency: this.model.data.currency
                };
            }
        }

        validate(show) {

            var valid = true;

            if (!this.data.CompanyCode) {
                valid = false;
            }

            if (!this.data.CompanyName) {
                valid = false;
            }

            return valid;
        }
    }

    return CompanyForm;
})