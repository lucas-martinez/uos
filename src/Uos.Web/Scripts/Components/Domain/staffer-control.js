﻿define('uos.domain.staffer.control', ['jquery', 'uos.control.base', 'uos.domain.staffer.browse', 'uos.domain.staffer.detail', 'uos.domain.staffer.edit', 'uos.domain.staffer.entry', 'uos.domain.staffer.review'], function ($, Control) {

    class StafferControl extends Control {

        constructor(id) {
            super(id);

            this.defaults = {
                CompanyCode: null,
                CompanyName: null,
                CreatedOn: null,
                FirstName: null,
                FullName: null,
                LastName: null,
                Identity: null
            };

            this.mapping = {
                companyCode: "CompanyCode",
                companyName: "CompanyName",
                firstName: "FirstName",
                fullName: "FullName",
                identity: "EmailAddress",
                lastName: "LastName",
                roles: "Roles",
                since: "CreatedOn"
            };
        }

        init() {
            if (!super.init()) {
                return false;
            }

            return true;
        }
    }

    return StafferControl;

});