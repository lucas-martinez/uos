﻿define('uos.domain.company.review', ['jquery', 'uos.control.base'], function ($, Control) {

    Control.Review = class CompanyReview extends Control.Review {

        constructor(control, id) {
            super(control, id);
        }
    }

})