﻿define('uos.domain.company.browse', ['jquery', 'uos.control.base', 'uos.data.store'], function ($, Control, Store) {

    Control.Browse = class CompanyBrowse extends Control.Browse {

        constructor(control, id) {
            super(control, id);
        }

        init() {

            if (!super.init()) {
                return false;
            }

            return true;
        }

        onManageClicked(e) {
        }
    }

})