﻿define('uos.domain.company.edit', ['jquery', 'uos.control.base', 'uos.domain.company.form', 'uos.domain.company.features', 'kendo.data.contenteditable'], function ($, Control, CompanyForm, CompanyFeatures) {

    var token = requireConfig.websiteOptions.token;
    var saveUrl = requireConfig.pageOptions.saveUrl;

    Control.Edit = class CompanyEdit extends Control.Edit {

        constructor(control, id) {
            super(control, id);

            this.form = new CompanyForm('company-edit-form');

            this.features = new CompanyFeatures('company-edit-features');
        }

        get data() {
            var data = this.form.data;
            data.Features = this.features.data;
            return data;
        }

        set data(data) {
            this.form.reset(data);
            this.features.data = data ? (data.Features || []) : [];
        }

        init() {

            if (!super.init()) {
                return false;
            }

            this.form.init();

            this.features.init();

            this.fire('control:entry:pristine');

            this.on(this.form.model.data, 'change', this.refresh);

            this.on(this.features.model, 'change', this.refresh);

            return true;
        }

        refresh() {
            var current = this.form.data, changes = this.form.changes(current);

            if (changes.length === 0 && this.features.pristine) {
                this.fire('control:edit:pristine');
            } else if (!current.CompanyCode || !current.CompanyName) {
                this.fire('control:edit:invalid');
            }
            else if (this.form.validate()) {
                this.fire('control:edit:valid');
            }
            else {
                this.fire('control:edit:invalid');
            }
        }

        revert() {
            this.form.reset();
            this.features.reset();
        }

        update() {
            super.update();

            this.original.set('Features', this.features.data);

            this.fire('control:edit:pristine');

            this.fire('control:detail');

            //this.refresh();
        }
    }
});