﻿define('uos.domain.identity.roles', ['jquery', 'kendo', 'uos.component.base'], function ($, kendo, Component) {

    var _separator = '-';

    function init(checkboxes, model, totals) {
        checkboxes.each(function (index, el) {
            var name = el.value, value = el.checked, keys = name.split(_separator), key = keys[0];

            model.values[name] = value;

            if (keys.length > 1) {
                totals[key] = (totals[key] || 0) + 1;

                if (value && model.counts[key] !== -1) {
                    model.counts[key] = (model.counts[key] || 0) + 1;
                }
            }
            else if (this.checked) {
                model.counts[key] = -1;
            }
        }).promise().done(function () {

            for (var key in model.counts) {
                if (model.counts[key] === -1) {
                    model.counts[key] = totals[key];
                    for (var name in model.values) {
                        var keys = name.split('-');
                        if (keys.length > 1 && keys[0] === key) {
                            model.values[name] = true;
                        }
                    }
                }
            }

            model.trigger("change", { field: "counts" });
            model.trigger("change", { field: "values" });
            model.trigger("change", { field: "value" });
        });
    };

    class IdentityFeatures extends Component {
        constructor(id) {
            super(id);

            this.change = {}; // used to avoid double change fired from input

            this.model = kendo.observable({

                counts: {
                    financial: function () {
                        return (this.FI || 0) + (this.CO || 0) + (this.EC || 0) + (this.IM || 0) + (this.TR || 0);
                    },
                    logistics: function () {
                        return (this.LO || 0) + (this.SD || 0) + (this.MM || 0) + (this.PM || 0) + (this.PP || 0) + (this.PS || 0) + (this.QM || 0) + (this.TM || 0);
                    },
                    personnel: function () {
                        return (this.PA || 0) + (this.PD || 0);
                    }
                },
                value: (function (component) {
                    return function () {
                        return component.data.join(' ');
                    };
                })(this),
                values: {}
            });

            this.original = [];

            this.totals = {};
        }

        get data() {
            var values = this.values, result = [];

            for (var i = 0; i < values.length; i++) {
                var name = values[i], keys = name.split(_separator), key = keys[0];

                if (key === name || values.indexOf(key) === -1) {
                    result.push(name);
                }
            }

            return result;
        }

        set data(roles) {

            // clean 
            for (var name in this.model.values) {
                if (name.toLocaleUpperCase() === name && this.model.values[name] === true) {
                    this.model.values[name] = false;
                }
            }

            if (typeof roles !== 'object' || !roles.length) {
                roles = [];
            }
            
            for (var i = 0; i < roles.length; i++) {
                this.model.values[roles[i]] = true;
            }

            for (var i = 0; i < roles.length; i++) {
                this.model.values[roles[i]] = true;
            }

            for (var name in this.model.values) {
                if (name.toLocaleUpperCase() === name) {
                    var keys = name.split(_separator), key = keys[0];
                    if (this.model.values[key] === true) {
                        this.model.values[name] = true;
                    }
                }
            }

            this.model.trigger("change", { field: "values" });
            this.model.trigger("change", { field: "value" });

            for (var name in this.totals) {
                if (name.toLocaleUpperCase() === name) {
                    if (this.model.values[name] === true) {
                        this.model.counts[name] = this.totals[name];
                    }
                    else {
                        this.model.counts[name] = this.count(name);
                    }
                }
            }

            this.model.trigger("change", { field: "counts" });

            this.original = this.data;
        }

        get pristine() {
            var current = this.data.sort().join(' '), original = !this.original ? "" : this.original.sort().join(' ');

            return current == original;
        }

        get values() {
            var values = [];

            for (var name in this.model.values) {
                if (name.toLocaleUpperCase() === name && this.model.values[name] === true) {
                    values.push(name);
                }
            }

            values.sort();

            return values;
        }

        clear() {
            this.data = [];
        }

        count(key) {
            var result = 0;
            for (var name in this.model.values) {
                var keys = name.split(_separator);
                if (keys.length > 1 && keys[0] === key && this.model.values[name]) {
                    result++;
                }
            }
            return result;
        }

        init() {

            if (!super.init()) {
                return false;
            }

            kendo.bind(this.element, this.model);

            this.on(this.model, 'change', this.onChanged);

            $('.tab-content', this.element).tab();

            $('.tree-toggle', this.element).click(function (e) {

                // avoid checking the box
                e.preventDefault();

                var $tree = $(this).parent().parent('li').children('ul.tree');

                var hidden = $tree.hasClass('hidden');

                if (hidden) {
                    $tree.removeClass('hidden');
                    e.target.innerText = "[-]";
                }
                else {
                    $tree.addClass('hidden');
                    e.target.innerText = "[+]";
                }
            });

            $(function () {
                $('ul.tree', this.element).addClass('hidden');
            });

            init($('input[type=checkbox]', this.element), this.model, this.totals);
        }

        onChanged(e) {

            if (!e || !e.field) {
                return;
            }

            var model = this.model;//e.sender.parents && e.sender.parents.length ? e.sender.parents[0] : e.sender;

            if (!model.values) {
                return;
            }

            var value = e.field.match(/values\['(.*)'\]/);

            if (value && value.length === 2) {

                value = value[1];
                var checked = model.values[value];

                // avoid double entry of the same event
                if (this.change.name === value && this.change.value === checked) {
                    return;
                }

                this.change = { name: value, value: checked };

                var keys = value.split(_separator), key = keys[0];

                if (keys.length > 1) {

                    var count = this.count(key);

                    model.counts[key] = count;

                    model.values[key] = (count === this.totals[key]);
                }
                else {
                    model.counts[key] = checked ? this.totals[key] : 0;

                    for (var name in model.values) {
                        var value = model.values[name], keys = name.split('-');
                        if (keys.length > 1 && keys[0] === key) {
                            model.values[name] = checked;
                        }
                    }
                }

                model.trigger("change", { field: "counts" });
                model.trigger("change", { field: "values" });
                model.trigger("change", { field: "value" });
            }
        }

        reset() {
            this.data = this.original;
        }
    }

    return IdentityFeatures;
})