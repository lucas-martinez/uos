﻿define('uos.domain.company.entry', ['jquery', 'uos.control.base', 'uos.domain.company.form', 'uos.domain.company.features', 'uos.data.store', 'kendo.data.contenteditable'], function ($, Control, CompanyForm, CompanyFeatures, Store) {

    class CompanyEntryForm extends CompanyForm {
        constructor(id) {
            super(id);

            this.model.data.set('users', []);
            this.model.data.set('roles', []);
        }

        get data() {
            var data = super.data;
            data.Roles = this.model.data.get('roles');
            data.Users = this.model.data.get('users');
            return data;
        }

        set data(data) {
            super.data = data;
            this.model.data.set('roles', data.Roles || this.original.Roles);
            this.model.data.set('users', data.Users || this.original.Users);
        }

        init() {
            if (!super.init()) {
                return false;
            }

            Store.kendoMultiSelect($('#' + this.id + '-roles'), null, true);

            return true;
        }

        reset() {
            this.data = {
                Country: this.model.data.country,
                Currency: this.model.data.currency,
                Roles: this.model.data.roles,
                Users: this.model.data.users
            };

            super.reset();
        }
    }

    Control.Entry = class CompanyEntry extends Control.Entry {

        constructor(control, id) {
            super(control, id);

            this.form = new CompanyEntryForm('company-entry-form');

            this.features = new CompanyFeatures('company-entry-features');
        }

        get data() {
            var data = this.form.data;
            data.Features = this.features.data;
            return data;
        }

        set data(data) {
            this.form.data = data;
            this.features.data = !!data ? data.Features || [] : [];
        }

        get pristine() {
            if (!super.pristine) {
                return false;
            }

            return true;
        }

        get valied() {
            if (!super.valid) {
                return false;
            }

            return true;
        }

        init() {

            if (!super.init()) {
                return false;
            }

            this.form.init();

            this.features.init();

            this.fire('control:entry:pristine');

            this.on(this.form.model.data, 'change', this.refresh);

            this.on(this.features.model, 'change', this.refresh);

            return true;
        }

        insert() {
            if (!this.form.validate()) {
                console.log('validations errors. Should reflect errors.');
                return;
            }
            if (!this.control.review) {
                post().done(function () {
                    this.reset();
                    this.fire('control:entry:pristine');
                });
                return;
            }

            var dataItem = this.control.browse.dataSource.add(this.data);

            var row = this.control.browse.kendoGrid.tbody.find("tr[data-uid='" + dataItem.uid + "']");

            // this should fire the event so it can be previewed
            this.control.browse.kendoGrid.select(row);

            this.reset();

            this.fire('control:entry:pristine');
        }

        refresh() {
            var current = this.form.data, changes = this.form.changes(current);

            if (changes.length === 0 && this.features.data.length === 0) {
                this.fire('control:entry:pristine');
            } else {
                this.fire('control:entry:dirty');

                if (!current.CompanyCode || !current.CompanyName) {
                    this.fire('control:entry:invalid');
                }
                else if (this.form.validate()) {
                    this.fire('control:entry:valid');
                }
                else {
                    this.fire('control:entry:invalid');
                }
            }
        }

        post() {
            this.fire('control:busy');

            // TODO: 

            $.post('/domain/company', this.data).then(function (response) {

                if (this.control.browse) {

                    var dataItem = this.control.browse.dataSource.add(response.value);

                    var row = this.control.browse.kendoGrid.tbody.find("tr[data-uid='" + dataItem.uid + "']");

                    // this should fire the event so it can be previewed
                    this.control.browse.kendoGrid.select(row);
                }
            }).done(function () {
                this.fire('control:ready');
            });
        }

        reset() {
            this.form.reset();
            this.features.reset();
            this.refresh();
        }
    }

})