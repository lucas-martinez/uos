﻿define('uos.domain.identity.control', ['jquery', 'uos.control.base', 'uos.domain.identity.browse', 'uos.domain.identity.detail', 'uos.domain.identity.edit', 'uos.domain.identity.entry', 'uos.domain.identity.review'], function ($, Control) {

    class IdentityControl extends Control {

        constructor(id) {
            super(id);

            this.defaults = {
                address: null,
                BirthDate: null,
                Country: null,
                EmailAddress: null,
                Language: null,
                Locality: null,
                FullName: null,
                PhoneNumber: null,
                PostalCode: null,
                Remarks: null
            };

            this.mapping = {
                address: "Address",
                birth: "BirthDate",
                country: "Country",
                email: "EmailAddress",
                language: "Language",
                locality: "Locality",
                name: "FullName",
                phone: "PhoneNumber",
                postal: "PostalCode",
                remarks: "Remarks"
            };
        }

        init() {
            if (!super.init()) {
                return false;
            }

            return true;
        }
    }

    return IdentityControl;

});