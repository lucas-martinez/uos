﻿define('uos.domain.identity.detail', ['jquery', 'uos.control.base', 'uos.data.store', 'kendo.data.contenteditable'], function ($, Control, Store) {

    Control.Detail = class IdentityDetail extends Control.Detail {

        constructor(control, id) {
            super(control, id);
        }

        set data(data) {

            if (!data || typeof data !== 'object') {
                kendo.bind(this.element, {});
                return;
            }

            var model = {
                address: data.Address || null,
                birth: data.BirthDate || null,
                country: data.Country || null,
                since: data.CreatedOn || null,
                email: data.EmailAddresss || null,
                emailOk: data.EmailConfirmed || null,
                name: data.FullName || null,
                language: data.Language || null,
                locality: data.Locality || null,
                password: data.PasswordStrength || null,
                phone: data.PhoneNumber || null,
                phoneOk: data.PhoneConfirmed || null,
                region: data.Region || null,
                twoFactor: data.TwoFactorEnabled || null
            };

            if (typeof data.isNew === 'function' && data.isNew()) {
                model.label = "NEW";
            }
            else if (data.dirty === true) {
                model.label = "modified";
            }

            if (typeof data.Country === 'string') {
                model.country = Store.translate('Countries', data.Country);
            }

            if (typeof data.EmailConfirmed === 'boolean') {
                model.emailOk = Store.translate('Primitives', data.EmailConfirmed);
            }

            if (typeof data.Language === 'string') {
                model.language = Store.translate('Languages', data.Language);
            }

            if (typeof data.EmailConfirmed === 'boolean') {
                model.emailOk = Store.translate('Primitives', data.EmailConfirmed);
            }

            if (typeof data.PasswordStrength === 'string') {
                model.password = Store.translate('Primitives', data.PasswordStrength);
            }

            if (typeof data.TwoFactorEnabled === 'boolean') {
                model.twoFactor = Store.translate('Primitives', data.TwoFactorEnabled);
            }

            if (!!data.Roles) {
                var roles = [];
                var collections = ['ApplicationRoles']
                for (var i = 0; i < data.Roles.length; i++) {
                    var value = data.Roles[i], text = null;
                    for (var j = 0; j < collections.length; j++) {
                        text = Store.translate(collections[j], value);
                        if (!!text) {
                            break;
                        }
                    }
                    roles.push(text || value);
                }
                roles.sort();
                model.roles = roles.join(', ');
            }

            kendo.bind(this.element, model);
        }
    }
})