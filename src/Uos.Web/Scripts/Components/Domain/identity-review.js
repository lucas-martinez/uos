﻿define('uos.domain.identity.review', ['jquery', 'uos.control.base'], function ($, Control) {

    Control.Review = class IdentityReview extends Control.Review {

        constructor(control, id) {
            super(control, id);
        }
    }

})