﻿define('uos.domain.staffer.form', ['jquery', 'kendo', 'uos.form.base', 'uos.data.store'], function ($, kendo, Form, Store) {

    class StafferForm extends Form {

        constructor(control, id) {
            super(control, id);
        }

        get data() {

            var data = super.data;

            if (!data.Remarks) {
                data.Remarks = $(document.getElementById(this.id + '-remarks')).html();
            }

            return data;
        }

        set data(data) {
            super.data = data;
        }

        get valid() {
            return false;
        }

        init() {

            if (!super.init()) {
                return false;
            }

            return true;
        }

        validate(show) {

            var valid = true;

            var data = this.data;

            if (!data.CompanyCode) {
                valid = false;
            }

            if (!data.EmailAddress) {
                valid = false;
            }

            return valid;
        }
    }

    return StafferForm;
})