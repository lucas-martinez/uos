﻿define('uos.domain.company.detail', ['jquery', 'uos.control.base', 'uos.data.store', 'kendo.data.contenteditable'], function ($, Control, Store) {

    Control.Detail = class CompanyDetail extends Control.Detail {

        constructor(control, id) {
            super(control, id);
        }

        set data(data) {

            if (!data || typeof data !== 'object') {
                kendo.bind(this.element, {});
                return;
            }

            var model = {
                code: data.CompanyCode || null,
                name: data.CompanyName || null
            };

            if (typeof data.isNew === 'function' && data.isNew()) {
                model.label = "NEW";
            }
            else if (data.dirty === true) {
                model.label = "modified";
            }

            if (typeof data.CompanySize === 'string') {
                model.size = Store.translate('CompanySizes', data.CompanySize);
            }

            if (typeof data.CompanyType === 'string') {
                model.type = Store.translate('CompanyTypes', data.CompanyType);
            }

            if (typeof data.Industry === 'string') {
                model.industry = Store.translate('Industries', data.Industry);
            }

            if (typeof data.Country === 'string') {
                model.country = Store.translate('Countries', data.Country);
            }

            if (typeof data.Currency === 'string') {
                model.currency = Store.translate('Currencies', data.Currency);
            }

            if (typeof data.MetricSystem === 'string') {
                model.metric = Store.translate('MetricSystems', data.MetricSystem);
            }

            if (!!data.Features) {
                var features = [];
                var collections = ['FinancialFeatures', 'LogisticsFeatures', 'PersonnelFeatures']
                for (var i = 0; i < data.Features.length; i++) {
                    var value = data.Features[i], text = null;
                    for (var j = 0; j < collections.length; j++) {
                        text = Store.translate(collections[j], value);
                        if (!!text) {
                            break;
                        }
                    }
                    features.push(text || value);
                }
                features.sort();
                model.features = features.join(', ');
            }

            kendo.bind(this.element, model);
        }
    }
})