﻿define('uos.domain.staffer.entry', ['jquery', 'uos.control.base', 'uos.domain.staffer.form', 'uos.domain.staffer.roles', 'uos.data.store', 'kendo.data.contenteditable'], function ($, Control, StafferForm, StafferFeatures, Store) {

    class StafferEntryForm extends StafferForm {
        constructor(control, id) {
            super(control, id);
        }

        get data() {
            var data = super.data;
            return data;
        }

        set data(data) {
            super.data = data;
        }

        init() {
            if (!super.init()) {
                return false;
            }

            return true;
        }

        reset() {
            this.data = {};

            super.reset();
        }
    }

    Control.Entry = class StafferEntry extends Control.Entry {

        constructor(control, id) {
            super(control, id);

            this.form = new StafferEntryForm(control, 'staffer-entry-form');

            this.roles = new StafferFeatures('staffer-entry-roles');
        }

        get data() {
            var data = this.form.data;
            data.Features = this.roles.data;
            return data;
        }

        set data(data) {
            this.form.data = data;
            this.roles.data = !!data ? data.Features || [] : [];
        }

        get pristine() {
            if (!super.pristine) {
                return false;
            }

            return true;
        }

        get valied() {
            if (!super.valid) {
                return false;
            }

            return true;
        }

        init() {

            if (!super.init()) {
                return false;
            }

            this.form.init();

            this.roles.init();

            this.fire('control:entry:pristine');

            this.on(this.form.model.data, 'change', this.refresh);

            this.on(this.roles.model, 'change', this.refresh);

            return true;
        }

        insert() {
            if (!this.form.validate()) {
                console.log('validations errors. Should reflect errors.');
                return;
            }
            if (!this.control.review) {
                post().done(function () {
                    this.reset();
                    this.fire('control:entry:pristine');
                });
                return;
            }

            var dataItem = this.control.browse.dataSource.add(this.data);

            var row = this.control.browse.kendoGrid.tbody.find("tr[data-uid='" + dataItem.uid + "']");

            // this should fire the event so it can be previewed
            this.control.browse.kendoGrid.select(row);

            this.reset();

            this.fire('control:entry:pristine');
        }

        refresh() {
            if (this.form.changes.length === 0 && this.roles.data.length === 0) {
                this.fire('control:entry:pristine');
            } else {
                this.fire('control:entry:dirty');

                this.validate()
            }
        }

        post() {
            this.fire('control:busy');

            // TODO: 

            $.post('/domain/staffer', this.data).then(function (response) {

                if (this.control.browse) {

                    var dataItem = this.control.browse.dataSource.add(response.value);

                    var row = this.control.browse.kendoGrid.tbody.find("tr[data-uid='" + dataItem.uid + "']");

                    // this should fire the event so it can be previewed
                    this.control.browse.kendoGrid.select(row);
                }
            }).done(function () {
                this.fire('control:ready');
            });
        }

        reset() {
            this.form.reset();
            this.roles.reset();
            this.refresh();
        }

        validate() {
            var data = this.form.data;

            if (!data.CompanyCode || !data.EmailAddress) {
                this.fire('control:entry:invalid');
            }
            else if (this.form.validate()) {
                this.fire('control:entry:valid');
            }
            else {
                this.fire('control:entry:invalid');
            }
        }
    }

})