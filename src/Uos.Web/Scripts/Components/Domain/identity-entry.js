﻿define('uos.domain.identity.entry', ['jquery', 'uos.control.base', 'uos.domain.identity.form', 'uos.domain.identity.roles', 'uos.data.store', 'kendo.data.contenteditable'], function ($, Control, IdentityForm, IdentityFeatures, Store) {

    class IdentityEntryForm extends IdentityForm {
        constructor(control, id) {
            super(control, id);
        }

        init() {
            if (!super.init()) {
                return false;
            }

            Store.kendoMultiSelect($('#' + this.id + '-roles'), null, true);

            return true;
        }

        reset() {
            this.data = {
                Country: this.model.data.country,
                Language: this.model.data.language,
            };

            super.reset();
        }
    }

    Control.Entry = class IdentityEntry extends Control.Entry {

        constructor(control, id) {
            super(control, id);

            this.form = new IdentityEntryForm(this.control, 'identity-entry-form');

            this.roles = new IdentityFeatures(this.control, 'identity-entry-roles');
        }

        get data() {
            var data = this.form.data;
            data.Roles = this.roles.data;
            return data;
        }

        set data(data) {
            this.form.data = data;
            this.roles.data = !!data ? data.Roles || [] : [];
        }

        get pristine() {
            if (!super.pristine) {
                return false;
            }

            return true;
        }

        get valid() {
            if (!super.valid) {
                return false;
            }

            return true;
        }

        init() {

            if (!super.init()) {
                return false;
            }

            this.form.init();

            this.roles.init();

            this.fire('control:entry:pristine');

            this.on(this.form.model.data, 'change', this.refresh);

            this.on(this.roles.model, 'change', this.refresh);

            return true;
        }

        insert() {
            if (!this.form.validate()) {
                console.log('validations errors. Should reflect errors.');
                return;
            }
            if (!this.control.review) {
                post().done(function () {
                    this.reset();
                    this.fire('control:entry:pristine');
                });
                return;
            }

            var dataItem = this.control.browse.dataSource.add(this.data);

            var row = this.control.browse.kendoGrid.tbody.find("tr[data-uid='" + dataItem.uid + "']");

            // this should fire the event so it can be previewed
            this.control.browse.kendoGrid.select(row);

            this.reset();

            this.fire('control:entry:pristine');
        }

        refresh() {
            if (this.form.pristine && this.roles.pristine) {
                this.fire('control:entry:pristine');
            } else {
                this.fire('control:entry:dirty');

                this.validate();
            }
        }

        post() {
            this.fire('control:busy');

            // TODO: 

            $.post('/domain/identity', this.data).then(function (response) {

                if (this.control.browse) {

                    var dataItem = this.control.browse.dataSource.add(response.value);

                    var row = this.control.browse.kendoGrid.tbody.find("tr[data-uid='" + dataItem.uid + "']");

                    // this should fire the event so it can be previewed
                    this.control.browse.kendoGrid.select(row);
                }
            }).done(function () {
                this.fire('control:ready');
            });
        }

        reset() {
            this.form.reset();
            this.roles.reset();
            this.refresh();
        }

        validate() {
            var current = this.form.data;

            if (!current.EmailAddress) {
                this.fire('control:entry:invalid');
            }
            else if (this.form.validate()) {
                this.fire('control:entry:valid');
            }
            else {
                this.fire('control:entry:invalid');
            }
        }
    }

})