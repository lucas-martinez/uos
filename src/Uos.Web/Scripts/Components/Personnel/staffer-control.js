﻿define('uos.personnel.staffer.control', ['jquery', 'uos.control.base', 'uos.personnel.staffer.browse', 'uos.personnel.staffer.detail', 'uos.personnel.staffer.edit', 'uos.personnel.staffer.entry', 'uos.personnel.staffer.review'], function ($, Control) {

    class StafferControl extends Control {

        constructor(id) {
            super(id);

            this.defaults = {
                CreatedOn: null,
                FirstName: null,
                FullName: null,
                LastName: null,
                Identity: null,
                Roles: []
            };

            this.mapping = {
                firstName: "FirstName",
                fullName: "FullName",
                identity: "Identity",
                lastName: "LastName",
                roles: "Roles",
                since: "CreatedOn"
            };
        }

        init() {
            if (!super.init()) {
                return false;
            }

            return true;
        }
    }

    return StafferControl;

});