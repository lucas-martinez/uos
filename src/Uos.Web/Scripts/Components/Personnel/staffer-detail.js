﻿define('uos.personnel.staffer.detail', ['jquery', 'uos.control.base', 'uos.data.store', 'kendo.data.contenteditable'], function ($, Control, Store) {

    Control.Detail = class StafferDetail extends Control.Detail {

        constructor(control, id) {
            super(control, id);
        }

        set data(data) {

            if (!data || typeof data !== 'object') {
                kendo.bind(this.element, {});
                return;
            }

            var model = {
                code: data.StafferCode || null,
                name: data.StafferName || null
            };

            if (typeof data.isNew === 'function' && data.isNew()) {
                model.label = "NEW";
            }
            else if (data.dirty === true) {
                model.label = "modified";
            }

            if (typeof data.StafferSize === 'string') {
                model.size = Store.translate('StafferSizes', data.StafferSize);
            }

            if (typeof data.StafferType === 'string') {
                model.type = Store.translate('StafferTypes', data.StafferType);
            }

            if (typeof data.Industry === 'string') {
                model.industry = Store.translate('Industries', data.Industry);
            }

            if (typeof data.Country === 'string') {
                model.country = Store.translate('Countries', data.Country);
            }

            if (typeof data.Currency === 'string') {
                model.currency = Store.translate('Currencies', data.Currency);
            }

            if (typeof data.MetricSystem === 'string') {
                model.metric = Store.translate('MetricSystems', data.MetricSystem);
            }

            if (!!data.Features) {
                var roles = [];
                var collections = ['FinancialFeatures', 'LogisticsFeatures', 'PersonnelFeatures']
                for (var i = 0; i < data.Features.length; i++) {
                    var value = data.Features[i], text = null;
                    for (var j = 0; j < collections.length; j++) {
                        text = Store.translate(collections[j], value);
                        if (!!text) {
                            break;
                        }
                    }
                    roles.push(text || value);
                }
                roles.sort();
                model.roles = roles.join(', ');
            }

            kendo.bind(this.element, model);
        }
    }
})