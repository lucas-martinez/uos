﻿define('uos.form.base', ['jquery', 'kendo', 'uos.component.base'], function ($, kendo, Component) {

    class Form extends Component {

        constructor(control, id) {
            super(id);

            this.control = control;

            this.model = {
                data: kendo.observable({}),
                messanges: {},
                status: {},
                texts: {}
            };

            this.filter = {
            };

            this.rules = {
            };

            this.original = null;
        }

        get data() {

            var result = {};

            for (var modelName in this.control.mapping) {
                var resultName = this.control.mapping[modelName], value = this.model.data[modelName];
                if (typeof value !== 'undefined') {
                    if (value != null && typeof value === 'object') {
                        if (value[resultName]) {
                            value = value[resultName];
                        }
                        else if (value.value) {
                            value = value.value;
                        }
                    }
                    result[resultName] = value;
                }
                else if (typeof defaults === 'object') {
                    result[resultName] = this.control.defaults[resultName] || null;
                }
                else {
                    result[resultName] = null;
                }
            }

            return result;
        }

        set data(source) {

            if (typeof source !== 'object') {
                source = {};
            }

            for (var modelName in this.control.mapping) {
                var sourceName = this.control.mapping[modelName], value = source[sourceName];
                if (typeof value !== 'undefined') {
                    this.model.data.set(modelName, value);
                }
                else if (typeof this.control.defaults === 'object') {
                    this.model.data.set(modelName, this.control.defaults[sourceName] || null);
                }
                else {
                    this.model.data.set(modelName, null);
                }
            }
        }

        get changes() {

            var diff = [], pristine = this.original || this.control.defaults;

            for (var key in this.control.mapping) {
                var current = this.model.data.get(key), name = this.control.mapping[key], original = pristine[name];

                if (current && typeof current === 'object' && typeof current[name] !== 'undefined') {
                    if (current[name] != original) {
                        diff.push(name);
                        /*for (var prop in current) {
                            if (typeof prop === 'string') {
                                var k = this.control.inverseMap(prop), value = current[prop];
                                if (typeof k === 'string' && prop !== name) {
                                    this.model.data.set(k, value);
                                }
                            }
                        }*/
                    }
                    current = current[name];
                }
                else if (current !== original) {
                    diff.push(name);
                }
            }

            return diff;
        }

        get pristine() {
            return this.changes.length === 0;
        }

        init() {

            if (!super.init()) {
                return false;
            }

            for (var name in this.control.mapping) {

                this.filter[name] = (function (id) {
                    return function () {
                        return {
                            startsWith: $(document.getElementById(id)).val()
                        }
                    };
                })(this.id + '-' + name);

                this.model.messanges[name] = null;

                this.model.status[name] = (function (name, form) {
                    return {
                        error: function () {
                            return false;
                        },
                        pristine: function () {
                            return form.changes.length === 0;
                        },
                        success: function () {
                            return true;
                        },
                        warning: function () {
                            return false;
                        }
                    }
                })(name, this);
            }

            this.data = {};

            this.original = kendo.observable(this.data);

            kendo.bind(this.element, this.model);

            //this.validator = $(this.element).kendoValidator().data("kendoValidator");

            return true;
        }

        reset(record) {
            if (typeof record === 'object') {
                this.original = record;
                this.data = record;
            } else {
                this.data = this.control.defaults;
            }
        }
    }

    return Form;
});