﻿define('uos.select-well', ['jquery', 'kendo', 'uos.component.base'], function ($, kendo, Component) {

    class SelectWell extends Component {
        constructor(id, model) {
            super(id);

            this.model = kendo.observable(model);
        }
    }

    return SelectWell;
});