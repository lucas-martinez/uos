﻿define('uos.filter', ['jquery', 'uos.bootstrap', 'uos.data', 'uos.search'], function ($, U) {

    U.search.set('filter', null);

    $(document).ready(function () {

        U.search.bind("change", function (e) {

            if (e.field === 'filter') {

                var data = {
                    activity: [],
                    company: [],
                    date: [],
                    domain: [],
                    email: [],
                    issuer: [],
                    feature: [],
                    name: [],
                    product: [],
                    segment: [],
                    user: []
                };

                for (var i = 0; i < U.search.filter.length; i++) {
                    Array.prototype.push.apply(data[U.search.filter[i].Type], U.search.filter[i].Values);
                }

                if (data.activity.length) {
                    var cf = {};
                    for (var i in data.segment) {
                        U.search.filters.push({
                            field: 'SegmentCode',
                            operator: 'eq',
                            value: data.segment[i]
                        });
                    }
                }

                if (data.segment.length) {
                    var cf = {};
                    for (var i in data.segment) {
                        U.search.filters.push({
                            field: 'SegmentCode',
                            operator: 'eq',
                            value: data.segment[i]
                        });
                    }
                }

                if (data.email.length) {
                    for (var i in data.email) {
                        U.search.filters.push({
                            field: 'EmailAddress',
                            operator: 'eq',
                            value: data.email[i]
                        });
                    }
                }

                if (data.company.length) {
                    for (var i in data.group) {
                        U.search.filters.push({
                            field: 'CompanyCode',
                            operator: 'eq',
                            value: data.company[i]
                        });
                    }
                }

                if (data.feature.length) {
                    for (var i in data.group) {
                        U.search.filters.push({
                            field: 'FeatureName',
                            operator: 'eq',
                            value: data.group[i]
                        });
                    }
                }

                if (data.user.length) {
                    for (var i in data.user) {
                        U.search.filters.push({
                            field: 'UserName',
                            operator: 'eq',
                            value: data.user[i]
                        });
                    }
                }

                $('.k-grid[data-role="grid"]').map(function () {
                    return { id: this.id, dataSource: $(this).data().kendoGrid.dataSource };
                }).each(function () {
                    this.dataSource.filter(U.search);
                });
            }
        });

        kendo.bind($('.navbar [role=search]'), U.search);
    });

    return U.search.filter;
});