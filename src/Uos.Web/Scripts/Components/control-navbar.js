﻿define('uos.control.navbar', ['jquery', 'kendo', 'uos.component.base', 'bootstrap', 'datetimepicker'], function ($, kendo, Component) {

    class Navbar extends Component {

        constructor(id) {
            super(id);
            this.model = kendo.observable({
                browseDisabled: false,
                browseHidden: true,
                detailDisabled: true,
                detailHidden: true,
                discardDisabled: true,
                editDisabled: true,
                editHidden: true,
                entryDisabled: false,
                entryHidden: true,
                filterDisabled: false,
                historyDisabled: false,
                insertDisabled: true,
                resetDisabled: true,
                revertDisabled: true,
                reviewCount: 0,
                reviewDisabled: true,
                reviewHidden: true,
                saveDisabled: true,
                updateDisabled: true,
            });

            this.active = null;

            this.when('control:ready', function (control) {

                this.control = control;

                if (!this.element) {
                    this.init();
                }
            });

            this.when('control:browse', function () {
                this.activate('browse');
            });

            this.when('control:browse:active', function () {
                this.model.set("browseHidden", false);
                this.model.set("detailHidden", true);
                this.model.set("editHidden", true);
                this.model.set("entryHidden", true);
                this.model.set("reviewHidden", true);
            });

            this.when('control:browse:dirty', function (browse, count) {
                this.model.set("reviewDisabled", false);
                var count = browse.addedItems.length + browse.changedItems.length + browse.removedItems.length;
                this.model.set("reviewCount", count);
                this.model.set("discardDisabled", false);
                this.model.set("saveDisabled", false);
            });

            this.when('control:browse:pristine', function () {
                this.model.set("reviewDisabled", true);
                this.model.set("reviewCount", 0);
                this.model.set("discardDisabled", true);
                this.model.set("saveDisabled", true);
            });

            this.when('control:browse:selection-changed', function (selection) {
                var nothing = !selection || selection.length === 0;
                this.model.set("detailDisabled", nothing);
                this.model.set("editDisabled", nothing);
            });

            this.when('control:detail', function () {
                this.activate('detail');
            });

            this.when('control:detail:active', function () {
                this.model.set("browseHidden", true);
                this.model.set("detailHidden", false);
                this.model.set("editHidden", true);
                this.model.set("entryHidden", true);
                this.model.set("reviewHidden", true);
            });

            this.when('control:edit', function () {
                this.activate('edit');
            });

            this.when('control:edit:active', function () {
                this.model.set("browseHidden", true);
                this.model.set("detailHidden", true);
                this.model.set("editHidden", false);
                this.model.set("entryHidden", true);
                this.model.set("reviewHidden", true);
            });

            this.when('control:edit:dirty', function () {
                this.model.set("revertDisabled", false);
            });

            this.when('control:edit:invalid', function () {
                this.model.set("updateDisabled", true);
                this.model.set("revertDisabled", false);
            });

            this.when('control:edit:pristine', function () {
                this.model.set("updateDisabled", true);
                this.model.set("revertDisabled", true);
            });

            this.when('control:edit:valid', function () {
                this.model.set("updateDisabled", false);
                this.model.set("revertDisabled", false);
            });

            this.when('control:entry', function () {
                this.activate('entry');
            });

            this.when('control:entry:active', function () {
                this.model.set("browseHidden", true);
                this.model.set("detailHidden", true);
                this.model.set("editHidden", true);
                this.model.set("entryHidden", false);
                this.model.set("reviewHidden", true);
            });

            this.when('control:entry:dirty', function () {
                this.model.set("resetDisabled", false);
            });

            this.when('control:entry:invalid', function () {
                this.model.set("insertDisabled", true);
            });

            this.when('control:entry:pristine', function () {
                this.model.set("insertDisabled", true);
                this.model.set("resetDisabled", true);
            });

            this.when('control:entry:valid', function () {
                this.model.set("insertDisabled", false);
            });

            this.when('control:filter:disable', function () {
                this.model.set("filterDisabled", true);
            });

            this.when('control:filter:enable', function () {
                this.model.set("filterDisabled", false);
            });

            this.when('control:history:disable', function () {
                this.model.set("historyDisabled", true);
            });

            this.when('control:history:enable', function () {
                this.model.set("historyDisabled", false);
            });

            this.when('control:revert:disable', function () {
                this.model.set("revertDisabled", true);
            });

            this.when('control:revert:enable', function () {
                this.model.set("revertDisabled", false);
            });

            this.when('control:review', function () {
                this.activate('review');
            });

            this.when('control:review:active', function () {
                this.model.set("browseHidden", true);
                this.model.set("detailHidden", true);
                this.model.set("editHidden", true);
                this.model.set("entryHidden", true);
                this.model.set("reviewHidden", false);
            });

            this.when('control:review:count', function (count) {
                this.model.set("reviewCount", count);
            });

            this.when('control:review:disable', function () {
                this.model.set("reviewDisabled", true);
            });

            this.when('control:review:enable', function () {
                this.model.set("reviewDisabled", false);
            });

            this.when('control:save:disable', function () {
                this.model.set("saveDisabled", true);
            });

            this.when('control:save:enable', function () {
                this.model.set("saveDisabled", false);
            });
        }

        init() {

            if (!super.init()) {
                return false;
            }

            kendo.bind($(this.element), this.model);

            $(document.getElementById('control-panel')).tab();

            (function ($datetimepicker) {
                /// initialize datetime pickers
                // https://eonasdan.github.io/bootstrap-datetimepicker/
                $datetimepicker.each(function (index, el) {
                    switch (index) {
                        case 0:
                            $(el).datetimepicker({
                            }).on("dp.change", function (e) {
                                $($datetimepicker[1]).data("DateTimePicker").minDate(e.date);
                            });
                            break;

                        case 1:
                            $(el).datetimepicker({
                                useCurrent: false //Important! See issue #1075
                            }).on("dp.change", function (e) {
                                $($datetimepicker[0]).data("DateTimePicker").maxDate(e.date);
                            });
                            break;
                    }
                });
            })($('.date input', this.element));

            (function (self) {

                $('[role=tablist] > li > a', self.element).on('shown.bs.tab', function (e) {
                    var activeTab = $(e.target);         // active tab
                    var previousTab = $(e.relatedTarget);

                    if (activeTab.parent().hasClass('disabled') || !self.activate(activeTab.attr('href'))) {
                        e.preventDefault();
                        previousTab.tab('show');
                    }
                });

                function activate() {
                    self.activate(window.location.hash);
                }

                $(window).on('hashchange', activate);

                activate();

            })(this);

            return true;
        }

        activate(id) {

            function fail() {

                if (!this.active) {
                    for (let tab of ['browse', 'entry', 'detail', 'edit']) {
                        if (!!this.control[tab]) {
                            this.fire('control:' + tab);
                            return;
                        }
                    }
                }

                var hash = '#' + this.active;

                if (window.location.hash !== hash) {
                    window.location.hash = (hash || '');
                } else {
                    this.fire('control:' + this.active);
                }
                return false;
            }

            if (typeof id !== 'string' || id.length === 0) {
                return fail.apply(this);
            }

            if (id === this.active) {
                return true;
            }

            if (id[0] === '#') {
                id = id.substr(1);
            }

            id = id.toLocaleLowerCase();

            if (!document.getElementById(id)) {
                return fail.apply(this);
            }

            var $a = $('[role=tablist] > li > a[href="#' + id + '"]', this.element);

            if ($a.length === 0) {
                return fail.apply(this);
            }

            if ($a.parent().hasClass('disabled')) {
                return fail.apply(this);
            }

            if (!this.control || !this.control[id]) {
                return fail.apply(this);
            }

            this.active = id;

            $a.tab('show');

            this.fire('control:' + this.active + ':active');

            var hash = '#' + this.active;

            if (window.location.hash !== hash) {
                window.location.hash = hash;
            }

            return true;
        }
    }

    return new Navbar('control-navbar');
});