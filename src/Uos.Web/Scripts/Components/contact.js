﻿define('uos.contact', ['jquery', 'require', 'uos.bootstrap'], function ($, require, U) {
    var _params = {},
       _config = {
       };

    $.each(decodeURIComponent(window.location.search.substring(1)).split('&'), function (index, item) {
        var arr = item.split('=');
        if (arr[0].length) {
            _params[arr[0].toLowerCase()] = arr.length > 1 ? arr[1] : true;
        }
    });

    U.widget = U.widget || {};

    U.widget.contact = function ($placeholder) {
        var $el,
            _completed = false,
            _done = [],
            _widgetUrl = $placeholder.data('widget'),
            _baseUrl = _widgetUrl.substring(0, _widgetUrl.lastIndexOf('/') + 1),
            _ready = [];

        function bind() {
            $el.find('button[name=cmd-cancel]').click(function () {
                $.each(_done, function (index, fn) {
                    fn($el);
                });
            });

            $el.find('button[name=cmd-send]').click(function () {
                $.each(_done, function (index, fn) {
                    fn($el);
                });
            });
        };

        function blend() {
            U.blend($el.find('.umbiliko-contact-body'), 1);
        };

        function done(fn) {
            if (typeof fn !== 'function') {
                return this;
            }
            if (_completed) {
                fn($el);
            } else {
                _done.push(fn);
            }
            return this;
        };

        function ready(fn) {
            if (typeof fn !== 'function') {
                return this;
            }
            if ($el) {
                fn($el);
            } else {
                _ready.push(fn);
            }
            return this;
        };

        function refresh() {
        };

        $.ajax({
            error: function (err) {
            },
            method: 'GET',
            success: function (html) {
                $el = $(html).replaceAll($placeholder);
                bind();
                refresh();
                $.each(_ready, function (index, fn) {
                    fn($el);
                });
            },
            url: _widgetUrl
        });

        return {
            done: done,
            ready: ready,
            blend: function () {
                if ($el) {
                    blend();
                } else {
                    _ready.push(blend);
                }
                return this;
            }
        };
    };

    /*require(['angular'], function () {
        U.contact.controller = U.module.controller('UmbilikoContactController', ['$scope', function ($scope) {
            $scope.greeting = 'Hola!';
        }]);
    });*/

    return U.contact;
});
