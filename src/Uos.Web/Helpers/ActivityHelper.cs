﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web.Mvc;
using System.Web.Routing;
using System.Threading.Tasks;

namespace Uos.Web.Helpers
{
    using Contracts;
    using Filters;
    using Utils;

    public static class ActivityHelper
    {
        public static Activity Action<TController>(this UrlHelper urlHelper, Expression<Func<TController, ActionResult>> expression)
                where TController : Controller
        {
            return Activity(urlHelper, expression);
        }

        public static Activity Action<TController>(this UrlHelper urlHelper, Expression<Func<TController, Task<ActionResult>>> expression)
                where TController : Controller
        {
            return Activity(urlHelper, expression);
        }

        public static Activity Json<TController>(this UrlHelper urlHelper, Expression<Func<TController, JsonResult>> expression)
                where TController : Controller
        {
            return Activity(urlHelper, expression);
        }

        public static Activity Json<TController>(this UrlHelper urlHelper, Expression<Func<TController, Task<JsonResult>>> expression)
                where TController : Controller
        {
            return Activity(urlHelper, expression);
        }

        public static Activity PartialView<TController>(this UrlHelper urlHelper, Expression<Func<TController, PartialViewResult>> expression)
                where TController : Controller
        {
            return Activity(urlHelper, expression);
        }
        public static Activity PartialView<TController>(this UrlHelper urlHelper, Expression<Func<TController, Task<PartialViewResult>>> expression)
                where TController : Controller
        {
            return Activity(urlHelper, expression);
        }

        public static Activity View<TController>(this UrlHelper urlHelper, Expression<Func<TController, ViewResult>> expression)
                where TController : Controller
        {
            return Activity(urlHelper, expression);
        }
        public static Activity View<TController>(this UrlHelper urlHelper, Expression<Func<TController, Task<ViewResult>>> expression)
                where TController : Controller
        {
            return Activity(urlHelper, expression);
        }

        public static Activity Activity(this UrlHelper urlHelper, Expression expression)
        {
            return Activity(urlHelper, expression, new RouteValueDictionary());
        }

        public static Activity Activity(this UrlHelper urlHelper, Expression expression, object routeValues)
        {
            return Activity(urlHelper, expression, new RouteValueDictionary(routeValues));
        }

        public static Activity Activity(this UrlHelper urlHelper, Expression expression, RouteValueDictionary routeValues)
        {
            var member = expression.GetMember();

            if (member.MemberType != MemberTypes.Method)
            {
                return new Activity();
            }

            var actionMethod = (MethodInfo)member;
            var controllerType = actionMethod.DeclaringType;

            var actionName = actionMethod.Name;
            var area = actionMethod.DeclaringType.GetCustomAttributes<RouteAreaAttribute>().Select(attr => attr.AreaName).SingleOrDefault();
            var controllerName = controllerType.Name.TrimEnd("Controller");
            var feature = actionMethod.GetCustomAttributes<ActivityAttribute>().Select(attr => attr.Feature).SingleOrDefault() ??
                actionMethod.DeclaringType.GetCustomAttributes<FeatureAttribute>().Select(attr => attr.Name).SingleOrDefault();
            var name = actionMethod.GetCustomAttributes<ActivityAttribute>().Select(attr => attr.Name).SingleOrDefault();
            var baseUri = new Uri(urlHelper.RequestContext.HttpContext.Request.Url.GetLeftPart(UriPartial.Scheme | UriPartial.Authority), UriKind.Absolute);


            if (area != null)
            {
                routeValues.Add("area", area);
            }

            var actionUrl = urlHelper.Action(
                actionName: actionName,
                controllerName: controllerName,
                routeValues: routeValues);

            var local = true;

            if (urlHelper.RequestContext.HttpContext.Request.UrlReferrer != null)
            {
                local = baseUri.Host == urlHelper.RequestContext.HttpContext.Request.UrlReferrer.Host;
            }

            return new Activity(
                action: actionName,
                area: area,
                controller: controllerName,
                feature: feature,
                local: local,
                name: name,
                uri: new Uri(baseUri, actionUrl));
        }

        public static string Format(this UrlHelper urlHelper, Expression expression, object routeValues)
        {
            var model = Activity(urlHelper, expression, routeValues);
            
            return model.Url;
        }
    }
}
