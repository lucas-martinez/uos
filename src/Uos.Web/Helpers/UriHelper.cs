﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Uos.Web.Helpers
{
    public static class UriExtensions
    {
        public static Uri WithQuery(this Uri uri, Action<NameValueCollection> query)
        {
            var uriBuilder = new UriBuilder(uri);
            var collection = HttpUtility.ParseQueryString(uriBuilder.Query);
            query(collection);
            uriBuilder.Query = collection.ToString();
            return uriBuilder.Uri;
        }
    }
}
