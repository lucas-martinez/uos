﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Helpers
{
    public static class ModelState
    {
        public static IResult GetResult(this System.Web.Http.ModelBinding.ModelStateDictionary modelState)
        {
            return new Result(modelState.IsValid, GetErrorMessages(modelState));
        }

        public static IResult GetResult<TValue>(this System.Web.Http.ModelBinding.ModelStateDictionary modelState, TValue value)
        {
            return new Result<TValue>(modelState.IsValid, value, GetErrorMessages(modelState));
        }

        public static IResult GetResult(this System.Web.Mvc.ModelStateDictionary modelState)
        {
            return new Result(modelState.IsValid, GetErrorMessages(modelState));
        }

        public static IResult GetResult<TValue>(this System.Web.Mvc.ModelStateDictionary modelState, TValue value)
        {
            return new Result<TValue>(modelState.IsValid, value, GetErrorMessages(modelState));
        }

        public static IList<Message> GetErrorMessages(this System.Web.Http.ModelBinding.ModelStateDictionary modelState)
        {
            if (modelState.IsValid)
            {
                return new List<Message>();
            }

            return modelState.SelectMany(state => state.Value.Errors.Select(error =>
            {
                var name = state.Key;
                var text = error.ErrorMessage;

                if (error.Exception != null)
                {
                    name += "[" + error.Exception.GetType().Name.TrimEnd("Exception") + "]";

                    if (string.IsNullOrEmpty(text))
                    {
                        text = error.Exception.Message;
                    }
                }

                if (string.IsNullOrEmpty(text))
                {
                    text = string.Format(Resources.Validation.InvalidFieldValue, state.Key, state.Value.Value);
                }

                return new Message(MessageTypes.Error, text, name: name);

            })).ToList();
        }

        public static IList<Message> GetErrorMessages(this System.Web.Mvc.ModelStateDictionary modelState)
        {
            if (modelState.IsValid)
            {
                return new List<Message>();
            }

            return modelState.SelectMany(state => state.Value.Errors.Select(error =>
            {
                var name = state.Key;
                var text = error.ErrorMessage;

                if (error.Exception != null)
                {
                    name += "[" + error.Exception.GetType().Name.TrimEnd("Exception") + "]";

                    if (string.IsNullOrEmpty(text))
                    {
                        text = error.Exception.Message;
                    }
                }

                if (string.IsNullOrEmpty(text))
                {
                    text = string.Format(Resources.Validation.InvalidFieldValue, state.Key, state.Value.Value);
                }

                return new Message(MessageTypes.Error, text, name: name);

            })).ToList();
        }
    }
}
