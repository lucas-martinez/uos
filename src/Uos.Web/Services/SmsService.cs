﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;

namespace Uos.Web.Services
{
    public class SmsService : IIdentityMessageService
    {
        Task IIdentityMessageService.SendAsync(IdentityMessage message)
        {
            return Twilio.SendAsync(message.Body, message.Destination);
        }
    }
}