﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;

namespace Uos.Web.Services
{
    public class SmtpService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            return Email.SendAsync(message.Subject, message.Body, new { }, message.Destination);
        }
    }
}