﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Umbiliko.Web.Providers
{
    using Constants;
    using Contracts;
    using Entities;
    using Services;
    using Assertion;
    using System.Net.Http;

    public class UserDataContext
    {
        AccountContext _account;
        CompanyContext _company;
        DomainContext _domain;
        UserInfo _data;
        DbContext _dbContext;
        SegmentContext _segment;
        SessionContext _session;
        TokenContext _token;
        UserContext _user;

        public UserDataContext(UserInfo data, DbContext dbContext)
        {
            _data = data;
            _dbContext = dbContext;
        }

        private UserDataContext(AccountContext account)
            : this(new UserInfo(), account.DbContext)
        {
            _account = account;
            _company = account.Company;
            _segment = account.Segment;
            _user = account.User;

            _data.UpdateFrom(_account.Entity);
            
            if (_user != null)
            {
                _data.UpdateFrom(_user.Entity);
            }
        }

        private UserDataContext(SessionContext session)
            : this(new UserInfo(), session.DbContext)
        {
            _account = session.Account;
            _company = session.Company;
            _segment = session.Segment;
            _session = session;
            _user = session.User;

            _data.UpdateFrom(_session.Entity);

            if (_account != null)
            {
                _data.UpdateFrom(_account.Entity);
            }

            if (_user != null)
            {
                _data.UpdateFrom(_user.Entity);
            }
        }

        private UserDataContext(TokenContext token)
            : this(new UserInfo(), token.DbContext)
        {
            _token = token;

            _data.UpdateFrom(token);
        }

        private UserDataContext(UserContext user)
            : this(new UserInfo(), user.DbContext)
        {
            _user = user;

            _data.UpdateFrom(user);
        }

        public AccountContext Account
        {
            get { return _account ?? (_session != null ? _session.Account : null); }
        }

        public DbContext DbContext
        {
            get { return _dbContext; }
        }

        public SegmentContext SegmentContext
        {
            get { return _segment ?? (Account != null ? _account.Segment : null); }
        }

        public SessionContext Session
        {
            get { return _session; }
        }

        public UserContext User
        {
            get { return _user ?? (Account != null ? _account.User : null); }
        }

        public UserInfo UserData
        {
            get { return _data; }
        }
        
        public static async Task<UserDataContext> FindByEmailAsync(DbContext dbContext, string email)
        {
            var source = dbContext.Set<UserEntity>()
                .Include(e => e.Accounts)
                .Include(e => e.Accounts.Select(a => a.Company))
                .Include(e => e.Claims);

            var entity = await source.SingleOrDefaultAsync(e => e.EmailAddress == email);

            if (entity == null) return null;

            return new UserDataContext(UserContext.FromEntity(entity, dbContext));
        }
        
        public static async Task<UserDataContext> FindByNameAsync(DbContext dbContext, string name)
        {
            var source = dbContext.Set<UserEntity>()
                .Include(e => e.Accounts)
                .Include(e => e.Claims);

            var entity = await source.SingleOrDefaultAsync(e => e.UserName == name);
            
            return new UserDataContext(new UserContext(entity, dbContext));
        }

        public static async Task<UserDataContext> FindBySessionIdAsync(DbContext dbContext, Guid id)
        {
            var source = dbContext.Set<SessionEntity>()
                .Include(e => e.Account)
                .Include(e => e.Account.Company)
                .Include(e => e.Account.Segment)
                .Include(e => e.Domain)
                .Include(e => e.Domain.Company)
                .Include(e => e.Domain.Product)
                //.Include(e => e.Domain.Consumer)
                //.Include(e => e.Domain.Customer)
                .Include(e => e.User.Accounts)
                .Include(e => e.User.Claims);

            var entity = await source.SingleOrDefaultAsync(e => e.Id == id);

            if (entity == null) return null;

            SessionContext session;

            if (entity.Account != null)
            {
                var user = new UserContext(entity.User, dbContext);

                var account = new AccountContext(entity.Account, user);

                session = new SessionContext(entity, account);
            }
            else if (entity.User != null)
            {
                var user = new UserContext(entity.User, dbContext);

                session = new SessionContext(entity, user);
            }
            else
            {
                session = new SessionContext(entity, dbContext);
            }
            
            return new UserDataContext(session);
        }

        public static async Task<UserDataContext> FindByTokenAsync(DbContext dbContext, string token)
        {
            var source = dbContext.Set<TokenEntity>()
                .Include(e => e.Company)
                .Include(e => e.Segment)
                .Include(e => e.User);

            var entity = await source.SingleOrDefaultAsync(e => e.TokenValue == token);

            if (entity == null) return null;
            
            return new UserDataContext(new TokenContext(entity, dbContext));
        }

        public async Task<UserContext> RefreshFromUserAsync()
        {
            if (_data.Name == null) return _user;

            var ctx = await FindByNameAsync(DbContext, _data.Name);

            if (ctx == null) return _user;
            
            _user = ctx._user;

            return _user;
        }

        public async Task<UserContext> RefreshFromLoginAsync()
        {
            return null;
        }

        public async Task<UserContext> RefreshFromSessionAsync()
        {
            return null;
        }
        
        public static implicit operator UserInfo(UserDataContext  context)
        {
            return context != null ? context.UserData : null;
        }
    }
}
