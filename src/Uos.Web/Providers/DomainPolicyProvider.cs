﻿using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Cors;

namespace Uos.Web.Providers
{
    using Constants;
    using Contracts;
    using Entities;
    using Services;

    public class DomainPolicyProvider : ICorsEngine, Microsoft.Owin.Cors.ICorsPolicyProvider, System.Web.Http.Cors.ICorsPolicyProvider, System.Web.Http.Cors.ICorsPolicyProviderFactory
    {
        readonly static CorsPolicy Reject = new CorsPolicy
        {
            AllowAnyHeader = false,
            AllowAnyMethod = false,
            AllowAnyOrigin = false,
            PreflightMaxAge = 0,
            SupportsCredentials = false
        };

        DomainStore _domainStore;

        public DomainStore DomainStore
        {
            get { return _domainStore ?? (_domainStore = new DomainStore(new UmbilikoDbContext())); }
        }

        public async Task<CorsPolicy> CreateCorsPolicyAsync(Uri remoteUri, string method)
        {
            DomainInfo domain;

            if (remoteUri.Host == "localhost")
            {
                domain = DomainInfo.LocalHost();
            }
            else
            {
                domain = await DomainStore.FindAsync(remoteUri.Host);
            }
            

            if (domain == null) return Reject;

            var policy = new CorsPolicy
            {
                AllowAnyHeader = true,
                //AllowAnyOrigin = true,
                AllowAnyMethod = true,
                //PreflightMaxAge = 1000,
                SupportsCredentials = true
            };

            policy.Headers.Add("Accept");
            policy.Headers.Add("Access-Control-Allow-Credentials");
            policy.Headers.Add("Access-Control-Allow-Methods");
            policy.Headers.Add("Access-Control-Allow-Origin");
            policy.Headers.Add("Authorization");
            policy.Headers.Add("Content-Type");
            policy.Headers.Add("Origin");
            policy.Headers.Add("X-Requested-With");

            policy.Methods.Add("DELETE");
            policy.Methods.Add("GET");
            policy.Methods.Add("OPTIONS");
            policy.Methods.Add("PATCH");
            policy.Methods.Add("POST");
            policy.Methods.Add("PUT");
            
            policy.Origins.Add(remoteUri.GetLeftPart(UriPartial.Scheme | UriPartial.Authority));

            return policy;
        }

        CorsResult ICorsEngine.EvaluatePolicy(CorsRequestContext requestContext, CorsPolicy policy)
        {
            return new CorsResult
            {
                AllowedOrigin = requestContext.Origin,
                PreflightMaxAge = policy.PreflightMaxAge,
                SupportsCredentials = policy.SupportsCredentials
            };
        }

        Task<CorsPolicy> Microsoft.Owin.Cors.ICorsPolicyProvider.GetCorsPolicyAsync(IOwinRequest request)
        {
            Uri referrerUri;
            if (!Uri.TryCreate(request.Headers["Referer"], UriKind.Absolute, out referrerUri))
            {
                Debug.Assert(false);
            }

            return CreateCorsPolicyAsync(referrerUri, request.Method);
        }

        Task<CorsPolicy> System.Web.Http.Cors.ICorsPolicyProvider.GetCorsPolicyAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            return CreateCorsPolicyAsync(request.Headers.Referrer, Convert.ToString(request.Method));
        }

        System.Web.Http.Cors.ICorsPolicyProvider System.Web.Http.Cors.ICorsPolicyProviderFactory.GetCorsPolicyProvider(HttpRequestMessage request)
        {
            return this;
        }
    }
}
