﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Facebook;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace Uos.Web.Providers
{   
    public class FacebookAuthenticationProvider : Microsoft.Owin.Security.Facebook.FacebookAuthenticationProvider
    {
        /*public override async Task Authenticated(FacebookAuthenticatedContext context)
        {
            var nameIdentifier = context.Identity.FindFirst(ClaimTypes.NameIdentifier);
            
            Debug.Assert(context.Id == nameIdentifier.Value);

            var login = new LoginInfo
            {
                AccessToken = context.AccessToken,
                Label = context.Name,
                LoginKey = nameIdentifier.Value,
                LoginProvider = nameIdentifier.Issuer,
                LoginUtc = DateTime.UtcNow
            };

            if (context.ExpiresIn.HasValue) login.ExpiresUtc = DateTime.UtcNow + context.ExpiresIn.Value;

            Debug.Assert(context.Name == context.UserName);

            //context.Identity.AddClaimExact(type: UmbilikoClaimTypes.AccessToken, value: context.AccessToken, issuer: login.LoginProvider);
            //context.Identity.AddClaimExact(type: ClaimTypes.Email, value: context.Email, issuer: login.LoginProvider);
            //context.Identity.AddClaimExact(type: ClaimTypes.Expiration, input: login.ExpiresUtc, issuer: login.LoginProvider);
            //context.Identity.AddClaimExact(type: ClaimTypes.Name, value: context.Name, issuer: login.LoginProvider);
            context.Identity.AddClaimExact(type: ClaimTypes.Uri, value: context.Link, issuer: login.LoginProvider);

            if (context.User != null)
            {
                var errors = new List<string>();

                var serializer = new JsonSerializer();

                serializer.Error += new EventHandler<Newtonsoft.Json.Serialization.ErrorEventArgs>((currentObject, args) =>
                {
                    errors.Add(args.ErrorContext.Path);
                    args.ErrorContext.Handled = true;
                });

                var userData = context.User.ToObject<FacebookUserInfo>(serializer);

                context.Identity.AddClaimExact(type: UmbilikoClaimTypes.SessionId, value: userData.Id, issuer: login.LoginProvider);
                context.Identity.AddClaimExact(type: UmbilikoClaimTypes.DisplayName, value: userData.Name, issuer: login.LoginProvider);
            }

            IResult result;

            try
            {
                result = await AuthenticationProvider.Current.AuthenticateAsync(login, context.Properties, context.Identity, context);
            }
            catch (Exception e)
            {
                result = Result.Fail(e);
            }

            await base.Authenticated(context);
        }*/
    }
}
