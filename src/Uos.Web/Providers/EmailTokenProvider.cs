﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Providers
{
    using Entities;
    
    public class EmailTokenProvider : UserTokenProviderBase
    {
        private DbContext _dbContext;

        public EmailTokenProvider(DbContext dbContext)
        {
            _dbContext = dbContext;
        }
    }
}
