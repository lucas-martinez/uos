﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.MicrosoftAccount;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Providers
{   
    public class MicrosoftAccountAuthenticationProvider : Microsoft.Owin.Security.MicrosoftAccount.MicrosoftAccountAuthenticationProvider
    {
        /*public override async Task Authenticated(MicrosoftAccountAuthenticatedContext context)
        {
            var nameIdentifier = context.Identity.FindFirst(ClaimTypes.NameIdentifier);
            
            Debug.Assert(context.Id == nameIdentifier.Value);

            var login = new LoginInfo
            {
                AccessToken = context.AccessToken,
                Label = context.FirstName ?? context.LastName,
                LoginKey = nameIdentifier.Value,
                LoginProvider = nameIdentifier.Issuer,
                LoginUtc = DateTime.UtcNow
            };

            if (context.ExpiresIn.HasValue) login.ExpiresUtc = DateTime.UtcNow + context.ExpiresIn.Value;

            context.Identity.AddClaimExact(type: UmbilikoClaimTypes.AccessToken, value: context.AccessToken, issuer: login.LoginProvider);
            context.Identity.AddClaimExact(type: ClaimTypes.Email, value: context.Email, issuer: login.LoginProvider);
            context.Identity.AddClaimExact(type: ClaimTypes.Expiration, input: login.ExpiresUtc, issuer: login.LoginProvider);
            context.Identity.AddClaimExact(type: ClaimTypes.GivenName, value: context.FirstName, issuer: login.LoginProvider);
            context.Identity.AddClaimExact(type: ClaimTypes.Surname, value: context.LastName, issuer: login.LoginProvider);
            context.Identity.AddClaimExact(type: ClaimTypes.Name, value: context.Name, issuer: login.LoginProvider);

            if (context.User != null)
            {
                var errors = new List<string>();

                var serializer = new JsonSerializer();

                serializer.Error += new EventHandler<Newtonsoft.Json.Serialization.ErrorEventArgs>((currentObject, args) =>
                {
                    errors.Add(args.ErrorContext.Path);
                    args.ErrorContext.Handled = true;
                });

                var userData = context.User.ToObject<MicrosoftAccountUserInfo>(serializer);
            }

            IResult result;

            try
            {
                result = await AuthenticationProvider.Current.AuthenticateAsync(login, context.Properties, context.Identity, context);
            }
            catch (Exception e)
            {
                result = Result.Fail(e);
            }

            await base.Authenticated(context);
        }*/
    }
}
