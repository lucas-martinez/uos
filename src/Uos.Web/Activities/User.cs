﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Uos.Web.Activities
{
    using Controllers;
    
    public static class User
    {
        public static Expression<Func<HomeController, ViewResult>> GetRegisterView
        {
            get { return Linq.Expression<HomeController, ViewResult>(controller => controller.Register()); }
        }

        public static Expression<Func<HomeController, Task<ActionResult>>> ExternalLoginCallback
        {
            get { return Linq.Expression<HomeController, Task<ActionResult>>(controller => controller.ExternalLoginCallback(null)); }
        }
    }
}
