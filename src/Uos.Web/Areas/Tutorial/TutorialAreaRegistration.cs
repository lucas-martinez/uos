﻿using System.Web.Mvc;

namespace Uos.Web.Areas.Tutorial
{
    using Constants;
    using Controllers;

    public class TutorialAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get { return AreaNames.Tutorial; }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.Routes.MapMvcAttributeRoutes();

            context.MapRoute(
                AreaNames.Tutorial,
                url: AreaNames.Tutorial + "/{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { typeof(HomeController).Namespace }
            );
        }
    }
}