﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Uos.Web.Areas.Tutorial.Controllers
{
    using Constants;

    [Authorize]
    [RouteArea(AreaNames.Tutorial), RoutePrefix("")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class HomeController : Controller
    {
        //
        // GET: tutorial
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}