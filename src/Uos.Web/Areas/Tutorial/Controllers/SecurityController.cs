﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Uos.Web.Areas.Tutorial.Controllers
{
    using Constants;
    using Filters;
    using Models;
    
    [Authorize, RouteArea(AreaNames.Tutorial), RoutePrefix("security")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class SecurityController : Controller
    {
        //
        // GET: tutorial/security
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }

        //
        // GET: tutorial/security/clearance
        [HttpGet, Route("clearance")]
        public ViewResult Clearance()
        {
            return View();
        }
    }
}