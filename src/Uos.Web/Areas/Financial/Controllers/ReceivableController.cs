﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Financial.Controllers
{
    using Constants;
    using Filters;
    using Models;
    
    // Accounts Receivable
    [Feature(Uos.Accounting.Features.Receivable)]
    [Authorize]
    [RouteArea(AreaNames.Financial), RoutePrefix("accounting/receivable")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class ReceivableController : Controller
    {
        // 
        // GET: financial/accounting/receivable
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}