﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Financial.Controllers
{
    using Constants;
    using Filters;
    using Models;
    
    // Management Consolidation
    [Feature(Uos.Enterprise.Features.Management)]
    [Authorize]
    [RouteArea(AreaNames.Financial), RoutePrefix("enterprise/management")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class ManagementController : Controller
    {
        // 
        // GET: financial/enterprise/management
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}