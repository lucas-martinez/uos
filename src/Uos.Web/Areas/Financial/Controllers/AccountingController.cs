﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Financial.Controllers
{
    using Constants;
    using Filters;
    using Models;

    // Financial Accounting
    [Feature(Uos.Financial.Features.Accounting)]
    [Authorize]
    [RouteArea(AreaNames.Financial), RoutePrefix("accounting")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class AccountingController : Controller
    {
        //
        // GET: financial/accounting
        [HttpGet, Route("")]
        public RedirectResult Index()
        {
            // TODO: find out which feature to redirect to
            return RedirectPermanent("financial/accounting/general");
        }
    }
}