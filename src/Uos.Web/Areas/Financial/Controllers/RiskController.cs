﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Financial.Controllers
{
    using Constants;
    using Filters;

    // Market Risk Management
    [Feature(Uos.Treasury.Features.Risk)]
    [Authorize]
    [RouteArea(AreaNames.Financial), RoutePrefix("treasury/risk")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class RiskController : Controller
    {
        // GET: financial/treasury/risk
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}