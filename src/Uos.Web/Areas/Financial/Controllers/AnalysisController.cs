﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Financial.Controllers
{
    using Constants;
    using Filters;
    using Models;

    // Sales and Profitability Analysis
    [Feature(Uos.Controlling.Features.Analysis)]
    [Authorize]
    [RouteArea(AreaNames.Financial), RoutePrefix("controlling/analysis")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class AnalysisController : Controller
    {
        // 
        // GET: financial/controlling/analysis
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}