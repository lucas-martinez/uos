﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Financial.Controllers
{
    using Constants;
    using Filters;

    // Treasury Management
    [Feature(Uos.Treasury.Features.Management)]
    [Authorize]
    [RouteArea(AreaNames.Financial), RoutePrefix("treasury")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class TreasuryController : Controller
    {
        //
        // GET: financial/treasury
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}