﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Financial.Controllers
{
    using Constants;
    using Filters;
    using Models;

    // Activity-Based Controlling
    [Feature(Uos.Controlling.Features.Activity)]
    [Authorize]
    [RouteArea(AreaNames.Financial), RoutePrefix("controlling/activity")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class ActivityController : Controller
    {
        // 
        // GET: financial/controlling/activity
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}