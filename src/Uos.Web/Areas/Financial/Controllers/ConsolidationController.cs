﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Financial.Controllers
{
    using Constants;
    using Filters;
    using Models;

    // Consolidation
    [Feature(Uos.Accounting.Features.Consolidation)]
    [Authorize]
    [RouteArea(AreaNames.Financial), RoutePrefix("accounting/consolidation")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class ConsolidationController : Controller
    {
        // GET: financial/accounting/consolidation
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}