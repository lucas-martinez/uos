﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Financial.Controllers
{
    using Constants;
    using Filters;
    using Models;

    // Cash Management
    [Feature(Uos.Treasury.Features.Cash)]
    [Authorize]
    [RouteArea(AreaNames.Financial), RoutePrefix("treasury/cash")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class CashController : Controller
    {
        // GET: financial/treasury/cash
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}