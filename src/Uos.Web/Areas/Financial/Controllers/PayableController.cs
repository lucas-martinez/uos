﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Financial.Controllers
{
    using Constants;
    using Filters;
    using Models;
    
    // Accounts Payable
    [Feature(Uos.Accounting.Features.Payable)]
    [Authorize]
    [RouteArea(AreaNames.Financial), RoutePrefix("accounting/payable")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class PayableController : Controller
    {
        //
        // GET: financial/accounting/payable
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}