﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Uos.Web.Areas.Financial.Controllers
{
    using Constants;
    using Filters;
    using Models;

    // Founds Management
    [Feature(Uos.Treasury.Features.Founds)]
    [Authorize]
    [RouteArea(AreaNames.Financial), RoutePrefix("treasury/founds")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class FoundsController : Controller
    {
        // GET: financial/treasury/founds
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}