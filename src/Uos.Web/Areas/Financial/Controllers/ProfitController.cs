﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Financial.Controllers
{
    using Constants;
    using Filters;
    using Models;
    
    // Profit Center Accounting
    [Feature(Uos.Enterprise.Features.Profit)]
    [Authorize]
    [RouteArea(AreaNames.Financial), RoutePrefix("enterprise/profit")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class ProfitController : Controller
    {
        // 
        // GET: financial/enterprise/profit
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}