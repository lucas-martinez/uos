﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Financial.Controllers
{
    using Constants;
    using Filters;
    using Models;

    // Enterprise Controlling
    [Feature(Uos.Financial.Features.Enterprise)]
    [Authorize]
    [RouteArea(AreaNames.Financial), RoutePrefix("enterprise")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class EnterpriseController : Controller
    {
        //
        // GET: financial/enterprise
        [HttpGet, Route("")]
        public RedirectResult Index()
        {
            // TODO: find out which feature to redirect to
            return RedirectPermanent("financial/enterprise/executive");
        }
    }
}