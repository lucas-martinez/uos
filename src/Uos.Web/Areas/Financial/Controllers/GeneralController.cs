﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Financial.Controllers
{
    using Constants;
    using Filters;
    using Models;

    // General Ledger Accounting
    [Feature(Uos.Accounting.Features.General)]
    [Authorize]
    [RouteArea(AreaNames.Financial), RoutePrefix("accounting/general")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class GeneralController : Controller
    {
        //
        // GET: financial/accounting/general
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}