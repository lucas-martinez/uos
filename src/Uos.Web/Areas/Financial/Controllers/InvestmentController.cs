﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Financial.Controllers
{
    using Constants;
    using Filters;
    using Models;
    
    // Capital Investment Management (programs)
    [Feature(Uos.Investment.Features.Capital)]
    [Authorize]
    [RouteArea(AreaNames.Financial), RoutePrefix("investment")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class InvestmentController : Controller
    {
        //
        // GET: financial/investment
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}