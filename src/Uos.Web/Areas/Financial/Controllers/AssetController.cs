﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Financial.Controllers
{
    using Constants;
    using Filters;
    using Models;

    // Asset Accounting
    [Feature(Uos.Accounting.Features.Asset)]
    [Authorize]
    [RouteArea(AreaNames.Financial), RoutePrefix("accounting/asset")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class AssetController : Controller
    {
        // GET: financial/accounting/asset
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}