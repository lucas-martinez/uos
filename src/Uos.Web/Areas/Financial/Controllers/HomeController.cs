﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Uos.Web.Areas.Financial.Controllers
{
    using Constants;
    using Filters;
    using Models;

    [Authorize, Feature(Uos.Financial.Features.Accounting)]
    [RouteArea(AreaNames.Financial), RoutePrefix("")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class HomeController : Controller
    {
        //
        // GET: financial
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}