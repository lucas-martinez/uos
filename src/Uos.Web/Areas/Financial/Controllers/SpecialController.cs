﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Financial.Controllers
{
    using Constants;
    using Filters;
    using Models;
    
    // Special Purpose Ledger
    [Feature(Uos.Accounting.Features.Special)]
    [Authorize]
    [RouteArea(AreaNames.Financial), RoutePrefix("accounting/special")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class SpecialController : Controller
    {
        //
        // GET: financial/accounting/special
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}