﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Financial.Controllers
{
    using Constants;
    using Filters;
    using Models;

    // Executive Information System
    [Feature(Uos.Enterprise.Features.Executive)]
    [Authorize]
    [RouteArea(AreaNames.Financial), RoutePrefix("enterprise/executive")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class ExecutiveController : Controller
    {
        // 
        // GET: financial/enterprise/executive
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}