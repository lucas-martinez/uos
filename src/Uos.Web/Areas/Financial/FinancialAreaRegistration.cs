﻿using System.Web.Mvc;

namespace Uos.Web.Areas.Financial
{
    using Constants;
    using Controllers;

    public class FinancialAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get { return AreaNames.Financial; }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.Routes.MapMvcAttributeRoutes();

            context.MapRoute(
                AreaNames.Financial,
                url: AreaNames.Financial + "/{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { typeof(HomeController).Namespace }
            );
        }
    }
}