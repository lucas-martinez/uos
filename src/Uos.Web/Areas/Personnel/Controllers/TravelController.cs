﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Personnel.Controllers
{
    using Constants;
    using Filters;
    using Models;
    
    // Travel Expenses
    [Feature(Uos.Personnel.Administration.Features.Travel)]
    [Authorize]
    [RouteArea(AreaNames.Personnel), RoutePrefix("administration/travel")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class TravelController : Controller
    {
        //
        // GET: personnel/administration/travel
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}