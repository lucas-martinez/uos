﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Personnel.Controllers
{
    using Constants;
    using Filters;
    using Models;
    
    // Time Management
    [Feature(Uos.Personnel.Administration.Features.Time)]
    [Authorize]
    [RouteArea(AreaNames.Personnel), RoutePrefix("administration/time")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class TimeController : Controller
    {
        //
        // GET: personnel/administration/time
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}