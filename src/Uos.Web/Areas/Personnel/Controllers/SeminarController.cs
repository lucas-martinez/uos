﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Personnel.Controllers
{
    using Constants;
    using Filters;
    using Models;
    
    // Seminars and Convention Managing
    [Feature(Uos.Personnel.Development.Features.Seminar)]
    [Authorize]
    [RouteArea(AreaNames.Personnel), RoutePrefix("development/seminar")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class SeminarController : Controller
    {
        //
        // GET: personnel/development/seminar
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}