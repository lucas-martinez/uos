﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Personnel.Controllers
{
    using Constants;
    using Filters;
    using Models;
    
    // Workforce Planning
    [Feature(Uos.Personnel.Development.Features.Workforce)]
    [Authorize]
    [RouteArea(AreaNames.Personnel), RoutePrefix("development/workforce")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class WorkforceController : Controller
    {
        //
        // GET: personnel/development/workforce
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}