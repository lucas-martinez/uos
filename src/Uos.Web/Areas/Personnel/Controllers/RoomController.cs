﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Personnel.Controllers
{
    using Constants;
    using Filters;
    using Models;
    
    // Room Reservation Planning
    [Feature(Uos.Personnel.Development.Features.Room)]
    [Authorize]
    [RouteArea(AreaNames.Personnel), RoutePrefix("development/room")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class RoomController : Controller
    {
        //
        // GET: personnel/development/room
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}