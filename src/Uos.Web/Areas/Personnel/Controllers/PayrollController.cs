﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Personnel.Controllers
{
    using Constants;
    using Filters;
    using Models;
    
    // Payroll
    [Feature(Uos.Personnel.Administration.Features.Payroll)]
    [Authorize]
    [RouteArea(AreaNames.Personnel), RoutePrefix("administration/payroll")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class PayrollController : Controller
    {
        //
        // GET: personnel/administration/payroll
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}