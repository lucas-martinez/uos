﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Uos.Web.Areas.Personnel.Controllers
{
    using Constants;
    using Filters;
    using Models;
    
    [Authorize, RouteArea(AreaNames.Personnel), RoutePrefix("staffer")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class StafferController : Controller
    {
        //
        // GET: /personnel/staffer
        [Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}