﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Personnel.Controllers
{
    using Constants;
    using Filters;
    using Models;
    
    // Organizational Management
    [Feature(Uos.Personnel.Development.Features.Organizational)]
    [Authorize]
    [RouteArea(AreaNames.Personnel), RoutePrefix("development/organizational")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class OrganizationalController : Controller
    {
        //
        // GET: personnel/development/organizational
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}