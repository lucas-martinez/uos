﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Personnel.Controllers
{
    using Constants;
    using Filters;
    using Models;

    // Personnel Development
    [Feature(Uos.Personnel.Features.Development)]
    [Authorize]
    [RouteArea(AreaNames.Personnel), RoutePrefix("development")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class DevelopmentController : Controller
    {
        //
        // GET: personnel/development
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}