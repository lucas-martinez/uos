﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Personnel.Controllers
{
    using Constants;
    using Filters;
    using Models;

    // Incentive Wages
    [Feature(Uos.Personnel.Administration.Features.Incentive)]
    [Authorize]
    [RouteArea(AreaNames.Personnel), RoutePrefix("administration/incentive")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class IncentiveController : Controller
    {
        //
        // GET: personnel/administration/incentive
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}