﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Uos.Web.Areas.Personnel.Controllers
{
    using Constants;
    using Filters;
    using Models;
    
    // Human Resources Applications
    [Authorize, Feature(Uos.Personnel.Features.Administration)]
    [RouteArea(AreaNames.Personnel), RoutePrefix("")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class HomeController : Controller
    {
        // GET: personnel
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}