﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Personnel.Controllers
{
    using Constants;
    using Filters;
    using Models;
    
    // Applicant Management
    [Feature(Uos.Personnel.Administration.Features.Applicant)]
    [Authorize]
    [RouteArea(AreaNames.Personnel), RoutePrefix("administration/applicant")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class ApplicantController : Controller
    {   
        //
        // GET: personnel/administration/applicant
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}