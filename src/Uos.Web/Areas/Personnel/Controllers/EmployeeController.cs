﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Personnel.Controllers
{
    using Constants;
    using Filters;
    using Models;
    
    // Employee Management
    [Feature(Uos.Personnel.Administration.Features.Employee)]
    [Authorize]
    [RouteArea(AreaNames.Personnel), RoutePrefix("administration/employee")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class EmployeeController : Controller
    {
        //
        // GET: personnel/administration/employee
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}