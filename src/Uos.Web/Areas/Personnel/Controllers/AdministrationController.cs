﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Personnel.Controllers
{
    using Constants;
    using Filters;
    using Models;

    // Personnel Administration
    [Feature(Uos.Personnel.Features.Administration)]
    [Authorize]
    [RouteArea(AreaNames.Personnel), RoutePrefix("administration")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class AdministrationController : Controller
    {
        //
        // GET: personnel/administration
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}