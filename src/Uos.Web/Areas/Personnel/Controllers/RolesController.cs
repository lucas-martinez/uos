﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Personnel.Controllers
{
    using Constants;
    using Web.Models;

    [Authorize]
    [RouteArea(AreaNames.Personnel), RoutePrefix("roles")]
    public class RolesController : Controller
    {
        //
        // GET|POST: /personnel/roles
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post), Route("")]
        public async Task<JsonResult> GetRoleNames(string startsWith, [Bind(Prefix = "filter")]FilterRequestModel model)
        {
            if (startsWith == null && model != null && model.Filters != null)
            {
                startsWith = model.Filters.Where(f => f.Value != null).Select(f => f.Value).FirstOrDefault();
            }

            IEnumerable<string> names;

            if (startsWith == null) names = new string[0];
            else
            {
                var result = await Staffer.GetStafferRoleNamesAsync(startsWith);
                names = result.OfType<string>();
            }

            return Json(names, JsonRequestBehavior.AllowGet);
        }

        JsonResult Json(DataSourceRequest request, IResult result)
        {
            return Json(result.OfType<StafferRoleEntity>().ToDataSourceResult(request, ModelState));
        }

        JsonResult Json(DataSourceRequest request, IEnumerable<IResult> results)
        {
            return Json(results.SelectMany(result => result.OfType<StafferRoleEntity>()).ToDataSourceResult(request, ModelState));
        }
    }
}