﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Personnel.Controllers
{
    using Constants;
    using Filters;
    using Models;

    // Benefits
    [Feature(Uos.Personnel.Administration.Features.Benefits)]
    [Authorize]
    [RouteArea(AreaNames.Personnel), RoutePrefix("administration/benefits")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class BenefitsController : Controller
    {
        //
        // GET: personnel/administration/benefits
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}