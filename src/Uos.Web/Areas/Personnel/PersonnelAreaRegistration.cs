﻿using System.Web.Mvc;

namespace Uos.Web.Areas.Personnel
{
    using Constants;
    using Controllers;

    public class PersonnelAreaRegistration : AreaRegistration 
    {
        public override string AreaName
        {
            get { return AreaNames.Personnel; }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.Routes.MapMvcAttributeRoutes();

            context.MapRoute(
                "personnel",
                url: AreaNames.Personnel + "/{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { typeof(HomeController).Namespace }
            );
        }
    }
}