﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Uos.Web.Areas.Personnel.Models
{
    public class StafferModel
    {
        [MaxLength(Uos.Domain.Lengths.CompanyCode), Required]
        public string CompanyCode { get; set; }

        public string CompanyName { get; set; }

        public DateTime? CreatedOn { get; set; }

        [MaxLength(Uos.Domain.Lengths.EmailAddress), Required]
        public string EmailAddress { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime? LoggedOn { get; set; }

        public IList<string> Roles { get; set; }

        public int StafferId { get; set; }
    }
}