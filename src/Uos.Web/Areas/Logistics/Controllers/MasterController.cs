﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Logistics.Controllers
{
    using Constants;
    using Filters;
    using Models;
    
    [Authorize]
    [Feature(Uos.Logistics.Features.Master)]
    [RouteArea(AreaNames.Logistics), RoutePrefix("master")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class MasterController : Controller
    {
        //
        // GET: logistics/master
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}