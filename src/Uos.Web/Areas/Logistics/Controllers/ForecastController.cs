﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Logistics.Controllers
{
    using Constants;
    using Filters;
    using Models;
    
    [Authorize]
    [Feature(Uos.Logistics.Features.Forecast)]
    [RouteArea(AreaNames.Logistics), RoutePrefix("forecast")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class ForecastController : Controller
    {
        // GET: lgistics/forecast
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}