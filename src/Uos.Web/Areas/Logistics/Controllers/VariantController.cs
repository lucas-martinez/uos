﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Logistics.Controllers
{
    using Constants;
    using Filters;
    using Models;
    
    [Authorize]
    [Feature(Uos.Logistics.Features.Variant)]
    [RouteArea(AreaNames.Logistics), RoutePrefix("variant")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class VariantController : Controller
    {
        // 
        // GET: logistics/variant
        public ViewResult Index()
        {
            return View();
        }
    }
}