﻿using System.Web.Mvc;

namespace Uos.Web.Areas.Logistics
{
    using Constants;
    using Controllers;

    public class LogisticsAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get { return AreaNames.Logistics; }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.Routes.MapMvcAttributeRoutes();

            context.MapRoute(
                "logistics",
                url: AreaNames.Logistics + "/{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { typeof(GeneralController).Namespace }
            );
        }
    }
}