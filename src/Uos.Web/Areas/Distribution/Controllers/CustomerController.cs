﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Uos.Web.Areas.Distribution.Controllers
{
    using Constants;
    using Filters;

    [Authorize, Feature(Uos.Transportation.Features.Planning)]
    [RouteArea(AreaNames.Distribution), RoutePrefix("customer")]
    public class CustomerController : Controller
    {
        // GET: management/customer
        public ViewResult Index()
        {
            return View();
        }
    }
}