﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Distribution.Controllers
{
    using Constants;
    using Filters;
    using Models;

    // Foreign Treade
    [Authorize, Feature(Uos.Distribution.Features.Foreign)]
    [RouteArea(AreaNames.Distribution), RoutePrefix("foreign-trade")]
    public class ForeignController : Controller
    {
        //
        // GET: logistics/distribution/foreign
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}