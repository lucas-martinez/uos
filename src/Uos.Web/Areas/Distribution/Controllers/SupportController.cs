﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Distribution.Controllers
{
    using Constants;
    using Filters;
    using Models;

    // Sales Support
    [Authorize]
    [Feature(Uos.Distribution.Features.Support)]
    [RouteArea(AreaNames.Distribution), RoutePrefix("sales-support")]
    public class SupportController : Controller
    {
        //
        // GET: logistics/distribution/sales-support
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}