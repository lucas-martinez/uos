﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Distribution.Controllers
{
    using Constants;
    using Filters;
    using Models;

    // Sales
    [Authorize]
    [Feature(Uos.Distribution.Features.Sales)]
    [RouteArea(AreaNames.Distribution), RoutePrefix("sales")]
    public class SalesController : Controller
    {
        //
        // GET: logistics/distribution/sales
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}