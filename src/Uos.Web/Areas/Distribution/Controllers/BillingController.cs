﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Distribution.Controllers
{
    using Constants;
    using Filters;
    using Models;

    // Billing
    [Authorize, Feature(Uos.Distribution.Features.Billing)]
    [RouteArea(AreaNames.Distribution), RoutePrefix("billing")]
    public class BillingController : Controller
    {
        //
        // GET: logistics/distribution/billing
        public ViewResult Index()
        {
            return View();
        }
    }
}