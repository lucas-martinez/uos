﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Uos.Web.Areas.Distribution.Controllers
{
    using Constants;
    using Filters;

    [Authorize, Feature(Uos.Transportation.Features.Planning)]
    [RouteArea(AreaNames.Distribution), RoutePrefix("customers")]
    public class CustomersController : Controller
    {
        //
        // GET: distribution/customers
        [Route("")]
        public ActionResult Index()
        {
            return View();
        }
    }
}