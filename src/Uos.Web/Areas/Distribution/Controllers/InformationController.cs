﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Distribution.Controllers
{
    using Constants;
    using Filters;
    using Models;

    // Sales Information System
    [Authorize]
    [Feature(Uos.Logistics.Features.Distribution)]
    [RouteArea(AreaNames.Distribution), RoutePrefix("sales-information")]
    public class InformationController : Controller
    {
        //
        // GET: logistics/distribution/sales-information
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}