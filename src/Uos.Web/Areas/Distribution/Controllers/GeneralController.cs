﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Distribution.Controllers
{
    using Constants;
    using Filters;
    using Models;
    
    // General Sales Functions
    [Authorize]
    [Feature(Uos.Distribution.Features.General)]
    [RouteArea(AreaNames.Distribution), RoutePrefix("general-sales")]
    public class GeneralController : Controller
    {
        // GET: logistics/distribution/general
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}