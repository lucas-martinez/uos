﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Distribution.Controllers
{
    using Constants;
    using Filters;
    using Models;
    
    [Authorize]
    [Feature(Uos.Logistics.Features.Distribution)]
    [RouteArea(AreaNames.Distribution), RoutePrefix("")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class HomeController : Controller
    {
        //
        // GET: logistics/distribution
        [HttpGet, Route("")]
        public RedirectResult Index()
        {
            return RedirectPermanent("logistics/distribution/master-data");
        }
    }
}