﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Distribution.Controllers
{
    using Constants;
    using Filters;
    using Models;

    // Electronic Data Interchange
    [Authorize]
    [Feature(Uos.Distribution.Features.Interchange)]
    [RouteArea(AreaNames.Distribution), RoutePrefix("edi")]
    public class InterchangeController : Controller
    {
        //
        // GET: logistics/distribution/edi
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}