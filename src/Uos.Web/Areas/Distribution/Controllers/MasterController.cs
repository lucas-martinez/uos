﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Distribution.Controllers
{
    using Constants;
    using Filters;
    using Models;

    // Master Data
    [Authorize]
    [Feature(Uos.Distribution.Features.Master)]
    [RouteArea(AreaNames.Distribution), RoutePrefix("master-data")]
    public class MasterController : Controller
    {
        //
        // GET: logistics/distribution/master-sata
        public ViewResult Index()
        {
            return View();
        }
    }
}