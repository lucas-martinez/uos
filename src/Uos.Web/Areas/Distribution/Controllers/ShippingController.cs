﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Distribution.Controllers
{
    using Constants;
    using Filters;
    using Models;

    // Shipping
    [Authorize]
    [Feature(Uos.Distribution.Features.Shipping)]
    [RouteArea(AreaNames.Distribution), RoutePrefix("shipping")]
    public class ShippingController : Controller
    {
        //
        // GET: logistics/distribution/shipping
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}