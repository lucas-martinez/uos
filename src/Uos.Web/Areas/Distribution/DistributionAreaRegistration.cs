﻿using System.Web.Mvc;

namespace Uos.Web.Areas.Distribution
{
    using Constants;
    using Controllers;

    public class DistributionAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get { return AreaNames.Distribution; }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.Routes.MapMvcAttributeRoutes();

            context.MapRoute(
                AreaNames.Distribution.Replace('/', '_'),
                url: AreaNames.Distribution + "/{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { typeof(HomeController).Namespace }
            );
        }
    }
}