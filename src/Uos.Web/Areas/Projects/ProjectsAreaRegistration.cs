﻿using System.Web.Mvc;

namespace Uos.Web.Areas.Projects
{
    using Constants;
    using Controllers;

    public class ProjectsAreaRegistration : AreaRegistration 
    {
        public override string AreaName
        {
            get { return AreaNames.Projects; }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.Routes.MapMvcAttributeRoutes();

            context.MapRoute(
                AreaNames.Projects.Replace('/', '_'),
                url: AreaNames.Projects + "/{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { typeof(HomeController).Namespace }
            );
        }
    }
}