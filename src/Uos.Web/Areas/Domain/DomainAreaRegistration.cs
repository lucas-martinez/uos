﻿
using System.Web.Mvc;

namespace Uos.Web.Areas.Domain
{
    using Constants;
    using Controllers;

    public class DomainAreaRegistration : AreaRegistration 
    {
        public override string AreaName
        {
            get { return AreaNames.Domain; }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.Routes.MapMvcAttributeRoutes();

            context.MapRoute(
                AreaNames.Domain,
                url: AreaNames.Domain + "/{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { typeof(AssuranceController).Namespace }
            );
        }
    }
}