﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Domain.Controllers
{
    using Constants;
    using Helpers;
    using Models;
    using Web.Models;

    [Authorize]
    [RouteArea(AreaNames.Domain), RoutePrefix("companies")]
    public class CompaniesController : Controller
    {
        static readonly string[] UpdateProperties = typeof(StafferModel).GetProperties().Select(p => p.Name).Except(new[] { "CompanyCode", "Properties" }).ToArray();
        static readonly Func<CompanyEntity, IList<string>> CompanyFeatures = (e) => e.Features != null ? e.Features.Where(f => f.RemovedOn == null).Select(f => f.FeatureName).ToList() : (IList<string>)Array.Empty<string>();
        static readonly Func<CompanyEntity, CompanyModel> CompanySelector = (e) => new CompanyModel
        {
            CompanyCode = e.CompanyCode,
            CompanyName = e.CompanyName,
            CompanySize = e.CompanySize,
            CompanyType = e.CompanyType,
            Country = e.Country,
            Currency = e.Currency,
            Features = CompanyFeatures(e),
            Industry = e.Industry,
            MetricSystem = e.MetricSystem
        };

        //
        // GET|POST: domain/companies
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post), Route("")]
        public JsonResult ReadCompanies([DataSourceRequest]DataSourceRequest request)
        {
            return Json(Company.Store.Include(e => e.Features).ToDataSourceResult(request, CompanySelector), JsonRequestBehavior.AllowGet);
        }

        //
        // GET: domain/companies/{code}
        [HttpGet, Route("{code}")]
        public async Task<JsonResult> GetCompany(string code)
        {
            var result = await Company.GetCompanyAsync(code);

            if (!result.Succeeded) return Json(null, JsonRequestBehavior.AllowGet);

            var model = result.OfType<CompanyEntity>().Select(CompanySelector).Single();
            
            //model.Remarks = entity.Remarks.Where(e => e.RemovedOn == null).Select(e => e.Text).ToList();
            
            return Json(Result.Ok(model), JsonRequestBehavior.AllowGet);
        }

        //
        // GET: domain/companies/{code}/remarks
        [HttpGet, Route("{code}/remarks")]
        public async Task<JsonResult> GetCompanyRemarks(string code)
        {
            var result = await Company.GetCompanyAsync(code);

            if (!result.Succeeded) return Json(result, JsonRequestBehavior.AllowGet);

            var entity = result.OfType<CompanyEntity>().Single();

            var remarks = entity.Remarks.Where(e => e.RemovedOn == null).Select(e => e.Text).ToList();

            return Json(Result.Ok(remarks), JsonRequestBehavior.AllowGet);
        }

        //
        // GET|POST: domain/companies/auto-complete
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post), Route("auto-complete")]
        public async Task<JsonResult> GetCompanies(string startswith, [Bind(Prefix = "filter")]FilterRequestModel model, [DataSourceRequest]DataSourceRequest request)
        {
            Func<CompanyEntity, CompanyHeaderModel> selector = (e) => new CompanyHeaderModel { CompanyCode = e.CompanyCode, CompanyName = e.CompanyName };

            if (string.IsNullOrEmpty(startswith) && request != null && request.Filters != null && request.Filters.Count > 0)
            {
                return Json(Company.Store.ToDataSourceResult(request, selector), JsonRequestBehavior.AllowGet);
            }

            if (string.IsNullOrEmpty(startswith) && model != null && model.Filters != null)
            {
                startswith = model.Filters.Where(f => f.Value != null).Select(f => f.Value).FirstOrDefault();
            }

            if (string.IsNullOrEmpty(startswith))
            {
                return Json(Array.Empty<object>(), JsonRequestBehavior.AllowGet);
            }

            var result = await Company.GetCompaniesCodeNameAsync(startswith);

            if (!result.Succeeded) return Json(Array.Empty<object>(), JsonRequestBehavior.AllowGet);

            var value = result.OfType<IDictionary<string, string>>().Single();

            return Json(value.Select(o => new CompanyHeaderModel { CompanyCode = o.Key, CompanyName = o.Value }));
        }

        #region - Kendo UI batch edit -

        //
        // POST: domain/companies/batch-create
        [HttpPost, Route("batch-create"), ValidateInput(false)]
        public async Task<JsonResult> BatchCreateCompanies([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IList<CompanyEntryModel> models)
        {
            List<IResult> results = new List<IResult>();

            if (models != null && ModelState.IsValid)
            {
                for (var i = 0; i < models.Count; i++)
                {
                    var model = models[i];

                    model.CompanyCode = model.CompanyCode.ToUpperInvariant();
                    
                    var entity = new CompanyEntity
                    {
                        CompanyCode = model.CompanyCode,
                        CompanyName = model.CompanyName,
                        CompanySize = model.CompanySize,
                        CompanyType = model.CompanyType,
                        Country = model.Country,
                        Currency = model.Currency,
                        Industry = model.Industry,
                        MetricSystem = model.MetricSystem
                    };

                    var result = await Company.CreateCompanyAsync(entity);

                    results.Add(result);

                    if (model.Features != null)
                    {
                        result = await Company.SetFeaturesAsync(entity, model.Features);

                        if (!result.Succeeded) results.Add(result);
                    }

                    if (model.Users != null)
                    {
                        var roleNames = model.Roles != null ? model.Roles : new string[] { };

                        foreach (var identity in model.Users)
                        {
                            result = await Staffer.CreateStafferAsync(model.CompanyCode, identity, roleNames);

                            if (!result.Succeeded) results.Add(result);
                        }
                    }

                    if (model.Remarks != null && model.Remarks.Count > 0)
                    {
                        foreach (var html in model.Remarks)
                        {
                            await Company.AddCompanyNoticeAsync(model.CompanyCode, html.Trim());
                        }
                    }

                    var prefix = "models[" + i + "]";

                    TryUpdateModel(entity, prefix, UpdateProperties, ValueProvider);
                }
            }
            else
            {
                results = new List<IResult> { ModelState.GetResult() };
            }

            return Json(request, results);
        }

        //
        // DELETE|POST: domain/companies/batch-destroy
        [AcceptVerbs(HttpVerbs.Delete | HttpVerbs.Post), Route("batch-destroy")]
        public async Task<JsonResult> BatchDestroyCompanies([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<CompanyEntity> models)
        {
            List<IResult> results = new List<IResult>();

            if (models != null & ModelState.IsValid)
            {
                foreach (var model in models)
                {
                    var result = await Company.DeleteCompanyAsync(model.CompanyCode);

                    results.Add(result);
                }
            }

            return Json(request, results);
        }


        //
        // PATCH|PUT|POST: domain/companies/batch-update
        [AcceptVerbs(HttpVerbs.Patch | HttpVerbs.Put | HttpVerbs.Post), Route("batch-update")]
        public async Task<JsonResult> BatchUpdateCompanies([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IList<CompanyModel> models)
        {
            List<IResult> results = new List<IResult>();

            if (models != null & ModelState.IsValid)
            {
                var includeProperties = typeof(CompanyModel).GetProperties().Select(p => p.Name).Except(new[] { "CompanyCode" }).ToArray();

                for (var i = 0; i < models.Count; i++)
                {
                    var model = models[i];

                    var prefix = "models[" + i + "]";

                    if (!ValueProvider.ContainsPrefix(prefix))
                    {
                        results.Add(Result.Fail<CompanyModel>(model, () => string.Format("could not update {0}", i)));

                        continue;
                    }

                    IResult result;

                    result = await Company.UpdateCompanyAsync(model.CompanyCode, async (entity) =>
                    {
                        if (model.Features != null)
                        {
                            result = await Company.SetFeaturesAsync(entity, model.Features);

                            if (!result.Succeeded) results.Add(result);
                        }

                        return TryUpdateModel(entity, prefix, includeProperties, ValueProvider);
                    });

                    results.Add(result);
                }
            }
            else
            {
                results = new List<IResult> { ModelState.GetResult() };
            }

            return Json(request, results);
        }

        #endregion - Kendo UI inline edit -

        #region - Kendo UI inline edit -

        //
        // POST: domain/companies/inline-create
        [HttpPost, Route("inline-create"), ValidateInput(false)]
        public async Task<JsonResult> InlineCreateCompany([DataSourceRequest]DataSourceRequest request, CompanyEntity company)
        {
            IResult result = Result.Failure;

            if (company != null && ModelState.IsValid)
            {
                result = await Company.CreateCompanyAsync(company);
            }

            return Json(request, result);
        }

        //
        // DELETE|POST: domain/companies/inline-delete
        [AcceptVerbs(HttpVerbs.Delete | HttpVerbs.Post), Route("inline-delete")]
        public async Task<JsonResult> InlineDestroyCompany([DataSourceRequest]DataSourceRequest request, CompanyEntryModel model)
        {
            IResult result = Result.Failure;

            if (model != null && ModelState.IsValid)
            {
                var entity = new CompanyEntity
                {
                    CompanyCode = model.CompanyCode.ToUpperInvariant()
                };

                result = await Company.CreateCompanyAsync(entity);
                
                if (result.Succeeded)
                {
                    entity = result.OfType<CompanyEntity>().Single();

                    if (model.Features != null)
                    {
                        result = await Company.SetFeaturesAsync(entity, model.Features);
                    }
                }

                TryUpdateModel(entity, UpdateProperties, ValueProvider);
            }

            return Json(request, result);
        }

        //
        // PATCH|PUT|POST: domain/companies/inline-update
        [AcceptVerbs(HttpVerbs.Patch | HttpVerbs.Put | HttpVerbs.Post), Route("inline-update")]
        public async Task<JsonResult> InlineUpdateCompany([DataSourceRequest]DataSourceRequest request, CompanyModel model)
        {
            IResult result = Result.Failure;

            if (model != null && ModelState.IsValid)
            {
                result = await Company.UpdateCompanyAsync(model.CompanyCode, async (entity) =>
                {
                    if (model.Features != null)
                    {
                        result = await Company.SetFeaturesAsync(entity, model.Features);
                    }

                    return TryUpdateModel(entity, UpdateProperties, ValueProvider);
                });
            }

            return Json(request, result);
        }

        #endregion - Kendo UI inline edit -

        JsonResult Json(DataSourceRequest request, IResult result)
        {
            return Json(result.OfType<CompanyEntity>().ToDataSourceResult(request, ModelState));
        }

        JsonResult Json(DataSourceRequest request, IEnumerable<IResult> results)
        {
            return Json(results.SelectMany(result => result.OfType<CompanyEntity>()).ToDataSourceResult(request, ModelState));
        }
    }
}