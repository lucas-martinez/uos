﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Domain.Controllers
{
    using Cognito;
    using Constants;
    using Filters;
    using Helpers;
    using Models;

    [Authorize, RouteArea(AreaNames.Domain), RoutePrefix("staffers")]
    public class StaffersController : Controller
    {
        static readonly string[] UpdateProperties = typeof(StafferModel).GetProperties().Select(p => p.Name).Except(new[] { "CompanyCode", "EmailAddress", "Roles" }).ToArray();
        static readonly Func<StafferEntity, IList<string>> StafferRoles = (e) => e.Roles != null ? e.Roles.Where(r => r.RemovedOn == null).Select(r => r.RoleName).ToList() : (IList<string>)Array.Empty<string>();
        static readonly Func<StafferEntity, StafferModel> StafferSelector = (e) => new StafferModel
        {
            CompanyCode = e.CompanyCode,
            CompanyName = e.Company.CompanyName,
            CreatedOn = e.CreatedOn,
            FirstName = e.FirstName,
            EmailAddress = e.EmailAddress,
            LastName = e.LastName,
            LoggedOn = e.LoggedOn,
            Roles = StafferRoles(e),
            StafferId = e.StafferId
        };
        
        //
        // GET|POST: personnel/staffers
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post), Route("")]
        public JsonResult ReadStaffers([DataSourceRequest]DataSourceRequest request)
        {
            return Json(Staffer.Store.Include(e => e.Company, e => e.Roles).ToDataSourceResult(request, StafferSelector), JsonRequestBehavior.AllowGet);
        }
        /*
        //
        // GET: personnel/staffers/{code}
        [HttpGet, Route("{code}")]
        public async Task<JsonResult> GetStaffer(string code)
        {
            var result = await Staffer.GetStafferAsync(code);

            if (!result.Succeeded) return Json(null, JsonRequestBehavior.AllowGet);

            var entity = result.OfType<StafferEntity>().Single();

            var model = new StafferModel
            {
                CompanyCode = entity.CompanyCode,
                Identity = entity.Identity,
                Roles = entity.Roles.Where(r => r.RemovedOn == null).Select(r => r.RoleName).ToList()
            };

            return Json(Result.Ok(model), JsonRequestBehavior.AllowGet);
        }
        /*
        //
        // GET: personnel/staffers/{code}/remarks
        [HttpGet, Route("{code}/remarks")]
        public async Task<JsonResult> GetStafferRemarks(string code)
        {
            var result = await Staffer.GetStafferAsync(code);

            if (!result.Succeeded) return Json(result, JsonRequestBehavior.AllowGet);

            var entity = result.OfType<StafferEntity>().Single();

            var remarks = entity.Remarks.Where(e => e.RemovedOn == null).Select(e => e.Text).ToList();

            return Json(Result.Ok(remarks), JsonRequestBehavior.AllowGet);
        }
        */

        #region - Kendo UI batch edit -

        //
        // POST: personnel/staffers/batch-create
        [HttpPost, Route("batch-create"), ValidateInput(false)]
        public async Task<JsonResult> BatchCreateStaffers([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IList<StafferEntryModel> models)
        {
            List<IResult> results = new List<IResult>();

            if (models != null && ModelState.IsValid)
            {
                for (var i = 0; i < models.Count; i++)
                {
                    var model = models[i];

                    model.CompanyCode = model.CompanyCode.ToUpperInvariant();
                    model.EmailAddress = model.EmailAddress.ToLowerInvariant();

                    var staffer = new StafferEntity
                    {
                        CompanyCode = model.CompanyCode,
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        EmailAddress = model.EmailAddress
                    };

                    /*var prefix = "models[" + i + "]";

                    if (!ValueProvider.ContainsPrefix(prefix) || !TryUpdateModel(stafferEntity, prefix, ValueProvider))
                    {
                        results.Add(Result.Fail<StafferModel>(model, () => string.Format("could not update {0}", i)));
                    }*/

                    var result = await Staffer.CreateStafferAsync(staffer);

                    results.Add(result);

                    if (result.Succeeded && model.Roles != null)
                    {
                        result = await Staffer.SetRolesAsync(staffer, model.Roles);

                        if (!result.Succeeded) results.Add(result);
                    }

                    if (model.Remarks != null && model.Remarks.Count > 0)
                    {
                        foreach (var html in model.Remarks)
                        {
                            //await Staffer.AddStafferNoticeAsync(model.CompanyCode, model.Indentity, html.Trim());
                        }
                    }
                }
            }
            else
            {
                results = new List<IResult> { ModelState.GetResult() };
            }

            return Json(request, results);
        }

        //
        // DELETE|POST: personnel/staffers/batch-destroy
        [AcceptVerbs(HttpVerbs.Delete | HttpVerbs.Post), Route("batch-destroy")]
        public async Task<JsonResult> BatchDestroyStaffers([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<StafferEntity> models)
        {
            List<IResult> results = new List<IResult>();

            if (models != null & ModelState.IsValid)
            {
                foreach (var model in models)
                {
                    var result = await Staffer.DeleteStafferAsync(model.CompanyCode, model.EmailAddress);

                    results.Add(result);
                }
            }

            return Json(request, results);
        }


        //
        // PATCH|PUT|POST: personnel/staffers/batch-update
        [AcceptVerbs(HttpVerbs.Patch | HttpVerbs.Put | HttpVerbs.Post), Route("batch-update")]
        public async Task<JsonResult> BatchUpdateStaffers([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IList<StafferModel> models)
        {
            List<IResult> results = new List<IResult>();

            if (models != null & ModelState.IsValid)
            {
                for (var i = 0; i < models.Count; i++)
                {
                    var model = models[i];

                    var prefix = "models[" + i + "]";

                    if (!ValueProvider.ContainsPrefix(prefix))
                    {
                        results.Add(Result.Fail<StafferModel>(model, () => string.Format("could not update {0}", i)));

                        continue;
                    }

                    IResult result;

                    result = await Staffer.UpdateStafferAsync(model.CompanyCode, model.EmailAddress, async (entity) =>
                    {
                        if (model.Roles != null)
                        {
                            result = await Staffer.SetRolesAsync(entity, model.Roles);

                            if (!result.Succeeded) results.Add(result);
                        }

                        return TryUpdateModel(entity, prefix, UpdateProperties, ValueProvider);
                    });

                    results.Add(result);
                }
            }
            else
            {
                results = new List<IResult> { ModelState.GetResult() };
            }

            return Json(request, results);
        }

        #endregion - Kendo UI inline edit -

        #region - Kendo UI inline edit -

        //
        // POST: personnel/staffers/inline-create
        [HttpPost, Route("inline-create"), ValidateInput(false)]
        public async Task<JsonResult> InlineCreateStaffer([DataSourceRequest]DataSourceRequest request, StafferEntity staffer)
        {
            IResult result = Result.Failure;

            if (staffer != null && ModelState.IsValid)
            {
                result = await Staffer.CreateStafferAsync(staffer);
            }

            return Json(request, result);
        }

        //
        // DELETE|POST: personnel/staffers/inline-delete
        [AcceptVerbs(HttpVerbs.Delete | HttpVerbs.Post), Route("inline-delete")]
        public async Task<JsonResult> InlineDestroyStaffer([DataSourceRequest]DataSourceRequest request, StafferEntity staffer)
        {
            IResult result = Result.Failure;

            if (staffer != null && ModelState.IsValid)
            {
                result = await Staffer.CreateStafferAsync(staffer);
            }

            return Json(request, result);
        }

        //
        // PATCH|PUT|POST: personnel/staffers/inline-update
        [AcceptVerbs(HttpVerbs.Patch | HttpVerbs.Put | HttpVerbs.Post), Route("inline-update")]
        public async Task<JsonResult> InlineUpdateStaffer([DataSourceRequest]DataSourceRequest request, StafferModel model)
        {
            IResult result = Result.Failure;

            if (model != null && ModelState.IsValid)
            {
                result = await Staffer.UpdateStafferAsync(model.CompanyCode, model.EmailAddress, async (entity) =>
                {
                    if (model.Roles != null)
                    {
                        result = await Staffer.SetRolesAsync(entity, model.Roles);

                        //if (!result.Succeeded) results.Add(result);
                    }

                    return TryUpdateModel(entity, UpdateProperties, ValueProvider);
                });
            }

            return Json(request, result);
        }

        #endregion - Kendo UI inline edit -

        JsonResult Json(DataSourceRequest request, IResult result)
        {
            return Json(result.OfType<StafferEntity>().ToDataSourceResult(request, ModelState));
        }

        JsonResult Json(DataSourceRequest request, IEnumerable<IResult> results)
        {
            return Json(results.SelectMany(result => result.OfType<StafferEntity>()).ToDataSourceResult(request, ModelState));
        }
    }
}