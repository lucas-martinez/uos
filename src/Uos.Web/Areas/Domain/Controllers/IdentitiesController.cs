﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Uos.Cognito;

namespace Uos.Web.Areas.Domain.Controllers
{
    using Constants;
    using Helpers;
    using Models;

    [Authorize]
    [RouteArea(AreaNames.Domain), RoutePrefix("identities")]
    public class IdentitiesController : Controller
    {
        static readonly string[] UpdateProperties = typeof(IdentityModel).GetProperties().Select(p => p.Name).Except(new[] { "Claims", "EmailAddress", "Logins", "Roles" }).ToArray();
        static readonly Func<IdentityEntity, string, string> IdentityClaim = (e, claimType) => e.Claims.Where(c => c.RemovedOn == null && c.ClaimType == claimType).Select(c => c.ClaimValue).FirstOrDefault();
        static readonly Func<IdentityEntity, string, IList<string>> IdentityClaims = (e, claimType) => e.Claims.Where(c => c.RemovedOn == null && c.ClaimType == claimType).Select(c => c.ClaimValue).ToList();
        static readonly Func<IdentityEntity, IList<string>> IdentityRoles = (e) => e.Roles != null ? e.Roles.Where(r => r.RemovedOn == null).Select(r => r.RoleName).ToList() : (IList<string>)Array.Empty<string>();
        static readonly Func<IdentityEntity, Models.IdentityModel> IdentitySelector = (e) => new Models.IdentityModel
        {
            Address = IdentityClaim(e, Cognito.IdentityClaims.Address),
            Country = IdentityClaim(e, Cognito.IdentityClaims.Country),
            CreatedOn = e.CreatedOn,
            EmailAddress = e.EmailAddress,
            EmailConfirmed = e.EmailConfirmed,
            LockedOut = e.LockoutEndDateUtc == null,
            FirstName = IdentityClaim(e, Cognito.IdentityClaims.FirstName),
            FullName = IdentityClaim(e, Cognito.IdentityClaims.FullName),
            Language = IdentityClaim(e, Cognito.IdentityClaims.Language),
            LastName = IdentityClaim(e, Cognito.IdentityClaims.LastName),
            Locality = IdentityClaim(e, Cognito.IdentityClaims.Locality),
            OtherPhones = IdentityClaims(e, Cognito.IdentityClaims.OtherPhone),
            PasswordStrength = e.PasswordHash != null ? PasswordStrengths.Strong : PasswordStrengths.None,
            PhoneNumber = e.PhoneNumber,
            PhoneConfirmed = e.PhoneConfirmed,
            PostalCode = IdentityClaim(e, Cognito.IdentityClaims.PostalCode),
            Roles = IdentityRoles(e),
            Region = IdentityClaim(e, Cognito.IdentityClaims.Region),
            TwoFactorEnabled = e.TwoFactorEnabled,
            Urls = IdentityClaims(e, Cognito.IdentityClaims.Url),
        };

        //
        // GET|POST: domain/identities
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post), Route("")]
        public JsonResult ReadIdentities([DataSourceRequest]DataSourceRequest request)
        {
            return Json(Identity.Store.Include(e => e.Claims, e => e.Roles).ToDataSourceResult(request, IdentitySelector), JsonRequestBehavior.AllowGet);
        }

        //
        // GET: domain/identities/{identity}
        [HttpGet, Route("{identity}")]
        public async Task<JsonResult> GetIdentity(string identity)
        {
            var result = await Identity.GetIdentityAsync(identity);

            if (!result.Succeeded) return Json(null, JsonRequestBehavior.AllowGet);

            var entity = result.OfType<IdentityEntity>().Single();

            var model = new Models.IdentityModel
            {
                EmailAddress = entity.EmailAddress,
                Roles = entity.Roles.Where(f => f.RemovedOn == null).Select(r => r.RoleName).ToList()
            };

            //model.Remarks = entity.Remarks.Where(e => e.RemovedOn == null).Select(e => e.Text).ToList();

            /*var task = Identity.GetIdentityFeaturesAsync(entity.IdentityCode);

            task.Wait();

            if (task.Result.Succeeded)
            {
                model.Features = string.Join(" ", task.Result.OfType<string>());
            }*/

            return Json(Result.Ok(model), JsonRequestBehavior.AllowGet);
        }
        /*
        //
        // GET: domain/identities/{identity}/remarks
        [HttpGet, Route("{identity}/remarks")]
        public async Task<JsonResult> GetIdentityRemarks(string identity)
        {
            var result = await Identity.GetIdentityAsync(identity);

            if (!result.Succeeded) return Json(result, JsonRequestBehavior.AllowGet);

            var entity = result.OfType<IdentityEntity>().Single();

            var remarks = entity.Remarks.Where(e => e.RemovedOn == null).Select(e => e.Text).ToList();

            return Json(Result.Ok(remarks), JsonRequestBehavior.AllowGet);
        }
        */

        //
        // GET|POST: domain/identities/auto-complete
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post), Route("auto-complete")]
        public async Task<JsonResult> GetIdentities(string startswith, [Bind(Prefix = "filter")]Uos.Web.Models.FilterRequestModel model, [DataSourceRequest]DataSourceRequest request)
        {
            Func<IdentityEntity, IdentityHeaderModel> selector = (e) => new IdentityHeaderModel
            {
                EmailAddress = e.EmailAddress,
                FullName = e.FullName,
                PhoneNumber = e.PhoneNumber
            };

            if (string.IsNullOrEmpty(startswith) && request != null && request.Filters != null && request.Filters.Count > 0)
            {
                return Json(Identity.Store.ToDataSourceResult(request, selector), JsonRequestBehavior.AllowGet);
            }

            if (string.IsNullOrEmpty(startswith) && model != null && model.Filters != null)
            {
                startswith = model.Filters.Where(f => f.Value != null).Select(f => f.Value).FirstOrDefault();
            }

            if (string.IsNullOrEmpty(startswith))
            {
                return Json(Array.Empty<object>(), JsonRequestBehavior.AllowGet);
            }

            var result = await Identity.GetIdentities(startswith);

            if (!result.Succeeded) return Json(Array.Empty<object>(), JsonRequestBehavior.AllowGet);
            
            return Json(result.OfType<IdentityEntity>().Select(selector));
        }

        #region - Kendo UI batch edit -

        //
        // POST: domain/identities/batch-create
        [HttpPost, Route("batch-create"), ValidateInput(false)]
        public async Task<JsonResult> BatchCreateIdentities([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IList<IdentityEntryModel> models)
        {
            List<IResult> results = new List<IResult>();

            if (models != null && ModelState.IsValid)
            {
                for (var i = 0; i < models.Count; i++)
                {
                    var model = models[i];

                    model.EmailAddress = model.EmailAddress.ToLowerInvariant();

                    var entity = new IdentityEntity
                    {
                        EmailAddress = model.EmailAddress
                    };

                    
                    /*var prefix = "models[" + i + "]";

                    if (!ValueProvider.ContainsPrefix(prefix) || !TryUpdateModel(identityEntity, prefix, ValueProvider))
                    {
                        results.Add(Result.Fail<IdentityModel>(model, () => string.Format("could not update {0}", i)));
                    }*/

                    var result = await Identity.CreateIdentityAsync(entity);

                    results.Add(result);

                    entity = result.OfType<IdentityEntity>().Single();

                    if (result.Succeeded)
                    {
                        entity = result.OfType<IdentityEntity>().Single();

                        result = await Identity.SetClaimsAsync(entity, new Dictionary<string, string>
                        {
                            { Cognito.IdentityClaims.Address, model.Address },
                            { Cognito.IdentityClaims.BirthDate, Convert.ToString(model.BirthDate) },
                            { Cognito.IdentityClaims.Country, model.Country },
                            { Cognito.IdentityClaims.FirstName, model.FirstName },
                            { Cognito.IdentityClaims.FullName, model.FullName },
                            { Cognito.IdentityClaims.Language, model.Language },
                            { Cognito.IdentityClaims.LastName, model.LastName },
                            { Cognito.IdentityClaims.Locality, model.Locality },
                            { Cognito.IdentityClaims.Region, model.Region }
                        });

                        if (!result.Succeeded) results.Add(result);

                        if (model.OtherPhones != null)
                        {
                            result = await Identity.SetClaimsAsync(entity, Cognito.IdentityClaims.OtherPhone, model.OtherPhones);

                            if (!result.Succeeded) results.Add(result);
                        }

                        if (model.Urls != null)
                        {
                            result = await Identity.SetClaimsAsync(entity, Cognito.IdentityClaims.Url, model.Urls);

                            if (!result.Succeeded) results.Add(result);
                        }

                        if (model.Roles != null)
                        {
                            result = await Identity.SetRolesAsync(entity, model.Roles);

                            if (!result.Succeeded) results.Add(result);
                        }

                        if (model.Remarks != null && model.Remarks.Count > 0)
                        {
                            //result = await Identity.AddRemarksAsync(entity, model.Remarks);

                            //if (!result.Succeeded) results.Add(result);
                        }

                        var prefix = "models[" + i + "]";

                        TryUpdateModel(entity, prefix, UpdateProperties, ValueProvider);
                    }
                }
            }
            else
            {
                results = new List<IResult> { ModelState.GetResult() };
            }

            return Json(request, results);
        }

        //
        // DELETE|POST: domain/identities/batch-destroy
        [AcceptVerbs(HttpVerbs.Delete | HttpVerbs.Post), Route("batch-destroy")]
        public async Task<JsonResult> BatchDestroyIdentities([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<IdentityEntity> models)
        {
            List<IResult> results = new List<IResult>();

            if (models != null & ModelState.IsValid)
            {
                foreach (var model in models)
                {
                    var result = await Identity.DeleteIdentityAsync(model.EmailAddress);

                    results.Add(result);
                }
            }

            return Json(request, results);
        }


        //
        // PATCH|PUT|POST: domain/identities/batch-update
        [AcceptVerbs(HttpVerbs.Patch | HttpVerbs.Put | HttpVerbs.Post), Route("batch-update")]
        public async Task<JsonResult> BatchUpdateIdentities([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IList<Models.IdentityModel> models)
        {
            List<IResult> results = new List<IResult>();

            if (models != null & ModelState.IsValid)
            {
                for (var i = 0; i < models.Count; i++)
                {
                    var model = models[i];

                    var prefix = "models[" + i + "]";

                    if (!ValueProvider.ContainsPrefix(prefix))
                    {
                        results.Add(Result.Fail<Models.IdentityModel>(model, () => string.Format("could not update {0}", i)));

                        continue;
                    }

                    IResult result;

                    result = await Identity.UpdateIdentityAsync(model.EmailAddress, async (entity) =>
                    {
                        result = await Identity.SetClaimsAsync(entity, new Dictionary<string, string>
                        {
                            { Cognito.IdentityClaims.Address, model.Address },
                            { Cognito.IdentityClaims.BirthDate, Convert.ToString(model.BirthDate) },
                            { Cognito.IdentityClaims.Country, model.Country },
                            { Cognito.IdentityClaims.FirstName, model.FirstName },
                            { Cognito.IdentityClaims.FullName, model.FullName },
                            { Cognito.IdentityClaims.Language, model.Language },
                            { Cognito.IdentityClaims.LastName, model.LastName },
                            { Cognito.IdentityClaims.Locality, model.Locality },
                            { Cognito.IdentityClaims.Region, model.Region }
                        });

                        if (!result.Succeeded) results.Add(result);

                        if (model.OtherPhones != null)
                        {
                            result = await Identity.SetClaimsAsync(entity, Cognito.IdentityClaims.OtherPhone, model.OtherPhones);

                            if (!result.Succeeded) results.Add(result);
                        }

                        if (model.Urls != null)
                        {
                            result = await Identity.SetClaimsAsync(entity, Cognito.IdentityClaims.Url, model.Urls);

                            if (!result.Succeeded) results.Add(result);
                        }

                        if (model.Roles != null)
                        {
                            result = await Identity.SetRolesAsync(entity, model.Roles);

                            if (!result.Succeeded) results.Add(result);
                        }

                        /*if (model.Remarks != null && model.Remarks.Count > 0)
                        {
                            result = await Identity.AddRemarksAsync(entity, model.Remarks);

                            if (!result.Succeeded) results.Add(result);
                        }*/

                        return TryUpdateModel(entity, prefix, UpdateProperties, ValueProvider);
                    });
                    
                    if (result.Succeeded)
                    {
                        result = Result.Ok(result.OfType<IdentityEntity>().Select(IdentitySelector).Single());
                    }

                    results.Add(result);
                }
            }
            else
            {
                results = new List<IResult> { ModelState.GetResult() };
            }

            return Json(request, results);
        }

        #endregion - Kendo UI inline edit -

        #region - Kendo UI inline edit -

        //
        // POST: domain/identities/inline-create
        [HttpPost, Route("inline-create"), ValidateInput(false)]
        public async Task<JsonResult> InlineCreateIdentity([DataSourceRequest]DataSourceRequest request, IdentityEntity model)
        {
            IResult result = Result.Failure;

            if (model != null && ModelState.IsValid)
            {
                var entity = new IdentityEntity
                {
                    EmailAddress = model.EmailAddress
                };

                result = await Identity.CreateIdentityAsync(entity);

                if (result.Succeeded)
                {
                    entity = result.OfType<IdentityEntity>().Single();

                    TryUpdateModel(entity, UpdateProperties, ValueProvider);
                }
            }

            return Json(request, result);
        }

        //
        // DELETE|POST: domain/identities/inline-delete
        [AcceptVerbs(HttpVerbs.Delete | HttpVerbs.Post), Route("inline-delete")]
        public async Task<JsonResult> InlineDestroyIdentity([DataSourceRequest]DataSourceRequest request, IdentityEntity identity)
        {
            IResult result = Result.Failure;

            if (identity != null && ModelState.IsValid)
            {
                result = await Identity.CreateIdentityAsync(identity);
            }

            return Json(request, result);
        }

        //
        // PATCH|PUT|POST: domain/identities/inline-update
        [AcceptVerbs(HttpVerbs.Patch | HttpVerbs.Put | HttpVerbs.Post), Route("inline-update")]
        public async Task<JsonResult> InlineUpdateIdentity([DataSourceRequest]DataSourceRequest request, Models.IdentityModel model)
        {
            IResult result = Result.Failure;

            if (model != null && ModelState.IsValid)
            {
                result = await Identity.UpdateIdentityAsync(model.EmailAddress, async (entity) =>
                {
                    await Identity.SetRolesAsync(entity, model.Roles);

                    return TryUpdateModel(entity, UpdateProperties, ValueProvider);
                });
            }

            return Json(request, result);
        }

        #endregion - Kendo UI inline edit -

        JsonResult Json(DataSourceRequest request, IResult result)
        {
            return Json(result.OfType<IdentityEntity>().ToDataSourceResult(request, ModelState));
        }

        JsonResult Json(DataSourceRequest request, IEnumerable<IResult> results)
        {
            return Json(results.SelectMany(result => result.OfType<IdentityEntity>()).ToDataSourceResult(request, ModelState));
        }
    }
}