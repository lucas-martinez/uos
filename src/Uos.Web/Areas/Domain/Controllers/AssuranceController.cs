﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Uos.Web.Areas.Domain.Controllers
{
    using Constants;

    [Authorize(Roles = RoleProfiles.Administration)]
    [RouteArea(AreaNames.Domain), RoutePrefix("assurance")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class AssuranceController : Controller
    {
        //
        // GET: domain/assurance
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}