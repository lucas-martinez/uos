﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Uos.Web.Areas.Domain.Controllers
{
    using Constants;

    [Authorize(Roles = RoleProfiles.Administration)]
    [RouteArea(AreaNames.Domain), RoutePrefix("configuration")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class ConfigurationController : Controller
    {
        //
        // GET: /domain/configuration
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}