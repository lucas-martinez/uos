﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Uos.Web.Areas.Domain.Controllers
{
    using Constants;

    [Authorize, RouteArea(AreaNames.Domain), RoutePrefix("staffer")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class StafferController : Controller
    {
        //
        // GET: /personnel/staffer
        [Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}