﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Domain.Controllers
{
    using Constants;
    using Web.Models;

    [Authorize]
    [RouteArea(AreaNames.Domain), RoutePrefix("roles")]
    public class RolesController : Controller
    {
        //
        // GET|POST: domain/roles
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post), Route("")]
        public JsonResult GetRoleNames(string startsWith, [Bind(Prefix = "filter")]FilterRequestModel model)
        {
            var roles = Resources.Domain.ResourceManager.ToDictionary<Roles>();

            if (startsWith == null && model != null && model.Filters != null)
            {
                startsWith = model.Filters.Where(f => f.Value != null).Select(f => f.Value).FirstOrDefault();
            }

            IEnumerable<string> names;

            if (startsWith == null) names = new string[0];
            else
            {
                //var result = await Role.GetRoleNamesAsync(startsWith);
                //names = result.OfType<string>();
                names = roles.Select(r => Convert.ToString(r.Key)).Where(r => r.StartsWith(startsWith));
            }
            
            return Json(names, JsonRequestBehavior.AllowGet);
        }

        JsonResult Json(DataSourceRequest request, IResult result)
        {
            return Json(result.OfType<StafferRoleEntity>().ToDataSourceResult(request, ModelState));
        }

        JsonResult Json(DataSourceRequest request, IEnumerable<IResult> results)
        {
            return Json(results.SelectMany(result => result.OfType<StafferRoleEntity>()).ToDataSourceResult(request, ModelState));
        }
    }
}