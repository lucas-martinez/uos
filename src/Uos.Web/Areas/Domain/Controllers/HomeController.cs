﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Uos.Web.Areas.Domain.Controllers
{
    using Constants;

    [Authorize]
    [RouteArea(AreaNames.Domain), RoutePrefix("")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class HomeController : Controller
    {
        // GET: domain/
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}