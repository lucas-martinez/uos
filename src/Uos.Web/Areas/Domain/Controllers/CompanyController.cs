﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Uos.Web.Areas.Domain.Controllers
{
    using Constants;

    [Authorize]
    [RouteArea(AreaNames.Domain), RoutePrefix("company")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class CompanyController : Controller
    {
        // GET: domain/company
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }

        // GET: domain/company/{code}
        [HttpGet, Route("{code}")]
        public ViewResult Index(string code)
        {
            return View();
        }

        // GET: domain/company/control-template
        [HttpGet, Route("control-template")]
        public PartialViewResult ControlTemplate()
        {
            return PartialView();
        }
    }
}