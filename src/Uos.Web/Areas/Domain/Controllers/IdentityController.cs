﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Uos.Web.Areas.Domain.Controllers
{
    using Constants;

    [Authorize]
    [RouteArea(AreaNames.Domain), RoutePrefix("identity")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class IdentityController : Controller
    {
        //
        // GET: domain/user
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }

        // GET: domain/user/control-template
        [HttpGet, Route("control-template")]
        public PartialViewResult ControlTemplate()
        {
            return PartialView();
        }
    }
}