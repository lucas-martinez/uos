﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Uos.Domain;

namespace Uos.Web.Areas.Domain.Models
{
    public class IdentityModel : IdentityHeaderModel
    {
        /// <summary>
        /// Street Address
        /// </summary>
        public string Address { get; set; }

        public DateTime? BirthDate { get; set; }

        public string Country { get; set; }

        public DateTime CreatedOn { get; set; }
        
        public bool EmailConfirmed { get; set; }

        public string HomePhone { get; set; }

        public string FirstName { get; set; }
        
        public string Language { get; set; }

        public string LastName { get; set; }

        /// <summary>
        /// Town or City
        /// </summary>
        public string Locality { get; set; }

        public bool LockedOut { get; set; }

        public string MobilePhone { get; set; }

        public IList<string> OtherPhones { get; set; }

        public string PasswordStrength { get; set; }

        public bool PhoneConfirmed { get; set; }
        
        public string PostalCode { get; set; }

        /// <summary>
        /// State or Province
        /// </summary>
        public string Region { get; set; }

        public IList<string> Roles { get; set; }
        
        public bool TwoFactorEnabled { get; set; }

        public IList<string> Urls { get; set; }
    }
}