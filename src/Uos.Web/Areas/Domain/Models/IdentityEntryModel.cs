﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Uos.Web.Areas.Domain.Models
{
    public class IdentityEntryModel : IdentityModel
    {
        public DateTime? Birthday { get; set; }
        
        public IList<string> Remarks { get; set; } 
    }
}