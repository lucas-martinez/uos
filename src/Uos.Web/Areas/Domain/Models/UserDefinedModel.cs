﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Domain.Models
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class UserDefinedModel
    {
        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "name")]
        public string PropertyName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "syntax")]
        public string PropertySyntax { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "Value")]
        public string FieldValue { get; set; }
    }
}
