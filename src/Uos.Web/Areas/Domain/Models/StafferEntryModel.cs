﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Uos.Web.Areas.Domain.Models
{
    public class StafferEntryModel : StafferModel
    {
        public IList<string> Remarks { get; set; }
    }
}