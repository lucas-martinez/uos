﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Uos.Domain;

namespace Uos.Web.Areas.Domain.Models
{
    public class CompanyModel : CompanyHeaderModel
    {
        [MaxLength(Lengths.CompanySize)]
        public string CompanySize { get; set; }

        [MaxLength(Lengths.CompanyType)]
        public string CompanyType { get; set; }

        [MaxLength(Lengths.Country)]
        public string Country { get; set; }

        [MaxLength(Lengths.Currency)]
        public string Currency { get; set; }

        public IList<string> Features { get; set; }

        [MaxLength(Lengths.Industry)]
        public string Industry { get; set; }

        [MaxLength(Lengths.MetricSystem)]
        public string MetricSystem { get; set; }
    }
}