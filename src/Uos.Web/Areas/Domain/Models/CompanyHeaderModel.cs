﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Uos.Domain;

namespace Uos.Web.Areas.Domain.Models
{
    public class CompanyHeaderModel
    {
        [MaxLength(Lengths.CompanyCode), Required]
        public string CompanyCode { get; set; }

        [MaxLength(Lengths.CompanyName)]
        public string CompanyName { get; set; }
    }
}