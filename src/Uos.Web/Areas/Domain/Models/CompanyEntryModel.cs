﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Uos.Domain;

namespace Uos.Web.Areas.Domain.Models
{
    public class CompanyEntryModel : CompanyModel
    {
        [MaxLength(Lengths.Remarks)]
        public IList<string> Remarks { get; set; }

        [MaxLength(Lengths.RoleName)]
        public string[] Roles { get; set; }
        
        [MaxLength(Lengths.EmailAddress)]
        public string[] Users { get; set; }
    }
}
