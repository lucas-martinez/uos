﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Uos.Web.Areas.Domain.Models
{
    public class IdentityHeaderModel
    {
        public string EmailAddress { get; set; }

        public string FullName { get; set; }
        
        public string PhoneNumber { get; set; }
    }
}