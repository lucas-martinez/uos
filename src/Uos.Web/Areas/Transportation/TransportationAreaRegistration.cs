﻿using System.Web.Mvc;

namespace Uos.Web.Areas.Transportation
{
    using Constants;
    using Controllers;

    public class TransportationAreaRegistration : AreaRegistration 
    {
        public override string AreaName
        {
            get { return AreaNames.Transpportation; }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.Routes.MapMvcAttributeRoutes();

            context.MapRoute(
                AreaNames.Transpportation.Replace('/', '_'),
                url: AreaNames.Transpportation + "/{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { typeof(HomeController).Namespace }
            );
        }
    }
}