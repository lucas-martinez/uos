﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Uos.Web.Areas.Transportation.Controllers
{
    using Constants;
    using Filters;

    [Authorize, Feature(Uos.Transportation.Features.Planning)]
    [RouteArea(AreaNames.Transpportation), RoutePrefix("")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class HomeController : Controller
    {
        //
        // GET: /transportation
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }

        //
        // GET: transportation/inbund
        [HttpGet, Route("inbound")]
        public ViewResult Inbound()
        {
            return View();
        }

        //
        // GET: transportation/outbound
        [HttpGet, Route("outbound")]
        public ViewResult Outbound()
        {
            return View();
        }

        //
        // GET: transportation/outsourced
        [HttpGet, Route("outsourced")]
        public ViewResult Outsourced()
        {
            return View();
        }
    }
}