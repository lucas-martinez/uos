﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Uos.Web.Areas.Transportation.Controllers
{
    using Constants;
    using Filters;

    [Authorize, Feature(Uos.Transportation.Features.Planning)]
    [RouteArea(AreaNames.Transpportation), RoutePrefix("master/locations")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class LocationsController : Controller
    {
        //
        // GET: /transportation/master/locations
        [HttpGet, Route("")]
        public JsonResult Index()
        {
            return Json(null);
        }
    }
}