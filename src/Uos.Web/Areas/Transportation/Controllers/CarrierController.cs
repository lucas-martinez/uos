﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;

namespace Uos.Web.Areas.Transportation.Controllers
{
    using Constants;
    using Filters;
    using Models;
    
    [Authorize/*(Roles = RoleProfiles.Transportation)*/]
    [Feature(Uos.Distribution.Features.Transportation)]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    [RouteArea(AreaNames.Distribution), RoutePrefix("transportation/master/carrier")]
    public class CarrierController : Controller
    {
        //
        // GET: /transportation/master/carrier
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}