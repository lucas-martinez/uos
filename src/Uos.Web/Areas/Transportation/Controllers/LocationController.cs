﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Uos.Web.Areas.Transportation.Controllers
{
    using Constants;
    using Filters;

    [Authorize, Feature(Uos.Transportation.Features.Planning)]
    [RouteArea(AreaNames.Transpportation), RoutePrefix("master/location")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class LocationController : Controller
    {
        //
        // GET: /transportation/master/location
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}