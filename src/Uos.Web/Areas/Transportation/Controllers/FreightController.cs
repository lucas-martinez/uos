﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Domain;
using Uos.Transportation;

namespace Uos.Web.Areas.Transportation.Controllers
{
    using Constants;
    using Filters;
    using Models;

    [Authorize, Feature(Uos.Transportation.Features.Planning)]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    [RouteArea(AreaNames.Transpportation), RoutePrefix("freight")]
    public class FreightController : Controller
    {
        //
        // POST: /transportation/freight
        [HttpPost, Route("")]
        public ViewResult Create(FreightCreateModel data)
        {
            var result = Freight.Create(new FreightEntity());

            return View<FreightViewModel>("Detail", "Entry", result);
        }

        // GET: logistics/distribution/transportation/freight/entry
        [HttpGet, Route("entry"), Authorize(Roles=CompanyRoles.Customizer)]
        public ViewResult Entry()
        {
            var result = Result.Ok(new FreightViewModel(), "");

            return View(result);
        }

        // GET: freight/{freightId}
        [HttpGet, Route("{freightId}")]
        public ViewResult Detail(int freightId)
        {
            var result = Result.Ok(new FreightViewModel());// Processes.Freight.Read(freightId);
            
            return View<FreightViewModel>(result);
        }

        // GET: logistics/distribution/transportation/freight/{freightId}/edit
        [HttpGet, Route("{freightId}/edit")]
        public ViewResult Edit(int freightId)
        {
            var result = Freight.Read(freightId);

            return View<FreightViewModel>(result);
        }

        // GET: logistics/distribution/transportation/freight
        [HttpGet, Route("")]
        public ViewResult Index(FreightSearchModel data)
        {
            var result = Result.Ok(new FreightSearchModel());
            //var result = Processes.Freight.Search(new FreightEntity());
            
            return View(result);
        }

        // POST: logistics/distribution/transportation/freight/search
        [HttpPost, Route("search")]
        public ViewResult Search(FreightSearchModel data)
        {
            var result = Freight.Search(new FreightEntity());
            
            return View("Index", result);
        }

        // PUT: logistics/distribution/transportation/freight/{freightId}
        [HttpPut, Route("{freightId}")]
        public ViewResult Update(int freightId, FreightUpdateModel data)
        {
            var result = Freight.Update(freightId, new FreightEntity());

            return View<FreightViewModel>("Detail", "Edit", result);
        }

        protected ViewResult View<T>(IResult result)
        {
            if (result is IResult<T>)
            {
                return View(result);
            }

            return View("Fail", result);
        }

        protected ViewResult View<T>(string viewName, IResult result)
        {
            if (result is IResult<T>)
            {
                return View(viewName);
            }

            return View("Fail", result);
        }

        protected ViewResult View<T>(string viewName, string failViewName, IResult result)
        {
            if (result is IResult<T>)
            {
                return View(result.Succeeded ? viewName : failViewName, result);
            }

            return View("Fail", result);
        }
    }
}