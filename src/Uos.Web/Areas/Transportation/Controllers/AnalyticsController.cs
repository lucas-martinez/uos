﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Uos.Transportation;

namespace Uos.Web.Areas.Transportation.Controllers
{
    using Constants;
    using Filters;

    [Authorize, Feature(Uos.Transportation.Features.Planning)]
    [RouteArea(AreaNames.Transpportation), RoutePrefix("analytics")]
    [HandleError(ExceptionType = typeof(Exception), Order = 500, View = "~/Views/Error/500")]
    public class AnalyticsController : Controller
    {
        //
        // GET: /transportation/analytics
        [HttpGet, Route("")]
        public ViewResult Index()
        {
            return View();
        }
    }
}