﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Transportation.Models
{
    public class FreightSearchModel
    {
        public int Entity { get; set; }

        public string Customer { get; set; }

        public string ShipLocation { get; set; }

        public string FreightId { get; set; }

        public string ShipDate { get; set; }

        public string Direction { get; set; }

        public string ProNumberFrom { get; set; }

        public string ProNumberTo { get; set; }

        public string BillDate { get; set; }

        public string BilledMilleage { get; set; }

        public string Status { get; set; }

        public string DeliveryDate { get; set; }

        public string RandMiles { get; set; }

        public string CreatedDate { get; set; }

        public string Pta { get; set; }

        public string CarrierScac { get; set; }

        public string Source { get; set; }

        public string CBM { get; set; }

        public string Type { get; set; }

        public string Weight { get; set; }

        public string BolNumber { get; set; }

        public string Terms { get; set; }

        public string Class { get; set; }

        public string Tote { get; set; }

        public string Currency { get; set; }

        public string BatchNumber { get; set; }

        public string Aged { get; set; }

        public string Difference { get; set; }

        public string PurchaseOrderNumber { get; set; }

        public string OrderNumber { get; set; }

        public string ResponsibleParty { get; set; }

        public string EquipNUmber { get; set; }

        public string Exception { get; set; }

        public string ExceptStatus { get; set; }

        public string ChargeType { get; set; }

        public bool? MissingAssigments { get; set; }

        public string DistCode { get; set; }

        public string CheckDate { get; set; }

        public string CheckNumber { get; set; }

        public string Attachments { get; set; }

        public string CustomerGl { get; set; }

        public string ContainerCount { get; set; }

        public string ContainerAverageWeight { get; set; }

        public Location Origin { get; set; }

        public Location Destination { get; set; }

        public Location Stop { get; set; }

        public class Location
        {
            public string Name { get; set; }

            public string City { get; set; }

            public string State { get; set; }

            public string PostalCode { get; set; }
        }

        public IResult Result { get; set; }
    }
}
