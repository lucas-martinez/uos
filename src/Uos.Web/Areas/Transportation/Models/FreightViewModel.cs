﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Areas.Transportation.Models
{
    public class FreightViewModel
    {
        public int FreightId { get; set; }

        public string BatchNumber { get; set; }
        
        public string CarrerNumber { get; set; }

        public string CompanyNumber { get; set; }

        public string Consignee { get; set; }

        public string ConsigneeAddress1 { get; set; }

        public string ConsigneeAddress2 { get; set; }

        public string ConsigneeeAddress3 { get; set; }

        public string ConsigneeCity { get; set; }

        public string ConsigneeState { get; set; }

        public string ConsigneePostalCode { get; set; }

        public Nullable<System.DateTime> DeliveryDate { get; set; }

        public Nullable<int> Direction { get; set; }

        public string DeclaredValue { get; set; }

        public string DistCode { get; set; }
        
        public string PaymentMethod { get; set; }

        public string Name { get; set; }

        public string CarrierName { get; set; }

        public string CarrierSCAC { get; set; }

        public bool Interline { get; set; }

        public string Exceptions { get; set; }

        public string ProNumber { get; set; }

        public string Invoice { get; set; }

        public Nullable<System.DateTime> InvoiceDate { get; set; }

        public string BLNumber { get; set; }

        public string CarrierAccount { get; set; }

        public string DeliveryEx { get; set; }

        public string Shipper { get; set; }

        public string ShipperAddress1 { get; set; }

        public string ShipperAddress2 { get; set; }

        public string ShipperCity { get; set; }

        public string ShipperState { get; set; }

        public string ShipperPostalCode { get; set; }
        
        public Nullable<System.DateTime> ShipDate { get; set; }

        public string SVC { get; set; }

        public string CommDesc { get; set; }

        public Nullable<bool> Hazardous { get; set; }
       
        public string PONumber { get; set; }

        public string TrailerNumber { get; set; }

        public string Remark { get; set; }

        public string GST { get; set; }

        public int? Pieces { get; set; }

        public int? FreightClass { get; set; }

        public decimal? Weight { get; set; }

        public decimal? BilledDiscount { get; set; }

        public Nullable<int> BilledMileage { get; set; }

        public string AuditorRemark { get; set; }

        public string CheckNumber { get; set; }

        public Nullable<System.DateTime> DatePaid { get; set; }

        public Nullable<decimal> AmountBilled { get; set; }

        public Nullable<decimal> AmountPaid { get; set; }

        public string CorrectionCode { get; set; }

        public Nullable<decimal> RateDisc { get; set; }

        public Nullable<int> RatedMileage { get; set; }
        
        public string ShipperAddress3 { get; set; }

        public string ShippingLocationID { get; set; }

        public Nullable<System.DateTime> InvoiceReceived { get; set; }

        public string POType { get; set; }

        public string PieceType { get; set; }

        public Nullable<int> HoursBilled { get; set; }

        public string SupplierCode { get; set; }
    }

    public static class FreightViewExtensions
    {
        /*public static FreightViewModel ToViewModel(this OLTPS.EntityFramework.FreightEntry entity)
        {
            var model = new FreightViewModel
            {
                FreightId = entity.RowID,
                BatchNumber = entity.BatchNumber,
                CompanyNumber = entity.CompanyNumber,
                CarrerNumber = entity.CarrerNumber,
                PaymentMethod = entity.PaymentMethod,
                Name = entity.Name,
                CarrierName = entity.CarrierName,
                CarrierSCAC = entity.CarrierSCAC,
                SVC = entity.SVC,
                Interline = entity.Interline,
                Exceptions = entity.Exceptions,
                ProNumber = entity.ProNumber,
                Invoice = entity.Invoice,
                InvoiceDate = entity.InvoiceDate,
                BLNumber = entity.BLNumber,
                CarrierAccount = entity.CarrierAccount,
                DeliveryEx = entity.DeliveryEx,
                Shipper = entity.Shipper,
                ShipperAddress1 = entity.ShipperAddress1,
                ShipperAddress2 = entity.ShipperAddress2,
                ShipperCity = entity.ShipperCity,
                ShipperState = entity.ShipperState,
                ShipperPostalCode = entity.ShipperPostalCode,
                Consignee = entity.Consignee,
                ConsigneeAddress1 = entity.ConsigneeAddress1,
                ConsigneeAddress2 = entity.ConsigneeAddress2,
                ConsigneeCity = entity.ConsigneeCity,
                ConsigneeState = entity.ConsigneeState,
                ConsigneePostalCode = entity.ConsigneePostalCode,
                DeliveryDate = entity.DeliveryDate,
                Direction = entity.Direction,
                ShipDate = entity.ShipDate,
                CommDesc = entity.CommDesc,
                Hazardous = entity.Hazardous,
                DeclaredValue = entity.DeclaredValue,
                DistCode = entity.DistCode,
                PONumber = entity.PONumber,
                TrailerNumber = entity.TrailerNumber,
                Remark = entity.Remark,
                GST = entity.GST,
                Pieces = entity.Pieces,
                FreightClass = entity.FreightClass,
                Weight = entity.Weight,
                BilledDiscount = entity.BilledDiscount,
                BilledMileage = entity.BilledMileage,
                AuditorRemark = entity.AuditorRemark,
                CheckNumber = entity.CheckNumber,
                DatePaid = entity.DatePaid,
                AmountBilled = entity.AmountBilled,
                AmountPaid = entity.AmountPaid,
                CorrectionCode = entity.CorrectionCode,
                RateDisc = entity.RateDisc,
                RatedMileage = entity.RatedMileage,
                ConsigneeeAddress3 = entity.CosigneeeAddress3,
                ShipperAddress3 = entity.ShipperAddress3,
                ShippingLocationID = entity.ShippingLocationID,
                InvoiceReceived = entity.InvoiceReceived,
                POType = entity.POType,
                PieceType = entity.PieceType,
                HoursBilled = entity.HoursBilled,
                SupplierCode = entity.SupplierCode
            };

            return model;
        }*/
    }
}