﻿using System.Web.Mvc;

namespace Uos.Web.Areas.Materials
{
    using Constants;
    using Controllers;

    public class MaterialsAreaRegistration : AreaRegistration 
    {
        public override string AreaName
        {
            get { return AreaNames.Materials; }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.Routes.MapMvcAttributeRoutes();

            context.MapRoute(
                AreaNames.Materials.Replace('/', '_'),
                url: AreaNames.Materials + "/{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { typeof(HomeController).Namespace }
            );
        }
    }
}