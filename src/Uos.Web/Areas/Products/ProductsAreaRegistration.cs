﻿using System.Web.Mvc;

namespace Uos.Web.Areas.Products
{
    using Constants;
    using Controllers;

    public class ProductsAreaRegistration : AreaRegistration 
    {
        public override string AreaName
        {
            get { return AreaNames.Products; }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.Routes.MapMvcAttributeRoutes();
            context.MapRoute(
                AreaNames.Products.Replace('/', '_'),
                url: AreaNames.Products + "/{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { typeof(HomeController).Namespace }
            );
        }
    }
}