﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Products
{
    /// <summary>
    /// Products Planning features
    /// </summary>
    public class Features
    {
        /// <summary>
        /// Assembly Orders
        /// </summary>
        public const string Assembly = @"PP-ATO";

        /// <summary>
        /// Basic Data
        /// </summary>
        public const string Basic = @"PP-BD";

        /// <summary>
        /// Capacity Requirement Planning
        /// </summary>
        public const string Capacity = @"PP-CRP";

        /// <summary>
        /// Information System
        /// </summary>
        public const string Information = @"PP-IS";

        /// <summary>
        /// Kanban/just-in-time
        /// </summary>
        public const string Kanban = @"PP-KAB";

        /// <summary>
        /// Respective Manufacturing
        /// </summary>
        public const string Manufacturing = @"PP-REM";

        /// <summary>
        /// Master Planning
        /// </summary>
        public const string Master = @"PP-MP";

        /// <summary>
        /// Sales and Operations Planning
        /// </summary>
        public const string Operations = @"PP-SOP";

        /// <summary>
        /// Production Orders
        /// </summary>
        public const string Orders = @"PP-SFO";

        /// <summary>
        /// Plant Data Collection
        /// </summary>
        public const string Plant = @"PP-PDC";

        /// <summary>
        /// Production Planning for Process Industries
        /// </summary>
        public const string Production = @"PP-PI";

        /// <summary>
        /// Material Requirement Planning
        /// </summary>
        public const string Requirement = @"PP-MRP";
    }
}
