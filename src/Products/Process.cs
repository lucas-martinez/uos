﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Products
{
    /// <summary>
    /// Process Order Management
    /// https://help.sap.com/saphelp_erp60_sp/helpdata/en/09/86bf53f106b44ce10000000a174cb4/content.htm?frameset=/en/89/a4335f461e11d182b50000e829fbfe/frameset.htm&current_toc=/en/dd/8bbf53f106b44ce10000000a174cb4/plain.htm&node_id=5&show_children=false
    /// </summary>
    public static class Process
    {
        public static void CloseOrder(ProcessDocument order)
        {
        }
        public static void CreateOrder(ProcessDocument order)
        {
        }

        public static void ReleaseOrder(ProcessDocument order)
        {
        }
    }
}
