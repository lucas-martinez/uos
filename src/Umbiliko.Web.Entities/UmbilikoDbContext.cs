﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Entities
{
    using Constants;
    using Data;
    using Persistence;
    using Persistence.EntityFramework;

    public class UmbilikoDbContext : DbContext
    {
        /*DbSet<AccountEntity> _accounts;
        DbSet<ActivityEntity> _activities;
        DbSet<AgentEntity> _agents;
        DbSet<AgentRequestEntity> _agentRequests;
        DbSet<AgentResultEntity> _agentResults;
        DbSet<ChargeEntity> _charges;
        DbSet<ClaimEntity> _claims;
        DbSet<ContractEntity> _consumers;
        DbSet<CountryEntity> _countries;
        DbSet<CultureEntity> _cultures;
        DbSet<DomainEntity> _domains;
        DbSet<FeatureEntity> _features;
        DbSet<IndicatorEntity> _indicators;
        DbSet<InstanceEntity> _instances;
        DbSet<LoginEntity> _logins;
        DbSet<CompanyEntity> _companies;
        DbSet<ProductEntity> _products;
        DbSet<RegionEntity> _regions;
        DbSet<SessionEntity> _sessions;
        DbSet<SubscriptionEntity> _subscriptions;
        DbSet<TextEntity> _texts;
        DbSet<ThemeEntity> _themes;
        DbSet<TokenEntity> _tokens;
        DbSet<SegmentEntity> _units;
        DbSet<UserEntity> _users;*/

        public UmbilikoDbContext()
        {
            //Database.Log = message => Debug.WriteLine(message);
        }

        public UmbilikoDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
            //Database.Log = message => Debug.WriteLine(message);
        }

        /*public virtual DbSet<AccountEntity> Accounts
        {
            get { return _accounts ?? (_accounts = Set<AccountEntity>()); }
            set { _accounts = value; }
        }

        public DbSet<ActivityEntity> Activities
        {
            get { return _activities ?? (_activities = Set<ActivityEntity>()); }
            set { _activities = value; }
        }

        public virtual DbSet<AgentEntity> Agents
        {
            get { return _agents ?? (_agents = Set<AgentEntity>()); }
            set { _agents = value; }
        }

        public virtual DbSet<AgentRequestEntity> AgentRequests
        {
            get { return _agentRequests ?? (_agentRequests = Set<AgentRequestEntity>()); }
            set { _agentRequests = value; }
        }

        public virtual DbSet<AgentResultEntity> AgentResults
        {
            get { return _agentResults ?? (_agentResults = Set<AgentResultEntity>()); }
            set { _agentResults = value; }
        }

        public virtual DbSet<ChargeEntity> Charges
        {
            get { return _charges ?? (_charges = Set<ChargeEntity>()); }
            set { _charges = value; }
        }

        public virtual DbSet<ClaimEntity> Claims
        {
            get { return _claims ?? (_claims = Set<ClaimEntity>()); }
            set { _claims = value; }
        }

        public virtual DbSet<CompanyEntity> Companies
        {
            get { return _companies ?? (_companies = Set<CompanyEntity>()); }
            set { _companies = value; }
        }

        public virtual DbSet<CountryEntity> Countries
        {
            get { return _countries ?? (_countries = Set<CountryEntity>()); }
            set { _countries = value; }
        }

        public virtual DbSet<CultureEntity> Cultures
        {
            get { return _cultures ?? (_cultures = Set<CultureEntity>()); }
            set { _cultures = value; }
        }

        public virtual DbSet<ContractEntity> Consumers
        {
            get { return _consumers ?? (_consumers = Set<ContractEntity>()); }
            set { _consumers = value; }
        }

        public virtual DbSet<DomainEntity> Domains
        {
            get { return _domains ?? (_domains = Set<DomainEntity>()); }
            set { _domains = value; }
        }

        public virtual DbSet<FeatureEntity> Features
        {
            get { return _features ?? (_features = Set<FeatureEntity>()); }
            set { _features = value; }
        }

        public virtual DbSet<IndicatorEntity> Indicators
        {
            get { return _indicators ?? (_indicators = Set<IndicatorEntity>()); }
            set { _indicators = value; }
        }

        public virtual DbSet<InstanceEntity> Instances
        {
            get { return _instances ?? (_instances = Set<InstanceEntity>()); }
            set { _instances = value; }
        }

        public virtual DbSet<LoginEntity> Logins
        {
            get { return _logins ?? (_logins = Set<LoginEntity>()); }
            set { _logins = value; }
        }

        public virtual DbSet<ProductEntity> Products
        {
            get { return _products ?? (_products = Set<ProductEntity>()); }
            set { _products = value; }
        }

        public virtual DbSet<RegionEntity> Regions
        {
            get { return _regions ?? (_regions = Set<RegionEntity>()); }
            set { _regions = value; }
        }

        public virtual DbSet<SessionEntity> Sessions
        {
            get { return _sessions ?? (_sessions = Set<SessionEntity>()); }
            set { _sessions = value; }
        }

        public virtual DbSet<SubscriptionEntity> Subscriptions
        {
            get { return _subscriptions ?? (_subscriptions = Set<SubscriptionEntity>()); }
            set { _subscriptions = value; }
        }

        public virtual DbSet<TextEntity> Texts
        {
            get { return _texts ?? (_texts = Set<TextEntity>()); }
            set { _texts = value; }
        }

        public virtual DbSet<ThemeEntity> Themes
        {
            get { return _themes ?? (_themes = Set<ThemeEntity>()); }
            set { _themes = value; }
        }

        public virtual DbSet<TokenEntity> Tokens
        {
            get { return _tokens ?? (_tokens = Set<TokenEntity>()); }
            set { _tokens = value; }
        }

        public virtual DbSet<SegmentEntity> Segments
        {
            get { return _units ?? (_units = Set<SegmentEntity>()); }
            set { _units = value; }
        }

        public virtual DbSet<UserEntity> Users
        {
            get { return _users ?? (_users = Set<UserEntity>()); }
            set { _users = value; }
        }*/

        public static UmbilikoDbContext Create()
        {
#if DEBUG
            return new UmbilikoDbContext(typeof(UmbilikoDbContext).Name);
            //return new UmbilikoDbContext("LocalConnection);
#else
            return new UmbilikoDbContext(typeof(UmbilikoDbContext).Name);
#endif
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<AccountEntity>()
                .HasCompoundKey(e => new { e.CompanyCode, e.SegmentCode, e.UserName })
                .HasIdentity(e => e.Id, dbName: "id")
                .HasOptional(e => e.ChangeUtc, dbName: "change")
                .HasRequired(e => e.Company, foreignKey: e => e.CompanyCode, many: e => e.Accounts, cascade: false)
                .HasRequired(e => e.CompanyCode, dbName: "company", maxLength: Lengths.CompanyCodeLength, unicode: false, fixedLength: true)
                .HasRequired(e => e.Roles, dbName: "roles", maxLength: Lengths.AccountRolesLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.Segment, foreignKey: e => new { e.CompanyCode, e.SegmentCode }, many: e => e.Accounts, cascade: true)
                .HasRequired(e => e.SegmentCode, dbName: "segment", maxLength: Lengths.SegmentCodeLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.CreateUtc, dbName: "create")
                .HasRequired(e => e.User, foreignKey: e => e.UserName, many: e => e.Accounts, cascade: true)
                .HasRequired(e => e.UserName, dbName: "user", maxLength: Lengths.UserNameLength, unicode: false, fixedLength: false)
                .ToTable(Tables.Accounts);

            modelBuilder.Entity<ActivityEntity>()
               .HasCompoundKey(e => new { e.CompanyCode, e.ProductName, e.FeatureName, e.ActivityName })
               .HasIdentity(e => e.Id, dbName: "id")
               .HasOptional(e => e.AccessLevel, dbName: "level", maxLength: Lengths.AccessLevelLength, unicode: false, fixedLength: true)
               .HasOptional(e => e.ActionName, dbName: "action", maxLength: Lengths.ActionNameLength, unicode: false, fixedLength: false)
               .HasRequired(e => e.ActivityName, dbName: "name", maxLength: Lengths.ActivityNameLength, unicode: false, fixedLength: false)
               .HasRequired(e => e.ActivityType, dbName: "type", maxLength: Lengths.ActivityTypeLength, unicode: false, fixedLength: true)
               .HasOptional(e => e.Attributes, dbName: "attributes", maxLength: Lengths.ActitivyAttributesLength, unicode: false, fixedLength: false)
               .HasRequired(e => e.Company, foreignKey: e => e.CompanyCode, many: e => e.Activities, cascade: false)
               .HasRequired(e => e.CompanyCode, dbName: "company", maxLength: Lengths.CompanyCodeLength, unicode: false, fixedLength: true)
               .HasOptional(e => e.ControllerName, dbName: "controller", maxLength: Lengths.ControllerNameLength, unicode: false, fixedLength: false)
               .HasRequired(e => e.Description, dbName: "description", maxLength: Lengths.ClientSecretLength, unicode: false, fixedLength: false)
               .HasRequired(e => e.Feature, foreignKey: e => new { e.CompanyCode, e.ProductName, e.FeatureName }, many: e => e.Activities)
               .HasRequired(e => e.FeatureName, dbName: "feature", maxLength: Lengths.FeatureNameLength, unicode: false, fixedLength: false)
               .HasOptional(e => e.HttpVerbs, dbName: "verbs", maxLength: Lengths.ActivityVerbsLength, unicode: false, fixedLength: false)
               .HasRequired(e => e.Product, foreignKey: e => new { e.CompanyCode, e.ProductName }, many: e => e.Activities, cascade: false)
               .HasRequired(e => e.ProductName, dbName: "product", maxLength: Lengths.ProductNameLength, unicode: false, fixedLength: false)
               .HasOptional(e => e.ResponseType, dbName: "response", maxLength: Lengths.ActivityResponseTypeLength, unicode: false, fixedLength: false)
               .HasRequired(e => e.Roles, dbName: "roles", maxLength: Lengths.ActivityRolesLength, unicode: false, fixedLength: false)
               .HasRequired(e => e.UpdateUtc, dbName: "update")
               .ToTable(Tables.Activities);

            modelBuilder.Entity<ChargeEntity>()
                .HasIdentityKey(e => e.SerialNumber)
                .HasRequired(e => e.Amount, dbName: "amount")
                .HasRequired(e => e.Consumer, foreignKey: e => new { e.Customer, e.SegmentCode, e.CompanyCode, e.ProductName }, many: e => e.Charges, cascade: true)
                .HasRequired(e => e.Company, foreignKey: e => e.CompanyCode, cascade: false)
                .HasRequired(e => e.CompanyCode, dbName: "vendor", maxLength: Lengths.CompanyCodeLength, unicode: false, fixedLength: true)
                .HasRequired(e => e.Currency, dbName: "currency", maxLength: Lengths.CurrencyCodeLength, unicode: false, fixedLength: true)
                .HasRequired(e => e.Customer, dbName: "customer", maxLength: Lengths.CompanyCodeLength, unicode: false, fixedLength: true)
                .HasRequired(e => e.Feature, foreignKey: e => new { e.CompanyCode, e.ProductName, e.FeatureName }, cascade: false)
                .HasRequired(e => e.FeatureName, dbName: "feature", maxLength: Lengths.FeatureNameLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.Period, dbName: "period")
                .HasRequired(e => e.Product, foreignKey: e => new { e.CompanyCode, e.ProductName }, cascade: false)
                .HasRequired(e => e.ProductName, dbName: "product", maxLength: Lengths.ProductNameLength, unicode: false, fixedLength: false)
                //.HasRequired(e => e.Segment, foreignKey: e => new { e.CompanyCode, e.SegmentCode }, cascade: false)
                .HasRequired(e => e.SegmentCode, dbName: "segment", maxLength: Lengths.CompanyCodeLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.ServiceDetail, "detail", maxLength: Lengths.DescriptionLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.ServiceUtc, "datetime", precision: null)
                .HasRequired(e => e.Subscription, foreignKey: e => new { e.Customer, e.SegmentCode, e.CompanyCode, e.ProductName, e.FeatureName }, many: e => e.Charges, cascade: false)
                .ToTable(Tables.Charges);

            modelBuilder.Entity<ClaimEntity>()
                .HasIdentityKey(e => e.Id, dbName: "id")
                .HasRequired(e => e.ClaimType, dbName: "type", maxLength: Lengths.ClaimTypeLength, unicode: true, fixedLength: false)
                .HasRequired(e => e.ClaimValue, dbName: "value", maxLength: Lengths.ClaimValueLength, unicode: true, fixedLength: false)
                .HasRequired(e => e.ClaimValueType, dbName: "meta", maxLength: Lengths.ClaimValueTypeLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.CreatedUtc, dbName: "created")
                .HasOptional(e => e.LoginKey, dbName: "key", maxLength: Lengths.LoginKeyLength, unicode: false, fixedLength: false)
                .HasOptional(e => e.LoginProvider, dbName: "provider", maxLength: Lengths.LoginProviderLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.User, foreignKey: e => e.UserName, cascade: false)
                .HasRequired(e => e.UserName, dbName: "user", maxLength: Lengths.UserNameLength, unicode: false, fixedLength: false)
                .ToTable(Tables.Claims);

            modelBuilder.Entity<CompanyEntity>()
               .HasPrimaryKey(e => e.CompanyCode, dbName: "company", maxLength: Lengths.CompanyCodeLength, unicode: false, fixedLength: true)
               .HasRequired(e => e.CompanyName, dbName: "name", maxLength: Lengths.CompanyNameLength, unicode: false, fixedLength: false)
               .HasRequired(e => e.CompanyType, dbName: "type", maxLength: Lengths.CompanyTypeLength, unicode: false, fixedLength: false)
               .ToTable(Tables.Companies);

            modelBuilder.Entity<ContractEntity>()
                .HasCompoundKey(e => new { e.CustomerCode, e.ConsumerCode, e.CompanyCode, e.ProductName })
                .HasIdentity(e => e.Id, dbName: "id")
                .HasRequired(e => e.Company, foreignKey: e => e.CompanyCode, many: e => e.Contracts, cascade: false)
                .HasRequired(e => e.CompanyCode, dbName: "company", maxLength: Lengths.CompanyCodeLength, unicode: false, fixedLength: true)
                .HasRequired(e => e.Consumer, foreignKey: e => new { e.CustomerCode, e.ConsumerCode }, cascade: false)
                .HasRequired(e => e.ConsumerCode, dbName: "consumer", maxLength: Lengths.SegmentCodeLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.ClientId, dbName: "clientId")
                .HasRequired(e => e.ClientSecret, dbName: "clientSecret", maxLength: Lengths.CustomerSecretLength, unicode: false, fixedLength: false)
                .HasOptional(e => e.CountryCode, dbName: "country", maxLength: Lengths.CountryCodeLength, unicode: false, fixedLength: true)
                .HasRequired(e => e.Customer, foreignKey: e => e.CustomerCode, many: e => e.Providers, cascade: false)
                .HasRequired(e => e.CustomerCode, dbName: "customer", maxLength: Lengths.CompanyCodeLength, unicode: false, fixedLength: true)
                .HasRequired(e => e.Credit, dbName: "credit")
                .HasRequired(e => e.CurrencyCode, dbName: "currency", maxLength: Lengths.CurrencyCodeLength, unicode: false, fixedLength: true)
                .HasRequired(e => e.Product, foreignKey: e => new { e.CompanyCode, e.ProductName }, cascade: false)
                .HasRequired(e => e.ProductName, dbName: "product", maxLength: Lengths.ProductNameLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.SinceUtc, dbName: "since")
                .ToTable(Tables.Contracts);

            modelBuilder.Entity<CountryEntity>()
               .HasPrimaryKey(e => e.CountryCode, dbName: "code", maxLength: Lengths.CountryCodeLength, unicode: false, fixedLength: true)
               .HasIdentity(e => e.Identifier, dbName: "id")
               .HasRequired(e => e.CountryName, dbName: "name", maxLength: Lengths.CountryNameLength, unicode: false, fixedLength: false)
               .HasRequired(e => e.DivisionType, dbName: "division", maxLength: Lengths.RegionTypeLength, unicode: false, fixedLength: true)
               .ToTable(Tables.Countries);

            modelBuilder.Entity<CultureEntity>()
               .HasPrimaryKey(e => e.CultureName, dbName: "name", maxLength: Lengths.CultureNameLength, unicode: false, fixedLength: false)
               .HasIdentity(e => e.Id, dbName: "id")
               .HasOptional(e => e.Country, foreignKey: e => e.CountryCode, cascade: true)
               .HasOptional(e => e.CountryCode, dbName: "country", maxLength: Lengths.CountryCodeLength, unicode: false, fixedLength: true)
               .HasOptional(e => e.TextReplace, dbName: "replace", maxLength: Lengths.CultureNameLength, unicode: false, fixedLength: false)
               .ToTable(Tables.Cultures);

            modelBuilder.Entity<DomainEntity>()
                .HasPrimaryKey(e => e.DomainName, dbName: "name", maxLength: Lengths.DomainNameLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.AllowExternalLogins, dbName: "external-logins")
                .HasRequired(e => e.Company, foreignKey: e => e.CompanyCode, many: e => e.Domains, cascade: false)
                .HasRequired(e => e.CompanyCode, dbName: "company", maxLength: Lengths.SegmentCodeLength, unicode: false, fixedLength: true)
                .HasOptional(e => e.Consumer, foreignKey: e => new { e.CustomerCode, e.ConsumerCode }, cascade: false)
                .HasOptional(e => e.ConsumerCode, dbName: "consumer", maxLength: Lengths.CompanyCodeLength, unicode: false, fixedLength: true)
                .HasOptional(e => e.Customer, foreignKey: e => e.CustomerCode, cascade: false)
                .HasOptional(e => e.CustomerCode, dbName: "customer", maxLength: Lengths.CompanyCodeLength, unicode: false, fixedLength: true)
                .HasRequired(e => e.Description, dbName: "description", maxLength: Lengths.ClientSecretLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.Environment, dbName: "environment", maxLength: Lengths.EnvironmentLength, unicode: false, fixedLength: true)
                .HasOptional(e => e.Product, foreignKey: e => new { e.CompanyCode, e.ProductName }, many: e => e.Domains, cascade: false)
                .HasOptional(e => e.ProductName, dbName: "product", maxLength: Lengths.ProductNameLength, unicode: false, fixedLength: false)
                .ToTable(Tables.Domains);

            modelBuilder.Entity<FeatureEntity>()
                .HasCompoundKey(e => new { e.CompanyCode, e.ProductName, e.FeatureName })
                .HasIdentity(e => e.Id, dbName: "id")
                .HasRequired(e => e.Company, foreignKey: e => e.CompanyCode, many: e => e.Features, cascade: false)
                .HasRequired(e => e.CompanyCode, dbName: "company", maxLength: Lengths.CompanyCodeLength, unicode: false, fixedLength: true)
                .HasOptional(e => e.Description, dbName: "description", maxLength: Lengths.DescriptionLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.FeatureName, dbName: "feature", maxLength: Lengths.FeatureNameLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.Product, foreignKey: e => new { e.CompanyCode, e.ProductName }, many: e => e.Features, cascade: true)
                .HasRequired(e => e.ProductName, dbName: "product", maxLength: Lengths.ProductNameLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.UpdateUtc, dbName: "update")
                .ToTable(Tables.Features);
            
            modelBuilder.Entity<IndicatorEntity>()
                .HasCompoundKey(e => new { e.AgentId, e.Timestamp })
                .HasRequired(e => e.Agent, foreignKey: e => e.AgentId, many: e => e.Indicators)
                .HasRequired(e => e.AgentId, dbName: "agent")
                .HasRequired(e => e.Days, dbName: "days", maxLength: Lengths.DaysLength, fixedLength: false)
                .HasRequired(e => e.Hours, dbName: "hours", maxLength: Lengths.HoursLength, fixedLength: false)
                .HasRequired(e => e.Milliseconds, dbName: "milliseconds", maxLength: Lengths.MillisecondsLength, fixedLength: false)
                .HasRequired(e => e.Minutes, dbName: "minutes", maxLength: Lengths.MinutesLength, fixedLength: false)
                .HasRequired(e => e.Seconds, dbName: "seconds", maxLength: Lengths.SecondsLength, fixedLength: false)
                .HasRequired(e => e.Timestamp, dbName: "timestamp")
                .ToTable(Tables.Indicators);

            modelBuilder.Entity<InstanceEntity>()
                .HasCompoundKey(e => new { e.CompanyCode, e.MachineName, e.ProcessId })
                .HasRequired(e => e.Id, dbName: "id")
                .HasRequired(e => e.Company, foreignKey: e => e.CompanyCode, cascade: false)
                .HasRequired(e => e.CompanyCode, dbName: "company", maxLength: Lengths.CompanyCodeLength, unicode: false, fixedLength: true)
                .HasRequired(e => e.Domain, foreignKey: e => e.DomainName, cascade: true)
                .HasRequired(e => e.DomainName, dbName: "domain", maxLength: Lengths.DomainNameLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.IpAddress, dbName: "ip", maxLength: Lengths.IpAddressLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.MachineName, dbName: "machine", maxLength: Lengths.MachineNameLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.ProcessId, dbName: "process")
                .HasRequired(e => e.ProductVersion, dbName: "version", maxLength: Lengths.VersionLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.UpdateUtc, dbName: "update")
                .ToTable(Tables.Instances);

            modelBuilder.Entity<LoginEntity>()
                .HasCompoundKey(e => new { e.LoginProvider, e.LoginKey })
                .HasIdentity(e => e.Id, dbName: "id")
                .HasOptional(e => e.ExpiresUtc, dbName: "expires", precision: null)
                .HasRequired(e => e.Label, dbName: "label", maxLength: Lengths.LoginLabelLength, unicode: true, fixedLength: false)
                .HasRequired(e => e.LoginConfirmed, dbName: "confirmed")
                .HasRequired(e => e.LoginCount, dbName: "logins")
                .HasRequired(e => e.LoginKey, dbName: "key", maxLength: Lengths.LoginKeyLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.LoginProvider, dbName: "provider", maxLength: Lengths.LoginProviderLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.LoginToken, dbName: "token", maxLength: Lengths.LoginTokenLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.LoginUtc, dbName: "login", precision: 0)
                .HasRequired(e => e.User, foreignKey: e => e.UserName, many: e => e.Logins, cascade: true)
                .HasRequired(e => e.UserName, dbName: "user", maxLength: Lengths.UserNameLength, unicode: false, fixedLength: false)
                .ToTable(Tables.Logins);

            modelBuilder.Entity<ProductEntity>()
                .HasCompoundKey(e => new { e.CompanyCode, e.ProductName })
                .HasIdentity(e => e.Id, dbName: "id")
                .HasOptional(e => e.AlphaVersion, dbName: "alpha", maxLength: Lengths.VersionLength, unicode: false, fixedLength: false)
                .HasOptional(e => e.BetaVersion, dbName: "beta", maxLength: Lengths.VersionLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.Company, foreignKey: e => e.CompanyCode, many: e => e.Products, cascade: true)
                .HasRequired(e => e.CompanyCode, dbName: "company", maxLength: Lengths.CompanyCodeLength, unicode: false, fixedLength: true)
                .HasRequired(e => e.ProductName, dbName: "name", maxLength: Lengths.ProductNameLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.Description, dbName: "description", maxLength: Lengths.DescriptionLength, unicode: false, fixedLength: false)
                .HasOptional(e => e.ReleaseVersion, dbName: "release", maxLength: Lengths.VersionLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.UpdateUtc, dbName: "update")
                .ToTable(Tables.Products);

            modelBuilder.Entity<RedirectEntity>()
                .HasCompoundKey(e => new { e.CompanyCode, e.ProductName, e.EventName, e.AccessLevel })
                .HasIdentity(e => e.Id, dbName: "id")
                .HasRequired(e => e.AccessLevel, dbName: "level", maxLength: Lengths.AccessLevelLength, unicode: false, fixedLength: true)
                .HasRequired(e => e.Activity, foreignKey: e => new { e.CompanyCode, e.ProductName, e.FeatureName, e.ActivityName }, cascade: false)
                .HasRequired(e => e.ActivityName, dbName: "activity", maxLength: Lengths.ActivityNameLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.Company, foreignKey: e => e.CompanyCode/*, many: e => e.Redirects*/, cascade: true)
                .HasRequired(e => e.CompanyCode, dbName: "company", maxLength: Lengths.CompanyCodeLength, unicode: false, fixedLength: true)
                .HasRequired(e => e.Feature, foreignKey: e => new { e.CompanyCode, e.ProductName, e.FeatureName }, cascade: false)
                .HasRequired(e => e.FeatureName, dbName: "feature", maxLength: Lengths.FeatureNameLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.Product, foreignKey: e => new { e.CompanyCode, e.ProductName }/*, many: e => e.Redirects*/, cascade: false)
                .HasRequired(e => e.ProductName, dbName: "product", maxLength: Lengths.ProductNameLength, unicode: false, fixedLength: false)
                .HasOptional(e => e.User, foreignKey: e => e.UserName/*, many: e => e.Redirects*/, cascade: true)
                .HasOptional(e => e.UserName, dbName: "user", maxLength: Lengths.UserNameLength, unicode: false, fixedLength: false)
                .ToTable(Tables.Redirects);

            modelBuilder.Entity<RegionEntity>()
               .HasPrimaryKey(e => e.RegionId, dbName: "id")
               .HasRequired(e => e.Country, foreignKey: e => e.CountryCode)
               .HasRequired(e => e.CountryCode, dbName: "country", maxLength: Lengths.CountryCodeLength, unicode: false, fixedLength: true)
               .HasOptional(e => e.Culture, foreignKey: e => e.CultureName, cascade: false)
               .HasOptional(e => e.CultureName, dbName: "culture", maxLength: Lengths.CultureNameLength, unicode: false, fixedLength: false)
               .HasRequired(e => e.DivisionType, dbName: "division", maxLength: Lengths.RegionTypeLength, unicode: false, fixedLength: true)
               .HasRequired(e => e.RegionName, dbName: "name", maxLength: Lengths.RegionNameLength, unicode: false, fixedLength: false)
               .HasOptional(e => e.RegionParent, foreignKey: e => e.RegionParentId, many: e => e.RegionChildren)
               .HasOptional(e => e.RegionParentId, dbName: "parent")
               .HasRequired(e => e.RegionType, dbName: "type", maxLength: Lengths.RegionTypeLength, unicode: false, fixedLength: true)
               .ToTable(Tables.Regions);

            modelBuilder.Entity<RobotEntity>()
                .HasPrimaryKey(e => e.Id, dbName: "id")
                .HasRequired(e => e.Company, foreignKey: e => e.CompanyCode, /*many: e => e.Robots,*/ cascade: false)
                .HasRequired(e => e.CompanyCode, dbName: "company", maxLength: Lengths.CompanyCodeLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.MachineName, dbName: "machine", maxLength: Lengths.MachineNameLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.ProcessId, dbName: "process")
                .HasRequired(e => e.TasksCompleted, dbName: "completed")
                .HasRequired(e => e.TasksPending, dbName: "pending")
                .HasRequired(e => e.TasksRunning, dbName: "running")
                .ToTable(Tables.Robots);

            modelBuilder.Entity<RobotRequestEntity>()
                .HasKey(e => new { e.RobotId, e.Task, e.RequestUtc })
                .HasRequired(e => e.StatusCode, dbName: "status", maxLength: Lengths.StatusCodeLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.Robot, foreignKey: e => e.RobotId, many: e => e.Requests)
                .HasRequired(e => e.RobotId, dbName: "robot")
                .HasRequired(e => e.RequestData, dbName: "data", maxLength: Lengths.RequestDataLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.RequestUtc, dbName: "date")
                .ToTable(Tables.Requests);

            modelBuilder.Entity<RobotResultEntity>()
                .HasKey(e => new { e.RobotId, e.Task, e.StartedUtc })
                .HasRequired(e => e.Robot, foreignKey: e => e.RobotId, many: e => e.Results)
                .HasRequired(e => e.RobotId, dbName: "robot")
                .HasRequired(e => e.ResultData, dbName: "data", maxLength: Lengths.ResultDataLength, unicode: false, fixedLength: false)
                .ToTable(Tables.Results);

            modelBuilder.Entity<SegmentEntity>()
                .HasCompoundKey(e => new { e.CompanyCode, e.SegmentCode })
                .HasIdentity(e => e.Id, dbName: "id")
                .HasRequired(e => e.Company, foreignKey: e => e.CompanyCode, many: e => e.Segments, cascade: false)
                .HasRequired(e => e.CompanyCode, dbName: "company", maxLength: Lengths.CompanyCodeLength, unicode: false, fixedLength: true)
                .HasRequired(e => e.Description, dbName: "description", maxLength: Lengths.ClientSecretLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.Environment, dbName: "environment", maxLength: Lengths.EnvironmentLength, unicode: false, fixedLength: true)
                .HasRequired(e => e.SegmentCode, dbName: "code", maxLength: Lengths.CompanyCodeLength, unicode: false, fixedLength: false)
                .ToTable(Tables.Segments);

            modelBuilder.Entity<SessionEntity>()
                .HasPrimaryKey(e => e.Id, dbName: "id")
                .HasRequired(e => e.AccessToken, maxLength: Lengths.TokenValueLength, unicode: false, fixedLength: false)
                .HasOptional(e => e.Account, foreignKey: e => new { e.CompanyCode, e.SegmentCode, e.UserName }, many: e => e.Sessions, cascade: false)
                .HasOptional(e => e.Company, foreignKey: e => e.CompanyCode, many: e => e.Sessions, cascade: false)
                .HasOptional(e => e.CompanyCode, dbName: "company", maxLength: Lengths.CompanyCodeLength, unicode: false, fixedLength: true)
                .HasRequired(e => e.CreatedUtc, "create", precision: null)
                .HasRequired(e => e.Domain, foreignKey: e => e.DomainName, /*many: e => e.Sessions, */cascade: false)
                .HasRequired(e => e.DomainName, dbName: "domain", maxLength: Lengths.DomainNameLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.IpAddress, dbName: "ip", maxLength: Lengths.IpAddressLength, unicode: false, fixedLength: false)
                .HasOptional(e => e.ExpiresUtc, dbName: "expire", precision: null)
                .HasOptional(e => e.Fingerprint, dbName: "fingerprint", maxLength: Lengths.FingerprintLength, unicode: false, fixedLength: false)
                .HasOptional(e => e.Segment, foreignKey: e => new { e.CompanyCode, e.SegmentCode }, many: e => e.Sessions, cascade: false)
                .HasOptional(e => e.SegmentCode, dbName: "segment", maxLength: Lengths.SegmentCodeLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.User, foreignKey: e => e.UserName, many: e => e.Sessions, cascade: false)
                .HasRequired(e => e.UserName, dbName: "user", maxLength: Lengths.UserNameLength, unicode: false, fixedLength: false)
                .ToTable(Tables.Sessions);

            modelBuilder.Entity<SubscriptionEntity>()
               .HasCompoundKey(e => new { e.CustomerCode, e.ConsumerCode, e.CompanyCode, e.ProductName, e.FeatureName })
               .HasIdentity(e => e.Id, dbName: "id")
               .HasRequired(e => e.Company, foreignKey: e => e.CompanyCode, /*many: e => e.Consumers,*/ cascade: false)
               .HasRequired(e => e.CompanyCode, dbName: "company", maxLength: Lengths.CompanyCodeLength, unicode: false, fixedLength: true)
               .HasRequired(e => e.Consumer, foreignKey: e => new { e.CustomerCode, e.ConsumerCode }, many: e => e.Subscriptions, cascade: true)
               .HasRequired(e => e.ConsumerCode, dbName: "consumer", maxLength: Lengths.SegmentCodeLength, unicode: false, fixedLength: false)
               .HasRequired(e => e.CustomerCode, dbName: "customer", maxLength: Lengths.CompanyCodeLength, unicode: false, fixedLength: true)
               .HasRequired(e => e.Feature, foreignKey: e => new { e.CompanyCode, e.ProductName, e.FeatureName }, many: e => e.Subscriptions, cascade: false)
               .HasRequired(e => e.FeatureName, dbName: "feature", maxLength: Lengths.FeatureNameLength, unicode: false, fixedLength: false)
               .HasRequired(e => e.Product, foreignKey: e => new { e.CompanyCode, e.ProductName }, many: e => e.Subscriptions, cascade: false)
               .HasRequired(e => e.ProductName, dbName: "product", maxLength: Lengths.ProductNameLength, unicode: false, fixedLength: false)
               .HasRequired(e => e.SinceUtc, dbName: "since")
               .HasRequired(e => e.Status, dbName: "status", maxLength: Lengths.SubscriptionStatusLength, unicode: false, fixedLength: true)
               .HasOptional(e => e.UntilUtc, dbName: "until")
               .ToTable(Tables.Subscriptions);

            modelBuilder.Entity<ThemeEntity>()
                .HasCompoundKey(e => new { e.CompanyCode, e.SegmentCode, e.ThemeName })
                .HasIdentity(e => e.Identifier, dbName: "id")
                .HasRequired(e => e.CompanyCode, dbName: "company", maxLength: Lengths.CompanyCodeLength, unicode: false, fixedLength: true)
                .HasRequired(e => e.Segment, foreignKey: e => new { e.CompanyCode, e.SegmentCode })
                .HasRequired(e => e.SegmentCode, dbName: "segment", maxLength: Lengths.SegmentCodeLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.Company, foreignKey: e => e.CompanyCode, cascade: true)
                .HasRequired(e => e.ThemeName, dbName: "theme", maxLength: Lengths.ThemeNameLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.ThemeData, dbName: "data", maxLength: Lengths.ThemeDataLength, fixedLength: false)
                .ToTable(Tables.Themes);

            modelBuilder.Entity<TextEntity>()
                .HasCompoundKey(e => new { e.CompanyCode, e.ProductName, e.FeatureName, e.CultureName, e.TextName })
                .HasIdentity(e => e.Identifier, dbName: "id")
                .HasRequired(e => e.Company, foreignKey: e => e.CompanyCode, cascade: false)
                .HasRequired(e => e.CompanyCode, dbName: "company", maxLength: Lengths.CompanyCodeLength, unicode: false, fixedLength: true)
                .HasRequired(e => e.Culture, foreignKey: e => e.CultureName, many: e => e.Texts, cascade: true)
                .HasRequired(e => e.CultureName, dbName: "culture", maxLength: Lengths.CultureNameLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.Feature, foreignKey: e => new { e.CompanyCode, e.ProductName, e.FeatureName }, many: e => e.Texts, cascade: true)
                .HasRequired(e => e.FeatureName, dbName: "feature", maxLength: Lengths.FeatureNameLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.Product, foreignKey: e => new { e.CompanyCode, e.ProductName }, many: e => e.Texts, cascade: false)
                .HasRequired(e => e.ProductName, dbName: "product", maxLength: Lengths.ProductNameLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.TextName, dbName: "name", maxLength: Lengths.TextNameLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.TextValue, dbName: "value", maxLength: Lengths.TextValueLength, unicode: true, fixedLength: false)
                .ToTable(Tables.Texts);

            modelBuilder.Entity<TokenEntity>()
               .HasPrimaryKey(e => e.TokenValue, dbName: "value", maxLength: Lengths.TokenValueLength, unicode: false, fixedLength: false)
               .HasRequired(e => e.Company, foreignKey: e => e.CompanyCode, cascade: true)
               .HasRequired(e => e.CompanyCode, dbName: "company", maxLength: Lengths.CompanyCodeLength, unicode: false, fixedLength: true)
               .HasRequired(e => e.Purpose, dbName: "purpose")
               .HasRequired(e => e.Segment, foreignKey: e => new { e.CompanyCode, e.SegmentCode })
               .HasRequired(e => e.SegmentCode, dbName: "segment", maxLength: Lengths.SegmentCodeLength, unicode: false, fixedLength: false)
               .HasOptional(e => e.ExpiresUtc, dbName: "expire", precision: null)
               .HasRequired(e => e.IssuedUtc, dbName: "issue", precision: null)
               .HasRequired(e => e.User, foreignKey: e => e.UserName)
               .HasRequired(e => e.UserName, dbName: "user", maxLength: Lengths.UserNameLength, unicode: false, fixedLength: false)
               .ToTable(Tables.Tokens);

            modelBuilder.Entity<UserEntity>()
                .HasPrimaryKey(e => e.UserName, dbName: "user", maxLength: Lengths.UserNameLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.AccessFailedCount, dbName:  "fails")
                .HasOptional(e => e.ChangeUtc, dbName: "change")
                .HasRequired(e => e.CreatedUtc, dbName: "create")
                .HasOptional(e => e.EmailAddress, dbName: "email", maxLength: Lengths.EmailAddressLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.EmailConfirmed, dbName: "email_confirmed")
                .HasRequired(e => e.LockoutEnabled, dbName: "lockout_enabled")
                .HasOptional(e => e.LockoutEndDateUtc, dbName: "lockout_end")
                .HasOptional(e => e.PasswordHash, dbName: "password", maxLength: Lengths.PasswordHashLength, unicode: false, fixedLength: false)
                .HasOptional(e => e.PasswordStrength, dbName: "password_strength", maxLength: Lengths.PasswordStrengthLength, unicode: false, fixedLength: false)
                .HasOptional(e => e.PhoneNumber, dbName: "phone", maxLength: Lengths.PhoneNumberLength, unicode: false, fixedLength: false)
                .HasRequired(e => e.PhoneNumberConfirmed, dbName: "phone_confirmed")
                .HasRequired(e => e.UserStatus, dbName: "status")
                .ToTable(Tables.Users);

            //base.OnModelCreating(modelBuilder);
        }
    }
}
