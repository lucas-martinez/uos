﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web
{
    using Contracts;
    using Entities;
    
    public class LoginContext : EntityContext<LoginEntity>
    {
        UserContext _user;

        public LoginContext(LoginEntity entity, DbContext dbContext)
            : base(entity, dbContext)
        {
        }

        public LoginContext(LoginEntity entity, UserContext user)
            : base(entity, user.DbContext)
        {
            _user = user;
        }

        public string Issuer
        {
            get { return Entity.LoginProvider; }
        }

        public string NameIdentifier
        {
            get { return string.Join("_", Entity.LoginProvider, Entity.LoginKey); }
        }

        public UserContext User
        {
            get { return _user; }
        }

        public static LoginContext Create(UserContext user, string loginProvider, string loginKey)
        {
            var dbContext = user.DbContext;

            var dbSet = dbContext.Set<LoginEntity>();

            var entity = dbSet.Create();

            entity.LoginProvider = loginProvider;
            entity.LoginKey = loginKey;
            entity.User = user.Entity;
            entity.LoginConfirmed = false;
            entity.UserName = user.Name;

            dbSet.Add(entity);

            return new LoginContext(entity, user);
        }

        public static async Task<LoginContext> FindAsync(DbContext dbContex, string loginProvider, string loginKey)
        {
            var dbSet = dbContex.Set<LoginEntity>();

            var entity = await dbSet.Include(e => e.User).SingleOrDefaultAsync(e => e.LoginProvider == loginProvider && e.LoginKey == loginKey);

            return FromEntity(entity, dbContex);
        }

        public static LoginContext FromEntity(LoginEntity entity, DbContext dbContext)
        {
            if (entity.IsNull()) return null;

            if (entity.User != null) return new LoginContext(entity, UserContext.FromEntity(entity.User, dbContext));
            
            return new LoginContext(entity, dbContext);
        }

        public LoginContext Update(UserContext user, bool isConfirmed = true)
        {
            Entity.User = user;
            Entity.LoginConfirmed = isConfirmed;
            Entity.UserName = user.Name;

            return this;
        }

        /*public LoginContext Update(string email = null, string familyName = null, string gender = null, string givenName = null, string name = null, string profile = null)
        {
            if (email != null) Entity.EmailAddress = email;

            if (familyName != null) Entity.FamilyName = familyName;

            if (gender != null) Entity.Gender = gender;

            if (givenName != null) Entity.GivenName = givenName;

            if (name != null) Entity.Name = name;

            //if (profile != null) Entity.Profile = profile;

            return this;
        }*/
    }
}
