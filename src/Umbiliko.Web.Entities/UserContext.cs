﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web
{
    using Constants;
    using Contracts;
    using Entities;
    using Services;
    
    public class UserContext : EntityContext<UserEntity>
    {
        public UserContext(UserEntity entity, DbContext dbContext)
            : base(entity, dbContext)
        {
        }

        public string Email
        {
            get { return Entity.EmailAddress; }
        }

        public string Hash
        {
            get { return Entity.PasswordHash; }
        }

        public bool IsLockedOut
        {
            get { return Entity.LockoutEndDateUtc.HasValue && Entity.LockoutEndDateUtc.Value >= DateTime.UtcNow; }
        }

        public string Name
        {
            get { return Entity.UserName; }
        }

        public string PhoneNumber
        {
            get { return Entity.PhoneNumber; }
        }

        public bool TwoFactorEnabled
        {
            get { return Entity.TwoFactorEnabled; }
        }

        public string UserId
        {
            get { return Name; }
        }

        public AccountContext AddAccount(SegmentContext segment, params string[] roles)
        {
            return AccountContext.Create(segment, this, roles);
        }

        public async Task<AccountContext> AddOrUpdateAccountAsync(SegmentContext segment, params string[] roles)
        {
            var account = await AccountContext.FindAsync(segment, this);

            if (account != null)
            {
                account.Update(roles);
            }
            else
            {
                account = AccountContext.Create(segment, this, roles);
            }

            return account;
        }

        public static UserContext Create(DbContext dbContext, string name)
        {
            var dbSet = dbContext.Set<UserEntity>();

            var entity = dbSet.Add(dbSet.Create());

            entity.UserStatus = UserStatuses.Initial;
            entity.UserName = name;

            return new UserContext(entity, dbContext);
        }

        public static UserContext Find(DbContext dbContext, string userName)
        {
            var dbSet = dbContext.Set<UserEntity>();

            var entity = dbSet.Find(userName);

            return entity != null ? new UserContext(entity, dbContext) : null;
        }

        public static UserContext Find(DbContext dbContext, UserInfo data)
        {
            if (data == null) return null;

            return Find(dbContext, data.Name);
        }

        public static async Task<UserContext> FindAsync(DbContext dbContext, string userName)
        {
            if (userName == null) return null;

            userName = userName.ToLowerInvariant();

            var dbSet = dbContext.Set<UserEntity>();

            var entity = await dbSet.FindAsync(userName);

            return entity != null ? new UserContext(entity, dbContext) : null;
        }

        public static async Task<UserContext> FindAsync(DbContext dbContext, string loginProvider, string loginSecret)
        {
            if (loginProvider == null || loginSecret == null) return null;

            var dbSet = dbContext.Set<LoginEntity>();

            var entity = await dbSet.Include(e => e.User).SingleOrDefaultAsync(e => e.LoginProvider == loginProvider && e.LoginKey == loginSecret);

            return entity.IsNull() ? new UserContext(entity.User, dbContext) : null;
        }

        public static async Task<UserContext> FindByEmailAsync(DbContext dbContext, string email)
        {
            if (email == null) return null;

            email = email.ToLowerInvariant();

            var dbSet = dbContext.Set<UserEntity>();

            var entity = await dbSet.SingleOrDefaultAsync(e => e.EmailAddress == email);

            return entity != null ? new UserContext(entity, dbContext) : null;
        }

        public static UserContext FromEntity(UserEntity entity, DbContext dbContext)
        {
            return entity != null ? new UserContext(entity, dbContext) : null;
        }

        public static List<UserEntity> Select(DbContext dbContext, params string[] names)
        {
            var dbSet = dbContext.Set<UserEntity>();

            switch (names.Length)
            {
                case 0:
                    return new List<UserEntity> { };

                case 1:
                    var name = names.First();
                    var entity = dbSet.Find(name);
                    return entity != null ? new List<UserEntity> { entity } : null;

                default:
                    return dbSet.Where(e => names.Contains(e.UserName)).ToList();
            }
        }

        public static async Task<List<UserEntity>> SelectAsync(DbContext dbContext, params string[] names)
        {
            var dbSet = dbContext.Set<UserEntity>();

            switch (names.Length)
            {
                case 0:
                    return new List<UserEntity> { };

                case 1:
                    var name = names.First();
                    var entity = await dbSet.FindAsync(name);
                    return entity != null ? new List<UserEntity> { entity } : null;

                default:
                    return await dbSet.Where(e => names.Contains(e.UserName)).ToListAsync();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <param name="phoneNumber"></param>
        /// <param name="password">string.Empty to set to null</param>
        public void Update(string email = null, string phoneNumber = null, string password = null)
        {
            if (email != null) Entity.EmailAddress = email;

            if (phoneNumber != null) Entity.PhoneNumber = phoneNumber;

            if (password == null)
            {
            }
            else if (password == string.Empty)
            {
                Entity.PasswordHash = null;
            }
            else
            {
                var hasher = new PasswordHasher();

                Entity.PasswordHash = hasher.HashPassword(password);
            }
        }
        
        public bool VerifyPassword(string password)
        {
            var hash = Hash;

            var hasher = new PasswordHasher();

            return hash != null && hasher.VerifyHashedPassword(hash, password);
        }

        public static implicit operator string (UserContext context)
        {
            return context != null ? context.Entity.UserName : null;
        }
    }
}
