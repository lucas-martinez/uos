﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web
{
    using Constants;
    using Entities;

    public class ConsumerContext : EntityContext<ContractEntity>
    {
        readonly ProductContext _product;

        public ConsumerContext(ContractEntity entity, ProductContext product)
            : base(entity, product.DbContext)
        {
            _product = product;
        }

        public ProductContext Product
        {
            get { return _product; }
        }

        public static ConsumerContext Create(ProductContext product, SegmentContext segment, decimal credit, string country = null, string currency = Currencies.USD)
        {
            var dbContext = product.DbContext;

            var dbSet = dbContext.Set<ContractEntity>();

            var entity = dbSet.Add(dbSet.Create());
            
            entity.ConsumerCode = segment;
            entity.CountryCode = country;
            entity.Credit = credit;
            entity.CurrencyCode = currency;
            entity.Customer = segment.Company;
            entity.CustomerCode = segment.Company;
            entity.Product = product;
            entity.ProductName = product;
            entity.Consumer = segment;
            entity.Company = product.Company;
            entity.CompanyCode = product.Company;

            product.Entity.Consumers.Add(entity);
            
            var context = new ConsumerContext(entity, product);

            context.GenerateConsumerKey();

            context.GenerateConsumerSecret();

            return context;
        }

        public static ConsumerContext Find(ProductContext product, string companyCode, string segmentCode)
        {
            if (product == null) return null;

            var dbContext = product.DbContext;

            var dbSet = dbContext.Set<ContractEntity>();

            var entity = dbSet.Find(companyCode, segmentCode, product.Entity.CompanyCode, product.Entity.ProductName);

            return entity != null ? new ConsumerContext(entity, product) : null;
        }

        public static async Task<ConsumerContext> FindAsync(ProductContext product, string companyCode, string segmentCode)
        {
            if (product == null) return null;

            var dbContext = product.DbContext;

            var dbSet = dbContext.Set<ContractEntity>();

            var entity = await dbSet.FindAsync(companyCode, segmentCode, product.Entity.CompanyCode, product.Entity.ProductName);

            return entity != null ? new ConsumerContext(entity, product) : null;
        }

        public static ConsumerContext FromEntity(ContractEntity entity, DbContext dbContext)
        {
            return new ConsumerContext(entity, ProductContext.FromEntity(entity.Product, dbContext));
        }

        public Guid GenerateConsumerKey()
        {
            return Entity.ClientId = Guid.NewGuid();
        }

        public string GenerateConsumerSecret()
        {
            var buffer = new byte[32];

            Buffer.BlockCopy(Guid.NewGuid().ToByteArray(), 0, buffer, 0, 16);
            Buffer.BlockCopy(Guid.NewGuid().ToByteArray(), 0, buffer, 16, 16);

            var base64 = Convert.ToBase64String(buffer);

            return Entity.ClientSecret = base64;
        }

        public void Update(decimal? credit, string country = null, string currency = null)
        {
            if (credit.HasValue) Entity.Credit = credit.Value;

            if (country != null) Entity.CountryCode = country;

            if (currency != null) Entity.CurrencyCode = currency;
        }
    }
}
