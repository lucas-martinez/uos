﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web
{
    using Constants;
    using Entities;

    public class SegmentContext : EntityContext<SegmentEntity>
    {
        readonly CompanyContext _company;

        public SegmentContext(SegmentEntity entity, CompanyContext company)
            : base(entity, company.DbContext)
        {
            _company = company;
        }

        public CompanyContext Company
        {
            get { return _company; }
        }

        public AccountContext AddAccount(UserContext user, params string[] roles)
        {
            return AccountContext.Create(this, user, roles);
        }

        public async Task<AccountContext> AddOrUpdateAccountAsync(UserContext user, params string[] roles)
        {
            var account = await AccountContext.FindAsync(this, user);

            if (account != null)
            {
                account.Update(roles: roles);
            }
            else
            {
                account = AddAccount(user, roles);
            }

            return account;
        }

        public static SegmentContext Create(CompanyContext company, string segment, string environment = Environments.Production, string description = null)
        {
            var dbContext = company.DbContext;

            var dbSet = dbContext.Set<SegmentEntity>();

            var entity = dbSet.Add(dbSet.Create());
            
            entity.SegmentCode = segment;
            entity.Company = company;
            entity.Description = description;
            entity.Environment = environment;
            entity.CompanyCode = company;

            company.Entity.Segments.Add(entity);

            return new SegmentContext(entity, company);
        }

        public static ICollection<SegmentContext> Find(CompanyContext company)
        {
            if (company == null) return null;

            var dbContext = company.DbContext;

            var dbSet = dbContext.Set<SegmentEntity>();

            string companyCode = company;

            var entities = dbSet.Where(e => e.CompanyCode == companyCode).ToList();

            return entities.Select(entity => new SegmentContext(entity, company)).ToArray();
        }

        public static SegmentContext Find(CompanyContext company, string client)
        {
            if (company == null) return null;

            var dbContext = company.DbContext;

            var dbSet = dbContext.Set<SegmentEntity>();
            
            var entity = dbSet.Find(company.Entity.CompanyCode, client);

            return entity != null ? new SegmentContext(entity, company) : null;
        }

        public static SegmentContext Find(CompanyContext company, int identifier)
        {
            if (company == null) return null;

            var dbContext = company.DbContext;

            var dbSet = dbContext.Set<SegmentEntity>();

            string companyCode = company;

            var entity = dbSet.SingleOrDefault(e => e.Id == identifier && e.CompanyCode == companyCode);

            return entity != null ? new SegmentContext(entity, company) : null;
        }

        public static ICollection<SegmentContext> Find(CompanyContext company, params int[] identifiers)
        {
            if (company == null) return null;

            var dbContext = company.DbContext;

            var dbSet = dbContext.Set<SegmentEntity>();

            string companyCode = company;

            var entities = dbSet.Where(e => identifiers.Contains(e.Id) && e.CompanyCode == companyCode).ToList();

            return entities.Select(entity => new SegmentContext(entity, company)).ToArray();
        }

        public static async Task<ICollection<SegmentContext>> FindAsync(CompanyContext company)
        {
            if (company == null) return null;

            var dbContext = company.DbContext;

            var dbSet = dbContext.Set<SegmentEntity>();

            string companyCode = company;

            var entities = await dbSet.Where(e => e.CompanyCode == companyCode).ToListAsync();

            return entities.Select(entity => new SegmentContext(entity, company)).ToArray();
        }

        public static async Task<SegmentContext> FindAsync(CompanyContext company, string client)
        {
            if (company == null) return null;

            var dbContext = company.DbContext;

            var dbSet = dbContext.Set<SegmentEntity>();
            
            var entity = await dbSet.FindAsync(company.Entity.CompanyCode, client);

            return entity != null ? new SegmentContext(entity, company) : null;
        }

        public static async Task<SegmentContext> FindAsync(CompanyContext company, int identifier)
        {
            if (company == null) return null;

            var dbContext = company.DbContext;

            var dbSet = dbContext.Set<SegmentEntity>();

            string companyCode = company;

            var entity = await dbSet.SingleOrDefaultAsync(e => e.Id == identifier && e.CompanyCode == companyCode);

            return entity != null ? new SegmentContext(entity, company) : null;
        }

        public static async Task<ICollection<SegmentContext>> FindAsync(CompanyContext company, params int[] identifiers)
        {
            if (company == null) return null;

            var dbContext = company.DbContext;

            var dbSet = dbContext.Set<SegmentEntity>();

            string companyCode = company;

            var entities = await dbSet.Where(e => identifiers.Contains(e.Id) && e.CompanyCode == companyCode).ToListAsync();

            return entities.Select(entity => new SegmentContext(entity, company)).ToArray();
        }

        public static SegmentContext FromEntity(SegmentEntity entity, DbContext dbContext)
        {
            return entity != null ? new SegmentContext(entity, CompanyContext.FromEntity(entity.Company, dbContext)) : null;
        }

        public void Update(string environment = null, string description = null)
        {
            if (environment != null) Entity.Environment = environment;

            if (description != null) Entity.Description = description;
        }

        public static implicit operator string(SegmentContext context)
        {
            return context != null ? context.Entity.SegmentCode : null;
        }
    }
}
