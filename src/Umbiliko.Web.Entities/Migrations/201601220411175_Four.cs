namespace Uos.Web.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Four : DbMigration
    {
        public override void Up()
        {
            AddColumn("uko.sessions", "fingerprint", c => c.String(maxLength: 127, unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("uko.sessions", "fingerprint");
        }
    }
}
