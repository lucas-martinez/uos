namespace Uos.Web.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Three : DbMigration
    {
        public override void Up()
        {
            DropIndex("uko.domains", new[] { "company", "product" });
            AlterColumn("uko.domains", "product", c => c.String(maxLength: 20, unicode: false));
            CreateIndex("uko.domains", new[] { "company", "product" });
        }
        
        public override void Down()
        {
            DropIndex("uko.domains", new[] { "company", "product" });
            AlterColumn("uko.domains", "product", c => c.String(nullable: false, maxLength: 20, unicode: false));
            CreateIndex("uko.domains", new[] { "company", "product" });
        }
    }
}
