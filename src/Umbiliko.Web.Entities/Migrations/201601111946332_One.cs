namespace Uos.Web.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class One : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "uko.accounts",
                c => new
                    {
                        company = c.String(nullable: false, maxLength: 4, fixedLength: true, unicode: false),
                        segment = c.String(nullable: false, maxLength: 4, unicode: false),
                        user = c.String(nullable: false, maxLength: 40, unicode: false),
                        change = c.DateTime(),
                        create = c.DateTime(nullable: false),
                        id = c.Int(nullable: false, identity: true),
                        roles = c.String(nullable: false, maxLength: 255, unicode: false),
                    })
                .PrimaryKey(t => new { t.company, t.segment, t.user })
                .ForeignKey("uko.companies", t => t.company)
                .ForeignKey("uko.segments", t => new { t.company, t.segment }, cascadeDelete: true)
                .ForeignKey("uko.users", t => t.user, cascadeDelete: true)
                .Index(t => t.company)
                .Index(t => new { t.company, t.segment })
                .Index(t => t.user);
            
            CreateTable(
                "uko.companies",
                c => new
                    {
                        company = c.String(nullable: false, maxLength: 4, fixedLength: true, unicode: false),
                        name = c.String(nullable: false, maxLength: 20, unicode: false),
                        type = c.String(nullable: false, maxLength: 3, unicode: false),
                    })
                .PrimaryKey(t => t.company);
            
            CreateTable(
                "uko.activities",
                c => new
                    {
                        company = c.String(nullable: false, maxLength: 4, fixedLength: true, unicode: false),
                        product = c.String(nullable: false, maxLength: 20, unicode: false),
                        feature = c.String(nullable: false, maxLength: 20, unicode: false),
                        name = c.String(nullable: false, maxLength: 40, unicode: false),
                        level = c.String(maxLength: 3, fixedLength: true, unicode: false),
                        type = c.String(nullable: false, maxLength: 3, fixedLength: true, unicode: false),
                        action = c.String(maxLength: 40, unicode: false),
                        attributes = c.String(maxLength: 255, unicode: false),
                        controller = c.String(maxLength: 40, unicode: false),
                        description = c.String(nullable: false, maxLength: 128, unicode: false),
                        verbs = c.String(maxLength: 40, unicode: false),
                        id = c.Int(nullable: false, identity: true),
                        response = c.String(maxLength: 40, unicode: false),
                        roles = c.String(nullable: false, maxLength: 255, unicode: false),
                        update = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => new { t.company, t.product, t.feature, t.name })
                .ForeignKey("uko.companies", t => t.company)
                .ForeignKey("uko.features", t => new { t.company, t.product, t.feature }, cascadeDelete: true)
                .ForeignKey("uko.products", t => new { t.company, t.product })
                .Index(t => t.company)
                .Index(t => new { t.company, t.product, t.feature });
            
            CreateTable(
                "uko.features",
                c => new
                    {
                        company = c.String(nullable: false, maxLength: 4, fixedLength: true, unicode: false),
                        product = c.String(nullable: false, maxLength: 20, unicode: false),
                        feature = c.String(nullable: false, maxLength: 20, unicode: false),
                        description = c.String(maxLength: 255, unicode: false),
                        id = c.Int(nullable: false, identity: true),
                        update = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => new { t.company, t.product, t.feature })
                .ForeignKey("uko.companies", t => t.company)
                .ForeignKey("uko.products", t => new { t.company, t.product }, cascadeDelete: true)
                .Index(t => t.company)
                .Index(t => new { t.company, t.product });
            
            CreateTable(
                "uko.products",
                c => new
                    {
                        company = c.String(nullable: false, maxLength: 4, fixedLength: true, unicode: false),
                        name = c.String(nullable: false, maxLength: 20, unicode: false),
                        alpha = c.String(maxLength: 20, unicode: false),
                        beta = c.String(maxLength: 20, unicode: false),
                        description = c.String(nullable: false, maxLength: 255, unicode: false),
                        id = c.Int(nullable: false, identity: true),
                        release = c.String(maxLength: 20, unicode: false),
                        update = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => new { t.company, t.name })
                .ForeignKey("uko.companies", t => t.company, cascadeDelete: true)
                .Index(t => t.company);
            
            CreateTable(
                "uko.contracts",
                c => new
                    {
                        customer = c.String(nullable: false, maxLength: 4, fixedLength: true, unicode: false),
                        consumer = c.String(nullable: false, maxLength: 4, unicode: false),
                        company = c.String(nullable: false, maxLength: 4, fixedLength: true, unicode: false),
                        product = c.String(nullable: false, maxLength: 20, unicode: false),
                        key = c.Guid(nullable: false),
                        secret = c.String(nullable: false, maxLength: 255, unicode: false),
                        credit = c.Decimal(nullable: false, precision: 18, scale: 2),
                        country = c.String(maxLength: 3, fixedLength: true, unicode: false),
                        currency = c.String(nullable: false, maxLength: 3, fixedLength: true, unicode: false),
                        id = c.Int(nullable: false, identity: true),
                        since = c.DateTime(nullable: false),
                        ProductEntity_CompanyCode = c.String(maxLength: 4, fixedLength: true, unicode: false),
                        ProductEntity_ProductName = c.String(maxLength: 20, unicode: false),
                    })
                .PrimaryKey(t => new { t.customer, t.consumer, t.company, t.product })
                .ForeignKey("uko.companies", t => t.company)
                .ForeignKey("uko.segments", t => new { t.customer, t.consumer })
                .ForeignKey("uko.companies", t => t.customer)
                .ForeignKey("uko.products", t => new { t.company, t.product })
                .ForeignKey("uko.products", t => new { t.ProductEntity_CompanyCode, t.ProductEntity_ProductName })
                .Index(t => new { t.customer, t.consumer })
                .Index(t => t.company)
                .Index(t => new { t.company, t.product })
                .Index(t => new { t.ProductEntity_CompanyCode, t.ProductEntity_ProductName });
            
            CreateTable(
                "uko.charges",
                c => new
                    {
                        SerialNumber = c.Long(nullable: false, identity: true),
                        amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        segment = c.String(nullable: false, maxLength: 4, unicode: false),
                        currency = c.String(nullable: false, maxLength: 3, fixedLength: true, unicode: false),
                        customer = c.String(nullable: false, maxLength: 4, fixedLength: true, unicode: false),
                        feature = c.String(nullable: false, maxLength: 20, unicode: false),
                        period = c.Byte(nullable: false),
                        product = c.String(nullable: false, maxLength: 20, unicode: false),
                        detail = c.String(nullable: false, maxLength: 255, unicode: false),
                        datetime = c.DateTime(nullable: false),
                        vendor = c.String(nullable: false, maxLength: 4, fixedLength: true, unicode: false),
                    })
                .PrimaryKey(t => t.SerialNumber)
                .ForeignKey("uko.companies", t => t.vendor)
                .ForeignKey("uko.contracts", t => new { t.customer, t.segment, t.vendor, t.product }, cascadeDelete: true)
                .ForeignKey("uko.features", t => new { t.vendor, t.product, t.feature })
                .ForeignKey("uko.products", t => new { t.vendor, t.product })
                .ForeignKey("uko.subscriptions", t => new { t.customer, t.segment, t.vendor, t.product, t.feature })
                .Index(t => new { t.customer, t.segment, t.vendor, t.product })
                .Index(t => new { t.customer, t.segment, t.vendor, t.product, t.feature })
                .Index(t => new { t.vendor, t.product, t.feature })
                .Index(t => t.vendor);
            
            CreateTable(
                "uko.subscriptions",
                c => new
                    {
                        customer = c.String(nullable: false, maxLength: 4, fixedLength: true, unicode: false),
                        consumer = c.String(nullable: false, maxLength: 4, unicode: false),
                        company = c.String(nullable: false, maxLength: 4, fixedLength: true, unicode: false),
                        product = c.String(nullable: false, maxLength: 20, unicode: false),
                        feature = c.String(nullable: false, maxLength: 20, unicode: false),
                        id = c.Int(nullable: false, identity: true),
                        since = c.DateTime(nullable: false),
                        status = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
                        until = c.DateTime(),
                        Customer_CompanyCode = c.String(maxLength: 4, fixedLength: true, unicode: false),
                    })
                .PrimaryKey(t => new { t.customer, t.consumer, t.company, t.product, t.feature })
                .ForeignKey("uko.companies", t => t.company)
                .ForeignKey("uko.segments", t => new { t.customer, t.consumer }, cascadeDelete: true)
                .ForeignKey("uko.companies", t => t.Customer_CompanyCode)
                .ForeignKey("uko.features", t => new { t.company, t.product, t.feature })
                .ForeignKey("uko.products", t => new { t.company, t.product })
                .ForeignKey("uko.contracts", t => new { t.customer, t.consumer, t.company, t.product }, cascadeDelete: true)
                .Index(t => new { t.customer, t.consumer })
                .Index(t => new { t.customer, t.consumer, t.company, t.product })
                .Index(t => t.company)
                .Index(t => new { t.company, t.product, t.feature })
                .Index(t => t.Customer_CompanyCode);
            
            CreateTable(
                "uko.segments",
                c => new
                    {
                        company = c.String(nullable: false, maxLength: 4, fixedLength: true, unicode: false),
                        code = c.String(nullable: false, maxLength: 4, unicode: false),
                        ChangedUtc = c.DateTime(nullable: false),
                        CreatedUtc = c.DateTime(nullable: false),
                        description = c.String(nullable: false, maxLength: 128, unicode: false),
                        environment = c.String(nullable: false, maxLength: 3, fixedLength: true, unicode: false),
                        id = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => new { t.company, t.code })
                .ForeignKey("uko.companies", t => t.company)
                .Index(t => t.company);
            
            CreateTable(
                "uko.sessions",
                c => new
                    {
                        id = c.Guid(nullable: false),
                        AccessToken = c.String(nullable: false, maxLength: 255, unicode: false),
                        company = c.String(maxLength: 4, fixedLength: true, unicode: false),
                        create = c.DateTime(nullable: false),
                        domain = c.String(nullable: false, maxLength: 127, unicode: false),
                        expire = c.DateTime(),
                        ip = c.String(nullable: false, maxLength: 20, unicode: false),
                        segment = c.String(maxLength: 4, unicode: false),
                        user = c.String(nullable: false, maxLength: 40, unicode: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("uko.accounts", t => new { t.company, t.segment, t.user })
                .ForeignKey("uko.companies", t => t.company)
                .ForeignKey("uko.domains", t => t.domain)
                .ForeignKey("uko.segments", t => new { t.company, t.segment })
                .ForeignKey("uko.users", t => t.user)
                .Index(t => new { t.company, t.segment, t.user })
                .Index(t => t.domain);
            
            CreateTable(
                "uko.domains",
                c => new
                    {
                        name = c.String(nullable: false, maxLength: 127, unicode: false),
                        externallogins = c.Boolean(name: "external-logins", nullable: false),
                        consumer = c.String(maxLength: 4, unicode: false),
                        customer = c.String(maxLength: 4, fixedLength: true, unicode: false),
                        description = c.String(nullable: false, maxLength: 128, unicode: false),
                        environment = c.String(nullable: false, maxLength: 3, fixedLength: true, unicode: false),
                        product = c.String(nullable: false, maxLength: 20, unicode: false),
                        company = c.String(nullable: false, maxLength: 4, fixedLength: true, unicode: false),
                    })
                .PrimaryKey(t => t.name)
                .ForeignKey("uko.companies", t => t.company)
                .ForeignKey("uko.segments", t => new { t.customer, t.consumer })
                .ForeignKey("uko.companies", t => t.customer)
                .ForeignKey("uko.products", t => new { t.company, t.product })
                .Index(t => new { t.customer, t.consumer })
                .Index(t => new { t.company, t.product })
                .Index(t => t.company);
            
            CreateTable(
                "uko.instances",
                c => new
                    {
                        company = c.String(nullable: false, maxLength: 4, fixedLength: true, unicode: false),
                        machine = c.String(nullable: false, maxLength: 20, unicode: false),
                        process = c.Int(nullable: false),
                        domain = c.String(nullable: false, maxLength: 127, unicode: false),
                        id = c.Guid(nullable: false),
                        ip = c.String(nullable: false, maxLength: 20, unicode: false),
                        version = c.String(nullable: false, maxLength: 20, unicode: false),
                        update = c.DateTime(nullable: false),
                        DomainEntity_DomainName = c.String(maxLength: 127, unicode: false),
                    })
                .PrimaryKey(t => new { t.company, t.machine, t.process })
                .ForeignKey("uko.companies", t => t.company)
                .ForeignKey("uko.domains", t => t.domain, cascadeDelete: true)
                .ForeignKey("uko.domains", t => t.DomainEntity_DomainName)
                .Index(t => t.company)
                .Index(t => t.domain)
                .Index(t => t.DomainEntity_DomainName);
            
            CreateTable(
                "uko.users",
                c => new
                    {
                        user = c.String(nullable: false, maxLength: 40, unicode: false),
                        fails = c.Int(nullable: false),
                        email = c.String(maxLength: 80, unicode: false),
                        email_confirmed = c.Boolean(nullable: false),
                        change = c.DateTime(),
                        create = c.DateTime(nullable: false),
                        lockout_enabled = c.Boolean(nullable: false),
                        lockout_end = c.DateTime(),
                        phone = c.String(maxLength: 20, unicode: false),
                        phone_confirmed = c.Boolean(nullable: false),
                        password_strength = c.String(maxLength: 5, unicode: false),
                        password = c.String(maxLength: 127, unicode: false),
                        SecurityStamp = c.String(),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        status = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.user);
            
            CreateTable(
                "uko.claims",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        type = c.String(nullable: false, maxLength: 127),
                        value = c.String(nullable: false, maxLength: 127),
                        meta = c.String(nullable: false, maxLength: 127, unicode: false),
                        created = c.DateTime(nullable: false),
                        key = c.String(maxLength: 127, unicode: false),
                        provider = c.String(maxLength: 20, unicode: false),
                        user = c.String(nullable: false, maxLength: 40, unicode: false),
                        UserEntity_UserName = c.String(maxLength: 40, unicode: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("uko.users", t => t.user)
                .ForeignKey("uko.users", t => t.UserEntity_UserName)
                .Index(t => t.user)
                .Index(t => t.UserEntity_UserName);
            
            CreateTable(
                "uko.logins",
                c => new
                    {
                        provider = c.String(nullable: false, maxLength: 20, unicode: false),
                        key = c.String(nullable: false, maxLength: 127, unicode: false),
                        expires = c.DateTime(),
                        label = c.String(nullable: false, maxLength: 127),
                        confirmed = c.Boolean(nullable: false),
                        logins = c.Int(nullable: false),
                        token = c.String(nullable: false, maxLength: 255, unicode: false),
                        login = c.DateTime(nullable: false),
                        id = c.Int(nullable: false, identity: true),
                        user = c.String(nullable: false, maxLength: 40, unicode: false),
                    })
                .PrimaryKey(t => new { t.provider, t.key })
                .ForeignKey("uko.users", t => t.user, cascadeDelete: true)
                .Index(t => t.user);
            
            CreateTable(
                "uko.texts",
                c => new
                    {
                        company = c.String(nullable: false, maxLength: 4, fixedLength: true, unicode: false),
                        product = c.String(nullable: false, maxLength: 20, unicode: false),
                        feature = c.String(nullable: false, maxLength: 20, unicode: false),
                        culture = c.String(nullable: false, maxLength: 20, unicode: false),
                        name = c.String(nullable: false, maxLength: 20, unicode: false),
                        id = c.Int(nullable: false, identity: true),
                        value = c.String(nullable: false),
                    })
                .PrimaryKey(t => new { t.company, t.product, t.feature, t.culture, t.name })
                .ForeignKey("uko.companies", t => t.company)
                .ForeignKey("uko.cultures", t => t.culture, cascadeDelete: true)
                .ForeignKey("uko.features", t => new { t.company, t.product, t.feature }, cascadeDelete: true)
                .ForeignKey("uko.products", t => new { t.company, t.product })
                .Index(t => t.company)
                .Index(t => new { t.company, t.product, t.feature })
                .Index(t => t.culture);
            
            CreateTable(
                "uko.cultures",
                c => new
                    {
                        name = c.String(nullable: false, maxLength: 20, unicode: false),
                        country = c.String(maxLength: 3, fixedLength: true, unicode: false),
                        id = c.Int(nullable: false, identity: true),
                        replace = c.String(maxLength: 20, unicode: false),
                    })
                .PrimaryKey(t => t.name)
                .ForeignKey("uko.countries", t => t.country, cascadeDelete: true)
                .Index(t => t.country);
            
            CreateTable(
                "uko.countries",
                c => new
                    {
                        code = c.String(nullable: false, maxLength: 3, fixedLength: true, unicode: false),
                        name = c.String(nullable: false, maxLength: 20, unicode: false),
                        division = c.String(nullable: false, maxLength: 3, fixedLength: true, unicode: false),
                        id = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.code);
            
            CreateTable(
                "uko.agents",
                c => new
                    {
                        id = c.Guid(nullable: false),
                        secret = c.String(maxLength: 128, unicode: false),
                        CustomerNumber = c.Int(),
                        machine = c.String(nullable: false, maxLength: 20, unicode: false),
                        process = c.Int(nullable: false),
                        completed = c.Long(nullable: false),
                        pending = c.Long(nullable: false),
                        running = c.Long(nullable: false),
                        Customer_CompanyCode = c.String(maxLength: 4, fixedLength: true, unicode: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("uko.companies", t => t.Customer_CompanyCode)
                .Index(t => t.Customer_CompanyCode);
            
            CreateTable(
                "uko.indicators",
                c => new
                    {
                        agent = c.Guid(nullable: false),
                        timestamp = c.Int(nullable: false),
                        days = c.Binary(nullable: false, maxLength: 20),
                        hours = c.Binary(nullable: false, maxLength: 20),
                        milliseconds = c.Binary(nullable: false, maxLength: 20),
                        minutes = c.Binary(nullable: false, maxLength: 20),
                        seconds = c.Binary(nullable: false, maxLength: 20),
                    })
                .PrimaryKey(t => new { t.agent, t.timestamp })
                .ForeignKey("uko.agents", t => t.agent, cascadeDelete: true)
                .Index(t => t.agent);
            
            CreateTable(
                "uko.requests",
                c => new
                    {
                        agent = c.Guid(nullable: false),
                        Task = c.Long(nullable: false),
                        date = c.DateTime(nullable: false),
                        data = c.String(nullable: false, unicode: false),
                    })
                .PrimaryKey(t => new { t.agent, t.Task, t.date })
                .ForeignKey("uko.agents", t => t.agent, cascadeDelete: true)
                .Index(t => t.agent);
            
            CreateTable(
                "uko.results",
                c => new
                    {
                        agent = c.Guid(nullable: false),
                        Task = c.Long(nullable: false),
                        StartedUtc = c.DateTime(nullable: false),
                        CompletedUtc = c.DateTime(),
                        status = c.String(nullable: false, maxLength: 20, unicode: false),
                        summary = c.String(nullable: false, unicode: false),
                    })
                .PrimaryKey(t => new { t.agent, t.Task, t.StartedUtc })
                .ForeignKey("uko.agents", t => t.agent, cascadeDelete: true)
                .Index(t => t.agent);
            
            CreateTable(
                "uko.redirects",
                c => new
                    {
                        company = c.String(nullable: false, maxLength: 4, fixedLength: true, unicode: false),
                        product = c.String(nullable: false, maxLength: 20, unicode: false),
                        EventName = c.String(nullable: false, maxLength: 128),
                        level = c.String(nullable: false, maxLength: 3, fixedLength: true, unicode: false),
                        activity = c.String(nullable: false, maxLength: 40, unicode: false),
                        feature = c.String(nullable: false, maxLength: 20, unicode: false),
                        id = c.Int(nullable: false, identity: true),
                        user = c.String(maxLength: 40, unicode: false),
                    })
                .PrimaryKey(t => new { t.company, t.product, t.EventName, t.level })
                .ForeignKey("uko.activities", t => new { t.company, t.product, t.feature, t.activity })
                .ForeignKey("uko.companies", t => t.company, cascadeDelete: true)
                .ForeignKey("uko.features", t => new { t.company, t.product, t.feature })
                .ForeignKey("uko.products", t => new { t.company, t.product })
                .ForeignKey("uko.users", t => t.user, cascadeDelete: true)
                .Index(t => new { t.company, t.product, t.feature, t.activity })
                .Index(t => t.user);
            
            CreateTable(
                "uko.regions",
                c => new
                    {
                        id = c.Guid(nullable: false),
                        country = c.String(nullable: false, maxLength: 3, fixedLength: true, unicode: false),
                        culture = c.String(maxLength: 20, unicode: false),
                        division = c.String(nullable: false, maxLength: 3, fixedLength: true, unicode: false),
                        name = c.String(nullable: false, maxLength: 20, unicode: false),
                        parent = c.Guid(),
                        type = c.String(nullable: false, maxLength: 3, fixedLength: true, unicode: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("uko.countries", t => t.country, cascadeDelete: true)
                .ForeignKey("uko.cultures", t => t.culture)
                .ForeignKey("uko.regions", t => t.parent)
                .Index(t => t.country)
                .Index(t => t.culture)
                .Index(t => t.parent);
            
            CreateTable(
                "uko.themes",
                c => new
                    {
                        company = c.String(nullable: false, maxLength: 4, fixedLength: true, unicode: false),
                        segment = c.String(nullable: false, maxLength: 4, unicode: false),
                        theme = c.String(nullable: false, maxLength: 20, unicode: false),
                        id = c.Int(nullable: false, identity: true),
                        data = c.Binary(nullable: false),
                    })
                .PrimaryKey(t => new { t.company, t.segment, t.theme })
                .ForeignKey("uko.companies", t => t.company, cascadeDelete: true)
                .ForeignKey("uko.segments", t => new { t.company, t.segment }, cascadeDelete: true)
                .Index(t => t.company)
                .Index(t => new { t.company, t.segment });
            
            CreateTable(
                "uko.tokens",
                c => new
                    {
                        value = c.String(nullable: false, maxLength: 255, unicode: false),
                        segment = c.String(nullable: false, maxLength: 4, unicode: false),
                        company = c.String(nullable: false, maxLength: 4, fixedLength: true, unicode: false),
                        expire = c.DateTime(),
                        issue = c.DateTime(nullable: false),
                        purpose = c.Byte(nullable: false),
                        user = c.String(nullable: false, maxLength: 40, unicode: false),
                    })
                .PrimaryKey(t => t.value)
                .ForeignKey("uko.companies", t => t.company, cascadeDelete: true)
                .ForeignKey("uko.segments", t => new { t.company, t.segment }, cascadeDelete: true)
                .ForeignKey("uko.users", t => t.user, cascadeDelete: true)
                .Index(t => new { t.company, t.segment })
                .Index(t => t.company)
                .Index(t => t.user);
            
        }
        
        public override void Down()
        {
            DropForeignKey("uko.tokens", "user", "uko.users");
            DropForeignKey("uko.tokens", new[] { "company", "segment" }, "uko.segments");
            DropForeignKey("uko.tokens", "company", "uko.companies");
            DropForeignKey("uko.themes", new[] { "company", "segment" }, "uko.segments");
            DropForeignKey("uko.themes", "company", "uko.companies");
            DropForeignKey("uko.regions", "parent", "uko.regions");
            DropForeignKey("uko.regions", "culture", "uko.cultures");
            DropForeignKey("uko.regions", "country", "uko.countries");
            DropForeignKey("uko.redirects", "user", "uko.users");
            DropForeignKey("uko.redirects", new[] { "company", "product" }, "uko.products");
            DropForeignKey("uko.redirects", new[] { "company", "product", "feature" }, "uko.features");
            DropForeignKey("uko.redirects", "company", "uko.companies");
            DropForeignKey("uko.redirects", new[] { "company", "product", "feature", "activity" }, "uko.activities");
            DropForeignKey("uko.results", "agent", "uko.agents");
            DropForeignKey("uko.requests", "agent", "uko.agents");
            DropForeignKey("uko.indicators", "agent", "uko.agents");
            DropForeignKey("uko.agents", "Customer_CompanyCode", "uko.companies");
            DropForeignKey("uko.accounts", "user", "uko.users");
            DropForeignKey("uko.accounts", new[] { "company", "segment" }, "uko.segments");
            DropForeignKey("uko.accounts", "company", "uko.companies");
            DropForeignKey("uko.activities", new[] { "company", "product" }, "uko.products");
            DropForeignKey("uko.activities", new[] { "company", "product", "feature" }, "uko.features");
            DropForeignKey("uko.features", new[] { "company", "product" }, "uko.products");
            DropForeignKey("uko.texts", new[] { "company", "product" }, "uko.products");
            DropForeignKey("uko.texts", new[] { "company", "product", "feature" }, "uko.features");
            DropForeignKey("uko.texts", "culture", "uko.cultures");
            DropForeignKey("uko.cultures", "country", "uko.countries");
            DropForeignKey("uko.texts", "company", "uko.companies");
            DropForeignKey("uko.contracts", new[] { "ProductEntity_CompanyCode", "ProductEntity_ProductName" }, "uko.products");
            DropForeignKey("uko.subscriptions", new[] { "customer", "consumer", "company", "product" }, "uko.contracts");
            DropForeignKey("uko.contracts", new[] { "company", "product" }, "uko.products");
            DropForeignKey("uko.contracts", "customer", "uko.companies");
            DropForeignKey("uko.contracts", new[] { "customer", "consumer" }, "uko.segments");
            DropForeignKey("uko.contracts", "company", "uko.companies");
            DropForeignKey("uko.charges", new[] { "customer", "segment", "vendor", "product", "feature" }, "uko.subscriptions");
            DropForeignKey("uko.subscriptions", new[] { "company", "product" }, "uko.products");
            DropForeignKey("uko.subscriptions", new[] { "company", "product", "feature" }, "uko.features");
            DropForeignKey("uko.subscriptions", "Customer_CompanyCode", "uko.companies");
            DropForeignKey("uko.subscriptions", new[] { "customer", "consumer" }, "uko.segments");
            DropForeignKey("uko.sessions", "user", "uko.users");
            DropForeignKey("uko.logins", "user", "uko.users");
            DropForeignKey("uko.claims", "UserEntity_UserName", "uko.users");
            DropForeignKey("uko.claims", "user", "uko.users");
            DropForeignKey("uko.sessions", new[] { "company", "segment" }, "uko.segments");
            DropForeignKey("uko.sessions", "domain", "uko.domains");
            DropForeignKey("uko.domains", new[] { "company", "product" }, "uko.products");
            DropForeignKey("uko.instances", "DomainEntity_DomainName", "uko.domains");
            DropForeignKey("uko.instances", "domain", "uko.domains");
            DropForeignKey("uko.instances", "company", "uko.companies");
            DropForeignKey("uko.domains", "customer", "uko.companies");
            DropForeignKey("uko.domains", new[] { "customer", "consumer" }, "uko.segments");
            DropForeignKey("uko.domains", "company", "uko.companies");
            DropForeignKey("uko.sessions", "company", "uko.companies");
            DropForeignKey("uko.sessions", new[] { "company", "segment", "user" }, "uko.accounts");
            DropForeignKey("uko.segments", "company", "uko.companies");
            DropForeignKey("uko.subscriptions", "company", "uko.companies");
            DropForeignKey("uko.charges", new[] { "vendor", "product" }, "uko.products");
            DropForeignKey("uko.charges", new[] { "vendor", "product", "feature" }, "uko.features");
            DropForeignKey("uko.charges", new[] { "customer", "segment", "vendor", "product" }, "uko.contracts");
            DropForeignKey("uko.charges", "vendor", "uko.companies");
            DropForeignKey("uko.products", "company", "uko.companies");
            DropForeignKey("uko.features", "company", "uko.companies");
            DropForeignKey("uko.activities", "company", "uko.companies");
            DropIndex("uko.tokens", new[] { "user" });
            DropIndex("uko.tokens", new[] { "company" });
            DropIndex("uko.tokens", new[] { "company", "segment" });
            DropIndex("uko.themes", new[] { "company", "segment" });
            DropIndex("uko.themes", new[] { "company" });
            DropIndex("uko.regions", new[] { "parent" });
            DropIndex("uko.regions", new[] { "culture" });
            DropIndex("uko.regions", new[] { "country" });
            DropIndex("uko.redirects", new[] { "user" });
            DropIndex("uko.redirects", new[] { "company", "product", "feature", "activity" });
            DropIndex("uko.results", new[] { "agent" });
            DropIndex("uko.requests", new[] { "agent" });
            DropIndex("uko.indicators", new[] { "agent" });
            DropIndex("uko.agents", new[] { "Customer_CompanyCode" });
            DropIndex("uko.cultures", new[] { "country" });
            DropIndex("uko.texts", new[] { "culture" });
            DropIndex("uko.texts", new[] { "company", "product", "feature" });
            DropIndex("uko.texts", new[] { "company" });
            DropIndex("uko.logins", new[] { "user" });
            DropIndex("uko.claims", new[] { "UserEntity_UserName" });
            DropIndex("uko.claims", new[] { "user" });
            DropIndex("uko.instances", new[] { "DomainEntity_DomainName" });
            DropIndex("uko.instances", new[] { "domain" });
            DropIndex("uko.instances", new[] { "company" });
            DropIndex("uko.domains", new[] { "company" });
            DropIndex("uko.domains", new[] { "company", "product" });
            DropIndex("uko.domains", new[] { "customer", "consumer" });
            DropIndex("uko.sessions", new[] { "domain" });
            DropIndex("uko.sessions", new[] { "company", "segment", "user" });
            DropIndex("uko.segments", new[] { "company" });
            DropIndex("uko.subscriptions", new[] { "Customer_CompanyCode" });
            DropIndex("uko.subscriptions", new[] { "company", "product", "feature" });
            DropIndex("uko.subscriptions", new[] { "company" });
            DropIndex("uko.subscriptions", new[] { "customer", "consumer", "company", "product" });
            DropIndex("uko.subscriptions", new[] { "customer", "consumer" });
            DropIndex("uko.charges", new[] { "vendor" });
            DropIndex("uko.charges", new[] { "vendor", "product", "feature" });
            DropIndex("uko.charges", new[] { "customer", "segment", "vendor", "product", "feature" });
            DropIndex("uko.charges", new[] { "customer", "segment", "vendor", "product" });
            DropIndex("uko.contracts", new[] { "ProductEntity_CompanyCode", "ProductEntity_ProductName" });
            DropIndex("uko.contracts", new[] { "company", "product" });
            DropIndex("uko.contracts", new[] { "company" });
            DropIndex("uko.contracts", new[] { "customer", "consumer" });
            DropIndex("uko.products", new[] { "company" });
            DropIndex("uko.features", new[] { "company", "product" });
            DropIndex("uko.features", new[] { "company" });
            DropIndex("uko.activities", new[] { "company", "product", "feature" });
            DropIndex("uko.activities", new[] { "company" });
            DropIndex("uko.accounts", new[] { "user" });
            DropIndex("uko.accounts", new[] { "company", "segment" });
            DropIndex("uko.accounts", new[] { "company" });
            DropTable("uko.tokens");
            DropTable("uko.themes");
            DropTable("uko.regions");
            DropTable("uko.redirects");
            DropTable("uko.results");
            DropTable("uko.requests");
            DropTable("uko.indicators");
            DropTable("uko.agents");
            DropTable("uko.countries");
            DropTable("uko.cultures");
            DropTable("uko.texts");
            DropTable("uko.logins");
            DropTable("uko.claims");
            DropTable("uko.users");
            DropTable("uko.instances");
            DropTable("uko.domains");
            DropTable("uko.sessions");
            DropTable("uko.segments");
            DropTable("uko.subscriptions");
            DropTable("uko.charges");
            DropTable("uko.contracts");
            DropTable("uko.products");
            DropTable("uko.features");
            DropTable("uko.activities");
            DropTable("uko.companies");
            DropTable("uko.accounts");
        }
    }
}
