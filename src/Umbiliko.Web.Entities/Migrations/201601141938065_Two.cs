namespace Uos.Web.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Two : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "uko.agents", newName: "robots");
            DropIndex("uko.robots", new[] { "Customer_CompanyCode" });
            RenameColumn(table: "uko.requests", name: "agent", newName: "robot");
            RenameColumn(table: "uko.results", name: "agent", newName: "robot");
            RenameColumn(table: "uko.robots", name: "Customer_CompanyCode", newName: "company");
            RenameIndex(table: "uko.requests", name: "IX_agent", newName: "IX_robot");
            RenameIndex(table: "uko.results", name: "IX_agent", newName: "IX_robot");
            AddColumn("uko.contracts", "clientId", c => c.Guid(nullable: false));
            AddColumn("uko.contracts", "clientSecret", c => c.String(nullable: false, maxLength: 255, unicode: false));
            AddColumn("uko.requests", "status", c => c.String(nullable: false, maxLength: 20, unicode: false));
            AddColumn("uko.results", "data", c => c.String(nullable: false, unicode: false));
            AlterColumn("uko.robots", "company", c => c.String(nullable: false, maxLength: 4, fixedLength: true, unicode: false));
            CreateIndex("uko.robots", "company");
            DropColumn("uko.contracts", "key");
            DropColumn("uko.contracts", "secret");
            DropColumn("uko.robots", "secret");
            DropColumn("uko.robots", "CustomerNumber");
            DropColumn("uko.results", "status");
            DropColumn("uko.results", "summary");
        }
        
        public override void Down()
        {
            AddColumn("uko.results", "summary", c => c.String(nullable: false, unicode: false));
            AddColumn("uko.results", "status", c => c.String(nullable: false, maxLength: 20, unicode: false));
            AddColumn("uko.robots", "CustomerNumber", c => c.Int());
            AddColumn("uko.robots", "secret", c => c.String(maxLength: 128, unicode: false));
            AddColumn("uko.contracts", "secret", c => c.String(nullable: false, maxLength: 255, unicode: false));
            AddColumn("uko.contracts", "key", c => c.Guid(nullable: false));
            DropIndex("uko.robots", new[] { "company" });
            AlterColumn("uko.robots", "company", c => c.String(maxLength: 4, fixedLength: true, unicode: false));
            DropColumn("uko.results", "data");
            DropColumn("uko.requests", "status");
            DropColumn("uko.contracts", "clientSecret");
            DropColumn("uko.contracts", "clientId");
            RenameIndex(table: "uko.results", name: "IX_robot", newName: "IX_agent");
            RenameIndex(table: "uko.requests", name: "IX_robot", newName: "IX_agent");
            RenameColumn(table: "uko.robots", name: "company", newName: "Customer_CompanyCode");
            RenameColumn(table: "uko.results", name: "robot", newName: "agent");
            RenameColumn(table: "uko.requests", name: "robot", newName: "agent");
            CreateIndex("uko.robots", "Customer_CompanyCode");
            RenameTable(name: "uko.robots", newName: "agents");
        }
    }
}
