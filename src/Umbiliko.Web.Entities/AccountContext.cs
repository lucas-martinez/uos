﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web
{
    using Constants;
    using Contracts;
    using Entities;

    public class AccountContext : EntityContext<AccountEntity>
    {
        readonly CompanyContext _company;
        //readonly CustomerContext _customer;
        readonly SegmentContext _segment;
        readonly UserContext _user;

        /*public AccountContext(AccountEntity entity, CustomerContext customer, UserContext user)
            : base(entity, customer.DbContext)
        {
            _customer = customer;
            _segment = SegmentContext.FromEntity(customer.Entity.Reference, customer.DbContext);
            _company = _segment.Company;
            _user = user;
        }*/

        public AccountContext(AccountEntity entity, UserContext user, SegmentContext segment = null)
            : base(entity, user.DbContext)
        {
            _segment = segment ?? SegmentContext.FromEntity(entity.Segment, user.DbContext);
            _company = _segment.Company;
            //_customer = null;
            _user = user;
        }

        public CompanyContext Company
        {
            get { return _company; }
        }

        public string CompanyCode
        {
            get { return Entity.CompanyCode; }
        }

        /*public CustomerContext Customer
        {
            get { return _customer; }
        }*/

        public IList<string> Roles
        {
            get { return Entity.Roles.Split(new [] { Characters.Space }, StringSplitOptions.RemoveEmptyEntries); }
        }

        public SegmentContext Segment
        {
            get { return _segment; }
        }

        public string SegmentCode
        {
            get { return Entity.SegmentCode; }
        }

        public UserContext User
        {
            get { return _user; }
        }

        public string AddRoles(params string[] roles)
        {
            if (roles == null) return Entity.Roles;

            if (string.IsNullOrEmpty(Entity.Roles))
            {
                Update(roles);
            }
            else
            {
                Update(Entity.Roles.Split(Characters.Space).Union(roles).ToArray());
            }

            return Entity.Roles;
        }

        public SessionEntity AddSession(string ip, DateTime? expireUtc)
        {
            var session = new SessionEntity
            {
                SegmentCode = Entity.SegmentCode,
                CompanyCode = Entity.CompanyCode,
                IpAddress = ip,
                ExpiresUtc = expireUtc,
                CreatedUtc = DateTime.UtcNow,
                User = User,
                UserName = User
            };

            Entity.Sessions.Add(session);

            return session;
        }

        public static AccountContext Create(SegmentContext segment, UserContext user, params string[] roles)
        {
            var dbContext = segment.DbContext;

            var dbSet = dbContext.Set<AccountEntity>();

            var entity = dbSet.Add(dbSet.Create());

            Array.Sort(roles, StringComparer.OrdinalIgnoreCase);
            
            entity.SegmentCode = segment.Entity.SegmentCode;
            entity.Company = segment.Company.Entity;
            entity.Segment = segment;
            entity.CompanyCode = segment.Entity.CompanyCode;
            entity.User = user.Entity;
            entity.UserName = user.Entity.UserName;

            segment.Entity.Accounts.Add(entity);

            var context = new AccountContext(entity, user, segment);

            context.Update(roles);

            return context;
        }

        public static async Task<AccountContext> FindAsync(SegmentContext segment, UserContext user)
        {
            if (segment == null) return null;

            var dbContext = segment.DbContext;

            var dbSet = dbContext.Set<AccountEntity>();
            
            var entity = await dbSet.FindAsync(segment.Entity.CompanyCode, segment.Entity.SegmentCode, user.Entity.UserName);

            return entity != null ? new AccountContext(entity, user, segment) : null;
        }

        public static async Task<AccountContext> FindAsync(DbContext dbContext, string userName, string companyCode, string segmentCode)
        {
            var user = UserContext.Find(dbContext, userName);

            if (user == null) return null;

            var company = await CompanyContext.FindAsync(dbContext, companyCode);

            if (company == null) return null;

            var segment = await SegmentContext.FindAsync(company, segmentCode);

            if (segment == null) return null;

            return await FindAsync(segment, user);
        }

        public static async Task<AccountContext> FindAsync(UserContext user, SegmentContext segment)
        {
            if (segment == null) return null;

            var dbContext = user.DbContext;

            var dbSet = dbContext.Set<AccountEntity>();
            
            var entity = await dbSet.FindAsync(segment.Entity.CompanyCode, segment.Entity.SegmentCode, user.Entity.UserName);

            return entity != null ? new AccountContext(entity, user, segment) : null;
        }

        public static async Task<AccountContext> FindAsync(UserContext user, string companyCode, string segmentCode = null)
        {
            if (user == null) return null;

            if (companyCode == null) return null;

            var dbContext = user.DbContext;

            var dbSet = dbContext.Set<AccountEntity>().Include(e => e.Segment);

            AccountEntity entity;

            if (segmentCode != null)
            {
                entity = dbSet.SingleOrDefault(e => e.UserName == user.Name && e.CompanyCode == companyCode && e.SegmentCode == segmentCode);
            }
            else
            {
                entity = dbSet.OrderBy(e => e.CreateUtc).FirstOrDefault(e => e.UserName == user.Name && e.CompanyCode == companyCode);
            }

            return entity != null ? new AccountContext(entity, user) : null;
        }

        public static AccountContext FromEntity(AccountEntity entity, DbContext dbContext)
        {
            if (entity == null) return null;
            var segment = SegmentContext.FromEntity(entity.Segment, dbContext);
            var user = UserContext.FromEntity(entity.User, dbContext);
            return new AccountContext(entity, user, segment);
        }

        public string Update(params string[] roles)
        {
            Array.Sort(roles, StringComparer.OrdinalIgnoreCase);

            return Entity.Roles = string.Join(" ", roles.Select(role => role.Trim().ToLowerInvariant()).Distinct());
        }

        public static implicit operator string(AccountContext context)
        {
            return context != null ? context.Entity.UserName : null;
        }
    }
}
