﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web
{
    using Assertion;
    using Entities;

    public static class DbContextExtensions
    {
        public static IResult TrySaveChanges(this DbContext dbContext)
        {
            try
            {
                dbContext.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                return dbContext.Fail(ex);
            }
            catch (DbUpdateException ex)
            {
                return dbContext.Fail(ex);
            }
            catch (Exception)
            {
                throw;
            }

            return Result.Default;
        }

        public static async Task<IResult> TrySaveChangesAsync(this DbContext dbContext)
        {
            try
            {
                await dbContext.SaveChangesAsync();
            }
            catch (DbEntityValidationException ex)
            {
                return dbContext.Fail(ex);
            }
            catch (DbUpdateException ex)
            {
                return dbContext.Fail(ex);
            }
            catch (Exception)
            {
                throw;
            }

            return Result.Default;
        }

        public static IResult Fail(this DbContext dbContext, DbEntityValidationException ex)
        {
            var errorMessage = ex.InnerMessage();

            var messages = ex.EntityValidationErrors.Select(validation => errorMessage + " " +
                "EntityType: " + validation.Entry.GetEntityType() + ", " +
                "EntityKey: [" + dbContext.GetEntityKey(validation.Entry.Entity).Format() + "], " +
                "Properties: " + validation.Entry.Entity.GetType().Name + " " + validation.Format()).ToArray();

            return Result.Fail(messages);
        }

        public static IResult Fail(this DbContext dbContext, DbUpdateException ex)
        {
            var errorMessage = ex.InnerMessage();

            var messages = ex.Entries.Select(entry => errorMessage + " EntityType: " + entry.GetEntityType() + ", EntityKey: [" + dbContext.GetEntityKey(entry.Entity).Format() + "]").ToArray();

            return Result.Fail(messages);
        }

        private static EntityKey GetEntityKey(this DbContext dbContext, object entity)
        {
            var oc = ((IObjectContextAdapter)dbContext).ObjectContext;
            return oc.ObjectStateManager.GetObjectStateEntry(entity).EntityKey;
        }

        public static string GetEntityType(this DbEntityEntry entry)
        {
            if (entry == null || entry.Entity == null)
            {
                return "null";
            }

            var type = entry.Entity.GetType();

            if (type.BaseType == typeof(object))
            {
                return type.Name;
            }

            if (type.Name.StartsWith(type.BaseType.Name + "_"))
            {
                return type.BaseType.Name;
            }

            return type.Name;
        }

        private static string Format(this DbEntityValidationResult validation)
        {
            return string.Join(", ", validation.ValidationErrors.Select(entry => string.Format("\"{0}\": \"{1}\"", entry.PropertyName, entry.ErrorMessage)));
        }
    }
}
