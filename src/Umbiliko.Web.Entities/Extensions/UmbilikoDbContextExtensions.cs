﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web
{
    using Constants;
    using Entities;

    public static class UmbilikoDbContextExtensions
    {
        public static async Task<CompanyContext> AddOrUpdateCompanyAsyn(this DbContext dbContext, string companyCode, string companyName, string companyType = CompanyTypes.Corporation)
        {
            var company = await CompanyContext.FindAsync(dbContext, companyCode) ?? CompanyContext.Create(dbContext, companyCode);

            company.Update(name: companyName, type: companyType);

            return company; 
        }

        public static async Task<DomainContext> AddOrUpdateDomainAsync(this DbContext dbContext, string domainName, string environment = Environments.Development, string description = null)
        {
            var domain = await DomainContext.FindAsync(dbContext, domainName) ?? DomainContext.Create(dbContext, domainName, environment);

            domain.Update(environment: environment, description: description);

            return domain;
        }

        public static async Task<UserContext> AddOrUpdateUserAsync(this DbContext dbContext, string userName, string emailAddress = null, string phoneNumber = null, string password = null)
        {
            var user = await UserContext.FindAsync(dbContext, userName) ?? UserContext.Create(dbContext, userName);

            user.Update(email: emailAddress, phoneNumber: phoneNumber, password: password);

            return user;
        }
    }
}
