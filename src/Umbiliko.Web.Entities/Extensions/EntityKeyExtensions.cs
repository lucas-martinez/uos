﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Entities
{
    public static class EntityKeyExtensions
    {
        public static IDictionary<string, object> GetValues(this EntityKey entityKey)
        {
            return entityKey.EntityKeyValues.ToDictionary(member => member.Key, member => member.Value);
        }

        public static string Format(this EntityKey entityKey)
        {
            if (entityKey == null || entityKey.EntityKeyValues == null) return "null";

            return string.Join(", ", entityKey.EntityKeyValues.Select(member => string.Format("{0}: {1}", member.Key, Format(member.Value))));
        }

        private static string Format(object value)
        {
            if (value == null) return "null";

            var typeCode = Type.GetTypeCode(value.GetType());
            
            switch (typeCode)
            {
                case TypeCode.String:
                    return '"' + (string)value + '"';

                case TypeCode.DateTime:
                    return '"' + Convert.ToString(value) + '"';

                default:
                    return Convert.ToString(value);
            }
        }
    }
}
