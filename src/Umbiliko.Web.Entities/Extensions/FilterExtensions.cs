﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web
{
    using Constants;
    using Entities;
    using Inquiry;
    using Models;

    public static class FilterExtensions
    {
        #region - filters -

        public static Expression<Func<AccountEntity, bool>> ApplyTo(this Filter filter, Expression<Func<AccountEntity, bool>> predicate)
        {
            if (filter == null) return predicate;
            
            if (filter.HasClients) predicate = predicate.And(e => filter.Clients.Contains(e.SegmentCode));

            if (filter.HasEmails) predicate = predicate.And(e => filter.Emails.Contains(e.User.EmailAddress));
            
            if (filter.HasRoles) predicate = predicate.And(e => e.Roles.Split(Characters.Space).Any(r => filter.Roles.Contains(r.TrimEnd(Characters.Exclamation))));

            if (filter.HasUsers) predicate = predicate.And(e => filter.Users.Contains(e.UserName));
            
            return predicate;
        }

        public static Expression<Func<ActivityEntity, bool>> ApplyTo(this Filter filter, Expression<Func<ActivityEntity, bool>> predicate)
        {
            if (filter == null) return predicate;

            if (filter.HasActivities) predicate = predicate.And(e => filter.Activities.Contains(e.ActivityName));

            if (filter.HasFeatures) predicate = predicate.And(e => filter.Features.Contains(e.FeatureName));

            if (filter.HasProducts) predicate = predicate.And(e => filter.Products.Contains(e.ProductName));
            
            return predicate;
        }

        public static Expression<Func<CompanyEntity, bool>> ApplyTo(this Filter filter, Expression<Func<CompanyEntity, bool>> predicate)
        {
            if (filter == null) return predicate;

            if (filter.HasClients) predicate = predicate.And(e => e.Segments.Any(c => filter.Clients.Contains(c.SegmentCode)));

            return predicate;
        }

        public static Expression<Func<ContractEntity, bool>> ApplyTo(this Filter filter, Expression<Func<ContractEntity, bool>> predicate)
        {
            if (filter == null) return predicate;

            if (filter.HasClients) predicate = predicate.And(e => filter.Clients.Contains(e.ConsumerCode));

            if (filter.HasProducts) predicate = predicate.And(e => filter.Products.Contains(e.ProductName));

            return predicate;
        }

        public static Expression<Func<DomainEntity, bool>> ApplyTo(this Filter filter, Expression<Func<DomainEntity, bool>> predicate)
        {
            if (filter == null) return predicate;

            if (filter.HasDomains) predicate = predicate.And(e => filter.Products.Contains(e.ProductName));

            if (filter.HasProducts) predicate = predicate.And(e => filter.Products.Contains(e.ProductName));
            
            return predicate;
        }

        public static Expression<Func<FeatureEntity, bool>> ApplyTo(this Filter filter, Expression<Func<FeatureEntity, bool>> predicate)
        {
            if (filter == null) return predicate;
            
            if (filter.HasActivities) predicate = predicate.And(e => e.Activities.Any(a => filter.Activities.Contains(a.ActivityName)));

            if (filter.HasFeatures) predicate = predicate.And(e => filter.Features.Contains(e.FeatureName));

            if (filter.HasProducts) predicate = predicate.And(e => filter.Products.Contains(e.ProductName));
            
            return predicate;
        }

        public static Expression<Func<LoginEntity, bool>> ApplyTo(this Filter filter, Expression<Func<LoginEntity, bool>> predicate)
        {
            if (filter == null) return predicate;
            
            if (filter.HasUsers) predicate = predicate.And(e => filter.Users.Contains(e.UserName));

            return predicate;
        }

        public static Expression<Func<ProductEntity, bool>> ApplyTo(this Filter filter, Expression<Func<ProductEntity, bool>> predicate)
        {
            if (filter == null) return predicate;
            
            if (filter.HasActivities) predicate = predicate.And(e => e.Activities.Any(a => filter.Activities.Contains(a.ActivityName)));

            if (filter.HasFeatures) predicate = predicate.And(e => e.Features.Any(f => filter.Features.Contains(f.FeatureName)));

            if (filter.HasProducts) predicate = predicate.And(e => filter.Products.Contains(e.ProductName));
            
            return predicate;
        }

        public static Expression<Func<SegmentEntity, bool>> ApplyTo(this Filter filter, Expression<Func<SegmentEntity, bool>> predicate)
        {
            if (filter == null) return predicate;

            if (filter.HasClients) predicate = predicate.And(e => filter.Clients.Contains(e.SegmentCode));

            return predicate;
        }

        public static Expression<Func<SubscriptionEntity, bool>> ApplyTo(this Filter filter, Expression<Func<SubscriptionEntity, bool>> predicate)
        {
            if (filter == null) return predicate;
            
            if (filter.HasClients) predicate = predicate.And(e => filter.Clients.Contains(e.ConsumerCode));

            if (filter.HasFeatures) predicate = predicate.And(e => filter.Features.Contains(e.FeatureName));

            if (filter.HasProducts) predicate = predicate.And(e => filter.Products.Contains(e.ProductName));
            
            return predicate;
        }
        
        public static Expression<Func<UserEntity, bool>> ApplyTo(this Filter filter, Expression<Func<UserEntity, bool>> predicate)
        {
            if (filter == null) return predicate;
            
            if (filter.HasUsers) predicate = predicate.And(e => filter.Users.Contains(e.UserName));
            
            return predicate;
        }

        #endregion - filter -
    }
}
