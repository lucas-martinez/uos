﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web
{
    using Contracts;
    using Inquiry;

    public static class UserExtensions
    {
        public static IQueryable<TEntity> Select<TEntity, TIdentifier>(this UserContext user)
            where TEntity : class, IUserEntity<TIdentifier>
            where TIdentifier : IComparable<TIdentifier>, IEquatable<TIdentifier>
        {
            string name = user;
            var predicate = PredicateBuilder.True<TEntity>();

            predicate = predicate.And(e => e.UserName == name);

            return user.DbContext.Set<TEntity>().Where(predicate).AsExpandableAsync();
        }

        public static IQueryable<TEntity> Select<TEntity, TIdentifier>(this UserContext user, TIdentifier identifier)
            where TEntity : class, IUserEntity<TIdentifier>
            where TIdentifier : IComparable<TIdentifier>, IEquatable<TIdentifier>
        {
            string name = user;
            var predicate = PredicateBuilder.True<TEntity>();

            predicate = predicate.And(e => e.UserName == name && identifier.Equals(e.Id));

            return user.DbContext.Set<TEntity>().Where(predicate).AsExpandableAsync();
        }

        public static IQueryable<TEntity> Select<TEntity, TIdentifier>(this UserContext user, List<TIdentifier> identifiers)
            where TEntity : class, IUserEntity<TIdentifier>
            where TIdentifier : IComparable<TIdentifier>, IEquatable<TIdentifier>
        {
            string name = user;
            var predicate = PredicateBuilder.True<TEntity>();

            predicate = predicate.And(e => e.UserName == name && identifiers.Contains(e.Id));

            return user.DbContext.Set<TEntity>().Where(predicate).AsExpandableAsync();
        }
    }
}
