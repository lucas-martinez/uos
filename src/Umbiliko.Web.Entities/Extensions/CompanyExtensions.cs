﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web
{
    using Contracts;
    using Inquiry;

    public static class CompanyExtensions
    {
        public static IQueryable<TEntity> Select<TEntity, TIdentifier>(this CompanyContext company)
            where TEntity : class, ICompanyEntity<TIdentifier>
            where TIdentifier : IComparable<TIdentifier>, IEquatable<TIdentifier>
        {
            string companyCode = company;
            var predicate = PredicateBuilder.True<TEntity>();

            predicate = predicate.And(e => e.CompanyCode == companyCode);

            return company.DbContext.Set<TEntity>().Where(predicate).AsExpandableAsync();
        }

        public static IQueryable<TEntity> Select<TEntity, TIdentifier>(this CompanyContext company, TIdentifier identifier)
            where TEntity : class, ICompanyEntity<TIdentifier>
            where TIdentifier : IComparable<TIdentifier>, IEquatable<TIdentifier>
        {
            string companyCode = company;
            var predicate = PredicateBuilder.True<TEntity>();

            predicate = predicate.And(e => e.CompanyCode == companyCode && identifier.Equals(e.Id));

            return company.DbContext.Set<TEntity>().Where(predicate).AsExpandableAsync();
        }

        public static IQueryable<TEntity> Select<TEntity, TIdentifier>(this CompanyContext company, List<TIdentifier> identifiers)
            where TEntity : class, ICompanyEntity<TIdentifier>
            where TIdentifier : IComparable<TIdentifier>, IEquatable<TIdentifier>
        {
            string companyCode = company;
            var predicate = PredicateBuilder.True<TEntity>();

            predicate = predicate.And(e => e.CompanyCode == companyCode && identifiers.Contains(e.Id));

            return company.DbContext.Set<TEntity>().Where(predicate).AsExpandableAsync();
        }
    }
}
