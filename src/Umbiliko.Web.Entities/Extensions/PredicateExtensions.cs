﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web
{
    using Constants;
    using Entities;
    using Inquiry;

    public static class PredicateExtensions
    {
        public static Expression<Func<AccountEntity, bool>> Apply(this Expression<Func<AccountEntity, bool>> predicate, string user = null, string client = null, string email = null)
        {
            if (!string.IsNullOrEmpty(client))
            {
                predicate = predicate.And(e => e.SegmentCode == client);
            }

            if (!string.IsNullOrEmpty(user))
            {
                predicate = predicate.And(e => e.UserName == user);
            }
            else if (!string.IsNullOrEmpty(email))
            {
                predicate = predicate.And(e => e.User.EmailAddress == email);
            }

            return predicate;
        }

        public static Expression<Func<AccountEntity, bool>> Apply(this Expression<Func<AccountEntity, bool>> predicate, params string[] clients)
        {
            if (clients.Length > 0)
            {
                clients = clients.Select(client => client.ToUpperInvariant()).ToArray();
                predicate = predicate.And(e => clients.Contains(e.SegmentCode));
            }

            return predicate;
        }

        public static Expression<Func<ActivityEntity, bool>> Apply(this Expression<Func<ActivityEntity, bool>> predicate, string product = null, string feature = null, string activity = null)
        {
            if (!string.IsNullOrEmpty(product))
            {
                predicate = predicate.And(e => e.ProductName == product);
            }

            if (!string.IsNullOrEmpty(feature))
            {
                feature = feature.ToLowerInvariant();
                predicate = predicate.And(e => e.FeatureName == feature);
            }

            if (!string.IsNullOrEmpty(activity))
            {
                activity = activity.ToLowerInvariant();
                predicate = predicate.And(e => e.ActivityName == activity);
            }

            return predicate;
        }

        public static Expression<Func<CompanyEntity, bool>> Apply(this Expression<Func<CompanyEntity, bool>> predicate)
        {
            return predicate;
        }

        public static Expression<Func<ContractEntity, bool>> Apply(this Expression<Func<ContractEntity, bool>> predicate, string customer = null, string client = null, string product = null, string vendor = null)
        {
            if (!string.IsNullOrEmpty(customer))
            {
                customer = customer.ToUpperInvariant();
                predicate = predicate.And(e => e.CustomerCode == customer);
            }

            if (!string.IsNullOrEmpty(client))
            {
                client = client.ToUpperInvariant();
                predicate = predicate.And(e => e.ConsumerCode == client);
            }

            if (!string.IsNullOrEmpty(product))
            {
                predicate = predicate.And(e => e.ProductName == product);
            }

            if (!string.IsNullOrEmpty(vendor))
            {
                vendor = vendor.ToUpperInvariant();
                predicate = predicate.And(e => e.CompanyCode == vendor);
            }

            return predicate;
        }

        public static Expression<Func<DomainEntity, bool>> Apply(this Expression<Func<DomainEntity, bool>> predicate, string product = null, string environment = null)
        {
            if (!string.IsNullOrEmpty(product))
            {
                predicate = predicate.And(e => e.ProductName == product);
            }

            if (!string.IsNullOrEmpty(environment))
            {
                environment = environment.ToUpperInvariant();
                predicate = predicate.And(e => e.Environment == environment);
            }

            return predicate;
        }

        public static Expression<Func<FeatureEntity, bool>> Apply(this Expression<Func<FeatureEntity, bool>> predicate, string product = null)
        {
            if (!string.IsNullOrEmpty(product))
            {
                predicate = predicate.And(e => e.ProductName == product);
            }

            return predicate;
        }

        public static Expression<Func<LoginEntity, bool>> Apply(this Expression<Func<LoginEntity, bool>> predicate, string user = null, string provider = null, string email = null)
        {
            if (!string.IsNullOrEmpty(provider))
            {
                predicate = predicate.And(e => e.LoginProvider == provider);
            }

            if (!string.IsNullOrEmpty(user))
            {
                predicate = predicate.And(e => e.UserName == user);
            }
            else if (!string.IsNullOrEmpty(email))
            {
                predicate = predicate.And(e => e.User.EmailAddress == email);
            }

            return predicate;
        }

        public static Expression<Func<ProductEntity, bool>> Apply(this Expression<Func<ProductEntity, bool>> predicate)
        {
            return predicate;
        }
        public static Expression<Func<SegmentEntity, bool>> Apply(this Expression<Func<SegmentEntity, bool>> predicate, string environment = null)
        {
            if (!string.IsNullOrEmpty(environment))
            {
                environment = environment.ToUpperInvariant();
                predicate = predicate.And(e => e.Environment == environment);
            }

            return predicate;
        }

        public static Expression<Func<SegmentEntity, bool>> Apply(this Expression<Func<SegmentEntity, bool>> predicate, params string[] clients)
        {
            if (clients.Length > 0)
            {
                clients = clients.Select(client => client.ToUpperInvariant()).ToArray();
                predicate = predicate.And(e => clients.Contains(e.SegmentCode));
            }

            return predicate;
        }

        public static Expression<Func<SessionEntity, bool>> Apply(this Expression<Func<SessionEntity, bool>> predicate)
        {
            return predicate;
        }

        public static Expression<Func<SubscriptionEntity, bool>> Apply(this Expression<Func<SubscriptionEntity, bool>> predicate, string product = null, string feature = null, string customer = null, string client = null)
        {
            if (!string.IsNullOrEmpty(product))
            {
                predicate = predicate.And(e => e.ProductName == product);
            }

            if (!string.IsNullOrEmpty(feature))
            {
                feature = feature.ToLowerInvariant();
                predicate = predicate.And(e => e.FeatureName == feature);
            }

            if (!string.IsNullOrEmpty(customer))
            {
                customer = customer.ToUpperInvariant();
                predicate = predicate.And(e => e.CustomerCode == customer);
            }

            if (!string.IsNullOrEmpty(client))
            {
                client = client.ToUpperInvariant();
                predicate = predicate.And(e => e.ConsumerCode == client);
            }

            return predicate;
        }
    }
}
