﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web
{
    using Constants;
    using Contracts;
    using Entities;

    public static class UserDataExtensions
    {
        public static void UpdateFrom(this SessionInfo session, SessionEntity entity)
        {
            if (entity == null) return;

            session.AccessToken = entity.AccessToken;

            session.Account = entity.Account;

            session.ExpiresUtc = entity.ExpiresUtc;

            session.IssuedUtc = entity.CreatedUtc;

            session.Id = entity.Id;

            session.User = entity.User;
        }

        public static void UpdateFrom(this SessionInfo session, TokenEntity entity)
        {
            if (entity == null) return;

            session.AccessToken = entity.TokenValue;

            if (entity.CompanyCode != null && entity.SegmentCode != null)
            {
                session.Account = new AccountInfo
                {
                    CompanyCode = entity.CompanyCode,

                    CompanyName = entity.Company.CompanyName,

                    SegmentCode = entity.SegmentCode,

                    SegmentDescription = entity.Segment.Description
                };
            }

            session.ExpiresUtc = entity.ExpiresUtc;

            session.User.Name = entity.UserName;
        }

        public static void UpdateFrom(this UserInfo user, UserEntity entity)
        {
            if (entity == null) return;

            user.AccessFailedCount = entity.AccessFailedCount;
            
            //
            // List accounts (all)

            user.Accounts = entity.Accounts
                .OrderBy(e => e.CompanyCode)
                .ThenBy(e => e.SegmentCode)
                .Select(e => new AccountInfo
                {
                    CompanyCode = e.CompanyCode,
                    CompanyName = e.Company.CompanyName,
                    Environment = e.Segment.Environment,
                    ExpiresUtc = null,
                    SegmentCode = e.SegmentCode,
                    SegmentDescription = e.Segment.Description
                })
                .ToList();

            //
            // List claims

            user.Claims = entity.Claims.Select(e => new ClaimInfo
            {
                Type = e.ClaimType,
                Value = e.ClaimValue,
                ValueType = e.ClaimValueType,
                Issuer = e.LoginProvider
            }).ToList();

            user.Email = entity.EmailAddress;

            user.EmailConfirmed = entity.EmailConfirmed;

            user.Name = entity.UserName;
            
            user.PasswordHash = entity.PasswordHash;

            user.LockoutEnabled = entity.LockoutEnabled;

            user.LockoutEndDateUtc = entity.LockoutEndDateUtc;

            //
            // List logins

            user.Logins = entity.Logins
                .OrderBy(e => e.LoginProvider)
                .Select(e => new LoginInfo
                {
                    AccessToken = e.LoginToken,
                    ExpiresUtc = e.ExpiresUtc,
                    Label = e.Label,
                    LoginCount = e.LoginCount,
                    LoginKey = e.LoginKey,
                    LoginProvider = e.LoginProvider,
                    LoginUtc = e.LoginUtc
                }).ToList();

            user.PhoneNumber = entity.PhoneNumber;

            user.PhoneNumberConfirmed = entity.PhoneNumberConfirmed;

            user.SecurityStamp = entity.SecurityStamp;

            user.Name = entity.UserName;
        }
    }
}
