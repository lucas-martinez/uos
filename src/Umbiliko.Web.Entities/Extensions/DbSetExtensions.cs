﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uos.Inquiry;
using Uos.Web.Contracts;

namespace Uos.Web
{
    public static class DbSetExtensions
    {
        public static IQueryable<TEntity> Select<TEntity, TIdentifier>(this DbSet<TEntity> dbSet, TIdentifier identifier)
            where TEntity : class, IEntity<TIdentifier>
            where TIdentifier : IComparable<TIdentifier>, IEquatable<TIdentifier>
        {
            var predicate = PredicateBuilder.True<TEntity>();

            predicate = predicate.And(e => identifier.Equals(e.Id));

            return dbSet.Where(predicate).AsExpandableAsync();
        }

        public static IQueryable<TEntity> Select<TEntity, TIdentifier>(this DbSet<TEntity> dbSet, List<TIdentifier> identifiers)
            where TEntity : class, IEntity<TIdentifier>
            where TIdentifier : IComparable<TIdentifier>, IEquatable<TIdentifier>
        {
            var predicate = PredicateBuilder.True<TEntity>();

            predicate = predicate.And(e => identifiers.Contains(e.Id));

            return dbSet.Where(predicate).AsExpandableAsync();
        }
    }
}
