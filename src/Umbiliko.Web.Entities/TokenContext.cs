﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web
{
    using Entities;
    
    public class TokenContext : EntityContext<TokenEntity>
    {
        public TokenContext(TokenEntity entity, DbContext dbContext)
            : base(entity, dbContext)
        {
        }
    }
}
