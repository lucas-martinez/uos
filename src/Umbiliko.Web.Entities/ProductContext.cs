﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web
{
    using Constants;
    using Contracts;
    using Entities;

    public class ProductContext : EntityContext<ProductEntity>
    {
        readonly CompanyContext _company;

        public ProductContext(ProductEntity entity, DbContext dbContext)
            : base(entity, dbContext)
        {
            _company = CompanyContext.FromEntity(entity.Company, dbContext);
        }

        public ProductContext(ProductEntity entity, CompanyContext company)
            : base(entity, company.DbContext)
        {
            _company = company;
        }

        public CompanyContext Company
        {
            get { return _company; }
        }

        public string Name
        {
            get { return Entity.ProductName; }
        }

        public ConsumerContext AddConsumer(SegmentContext reference, decimal credit = 0, string currency = Currencies.USD, string country = null)
        {
            return ConsumerContext.Create(this, reference, credit, currency: currency, country: country);
        }

        public DomainEntity AddDomain(string name, string environment, string description = null)
        {
            return DomainContext.Create(Company, name, this, environment, description);
        }

        public FeatureEntity AddFeature(string name, string description = null)
        {
            return FeatureContext.Create(this, name, description);
        }

        public ConsumerContext AddOrUpdateConsumer(SegmentContext segment, decimal? credit = null, string countryCode = null, string currencyCode = Currencies.USD)
        {
            var customer = ConsumerContext.Find(this, segment.Entity.CompanyCode, segment.Entity.SegmentCode);

            if (customer != null)
            {
                customer.Update(credit, country: countryCode, currency: currencyCode);
            }
            else
            {
                customer = ConsumerContext.Create(this, segment, credit ?? 0, country: countryCode, currency: currencyCode);
            }

            return customer;
        }

        public DomainContext AddOrUpdateDomain(string name, string environment, string description = null)
        {
            var domain = DomainContext.Find(Company, name);

            if (domain != null)
            {
                domain.Update(this, environment, description);
            }
            else
            {
                domain = DomainContext.Create(Company, name, this, environment, description);
            }

            return domain;
        }

        public SubscriptionEntity AddOrUpdateSubscription(string feature, ContractEntity consumer)
        {
            var context = FeatureContext.Find(this, feature);

            return context.AddOrUpdateSubscription(consumer);
        }

        public FeatureContext AddFeature(string name, string description = null, IDictionary<string, string> activities = null)
        {
            var feature = FeatureContext.Create(this, name, description);

            feature.AddActivities(activities);

            return feature;
        }

        public FeatureContext AddOrUpdateFeature(string name, string description = null, IDictionary<string, string> activities = null)
        {
            var feature = FeatureContext.Find(this, name);

            if (feature != null)
            {
                feature.Update(description, activities);
            }
            else
            {
                feature = FeatureContext.Create(this, name, description);

                feature.AddActivities(activities);
            }

            return feature;
        }

        public static ProductContext Create(CompanyContext company, string name)
        {
            var dbContext = company.DbContext;

            var dbSet = dbContext.Set<ProductEntity>();

            var entity = dbSet.Add(dbSet.Create());

            entity.ProductName = name;
            entity.Company = company.Entity;
            entity.CompanyCode = company;

            company.Entity.Products.Add(entity);

            return new ProductContext(entity, company);
        }
        
        public static async Task<ProductContext> FindAsync(CompanyContext company, string name)
        {
            if (company == null) return null;

            var dbContext = company.DbContext;

            var dbSet = dbContext.Set<ProductEntity>();

            var entity = await dbSet.FindAsync(company.Entity.CompanyCode, name);

            return entity != null ? new ProductContext(entity, company) : null;
        }

        public static ProductContext FromEntity(ProductEntity entity, DbContext dbContext)
        {
            return new ProductContext(entity, CompanyContext.FromEntity(entity.Company, dbContext));
        }

        public async Task<ICollection<ConsumerInfo>> GetConsumersAsync()
        {
            var product = Entity;
            var source = DbContext.Set<SubscriptionEntity>().Include(e => e.Consumer);

            var groups = await source.Where(e => e.Status == SubscriptionStatuses.Active)
                .OrderBy(e => e.CustomerCode).ThenBy(e => e.ConsumerCode).ThenBy(e => e.FeatureName)
                .Select(e => new { e.CustomerCode, e.ConsumerCode, e.FeatureName, e.UntilUtc })
                .GroupBy(e => new { e.CustomerCode, e.ConsumerCode })
                .ToListAsync();

            return groups.Select(group => new ConsumerInfo
            {
                CompanyCode = group.Key.CustomerCode,
                SegmentCode = group.Key.ConsumerCode,
                Subscriptions = group.Select(o => new SubscriptionInfo
                {
                    FeatureName = o.FeatureName,
                    ExpiresUtc = o.UntilUtc
                }).ToList()
            }).ToList();
        }

        public async Task<ICollection<FeatureInfo>> GetFeaturesAsync()
        {
            var product = Entity;
            var groups = await DbContext.Set<ActivityEntity>()
                .Select(e => new { e.FeatureName, e.ActivityName, e.AccessLevel, e.Roles })
                .GroupBy(e => new { e.FeatureName })
                .ToListAsync();

            Func<string, AccessLevels> ParseLevel = (value) =>
            {
                AccessLevels level;
                return Enum.TryParse<AccessLevels>(value, out level) ? level : AccessLevels.Private;
            };

            return groups.Select(group => new FeatureInfo
            {
                Name = group.Key.FeatureName,
                Activities = group.Select(o => new ActivityInfo
                {
                    AccessLevel = ParseLevel(o.AccessLevel),
                    Name = o.ActivityName,
                    Roles = o.Roles.Split(Characters.Space)
                }).ToList()
            }).ToList();
        }

        public ProductContext Update(string description)
        {
            if (description != null) Entity.Description = description;

            return this;
        }

        public static implicit operator string (ProductContext context)
        {
            return context != null ? context.Entity.ProductName : null;
        }
    }
}
