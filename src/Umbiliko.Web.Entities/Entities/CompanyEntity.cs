﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Entities
{
    using Constants;
    using Contracts;

    /// <summary>
    /// Business Entity or Organization
    /// </summary>
    public class CompanyEntity : IEntity<string>
    {
        ICollection<AccountEntity> _accounts;
        ICollection<ActivityEntity> _activities;
        ICollection<ContractEntity> _contracts;
        ICollection<DomainEntity> _domains;
        ICollection<FeatureEntity> _features;
        ICollection<ProductEntity> _products;
        ICollection<ContractEntity> _providers;
        string _companyCode;
        ICollection<SegmentEntity> _segments;
        ICollection<SessionEntity> _sessions;

        public virtual ICollection<AccountEntity> Accounts
        {
            get { return _accounts ?? (_accounts = new Collection<AccountEntity>()); }
            set { _accounts = value; }
        }

        public virtual ICollection<ActivityEntity> Activities
        {
            get { return _activities ?? (_activities = new Collection<ActivityEntity>()); }
            set { _activities = value; }
        }

        public virtual string CompanyName { get; set; }

        public virtual string CompanyType { get; set; }

        public virtual ICollection<ContractEntity> Contracts
        {
            get { return _contracts ?? (_contracts = new Collection<ContractEntity>()); }
            set { _contracts = value; }
        }

        public virtual ICollection<DomainEntity> Domains
        {
            get { return _domains ?? (_domains = new Collection<DomainEntity>()); }
            set { _domains = value; }
        }

        public virtual ICollection<FeatureEntity> Features
        {
            get { return _features ?? (_features = new Collection<FeatureEntity>()); }
            set { _features = value; }
        }

        string IEntity<string>.Id
        {
            get { return CompanyCode; }
        }
        
        public virtual ICollection<ProductEntity> Products
        {
            get { return _products ?? (_products = new Collection<ProductEntity>()); }
            set { _products = value; }
        }

        public virtual ICollection<ContractEntity> Providers
        {
            get { return _providers ?? (_providers = new Collection<ContractEntity>()); }
            set { _providers = value; }
        }

        /// <summary>
        /// Trademark, Security Identifier
        /// </summary>
        public virtual string CompanyCode
        {
            get { return _companyCode; }
            set { _companyCode = value != null ? value.ToUpperInvariant() : string.Empty; }
        }

        public virtual ICollection<SegmentEntity> Segments
        {
            get { return _segments ?? (_segments = new Collection<SegmentEntity>()); }
            set { _segments = value; }
        }

        public virtual ICollection<SessionEntity> Sessions
        {
            get { return _sessions ?? (_sessions = new Collection<SessionEntity>()); }
            set { _sessions = value; }
        }
    }
}
