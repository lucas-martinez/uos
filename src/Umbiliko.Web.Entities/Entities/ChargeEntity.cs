﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Entities
{
    using Contracts;

    public class ChargeEntity : ICompanyEntity<long>
    {
        byte? _period;
        DateTime? _serviceUtc;

        public virtual decimal Amount { get; set; }

        public virtual string SegmentCode { get; set; }

        public virtual CompanyEntity Company { get; set; }

        public virtual string Currency { get; set; }

        public virtual string Customer { get; set; }

        public virtual ContractEntity Consumer { get; set; }

        public virtual FeatureEntity Feature { get; set; }

        public virtual string FeatureName { get; set; }

        long IEntity<long>.Id
        {
            get { return SerialNumber; }
        }

        /// <summary>
        /// Week of the year
        /// </summary>
        public virtual byte Period
        {
            get { return _period ?? (_period = ServiceUtc.GetWeekOfYear()).Value; }
            set { _period = value; }
        }

        public virtual ProductEntity Product { get; set; }

        public virtual string ProductName { get; set; }

        public virtual string ServiceDetail { get; set; }

        public virtual long SerialNumber { get; set; }
        
        public virtual DateTime ServiceUtc
        {
            get { return _serviceUtc ?? (_serviceUtc = DateTime.UtcNow).Value; }
            set { _serviceUtc = value; }
        }

        public virtual SubscriptionEntity Subscription { get; set; }

        public virtual string CompanyCode { get; set; }
    }
}
