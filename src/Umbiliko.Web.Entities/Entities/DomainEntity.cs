﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Entities
{
    using Contracts;

    public class DomainEntity : ICompanyEntity<string>
    {
        ICollection<InstanceEntity> _instances;

        public virtual bool AllowExternalLogins { get; set; }

        public virtual SegmentEntity Consumer { get; set; }

        public virtual string ConsumerCode { get; set; }

        public virtual CompanyEntity Customer { get; set; }

        /// <summary>
        /// Optional. If specified only consumers from this company can login
        /// </summary>
        public virtual string CustomerCode { get; set; }

        public virtual string Description { get; set; }

        public virtual string DomainName { get; set; }

        public virtual string Environment { get; set; }

        string IEntity<string>.Id
        {
            get { return DomainName; }
        }

        public virtual ICollection<InstanceEntity> Instances
        {
            get { return _instances ?? (_instances = new List<InstanceEntity>()); }
            set { _instances = value; }
        }

        public virtual ProductEntity Product { get; set; }

        public virtual string ProductName { get; set; }
        
        public virtual CompanyEntity Company { get; set; }

        public virtual string CompanyCode { get; set; }
    }
}
