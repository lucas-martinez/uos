﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Entities
{
    public class MessageEntity
    {
        public DateTime CreateUtc { get; set; }

        public string MessageCode { get; set; }

        public string MessageType { get; set; }

        public UserEntity User { get; set; }

        public string UserName { get; set; }
    }
}
