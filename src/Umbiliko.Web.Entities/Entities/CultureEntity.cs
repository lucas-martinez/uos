﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Entities
{
    using Contracts;

    public class CultureEntity : IEntity<int>
    {
        ICollection<TextEntity> _texts;

        public virtual CountryEntity Country { get; set; }

        public virtual string CountryCode { get; set; }

        public virtual string CultureName { get; set; }

        public virtual int Id { get; set; }

        public virtual string TextReplace { get; set; }

        public virtual ICollection<TextEntity> Texts
        {
            get { return _texts ?? (_texts = new Collection<TextEntity>()); }
            set { _texts = value; }
        }
    }
}
