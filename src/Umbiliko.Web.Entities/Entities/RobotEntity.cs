﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Entities
{
    using Contracts;

    public class RobotEntity : IEntity<Guid>
    {
        Guid? _id;
        ICollection<IndicatorEntity> _indicators;
        ICollection<RobotRequestEntity> _requests;
        ICollection<RobotResultEntity> _results;

        public virtual Guid Id
        {
            get { return _id ?? (_id = Guid.NewGuid()).Value; }
            set { _id = value; }
        }

        public virtual CompanyEntity Company { get; set; }

        public virtual string CompanyCode { get; set; }

        public virtual ICollection<IndicatorEntity> Indicators
        {
            get { return _indicators ?? (_indicators = new List<IndicatorEntity>()); }
            set { _indicators = value; }
        }

        public virtual string MachineName { get; set; }

        public virtual int ProcessId { get; set; }

        public virtual ICollection<RobotRequestEntity> Requests
        {
            get { return _requests ?? (_requests = new List<RobotRequestEntity>()); }
            set { _requests = value; }
        }

        public virtual ICollection<RobotResultEntity> Results
        {
            get { return _results ?? (_results = new List<RobotResultEntity>()); }
            set { _results = value; }
        }

        public virtual AgentTasks TasksCompleted { get; set; }

        public virtual AgentTasks TasksPending { get; set; }

        public virtual AgentTasks TasksRunning { get; set; }
    }
}
