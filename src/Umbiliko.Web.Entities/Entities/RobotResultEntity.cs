﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Entities
{
    public class RobotResultEntity
    {
        public virtual RobotEntity Robot { get; set; }

        public virtual Guid RobotId { get; set; }

        public virtual DateTime? CompletedUtc { get; set; }

        public virtual DateTime StartedUtc { get; set; }

        public virtual string ResultData { get; set; }

        public virtual AgentTasks Task { get; set; }
    }
}
