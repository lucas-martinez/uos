﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Entities
{
    public class IndicatorEntity
    {
        public virtual RobotEntity Agent { get; set; }

        public virtual Guid AgentId { get; set; }
        
        public byte[] Days { get; set; }

        public byte[] Hours { get; set; }

        public byte[] Milliseconds { get; set; }

        public byte[] Minutes { get; set; }

        public byte[] Seconds { get; set; }

        public int Timestamp { get; set; }
    }
}
