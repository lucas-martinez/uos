﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Entities
{
    using Contracts;

    public class FeatureEntity : ICompanyEntity<int>
    {
        ICollection<ActivityEntity> _activities;
        ICollection<SubscriptionEntity> _subscriptions;
        ICollection<TextEntity> _texts;
        DateTime? _updateUtc;

        public virtual ICollection<ActivityEntity> Activities
        {
            get { return _activities ?? (_activities = new Collection<ActivityEntity>()); }
            set { _activities = value; }
        }
        
        public virtual string FeatureName { get; set; }

        public virtual string Description { get; set; }

        public virtual int Id { get; set; }

        public virtual ProductEntity Product { get; set; }

        public virtual string ProductName { get; set; }

        public virtual ICollection<SubscriptionEntity> Subscriptions
        {
            get { return _subscriptions ?? (_subscriptions = new Collection<SubscriptionEntity>()); }
            set { _subscriptions = value; }
        }

        public virtual ICollection<TextEntity> Texts
        {
            get { return _texts ?? (_texts = new Collection<TextEntity>()); }
            set { _texts = value; }
        }

        public virtual DateTime UpdateUtc
        {
            get { return _updateUtc ?? (_updateUtc = DateTime.UtcNow).Value; }
            set { _updateUtc = value; }
        }

        public virtual CompanyEntity Company { get; set; }

        public virtual string CompanyCode { get; set; }
    }
}
