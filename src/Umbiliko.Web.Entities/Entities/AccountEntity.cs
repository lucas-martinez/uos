﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Entities
{
    using Constants;
    using Contracts;

    public class AccountEntity : ICompanyEntity<int>, IUserEntity<int>, ITrade
    {
        DateTime? _changeUtc;
        DateTime? _createUtc;
        ICollection<SessionEntity> _sessions;
        
        public virtual DateTime ChangeUtc
        {
            get { return _changeUtc ?? CreateUtc; }
            set { _changeUtc = value; }
        }

        public virtual string SegmentCode { get; set; }

        public virtual CompanyEntity Company { get; set; }

        public virtual DateTime CreateUtc
        {
            get { return _createUtc ?? (_createUtc = DateTime.UtcNow).Value; }
            set { _createUtc = value; }
        }

        public virtual int Id { get; set; }

        public virtual string Roles { get; set; }
        
        public virtual ICollection<SessionEntity> Sessions
        {
            get { return _sessions ?? (_sessions = new Collection<SessionEntity>()); }
            set { _sessions = value; }
        }

        public virtual SegmentEntity Segment { get; set; }

        public virtual string CompanyCode { get; set; }

        public virtual UserEntity User { get; set; }

        public virtual string UserName { get; set; }

        public static implicit operator AccountInfo(AccountEntity entity)
        {
            return entity == null ? null : new AccountInfo
            {
                CompanyCode = entity.CompanyCode,
                CompanyName = entity.Company.CompanyName,
                Roles = entity.Roles.Split(Characters.Space),
                SegmentCode = entity.SegmentCode,
                SegmentDescription = entity.Segment.Description
            };
        }
    }
}
