﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Entities
{
    using Contracts;

    public class ProductEntity : ICompanyEntity<int>
    {
        ICollection<ActivityEntity> _activities;
        ICollection<ContractEntity> _consumers;
        ICollection<DomainEntity> _domains;
        ICollection<FeatureEntity> _features;
        ICollection<SubscriptionEntity> _subscriptions;
        ICollection<TextEntity> _texts;
        DateTime? _updateUtc;

        public virtual ICollection<ActivityEntity> Activities
        {
            get { return _activities ?? (_activities = new Collection<ActivityEntity>()); }
            set { _activities = value; }
        }

        public virtual ICollection<ContractEntity> Consumers
        {
            get { return _consumers ?? (_consumers = new Collection<ContractEntity>()); }
            set { _consumers = value; }
        }

        public virtual string AlphaVersion { get; set; }

        public virtual string BetaVersion { get; set; }

        public virtual CompanyEntity Company { get; set; }

        public virtual string Description { get; set; }

        public virtual ICollection<DomainEntity> Domains
        {
            get { return _domains ?? (_domains = new List<DomainEntity>()); }
            set { _domains = value; }
        }

        public virtual ICollection<FeatureEntity> Features
        {
            get { return _features ?? (_features = new Collection<FeatureEntity>()); }
            set { _features = value; }
        }

        public virtual int Id { get; set; }

        public virtual string ProductName { get; set; }

        public virtual string ReleaseVersion { get; set; }
        
        public virtual ICollection<SubscriptionEntity> Subscriptions
        {
            get { return _subscriptions ?? (_subscriptions = new Collection<SubscriptionEntity>()); }
            set { _subscriptions = value; }
        }

        public virtual ICollection<TextEntity> Texts
        {
            get { return _texts ?? (_texts = new Collection<TextEntity>()); }
            set { _texts = value; }
        }

        public virtual string CompanyCode { get; set; }

        public virtual DateTime UpdateUtc
        {
            get { return _updateUtc ?? (_updateUtc = DateTime.UtcNow).Value; }
            set { _updateUtc = value; }
        }
    }
}