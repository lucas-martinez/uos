﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Entities
{
    public class CountryEntity
    {
        public virtual string CountryCode { get; set; }

        public virtual string CountryName { get; set; }

        public virtual string DivisionType { get; set; }

        public virtual int Identifier { get; set; }
    }
}
