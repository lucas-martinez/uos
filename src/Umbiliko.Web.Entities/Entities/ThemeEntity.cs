﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Entities
{
    public class ThemeEntity
    {
        public virtual SegmentEntity Segment { get; set; }

        public virtual string SegmentCode { get; set; }

        public virtual CompanyEntity Company { get; set; }

        public virtual string CompanyCode { get; set; }

        public virtual int Identifier { get; set; }

        public virtual string ThemeName { get; set; }

        public virtual byte[] ThemeData { get; set; }
    }
}
