﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Entities
{
    using Contracts;

    public class RegionEntity : IEntity<Guid>
    {
        ICollection<RegionEntity> _children;
        Guid? _regionId;

        public virtual CountryEntity Country { get; set; }

        public virtual string CountryCode { get; set; }

        public virtual CultureEntity Culture { get; set; }

        public virtual string CultureName { get; set; }

        public virtual string DivisionType { get; set; }

        Guid IEntity<Guid>.Id
        {
            get { return RegionId; }
        }

        public virtual ICollection<RegionEntity> RegionChildren
        {
            get { return _children ?? (_children = new List<RegionEntity>()); }
            set { _children = value; }
        }

        public virtual Guid RegionId
        {
            get { return _regionId ?? (_regionId = Guid.NewGuid()).Value; }
            set { _regionId = value; }
        }

        public virtual string RegionName { get; set; }

        public virtual RegionEntity RegionParent { get; set; }

        public virtual Guid? RegionParentId { get; set; }

        public virtual string RegionType { get; set; }
    }
}
