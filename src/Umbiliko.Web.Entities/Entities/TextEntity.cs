﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Entities
{
    public class TextEntity
    {
        public virtual CultureEntity Culture { get; set; }

        public virtual string CultureName { get; set; }

        public virtual CompanyEntity Company { get; set; }

        public virtual string CompanyCode { get; set; }

        public virtual FeatureEntity Feature { get; set; }

        public virtual string FeatureName { get; set; }

        public virtual int Identifier { get; set; }

        public virtual ProductEntity Product { get; set; }
        
        public virtual string ProductName { get; set; }
        
        public virtual string TextName { get; set; }
        
        public virtual string TextValue { get; set; }
    }
}