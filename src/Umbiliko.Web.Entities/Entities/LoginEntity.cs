﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Entities
{
    using Contracts;
    
    public class LoginEntity : IEntity<int>
    {
        public virtual DateTime? ExpiresUtc { get; set; }

        public virtual string Label { get; set; }

        public virtual bool LoginConfirmed { get; set; }

        public virtual int LoginCount { get; set; }
        
        public virtual string LoginKey { get; set; }

        public virtual string LoginProvider { get; set; }

        public virtual string LoginToken { get; set; }

        public virtual DateTime LoginUtc { get; set; }
        
        public virtual int Id { get; set; }
        
        public virtual UserEntity User { get; set; }

        public virtual string UserName { get; set; }

        public static implicit operator UserInfo(LoginEntity entity)
        {
            if (entity == null) return null;

            var user = new UserInfo();

            user.UpdateFrom(entity.User);

            return user;
        }

        public static bool operator ==(LoginEntity left, LoginEntity right)
        {
            if (ReferenceEquals(left, right))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)left == null) || ((object)right == null))
            {
                return false;
            }

            // Return true if the key fields match:
            return left.LoginKey == right.LoginKey && left.LoginProvider == right.LoginProvider && left.UserName == right.UserName;
        }

        public static bool operator !=(LoginEntity left, LoginEntity right)
        {
            return !(left == right);
        }
    }

    public static class LoginExtensions
    {
        public static bool IsNull(this LoginEntity entity)
        {
            return entity == (LoginEntity)null;
        }

        public static bool NotNull(this LoginEntity entity)
        {
            return entity != (LoginEntity)null;
        }
    }
}
