﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Entities
{
    using Contracts;

    public class RedirectEntity : ICompanyEntity<int>, IUserEntity<int>, ITrade
    {
        public virtual string AccessLevel { get; set; }

        public virtual ActivityEntity Activity { get; set; }

        public virtual string ActivityName { get; set; }

        public virtual CompanyEntity Company { get; set; }

        /// <summary>
        /// Enter (login), Exit (logout), Open
        /// </summary>
        public virtual string EventName { get; set; }

        public virtual FeatureEntity Feature { get; set; }

        public virtual string FeatureName { get; set; }

        public virtual ProductEntity Product { get; set; }

        public virtual string ProductName { get; set; }

        public virtual int Id { get; set; }

        public virtual string CompanyCode { get; set; }

        public virtual UserEntity User { get; set; }

        public virtual string UserName { get; set; }
    }
}
