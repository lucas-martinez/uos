﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Entities
{
    using Constants;
    using Contracts;
    using Results;

    public class UserEntity : IEntity<string>
    {
        ICollection<AccountEntity> _accounts;
        DateTime? _changeUtc;
        ICollection<ClaimEntity> _claims;
        DateTime? _createdUtc;
        ICollection<LoginEntity> _logins;
        ICollection<SessionEntity> _sessions;

        /// <summary>
        /// Used to record failures for the purposes of lockout.
        /// </summary>
        public virtual int AccessFailedCount { get; set; }

        public virtual ICollection<AccountEntity> Accounts
        {
            get { return _accounts ?? (_accounts = new Collection<AccountEntity>()); }
            set { _accounts = value; }
        }

        /// <summary>
        /// Email address for the user.
        /// </summary>
        public virtual string EmailAddress { get; set; }

        /// <summary>
        /// True if the email is confirmed, default is false.
        /// </summary>
        public virtual bool EmailConfirmed { get; set; }

        public virtual DateTime ChangeUtc
        {
            get { return _changeUtc ?? CreatedUtc; }
            set { _changeUtc = value; }
        }

        public virtual ICollection<ClaimEntity> Claims
        {
            get { return _claims ?? (_claims = new Collection<ClaimEntity>()); }
            set { _claims = value; }
        }

        public virtual DateTime CreatedUtc
        {
            get { return _createdUtc ?? (_createdUtc = DateTime.UtcNow).Value; }
            set { _createdUtc = value; }
        }

        string IEntity<string>.Id
        {
            get { return UserName; }
        }

        /// <summary>
        /// Is lockout enabled for this user.
        /// </summary>
        public virtual bool LockoutEnabled { get; set; }

        /// <summary>
        /// DateTime in UTC when lockout ends, any time in the past is considered not locked.
        /// </summary>
        public virtual DateTime? LockoutEndDateUtc { get; set; }

        public virtual ICollection<LoginEntity> Logins
        {
            get { return _logins ?? (_logins = new Collection<LoginEntity>()); }
            set { _logins = value; }
        }

        /// <summary>
        /// Phone number for the user.
        /// </summary>
        public virtual string PhoneNumber { get; set; }

        /// <summary>
        /// True if the phone number is confirmed, default is false.
        /// </summary>
        public virtual bool PhoneNumberConfirmed { get; set; }

        public virtual string PasswordStrength { get; set; }

        public virtual string PasswordHash { get; set; }

        /// <summary>
        /// A random value that should change whenever a users credentials have changed (password changed, login removed)
        /// </summary>
        public virtual string SecurityStamp { get; set; }
        
        public virtual ICollection<SessionEntity> Sessions
        {
            get { return _sessions ?? (_sessions = new Collection<SessionEntity>()); }
            set { _sessions = value; }
        }

        /// <summary>
        /// Is two factor enabled for the user.
        /// </summary>
        public virtual bool TwoFactorEnabled { get; set; }

        /// <summary>
        /// User name.
        /// </summary>
        public virtual string UserName { get; set; }
        
        public virtual UserStatuses UserStatus { get; set; }

        public static implicit operator UserInfo(UserEntity entity)
        {
            if (entity == null) return null;

            var user = new UserInfo();

            user.UpdateFrom(entity);

            return user;
        }

        public static implicit operator UserResult(UserEntity entity)
        {
            return UserResult.Assert(entity);
        }
    }
}
