﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Entities
{
    using Contracts;

    /// <summary>
    /// Market segmentation is a marketing strategy which involves dividing a broad target market into subsets of consumers, businesses, or countries
    /// </summary>
    public class SegmentEntity : ICompanyEntity<int>
    {
        ICollection<AccountEntity> _accounts;
        DateTime? _changedUtc;
        DateTime? _createdUtc;
        ICollection<SessionEntity> _sessions;
        ICollection<SubscriptionEntity> _subscriptions;

        public virtual ICollection<AccountEntity> Accounts
        {
            get { return _accounts ?? (_accounts = new Collection<AccountEntity>()); }
            set { _accounts = value; }
        }

        public virtual DateTime ChangedUtc
        {
            get { return _changedUtc ?? CreatedUtc; }
            set { _changedUtc = value; }
        }

        /// <summary>
        /// Group security identifier
        /// </summary>
        public virtual string SegmentCode { get; set; }

        public virtual CompanyEntity Company { get; set; }

        public virtual DateTime CreatedUtc
        {
            get { return _createdUtc ?? (_createdUtc = DateTime.UtcNow).Value; }
            set { _createdUtc = value; }
        }

        public virtual string Description{ get; set; }

        public virtual string Environment { get; set; }

        public virtual int Id { get; set; }

        public virtual ICollection<SessionEntity> Sessions
        {
            get { return _sessions ?? (_sessions = new Collection<SessionEntity>()); }
            set { _sessions = value; }
        }

        public virtual ICollection<SubscriptionEntity> Subscriptions
        {
            get { return _subscriptions ?? (_subscriptions = new Collection<SubscriptionEntity>()); }
            set { _subscriptions = value; }
        }

        public virtual string CompanyCode { get; set; }
    }
}
