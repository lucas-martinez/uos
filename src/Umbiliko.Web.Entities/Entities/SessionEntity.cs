﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Entities
{
    using Contracts;
    using Results;

    public class SessionEntity : ICompanyEntity<Guid>
    {
        Guid? _identifier;
        DateTime? _createdUtc;

        public virtual string AccessToken { get; set; }

        public virtual AccountEntity Account { get; set; }

        public virtual CompanyEntity Company { get; set; }

        public virtual string CompanyCode { get; set; }

        public virtual DateTime CreatedUtc
        {
            get { return _createdUtc ?? (_createdUtc = DateTime.UtcNow).Value; }
            set { _createdUtc = value; }
        }

        public virtual DomainEntity Domain { get; set; }

        public virtual string DomainName { get; set; }

        public virtual DateTime? ExpiresUtc { get; set; }

        public virtual string Fingerprint { get; set; }

        public virtual Guid Id
        {
            get { return _identifier ?? (_identifier = Guid.NewGuid()).Value; }
            set { _identifier = value; }
        }

        public virtual string IpAddress { get; set; }

        public virtual SegmentEntity Segment { get; set; }

        public virtual string SegmentCode { get; set; }

        public virtual UserEntity User { get; set; }

        public virtual string UserName { get; set; }
        
        public static implicit operator SessionInfo(SessionEntity entity)
        {
            return entity == null ? null : new SessionInfo
            {
                AccessToken = entity.AccessToken,
                AccessTokenSecret = null,
                Account = entity.Account,
                DomainName = entity.DomainName,
                ExpiresUtc = entity.ExpiresUtc,
                Fingerprint = null,
                Id = entity.Id,
                IpAddress = entity.IpAddress,
                IssuedUtc = entity.CreatedUtc,
                User = entity.User
            };
        }
    }
}