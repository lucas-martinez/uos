﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Entities
{
    public class RobotRequestEntity
    {
        public virtual string StatusCode { get; set; }

        public virtual RobotEntity Robot { get; set; }

        public virtual Guid RobotId { get; set; }

        public virtual string RequestData { get; set; }

        public virtual DateTime RequestUtc { get; set; }

        public virtual AgentTasks Task { get; set; }
    }
}
