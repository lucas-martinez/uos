﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Entities
{
    using Constants;
    using Contracts;

    public class ActivityEntity : ICompanyEntity<int>
    {
        DateTime? _updateUtc;

        public virtual string AccessLevel { get; set; } = AccessibilityLevels.Private;

        public virtual string ActivityName { get; set; }

        public virtual string ActivityType { get; set; }

        public virtual string ActionName { get; set; }

        public virtual string Attributes { get; set; }
        
        public virtual CompanyEntity Company { get; set; }

        public virtual string CompanyCode { get; set; }

        public virtual string ControllerName { get; set; }

        public virtual string Description { get; set; }

        public virtual FeatureEntity Feature { get; set; }

        public virtual string FeatureName { get; set; }

        public virtual string HttpVerbs { get; set; }

        public virtual int Id { get; set; }

        public virtual ProductEntity Product { get; set; }

        public virtual string ProductName { get; set; }

        public virtual string ResponseType { get; set; }

        public virtual string Roles { get; set; }

        public virtual DateTime UpdateUtc
        {
            get { return _updateUtc ?? (_updateUtc = DateTime.UtcNow).Value; }
            set { _updateUtc = value; }
        }
    }
}
