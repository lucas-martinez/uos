﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Entities
{
    using Contracts;

    public class ClaimEntity : IEntity<int>
    {
        public virtual string ClaimType { get; set; }

        public virtual string ClaimValue { get; set; }

        public virtual string ClaimValueType { get; set; }
        
        public virtual DateTime CreatedUtc { get; set; }
        
        public virtual int Id { get; set; }
        
        public virtual string LoginKey { get; set; }

        public virtual string LoginProvider { get; set; }

        public virtual UserEntity User { get; set; }

        public virtual string UserName { get; set; }
    }
}
