﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Entities
{
    using Constants;
    using Contracts;

    public class TokenEntity
    {
        public virtual string SegmentCode { get; set; }

        public virtual SegmentEntity Segment { get; set; }

        public virtual CompanyEntity Company { get; set; }

        public virtual string CompanyCode { get; set; }

        public virtual DateTime? ExpiresUtc { get; set; }

        public virtual DateTime IssuedUtc { get; set; }
        
        public virtual TokenTypes Purpose { get; set; }

        public virtual string TokenValue { get; set; }

        public virtual string UserName { get; set; }

        public virtual UserEntity User { get; set; }

        public static implicit operator UserInfo(TokenEntity entity)
        {
            if (entity == null || entity.User == null) return null;

            var user = new UserInfo();

            user.UpdateFrom(entity.User);

            return user;
        }
    }
}
