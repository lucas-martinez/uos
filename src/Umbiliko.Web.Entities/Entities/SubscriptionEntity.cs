﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Entities
{
    using Contracts;

    public class SubscriptionEntity : IEntity<int>
    {
        ICollection<ChargeEntity> _charges;
        DateTime? _sinceUtc;
        
        public virtual ICollection<ChargeEntity> Charges
        {
            get { return _charges ?? (_charges = new Collection<ChargeEntity>()); }
            set { _charges = value; }
        }

        public virtual CompanyEntity Company { get; set; }

        public virtual string CompanyCode { get; set; }

        public virtual SegmentEntity Consumer { get; set; }

        public virtual string ConsumerCode { get; set; }

        public virtual CompanyEntity Customer { get; set; }

        public virtual string CustomerCode { get; set; }

        public virtual FeatureEntity Feature { get; set; }

        public virtual string FeatureName { get; set; }

        public virtual int Id { get; set; }

        public virtual ProductEntity Product { get; set; }

        public virtual string ProductName { get; set; }

        public virtual DateTime SinceUtc
        {
            get { return _sinceUtc ?? (_sinceUtc = DateTime.UtcNow).Value; }
            set { _sinceUtc = value; }
        }

        public virtual string Status { get; set; }
        
        public virtual DateTime? UntilUtc { get; set; }
    }
}
