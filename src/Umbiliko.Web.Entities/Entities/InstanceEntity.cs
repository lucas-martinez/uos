﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Entities
{
    using Contracts;

    public class InstanceEntity : ICompanyEntity<Guid>
    {
        Guid? _identity;
        DateTime? _updateUtc;

        public virtual CompanyEntity Company { get; set; }

        public virtual string CompanyCode { get; set; }

        public virtual DomainEntity Domain { get; set; }

        public virtual string DomainName { get; set; }

        public virtual Guid Id
        {
            get { return _identity ?? (_identity = Guid.NewGuid()).Value; }
            set { _identity = value; }
        }

        public virtual string IpAddress { get; set; }
        
        public virtual string MachineName { get; set; }

        public virtual int ProcessId { get; set; }
        
        public virtual string ProductVersion { get; set; }

        public virtual DateTime UpdateUtc
        {
            get { return _updateUtc ?? (_updateUtc = DateTime.UtcNow).Value; }
            set { _updateUtc = value; }
        }
    }
}
