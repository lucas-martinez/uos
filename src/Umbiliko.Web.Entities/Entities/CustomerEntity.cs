﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Entities
{
    using Contracts;

    public class CustomerEntity : IEntity<int>
    {
        public virtual CompanyEntity Company { get; set; }

        public virtual CompanyEntity CustomerCompany { get; set; }
        
        public virtual string CustomerTrademark { get; set; }

        public int Id { get; set; }

        public virtual string Trademark { get; set; }
    }
}
