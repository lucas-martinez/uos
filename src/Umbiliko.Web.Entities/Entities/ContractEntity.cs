﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Uos.Web.Entities
{
    using Contracts;

    public class ContractEntity : IEntity<int>
    {
        Guid? _clientId;
        ICollection<ChargeEntity> _charges;
        DateTime? _sinceUtc;
        ICollection<SubscriptionEntity> _subscriptions;

        public virtual Guid ClientId
        {
            get { return _clientId ?? (_clientId = Guid.NewGuid()).Value; }
            set { _clientId = value; }
        }

        public virtual string ClientSecret { get; set; }

        public virtual CompanyEntity Company { get; set; }
        
        public virtual string CompanyCode { get; set; }

        public virtual SegmentEntity Consumer { get; set; }

        public virtual string ConsumerCode { get; set; }

        public virtual decimal Credit { get; set; }

        public virtual string CountryCode { get; set; }

        public virtual string CurrencyCode { get; set; }


        public virtual CompanyEntity Customer { get; set; }
        
        public virtual string CustomerCode { get; set; }

        public virtual int Id { get; set; }

        public virtual ProductEntity Product { get; set; }

        public virtual string ProductName { get; set; }

        public virtual ICollection<ChargeEntity> Charges
        {
            get { return _charges ?? (_charges = new Collection<ChargeEntity>()); }
            set { _charges = value; }
        }

        public virtual DateTime SinceUtc
        {
            get { return _sinceUtc ?? (_sinceUtc = DateTime.UtcNow).Value; }
            set { _sinceUtc = value; }
        }

        public virtual ICollection<SubscriptionEntity> Subscriptions
        {
            get { return _subscriptions ?? (_subscriptions = new Collection<SubscriptionEntity>()); }
            set { _subscriptions = value; }
        }
    }
}
