﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Contracts
{
    using Entities;

    public interface ICompanyEntity<TIdentifier> : IEntity<TIdentifier>
        where TIdentifier : IComparable<TIdentifier>, IEquatable<TIdentifier>
    {
        CompanyEntity Company { get; set; }

        string CompanyCode { get; set; }  
    }
}
