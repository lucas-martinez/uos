﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Contracts
{
    using Entities;

    public interface IUmbilikoDbInitialization
    {
        void Seed(UmbilikoDbContext dbContext);
    }
}
