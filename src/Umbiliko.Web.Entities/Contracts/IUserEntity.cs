﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Contracts
{
    using Entities;

    public interface IUserEntity<TIdentifier> : IEntity<TIdentifier>
        where TIdentifier : IComparable<TIdentifier>, IEquatable<TIdentifier>
    {
        UserEntity User { get; set; }

        string UserName { get; set; }  
    }
}
