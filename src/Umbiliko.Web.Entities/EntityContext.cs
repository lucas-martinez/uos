﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web
{
    using Assertion;

    public abstract class EntityContext
    {
        readonly DbContext _dbContext;

        protected EntityContext(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        internal protected DbContext DbContext
        {
            get { return _dbContext; }
        }

        public IResult SaveChanges()
        {
            return DbContext.TrySaveChanges();
        }

        public async Task<IResult> SaveChangesAsync()
        {
            return await DbContext.TrySaveChangesAsync();
        }
    }

    public class EntityContext<TEntity> : EntityContext
        where TEntity : class
    {
        readonly TEntity _entity;

        protected EntityContext(TEntity entity, DbContext dbContext)
            : base(dbContext)
        {
            _entity = entity;
        }

        public TEntity Entity
        {
            get { return _entity; }
        }

        public static implicit operator TEntity(EntityContext<TEntity> context)
        {
            return context != null ? context.Entity : null;
        }
    }
}
