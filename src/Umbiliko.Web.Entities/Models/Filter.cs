﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Models
{
    public class Filter
    {
        public ICollection<string> Activities { get; set; }

        public ICollection<string> Companies { get; set; }

        public ICollection<string> Domains { get; set; }

        public ICollection<string> Emails { get; set; }

        public ICollection<string> Features { get; set; }

        public bool HasActivities { get { return Activities != null && Activities.Count > 0; } }

        public bool HasCompany { get { return Companies != null && Companies.Count > 0; } }

        public bool HasDomains { get { return Domains != null && Domains.Count > 0; } }

        public bool HasEmails { get { return Emails != null && Emails.Count > 0; } }

        public bool HasFeatures { get { return Features != null && Features.Count > 0; } }

        public bool HasMachines { get { return Machines != null && Machines.Count > 0; } }

        public bool HasPhones { get { return Phones != null && Phones.Count > 0; } }

        public bool HasProducts { get { return Products != null && Products.Count > 0; } }

        public bool HasRoles { get { return Roles != null && Roles.Count > 0; } }

        public bool HasTerms { get { return Terms != null && Terms.Count > 0; } }

        public bool HasClients { get { return Clients != null && Clients.Count > 0; } }

        public bool HasUsers { get { return Users != null && Users.Count > 0; } }

        public ICollection<string> Machines { get; set; }

        public ICollection<string> Phones { get; set; }

        public ICollection<string> Products { get; set; }

        public ICollection<string> Roles { get; set; }

        public ICollection<string> Terms { get; set; }

        public ICollection<string> Clients { get; set; }

        public ICollection<string> Users { get; set; }

        public static implicit operator bool(Filter filter)
        {
            if (filter == null) return false;

            return filter.HasActivities || filter.HasClients || filter.HasCompany
                || filter.HasDomains || filter.HasEmails || filter.HasFeatures
                || filter.HasMachines || filter.HasPhones || filter.HasProducts
                || filter.HasRoles || filter.HasTerms || filter.HasUsers;
        }
    }
}
