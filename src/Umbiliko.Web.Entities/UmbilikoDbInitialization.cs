﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Entities
{
    using Constants;
    using Contracts;

    internal class UmbilikoDbInitialization : IUmbilikoDbInitialization
    {
        public UmbilikoDbInitialization()
        {
        }

        public void Seed(UmbilikoDbContext dbContext)
        {
            var company = dbContext.AddOrUpdateCompanyAsyn(
                companyCode: Defaults.InternalCustomerCode,
                companyName: Defaults.ApplicationCompanyName,
                companyType: CompanyTypes.OneManCompany).Result;
            
            var product = company.AddOrUpdateProductAsync(
                name: Defaults.ApplicationProductName, 
                description: Strings.Defaults.ApplicationProductDescription).Result;
           
            product.AddOrUpdateFeature(
                Features.Administration, Strings.Features.Administration,
                new Dictionary<string, string>
                {
                    { Activities.CreateCompanies, Strings.Activities.CreateCompanies },
                    { Activities.DestroyCompanies, Strings.Activities.DestroyCompanies },
                    { Activities.DisplayCompanies, Strings.Activities.DisplayCompanies },
                    { Activities.DownloadCompanies, Strings.Activities.DownloadCompanies },
                    { Activities.ListCompanies, Strings.Activities.ListCompanies },
                    { Activities.ReadCompanies, Strings.Activities.ReadCompanies },
                    { Activities.UpdateCompanies, Strings.Activities.UpdateCompanies },
                    { Activities.UploadCompanies, Strings.Activities.UploadCompanies },

                    { Activities.CreateUsers, Strings.Activities.CreateUsers },
                    { Activities.DestroyUsers, Strings.Activities.DestroyUsers },
                    { Activities.DisplayUsers, Strings.Activities.DisplayUsers },
                    { Activities.DownloadUsers, Strings.Activities.DownloadUsers },
                    { Activities.ListUsers, Strings.Activities.ListUsers },
                    { Activities.ReadUsers, Strings.Activities.ReadUsers },
                    { Activities.UpdateUsers, Strings.Activities.UpdateUsers },
                    { Activities.UploadUsers, Strings.Activities.UploadUsers }
                });

            product.AddOrUpdateFeature(
                Features.Authentication, Strings.Features.Authentication,
                new Dictionary<string, string>
                {
                    { Activities.CreateAccounts, Strings.Activities.CreateAccounts },
                    { Activities.DestroyAccounts, Strings.Activities.DestroyAccounts },
                    { Activities.DisplayAccounts, Strings.Activities.DisplayAccounts },
                    { Activities.DownloadAccounts, Strings.Activities.DownloadAccounts },
                    { Activities.ReadAccounts, Strings.Activities.ReadAccounts },
                    { Activities.UpdateAccounts, Strings.Activities.UpdateAccounts },
                    { Activities.UploadAccounts, Strings.Activities.UploadAccounts },

                    { Activities.CreateSegments, Strings.Activities.CreateSegments },
                    { Activities.DestroySegments, Strings.Activities.DestroySegments },
                    { Activities.DisplaySegments, Strings.Activities.DisplaySegments },
                    { Activities.DownloadSegments, Strings.Activities.DownloadSegments },
                    { Activities.ReadSegments, Strings.Activities.ReadSegments },
                    { Activities.UpdateSegments, Strings.Activities.UpdateSegments },
                    { Activities.UploadSegments, Strings.Activities.UploadSegments }
                });

            product.AddOrUpdateFeature(
                Features.Authorization, Strings.Features.Authorization,
                new Dictionary<string, string>
                {
                    { Activities.CreateConsumers, Strings.Activities.CreateConsumers },
                    { Activities.DestroyConsumers, Strings.Activities.DestroyConsumers },
                    { Activities.DisplayConsumers, Strings.Activities.DisplayConsumers },
                    { Activities.DownloadConsumers, Strings.Activities.DownloadConsumers },
                    { Activities.ReadConsumers, Strings.Activities.ReadConsumers },
                    { Activities.UpdateConsumers, Strings.Activities.UpdateConsumers },
                    { Activities.UploadConsumers, Strings.Activities.UploadConsumers },

                    { Activities.CreateSubscriptions, Strings.Activities.CreateSubscriptions },
                    { Activities.DestroySubscriptions, Strings.Activities.DestroySubscriptions },
                    { Activities.DisplaySubscriptions, Strings.Activities.DisplaySubscriptions },
                    { Activities.DownloadSubscriptions, Strings.Activities.DownloadSubscriptions },
                    { Activities.ReadSubscriptions, Strings.Activities.ReadSubscriptions },
                    { Activities.UpdateSubscriptions, Strings.Activities.UpdateSubscriptions },
                    { Activities.UploadSubscriptions, Strings.Activities.UploadSubscriptions }
                });

            product.AddOrUpdateFeature(
                Features.Automation, Strings.Features.Authomation,
                new Dictionary<string, string>
                {
                });

            product.AddOrUpdateFeature(
                Features.Configuration, Strings.Features.Configuration,
                new Dictionary<string, string>
                {
                    { Activities.CreateActivities, Strings.Activities.CreateActivities },
                    { Activities.DestroyActivities, Strings.Activities.DestroyActivities },
                    { Activities.DisplayActivities, Strings.Activities.DisplayActivities },
                    { Activities.ListActivities, Strings.Activities.ListActivities },
                    { Activities.ReadActivities, Strings.Activities.ReadActivities },
                    { Activities.UpdateActivities, Strings.Activities.UpdateActivities },

                    { Activities.CreateDomains, Strings.Activities.CreateDomains },
                    { Activities.DestroyDomains, Strings.Activities.DestroyDomains },
                    { Activities.DisplayDomains, Strings.Activities.DisplayDomains },
                    { Activities.DownloadDomains, Strings.Activities.DownloadDomains },
                    { Activities.ReadDomains, Strings.Activities.ReadDomains },
                    { Activities.ListDomains, Strings.Activities.ListDomains },
                    { Activities.UpdateDomains, Strings.Activities.UpdateDomains },
                    { Activities.UploadDomains, Strings.Activities.UploadDomains },

                    { Activities.CreateFeatures, Strings.Activities.CreateFeatures },
                    { Activities.DestroyFeatures, Strings.Activities.DestroyFeatures },
                    { Activities.DisplayFeatures, Strings.Activities.DisplayFeatures },
                    { Activities.ListFeatures, Strings.Activities.ListFeatures },
                    { Activities.ReadFeatures, Strings.Activities.ReadFeatures },
                    { Activities.UpdateFeatures, Strings.Activities.UpdateFeatures },
                    

                    { Activities.CreateInstances, Strings.Activities.CreateInstances },
                    { Activities.DestroyInstances, Strings.Activities.DestroyInstances },
                    { Activities.DisplayInstances, Strings.Activities.DisplayInstances },
                    { Activities.ReadInstances, Strings.Activities.ReadInstances },
                    { Activities.UpdateInstances, Strings.Activities.UpdateInstances },

                    { Activities.CreateProducts, Strings.Activities.CreateProducts },
                    { Activities.DestroyProducts, Strings.Activities.DestroyProducts },
                    { Activities.DisplayProducts, Strings.Activities.DisplayProducts },
                    { Activities.DownloadProducts, Strings.Activities.DownloadProducts },
                    { Activities.ListProducts, Strings.Activities.ListProducts },
                    { Activities.ReadProducts, Strings.Activities.ReadProducts },
                    { Activities.UpdateProducts, Strings.Activities.UpdateProducts },
                    { Activities.UploadProducts, Strings.Activities.UploadProducts }
                });

            foreach (var feature in product.Entity.Features)
            {
                foreach (var activity in feature.Activities)
                {
                    activity.Roles = Roles.Administrator;
                }
            }

            var user = dbContext.AddOrUpdateUserAsync(
                userName: Defaults.AdminName, 
                emailAddress: Defaults.AdminEmailAddress, 
                phoneNumber: Defaults.AdminMobileNumber, 
                password: Defaults.AdminPassword).Result;

            var guest = dbContext.AddOrUpdateUserAsync(
                userName: Defaults.GuestName).Result;

            var segment = product.Company.AddOrUpdateSegment(
                segmentCode: Defaults.InernalConsumerCode, 
                environment: Environments.Production, 
                description: Strings.Defaults.PrincipalSegmentDescription);

            var account = segment.AddOrUpdateAccountAsync(user, Roles.Administrator, Roles.Manager, Roles.Scheduler).Result;

            var localhost = dbContext.AddOrUpdateDomainAsync(
                domainName: DomainInfo.LocalHost().DomainName, 
                environment: Environments.Development, 
                description: Strings.Defaults.ApplicationDomainDescription).Result;

            var domain = product.AddOrUpdateDomain(Defaults.LocalHost, Environments.Production, "localhost");
            
            // Creates subscriptions
            var consumer = product.AddOrUpdateConsumer(segment, 
                credit: 0, 
                countryCode: Countries.CRI, 
                currencyCode: Currencies.USD);

            product.Entity.Features.ForEach(feature => { new FeatureContext(feature, product).AddOrUpdateSubscription(consumer); });
            
            var save = product.SaveChanges();

            if (!save.Success)
            {
                throw new Exception(string.Join("|", save.Messages));
            }
        }
    }
}