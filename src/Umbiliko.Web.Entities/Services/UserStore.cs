﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Uos.Web.Services
{
    using Assertion;
    using Constants;
    using Contracts;
    using Entities;
    using Results;
    using Utilities;

    public abstract class UserStore : IUserStore
    {
        DbContext _dbContext;
        IQueryable<LoginEntity> _dbLogins;
        IQueryable<SessionEntity> _dbSessions;
        IQueryable<TokenEntity> _dbTokens;
        IQueryable<UserEntity> _dbUsers;
        PasswordHasher _passwordHasher;
        PasswordValidator _passwordValidator;
        
        public UserStore(DbContext dbContext, PasswordStrength requiredPasswordStrength)
        {
            _dbContext = dbContext;
            _passwordValidator = new PasswordValidator(requiredPasswordStrength);
        }

        public DbContext DbContext
        {
            get { return _dbContext; }
        }

        protected IQueryable<LoginEntity> DbLogins
        {
            get
            {
                return _dbLogins ?? (_dbLogins = DbContext.Set<LoginEntity>()
                  .Include(e => e.User.Accounts)
                  .Include(e => e.User.Accounts.Select(a => a.Company))
                  .Include(e => e.User.Accounts.Select(a => a.Segment))
                  .Include(e => e.User.Claims)
                  .Include(e => e.User.Logins));
            }
        }
        
        protected IQueryable<TokenEntity> DbTokens
        {
            get
            {
                return _dbTokens ?? (_dbTokens = DbContext.Set<TokenEntity>()
                  .Include(e => e.User.Accounts)
                  .Include(e => e.User.Accounts.Select(a => a.Company))
                  .Include(e => e.User.Accounts.Select(a => a.Segment))
                  .Include(e => e.User.Claims)
                  .Include(e => e.User.Logins));
            }
        }

        protected IQueryable<UserEntity> DbUsers
        {
            get
            {
                return _dbUsers ?? (_dbUsers = DbContext.Set<UserEntity>()
                  .Include(e => e.Accounts)
                  .Include(e => e.Accounts.Select(a => a.Company))
                  .Include(e => e.Accounts.Select(a => a.Segment))
                  .Include(e => e.Claims)
                  .Include(e => e.Logins));
            }
        }

        public PasswordHasher PasswordHasher
        {
            get { return _passwordHasher ?? (_passwordHasher = new PasswordHasher()); }
        }

        public PasswordValidator PasswordValidator
        {
            get { return _passwordValidator; }
        }

        public async Task<UserResult> CreateUserAsync(UserInfo data)
        {
            //var result = await GetUserAsync(data);

            //if (result == null || !result.Success) return result;

            IResult result;

            if (data == null) return UserResult.Fail("NullArgument");

            var dbContext = DbContext;

            var dbSet = dbContext.Set<UserEntity>();

            var user = await dbSet.FirstOrDefaultAsync(e => e.UserName == data.Name);

            if (user != null) return UserResult.Fail("UserExist");

            user = dbSet.Create();

            user.UserName = data.Name;
            user.EmailAddress = data.Email;
            user.PasswordHash = data.PasswordHash;
            user.PhoneNumber = data.PhoneNumber;

            dbSet.Add(user);

            result = await dbContext.TrySaveChangesAsync();

            return result.Success ? UserResult.Assert(data) : UserResult.Fail(result.Messages);
        }

        public Task<UserResult> DestroyUserAsync(UserInfo data)
        {
            throw new NotImplementedException();
        }

        //public abstract Task<UserResult> FindUserBySessionIdAsync(Guid sessionId);

        public async Task<UserResult> FindUserAsync(string userNameOrEmailAddress)
        {
            var find = await FindUserEntityAsync(userNameOrEmailAddress);

            if (!find.Success) return UserResult.Fail(find.Messages);
            
            return UserResult.Assert(find.Value);
        }

        public async Task<UserResult> FindUserAsync(string userNameOrEmailAddress, string password)
        {
            var find = await FindUserEntityAsync(userNameOrEmailAddress);

            if (!find.Success) return UserResult.Fail(find.Messages);

            var user = find.Value;
            
            //
            // Validate password

            if (VerifyHashedPassword(find.Value.PasswordHash, password))
            {
                user.AccessFailedCount = 0;

                user.LockoutEndDateUtc = null;

                var save = await DbContext.TrySaveChangesAsync();

                if (!save.Success) return UserResult.Fail(save.Messages);
            }
            else
            {
                user.AccessFailedCount++;
                //
                // Set lockout

                IResult save;

                if (user.LockoutEnabled == true)
                {
                    if (user.AccessFailedCount >= Defaults.AccessFailedLock)
                    {
                        user.LockoutEndDateUtc = DateTime.UtcNow + TimeSpan.FromMinutes(3);

                        save = await DbContext.TrySaveChangesAsync();

                        if (!save.Success) return UserResult.Fail(save.Messages);

                        return UserResult.Fail("Lockout");
                    }
                }

                save = await DbContext.TrySaveChangesAsync();

                if (!save.Success) return UserResult.Fail(save.Messages);

                return UserResult.Fail("InvalidPassword");
            }

            return UserResult.Assert(user);
        }

        public async Task<UserResult> FindUserAsync(UserInfo data)
        {
            UserResult user = null;

            /*if (data.UserId.HasValue)
            {
                user = await FindByIdAsync(data.UserId.Value);
            }*/

            if (user == null && data.Email != null)
            {
                user = await FindUserByEmailAsync(data.Email);
            }

            if (user == null && data.Logins != null)
            {
                foreach (var login in data.Logins)
                {
                    user = await FindUserByLoginAsync(login.LoginProvider, providerKey: login.LoginKey);

                    if (user.Success) break;
                }
            }

            return user;
        }

        public async Task<UserResult> FindUserByEmailAsync(string email)
        {
            var entity = await DbUsers.SingleOrDefaultAsync(e => e.EmailAddress == email);

            return UserResult.Assert(entity);
        }

        public async Task<UserResult> FindUserByLoginAsync(string loginProvider, string providerKey)
        {
            var entity = await DbLogins.SingleOrDefaultAsync(e => e.LoginProvider == loginProvider && e.LoginKey == providerKey);

            return UserResult.Assert(entity);
        }

        protected async Task<IResult<UserEntity>> FindUserEntityAsync(string userNameOrEmalAddress)
        {
            var entity = await DbUsers.FirstOrDefaultAsync(e => e.UserName == userNameOrEmalAddress);

            if (entity == null)
            {
                var regex = new RegexUtilities();

                if (regex.IsValidEmailAddress(userNameOrEmalAddress))
                {
                    entity = await DbUsers.FirstOrDefaultAsync(e => e.EmailAddress == userNameOrEmalAddress);
                }
            }

            return await ValidateUserEntityAsync(entity);
        }
        protected async Task<IResult<UserEntity>> FindUserEntityAsync(string loginProvider, string loginKey)
        {
            var entity = await DbLogins.SingleOrDefaultAsync(e => e.LoginProvider == loginProvider && e.LoginKey == loginKey);

            if (entity == null) return Result<UserEntity>.Fail("NotFount");

            return await ValidateUserEntityAsync(entity.User);
        }

        public async Task<LoginEntity> FirstConfirmedLoginsAsync(UserInfo data)
        {
            var dbContext = DbContext;

            var dbSet = dbContext.Set<LoginEntity>();

            foreach (var item in data.Logins)
            {
                var entity = await dbSet
                    .Include(e => e.User)
                    .Include(e => e.User.Accounts)
                    .Include(e => e.User.Accounts.Select(x => x.Company))
                    .Include(e => e.User.Accounts.Select(x => x.Segment))
                    .Include(e => e.User.Logins)
                    .SingleOrDefaultAsync(e => e.LoginConfirmed && e.LoginKey == item.LoginKey && e.LoginProvider == item.LoginProvider);

                if (entity != null)
                {
                    return entity;
                }
            }

            return null;
        }

        public async Task<IResult> RefreshClaimsAsync(UserInfo user, LoginInfo login, ClaimsIdentity identity)
        {
            var dbContext = DbContext;

            var dbSet = dbContext.Set<ClaimEntity>();

            var count = 0;

            foreach (var claim in identity.Claims)
            {
                if (!user.Claims.Any(c => c.Type == claim.Type && c.Value == claim.Value && c.Issuer == claim.Issuer))
                {
                    user.Claims.Add(claim);

                    count++;

                    var entity = dbSet.Create();

                    entity.CreatedUtc = login.LoginUtc;
                    entity.LoginKey = login.LoginKey;
                    entity.LoginProvider = claim.Issuer;
                    entity.ClaimType = claim.Type;
                    entity.ClaimValue = claim.Value;
                    entity.ClaimValueType = claim.ValueType;
                    entity.UserName = user.Name;

                    dbSet.Add(entity);
                }
            }

            return await dbContext.TrySaveChangesAsync();
        }

        public void RefreshLogins(UserInfo data, IList<LoginEntity> logins)
        {
            foreach (var entity in logins.Where(e => !data.Logins.Any(o => o.LoginProvider == e.LoginProvider && o.LoginKey == e.LoginKey)))
            {
                data.Logins.Add(new LoginInfo
                {
                    AccessToken = entity.LoginToken,
                    ExpiresUtc = entity.ExpiresUtc,
                    Label = entity.Label,
                    LoginConfirmed = entity.LoginConfirmed,
                    LoginCount = entity.LoginCount,
                    LoginKey = entity.LoginKey,
                    LoginProvider = entity.LoginProvider,
                    LoginUtc = entity.LoginUtc,
                });
            }
        }

        public async Task<IResult> RefreshLoginsAsync(UserInfo user)
        {
            var dbContext = DbContext;

            var dbSet = dbContext.Set<LoginEntity>();

            var logins = await dbSet.Where(e => e.UserName == user.Name).ToListAsync();

            foreach (var login in user.Logins)
            {
                var entity = logins.FirstOrDefault(e => e.LoginProvider == login.LoginProvider && e.LoginKey == login.LoginKey);

                if (entity.NotNull())
                {
                    if (entity.LoginUtc < login.LoginUtc)
                    {
                        entity.ExpiresUtc = login.ExpiresUtc;
                        entity.LoginCount++;
                        entity.Label = login.Label;
                        entity.LoginToken = login.AccessToken;
                        entity.LoginUtc = login.LoginUtc;
                    }

                    if (login.LoginConfirmed)
                    {
                        entity.LoginConfirmed = true;
                    }
                    else
                    {
                        login.LoginConfirmed = entity.LoginConfirmed;
                    }
                }
                else
                {
                    entity = dbSet.Create();

                    entity.ExpiresUtc = login.ExpiresUtc;
                    entity.Label = login.Label;
                    entity.LoginConfirmed = login.LoginConfirmed;
                    entity.LoginCount = 1;
                    entity.LoginKey = login.LoginKey;
                    entity.LoginProvider = login.LoginProvider;
                    entity.LoginToken = login.AccessToken;
                    entity.UserName = user.Name;
                    entity.LoginUtc = login.LoginUtc;

                    dbSet.Add(entity);

                    login.LoginCount = 1;
                }
            }

            RefreshLogins(user, logins);

            return await dbContext.TrySaveChangesAsync();
        }
        
        protected async Task<IResult<UserEntity>> ValidateUserEntityAsync(UserEntity entity)
        {
            if (entity == null) return Result<UserEntity>.Fail("NotFound");

            if (entity.LockoutEndDateUtc.HasValue)
            {
                if (entity.LockoutEndDateUtc.Value >= DateTime.UtcNow)
                {
                    return Result<UserEntity>.Fail("Lockout");
                }

                entity.AccessFailedCount = 0;

                entity.LockoutEndDateUtc = null;

                var save = await DbContext.TrySaveChangesAsync();

                if (!save.Success) return Result<UserEntity>.Fail(save.Messages);
            }

            return Result.Ok(entity);
        }

        public IResult ValidateEmailAddress(string emailAddress)
        {
            if (string.IsNullOrEmpty(emailAddress)) return Result.Fail("EmailAddressRequired");

            if (emailAddress.Length < 4) return Result.Fail("EmailAddressInvalid");

            return Result.Default;
        }

        public IResult ValidatePassword(string password)
        {
            //if (string.IsNullOrEmpty(password)) return Result.Fail("EmailAddressRequired");

            //if (password.Length < 4) return Result.Fail("EmailAddressInvalid");

            return Result.Default;
        }

        public bool VerifyHashedPassword(string hashedPassword, string providedPassword)
        {
            return PasswordHasher.VerifyHashedPassword(hashedPassword, providedPassword);
        }

        public IResult ValidateUserName(string userName)
        {
            if (string.IsNullOrEmpty(userName)) return Result.Fail("UserNameRequired");

            if (userName.Length < 4) return Result.Fail("UserNameTooShort");

            return Result.Default;
        }

        public async Task<UserResult> UpdateAsync(UserRequest request)
        {
            var result = await FindUserAsync(request.Data);

            if (result == null || !result.Success) return result;

            return result;
        }
    }
}
