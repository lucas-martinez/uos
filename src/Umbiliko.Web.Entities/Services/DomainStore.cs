﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Services
{
    using Constants;
    using Contracts;
    using Entities;

    public class DomainStore : DomainStoreBase
    {
        readonly DbContext _dbContext;
        IQueryable<DomainEntity> _dbDomains;
        IQueryable<SubscriptionEntity> _dbSubscriptions;

        public DomainStore(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public DbContext DbContext
        {
            get { return _dbContext; }
        }

        private IQueryable<DomainEntity> DbDomains
        {
            get
            {
                return _dbDomains ?? (_dbDomains = DbContext.Set<DomainEntity>()
                    .Include(e => e.Company)
                    .Include(e => e.Consumer)
                    .Include(e => e.Customer)
                    .Include(e => e.Product)
                    .Include(e => e.Product.Activities)
                    //.Include(e => e.Product.Features)
                    //.Include(e => e.Product.Subscriptions)
                    );
            }
        }

        private IQueryable<SubscriptionEntity> DbSubscriptions
        {
            get
            {
                return _dbSubscriptions ?? (_dbSubscriptions = DbContext.Set<SubscriptionEntity>()
                    //.Include(e => e.Company)
                    //.Include(e => e.Consumer)
                    //.Include(e => e.Customer)
                    //.Include(e => e.Product)
                    //.Include(e => e.Product.Activities)
                    //.Include(e => e.Product.Features)
                    //.Include(e => e.Product.Subscriptions)
                    );
            }
        }

        private AccessLevels ParseLevel(string value)
        {
            AccessLevels level;

            return Enum.TryParse(value, out level) ? level : AccessLevels.Private;
        }
        
        private List<FeatureInfo> GetFeatures(DomainEntity entity)
        {
            var activities = entity.Product.Activities
                .Where(e => e.ProductName == entity.ProductName)
                .Select(e => new { e.FeatureName, e.ActivityName, e.AccessLevel, e.Roles })
                .GroupBy(e => new { e.FeatureName })
                .ToList();

            return activities.Select(group => new FeatureInfo
            {
                Name = group.Key.FeatureName,
                Activities = group.Select(o => new ActivityInfo
                {
                    AccessLevel = ParseLevel(o.AccessLevel),
                    Name = o.ActivityName,
                    Roles = o.Roles.Split(Characters.Space)
                }).ToList()
            }).ToList();
        }

        private IQueryable<SubscriptionEntity> QueryableSubscriptions(DomainEntity entity)
        {
            IQueryable<SubscriptionEntity> source = DbSubscriptions;

            if (entity.CustomerCode == null)
            {
                source = source.Where(e => e.ProductName == entity.ProductName);
            }
            else if (entity.ConsumerCode == null)
            {
                source = source.Where(e => e.CustomerCode == entity.CustomerCode && e.ProductName == entity.ProductName);
            }
            else
            {
                source = source.Where(e => e.CustomerCode == entity.CustomerCode && e.ConsumerCode == entity.ConsumerCode && e.ProductName == entity.ProductName);
            }

            return source;
        }
        
        protected override async Task<DomainInfo> QueryAsync(string domainName)
        {
            var entity = await DbDomains.FirstOrDefaultAsync(e => e.DomainName == domainName);

            if (entity == null) return null;
            
            var subscriptions = QueryableSubscriptions(entity);

            var contracts = await subscriptions.GroupBy(e => new { e.CustomerCode, e.ConsumerCode }).ToListAsync();

            var consumers = contracts.Select(group => new ConsumerInfo
            {
                CompanyCode = group.Key.CustomerCode,
                SegmentCode = group.Key.ConsumerCode,
                Subscriptions = group.Select(o => new SubscriptionInfo
                {
                    FeatureName = o.FeatureName,
                    ExpiresUtc = o.UntilUtc
                }).ToList()
            }).ToList();

            var features = GetFeatures(entity);

            return new DomainInfo
            {
                CompanyCode = entity.CompanyCode,
                CompanyName = entity.Company.CompanyName,
                Consumers = consumers,
                DomainName = entity.DomainName,
                Environment = entity.Environment,
                ExpiresUtc = DateTime.UtcNow + TimeSpan.FromMinutes(5),
                Features = features,
                ProductName = entity.ProductName
            };
        }
    }
}
