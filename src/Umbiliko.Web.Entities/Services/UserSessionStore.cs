﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Services
{
    using Contracts;
    using Entities;
    using Results;
    using Constants;
    using Utilities;

    public class UserSessionStore : UserStore, ISessionStore
    {
        DbSet<SessionEntity> _dbSet;
        IQueryable<SessionEntity> _dbSessions;

        public UserSessionStore(DbContext dbContext, PasswordStrength requiredPasswordStrength)
            : base(dbContext, requiredPasswordStrength)
        {
        }

        private IQueryable<SessionEntity> DbSessions
        {
            get
            {
                return _dbSessions ?? (_dbSessions = DbSet
                  .Include(e => e.Account)
                  .Include(e => e.Account.Company)
                  .Include(e => e.Account.Segment)
                  .Include(e => e.User)
                  .Include(e => e.User.Accounts)
                  .Include(e => e.User.Accounts.Select(a => a.Company))
                  .Include(e => e.User.Accounts.Select(a => a.Segment))
                  .Include(e => e.User.Claims)
                  .Include(e => e.User.Logins));
            }
        }

        private DbSet<SessionEntity> DbSet
        {
            get { return _dbSet ?? (_dbSet = DbContext.Set<SessionEntity>()); }
        }

        public IUserStore UserStore
        {
            get { return this; }
        }

        public async Task<SessionResult> CreateSessionAsync(string userNameOrEmailAddress, string password, string ipAddress, string fingerprint, string domainName)
        {
            var find = await base.FindUserAsync(userNameOrEmailAddress, password);

            if (!find.Success) return SessionResult.Fail(new SessionInfo { User = find.Value }, find.Messages);

            var entity = DbSet.Add(DbSet.Create());

            entity.AccessToken = Convert.ToString(Guid.NewGuid());
            entity.DomainName = domainName;
            entity.ExpiresUtc = entity.CreatedUtc + Defaults.SessionTimeSpan;
            entity.IpAddress = ipAddress;
            entity.UserName = find.Value.Name;
            entity.CompanyCode = null;
            entity.SegmentCode = null;

            var save = await DbContext.TrySaveChangesAsync();

            if (!save.Success) return SessionResult.Fail(save.Messages);

            return new SessionInfo
            {
                AccessToken = entity.AccessToken,
                AccessTokenSecret = null,
                Account = null,
                DomainName = domainName,
                ExpiresUtc = entity.ExpiresUtc,
                Fingerprint = fingerprint,
                IpAddress = ipAddress,
                Id = entity.Id,
                IssuedUtc = entity.CreatedUtc,
                User = find.Value
            };
        }

        public async Task<SessionResult> CreateSessionAsync(LoginInfo login, string ipAddress, string fingerprint, string domainName)
        {
            var find = await FindUserEntityAsync(loginProvider: login.LoginProvider, loginKey: login.LoginKey);
            
            if (!find.Success) return SessionResult.Fail(new SessionInfo { User = find.Value }, find.Messages);

            var entity = DbSet.Add(DbSet.Create());

            entity.AccessToken = Convert.ToString(Guid.NewGuid());
            entity.DomainName = domainName;
            entity.ExpiresUtc = entity.CreatedUtc + Defaults.SessionTimeSpan;
            entity.IpAddress = ipAddress;
            entity.UserName = find.Value.UserName;
            entity.CompanyCode = null;
            entity.SegmentCode = null;

            var save = await DbContext.TrySaveChangesAsync();

            if (!save.Success) return SessionResult.Fail(save.Messages);

            return new SessionInfo
            {
                AccessToken = entity.AccessToken,
                AccessTokenSecret = null,
                Account = null,
                DomainName = domainName,
                ExpiresUtc = entity.ExpiresUtc,
                Fingerprint = fingerprint,
                IpAddress = ipAddress,
                Id = entity.Id,
                IssuedUtc = entity.CreatedUtc,
                User = find.Value
            };
        }

        /// <summary>
        /// Used for domain jump
        /// </summary>
        /// <param name="sessionId"></param>
        /// <param name="domainName"></param>
        /// <returns></returns>
        public async Task<SessionResult> CreateSessionAsync(Guid sessionId, string ipAddress, string fingerprint, string domainName, string companyCode, string segmentCode, bool expirePrevious)
        {
            var entity = await DbSessions.SingleOrDefaultAsync(e => e.Id == sessionId);

            if (entity == null) return SessionResult.Assert(entity);

            if (entity.DomainName != domainName || entity.IpAddress != ipAddress) return SessionResult.Assert(null);

            if (entity.ExpiresUtc.HasValue && entity.ExpiresUtc.Value <= DateTime.UtcNow)
                return new SessionResult
                {
                    Value = entity,
                    Messages = new List<string> { "Expired" },
                    Success = false
                };

            if (expirePrevious && entity.ExpiresUtc.HasValue && entity.ExpiresUtc.Value > DateTime.UtcNow)
            {
                entity.ExpiresUtc = DateTime.UtcNow;
            }

            var userName = entity.UserName;

            entity = DbSet.Add(DbSet.Create());

            entity.AccessToken = Convert.ToString(Guid.NewGuid());
            entity.DomainName = entity.DomainName;
            entity.ExpiresUtc = entity.CreatedUtc + Defaults.SessionTimeSpan;
            entity.IpAddress = ipAddress;
            entity.UserName = userName;
            entity.CompanyCode = companyCode;
            entity.SegmentCode = segmentCode;

            var save = await DbContext.TrySaveChangesAsync();

            if (!save.Success) return SessionResult.Fail(save.Messages);

            return new SessionInfo
            {
                Account = entity.Account,
                Id = entity.Id,
                User = entity.User
            };
        }


        public Task<SessionResult> CreateSessionAsync(string userToken, string ipAddress, string fingerprint, string domainName)
        {
            /*var result = await FindByAccessTokenAsync(token);

            if (result.Success) return result;

            var entity = await DbTokens.SingleOrDefaultAsync(e => e.TokenValue == token);

            return (_result = UserResult.Assert(entity));*/
            throw new NotImplementedException();
        }

        public async Task<SessionResult> CreateUserAsync(SessionInfo model)
        {
            var result = await base.CreateUserAsync(model.User);

            if (!result.Success) return SessionResult.Fail(result.Value, result.Messages);

            return await CreateSessionAsync(model.User.Name, model.User.PasswordHash, model.IpAddress, model.Fingerprint, model.DomainName);
        }

        public async Task<SessionResult> ExpireSessionAsync(Guid sessionId, string ipAddress, string fingerprint, string domainName)
        {
            var entity = await DbSessions.SingleOrDefaultAsync(e => e.Id == sessionId);

            if (entity == null) return SessionResult.Assert(entity);

            if (entity.ExpiresUtc.HasValue && entity.ExpiresUtc.Value > DateTime.UtcNow)
            {
                entity.ExpiresUtc = DateTime.UtcNow;
            }

            var save = await DbContext.TrySaveChangesAsync();

            return new SessionResult
            {
                Value = entity,
                Messages = save.Messages,
                Success = save.Success
            };
        }

        public Task<SessionResult> ExtendSessionAsync(Guid sessionId, string ipAddress, string fingerprint, string domainName, TimeSpan time)
        {
            throw new NotImplementedException();
        }

        public async Task<SessionResult> FindSessionByIdAsync(Guid sessionId)
        {
            var entity = await DbSessions.SingleOrDefaultAsync(e => e.Id == sessionId);

            return SessionResult.Assert(entity);
        }

        public async Task<SessionResult> FindSessionByTokenAsync(string accessToken)
        {
            var entity = await DbSessions.SingleOrDefaultAsync(e => e.AccessToken == accessToken);

            return SessionResult.Assert(entity);
        }

        public async Task<UserResult> FindByIdAsync(string userId)
        {
            Guid id;

            if (Guid.TryParse(userId, out id))
            {
                var find = await FindSessionByIdAsync(id);

                if (find.Success) return find.Value.User;
            }

            var entity = await DbSessions.SingleOrDefaultAsync(e => e.AccessToken == userId);


            if (entity == null) return UserResult.Assert(null);

            SessionInfo session = entity;

            return session.User;
        }
    }
}
