﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web
{
    using Constants;
    using Contracts;
    using Entities;

    public class SessionContext : EntityContext<SessionEntity>
    {
        AccountContext _account;
        CompanyContext _company;
        DomainContext _domain;
        SegmentContext _segment;
        UserContext _user;

        public SessionContext(SessionEntity entity, DbContext dbContext)
            : base(entity, dbContext)
        {
        }

        public SessionContext(SessionEntity entity, AccountContext account, DomainContext domain = null)
            : base(entity, account.DbContext)
        {
            _account = account;
            _company = _account.Company;
            _user = account.User;
            _domain = domain;
        }

        public SessionContext(SessionEntity entity, UserContext user, DomainContext domain = null)
            : base(entity, user.DbContext)
        {
            _user = user;
            _domain = domain;
            _company = domain.Company;
        }

        public AccountContext Account
        {
            get { return _account ?? (_account = AccountContext.FromEntity(Entity.Account, DbContext)); }
        }

        public CompanyContext Company
        {
            get { return _company ?? (_company = CompanyContext.FromEntity(Entity.Company, DbContext)); }
        }

        public DateTime AuthenticationInstant
        {
            get { return Entity.CreatedUtc; }
        }

        public DomainContext Domain
        {
            get { return _domain; }
        }

        public DateTime? ExpiresUtc
        {
            get { return Entity.ExpiresUtc; }
        }

        public Guid Identifier
        {
            get { return Entity.Id; }
        }

        public SegmentContext Segment
        {
            get { return _segment ?? (_segment = SegmentContext.FromEntity(Entity.Segment, DbContext)); }
        }

        public UserContext User
        {
            get { return _user ?? (_user = UserContext.FromEntity(Entity.User, DbContext)); }
        }

        public static SessionContext Create(DomainContext domain, AccountContext account)
        {
            var dbContext = account.DbContext;
            var dbSet = dbContext.Set<SessionEntity>();

            var entity = dbSet.Create();

            entity.Account = account;
            entity.SegmentCode = account.SegmentCode;
            entity.CompanyCode = account.CompanyCode;
            entity.Company = account.Company;
            entity.Domain = domain;
            entity.DomainName = domain.Name;
            entity.Segment = account.Segment;
            entity.User = account.User;
            entity.UserName = account.User.Name;

            dbSet.Add(entity);

            return new SessionContext(entity, account, domain);
        }

        public static SessionContext Create(DomainContext domain, UserContext user)
        {
            var dbContext = user.DbContext;
            var dbSet = dbContext.Set<SessionEntity>();

            var entity = dbSet.Create();

            entity.User = user;
            entity.UserName = user.Name;
            entity.Domain = domain;
            entity.DomainName = domain.Name;

            dbSet.Add(entity);

            return new SessionContext(entity, user, domain);
        }

        public static SessionContext Find(DbContext dbContext, Guid identifier)
        {
            var source = dbContext.Set<SessionEntity>()
                .Include(e => e.Account)
                .Include(e => e.Domain)
                .Include(e => e.User);

            var entity = source.SingleOrDefault(e => e.Id == identifier);

            return FromEntity(entity, dbContext);
        }
        
        public static async Task<SessionContext> FindAsync(DbContext dbContext, Guid identifier)
        {
            var source = dbContext.Set<SessionEntity>()
                .Include(e => e.Account)
                .Include(e => e.Domain)
                .Include(e => e.User);

            var entity = await source.SingleOrDefaultAsync(e => e.Id == identifier);

            return FromEntity(entity, dbContext);
        }

        public static async Task<SessionContext> FindAsync(DomainContext domain, string userName)
        {
            var dbContext = domain.DbContext;

            var source = dbContext.Set<SessionEntity>()
                .Include(e => e.Account)
                .Include(e => e.User);

            var entity = await source.OrderByDescending(e => e.CreatedUtc).FirstOrDefaultAsync(e => e.UserName == userName && e.ExpiresUtc == null);

            return entity != null ? new SessionContext(entity, new UserContext(entity.User, dbContext), domain) : null;
        }

        public static SessionContext FromEntity(SessionEntity entity, DbContext dbContext)
        {
            return entity != null ? new SessionContext(entity, dbContext) : null;
        }

        public SessionContext Update(string accessToken, DateTime? expireUtc, string ipAddress)
        {
            if (accessToken != null) Entity.AccessToken = accessToken;

            if (expireUtc.HasValue) Entity.ExpiresUtc = expireUtc;

            if (ipAddress != null) Entity.IpAddress = ipAddress;

            return this;
        }
    }
}
