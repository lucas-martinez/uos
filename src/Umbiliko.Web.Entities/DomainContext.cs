﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web
{
    using Constants;
    using Contracts;
    using Entities;
    using Inquiry;

    public class DomainContext : EntityContext<DomainEntity>
    {
        readonly static ConcurrentDictionary<string, DomainInfo> _dictionary;
        readonly static ConcurrentDictionary<string, object> _locks;

        static DomainContext()
        {
            _dictionary = new ConcurrentDictionary<string, DomainInfo>();
            _locks = new ConcurrentDictionary<string, object>();
        }

        readonly CompanyContext _company;
        readonly ProductContext _product;


        public DomainContext(DomainEntity entity, DbContext dbContext)
            : base(entity, dbContext)
        {
            _company = CompanyContext.FromEntity(entity.Company, dbContext);
            _product = new ProductContext(entity.Product, dbContext);
        }

        public DomainContext(DomainEntity entity, CompanyContext company)
            : base(entity, company.DbContext)
        {
            _company = company;
            _product = new ProductContext(entity.Product, company.DbContext);
        }

        public CompanyContext Company
        {
            get { return _company; }
        }

        public string Name
        {
            get { return Entity.DomainName; }
        }

        public ProductContext Product
        {
            get { return _product; }
        }

        public static DomainContext Create(DbContext dbContext, string domainName, string environment, string description = null)
        {
            var dbSet = dbContext.Set<DomainEntity>();

            var entity = dbSet.Add(dbSet.Create());

            entity.Description = description;
            entity.DomainName = domainName;
            entity.Environment = environment;
            
            return new DomainContext(entity, dbContext);
        }

        public static DomainContext Create(CompanyContext company, string name, ProductContext product, string environment, string description = null)
        {
            var dbContext = company.DbContext;

            var dbSet = dbContext.Set<DomainEntity>();

            var entity = dbSet.Add(dbSet.Create());

            entity.Company = company;
            entity.Description = description;
            entity.DomainName = name;
            entity.Environment = environment;
            entity.Product = product;
            entity.ProductName = product;
            entity.CompanyCode = company;

            company.Entity.Domains.Add(entity);

            return new DomainContext(entity, company);
        }

        public static DomainContext Find(DbContext dbContext, string name)
        {
            var dbSet = dbContext.Set<DomainEntity>();

            var entity = dbSet.Find(name);

            return entity != null ? new DomainContext(entity, CompanyContext.FromEntity(entity.Company, dbContext)) : null;
        }

        public static DomainContext Find(CompanyContext company, string name)
        {
            if (company == null) return null;

            var dbContext = company.DbContext;

            var dbSet = dbContext.Set<DomainEntity>();

            var entity = dbSet.Find(name);

            return entity != null ? new DomainContext(entity, company) : null;
        }

        public static async Task<DomainContext> FindAsync(DbContext dbContext, string domainName)
        {
            var dbSet = dbContext.Set<DomainEntity>();

            var entity = await dbSet.FindAsync(domainName);

            return entity != null ? new DomainContext(entity, CompanyContext.FromEntity(entity.Company, dbContext)) : null;
        }

        public static async Task<DomainContext> FindAsync(CompanyContext company, string domainName)
        {
            if (company == null) return null;

            var dbContext = company.DbContext;

            var dbSet = dbContext.Set<DomainEntity>();

            var entity = await dbSet.FindAsync(domainName);

            return entity != null ? new DomainContext(entity, company) : null;
        }

        public static DomainContext FromEntity(DomainEntity entity, DbContext dbContext)
        {
            return entity != null ? new DomainContext(entity, dbContext) : null;
        }

        public Expression<Func<T, bool>> Predicate<T>() where T : ITrade
        {
            string companyCode = this;

            return PredicateBuilder.True<T>().And(e => e.CompanyCode == companyCode);
        }

        public IQueryable<AccountEntity> Select(string customerCode = null, string userName = null, string segmentCode = null, string emailAddress = null)
        {
            var account = PredicateBuilder.True<AccountEntity>();
            var contract = PredicateBuilder.True<ContractEntity>();
            var environment = Entity.Environment;
            var supplier = Entity.CompanyCode;

            contract = contract.And(e => e.CompanyCode == supplier && e.Consumer.Environment == environment);

            if (customerCode == null)
            {
                customerCode = Entity.CustomerCode;
            }
            else if (Entity.CustomerCode != null && customerCode != Entity.CustomerCode)
            {
                throw new ArgumentException("customer");
            }

            if (customerCode != null)
            {
                contract = contract.And(e => e.CustomerCode == customerCode);
                account = account.And(e => e.CompanyCode == customerCode);
            }

            if (segmentCode != null)
            {
                contract = contract.And(e => e.ConsumerCode == segmentCode);
                account = account.And(e => e.SegmentCode == segmentCode);
            }

            if (userName != null)
            {
                account = account.And(e => e.UserName == userName);
            }
            else if (emailAddress != null)
            {
                account = account.And(e => e.User.EmailAddress == emailAddress);
            }

            return DbContext.Set<ContractEntity>().Where(contract).AsExpandableAsync().SelectMany(e => e.Consumer.Accounts).Where(account).AsExpandableAsync();
        }

        public void Update(ProductContext product = null, string environment = null, string description = null)
        {
            if (description != null) Entity.Description = description;

            if (environment != null) Entity.Environment = environment;

            if (product != null)
            {
                Entity.Product = product;
                Entity.ProductName = product;
            }
        }

        public static implicit operator string (DomainContext context)
        {
            return context != null ? context.Entity.DomainName : null;
        }
    }
}
