﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web.Entities
{
    public class DebugMigrationsLogger : System.Data.Entity.Migrations.Infrastructure.MigrationsLogger
    {
        public override void Info(string message)
        {
            Debug.WriteLine(message);
        }
        public override void Verbose(string message)
        {
            Debug.WriteLine(message);
        }
        public override void Warning(string message)
        {
            Debug.WriteLine("WARNING: " + message);
        }
    }
}
