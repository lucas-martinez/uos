﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web
{
    using Constants;
    using Entities;

    public class FeatureContext : EntityContext<FeatureEntity>
    {
        readonly ProductContext _product;

        public FeatureContext(FeatureEntity entity, ProductContext product)
            : base(entity, product.DbContext)
        {
            _product = product;
        }

        public ActivityEntity AddActivity(string name, string type = AccessibilityLevels.Internal, string description = null)
        {
            var activity = new ActivityEntity
            {
                ActivityName = name,
                ActivityType = type,
                Company = Entity.Company,
                Description = description,
                Feature = Entity,
                FeatureName = Entity.FeatureName,
                Product = Entity.Product,
                ProductName = Entity.ProductName,
                Roles = string.Empty,
                CompanyCode = Entity.CompanyCode,
            };

            Entity.Activities.Add(activity);

            return activity;
        }

        public FeatureEntity AddActivities(IDictionary<string, string> activities)
        {
            foreach (var entry in activities)
            {
                AddActivity(entry.Key, description: entry.Value);
            }

            return Entity;
        }

        public void AddOrUpdateActivities(IDictionary<string, string> activities)
        {
            foreach (var entry in activities)
            {
                var activity = Entity.Activities.SingleOrDefault(e => e.ActivityName == entry.Key);

                if (activity == null)
                {
                    activity = AddActivity(entry.Key, description: entry.Value);
                }
                else if (entry.Value != null)
                {
                    activity.Description = entry.Value;
                }
            }
        }

        public SubscriptionEntity AddOrUpdateSubscription(ContractEntity contract)
        {
            var subscription = Entity.Subscriptions.SingleOrDefault(e => e.CustomerCode == contract.CustomerCode && e.ConsumerCode == contract.ConsumerCode);

            if (subscription == null)
            {
                subscription = AddSubscription(contract);
            }

            return subscription;
        }

        public SubscriptionEntity AddSubscription(ContractEntity contract, DateTime? untilUtc = null)
        {
            var dbSet = DbContext.Set<SubscriptionEntity>();

            var subscription = new SubscriptionEntity
            {
                ConsumerCode = contract.ConsumerCode,
                CustomerCode = contract.CustomerCode,
                Feature = Entity,
                FeatureName = Entity.FeatureName,
                Product = Entity.Product,
                ProductName = Entity.ProductName,
                Consumer = contract.Consumer,
                Status = SubscriptionStatuses.Active,
                UntilUtc = untilUtc,
                CompanyCode = Entity.CompanyCode,
            };

            dbSet.Add(subscription);

            contract.Subscriptions.Add(subscription);
            Entity.Subscriptions.Add(subscription);

            return subscription;
        }

        public static FeatureContext Create(ProductContext product, string featureName, string description = null)
        {
            var dbContext = product.DbContext;

            var dbSet = dbContext.Set<FeatureEntity>();

            var entity = dbSet.Add(dbSet.Create());
            
            entity.FeatureName = featureName;
            entity.Product = product;
            entity.ProductName = product;
            entity.Company = product.Company;
            entity.Description = description;
            entity.CompanyCode = product.Company;
            
            product.Entity.Features.Add(entity);

            return new FeatureContext(entity, product);
        }

        public static FeatureContext Find(ProductContext product, string featureName)
        {
            if (product == null) return null;

            var dbContext = product.DbContext;

            var dbSet = dbContext.Set<FeatureEntity>();
            
            var entity = dbSet.Find(product.Entity.CompanyCode, product.Entity.ProductName, featureName);

            return entity != null ? new FeatureContext(entity, product) : null;
        }

        public static async Task<FeatureContext> FindAsync(ProductContext product, string featureName)
        {
            if (product == null) return null;

            var dbContext = product.DbContext;

            var dbSet = dbContext.Set<FeatureEntity>();
            
            var entity = await dbSet.FindAsync(product.Entity.CompanyCode, product.Entity.ProductName, featureName);

            return entity != null ? new FeatureContext(entity, product) : null;
        }

        public void Update(string description = null, IDictionary<string, string> activities = null)
        {
            if (description != null) Entity.Description = description;

            if (activities != null)
            {
                AddOrUpdateActivities(activities);
            }
        }
    }
}
