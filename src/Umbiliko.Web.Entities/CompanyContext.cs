﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web
{
    using Constants;
    using Contracts;
    using Entities;
    using Inquiry;
    using Models;

    public class CompanyContext : EntityContext<CompanyEntity>
    {
        private CompanyContext(CompanyEntity company, DbContext dbContext)
            : base(company, dbContext)
        {
        }

        public string CompanyCode
        {
            get { return Entity.CompanyCode; }
        }

        public string CompanyName
        {
            get { return Entity.CompanyName; }
        }

        public string CompanyType
        {
            get { return Entity.CompanyType; }
        }

        public DomainContext AddDomain(string name, ProductContext product, string environment, string description = null)
        {
            return DomainContext.Create(this, name, product, environment, description);
        }

        public ProductContext AddProduct(string name, string description = null)
        {
            return ProductContext.Create(this, name).Update(description: description);
        }

        public SegmentContext AddSegment(string client, string description = null)
        {
            return SegmentContext.Create(this, client, description);
        }

        public SegmentContext AddOrUpdateSegment(string segmentCode, string environment = Environments.Production, string description = null)
        {
            var segment = SegmentContext.Find(this, segmentCode);

            if (segment == null)
            {
                segment = SegmentContext.Create(this, segment: segmentCode, environment: environment, description: description);
            }
            else
            {
                segment.Update(environment: environment, description: description);
            }

            return segment;
        }

        public DomainContext AddOrUpdateDomain(string name, ProductContext product, string environment, string description = null)
        {
            var domain = DomainContext.Find(this, name);

            if (domain != null)
            {
                domain.Update(product, environment, description);
            }
            else
            {
                domain = DomainContext.Create(this, name, product, environment, description);
            }

            return domain;
        }

        public async Task<ProductContext> AddOrUpdateProductAsync(string name, string description = null)
        {
            var product = await ProductContext.FindAsync(this, name) ?? ProductContext.Create(this, name);

            product.Update(description);

            return product;
        }

        public static CompanyContext Create(DbContext dbContext, string companyCode)
        {
            var dbSet = dbContext.Set<CompanyEntity>();

            var entity = dbSet.Add(dbSet.Create());

            entity.CompanyCode = companyCode;

            return new CompanyContext(entity, dbContext);
        }

        public static void DeleteSegments(List<int> identifiers)
        {
        }

        public static async Task<CompanyContext> FindAsync(DbContext dbContext, string companyCode)
        {
            var dbSet = dbContext.Set<CompanyEntity>();

            var entity = await dbSet.FindAsync(companyCode);

            return entity != null ? new CompanyContext(entity, dbContext) : null;
        }

        public static CompanyContext FromEntity(CompanyEntity entity, DbContext dbContext)
        {
            return new CompanyContext(entity, dbContext);
        }

        public ICollection<string> GetClients()
        {
            return Entity.Segments.Select(e => e.SegmentCode).ToList();
        }

        public ICollection<string> GetProductNames()
        {
            return Entity.Products.Select(e => e.ProductName).ToList();
        }

        public Expression<Func<T, bool>> Predicate<T, TIdentifier>()
            where T : ICompanyEntity<TIdentifier>
            where TIdentifier : IComparable<TIdentifier>, IEquatable<TIdentifier>
        {
            var companyCode = Entity.CompanyCode;

            return PredicateBuilder.True<T>().And(e => e.CompanyCode == companyCode);
        }

        public CompanyContext Update(string name, string type)
        {
            if (name != null) Entity.CompanyName = name;

            if (type != null) Entity.CompanyType = type;

            return this;
        }

        public static implicit operator string (CompanyContext context)
        {
            return context != null ? context.Entity.CompanyCode : null;
        }
    }
}