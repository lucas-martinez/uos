﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uos.Web
{
    using Constants;
    using Contracts;
    using Entities;
    
    public class InstanceContext : EntityContext<InstanceEntity>
    {
        readonly DomainContext _domain;

        public InstanceContext(InstanceEntity entity, DbContext dbContext)
            : base(entity, dbContext)
        {
            _domain = DomainContext.FromEntity(entity.Domain, dbContext);
        }

        public DomainContext Domain
        {
            get { return _domain; }
        }

        public static async Task<InstanceContext> FindAsync(DbContext dbContext, Guid identifier)
        {
            var dbSet = dbContext.Set<InstanceEntity>();
            
            var entity = await dbSet
                .Include(e => e.Domain)
                .Include(e => e.Domain.Company)
                .Include(e => e.Domain.Product)
                .Include(e => e.Domain.Product.Company)
                .SingleOrDefaultAsync(e => e.Id == identifier);
            
            return FromEntity(entity, dbContext);
        }

        public static InstanceContext FromEntity(InstanceEntity entity, DbContext dbContext)
        {
            return entity != null ? new InstanceContext(entity, dbContext) : null;
        }
    }
}
