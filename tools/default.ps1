Include .\versioning.ps1

properties {
    $solutionName = "ContinuousDelivery"
    $baseDirectory = (Get-Item $psake.build_script_dir).Parent.FullName
    $buildDirectory = "$baseDirectory\.build"
    $buildLog = "$buildDirectory\compile.log"
    $configuration = "Release"
    $contentDirectory = "$buildDirectory\content"    
    $maxWarningsAllowed = 22
    $packageDirectory = "$buildDirectory\package"
    $srcDirectory = "$baseDirectory\src"
    $nuspecPath = "$srcDirectory\$solutionName.nuspec"
    $solution = "$srcDirectory\$solutionName.sln"
    $testDirectory = "$buildDirectory\test"
    $toolsDirectory = "$baseDirectory\tools"
    $majorVersion = 0
    $minorVersion = 1
    $patchVersion = 0
    $buildNumber = $null
    $prerelease = $null
}

# Build process: Clean, Compile, Test, Inspect, Package
task default -depends Package

# Delete existing build directory and clean solution
task Clean {
    # Delete existing build directory
    if (Test-Path $buildDirectory) 
    {
        Remove-Item $buildDirectory -Force -Recurse
    }
    
    # Clean solution
    # VisualStudioVersion property is necessary to locate WebApplication target
    & msbuild $solution /t:Clean /v:Quiet
}

# Compile solution
task Compile -depends Clean, VersionAssemblies {
    # Create build directory
    New-Item $buildDirectory -ItemType Directory -Force | Out-Null

    # Compile solution
    # VisualStudioVersion property is necessary to locate WebApplication target
    & msbuild $solution /fl /flp:logfile=$buildLog /p:Configuration=$configuration /p:OutDir=$contentDirectory /t:Build /v:Quiet

    # Copy binaries to test directory for testing
    Copy-Item $contentDirectory\* -Destination $buildDirectory\test

    # Move binaries from _PublishedWebsites to content directory
    Remove-Item $contentDirectory\* -Exclude _PublishedWebsites -Force
    Copy-Item $contentDirectory\_PublishedWebsites\*\* $contentDirectory -Exclude packages.config -Recurse
    Remove-Item $contentDirectory\_PublishedWebsites -Force -Recurse
}

# TODO: Refactor the generation logic to separate functions
# Generates a consistent version number for artifacts such as assemblies and packages
task VersionAssemblies {
    $assemblyVersion = Get-AssemblyVersion -Major $majorVersion -Minor $minorVersion -Patch $patchVersion -Build $buildNumber
    $commonAssemblyInfoPath = "$srcDirectory\Common\CommonAssemblyInfo.cs"
    $nugetVersion = Get-NugetVersion -Major $majorVersion -Minor $minorVersion -Patch $patchVersion -Build $buildNumber -Prerelease $prerelease
    $semanticVersion = Get-SemanticVersion -Major $majorVersion -Minor $minorVersion -Patch $patchVersion -Build $buildNumber -Prerelease $prerelease
    $year = Get-Date -Format "yyyy"
    
    Write-Output "Versioning assemblies with assembly version $assemblyVersion and semantic version $semanticVersion"

    # Backup versioned files
    Copy-Item $commonAssemblyInfoPath "$commonAssemblyInfoPath.bak" -Force
    Copy-Item $nuspecPath "$nuspecPath.bak" -Force

    # Generate assembly info version
    (Get-Content $commonAssemblyInfoPath) | ForEach-Object { 
        $_ -replace '\[assembly: AssemblyCopyright\(".*"\)\]', "[assembly: AssemblyCopyright(`"Copyright � $year`")]" `
           -replace '\[assembly: AssemblyVersion\(".*"\)]', "[assembly: AssemblyVersion(`"$assemblyVersion`")]" `
           -replace '\[assembly: AssemblyFileVersion\(".*"\)]', "[assembly: AssemblyFileVersion(`"$assemblyVersion`")]" `
           -replace '\[assembly: AssemblyInformationalVersion\(".*"\)]', "[assembly: AssemblyInformationalVersion(`"$semanticVersion`")]" `
    } | Set-Content $commonAssemblyInfoPath -Encoding UTF8

    # Generate nuspec version
    (Get-Content $nuspecPath) | ForEach-Object { $_ -replace "<version>.*</version>", "<version>$nugetVersion</version>" } | Set-Content $nuspecPath
}

# Run analysis tools such as StyleCop, FxCop, NDepend, Resharper etc
task Inspect -depends Test {
    # TODO: Resolve issue with missing Microsoft.VisualStudio.CodeAnalysis.Interop.dll on CI server then re-enable FxCop inspection
    # TODO: Assemblies to analyze should be dynamically determined, could use FxCop project
    # Run FxCop analysis
    #Invoke-Expression "$toolsDirectory\FxCop\FxCopCmd /file:$contentDirectory\bin\ContinuousDelivery.dll /gac /out:$buildDirectory\inspect.xml"

    # TODO: Parse FxCop warnings from output
    $fxCopWarnings = 0

    # StyleCop is run by MSBuild in the Compile task and outputs warnings to 
    # the MSBuild log along with compiler warnings
    $msBuildWarnings = (Get-Content $buildLog | Where-Object { $_ -like "* Warning(s)" }).Replace(" Warning(s)", "").Trim()
    
    $warnings = $fxCopWarnings + $msBuildWarnings

    Write-Output "$warnings warning(s)"

    # Fail build if warnings exceed maximum allowed
    if ($warnings -gt $maxWarningsAllowed) 
    {
        $exception = "Maximum number of warnings exceeded."
        throw $exception 
    }
}

# Create distribution package using NuGet, NPM, Windows Installer, etc
task Package -depends Inspect {     
    $tempDirectory = "$buildDirectory\.temp"
    $nuspecFile = [System.IO.Path]::GetFileName($nuspecPath)
 
    # Create temp directories
    New-Item $tempDirectory -ItemType Directory -Force | Out-Null
    New-Item $tempDirectory\content -ItemType Directory -Force | Out-Null
    New-Item $tempDirectory\tools -ItemType Directory -Force | Out-Null
    New-Item $tempDirectory\tools\Microsoft -ItemType Directory -Force | Out-Null

    # Copy package content
    Copy-Item $nuspecPath $tempDirectory
    Copy-Item $contentDirectory\** $tempDirectory\content -Recurse
    Copy-Item chocolateyInstall.ps1 $tempDirectory\tools\
    Copy-Item chocolateyUninstall.ps1 $tempDirectory\tools\
    Copy-Item configure.ps1 $tempDirectory\tools\
    Copy-Item install.ps1 $tempDirectory\tools\
    Copy-Item uninstall.ps1 $tempDirectory\tools\
    Copy-Item $toolsDirectory\Microsoft\Microsoft.Web.XmlTransform.dll $tempDirectory\tools\Microsoft\

    # Create Chocolatey package
    pushd $tempDirectory
        & $toolsDirectory\Chocolatey\chocolateyinstall\chocolatey.ps1 pack $nuspecFile
    popd

    # Move package and delete temp directory
    New-Item $packageDirectory -ItemType Directory | Out-Null
    Move-Item $tempDirectory\$solutionName.*.nupkg -Destination $packageDirectory
    Remove-Item $tempDirectory -Force -Recurse
}

# Run unit tests, test failure should fail the build
task Test -depends Compile {
    # TODO: Assemblies to test should be dynamically determined, could use NUnit project
    & $toolsDirectory\NUnit\nunit-console.exe $testDirectory\ContinuousDeliveryTests.dll /framework:4.0 /xml:$buildDirectory\test.xml
}
