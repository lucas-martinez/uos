Include .\versioning.ps1

properties {
    $solution = ""
	$module = ""
	$project = ""
    $baseDirectory = (Get-Item (Split-Path $myInvocation.MyCommand.Path)).Parent.FullName
    $buildDirectory = "$baseDirectory\.build"
    $buildLog = "$buildDirectory\compile.log"
    $configuration = "Release"
    $maxWarningsAllowed = 22
    $packageDirectory = "$buildDirectory\package"
    $srcDirectory = "$baseDirectory\src"
    $nuspecPath = "$srcDirectory\$solution.nuspec"
    $solution = "$srcDirectory\$solution.sln"
    $testDirectory = "$buildDirectory\test"
    $toolsDirectory = "$baseDirectory\tools"
    $majorVersion = 0
    $minorVersion = 1
    $patchVersion = 0
    $buildNumber = $null
    $prerelease = $null
}

# Build process: Clean, Compile, Test, Inspect, Package
task default -depends Package

task Initialize {
	Write-Output "$baseDirectory" 
    Assert ($solution -ne $null) "solution should not be null"
    $project = $solution
    if ($module -ne $null)
    {
		$solutionFile = "$solutionName.sln"
	    $project = "$solution.$module"
    }
}

# Clean solution
task Clean -depends Initialize {
    # Clean solution
    & msbuild "$solution.sln" /t:Clean /v:Quiet
}

# Compile solution
task Compile -depends Clean, VersionAssemblies {
    # Compile solution
    & msbuild $solution /fl /flp:logfile=$buildLog /p:Configuration=$configuration /t:Build /v:Quiet
}

# TODO: Refactor the generation logic to separate functions
# Generates a consistent version number for artifacts such as assemblies and packages
task VersionAssemblies {
    $assemblyVersion = Get-AssemblyVersion -Major $majorVersion -Minor $minorVersion -Patch $patchVersion -Build $buildNumber
    $commonAssemblyInfoPath = "$srcDirectory\Common\CommonAssemblyInfo.cs"
    $nugetVersion = Get-NugetVersion -Major $majorVersion -Minor $minorVersion -Patch $patchVersion -Build $buildNumber -Prerelease $prerelease
    $semanticVersion = Get-SemanticVersion -Major $majorVersion -Minor $minorVersion -Patch $patchVersion -Build $buildNumber -Prerelease $prerelease
    $year = Get-Date -Format "yyyy"
    
    Write-Output "Versioning assemblies with assembly version $assemblyVersion and semantic version $semanticVersion"

    # Backup versioned files
    Copy-Item $commonAssemblyInfoPath "$commonAssemblyInfoPath.bak" -Force
    Copy-Item $nuspecPath "$nuspecPath.bak" -Force

    # Generate assembly info version
    (Get-Content $commonAssemblyInfoPath) | ForEach-Object { 
        $_ -replace '\[assembly: AssemblyCopyright\(".*"\)\]', "[assembly: AssemblyCopyright(`"Copyright � $year`")]" `
           -replace '\[assembly: AssemblyVersion\(".*"\)]', "[assembly: AssemblyVersion(`"$assemblyVersion`")]" `
           -replace '\[assembly: AssemblyFileVersion\(".*"\)]', "[assembly: AssemblyFileVersion(`"$assemblyVersion`")]" `
           -replace '\[assembly: AssemblyInformationalVersion\(".*"\)]', "[assembly: AssemblyInformationalVersion(`"$semanticVersion`")]" `
    } | Set-Content $commonAssemblyInfoPath -Encoding UTF8

    # Generate nuspec version
    (Get-Content $nuspecPath) | ForEach-Object { $_ -replace "<version>.*</version>", "<version>$nugetVersion</version>" } | Set-Content $nuspecPath
}

# Run analysis tools such as StyleCop, FxCop, NDepend, Resharper etc
task Inspect -depends Test {
    # TODO: Resolve issue with missing Microsoft.VisualStudio.CodeAnalysis.Interop.dll on CI server then re-enable FxCop inspection
    # TODO: Assemblies to analyze should be dynamically determined, could use FxCop project
    # Run FxCop analysis
    #Invoke-Expression "$toolsDirectory\FxCop\FxCopCmd /file:$contentDirectory\bin\ContinuousDelivery.dll /gac /out:$buildDirectory\inspect.xml"

    # TODO: Parse FxCop warnings from output
    $fxCopWarnings = 0

    # StyleCop is run by MSBuild in the Compile task and outputs warnings to 
    # the MSBuild log along with compiler warnings
    $msBuildWarnings = (Get-Content $buildLog | Where-Object { $_ -like "* Warning(s)" }).Replace(" Warning(s)", "").Trim()
    
    $warnings = $fxCopWarnings + $msBuildWarnings

    Write-Output "$warnings warning(s)"

    # Fail build if warnings exceed maximum allowed
    if ($warnings -gt $maxWarningsAllowed) 
    {
        $exception = "Maximum number of warnings exceeded."
        throw $exception 
    }
}

# Create distribution package using NuGet, NPM, Windows Installer, etc
task Package -depends Compile {     
    #Create NuGet package
	
}

# Run unit tests, test failure should fail the build
task Test -depends Compile {
    # TODO: Assemblies to test should be dynamically determined, could use NUnit project
    & $toolsDirectory\NUnit\nunit-console.exe $testDirectory\ContinuousDeliveryTests.dll /framework:4.0 /xml:$buildDirectory\test.xml
}
