# Versioning extensions

function Get-AssemblyVersion {
    Param (
        [Parameter(Position=0, Mandatory=$True, ValueFromPipeline=$True)]
        [int]$major = 0,
        [Parameter(Position=1, Mandatory=$True, ValueFromPipeline=$True)]
        [int]$minor = 1, 
        [Parameter(Position=2, Mandatory=$False, ValueFromPipeline=$True)]
        [int]$patch = 0, 
        [Parameter(Position=3, Mandatory=$False, ValueFromPipeline=$True)]
        [int]$build = 0
    )

    return $("{0}.{1}.{2}.{3}" -f $major, $minor, $patch, $build)
}

function Get-SemanticVersion{
    Param (
        [Parameter(Position=0, Mandatory=$True, ValueFromPipeline=$True)]
        [int]$major = 0,
        [Parameter(Position=1, Mandatory=$True, ValueFromPipeline=$True)]
        [int]$minor = 1, 
        [Parameter(Position=2, Mandatory=$False, ValueFromPipeline=$True)]
        [int]$patch = 0, 
        [Parameter(Position=3, Mandatory=$False, ValueFromPipeline=$True)]
        [int]$build,
        [Parameter(Position=4, Mandatory=$False, ValueFromPipeline=$True)]
        [string]$hash,
        [Parameter(Position=5, Mandatory=$False, ValueFromPipeline=$True)]
        [string]$prerelease = $null,
        [string]$metadataDelimiter = ".",
        [string]$buildSeparator = "+"
    )

    if ($prerelease) 
    {
        $prereleaseMetadata = "-$prerelease"
    }
  
    # When no build number is specified mark the version as a dev build
    if (!$build) 
    { 
        $buildMetadata = "{0}{1}" -f $buildSeparator, "experimental"
    } 
    else 
    {
        $buildMetadata = "{0}{1}" -f $buildSeparator, $build
    }

    if ($hash) 
    {
        $hashMetadata = "{0}sha{0}{1}" -f $metadataDelimiter, $hash
    }

    return "{0}.{1}.{2}{3}{4}{5}" -f $major, $minor, $patch, $prereleaseMetadata, $buildMetadata, $hashMetadata
}

function Get-NuGetVersion {
    Param (
        [Parameter(Position=0, Mandatory=$True, ValueFromPipeline=$True)]
        [int]$major = 0,
        [Parameter(Position=1, Mandatory=$True, ValueFromPipeline=$True)]
        [int]$minor = 1, 
        [Parameter(Position=2, Mandatory=$False, ValueFromPipeline=$True)]
        [int]$patch = 0, 
        [Parameter(Position=3, Mandatory=$False, ValueFromPipeline=$True)]
        [int]$build,
        [Parameter(Position=4, Mandatory=$False, ValueFromPipeline=$True)]
        [string]$hash,
        [Parameter(Position=5, Mandatory=$False, ValueFromPipeline=$True)]
        [string]$prerelease = $null
    )

    if ($prerelease)
    {
        $nugetVersion = Get-SemanticVersion -Major $major -Minor $minor -Patch $patch -Build $build -Hash $hash -Prerelease $prerelease -BuildSeparator "-" -MetadataDelimiter "-"
    }
    else
    {
        $nugetVersion = Get-AssemblyVersion -Major $major -Minor $minor -Patch $patch -Build $build
    }

    return $nugetVersion;
}
